USE [OnRamp]
GO
/****** Object:  StoredProcedure [dbo].[GetLocationGroups]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 19 Sept 2011
-- Description:	To Search Location Group
-- =============================================

CREATE PROCEDURE [dbo].[GetLocationGroups]  -- [GetLocationGroups] '1=1'
(
	@SearchCriteria Varchar(2000)
)
AS
	SET NOCOUNT OFF;

DECLARE @SQL Varchar(MAX)

BEGIN

	SET @SQL =	'SELECT ID_LocationGroup, Name, ParentName,Is_Deleted,Is_Active FROM
				(
				SELECT		 A.ID_LocationGroup, A.Name, isnull(B.Name,'''') as ParentName,A.Is_Deleted,A.Is_Active
				FROM	Location_Groups A LEFT OUTER JOIN Location_Groups B 
					ON A.ParentID=B.ID_LocationGroup AND B.Is_Deleted=0 AND B.Is_Active=1
				WHERE  A.Is_Deleted=0 AND A.Is_Active=1  	
				) Temp WHERE ' + @SearchCriteria + ' ORDER BY ID_LocationGroup ASC '
				 

	PRINT @SQL;
	EXEC(@SQL);

END

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Get_ItemFSTAssignment]    Script Date: 06/24/2013 15:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 22-June-2009
-- Description:	To Assign Assest to FS
-- =============================================

CREATE PROCEDURE [dbo].[Get_ItemFSTAssignment]  
@ID_Assets Varchar(500),
@ID_Templates Varchar(500),
@TaskIDs nvarchar(Max),
@StartDate Datetime = null,
@EndDate Datetime = null

AS
Begin
	--SET NOCOUNT ON;
	Declare @ID_FSAssets bigint
	Declare @SQl nvarchar(Max)	
	Declare @SQLStatus nvarchar(Max)	
	-- Check Asset, Template exist or not
BEGIN TRY
 
	SET @SQL =
	 'Select Id_Asset,[Name] into ##TblAssets 
	 from Assets
     where Id_Asset IN ( ' + @ID_Assets + ')'
   print @SQL
	exec(@SQL)

	SET @SQL = 
       'Select Tasks.Id_Tasks,Tasks.Title,FS_Templates.Title AS FSTitle into ##TblFSTasks		
		from FS_Tasks 
		Left outer join Tasks 
        ON FS_Tasks.Id_Tasks = Tasks.Id_Tasks
		Left outer join FS_Templates 
        ON FS_Tasks.Id_Template = FS_Templates.Id_Template
		where FS_Tasks.Id_Template IN (' + @ID_Templates + ') AND Tasks.Is_Deleted = 0'
	exec(@SQL)

	SET @SQL = 
        'Select Id_Template,Title into ##TblTemplates
		 from FS_Templates where Id_Template IN (' + @ID_Templates + ')'
	exec(@SQL)

	SET @SQL = 
	 'Select ID_Tasks,Title  into ##TblTask
		from Tasks where Id_tasks IN (' + @TaskIDs + ') AND Tasks.Is_Deleted = 0'
	 exec(@SQL)

	 Select ##TblAssets.Name as Assets,##TblFSTasks.FSTitle as Template,##TblFSTasks.Title as Tasks,@StartDate as StartDate,@EndDate as EndDate
		FROM ##TblAssets,##TblFSTasks
		Union 
	 Select ##TblAssets.Name as Assets,##TblTemplates.Title AS Template,##TblTask.Title as Tasks,@StartDate as StartDate,@EndDate as EndDate
		from ##TblAssets,##TblTemplates,##TblTask 
	
	Drop table ##TblAssets
    Drop table ##TblTemplates
	Drop table ##TblTask
	Drop table ##TblFSTasks

	END TRY
   BEGIN CATCH
	Drop table ##TblAssets
    Drop table ##TblTemplates
	Drop table ##TblTask
	Drop table ##TblFSTasks
   END Catch	
	

End
	
--exec [Get_ItemFSTAssignment] '177,178,179,180,181','2,5,8,9,23,43','1,2,3,4,5'
GO
/****** Object:  StoredProcedure [dbo].[Add_FSDetail]    Script Date: 06/24/2013 15:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 22/5/2009
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_FSDetail] 
	-- Add the parameters for the stored procedure here
	@ID_FieldService bigint,
	@ID_Location bigint,
	@Date_Modified datetime,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO [dbo].[FieldService_Detail]
           ([ID_FieldService]
           ,[ID_Location]
           ,[Date_Modified]
           ,[Last_Modified_By])
     VALUES
           (@ID_FieldService,
           @ID_Location,
           @Date_Modified,
           @Last_Modified_By
           )
	
	SELECT * FROM dbo.FieldService_Detail WHERE (ID_FieldServiceDetail = SCOPE_IDENTITY())

END
GO
/****** Object:  StoredProcedure [dbo].[Search_Tasks]    Script Date: 06/24/2013 15:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Deepanshu Jouhari
-- Create date: 17 May 2009
-- Description:	To Search all Tasks
-- Exec [dbo].[Search_Tasks] 'name'
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_Tasks]
	@Title nvarchar(100)
AS
BEGIN
	Declare @SearchCriteria nvarchar(500)
    set @SearchCriteria =' 1=1 ';
	SET NOCOUNT ON;
	
	if (Len(@Title)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND Title LIKE ''%'+ @Title + '%'''
	end
	--	PRINT 'Hello'

	--PRINT @SearchCriteria
	DECLARE @SQL NVARCHAR(4000)

	SET @SQL = 'SELECT	* from Tasks where Is_Active=1 and Is_Deleted=0 AND ' + @SearchCriteria + ' Order By Title'
	--PRINT @SQL

	EXEC SP_EXECUTESQL @SQL
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeFromSecs]    Script Date: 06/24/2013 15:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetTimeFromSecs] 
(	-- Add the parameters for the function here
	@TotalSecs BigInt
)
RETURNS VarChar(10)
AS
BEGIN

Declare @Time Varchar(10),
		@hours int,
		@mins int 	

IF @TotalSecs is not null AND @TotalSecs > 0
	BEGIN		

			SET @hours = @TotalSecs/3600

			SET @TotalSecs = (@TotalSecs%3600)

			SET @mins = @TotalSecs/60

			SET @TotalSecs = @TotalSecs%60
		
			SET @Time = CAST(@hours AS VARCHAR) + ':' + RIGHT('00' + CAST(@mins AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(@TotalSecs AS VARCHAR(2)),2) 
	END
ELSE	
		SET @Time = '0:00:00';	

	RETURN @Time;
	
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_FSDetail]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To DELETE a FieldService Detail
-- =============================================

Create PROCEDURE [dbo].[Delete_FSDetail]
(
	@ID_FieldServiceDetail bigint
)
AS
	SET NOCOUNT OFF;
--select * from Tasks
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

UPDATE [dbo].[FieldService_Detail] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_FieldServiceDetail] = @ID_FieldServiceDetail))
GO
/****** Object:  StoredProcedure [dbo].[Delete_FSDetails]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To DELETE a FieldService Detail
-- =============================================

Create PROCEDURE [dbo].[Delete_FSDetails]
(
	@ID_FieldService bigint
)
AS
	SET NOCOUNT OFF;
--select * from Tasks
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

UPDATE [dbo].[FieldService_Detail] 
SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_FieldService] = @ID_FieldService))
GO
/****** Object:  StoredProcedure [dbo].[Search_Fieldservice]    Script Date: 06/24/2013 15:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Deepanshu Jouhari
-- Create date: 26 May 2009
-- Modified By : Vinay Bansal
-- Modified Date : 04 June 2009
-- Description:	To Search all Tasks
-- Exec [dbo].[Search_Fieldservice] 'name'
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_Fieldservice]
	@Title nvarchar(100),
	@ID_Location bigint,
	@ID_Employee BIGINT,
	@FromDate Datetime,
	@ToDate Datetime
AS
BEGIN
	Declare @SearchCriteria nvarchar(500)
    set @SearchCriteria =' 1=1 ';
	SET NOCOUNT ON;
	-- EXEC [Search_Fieldservice] '',-1,-1,null,null
	if (Len(@Title)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND FS.Title LIKE ''%'+ @Title + '%'''
	end
	--	PRINT 'Hello'

	--PRINT @SearchCriteria
	DECLARE @SQL NVARCHAR(4000)

	SET @SQL = 'SELECT	FS.*,L.Name as location,E.UserName 
				FROM dbo.fieldService FS
				Left Outer Join dbo.Locations L on FS.ID_Location=L.ID_Location
				Left Outer Join  dbo.Employees E on FS.ID_Employee=E.ID_Employee
				where FS.Is_Active=1 and FS.Is_Deleted=0 
				AND (' + CAST(@ID_Location AS Nvarchar) + ' = ''-1'' OR FS.ID_Location=' +  CAST(@ID_Location AS Nvarchar) + ')
				AND (' + CAST(@ID_Employee AS Nvarchar) + ' = ''-1'' OR FS.ID_Employee=' +  CAST(@ID_Employee AS Nvarchar) + ')
				AND FS.[DueDate] between '''  + convert(varchar(12),@FromDate,101) + ''' and ''' + convert(varchar(12),@ToDate,101) +'''
				AND ' + @SearchCriteria 

		
--			if(@FromDate <> null)
--				SET @SQl =@SQl + ' AND cast(convert(varchar(12),FS.[DueDate],101) as datetime)>= cast(convert(varchar(12),'+ @FromDate +',101) as  datetime) '+''
--			if(@ToDate <> null)
--				SET @SQl =@SQl + ' AND cast(convert(varchar(12),FS.[[DueDate]],101) as datetime)<= cast(convert(varchar(12),'+ @ToDate +',101) as  datetime) '+'' 
		SET @SQL = @SQl + + ' Order By FS.Title'
			
	PRINT @SQL

	EXEC SP_EXECUTESQL @SQL
--Select * from locations
--select * from Employees

END
GO
/****** Object:  StoredProcedure [dbo].[Add_FSAssets_old1]    Script Date: 06/24/2013 15:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 22-June-2009
-- Description:	To Assign Assest to FS
-- exec [dbo].[Add_FSAssets] 535,19,'2009-07-07','2010-07-07','','2009-07-07',28
-- =============================================

CREATE PROCEDURE [dbo].[Add_FSAssets_old1]  
@ID_Asset BIGINT,
@ID_Template bigint,
@StartDate Datetime,
@EndDate Datetime,
@TaskIDs nvarchar(Max),
@DateCreated DateTime,
@Last_Modified_By bigint
AS
Begin
	--SET NOCOUNT ON;
	Declare @ID_FSAssets bigint
	Declare @SQl nvarchar(Max)	
	Declare @SQLStatus nvarchar(Max)	
	-- Check Asset, Template exist or not
	set @ID_FSAssets = 0
	SET @SQLStatus = '	  Update [FS_AssetStatus] set [TaskStatus] = 1050 , FSStatus=1050
						  WHERE ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
						  AND ID_Tasks IN ('+ @TaskIDs +')	
						  AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'
						  AND [DueDate] < cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime) 
						  AND FsStatus = 1010 AND TaskStatus = 1010 -- Status New

						  
                          Delete from [FS_AssetStatus] 
						  WHERE ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
						  AND ID_Tasks IN ('+ @TaskIDs +')	
						  AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'
						  AND [DueDate] >= cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime) 
						  AND FsStatus = 1010 AND TaskStatus = 1010 -- Status New
		
				-- Insert into Fs_Assetes
		INSERT INTO FS_Assets (ID_Asset,ID_Template, StartDate,EndDate,Last_Modified_By,Date_Created,Date_Modified)
		Values ('+ Cast(@ID_Asset as nvarchar) + ','+ Cast(@ID_Template as nvarchar) +	',  cast('''+ CONVERT(varchar(10),(@StartDate), 101) +			
					  ''' As Datetime) , cast('''+ CONVERT(varchar(10),(@EndDate), 101) +			
					  ''' As Datetime) ,'+ Cast(@Last_Modified_By as nvarchar) 
					  +', GetDate(), GetDate())'
		print @SQLStatus
		Exec sp_executesql @SQLStatus
		Select @ID_FSAssets = Scope_Identity()
		
		SET @SQL = 'INSERT INTO FS_AssetDetails (ID_FSAsset,ID_Tasks, IS_Active,Is_Deleted,Last_Modified_By,Date_Modified)
					SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks, 1,0,'+ Cast(@Last_Modified_By as nvarchar) 
					+' , GetDate() From Tasks 
					Where ID_Tasks IN ('+ @TaskIDs +') OR 
						ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
					--Print @SQL

		-- Insert Into [FS_AssetStatus] By All the tasks
		INSERT INTO [FS_AssetStatus] ([ID_FSAsset],[ID_Tasks],[ID_Asset],[ID_Template],[DueDate],[TaskStatus],[FSStatus],[IS_Active],[IS_Deleted],[Last_Modified_By],[Date_Created],[Date_Modified])     
		   			  SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks,'+Cast(@ID_Asset as nvarchar) +
				      ','+ Cast(@ID_Template as nvarchar) +			
					  ', cast('''+ CONVERT(varchar(10),(@StartDate), 101) +			
					  ''' As Datetime) ,1010,1010,1,0,'+ Cast(@Last_Modified_By as nvarchar) 
					  +', GetDate(), GetDate() From Tasks 
					  Where ID_Tasks IN ('+ @TaskIDs +')
					  OR ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
	'
		print @SQL
		Exec sp_executesql @SQL
		
END
GO
/****** Object:  StoredProcedure [dbo].[Search_Employees]    Script Date: 06/24/2013 15:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Asif Ali
-- Create date: 21th March 2008
-- Description:	To Search all Employees by Surname, Department & Employee No.
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_Employees]
	@SearchCriteria NVARCHAR(400)
AS
BEGIN
	SET NOCOUNT ON;

if (Len(@SearchCriteria)>0)
	set @SearchCriteria = ' WHERE ' + @SearchCriteria
 

	DECLARE @SQL NVARCHAR(4000)
	SET @SQL = 'SELECT	ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, 
						ID_Department, ID_SecurityGroup, ID_Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, 
						Last_Modified_By,StateName, DepartmentName, SecurityGroupName, Skill
				
				FROM    Vw_SearchEmployees ' + @SearchCriteria 



--				WHERE '	+ @SearchCriteria 
				

	--PRINT @SQL
	set dateformat dmy 
	EXEC SP_EXECUTESQL @SQL
END


--.........................................................................................................................................................
--ALTER PROCEDURE [dbo].[Search_Employees] --'a', 1 , '1'
--	@Surname	varchar(50),
--	@ID_Department	bigint,
--	@EmpNo		varchar(50)
--AS
--	SET NOCOUNT ON;
--
--SELECT     ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, ID_Department, ID_SecurityGroup, ID_Skill, 
--           Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,
--		   StateName, DepartmentName, SecurityGroupName, Skill
--
--FROM       Vw_SearchEmployees
--
--WHERE	  (@Surname IS NULL or Surname LIKE @Surname + '%')
--	AND   (@ID_Department IS NULL or ID_Department = @ID_Department)
--	AND   (@EmpNo IS NULL or EmpNo LIKE @EmpNo + '%')
--	AND	   Is_Deleted=0 AND Is_Active=1
--.........................................................................................................................................................
GO
/****** Object:  StoredProcedure [dbo].[Search_Reasons]    Script Date: 06/24/2013 15:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Deepanshu Jouhari
-- Create date: 17 May 2009
-- Description:	To Search all Tasks
-- Exec [dbo].[Search_Tasks] 'name'
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_Reasons]
	@Name nvarchar(100)
AS
BEGIN
	Declare @SearchCriteria nvarchar(500)
    set @SearchCriteria =' 1=1 ';
	SET NOCOUNT ON;
	
	if (Len(@Name)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND Name LIKE ''%'+ @Name + '%'''
	end
	--	PRINT 'Hello'

	--PRINT @SearchCriteria
	DECLARE @SQL NVARCHAR(4000)

	SET @SQL = 'SELECT	* from Reasons where Is_Active=1 and Is_Deleted=0 AND ' + @SearchCriteria + ' Order By Name'
	--PRINT @SQL

	EXEC SP_EXECUTESQL @SQL
END
GO
/****** Object:  StoredProcedure [dbo].[Get_TasksBYFS_All]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 14-July- 2009
-- Description:	To GET all Tasks
-- =============================================

CREATE PROCEDURE [dbo].[Get_TasksBYFS_All] --'19,18'
@FSIDs nvarchar(Max) 
AS
BEGIN
	SET NOCOUNT ON;

	Declare @SQl nvarchar(MAx)

	SET @SQL = 'SELECT     ID_Tasks, Title,Description FROM Tasks
			WHERE	Is_Deleted=0 AND Is_Active=1
			AND ID_Tasks Not in (Select ID_Tasks From FS_Tasks 
			Where (''' + @FSIDs +'''= ''-1'' or  ID_Template in (' + @FSIDS +')))'

	Exec sp_executesql  @SQl
END
GO
/****** Object:  StoredProcedure [dbo].[getEmployeesByIDs]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--Procedure gets comma seperated employee id and retrive the list of Employees
--Deepanshu Jouhari
--exec getEmployeesByIDs '1,2,11,14,15'
CREATE PROCEDURE [dbo].[getEmployeesByIDs]
(
	@listID_Employees nvarchar(500)
)
AS
	declare @dsq  nvarchar(1500);
	--	set @str='1,2';
	set @dsq='select * from Employees where cast(ID_Employee as nvarchar(3)) in ('''+ Replace(@listID_Employees,',',''',''') +''')';
	print @dsq;
	exec(@dsq);
GO
/****** Object:  StoredProcedure [dbo].[Search_Locations]    Script Date: 06/24/2013 15:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Deepanshu Jouhari
-- Create date: 13 Jan. 2009
-- Description:	To Search all Locations
-- Exec [dbo].[Search_Locations] '1','name','L',1
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_Locations]
	@TagID varchar(50),
	@Name varchar(50),
	@LocationNo varchar(50),
	@ID_LocationGroup bigint
AS
BEGIN
	Declare @SearchCriteria nvarchar(500)
    set @SearchCriteria =' 1=1 ';
	SET NOCOUNT ON;
	
	if (Len(@TagID)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND L.TAGID LIKE ''' + @TagID + ''''
	end
--	PRINT 'Hello'
	if (Len(@Name)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND L.Name LIKE '''+ @Name + ''''
	end
 --PRINT 'Hello1'
	if (Len(@LocationNo)>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND L.LocationNo LIKE '''+ @LocationNo + ''''
	end
--PRINT 'Hello 2'
	if (@ID_LocationGroup>0)
	begin
		set @SearchCriteria = @SearchCriteria + ' AND L.ID_LocationGroup = cast('+ cast(@ID_LocationGroup as Nvarchar) + ' as bigint)' 
	end

	--PRINT @SearchCriteria
	DECLARE @SQL NVARCHAR(4000)
	SET @SQL = 'SELECT	L.*,isNULL(LG.Name,'''') as LocationGroup from Locations L Left Outer Join  Location_Groups LG ON L.ID_LocationGroup = LG.ID_LocationGroup where L.Is_Active=1 and L.Is_Deleted=0 AND ' + @SearchCriteria + ' Order By L.UsageCount DESC,L.Name'

	--PRINT @SQL
	EXEC SP_EXECUTESQL @SQL

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Assets_AllByLocation]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 20-June-2009
-- Description:	To GET all Assets
-- =============================================

CREATE PROCEDURE [dbo].[Get_Assets_AllByLocation] 
@IDs nvarchar(Max)


AS
SET NOCOUNT ON;
Declare @SQL nvarchar(Max)
Declare @ItemIDs nvarchar(Max) 
-- Select Statement
SET @ItemIDs=''
SET @SQL = 'SELECT @ItemIDs = @ItemIDs + Cast(ID_Asset as nvarchar) + '','' FROM Assets
			WHERE	 Is_Deleted=0 AND Is_Active=1
			AND ID_Location in (' + @IDs  +')'
--Print @SQL
EXEC sp_executesql 
			@query = @SQL,
			@params = N'@ItemIDs nvarchar(MAX) OUTPUT', 
			@ItemIDs = @ItemIDs OUTPUT 
Select @ItemIDs
GO
/****** Object:  StoredProcedure [dbo].[Get_Assets_AllByGroup]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 20-June-2009
-- Description:	To GET all Assets
-- =============================================

CREATE PROCEDURE [dbo].[Get_Assets_AllByGroup] 
@IDs nvarchar(Max)

AS
	SET NOCOUNT ON;
Declare @SQL nvarchar(Max)
Declare @ItemIDs nvarchar(Max) 
SET @ItemIDs=''
-- SELECt Statement
SET @SQL = 'SELECT @ItemIDs = @ItemIDs + Cast(ID_Asset as nvarchar) + '','' FROM Assets
			WHERE	 Is_Deleted=0 AND Is_Active=1
			AND ID_AssetGroup in (' + @IDs  +')'
--Print @SQL
EXEC sp_executesql 
			@query = @SQL,
			@params = N'@ItemIDs nvarchar(MAX) OUTPUT', 
			@ItemIDs = @ItemIDs OUTPUT 
Select @ItemIDs
GO
/****** Object:  StoredProcedure [dbo].[Add_FSAssetsStatus]    Script Date: 06/24/2013 15:04:01 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 20-June-2009
-- Description:	To Assign Assest to FS
-- =============================================

CREATE PROCEDURE [dbo].[Add_FSAssetsStatus]  
@ID_FSAsset BIGINT,
@ID_Asset BIGINT,
@ID_Tasks BIGINT,
@ID_Template bigint,
@StartDate Datetime,
@EndDate Datetime,
@DateCreated DateTime,
@Last_Modified_By bigint
AS
Begin
	SET NOCOUNT ON;
	

		
End
GO
/****** Object:  StoredProcedure [dbo].[Get_FSTTasks_By_IDs]    Script Date: 06/24/2013 15:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 26 June 2009
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[Get_FSTTasks_By_IDs] 
	@ID_Templates varchar(500)
AS
	SET NOCOUNT ON;
	-- Select Statement
  Declare @SQL Nvarchar(MAX)

	SET @SQL = 'SELECT F.ID_FsTask, F.ID_Template,F.ID_Tasks, T.Title
				, T.Description
				, Case  WHEN FSA.ID_Template IS NULL then Cast(1 as bit) else Cast(0 as bit) END as IS_Edit
				--, FSA.ID_Template
		FROM        [dbo].FS_Tasks  F
		INNER JOIN  Tasks T ON T.ID_Tasks =  F.ID_Tasks -- To get Task title
		LEFT OUTER JOIN  FS_Assets FSA ON FSA.ID_Template =  F.ID_Template and FSA.IS_Active = 1 AND FSA.IS_Deleted = 0
		WHERE		F.ID_Template IN (' + @ID_Templates + ')
					AND F.Is_Deleted=0 AND F.Is_Active=1
					AND T.Is_Deleted = 0				-- Added on 26 june by vijay
		Group BY F.ID_FsTask, F.ID_Template,F.ID_Tasks, T.Title
				, T.Description,FSA.ID_Template'


print @SQL;

Exec sp_executesql @SQL

-- [Get_FSTTasks_By_IDs] '2,4,6,7,11,10'
GO
/****** Object:  StoredProcedure [dbo].[Jobs_GetReportNew]    Script Date: 06/24/2013 15:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 31 Dec 2009
-- Description:	Get Jobs Report
-- exec [Jobs_GetReportNew] 2,'''j3'',''j4'',''j1'',''j2''','2010-03-01','2010-03-02'
-- =============================================

CREATE PROCEDURE [dbo].[Jobs_GetReportNew]
(
	@Option int = 0,
	@IDs varchar(6000),
	@FromDate DateTime,
	@ToDate DateTime 
)
AS
	SET NOCOUNT OFF;

Declare @Sql varchar(MAX) 

Declare @dateFrom Varchar(10),
		@toFrom Varchar(10)

SET @dateFrom = CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10))
SET @toFrom = CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10))
 
IF @Option = 0 
	BEGIN	

	SET @Sql = 'Select ID_AssetMovement,ID_LocationNew,JobId,[TimeStamp] as TimeIn,[TimeStampOut] as [TimeOut],isnull(DateDiff(ss,[TimeStamp],[TimeStampOut]),0) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,'''','''',ID_AssetMovement,0) as TotalTimeOut,Id_Reason,
				AM.ID_Asset,AM.ID_Employee,AM.ID_Location 
				,A.Name as AssetName,L.Name as LocationNAme,E.Name as EmployeeName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Employees E
				on AM.ID_Employee = E.ID_Employee
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)
				AND JobId is not null AND JobId IN ( ' + @IDs + ') 
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + ''''

		print @Sql;
		EXEC(@Sql);

	SET @Sql =	'Select Count(JobId) as NoofReturns,ID_LocationNew,MIN([TimeStamp]) as TimeIn,MAx([TimeStampOut]) as [TimeOut],AM.ID_Asset,JobId,
				[dbo].[GetTimeIn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',1) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',0,1) as TotalTimeOut,
				A.Name as AssetName,L.Name as LocationName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)
				AND JobId is not null AND JobId IN ( ' + @IDs + ') 
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''
				Group By ID_LocationNew,AM.ID_Asset,JobId,A.Name,L.Name
				order by ID_LocationNew,JobId'
	

			print @Sql;

			EXEC(@Sql);
				

	END
ELSE IF @Option = 1
	BEGIN
		
			SET @Sql = 'Select ID_AssetMovement,ID_LocationNew,JobId,[TimeStamp] as TimeIn,[TimeStampOut] as [TimeOut],isnull(DateDiff(ss,[TimeStamp],[TimeStampOut]),0) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,'''','''',ID_AssetMovement,0) as TotalTimeOut,Id_Reason,
				AM.ID_Asset,AM.ID_Employee,AM.ID_Location 
				,A.Name as AssetName,L.Name as LocationNAme,E.Name as EmployeeName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Employees E
				on AM.ID_Employee = E.ID_Employee
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings WHERE id_locationin IN ( ' + @IDs + '))		
				AND JobId is not null AND JobId <> ''-1''	
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + ''''

		print @Sql;
		EXEC(@Sql);

	SET @Sql =	'Select Count(JobId) as NoofReturns,ID_LocationNew,MIN([TimeStamp]) as TimeIn,MAx([TimeStampOut]) as [TimeOut],AM.ID_Asset,JobId,
				[dbo].[GetTimeIn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',1) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',0,1) as TotalTimeOut,
				A.Name as AssetName,L.Name as LocationName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings WHERE id_locationin IN ( ' + @IDs + '))		
				AND JobId is not null AND JobId <> ''-1''	
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''
				Group By ID_LocationNew,AM.ID_Asset,JobId,A.Name,L.Name
				order by ID_LocationNew,JobId'
	

			print @Sql;

			EXEC(@Sql);

	END
ELSE IF @Option = 2
	BEGIN
		SET @Sql = 'Select ID_AssetMovement,ID_LocationNew,JobId,[TimeStamp] as TimeIn,[TimeStampOut] as [TimeOut],isnull(DateDiff(ss,[TimeStamp],[TimeStampOut]),0) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,'''','''',ID_AssetMovement,0) as TotalTimeOut,Id_Reason,
				AM.ID_Asset,AM.ID_Employee,AM.ID_Location 
				,A.Name as AssetName,L.Name as LocationNAme,E.Name as EmployeeName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Employees E
				on AM.ID_Employee = E.ID_Employee
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)
				AND JobId is not null AND  JobId <> ''-1'' 
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + ''''

		print @Sql;
		EXEC(@Sql);

	SET @Sql =	'Select Count(JobId) as NoofReturns,ID_LocationNew,MIN([TimeStamp]) as TimeIn,MAx([TimeStampOut]) as [TimeOut],AM.ID_Asset,JobId,
				[dbo].[GetTimeIn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',1) as TotalTimeIn,
				[dbo].[GetTimeInBtwn_Station](ID_LocationNew,JobID,''' + @dateFrom + ''','''+@toFrom+''',0,1) as TotalTimeOut,
				A.Name as AssetName,L.Name as LocationName
				from Asset_Movement AM
				LEFT OUTER JOIN Assets A
				on AM.ID_Asset = A.ID_Asset
				LEFT OUTER JOIN Locations L
				ON id_locationnew = L.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)
				AND JobId is not null AND JobId <> ''-1'' 
				AND Convert(varchar(10),AM.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''
				Group By ID_LocationNew,AM.ID_Asset,JobId,A.Name,L.Name
				order by ID_LocationNew,JobId'	

			print @Sql;

		EXEC(@Sql);
	END

	
 
-----------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[HexStrToVarBinary]    Script Date: 06/24/2013 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[HexStrToVarBinary](@hexstr varchar(30))
RETURNS varbinary(8000)
AS
BEGIN     
DECLARE @hex char(1), 
@i int, 
@place bigint, 
@a bigint    
SET @i = LEN(@hexstr)     
set @place = convert(bigint,1)    
SET @a = convert(bigint, 0)   
 WHILE (@i > 0 AND (substring(@hexstr, @i, 1) like '[0-9A-Fa-f]'))     
 BEGIN         
SET @hex = SUBSTRING(@hexstr, @i, 1)         
SET @a = @a + convert(bigint, CASE WHEN @hex LIKE '[0-9]'   
								    THEN CAST(@hex as int)          
								    ELSE CAST(ASCII(UPPER(@hex))-55 as int) end * @place)    
set @place = @place * convert(bigint,16)        
SET @i = @i - 1         
END     
RETURN convert(varbinary(8000),@a)
END
GO
/****** Object:  StoredProcedure [dbo].[Jobs_GetReport]    Script Date: 06/24/2013 15:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 31 Dec 2009
-- Description:	Get Jobs Report
-- exec [Jobs_GetReport] 3,'''j3'',''j4'',''j1'',''j2''','2010-01-10','2010-02-13'
-- =============================================

CREATE PROCEDURE [dbo].[Jobs_GetReport]
(
	@Option int = 0,
	@IDs varchar(6000),
	@FromDate DateTime,
	@ToDate DateTime 
)
AS
	SET NOCOUNT OFF;

Declare @Sql varchar(MAX) 

Declare @dateFrom Varchar(10),
		@toFrom Varchar(10)

SET @dateFrom = CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10))
SET @toFrom = CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10))
 
IF @Option = 0 
	BEGIN		
	SET @Sql = 'SELECT Id_AssetMovement as Id_AssetMovement,Asset_movement.ID_Asset,JobID,Id_LocationNew as ID_Location,
				Locations.Name as LocationName,   
				dbo.GetMaxTimeStamp(Id_AssetMovement,0) as TimeIn,dbo.GetMaxTimeStamp(Id_AssetMovement,1) as [TimeOut],			
				dbo.GetTimeInStation(Id_AssetMovement,0) as TimeInStation,dbo.GetTimeInStation(Id_AssetMovement,1) as TTimeInStation,
				dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeInBwtStation,
				dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TTimeInBwtStation,
				Assets.Name as AssetName,isnull(dbo.GetLastEmployee(Id_AssetMovement),'''') as EmployeeName,
				dbo.GetNoofReturns(JobID,Asset_movement.Id_Asset,Id_LocationNew) as NoofTimes,
				dbo.GetInTransit_TimeStamp(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeIn_InTransit,
				dbo.[GetInTransit_TimeStamp](Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TimeOut_InTransit,		
				dbo.GetAvgInTime(JobID,''' + @dateFrom + ''','''+@toFrom+''',Asset_movement.ID_Asset,0,0) as AvgInTime,
				dbo.GetAvgInTime(JobID,'''+ @dateFrom + ''',''' +@toFrom+''',Asset_movement.ID_Asset,0,1) as AvgInBwtTime
				
				FROM Asset_movement
				LEFT OUTER JOIN Assets
				on Asset_movement.ID_Asset = Assets.ID_Asset
				LEFT OUTER JOIN Employees
				on Asset_movement.ID_Employee = Employees.ID_Employee
				LEFT OUTER JOIN Locations
				ON id_locationnew = locations.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)
				AND JobId is not null AND JobId IN ( ' + @IDs + ') 
				AND Convert(varchar(10),Asset_movement.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''				 
			    GROUP BY JobID,Id_LocationNew,Locations.Name,Asset_movement.ID_Asset,Assets.Name,Id_AssetMovement
				'
				
			print @Sql;

		EXEC(@Sql);
	END
ELSE IF @Option = 1
	BEGIN
		SET @Sql = 'SELECT Id_AssetMovement as Id_AssetMovement,Asset_movement.ID_Asset,JobID,Id_LocationNew as ID_Location,
				Locations.Name as LocationName,   
				dbo.GetMaxTimeStamp(Id_AssetMovement,0) as TimeIn,dbo.GetMaxTimeStamp(Id_AssetMovement,1) as [TimeOut],			
				dbo.GetTimeInStation(Id_AssetMovement,0) as TimeInStation,dbo.GetTimeInStation(Id_AssetMovement,1) as TTimeInStation
				,dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeInBwtStation,
				dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TTimeInBwtStation,
				Assets.Name as AssetName,isnull(dbo.GetLastEmployee(Id_AssetMovement),'''') as EmployeeName,
				dbo.GetNoofReturns(JobID,Asset_movement.Id_Asset,Id_LocationNew) as NoofTimes,
				dbo.GetInTransit_TimeStamp(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeIn_InTransit,
				dbo.[GetInTransit_TimeStamp](Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TimeOut_InTransit,	
				dbo.GetAvgInTime(Id_LocationNew,''' + @dateFrom + ''','''+@toFrom+''',Asset_movement.ID_Reader,1,0) as AvgInTime,
				dbo.GetAvgInTime(Id_LocationNew,'''+ @dateFrom + ''',''' +@toFrom+''',Asset_movement.ID_Reader,1,1) as AvgInBwtTime
				 
				FROM Asset_movement
				LEFT OUTER JOIN Assets
				on Asset_movement.ID_Asset = Assets.ID_Asset
				LEFT OUTER JOIN Employees
				on Asset_movement.ID_Employee = Employees.ID_Employee
				LEFT OUTER JOIN Locations
				ON id_locationnew = locations.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings WHERE id_locationin IN ( ' + @IDs + '))		
				AND JobId is not null AND JobId <> ''-1''			
				AND Convert(varchar(10),Asset_movement.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''				 
			    GROUP BY JobID,Id_LocationNew,Locations.Name,Asset_movement.ID_Asset,Assets.Name,Id_AssetMovement,Asset_movement.ID_Reader'
			
				-- AND JobId IN ( ' + @JobIDs + ') 	

			print @Sql;

		EXEC(@Sql);
	END
ELSE IF @Option = 2
	BEGIN
		SET @Sql = 'SELECT Id_AssetMovement as Id_AssetMovement,Asset_movement.ID_Asset,JobID,Id_LocationNew as ID_Location,
				Locations.Name as LocationName,   
				dbo.GetMaxTimeStamp(Id_AssetMovement,0) as TimeIn,dbo.GetMaxTimeStamp(Id_AssetMovement,1) as [TimeOut],			
				dbo.GetTimeInStation(Id_AssetMovement,0) as TimeInStation,dbo.GetTimeInStation(Id_AssetMovement,1) as TTimeInStation
				,dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeInBwtStation,
				dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TTimeInBwtStation,
				Assets.Name as AssetName,isnull(dbo.GetLastEmployee(Id_AssetMovement),'''') as EmployeeName,
				dbo.GetNoofReturns(JobID,Asset_movement.Id_Asset,Id_LocationNew) as NoofTimes,
				dbo.GetInTransit_TimeStamp(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeIn_InTransit,
				dbo.[GetInTransit_TimeStamp](Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TimeOut_InTransit,	
				dbo.GetAvgInTime(JobID,''' + @dateFrom + ''','''+@toFrom+''',Asset_movement.ID_Asset,0,0) as AvgInTime,
				dbo.GetAvgInTime(JobID,'''+ @dateFrom + ''',''' +@toFrom+''',Asset_movement.ID_Asset,0,1) as AvgInBwtTime
				 
				FROM Asset_movement
				LEFT OUTER JOIN Assets
				on Asset_movement.ID_Asset = Assets.ID_Asset
				LEFT OUTER JOIN Employees
				on Asset_movement.ID_Employee = Employees.ID_Employee
				LEFT OUTER JOIN Locations
				ON id_locationnew = locations.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)		
				AND JobId is not null AND JobId <> ''-1''		
				AND Convert(varchar(10),Asset_movement.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''				 
			    GROUP BY JobID,Asset_movement.ID_Asset,Assets.Name,Id_AssetMovement,Id_LocationNew,Locations.Name'
			
				-- AND JobId IN ( ' + @JobIDs + ') 	

			print @Sql;

		EXEC(@Sql);
	END
ELSE IF @Option = 3
	BEGIN
		SET @Sql = 'SELECT Id_AssetMovement as Id_AssetMovement,Asset_movement.ID_Asset,JobID,Id_LocationNew as ID_Location,
				Locations.Name as LocationName,   
				dbo.GetMaxTimeStamp(Id_AssetMovement,0) as TimeIn,dbo.GetMaxTimeStamp(Id_AssetMovement,1) as [TimeOut],			
				dbo.GetTimeInStation(Id_AssetMovement,0) as TimeInStation,dbo.GetTimeInStation(Id_AssetMovement,1) as TTimeInStation
				,dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeInBwtStation,
				dbo.GetTimeInBetweenStations(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TTimeInBwtStation,
				Assets.Name as AssetName,isnull(dbo.GetLastEmployee(Id_AssetMovement),'''') as EmployeeName,
				dbo.GetNoofReturns(JobID,Asset_movement.Id_Asset,Id_LocationNew) as NoofTimes,
				dbo.GetInTransit_TimeStamp(Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,0) as TimeIn_InTransit,
				dbo.[GetInTransit_TimeStamp](Id_AssetMovement,Asset_movement.ID_Asset,jobID,Id_locationnew,1) as TimeOut_InTransit,	
				dbo.GetAvgInTime(Id_LocationNew,''' + @dateFrom + ''','''+@toFrom+''',Asset_movement.ID_Reader,1,0) as AvgInTime,
				dbo.GetAvgInTime(Id_LocationNew,'''+ @dateFrom + ''',''' +@toFrom+''',Asset_movement.ID_Reader,1,1) as AvgInBwtTime
				 
				FROM Asset_movement
				LEFT OUTER JOIN Assets
				on Asset_movement.ID_Asset = Assets.ID_Asset
				LEFT OUTER JOIN Employees
				on Asset_movement.ID_Employee = Employees.ID_Employee
				LEFT OUTER JOIN Locations
				ON id_locationnew = locations.id_location
				WHERE id_locationnew in (select id_locationin from AntennaSettings)	
				AND JobId is not null AND JobId <> ''-1''				
				AND Convert(varchar(10),Asset_movement.Date_Modified,101) between ''' + CAST(Convert(varchar(10),@FromDate,101) AS Varchar(10)) + ''' AND ''' + CAST(Convert(varchar(10),@ToDate,101) AS Varchar(10)) + '''				 
			    GROUP BY JobID,Id_LocationNew,Locations.Name,Asset_movement.ID_Asset,Assets.Name,Id_AssetMovement,Asset_movement.ID_Reader'
			
				-- AND JobId IN ( ' + @JobIDs + ') 	

			print @Sql;

		EXEC(@Sql);
	END
 
-----------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Check_DuplicateTagID_forUPDATE]    Script Date: 06/24/2013 15:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 25th March 2008
-- Description:	To CHECK for Duplicate TAG ID.
-- =============================================

CREATE PROCEDURE [dbo].[Check_DuplicateTagID_forUPDATE]  --'Tag-0011187',1,'Location'
	-- Add the parameters for the stored procedure here
	@TagID varchar(50),
	@ID bigint
	

	
AS
BEGIN
	if EXISTS(select @TagID From Vw_Unique_TagID where TagID = @TagID AND ID<>@ID) 
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[GetReaders_By_IPAddress]    Script Date: 06/24/2013 15:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 27 March 2010
-- Description:	To get readers by ip address
--exec GetReaders_By_IPAddress '
-- =============================================

CREATE PROCEDURE [dbo].[GetReaders_By_IPAddress]
	@IPAddresses varchar(2000)
AS
	SET NOCOUNT ON;
DECLARE @SQL VARCHAR(3000)
SET @SQL = 'SELECT ID_Reader,Reader,IPAddress,ReaderType from Readers
WHERE	IpAddress IN (' + @IPAddresses + ' )
AND Is_Deleted=0 AND Is_Active=1'

EXEC(@SQL);
GO
/****** Object:  StoredProcedure [dbo].[Search_AssetInventory]    Script Date: 06/24/2013 15:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		Vinay Bansal
-- Create date: 15-June-2009
-- Description:	To Search all Inventory
-- Modified By - Vijay
-- Modified On - 30/06/2011
-- =========================================================

CREATE PROCEDURE [dbo].[Search_AssetInventory] 
 @StartDate Datetime = NULL ,
 @EndDate Datetime = NULL ,
 @LocID bigint = 0
AS
Begin
	Set NoCount ON;
	
declare @str varchar(max)


set @str =	 'select a.Name as AssetName, l.Name as LocationName,ins.InventoryStatusName as [Status],ih.InventoryDate as Date, e.Name as [InventoryBy]
	from Inventory_History as ih
	join Assets as a on a.ID_Asset = ih.ID_Asset join Inventory_Status as ins on ins.ID_InventoryStatus = ih.InventoryStatus 
	join Locations as l on l.ID_Location = ih.InventoryLocation 
	join Employees as e on e.ID_Employee = ih.InventoryBy where 1=1 '

if @LocID > 0  
begin
set @str = @str + ' AND ih.InventoryLocation = ' + cast(@LocID as varchar(50))
end

if @StartDate is not NULL
begin
 set @str = @str + ' AND (dateadd(dd,0,datediff(dd,0,ih.InventoryDate)) >= dateadd(dd,0,datediff(dd,0,''' + convert(varchar(30), @StartDate)+''')) 
						and dateadd(dd,0,datediff(dd,0,ih.InventoryDate)) <= dateadd(dd,0,datediff(dd,0,'''+	convert(varchar(30) ,@EndDate) +''')))'
end

 

exec(@str)

END
GO
/****** Object:  StoredProcedure [dbo].[Get_MaxTagID]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 08-June-2009
-- Description:	To Get Maximum TAG ID.
-- =============================================

Create PROCEDURE [dbo].[Get_MaxTagID]  --'Tag-0011187'
	-- Add the parameters for the stored procedure here
	@TagID varchar(50)
	
AS
BEGIN
	select MAX(TagID) From Vw_Unique_TagID where tagId like @TagID
END
GO
/****** Object:  StoredProcedure [dbo].[Check_DuplicateTagID]    Script Date: 06/24/2013 15:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 25th March 2008
-- Description:	To CHECK for Duplicate TAG ID.
-- =============================================

CREATE PROCEDURE [dbo].[Check_DuplicateTagID]  --'Tag-0011187'
	-- Add the parameters for the stored procedure here
	@TagID varchar(50)
	
AS
BEGIN
	if EXISTS(select @TagID From Vw_Unique_TagID where TagID = @TagID)
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[GetAssetGroups]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 19 Sept 2011
-- Description:	To ADD a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[GetAssetGroups] --'1=1'
(
	@SearchCriteria Varchar(2000)
)
AS
	SET NOCOUNT OFF;

DECLARE @SQL Varchar(MAX)

BEGIN

	SET @SQL =	'SELECT ID_AssetGroup, Name, ParentName,Is_Deleted,Is_Active FROM
				(
				SELECT		 A.ID_AssetGroup, A.Name, isnull(B.Name,'''') as ParentName,A.Is_Deleted,A.Is_Active
				FROM	Asset_Groups A LEFT OUTER JOIN Asset_Groups B 
					ON A.ParentID=B.ID_AssetGroup AND B.Is_Deleted=0 AND B.Is_Active=1
				WHERE  A.Is_Deleted=0 AND A.Is_Active=1  	
				) Temp WHERE ' + @SearchCriteria + ' ORDER BY ID_AssetGroup ASC '
				 

	PRINT @SQL;
	EXEC(@SQL);

END
GO
/****** Object:  StoredProcedure [dbo].[DeleteReadersByIDs]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 14-09-2011
-- Description:	To DELETE a Readers by Ids
-- =============================================

CREATE PROCEDURE [dbo].[DeleteReadersByIDs]
(
	@ReaderIds Varchar(4000)
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Locations] WHERE (([ID_Location] = @ID_Location))
BEGIN

   DECLARE @SQL NVarchar(MAX)

--	SET @SQL = 'Update Readers Set Is_Deleted = 1, Is_Active = 0 
--		Where ID_Reader IN ( ' + @ReaderIds + ')' 
--
--	Exec(@SQL);

	SET @SQL = 'Delete FROM AntennaSettings Where ID_Reader IN ( ' + @ReaderIds + ')'		

	Exec(@SQL);

	SET @SQL = 'Delete FROM ExternalSensor Where ReaderID IN ( ' + @ReaderIds + ')'		

	Exec(@SQL);

	SET @SQL = 'Delete FROM Readers Where ID_Reader IN ( ' + @ReaderIds + ')' 

	Exec(@SQL);

END
GO
/****** Object:  StoredProcedure [dbo].[Get_TagMovementSettingsByReaderID]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 22-09-2011
-- Description:	<Get InOutTimeSettings Settings>
-- =============================================
CREATE PROCEDURE [dbo].[Get_TagMovementSettingsByReaderID]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader bigint		        
		)		   
	
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT dmy 
	
	SELECT isnull(ReaderID,0) as ReaderID 
				 ,InTime_From, InTime_To, OutTime_To, OutTime_From,
				  isnull(SensorEventInterval,10) as SensorEventInterval,isnull(SensorEventIgnore,10) as SensorEventIgnore,
				  IsActive				
	FROM TagMovementSettings WHERE ReaderID = @ID_Reader

END
GO
/****** Object:  StoredProcedure [dbo].[Add_TagMovementSettings]    Script Date: 06/24/2013 15:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 22-09-2011
-- Description:	<Add In Out Time Settings>
-- =============================================
CREATE PROCEDURE [dbo].[Add_TagMovementSettings]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader Int
		   ,@InTime_From Int
		   ,@InTime_To Int
		   ,@OutTime_From Int
		   ,@OutTime_To Int
		   ,@SensorEventInterval INT
		   ,@SensorEventIgnore INT
		)		   
	
AS
BEGIN
	
	SET NOCOUNT ON;
	set dateformat dmy 

IF NOT EXISTS(Select * FROM TagMovementSettings Where ReaderID = @ID_Reader)
	BEGIN
		INSERT INTO   TagMovementSettings(ReaderID,InTime_From,InTime_To,OutTime_From,OutTime_To,SensorEventInterval,SensorEventIgnore)
						  VALUES(@ID_Reader,@InTime_From,@InTime_To,@OutTime_From,@OutTime_To,@SensorEventInterval,@SensorEventIgnore)
	END
ELSE
	BEGIN
		UPDATE   TagMovementSettings SET InTime_From= @InTime_From,InTime_To=@InTime_To,OutTime_From=@OutTime_From,OutTime_To=@OutTime_To,SensorEventIgnore=@SensorEventIgnore,SensorEventInterval=@SensorEventInterval
					 WHERE ReaderID = @ID_Reader
	END

 

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Reader_By_IDReader]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <23 July 2008>
-- Description:	<Selecr Readers By Reader ID>
-- =============================================
CREATE PROCEDURE  [dbo].[Get_Reader_By_IDReader]
	-- Add the parameters for the stored procedure here
(
@ID_Reader bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ID_Reader]
      ,[Reader]
      ,[IPAddress]
      ,[ID_Location]
      ,[ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,[Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
      ,[UseDefault]
	  ,[CurrentState]
	  ,[Terminal]	
	  ,[ReaderType]	
	  ,[ChangeLocationOn]
	  ,isnull(ReaderMode,ReaderType) as ReaderMode
	  ,[RxSensitivity]
	  ,[RxSensitivityValue1] 
	  ,[RxSensitivityValue2] 
	  ,[RxSensitivityValue3] 
	  ,[RxSensitivityValue4] 
  FROM  [Readers]
  Where [ID_Reader] = @ID_Reader

	
	SELECT isnull(ReaderID,0) as ReaderID 
				,isnull(IPAddress,'') as IPAddress
				,isnull(PortNo,0) as PortNo
				,isnull(UDPPort,0) as UDPPort
				,isnull(IsActive,0) as IsActive
				,isnull(RelayOffCmd,'') as RelayOffCmd
				,isnull(RelayOnCmd,'') as RelayOnCmd 
	FROM ExternalSensor WHERE ReaderID = @ID_Reader

	SELECT isnull(ReaderID,0) as ReaderID 
				 ,InTime_From, InTime_To, OutTime_To, OutTime_From,IsActive				
	FROM TagMovementSettings WHERE ReaderID = @ID_Reader

END
GO
/****** Object:  StoredProcedure [dbo].[Add_AntennaSettings]    Script Date: 06/24/2013 15:03:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
 
-- Create date: 28 Dec 2009
-- Description:	To get antenna settings of the reader
-- =============================================

CREATE PROCEDURE [dbo].[Add_AntennaSettings]
@ID_AntennaSetting int,
@ID_Reader bigint,
@AntennaNo varchar(50),
@AntennaName varchar(50),
@ID_LocationIn bigint,
@ID_LocationOut bigint,
@TransmitPower float = 0,
@TimeOut int =0,
@AssetRSSIMax float =0,
@AssetRSSIMin float =0,
@EmpRSSIMax float =0,
@EmpRSSIMin float =0,
@Last_Modified_By int = 0,
@IsActive bit = 1,
@ChangeLocOn varchar(5) = 'TR'
AS
SET NOCOUNT ON;
BEGIN

DECLARE @ID_AssetRSSI int,
		@ID_EmpRSSI int,
		@ID_EmpTagType int,
		@ID_AssetTagType int,
		@ErrorMsg varchar(200)

SET @ID_AssetTagType = 1
SET @ID_EmpTagType = 2

IF EXISTS(SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Asset%')
		SET @ID_AssetTagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Asset%')

IF EXISTS(SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Employee%')
		SET @ID_EmpTagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Employee%')

BEGIN TRY 
BEGIN TRAN T1

	IF EXISTS(SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType)
		BEGIN
			SET @ID_AssetRSSI  = (SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType)
			-- update
			Update RSSI_Range Set RSSI_Min = @AssetRSSIMin,RSSI_Max = @AssetRSSIMax,Last_Modified_By = @Last_Modified_By
			WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType
		END
	ELSE
		BEGIN
			-- Insert
			Insert into RSSI_Range(ID_TagType,ID_Reader,AntennaNo,RSSI_Min,RSSI_Max,Is_Active,Last_Modified_By)
						VALUES(@ID_AssetTagType,@ID_Reader,@AntennaNo,@AssetRSSIMin,@AssetRSSIMax,1,@Last_Modified_By)
			SET @ID_AssetRSSI = @@Identity
		END

	IF EXISTS(SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType)
		BEGIN
			SET @ID_EmpRSSI  = (SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType)
			-- update
			Update RSSI_Range Set RSSI_Min = @EmpRSSIMin,RSSI_Max = @EmpRSSIMax,Last_Modified_By = @Last_Modified_By
			WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType
		END
	ELSE
		BEGIN
			-- Insert
			Insert into RSSI_Range(ID_TagType,ID_Reader,AntennaNo,RSSI_Min,RSSI_Max,Is_Active,Last_Modified_By)
						VALUES(@ID_EmpTagType,@ID_Reader,@AntennaNo,@EmpRSSIMin,@EmpRSSIMax,1,@Last_Modified_By)
			SET @ID_EmpRSSI = @@Identity
		END

	 IF @ID_AntennaSetting = 0 AND Not Exists(Select ID_AntennaSetting FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo)
		BEGIN
			INSERT INTO AntennaSettings(ID_Reader,AntennaNo,AntennaName,ID_LocationIn,ID_LocationOut,TransmitPower,
										ID_AssetRSSIRange,ID_EmpRSSIRange,[TimeOut],IsActive,DateCreated)
						VALUES(@ID_Reader,@AntennaNo,@AntennaName,@ID_LocationIn,@ID_LocationOut,@TransmitPower,
										@ID_AssetRSSI,@ID_EmpRSSI,@TimeOut,@IsActive,getDate())			
			
			SET @ID_AntennaSetting = @@Identity
		END
   	 ELSE
		BEGIN
			UPDATE AntennaSettings SET AntennaName = @AntennaName,ID_LocationIn = @ID_LocationIn,ID_LocationOut = @ID_LocationOut,
								       TransmitPower = @TransmitPower,ID_AssetRSSIRange = @ID_AssetRSSI,ID_EmpRSSIRange = @ID_EmpRSSI,
									   [TimeOut] = @TimeOut,IsActive = @IsActive
			WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo
			--WHERE ID_AntennaSetting = @ID_AntennaSetting
		END	
		
		IF @AntennaNo = 'Antenna1'
			UPDATE Readers SET Ant1 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna2'
			UPDATE Readers SET Ant2 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna3'
			UPDATE Readers SET Ant3 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna4'
			UPDATE Readers SET Ant4 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader

		

		SELECT @ID_AntennaSetting

COMMIT TRAN T1
END TRY

BEGIN CATCH
ROLLBACK TRAN T1
 SET @ErrorMsg = ERROR_MESSAGE();
SELECT @ErrorMsg
--RAISERROR(
--@ErrorMsg,2,1 
--); 
END CATCH
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInBetweenStations]    Script Date: 06/24/2013 15:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangtani>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetTimeInBetweenStations] 
(	-- Add the parameters for the function here
	@ID_AssetMovement BIGINT,
	@ID_Asset BIGINT,
	@JobID varchar(50),
	@ID_LocationIn Bigint,
	@option int = 0
)
RETURNS VarChar(10)
AS
BEGIN
	-- Declare the return variable here

DECLARE @InDate DateTime,
			@OutDate DateTime,	
			@ID_Reader BigInt,
			@TimeOut Varchar(10),
			@TotalSecs BigInt

SET @InDate = GETDATE();
	SET @OutDate = @InDate

	SET @ID_Reader = (SELECT TOp 1 ID_Reader FROM Asset_Movement WHERE ID_AssetMovement = @ID_AssetMovement)

	SELECT @InDate = [TimeStamp],@OutDate = ISNULL([TimeStampOut],[TimeStamp]) FROM Asset_Movement
	WHERE ID_Asset = @ID_Asset AND JobID = @JobID AND ID_Reason = @ID_AssetMovement --ID_AssetMovement > @ID_AssetMovement 
	AND ID_LocationNew = (SELECT TOP 1 ID_LocationOut FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND ID_LocationIn = @ID_LocationIn AND id_locationout Not In (Select id_locationin from AntennaSettings))
	
IF @option = 0
BEGIN
	DECLARE	@hours int,
			@mins int 
    
	--SET @TimeOut = Cast(DateDiff(hh,@Indate,@OutDate) as Varchar(2)) + ':' + Cast(DateDiff(mi,@Indate,@OutDate) as varchar(2)) + ':' + Cast(DateDiff(ss,@Indate,@OutDate) as varchar(2))
	
	--@TotalSecs

	IF (@InDate IS NOT NULL) AND (@InDate < @OutDate)
		BEGIN
			SET  @TotalSecs = DateDiff(ss,@InDate,@OutDate) 

			SET @hours = @TotalSecs/3600

			SET @TotalSecs = (@TotalSecs%3600)

			SET @mins = @TotalSecs/60

			SET @TotalSecs = @TotalSecs%60
		
			SET @TimeOut = CAST(@hours AS VARCHAR) + ':' + RIGHT('00' + CAST(@mins AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(@TotalSecs AS VARCHAR(2)),2) 
			
		END
	ELSE
		SET @TimeOut = '0:00:00';	
END
ELSE
BEGIN

		Declare @TSecs float
		IF (@InDate IS NOT NULL) AND (@InDate < @OutDate)
			BEGIN
				SET  @TotalSecs = DateDiff(ss,@InDate,@OutDate) 
				
				set @TSecs = @TotalSecs / (24 * 3600)
				
				SET @TimeOut = CAST(@TotalSecs AS VARCHAR)
			END
		ELSE
			SET @TimeOut = '0';	

END

	RETURN @TimeOut;
	
END

---------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[GetInTransit_TimeStamp]    Script Date: 06/24/2013 15:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetInTransit_TimeStamp] 
(	-- Add the parameters for the function here
	@ID_AssetMovement BIGINT,
	@ID_Asset BIGINT,
	@JobID varchar(50),
	@ID_LocationIn Bigint,
	@In_Out int
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Date DateTime, 
			@ID_Reader BigInt
 
	SET @Date = null;

	SET @ID_Reader = (SELECT TOp 1 ID_Reader FROM Asset_Movement WHERE ID_AssetMovement = @ID_AssetMovement)

	IF @In_Out = 0
	BEGIN	
		SELECT @Date = [TimeStamp] FROM Asset_Movement
		WHERE ID_Asset = @ID_Asset AND JobID = @JobID AND ID_AssetMovement > @ID_AssetMovement AND
		ID_LocationNew = (SELECT TOP 1 ID_LocationOut FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND ID_LocationIn = @ID_LocationIn)
    END
	ELSE
	BEGIN	
		SELECT @Date  = [TimeStampOut] FROM Asset_Movement
		WHERE ID_Asset = @ID_Asset AND JobID = @JobID AND ID_AssetMovement > @ID_AssetMovement AND
		ID_LocationNew = (SELECT TOP 1 ID_LocationOut FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND ID_LocationIn = @ID_LocationIn)
    END	 
	
	RETURN @Date;
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInBtwn_Station]    Script Date: 06/24/2013 15:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetTimeInBtwn_Station] 
(	-- Add the parameters for the function here
	@ID_LocationIn BIGINT,
	@JobId varchar(20),
	@FromDate DateTime,
	@ToDate Datetime,
	@ID BIGINT,
	@Sum int = 1
)
RETURNS BigInt
AS
BEGIN
	-- Declare the return variable here
	DECLARE  @TotalSecs BigInt,
			 @ID_LocationOut BIGINT

	SET @TotalSecs = 0
	
	SET @ID_LocationOut = (SELECT Top 1 ID_LocationOut FROM AntennaSettings Where ID_LocationIn = @ID_LocationIn)	
	
IF @ID_LocationOut <> @ID_LocationIn AND @ID_LocationOut IS NOT NULL 		
BEGIN 
	IF(@Sum <> 0)				
		BEGIN  
			select @TotalSecs = ISNULL(SUM(DATEDIFF(ss,[TimeStamp],[TimeStampOut])),0) from Asset_Movement 
			WHERE JobID = @JobId AND id_locationnew = @ID_LocationOut
			AND Convert(varchar(10),Asset_movement.Date_Modified,101) between Convert(varchar(10),@FromDate,101) AND Convert(varchar(10),@ToDate,101)	
		END
	ELSE
		BEGIN  
			SET @TotalSecs = (select Top 1 ISNULL(DATEDIFF(ss,[TimeStamp],[TimeStampOut]),0) from Asset_Movement 
							  WHERE JobID = @JobId  AND ID_Reason = @ID AND ID_LocationNew = @ID_LocationOut)
			--AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate	
		END
END

RETURN @TotalSecs;
	
END

-------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  UserDefinedFunction [dbo].[GetAvgInTime]    Script Date: 06/24/2013 15:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetAvgInTime] 
(	-- Add the parameters for the function here
	@ID Varchar(15),
	@FromDate varchar(10),
	@ToDate varchar(10),
	@OtherId BigInt,
	@Option int,
	@In_Out int
)
RETURNS VarChar(10)
AS
BEGIN

Declare @Time Varchar(10),
		@hours int,
		@mins int,
		@sec bigint	

SET @sec = 0

IF @Option = 0
BEGIN
	IF @In_Out = 0
		BEGIN
			SET @Time = (Select distinct dbo.GetTimeFromSecs(SUM(DateDiff(ss,[TimeStamp],timestampout))/Count(JobID))
			FROM Asset_movement
			WHERE jobid = @ID AND Id_Asset = @OtherId AND
			id_locationnew in (select id_locationin from AntennaSettings) 
			--AND Cast(Convert(varchar(10),Asset_movement.Date_Modified,101) as datetime) >= @FromDate AND Cast(Convert(varchar(10),Asset_movement.Date_Modified,101) as datetime) <= @ToDate				 	
			AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate				 	
			group by jobid,Id_Asset)
	END
	ELSE	
		BEGIN
		 	SET @Time = (Select distinct dbo.GetTimeFromSecs(SUM(DateDiff(ss,[TimeStamp],timestampout))/Count(JobID))
			FROM Asset_movement
			WHERE jobid = @ID AND Id_Asset = @OtherId AND
			id_locationnew in (select id_locationout from AntennaSettings Where id_locationout Not In (Select id_locationin from AntennaSettings)) 
			--AND Cast(Convert(varchar(10),Asset_movement.Date_Modified,101) as datetime) >= @FromDate AND Cast(Convert(varchar(10),Asset_movement.Date_Modified,101) as datetime) <= @ToDate				 	
			AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate		
			group by jobid,Id_Asset)
		
		END	
END
ELSE
BEGIN
	IF @In_Out = 0
		BEGIN
			SET @Time = (Select distinct dbo.GetTimeFromSecs(SUM(DateDiff(ss,[TimeStamp],timestampout))/Count(id_locationnew))
			FROM Asset_movement
			WHERE id_locationnew = @ID 		 
			AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate				 	
			group by id_locationnew)
	END
	ELSE	
		BEGIN
			DECLARE @LocOut Bigint
			if(@OtherId <> 9999)
				SET @LocOut = (Select Top 1 Id_LocationOut FROM AntennaSettings where ID_LocationIn = @ID AND id_locationout Not In (Select id_locationin from AntennaSettings) AND ID_Reader = @OtherId)
			else
				SET @LocOut = (Select Top 1 Id_Location FROM Locations where [Description] LIKE '%Assign_InTransit%')
				
			SET @Time = (Select distinct dbo.GetTimeFromSecs(SUM(DateDiff(ss,[TimeStamp],timestampout))/Count(id_locationnew))
			FROM Asset_movement
			WHERE id_locationnew = @LocOut 				 
			AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate		
			group by id_locationnew)
		END	
END

IF @Time is null
	SET @Time = '000:00:00';
 
RETURN @Time;

	
END
GO
/****** Object:  StoredProcedure [dbo].[Get_AntennaPowerSettings]    Script Date: 06/24/2013 15:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
 
-- Create date: 15 Jan 2010
-- Description:	To get antenna power settings 
-- =============================================

CREATE PROCEDURE [dbo].[Get_AntennaPowerSettings]
@ID_Reader bigint = 0,
@Option int = 0
AS
SET NOCOUNT ON;
IF @Option = 1 --AND @ID_Reader <> 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,TransmitPower,IsActive,AntennaSettings.ID_Reader	
	FROM      AntennaSettings  	 
	WHERE	  AntennaSettings.ID_Reader = @ID_Reader --AND AntennaSettings.IsActive=1
END 
ELSE IF @Option = 0
BEGIN
		SELECT    AntennaSettings.AntennaNo as AntennaNo,TransmitPower,IsActive,AntennaSettings.ID_Reader	
		FROM      AntennaSettings  
END 

-- exec [Get_AntennaSettings] 0,3
GO
/****** Object:  StoredProcedure [dbo].[Get_ReaderAntennaSettings]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/*Created On : - 18 Feb 2010 */
/*Created By : - Vijay Mangtani */
/*Description : - To get settings for readers and corresponding antennas*/
/* exec [Get_ReaderAntennaSettings] */

CREATE PROCEDURE [dbo].[Get_ReaderAntennaSettings]
(
	@option int = 0
)
AS
BEGIN

Declare @AssetTagTypeID BigInt,
		@EmpTagTypeID BigInt

SET @AssetTagTypeID = (SELECT ID_TagType FROM Tag_Types WHERE TagType = 'Asset')
SET @AssetTagTypeID = (SELECT ID_TagType FROM Tag_Types WHERE TagType = 'Employee') 

IF @AssetTagTypeID IS NULL OR @AssetTagTypeID = 0
	SET @AssetTagTypeID = 1

IF @EmpTagTypeID IS NULL OR @EmpTagTypeID = 0
	SET @EmpTagTypeID = 2

Select distinct 'Antenna1' as AntennaNo,'Antenna1' as AntennaName,LocIn.ID_Location as ID_LocationIn,
		LocOut.ID_Location as ID_LocationOut,RDR.TxPower as TransmitPower,		
		isnull(RRAsset.RSSI_Min,0) as AssetRSSIMin,isnull(RRAsset.RSSI_Max,0) as AssetRSSIMax,
		isnull(RREmp.RSSI_Min,0) as EmpRSSIMin,isnull(RREmp.RSSI_Max,0) as EmpRSSIMax,
		HTTP_Connection as [TimeOut],
		(CASE WHEN (Ant1 = 0 AND Ant2 = 0 AND Ant3 = 0 AND Ant4 = 0) THEN 0 ELSE 1 end) as IsActive,0 as ID_AntennaSetting,
		isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,RDR.ID_Reader
		from Readers RDR
		LEFT OUTER JOIN RSSI_Range RRAsset
		ON RDR.ID_Reader = RRAsset.ID_Reader
		LEFT OUTER JOIN RSSI_Range RREmp
		ON RDR.ID_Reader = RREmp.ID_Reader
		LEFT OUTER JOIN Locations LocIn
		ON RDR.ID_Location = LocIn.Id_Location
		LEFT OUTER JOIN Locations LocOut
		ON RDR.ID_LocationOut = LocOut.Id_Location
		where RRAsset.AntennaNo = '-1' AND RRAsset.ID_TagType = @AssetTagTypeID
		AND RREmp.ID_TagType = @EmpTagTypeID
		AND RDR.ID_Location <> -1 AND RDR.ID_LocationOut <> -1
		AND RDR.Is_Active = 1

UNION ALL

	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 

END

---------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_All_AntennaSettings]    Script Date: 06/24/2013 15:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Bimlesh Thakur
-- Create date: 15 March, 2011
-- Description:	To get antenna settings of all readers
-- =============================================
CREATE PROCEDURE [dbo].[Get_All_AntennaSettings] 
	-- Add the parameters for the stored procedure here
	@ID_Reader bigint = 0,
	@Option int = 0
AS
SET NOCOUNT ON;
IF @Option = 1 --AND @ID_Reader <> 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,InventoryRounds,AntennaSenseThreshold,AntennaSenseValue,
		  IsLocalInventory,InventoryAlgorithm,StartQ,IsLocalLinkProfile,Profile,IsLocalFrequency,Frequency,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader,AntennaName as [AntennaName]
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location
	WHERE	  AntennaSettings.ID_Reader = @ID_Reader --AND AntennaSettings.IsActive=1
END
ELSE IF @Option = 3 --AND @ID_Reader <> 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,InventoryRounds,AntennaSenseThreshold,AntennaSenseValue,
		  IsLocalInventory,InventoryAlgorithm,StartQ,IsLocalLinkProfile,Profile,IsLocalFrequency,Frequency,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	WHERE AntennaSettings.ID_Reader = @ID_Reader AND AntennaSettings.IsActive=1
END
ELSE IF @Option = 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,InventoryRounds,AntennaSenseThreshold,AntennaSenseValue,
		  IsLocalInventory,InventoryAlgorithm,StartQ,IsLocalLinkProfile,Profile,IsLocalFrequency,Frequency,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	--WHERE AntennaSettings.IsActive=1
END
ELSE IF @Option = 2
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,InventoryRounds,AntennaSenseThreshold,AntennaSenseValue,
		  IsLocalInventory,InventoryAlgorithm,StartQ,IsLocalLinkProfile,Profile,IsLocalFrequency,Frequency,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	WHERE AntennaSettings.IsActive=1
END
GO
/****** Object:  StoredProcedure [dbo].[Get_AntennaSettings]    Script Date: 06/24/2013 15:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
 
-- Create date: 28 Dec 2009
-- Description:	To get antenna settings of the reader
-- =============================================

CREATE PROCEDURE [dbo].[Get_AntennaSettings]
@ID_Reader bigint = 0,
@Option int = 0
AS
SET NOCOUNT ON;
IF @Option = 1 --AND @ID_Reader <> 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader,AntennaName as [AntennaName]
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location
	WHERE	  AntennaSettings.ID_Reader = @ID_Reader --AND AntennaSettings.IsActive=1
END
ELSE IF @Option = 3 --AND @ID_Reader <> 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	WHERE AntennaSettings.ID_Reader = @ID_Reader AND AntennaSettings.IsActive=1
END
ELSE IF @Option = 0
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	--WHERE AntennaSettings.IsActive=1
END
ELSE IF @Option = 2
BEGIN
	SELECT    AntennaSettings.AntennaNo as AntennaNo,AntennaName as [AntennaName],ID_LocationIn,ID_LocationOut,
		  TransmitPower,
		  isnull(RRAsset.RSSI_Min,0) as [AssetRSSIMin],isnull(RRAsset.RSSI_Max,0) as [AssetRSSIMax],
		  isnull(RREmp.RSSI_Min,0) as [EmpRSSIMin],isnull(RREmp.RSSI_Max,0) as [EmpRSSIMax],
		  [TimeOut],IsActive,ID_AntennaSetting,isnull(LocIn.Name,'N/A') as LocationIn,isnull(LocOut.Name,'N/A') as LocationOut,AntennaSettings.ID_Reader
	FROM      AntennaSettings  
	LEFT OUTER JOIN RSSI_Range RRAsset
	ON ID_AssetRSSIRange = RRAsset.ID_RSSIRange
	LEFT OUTER JOIN RSSI_Range RREmp
	ON ID_EmpRSSIRange = RREmp.ID_RSSIRange	
	LEFT OUTER JOIN Locations LocIn
	ON ID_LocationIn = LocIn.ID_Location
	LEFT OUTER JOIN Locations LocOut
	ON ID_LocationOut = LocOut.ID_Location 
	WHERE AntennaSettings.IsActive=1
END


-- exec [Get_AntennaSettings] 0,3
GO
/****** Object:  StoredProcedure [dbo].[Add_All_AntennaSettings]    Script Date: 06/24/2013 15:03:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Bimlesh Thakur>
-- Create date: <15 March, 2011>
-- Description:	<Add Antenna Settings for all Readers>
-- Modified By : Vijay (28-11-2011)
-- =============================================
CREATE PROCEDURE [dbo].[Add_All_AntennaSettings]
	-- Add the parameters for the stored procedure here
@ID_AntennaSetting int,
@ID_Reader bigint,
@AntennaNo varchar(50),
@AntennaName varchar(50),
@ID_LocationIn bigint,
@ID_LocationOut bigint,
@TransmitPower float = 0,
@TimeOut int =0,
@AssetRSSIMax float =0,
@AssetRSSIMin float =0,
@EmpRSSIMax float =0,
@EmpRSSIMin float =0,
@Last_Modified_By int = 0,
@IsActive bit = 1,
@ChangeLocOn varchar(5) = 'TR',

@InventoryRounds int=0,
@AntennaSenseThreshold bigint=0,
@AntennaSenseValue int=0,
@IsLocalInventory bit=1,
@InventoryAlgorithm varchar(50)= 'FIXEDQ',
@StartQ int=0,
@IsLocalLinkProfile bit=1,
@Profile int=0,
@IsLocalFrequency bit=1,
@Frequency int=0

AS
SET NOCOUNT ON;
BEGIN

DECLARE @ID_AssetRSSI int,
		@ID_EmpRSSI int,
		@ID_EmpTagType int,
		@ID_AssetTagType int,
		@ErrorMsg varchar(200)

SET @ID_AssetTagType = 1
SET @ID_EmpTagType = 2

IF EXISTS(SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Asset%')
		SET @ID_AssetTagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Asset%')

IF EXISTS(SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Employee%')
		SET @ID_EmpTagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType like '%Employee%')

BEGIN TRY 
BEGIN TRAN T1

	IF EXISTS(SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType)
		BEGIN
			SET @ID_AssetRSSI  = (SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType)
			-- update
			Update RSSI_Range Set RSSI_Min = @AssetRSSIMin,RSSI_Max = @AssetRSSIMax,Last_Modified_By = @Last_Modified_By
			WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_AssetTagType
		END
	ELSE
		BEGIN
			-- Insert
			Insert into RSSI_Range(ID_TagType,ID_Reader,AntennaNo,RSSI_Min,RSSI_Max,Is_Active,Last_Modified_By)
						VALUES(@ID_AssetTagType,@ID_Reader,@AntennaNo,@AssetRSSIMin,@AssetRSSIMax,1,@Last_Modified_By)
			SET @ID_AssetRSSI = @@Identity
		END

	IF EXISTS(SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType)
		BEGIN
			SET @ID_EmpRSSI  = (SELECT ID_RSSIRange FROM RSSI_Range WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType)
			-- update
			Update RSSI_Range Set RSSI_Min = @EmpRSSIMin,RSSI_Max = @EmpRSSIMax,Last_Modified_By = @Last_Modified_By
			WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo AND ID_TagType = @ID_EmpTagType
		END
	ELSE
		BEGIN
			-- Insert
			Insert into RSSI_Range(ID_TagType,ID_Reader,AntennaNo,RSSI_Min,RSSI_Max,Is_Active,Last_Modified_By)
						VALUES(@ID_EmpTagType,@ID_Reader,@AntennaNo,@EmpRSSIMin,@EmpRSSIMax,1,@Last_Modified_By)
			SET @ID_EmpRSSI = @@Identity
		END

------------------------------------------------------------------------------------------------------------------------------------------------------

	 IF @ID_AntennaSetting = 0 AND Not Exists(Select ID_AntennaSetting FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND AntennaNo = @AntennaNo)
		BEGIN
			INSERT INTO AntennaSettings(ID_Reader,AntennaNo,AntennaName,ID_LocationIn,ID_LocationOut,TransmitPower,
										ID_AssetRSSIRange,ID_EmpRSSIRange,[TimeOut],IsActive,DateCreated,InventoryRounds,AntennaSenseThreshold,
										AntennaSenseValue,IsLocalInventory,InventoryAlgorithm,StartQ,IsLocalLinkProfile,Profile,IsLocalFrequency,Frequency)
						VALUES(@ID_Reader,@AntennaNo,@AntennaName,@ID_LocationIn,@ID_LocationOut,@TransmitPower,
										@ID_AssetRSSI,@ID_EmpRSSI,@TimeOut,@IsActive,getDate(),@InventoryRounds,@AntennaSenseThreshold,
										@AntennaSenseValue,@IsLocalInventory,@InventoryAlgorithm,@StartQ,@IsLocalLinkProfile,@Profile,@IsLocalFrequency,@Frequency)			
			
			SET @ID_AntennaSetting = @@Identity
		END
   	 ELSE
		BEGIN
			UPDATE AntennaSettings SET AntennaName = @AntennaName,ID_LocationIn = @ID_LocationIn,ID_LocationOut = @ID_LocationOut,AntennaNo = @AntennaNo,
								       TransmitPower = @TransmitPower,ID_AssetRSSIRange = @ID_AssetRSSI,ID_EmpRSSIRange = @ID_EmpRSSI,
									   [TimeOut] = @TimeOut,IsActive = @IsActive,InventoryRounds=@InventoryRounds,AntennaSenseThreshold=@AntennaSenseThreshold,
										AntennaSenseValue=@AntennaSenseValue,IsLocalInventory=@IsLocalInventory,InventoryAlgorithm=@InventoryAlgorithm,
										StartQ=@StartQ,IsLocalLinkProfile=@IsLocalLinkProfile,Profile=@Profile,IsLocalFrequency=@IsLocalFrequency,Frequency=@Frequency
			WHERE ID_Reader = @ID_Reader  AND ID_AntennaSetting = @ID_AntennaSetting
			--WHERE ID_AntennaSetting = @ID_AntennaSetting AND AntennaNo = @AntennaNo
		END	
		
		IF @AntennaNo = 'Antenna1'
			UPDATE Readers SET Ant1 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna2'
			UPDATE Readers SET Ant2 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna3'
			UPDATE Readers SET Ant3 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader
		IF @AntennaNo = 'Antenna4'
			UPDATE Readers SET Ant4 = @IsActive,ChangeLocationOn = @ChangeLocOn WHERE ID_Reader = @ID_Reader

		

		SELECT @ID_AntennaSetting

COMMIT TRAN T1
END TRY

BEGIN CATCH
ROLLBACK TRAN T1
 SET @ErrorMsg = ERROR_MESSAGE();
SELECT @ErrorMsg
--RAISERROR(
--@ErrorMsg,2,1 
--); 
END CATCH
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Asset]    Script Date: 06/24/2013 15:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified:	Deepanshu Jouhari
-- Modified By : Vinay Bansal ON 12-June- 2009	
-- Create date: 19th March 2008
-- Description:	To ADD a Asset
-- =============================================

CREATE PROCEDURE [dbo].[Add_Asset]
(
	@TagID varchar(50),
	@Name varchar(50),
	@AssetNo varchar(50),
	@Description varchar(1000),
	@ImageName varchar(500) ,
	@PurchaseDate datetime,
	@ID_TagType bigint,
	@ID_Location bigint,
	@ID_AssetType bigint,
	@ID_AssetStatus bigint,
	@ID_AssetGroup bigint,
	@ID_AssetComment bigint,
	@Last_Modified_By bigint,
	@ID_Reader bigint = 0,
	@ReferenceNo nvarchar(150)='',
	@Allocation nvarchar(100)='', -- Added By Vinay
	@LotNo	nvarchar(50)='', -- Added By Vinay
	@ExpiryDate datetime ='1900-01-01', -- Added By Vinay
	@ID_AssetMaster bigint = 0, -- Added By Vinay
	@PartCode nvarchar(10) = '0'  --Added By Deep
)
AS
	SET NOCOUNT OFF;

if(@ID_AssetMaster =-1) --Vernon Call
BEGIN
	IF NOT Exists (Select 1 From Master_Assets where [Description] = @Name) --1 or NULL
	BEGIN
		INSERT INTO Master_Assets (SNo,[Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
		Values (@Name,@Name,1,0,@Last_Modified_By, Getdate())
	END	
		Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = @Name
END

if(@ID_AssetMaster =0)
BEGIN
	IF NOT Exists (Select 1 From Master_Assets where [Description] = 'Default')
	BEGIN
		INSERT INTO Master_Assets (SNo,[Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
		Values ('Default','Default',1,0,@Last_Modified_By, Getdate())
	END	
		Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = 'Default'
END



IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
	Begin 	
		set dateformat dmy   --Added by Asif Ali on 24th March 2008.
		INSERT INTO [Assets] (PartCode,[TagID], [Name], [AssetNo], [Description], [ImageName], [PurchaseDate], [ID_TagType], [ID_Location], [ID_AssetType], [ID_AssetStatus], [ID_AssetGroup], [ID_AssetComment], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By], [ID_Employee],[ID_Reader],[ReferenceNo],LotNo,ExpiryDate,Allocation,ID_AssetMaster) 
			VALUES (@PartCode, @TagID, @Name, @AssetNo, @Description, @ImageName, @PurchaseDate, @ID_TagType, @ID_Location, @ID_AssetType, @ID_AssetStatus, @ID_AssetGroup, @ID_AssetComment, getdate(), getdate(), 0, 1, @Last_Modified_By, @Last_Modified_By,@ID_Reader,@ReferenceNo,@LotNo,@ExpiryDate,@Allocation,@ID_AssetMaster);
	
		SELECT ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_TagType, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ID_Employee ,ID_Reader,ReferenceNo,LotNo,Isnull(ExpiryDate,'') AS ExpiryDate,Allocation,PartCode,ID_AssetMaster FROM Assets WHERE (ID_Asset = SCOPE_IDENTITY())
	End
else
	Begin
		RaisError(N'Duplicate TAG ID',18,1);
	end

--Select * from Assets
--update Assets set ID_AssetMaster=23129 where ID_AssetMaster=23128
--select * from Master_Assets
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetFromStandardCSVFile]    Script Date: 06/24/2013 15:03:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 27 Oct 2009
-- Description:	To ADD a Asset From File
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetFromStandardCSVFile]
(
	@TagID varchar(50),
	@Name varchar(50),
	@AssetNo varchar(50),
	@Description varchar(500),
	@Location nvarchar(50),	
	@AssetGroup varchar(200),
	@Allocation nvarchar(100),
	@LotNo	nvarchar(50),
	@ExpiryDate datetime = null,
	@PurchaseDate datetime = null,
	@TagType varchar(15),	 
	@AssetType varchar(15),
	@AssetStatus varchar(15),	 
	@AssetComment varchar(50),	
	@ImageName varchar(200) = '',
	@Last_Modified_By bigint,
	@ReferenceNo nvarchar(150)='',
	@PartCode nvarchar(20)=''
	
)
AS
	SET NOCOUNT OFF;
	Declare @ID_AssetMaster Bigint,
				@ID_TagType bigint,
				@ID_Location bigint,
				@ID_AssetType bigint,
				@ID_AssetStatus bigint,
				@ID_AssetGroup bigint,
				@ID_AssetComment bigint	
    
    
IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID AND @TagID <> '') 
BEGIN 	
	SET DATEFORMAT dmy 
	IF NOT EXISTS (Select 1 FROM Locations WHERE [Name] = @Location)		
	BEGIN		 
		SELECT -2;
		RETURN;
	END
	ELSE
	BEGIN
		SET @ID_Location = (SELECT Top 1 ID_Location FROM Locations WHERE [Name] = @Location)
	END

	IF NOT EXISTS (Select 1 FROM Asset_Groups WHERE [Name] = @AssetGroup)		
	BEGIN		 
		SELECT -3;
		RETURN;
	END
	ELSE
	BEGIN
		SET @ID_AssetGroup = (SELECT Top 1 ID_AssetGroup FROM Asset_Groups WHERE [Name] = @AssetGroup)
	END

	IF @TagType = ''
		SET @ID_TagType = (Select Top 1 ID_TagType FROM Tag_Types WHERE TagType LIKE '%Asset%')
	ELSE IF EXISTS (Select 1 FROM Tag_Types WHERE TagType LIKE '%' + @TagType + '%')
		SET @ID_TagType = (Select Top 1 ID_TagType FROM Tag_Types WHERE TagType LIKE '%' + @TagType + '%')		
	ELSE
		SET @ID_TagType = 1		
	
	IF @AssetType = ''
		SET @ID_AssetType = (Select Top 1 ID_AssetType FROM Asset_Types WHERE [Type] LIKE '%Inventory%')
	ELSE IF EXISTS (Select 1 FROM Asset_Types WHERE [Type] LIKE '%' + @AssetType + '%')
		SET @ID_AssetType = (Select Top 1 ID_AssetType FROM Asset_Types WHERE [Type] LIKE '%' + @AssetType + '%')		
	ELSE
		SET @ID_AssetType = 2	

	IF @AssetStatus = ''
		SET @ID_AssetStatus = (Select ID_AssetStatus FROM Asset_Status WHERE [Status] LIKE '%Unknown%')
	ELSE IF EXISTS (Select 1 FROM Asset_Status WHERE [Status] LIKE '%' + @AssetStatus + '%')
		SET @ID_AssetStatus = (Select ID_AssetStatus FROM Asset_Status WHERE [Status] LIKE '%' + @AssetStatus + '%')		
	ELSE
		SET @ID_AssetStatus = 4	

	IF @AssetComment = ''
		SET @ID_AssetComment = 1
	ELSE IF EXISTS (Select 1 FROM Asset_Comments WHERE [Comment] LIKE '%' + @AssetComment + '%')
		SET @ID_AssetComment = (Select ID_AssetComment FROM Asset_Comments WHERE [Comment] LIKE '%' + @AssetComment + '%')		
	ELSE
		SET @ID_AssetComment = 1		

	IF NOT EXISTS (Select 1 FROM Master_Assets WHERE [Description] = @Name)		
	BEGIN
		INSERT INTO Master_Assets ([Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
		VALUES (@Name,1,0,@Last_Modified_By, Getdate() )
		SET @ID_AssetMaster = SCOPE_IDENTITY()
	END
	ELSE
	BEGIN
		SET @ID_AssetMaster = (SELECT Top 1 ID_AssetMaster FROM Master_Assets WHERE [Description] = @Name)
	END
	
	BEGIN TRY		
			INSERT INTO [Assets] ([TagID], [Name], [AssetNo], [Description], [ImageName], [PurchaseDate], [ID_TagType], [ID_Location], [ID_AssetType], [ID_AssetStatus], [ID_AssetGroup], [ID_AssetComment], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By], [ID_Employee],[ID_Reader],[ReferenceNo],LotNo,ExpiryDate,Allocation, ID_AssetMaster,PartCode) 
			VALUES (@TagID, @Name, @AssetNo,@Description,@ImageName, @PurchaseDate,@ID_TagType,@ID_Location,@ID_AssetType,@ID_AssetStatus,@ID_AssetGroup,@ID_AssetComment, getdate(), getdate(), 0, 1, @Last_Modified_By, @Last_Modified_By,0,@ReferenceNo,@LotNo,@ExpiryDate,@Allocation,@ID_AssetMaster,@PartCode) ;
			SELECT @@Identity;
	END TRY	
	BEGIN CATCH		 
			SELECT -4;
	END CATCH		 
	
END
ELSE
	BEGIN
		SELECT -1;
	END


-- ----------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetFromFile]    Script Date: 06/24/2013 15:03:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 11-June-2009
-- Description:	To ADD a Asset From File
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetFromFile]
(
	@TagID varchar(50),
	@Name varchar(50),
	@AssetNo varchar(50),
	@ID_Location bigint,
	@Allocation nvarchar(100),
	@LotNo	nvarchar(50),
	@ReferenceNo nvarchar(150)='',
	@ExpiryDate datetime,
	@Location nvarchar(50),	
	@Last_Modified_By bigint
	
)
AS
	SET NOCOUNT OFF;
	Declare @ID_AssetMaster Bigint
	IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
	Begin 	
		set dateformat dmy   
		IF NOT EXISTS (Select 1 FROM Locations WHERE [Name] = @Location)		
		BEGIN
			INSERT INTO [Locations] ([TagID], [Name], [LocationNo], [Description], [ID_LocationGroup], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) 
			VALUES ('', @Location, @Location, '', 1, getdate(), getdate(), 0,1, @Last_Modified_By);
			SELECT @ID_Location =  ID_Location FROM Locations WHERE (ID_Location = SCOPE_IDENTITY())
		END
		ELSE
		BEGIN
			SELECT @ID_Location =  ID_Location FROM Locations WHERE [Name] = @Location
		END

		IF NOT EXISTS (Select 1 FROM Master_Assets WHERE [Description] = @Name)		
		BEGIN
			INSERT INTO Master_Assets ([Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
			VALUES (@Name,1,0,@Last_Modified_By, Getdate() )
			SELECT @ID_AssetMaster =  ID_AssetMaster FROM Master_Assets WHERE (ID_AssetMaster = SCOPE_IDENTITY())
		END
				ELSE
		BEGIN
			SELECT @ID_AssetMaster =  ID_AssetMaster FROM Master_Assets WHERE [Description] = @Name
		END


		INSERT INTO [Assets] ([TagID], [Name], [AssetNo], [Description], [ImageName], [PurchaseDate], [ID_TagType], [ID_Location], [ID_AssetType], [ID_AssetStatus], [ID_AssetGroup], [ID_AssetComment], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By], [ID_Employee],[ID_Reader],[ReferenceNo],LotNo,ExpiryDate,Allocation, ID_AssetMaster) 
		VALUES (@TagID, @Name, @AssetNo, '','', GetDate(),1,@ID_Location,2,4,1,1, getdate(), getdate(), 0, 1, @Last_Modified_By, @Last_Modified_By,0,@ReferenceNo,@LotNo,@ExpiryDate,@Allocation,@ID_AssetMaster);
	End
else
	Begin
		RaisError(N'Duplicate TAG ID',18,1);
	end
GO
/****** Object:  StoredProcedure [dbo].[Add_Employee]    Script Date: 06/24/2013 15:03:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Employee
-- =============================================

CREATE PROCEDURE [dbo].[Add_Employee]
(
	@TagID varchar(50),
	@Name varchar(50),
	@Surname varchar(50),
	@EmpNo varchar(50),
	@Email varchar(50),
	@Address varchar(500),
	@City varchar(50),
	@ID_State bigint,
	@DOB datetime,
	@UserName varchar(50),
	@Password varchar(50),
	@ID_Department bigint,
	@ID_SecurityGroup bigint,
	@ID_TagType bigint,
	@ID_Skill bigint,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
	Begin 

	IF NOT EXISTS(Select UserName From Employees Where UserName = @UserName) 
	Begin 
		IF EXISTS(SELECT ID_TagType FROM Tag_Types WHERE TagType = 'Employee')
			set @ID_TagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType = 'Employee')	
		ELSE	
			set @ID_TagType = 2;
		
		set dateformat dmy  
		INSERT INTO [Employees] ([TagID], [Name], [Surname], [EmpNo], [Email], [Address], [City], [ID_State], [DOB], [UserName], [Password], [ID_Department], [ID_SecurityGroup], [ID_TagType], [ID_Skill], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@TagID, @Name, @Surname, @EmpNo, @Email, @Address, @City, @ID_State, @DOB, @UserName, @Password, @ID_Department, @ID_SecurityGroup, @ID_TagType, @ID_Skill, getdate(), getdate(), 0, 1, @Last_Modified_By);
	
		SELECT ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, ID_Department, ID_SecurityGroup, ID_TagType, ID_Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Employees WHERE (ID_Employee = SCOPE_IDENTITY())
	

	End

End
GO
/****** Object:  UserDefinedFunction [dbo].[GetLastEmployee]    Script Date: 06/24/2013 15:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetLastEmployee] 
(	-- Add the parameters for the function here
	@ID_AssetMovement BIGINT 
)
RETURNS Varchar(50)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @EmpName VarChar(50)

	BEGIN	
		SELECT @EmpName = Employees.Name FROM Asset_Movement
		LEFT OUTER JOIN Employees 
		ON Asset_Movement.ID_Employee = Employees.ID_Employee
		WHERE ID_AssetMovement = @ID_AssetMovement 
    END
	
	RETURN @EmpName;
	
END
GO
/****** Object:  StoredProcedure [dbo].[CheckOldPassword]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE Procedure [dbo].[CheckOldPassword]
(
@ID_Employee BigInt,
@Password varchar(50)
)
AS
SELECT [ID_Employee],
       [Password]
FROM  [Employees] where ID_Employee = @ID_Employee and Password = @Password
GO
/****** Object:  StoredProcedure [dbo].[Update_Employee_LastModified]    Script Date: 06/24/2013 15:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Employee
-- =============================================

Create  PROCEDURE [dbo].[Update_Employee_LastModified]
(
	@ID_Employee bigint,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

UPDATE [Employees] SET Last_Modified_By=@Last_Modified_By
WHERE (([ID_Employee] = @ID_Employee))
GO
/****** Object:  StoredProcedure [dbo].[Check_DuplicateUserName_forUPDATE]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 25th March 2008
-- Description:	To CHECK for Duplicate UserName
-- =============================================

CREATE PROCEDURE [dbo].[Check_DuplicateUserName_forUPDATE]  --'Tag-0011187',1,'Location'
	-- Add the parameters for the stored procedure here
	@UserName varchar(50),
	@ID_Employee bigint
	

	
AS
BEGIN
	if EXISTS(select @UserName From Employees where UserName = @UserName AND ID_Employee<>@ID_Employee) 
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Employees]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ======================================================
-- Author:		Asif Ali
-- Create date: 3rd May 2008
-- Description:	To GET all Employees for Drop Down List
-- ======================================================

CREATE PROCEDURE [dbo].[Get_Employees]
AS
	SET NOCOUNT ON;


	SELECT  
		-1 AS ID_Employee, 
		'--- Select --- ' AS UserName, 
		getdate() AS Date_Created, 
		getdate() AS Date_Modified, 
		0 AS Is_Deleted, 
		1 AS Is_Active, 
		0 AS Last_Modified_By
	
	FROM    Employees

UNION

	SELECT
		ID_Employee,
		--Case Is_Deleted When 0 then UserName when 1 then UserName + ' (Deleted)'  else UserName end as UserName,
		UserName,
		Date_Created,
		Date_Modified,
		Is_Deleted,
		Is_Active,
		isnull(Last_Modified_By,0) as Last_Modified_By

	FROM Employees order by UserName

--	WHERE  Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Login]    Script Date: 06/24/2013 15:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali,Deepanshu Jouhari
-- Create date: 21 March 2008
-- Description:	To Login a member.
-- =============================================
CREATE PROCEDURE [dbo].[Login] --'deepesh','deepesh'
	-- Add the parameters for the stored procedure here
	@UserName Varchar(50),
	@Password Varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;
 -- Insert Dispatch location here
--	0000000000000000000000D
	if NOT EXISTS(select [Name] From locations 
		where tagid='0000000000000000000000D')
	begin
		insert into locations (TagID,LocationNo,Name,ID_LocationGroup) values('0000000000000000000000D','D001','Dispatched',1)  
	end

 -- Insert statements for procedure here
	declare @Name varchar(50)
	if EXISTS(select @Name From Master_SecurityGroups 
				where ID_SecurityGroup in(Select ID_SecurityGroup from Employees 
					where ID_Employee in(Select ID_Employee from Employees where UserName = @UserName and Password=@Password )))

		select Employees.ID_Employee, Employees.UserName,Employees.Name,Employees.Surname, Employees.ID_SecurityGroup, 
		Master_SecurityGroups.Name as 'SecurityGroup', isnull(Master_SecurityGroups.AllowedFunctions,'') as AllowedFunctions,
		isnull(Master_SecurityGroups.ReadEditAccess,1) as ReadEditAccess,isnull(Master_SecurityGroups.ReadAll,1) as ReadAll 
			From Employees INNER JOIN Master_SecurityGroups ON
				Employees.ID_SecurityGroup = Master_SecurityGroups.ID_SecurityGroup
			Where Employees.UserName = @UserName and Employees.Password=@Password

END


-----------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  View [dbo].[Vw_SearchEmployees]    Script Date: 06/24/2013 15:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SearchEmployees]
AS
SELECT     dbo.Employees.ID_Employee, dbo.Employees.TagID, dbo.Employees.Name, dbo.Employees.Surname, dbo.Employees.EmpNo, dbo.Employees.Email, 
                      dbo.Employees.Address, dbo.Employees.City, dbo.Employees.ID_State, dbo.Employees.DOB, dbo.Employees.UserName, dbo.Employees.Password, 
                      dbo.Employees.ID_Department, dbo.Employees.ID_SecurityGroup, dbo.Employees.ID_Skill, dbo.Master_Departments.ID_Department AS DepartmentID,
                       dbo.Master_Departments.Name AS DepartmentName, dbo.Master_States.ID_State AS StateID, dbo.Master_States.Name AS StateName, 
                      dbo.Master_Skills.ID_Skill AS SkillID, dbo.Master_Skills.Skill, dbo.Master_SecurityGroups.ID_SecurityGroup AS SecurityGroupID, 
                      dbo.Master_SecurityGroups.Name AS SecurityGroupName, CONVERT(varchar(20), dbo.Employees.Date_Created, 103) AS Date_Created, 
                      CONVERT(varchar(20), dbo.Employees.Date_Modified, 103) AS Date_Modified, dbo.Employees.Is_Deleted, dbo.Employees.Is_Active, 
                      dbo.Employees.Last_Modified_By
FROM         dbo.Employees INNER JOIN
                      dbo.Master_States ON dbo.Employees.ID_State = dbo.Master_States.ID_State INNER JOIN
                      dbo.Master_Departments ON dbo.Employees.ID_Department = dbo.Master_Departments.ID_Department INNER JOIN
                      dbo.Master_SecurityGroups ON dbo.Employees.ID_SecurityGroup = dbo.Master_SecurityGroups.ID_SecurityGroup INNER JOIN
                      dbo.Master_Skills ON dbo.Employees.ID_Skill = dbo.Master_Skills.ID_Skill
WHERE     (dbo.Employees.Is_Deleted = 0) AND (dbo.Employees.Is_Active = 1) AND (dbo.Master_Departments.Is_Deleted = 0) AND 
                      (dbo.Master_Departments.Is_Active = 1) AND (dbo.Master_States.Is_Deleted = 0) AND (dbo.Master_States.Is_Active = 1) AND 
                      (dbo.Master_Skills.Is_Deleted = 0) AND (dbo.Master_Skills.Is_Active = 1) AND (dbo.Master_SecurityGroups.Is_Deleted = 0) AND 
                      (dbo.Master_SecurityGroups.Is_Active = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[41] 4[12] 2[29] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Employees"
            Begin Extent = 
               Top = 8
               Left = 528
               Bottom = 275
               Right = 693
            End
            DisplayFlags = 280
            TopColumn = 6
         End
         Begin Table = "Master_States"
            Begin Extent = 
               Top = 6
               Left = 241
               Bottom = 114
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Master_Departments"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Master_SecurityGroups"
            Begin Extent = 
               Top = 114
               Left = 240
               Bottom = 222
               Right = 405
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Master_Skills"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchEmployees'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N' = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchEmployees'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchEmployees'
GO
/****** Object:  StoredProcedure [dbo].[ForgotPassword]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===========================================================
-- Author:		Asif Ali
-- Create date: 28 March 2008
-- Description:	To check a valid member for Forgot password.
-- ============================================================
CREATE PROCEDURE [dbo].[ForgotPassword] --'deepesh','deepesh'
	-- Add the parameters for the stored procedure here
	@UserName Varchar(50),
	@Email Varchar(50) 
AS
BEGIN
	SET NOCOUNT ON;


 -- Insert statements for procedure here
	declare @Name varchar(50)
	if EXISTS(select @Name From Master_SecurityGroups 
				where ID_SecurityGroup in(Select ID_SecurityGroup from Employees 
					where ID_Employee in(Select ID_Employee from Employees where UserName = @UserName and Email=@Email )))

--		select Employees.ID_Employee, Employees.Email, Employees.Password, Employees.UserName,Employees.Name,Employees.Surname, Employees.ID_SecurityGroup, Master_SecurityGroups.Name as 'SecurityGroup' 
--			From Employees INNER JOIN Master_SecurityGroups ON
--				Employees.ID_SecurityGroup = Master_SecurityGroups.ID_SecurityGroup
--			Where Employees.UserName = @UserName and Employees.Email=@Email



select Employees.ID_Employee, Employees.Email, Employees.Password, Employees.UserName,Employees.Name,Employees.Surname, Employees.ID_SecurityGroup	
		From Employees 
			Where Employees.UserName = @UserName and Employees.Email=@Email


		
--	else 
--		select 0 as ID_Employee;	










--	if EXISTS(select UserName From Employee Where UserName = @UserName and Password=@Password) 
--Begin 	
--	select ID_Employee, UserName, ID_SecurityGroup From Employee Where UserName = @UserName and Password=@Password;
--End
--	else 
--		select 0;	

END
GO
/****** Object:  StoredProcedure [dbo].[Update_Employee]    Script Date: 06/24/2013 15:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Last Modified By : Vijay on 26 May 2010
-- Description:	To UPDATE a Employee
-- =============================================


CREATE PROCEDURE [dbo].[Update_Employee]
(
	@TagID varchar(50),
	@Name varchar(50),
	@Surname varchar(50),
	@EmpNo varchar(50),
	@Email varchar(50),
	@Address varchar(500),
	@City varchar(50),
	@ID_State bigint,
	@DOB datetime,
--	@UserName varchar(50),
--	@Password varchar(50),
	@ID_Department bigint,
	@ID_SecurityGroup bigint,
	@ID_TagType bigint,
	@ID_Skill bigint,
	@Last_Modified_By bigint,
	@ID_Employee bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Employees] SET [TagID] = @TagID, [Name] = @Name, [Surname] = @Surname, [EmpNo] = @EmpNo, [Email] = @Email, [Address] = @Address, [City] = @City, [ID_State] = @ID_State, [DOB] = @DOB, [ID_Department] = @ID_Department, [ID_SecurityGroup] = @ID_SecurityGroup,  [ID_Skill] = @ID_Skill, [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By WHERE (([ID_Employee] = @ID_Employee)); -- [ID_TagType] = @ID_TagType,
	
SELECT ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, ID_Department, ID_SecurityGroup, ID_TagType, ID_Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Employees WHERE (ID_Employee = @ID_Employee)
GO
/****** Object:  StoredProcedure [dbo].[Get_Employees_All]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Employees
-- =============================================

CREATE PROCEDURE [dbo].[Get_Employees_All]
AS
	SET NOCOUNT ON;
SELECT     ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, ID_Department, ID_SecurityGroup, ID_Skill, 
                      Date_Created, Date_Modified, Is_Deleted, Is_Active, isnull(Last_Modified_By,0) as Last_Modified_By
FROM         Employees

WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  View [dbo].[Vw_Unique_TagID]    Script Date: 06/24/2013 15:04:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Unique_TagID]

AS


SELECT  ID_Asset as ID, 
		TagID, 
		'Asset' as [Type]
FROM    Assets WHERE TagID Is Not Null AND TagID <> ''

	UNION

SELECT  ID_Employee as ID, 
		TagID, 
		'Employee' as [Type]
FROM    Employees WHERE TagID Is Not Null AND TagID <> ''

	UNION

SELECT  ID_Location as ID, 
		TagID, 
		'Location' as [Type]
FROM    Locations WHERE TagID Is Not Null AND TagID <> ''
GO
/****** Object:  StoredProcedure [dbo].[Update_EmployeePassword]    Script Date: 06/24/2013 15:04:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Procedure [dbo].[Update_EmployeePassword]
(
@ID_Employee BigInt,
@Password varchar(50)
)
AS
Update [Employees] set Password = @Password  where ID_Employee = @ID_Employee
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_AlertOccured_ByID_Alert]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 02 August 2008
-- Description:	To GET alert by ID_Alert
-- =============================================
CREATE PROCEDURE [dbo].[MR_Get_AlertOccured_ByID_Alert] 
(
@ID_Alert bigint,
@CheckType bigint
)
AS
	SET NOCOUNT ON;

DECLARE @EMPID VARCHAR(24)

DECLARE @ID_READER BIGINT
DECLARE @ALERT_TYPE BIGINT
SET @ALERT_TYPE = 0


SELECT @ID_READER = ID_Reader FROM dbo.Alert WHERE ID_Alert = @ID_Alert
--FATCH EMP ID & CHECK FOR MULTPLE OR UNKNOWN
SELECT @EMPID = RTRIM(E.TagID) 
FROM dbo.Alert A 
INNER JOIN Employees  E 
ON E.ID_Employee = A.ID_Employee AND A.ID_Alert = @ID_Alert


IF(@EMPID  = '00000000000000000000000M')
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Multiple Employee')
IF(@EMPID  = '000000000000000000000000')
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Unkown Employee')
IF(@ALERT_TYPE  = 0)
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Asset Status')

 
---EMAIL
IF(@CheckType =1)
	SELECT Is_Email  FROM dbo.Alert_Detail WHERE ID_Reader = @ID_READER AND ID_AlertType = @ALERT_TYPE
---SMS
IF(@CheckType =2)
	SELECT Is_SMS  FROM dbo.Alert_Detail WHERE ID_Reader = @ID_READER AND ID_AlertType = @ALERT_TYPE
---SCREEN
IF(@CheckType =3)
	SELECT Is_Reader  FROM dbo.Alert_Detail WHERE ID_Reader = @ID_READER AND ID_AlertType = @ALERT_TYPE
---SERVER
IF(@CheckType =4)
	SELECT Is_Server  FROM dbo.Alert_Detail WHERE ID_Reader = @ID_READER AND ID_AlertType = @ALERT_TYPE
GO
/****** Object:  StoredProcedure [dbo].[Get_AlertDetail_By_IDAlert]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Get_AlertDetail_By_IDAlert]
(
@ID_Alert bigint
)
AS
	SET NOCOUNT ON;

DECLARE @EMPID VARCHAR(24)

DECLARE @ID_READER BIGINT
DECLARE @ALERT_TYPE BIGINT
SET @ALERT_TYPE = 0


SELECT @ID_READER = ID_Reader FROM dbo.Alert WHERE ID_Alert = @ID_Alert
--FATCH EMP ID & CHECK FOR MULTPLE OR UNKNOWN
SELECT @EMPID = RTRIM(E.TagID) 
FROM dbo.Alert A 
INNER JOIN Employees  E 
ON E.ID_Employee = A.ID_Employee AND A.ID_Alert = @ID_Alert


IF(@EMPID  = '00000000000000000000000M')
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Multiple Employee')
IF(@EMPID  = '000000000000000000000000')
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Unkown Employee')
IF(@ALERT_TYPE  = 0)
 SELECT @ALERT_TYPE =  ID_AlertType FROM dbo.Alert_Type WHERE RTRIM(Alert_Name) = RTRIM('Asset Status')

 
SELECT Alert_Detail.*  FROM dbo.Alert_Detail WHERE ID_Reader = @ID_READER AND ID_AlertType = @ALERT_TYPE
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_AssetMovement_ByReaderAndTagList]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 5 th May 2008
-- Description:	To GET AssetMovement affected by particular reader in given Date Range
-- exec Get_AssetMovement_ByReaderAndTagList '2007-01-01','2009-01-01', 1, '''00000000000000000000000M'',''000000000000000000000000'''
--exec Get_AssetMovement_ByReaderAndTagList @date1='2008-05-06 00:00:00:000',@date2='2009-05-07 00:00:00:000',@ID_Reader=-1
--Previous Formati 108
-- =============================================

CREATE PROCEDURE [dbo].[MR_Get_AssetMovement_ByReaderAndTagList]
@date1 Datetime,
@date2 Datetime,
@ID_Reader bigint
AS
(select A.tagID as ID,
CONVERT ( varchar, A.Date_Modified ,100) as 'Time', A.AssetNo,A.Name,
E.Name + ' '+E.Surname as Employee,L.Name as Location  ,
1 as ID_Type ,A.ID_Asset as ID_Key,Alt.ID_Alert
from Assets A 
Inner Join locations L on A.ID_Location=L.ID_Location
Inner Join Employees E on A.ID_Employee=E.ID_Employee
Inner Join Alert alt on alt.ID_Alert=A.ID_Alert and isNULL(alt.AcknowledgedBy,0)<=0
where A.Is_deleted=0 and A.Is_Active=1 
and A.Date_Modified between @date1 and @date2
and (A.ID_Reader = @ID_Reader  or @ID_Reader<=0) 
)
Union
(select A.tagid as ID,
CONVERT ( varchar, AM.Date_Modified ,100) as 'Time', A.AssetNo,A.Name,
E.Name + ' '+ E.Surname as Employee
,L.Name as Location  ,2 as ID_Type ,AM.ID_AssetMovement as ID_Key,Alt.ID_Alert
from Asset_Movement AM
Inner Join  Assets A on  AM.ID_Asset=A.ID_Asset
Inner Join locations L on AM.ID_Location=L.ID_Location
Inner Join Employees E on AM.ID_Employee=E.ID_Employee
Inner Join Alert alt on alt.ID_Alert=AM.ID_Alert and isNULL(alt.AcknowledgedBy,0)<=0
where AM.Is_deleted=0 and AM.Is_Active=1
and Am.Date_Modified between @date1 and @date2
and (AM.ID_Reader = @ID_Reader or @ID_Reader<=0)
)
Order by [Time] desc
/*
Select * from Locations
Select * from Employees
select ID_Employee, * from Asset_Movement
select * from Assets where  in ('00000000000000000000000M','000000000000000000000000')
select * from Asset_Movement order by date_modified desc
select * from Employees where tagID  in ('00000000000000000000000M','000000000000000000000000') 
*/
GO
/****** Object:  StoredProcedure [dbo].[Delete_Employee]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Employee
-- Modified by Vijay : - To also change the TagID with dummy value on deleting the employee
-- Modified on : - 11-11-11
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Employee]
(
	@ID_Employee bigint
	
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

UPDATE [Employees] SET Is_Deleted=1, Date_Modified=getdate(),TagID = 'xxxxxxxxxxxxxxxxxxxxxxxx' 
WHERE (([ID_Employee] = @ID_Employee))
GO
/****** Object:  StoredProcedure [dbo].[Get_Employee_By_UserName]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:		Deepanshu Jouhari
-- Create date: 7th November 2008
-- Description:	To GET a Employee By TAGID
-- ========================================================

Create PROCEDURE [dbo].[Get_Employee_By_UserName]
	@UserName varchar(50)
AS
	SET NOCOUNT ON;
SELECT  * FROM Employees
WHERE UserName=@UserName AND Is_Deleted=0 AND Is_Active=1

--select * from [Employees]
GO
/****** Object:  View [dbo].[Vw_Asset_MovementOnly]    Script Date: 06/24/2013 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Asset_MovementOnly]
AS
SELECT     dbo.Asset_Movement.ID_Asset, Assets_1.Name AS 'AssetName', Assets_1.Description, Assets_1.AssetNo, 
                      dbo.Asset_Movement.Date_Modified AS 'Date_Modified', dbo.Asset_Movement.Last_Modified_By, 
                      Employees_1.Name + ' ' + Employees_1.Surname AS 'EmployeeName', Locations_1.Name AS 'LocatioName', Locations_1.LocationNo, 
                      dbo.Asset_Movement.ID_Location, Assets_1.ID_AssetType, Asset_Types_1.Type, Assets_1.ID_AssetGroup, 
                      Asset_Groups_1.Name AS 'GroupName',  Asset_Movement.ID_AssetStatus, Asset_Status.Status, Asset_Movement.ReferenceNo, dbo.Asset_Movement.ID_LocationNew
FROM         dbo.Asset_Movement INNER JOIN
                      dbo.Locations AS Locations_1 ON dbo.Asset_Movement.ID_Location = Locations_1.ID_Location INNER JOIN
                      dbo.Assets AS Assets_1 ON dbo.Asset_Movement.ID_Asset = Assets_1.ID_Asset INNER JOIN
                      dbo.Employees AS Employees_1 ON dbo.Asset_Movement.Last_Modified_By = Employees_1.ID_Employee INNER JOIN
                      dbo.Asset_Types AS Asset_Types_1 ON Assets_1.ID_AssetType = Asset_Types_1.ID_AssetType INNER JOIN
                      dbo.Asset_Groups AS Asset_Groups_1 ON Assets_1.ID_AssetGroup = Asset_Groups_1.ID_AssetGroup INNER JOIN
                      Asset_Status ON Asset_Movement.ID_AssetStatus = Asset_Status.ID_AssetStatus

-------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Tags]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Get_Tags]
as 
	SELECT 
	tags.ID, 
	case when isNULL(a.ID_Asset,0)=0 
	then case when isNULL(E.ID_Employee,0)<>0 then 'Employee' else 'NA' end
	else a.Description end as Description,
	isNull(a.AssetNo,'NA') as AssetNo,
	case when isNULL(a.ID_Asset,0)=0 
	then case when isNULL(E.ID_Employee,0)<>0 then E.UserName else 'NA' end
	else A.Name end as Name,
	tags.RSSI,isnull(L.Name,'NA') as location, A.Date_Created
	FROM tags
    Left outer join Employees E on E.TagID=tags.ID
	LEFT OUTER JOIN Assets A ON tags.ID = A.TagID 
	Left Outer Join Locations L on A.ID_Location=L.ID_Location
	
	ORDER BY tags.ID
--select * from tags
--Select * from Employees where tagID='000000000000000000000997'
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetMovement_ByReaderAndTagList]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 5 th May 2008
-- Description:	To GET AssetMovement affected by particular reader in given Date Range
-- exec Get_AssetMovement_ByReaderAndTagList '2007-01-01','2009-01-01', 1, '''00000000000000000000000M'',''000000000000000000000000'''
--exec Get_AssetMovement_ByReaderAndTagList @date1='2008-05-06 00:00:00:000',@date2='2009-05-07 00:00:00:000',@ID_Reader=-1
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetMovement_ByReaderAndTagList]
@date1 Datetime,
@date2 Datetime,
@ID_Reader bigint
AS
(select A.tagID as ID,
CONVERT ( varchar, A.Date_Modified ,108) as 'Time', A.AssetNo,A.Name,
E.Name + ' '+E.Surname as Employee,L.Name as Location  ,
1 as ID_Type ,A.ID_Asset as ID_Key
from Assets A 
Inner Join locations L on A.ID_Location=L.ID_Location
Inner Join Employees E on A.ID_Employee=E.ID_Employee
where A.Is_deleted=0 and A.Is_Active=1 
and A.Date_Modified between @date1 and @date2
and (A.ID_Reader = @ID_Reader  or @ID_Reader<=0) and E.tagID in ('00000000000000000000000M','000000000000000000000000'))
Union
(select A.tagid as ID,
CONVERT ( varchar, AM.Date_Modified ,108) as 'Time', A.AssetNo,A.Name,
E.Name + ' '+ E.Surname as Employee
,L.Name as Location  ,2 as ID_Type ,AM.ID_AssetMovement as ID_Key
from Asset_Movement AM
Inner Join  Assets A on  AM.ID_Asset=A.ID_Asset
Inner Join locations L on AM.ID_Location=L.ID_Location
Inner Join Employees E on AM.ID_Employee=E.ID_Employee
where AM.Is_deleted=0 and AM.Is_Active=1
and Am.Date_Modified between @date1 and @date2
and (AM.ID_Reader = @ID_Reader or @ID_Reader<=0) and E.tagID in ('00000000000000000000000M','000000000000000000000000'))
Order by [Time] desc
/*
Select * from Locations
Select * from Employees
select ID_Employee, * from Asset_Movement
select * from Assets where  in ('00000000000000000000000M','000000000000000000000000')
select * from Asset_Movement order by date_modified desc
select * from Employees where tagID  in ('00000000000000000000000M','000000000000000000000000') 
*/
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset_Location_by_Reader]    Script Date: 06/24/2013 15:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 28th May 2008, 30 July 2008, 1 Jan 2009
-- Description:	To UPDATE a Asset
-- Update_Asset_Location_by_Reader 'FFFFFFFF0000000000000056',0,46,15,'633909631048125000';
-- =============================================

CREATE PROCEDURE [dbo].[Update_Asset_Location_by_Reader] 
--'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(50),
	@ID_Location bigint,
	@Last_Modified_By bigint,
	@ID_Reader bigint,
	@ReferenceNo nvarchar(150)='',
	@ID_Reason bigint=0,
	@JobID varchar(50) = null,
	@UpdateAllMovements int = 0
)
AS
	SET NOCOUNT OFF;
Declare @OldLocation bigint,
		@LastID bigint

set @LastID = @ID_Reason

DECLARE @ID_Asset BIGINT

SET @ID_Asset = 0

set @OldLocation = 0

Select @ID_Asset=ID_Asset,@OldLocation = isNULL(ID_Location,-1) from  [Assets] where TagID = @TAGID

if(@Last_Modified_By = 0)
BEGIN
	IF EXISTS(SELECT ID_EMployee FROM Employees WHERE [Name] LIKE '%Unknown%')
		SET @Last_Modified_By = (SELECT ID_EMployee FROM Employees WHERE [Name] LIKE '%Unknown%')
	ELSE 
		SET @Last_Modified_By = (SELECT Top 1 ID_Employee from Assets Where TagID = @TagID)
END  

IF @JobID IS NULL
BEGIN
	IF(@ID_Location =1)
	begin
		select * from Reasons
			SELECT  @ID_Location = isNULL(ID_LocationOut,2)  FROM  dbo.Readers WHERE ID_Reader = @ID_Reader;
			select @ID_Reason = ID_Reason  FROM  dbo.Reasons WHERE ReasonNo = '1';
	end
	ELSE
	begin
			SELECT  @ID_Location = isNULL(ID_Location,1) FROM  dbo.Readers WHERE ID_Reader = @ID_Reader;
			select @ID_Reason = ID_Reason  FROM  dbo.Reasons WHERE ReasonNo = '2';
	end
END
	--select isNULL(ID_LocationOut,-1)  FROM  dbo.Readers WHERE ID_Reader = 4

print 'Select Location';

--print @TAGID + cast(@OldLocation as varchar) + ' ' +  cast(@ID_Location as varchar); 
if (@OldLocation <> @ID_Location) OR (@UpdateAllMovements = 1)
Begin

	print 'update the timeout for last record if any'
	IF @JobID IS NOT NULL
	BEGIN
		SET @LastID = (Select MAX(ID_AssetMovement) FROM [Asset_Movement] WHERE ID_Asset = @ID_Asset AND JobID = @JobID)
		UPDATE [Asset_Movement] SET TimeStampOut = getdate()
		WHERE ID_AssetMovement = @LastID
		--IN (Select MAX(ID_AssetMovement) FROM [Asset_Movement] WHERE ID_Asset = @ID_Asset AND JobID = @JobID)											
		--ID_Asset = @ID_Asset AND JobID = @JobID
		
	END	

	print 'Insert Into Asset Movement';
		INSERT INTO [Asset_Movement]
           ([ID_Asset]
           ,[ID_Location]
           ,[Date_Created]
           ,[Date_Modified]
		   ,[TimeStamp]
		   ,[TimeStampOut]
		   ,[JobID]
           ,[Is_Deleted]
           ,[Is_Active]
           ,[Last_Modified_By]
           ,[ID_Reader]
           ,[ID_AssetStatus]
		   ,[ID_Employee]
		   ,[ID_TagType]
		   ,[ID_Alert]
			,[ReferenceNo]
			,[ID_LocationNew]
			,[IS_VernonRead]
			,[ID_Reason])
			Select 
			[ID_Asset]
			,[ID_Location]
			,[Date_Modified]
			,getdate()
			,getdate()
			,getdate()
			,@JobID
			,[Is_Deleted]
			,[Is_Active]
			,ID_Employee as Last_Modified_By
			,CASE WHEN (@ID_Reader = 0 OR @ID_Reader is Null) THEN ID_Reader ELSE @ID_Reader end as ID_Reader
			,[ID_AssetStatus]
			,@Last_Modified_By as ID_Employee
			,[ID_TagType]	
			,[ID_Alert]
			,[ReferenceNo]
			,@ID_Location as ID_LocationNew
			,0 as IS_VernonRead
			,ISNULL(@LastID,@ID_Reason) AS ID_Reason
			From [dbo].[Assets] 
			where [TAGID] = @TAGID 
		 

End 

print 'Update Asset';

set dateformat dmy 


if(@ID_Reader <> 0)
	BEGIN
			UPDATE [Assets]
			SET [ID_Location] = @ID_Location,
			ID_Employee=@Last_Modified_By,
			Last_Modified_By = @Last_Modified_By,
			[Date_Modified] = getdate(),
			ID_Reader =	@ID_Reader,
			ReferenceNo=@ReferenceNo 
			WHERE TagID = @TagID
	END
ELSE
	BEGIN
			UPDATE [Assets]
			SET [ID_Location] = @ID_Location,
			[Date_Modified] = getdate(),
			ID_Employee=@Last_Modified_By, 
			Last_Modified_By = @Last_Modified_By, 
			ReferenceNo=@ReferenceNo 
			WHERE TagID = @TagID
	END
	

print 'Update Asset Completed';
GO
/****** Object:  StoredProcedure [dbo].[Get_Employee_By_ID]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a Employee by ID_Employee
-- =============================================

CREATE PROCEDURE [dbo].[Get_Employee_By_ID]
	@ID_Employee bigint
AS
	SET NOCOUNT ON;
SELECT     ID_Employee, TagID, Name, Surname, EmpNo, Email, Address, City, ID_State, DOB, UserName, Password, ID_Department, ID_SecurityGroup, ID_TagType, ID_Skill, 
                      Date_Created, Date_Modified, Is_Deleted, Is_Active, isnull(Last_Modified_By,0) as Last_Modified_By
FROM         Employees
WHERE		ID_Employee=@ID_Employee
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_FSAssetsReportOS]    Script Date: 06/24/2013 15:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Modified:	
-- Create date: 30-June-2009
-- Description:	To GET Report of FS Assest
-- =============================================

CREATE PROCEDURE [dbo].[Get_FSAssetsReportOS]  
@Name nvarchar(100),
@ID_Template bigint,
@ID_Location bigint,
@ID_AssetGroup bigint,
@StartDate Datetime,
@EndDate Datetime,
@FSTWise INT = 1,
@Opt int = 0
AS
Begin
	-- EXEC [Get_FSAssetsReportOS]  '%',-1,-1,-1,'2008-07-17','2011-07-17',0,1
	-- @opt = 1 For Item History Report
	-- @opt = 0 For Item outstanding Report
	-- @FSTWise = 1 then Template Wise else Task Wise
	SET NOCOUNT ON;

	IF (@Opt = 0)
	BEGIN	
	SELECT distinct  Assets, Template,DueDate, Location,Tasks,FSStatus,TaskStatus   From
	(
		SELECT Distinct FS_AS.ID_FSAsset,A.Name As Assets,FST.Title as Template,FS_AS.DueDate as DueDate,L.Name as Location,
        (Case when FS_AS.FSStatus = 1010 then 'New' 
		 when FS_AS.FSStatus = 1020 then 'In Process'
		 when FS_AS.FSStatus = 1030 then 'Completed'
		 when FS_AS.FSStatus = 1040 then 'Expired'
		 else 'Cancel' end) as FSStatus,
		CASE When @FSTWise = 1 then 0 else FS_AS.ID_Tasks end as ID_Tasks,
		CASE When @FSTWise = 1 then 'Task' else T.Title end as Tasks,
	    (CASE When @FSTWise = 1 then 'Ok' else  (Case when FS_AS.TaskStatus = 1010 then 'New' 
												when FS_AS.TaskStatus = 1020 then 'In Process'
												when FS_AS.TaskStatus = 1030 then 'Completed'
												when FS_AS.TaskStatus = 1040 then 'Expired'
												when FS_AS.TaskStatus = 1050 then 'Cancel'
												when FS_AS.TaskStatus = 1060 then 'Ignored'  
												else 'NA' end) end)
        as TaskStatus 
		FROM FS_AssetStatus FS_AS
		Inner Join Assets A ON FS_AS.ID_Asset = A.Id_Asset and A.Is_Deleted=0 and A.Is_Active=1  
		Inner Join Locations L ON A.ID_Location = L.ID_Location
		Inner Join Tasks T ON FS_AS.ID_Tasks = T.ID_Tasks 
		Inner Join FS_Templates FST ON FS_AS.ID_Template = FST.ID_Template
--		Inner Join FS_Assets FSA ON FS_AS.ID_FSAsset = FSA.ID_FSAsset 
		Where ((FSStatus = 1010 OR FSStatus = 1020) )	--AND (TaskStatus = 1010 OR TaskStatus = 1020)
		AND DueDate >= @StartDate
		AND DueDate <= @EndDate 
	    AND (@ID_Template = -1 OR FS_AS.ID_Template=@ID_Template)
	    AND (@ID_Location = -1 OR A.ID_Location=@ID_Location)
		AND (@ID_AssetGroup = -1 OR A.ID_AssetGroup=@ID_AssetGroup)
		AND (@Name = '%' or A.Name like @Name)		
		AND FS_AS.IS_Deleted=0 
	) as Temp
	GROUP BY ID_Tasks,Assets, Template,Location,Tasks,FSStatus,TaskStatus,ID_FSAsset,DueDate
	END
	ELSE
	BEGIN
		SELECT distinct  Assets, Template, MAX(StatusDate) as StatusDate, Location,Tasks,FSStatus,TaskStatus,EmployeeName,DueDate From
	 (
		SELECT Distinct FS_AS.ID_FSAsset,A.Name As Assets,FST.Title as Template,
		--ISNULL(FS_AS.StatusDate,FS_AS.DueDate) as StatusDate
		FS_AS.StatusDate,L.Name as Location,
        (Case when FS_AS.FSStatus = 1010 then 'New' 
		 when FS_AS.FSStatus = 1020 then 'In Process'
		 when FS_AS.FSStatus = 1030 then 'Completed'
		 when FS_AS.FSStatus = 1040 then 'Expired'
		 else 'Cancel' end) as FSStatus,
		CASE When @FSTWise = 1 then 0 else FS_AS.ID_Tasks end as ID_Tasks,
		CASE When @FSTWise = 1 then 'Task' else T.Title end as Tasks,
		(CASE When @FSTWise = 1 then 'Ok' else  (Case when FS_AS.TaskStatus = 1010 then 'New' 
												when FS_AS.TaskStatus = 1020 then 'In Process'
												when FS_AS.TaskStatus = 1030 then 'Completed'
												when FS_AS.TaskStatus = 1040 then 'Expired'
												when FS_AS.TaskStatus = 1050 then 'Cancel'
												when FS_AS.TaskStatus = 1060 then 'Ignored'  
												else 'NA' end) end)
        as TaskStatus,
		ISNULL(E.Name,'-') as EmployeeName,FS_AS.DueDate as DueDate 
		FROM FS_AssetStatus FS_AS 
		Inner Join Assets A ON FS_AS.ID_Asset = A.Id_Asset  
		Inner Join Locations L ON A.ID_Location = L.ID_Location
		Inner Join Employees E ON FS_AS.ID_Employee = E.ID_Employee
		Inner Join FS_Templates FST ON FS_AS.ID_Template = FST.ID_Template
		Inner Join Tasks T ON FS_AS.ID_Tasks = T.ID_Tasks 
--		Left Outer Join FS_Assets FSA ON FS_AS.ID_FSAsset = FSA.ID_FSAsset 
		Where (
		((FSStatus = 1010 OR FSStatus = 1020)and A.Is_Deleted=0 and A.Is_Active=1) OR FSStatus = 1030)	
		--AND (TaskStatus = 1030 OR TaskStatus = 1020) Not Needed to check the task conditions
		AND CAST(CONVERT(varchar(10), DueDate, 101) as Datetime) >= @StartDate
		AND CAST(CONVERT(varchar(10), DueDate, 101) as Datetime) <= @EndDate 
	    AND (@ID_Template = -1 OR FS_AS.ID_Template=@ID_Template)
	    AND (@ID_Location = -1 OR A.ID_Location=@ID_Location)
		AND (@ID_AssetGroup = -1 OR A.ID_AssetGroup=@ID_AssetGroup)
		AND (@Name = '%' or A.Name like @Name)	
		AND FS_AS.IS_Deleted=0 
	) as Temp
	GROUP BY ID_Tasks,Assets, Template,Location,Tasks,FSStatus,TaskStatus,EmployeeName,ID_FSAsset,DueDate
	END
END

/*
EXEC [Get_FSAssetsReportOS]  '%',-1,-1,-1,'2008-07-17','2011-07-17',0,1

select * from FS_AssetStatus where FSSTATUS in ('1010','1020','1030') and is_Deleted=0

select * from Assets where ID_Asset in (177,535,558)
*/
--select * from locations
--update locations set Date_Modified=getDate() where Is_Deleted=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Employee_By_TagID]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:		Deepanshu Jouhari
-- ModifiedBY: Vijay Mangtani
-- Create date: 30th Dec 2009
-- Description:	To GET a Employee By TAGID
-- ========================================================

CREATE PROCEDURE [dbo].[Get_Employee_By_TagID]
	@TagID varchar(6000),
	@optionCSV int = 0
AS
	SET NOCOUNT ON;
IF @optionCSV = 0
	BEGIN
		SELECT  *,'' as [Description] FROM Employees
		WHERE TagID=@TagID AND Is_Deleted=0 AND Is_Active=1
	END
ELSE
	BEGIN
		DECLARE @SQL Varchar(MAX)
		SET @SQL = 'SELECT ID_Employee,E.TagID,E.ID_TagType,E.Name,E.UserName,isnull(E.Last_Modified_By,0) as Last_Modified_By,
					  SG.Name as Description 							
					 FROM Employees E Inner Join Master_SecurityGroups SG
					 ON E.ID_SecurityGroup = SG.ID_SecurityGroup
					WHERE E.TagID IN (' + @TagID + ') AND E.Is_Deleted=0 AND E.Is_Active=1'
		print @SQL;
		EXEC(@SQL);
	END



-----------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Asset_ByReader]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 31st March 2008
-- Description:	To Get_Asset_ByReader affected by particular reader in given Date Range It is for History Grid, Picking all data.
-- exec Get_Asset_ByReader '2007-01-01','2009-01-01', 4
-- =============================================

CREATE PROCEDURE [dbo].[Get_Asset_ByReader]
@date1 Datetime,
@date2 Datetime,
@ID_Reader bigint
AS
BEGIN
if @ID_Reader<>-1
	Begin
		select E.tagID as etag, A.tagID as ID,
		CONVERT ( varchar, A.Date_Modified ,108) as 'Time',
		A.AssetNo,
		A.Name,
		E.Name + ' '+E.Surname as Employee,
		L.Name as Location,
		A.Date_Modified as Date_Modified,
		A.Description   
		from Assets A 
		Inner Join locations L on A.ID_Location=L.ID_Location
		Inner Join Employees E on A.ID_Employee=E.ID_Employee 
		where
		A.ReferenceNo = (Select top 1 ReferenceNo from Assets AE where AE.Date_Modified between @date1 and @date2
							and AE.ID_Reader = @ID_Reader Order by AE.[Date_Modified] desc)
		and	A.Is_deleted=0 and A.Is_Active=1 
		and A.Date_Modified between @date1 and @date2
		and A.ID_Reader = @ID_Reader
		Order by [Date_Modified] desc
	end
else
	begin
		select E.tagID as etag, A.tagID as ID,
		CONVERT ( varchar, A.Date_Modified ,108) as 'Time', A.AssetNo,A.Name,
		E.Name + ' '+E.Surname as Employee,L.Name as Location,A.Date_Modified as Date_Modified,
		A.Description       
		from Assets A 
		Inner Join locations L on A.ID_Location=L.ID_Location
		Inner Join Employees E on A.ID_Employee=E.ID_Employee
		where 
		A.ReferenceNo = (Select top 1 ReferenceNo from Assets AE where AE.Date_Modified between @date1 and @date2
							 Order by AE.[Date_Modified] desc)
		and A.Is_deleted=0 and A.Is_Active=1 
		and A.Date_Modified between @date1 and @date2
		Order by [Date_Modified] desc
	end
end
/*
Select * from Locations
Select * from Employees
select * from Asset_Movement
select * from Assets

select * from Employees
*/
GO
/****** Object:  View [dbo].[Vw_Asset_Movement]    Script Date: 06/24/2013 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_Asset_Movement]
AS
--SELECT     Assets.ID_Asset, Assets.Name AS 'AssetName', Assets.Description, Assets.AssetNo, Assets.Date_Modified AS 'DateMoved', 
--                      Assets.Last_Modified_By, Employees.Name + ' ' + Employees.Surname AS 'EmployeeName', Locations.Name AS 'LocationName', 
--                      Locations.LocationNo, Assets.ID_Location, Assets.ID_AssetType, Asset_Types.Type, Assets.ID_AssetGroup, Asset_Groups.Name AS 'GroupName', 
--                      Assets.ID_AssetStatus, Asset_Status.Status, Assets.ReferenceNo, Assets.TagID 
--FROM         Assets INNER JOIN
--                      Employees ON Assets.Last_Modified_By = Employees.ID_Employee INNER JOIN
--                      Locations ON Assets.ID_Location = Locations.ID_Location INNER JOIN
--                      Asset_Types ON Assets.ID_AssetType = Asset_Types.ID_AssetType INNER JOIN
--                      Asset_Groups ON Assets.ID_AssetGroup = Asset_Groups.ID_AssetGroup INNER JOIN
--                      Asset_Status ON Assets.ID_AssetStatus = Asset_Status.ID_AssetStatus
--UNION
SELECT     dbo.Asset_Movement.ID_Asset, Assets_1.Name AS 'AssetName', Assets_1.Description, Assets_1.AssetNo, 
                      dbo.Asset_Movement.Date_Created AS 'DateMoved', dbo.Asset_Movement.Last_Modified_By, 
                      Employees_1.Name + ' ' + Employees_1.Surname AS 'EmployeeName', Locations_1.Name AS 'LocationName', Locations_1.LocationNo, 
                      dbo.Asset_Movement.ID_Location, Assets_1.ID_AssetType, Asset_Types_1.Type, Assets_1.ID_AssetGroup, 
                      Asset_Groups_1.Name AS 'GroupName',  Asset_Movement.ID_AssetStatus, Asset_Status.Status, Asset_Movement.ReferenceNo,Assets_1.TagID 
FROM         dbo.Asset_Movement INNER JOIN
                      dbo.Locations AS Locations_1 ON dbo.Asset_Movement.ID_Location = Locations_1.ID_Location INNER JOIN
                      dbo.Assets AS Assets_1 ON dbo.Asset_Movement.ID_Asset = Assets_1.ID_Asset INNER JOIN
                      dbo.Employees AS Employees_1 ON dbo.Asset_Movement.Last_Modified_By = Employees_1.ID_Employee INNER JOIN
                      dbo.Asset_Types AS Asset_Types_1 ON Assets_1.ID_AssetType = Asset_Types_1.ID_AssetType INNER JOIN
                      dbo.Asset_Groups AS Asset_Groups_1 ON Assets_1.ID_AssetGroup = Asset_Groups_1.ID_AssetGroup INNER JOIN
                      Asset_Status ON Asset_Movement.ID_AssetStatus = Asset_Status.ID_AssetStatus
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4[30] 2[40] 3) )"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 3
   End
   Begin DiagramPane = 
      PaneHidden = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 5
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Asset_Movement'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_Asset_Movement'
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetMovement_ByReader]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 5 th May 2008, 30 July 2008
-- Description:	To GET AssetMovement affected by particular reader in given Date Range It is for History Grid, Picking all data.
-- exec Get_AssetMovement_ByReader '2010-01-18','2010-01-20', 4
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetMovement_ByReader]
@date1 Datetime,
@date2 Datetime,
@ID_Reader bigint

AS
BEGIN
if @ID_Reader<>-1
	Begin
		(select E.tagID as etag, A.tagID as ID,
		CONVERT ( varchar, A.Date_Modified ,108) as 'Time', A.AssetNo,A.Name,
		E.Name + ' '+E.Surname as Employee,L.Name as Location,
		DateAdd(second,1,A.Date_Modified) as Date_Modified   
		from Assets A 
		Inner Join locations L on A.ID_Location=L.ID_Location
		Inner Join Employees E on A.ID_Employee=E.ID_Employee
		where A.Is_deleted=0 and A.Is_Active=1 
		and A.Date_Modified between @date1 and @date2
		and A.ID_Reader = @ID_Reader)
		Union
		(select E.tagID as etag, A.tagid as ID,
		CONVERT ( varchar, AM.Date_Created ,108) as 'Time', A.AssetNo,A.Name,
		E.Name + ' '+ E.Surname as Employee
		,L.Name as Location, AM.Date_Created as Date_Modified  
		from Asset_Movement AM
		Inner Join  Assets A on  AM.ID_Asset=A.ID_Asset
		Inner Join locations L on AM.ID_Location=L.ID_Location
		Inner Join Employees E on AM.Last_Modified_By=E.ID_Employee
		where AM.Is_deleted=0 and AM.Is_Active=1
		and Am.Date_Modified between @date1 and @date2
		and AM.ID_Reader = @ID_Reader)
		Order by [Date_Modified] desc
	end
else
	begin
		(select E.tagID as etag, A.tagID as ID,
		CONVERT ( varchar, A.Date_Modified ,108) as 'Time', A.AssetNo,A.Name,
		E.Name + ' '+E.Surname as Employee,L.Name as Location,
		DateAdd(second,1,A.Date_Modified) as Date_Modified   
		from Assets A 
		Inner Join locations L on A.ID_Location=L.ID_Location
		Inner Join Employees E on A.ID_Employee=E.ID_Employee
		where A.Is_deleted=0 and A.Is_Active=1 
		and A.Date_Modified between @date1 and @date2
		)
		Union
		(select E.tagID as etag, A.tagid as ID,
		CONVERT ( varchar, AM.Date_Created ,108) as 'Time', A.AssetNo,A.Name,
		E.Name + ' '+ E.Surname as Employee
		,L.Name as Location, AM.Date_Created as Date_Modified    
		from Asset_Movement AM
		Inner Join  Assets A on  AM.ID_Asset=A.ID_Asset
		Inner Join locations L on AM.ID_Location=L.ID_Location
		Inner Join Employees E on AM.Last_Modified_By=E.ID_Employee
		where AM.Is_deleted=0 and AM.Is_Active=1
		and Am.Date_Modified between @date1 and @date2)
		Order by [Date_Modified] desc
	end

end
/*
Select * from Locations
Select * from Employees
select * from Asset_Movement
select * from Assets

select * from Employees
*/
GO
/****** Object:  StoredProcedure [dbo].[Check_DuplicateUserName]    Script Date: 06/24/2013 15:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 25th March 2008
-- Description:	To CHECK for Duplicate UserName.
-- =============================================

CREATE PROCEDURE [dbo].[Check_DuplicateUserName]  --'asif'
	-- Add the parameters for the stored procedure here
	@UserName varchar(50)
	
AS
BEGIN
	if EXISTS(select @UserName From Employees where UserName = @UserName)
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[Count_Objects]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =======================================================
-- Author:		Asif Ali
-- Create date: 21st March 2008
-- Description:	To Get Asset, Employee & Location COUNT
-- =======================================================

CREATE PROCEDURE [dbo].[Count_Objects]

AS
	SET NOCOUNT OFF;

select count(*) from Assets
select count(*) from Employees
select count(*) from Locations

--SELECT rows FROM sysindexes WHERE id = OBJECT_ID('Assets') AND indid < 2
--SELECT rows FROM sysindexes WHERE id = OBJECT_ID('Employees') AND indid < 2
--SELECT rows FROM sysindexes WHERE id = OBJECT_ID('Locations') AND indid < 2
GO
/****** Object:  StoredProcedure [dbo].[Get_ExternalSensorByReaderID]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 03-08-2011
-- Description:	<Get External Sensor Settings>
-- =============================================
CREATE PROCEDURE [dbo].[Get_ExternalSensorByReaderID]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader bigint		        
		)		   
	
AS
BEGIN
	
	SET NOCOUNT ON;
	SET DATEFORMAT dmy 
	
	SELECT isnull(ReaderID,0) as ReaderID 
				,isnull(IPAddress,'') as IPAddress
				,isnull(PortNo,0) as PortNo
				,isnull(UDPPort,0) as UDPPort
				,isnull(IsActive,0) as IsActive
				,isnull(RelayOffCmd,'') as RelayOffCmd
				,isnull(RelayOnCmd,'') as RelayOnCmd 
	FROM ExternalSensor WHERE ReaderID = @ID_Reader

END


----------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Add_ExternalSensor]    Script Date: 06/24/2013 15:03:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 03-08-2011
-- Description:	<Add External Sensor Settings>
-- =============================================
CREATE PROCEDURE [dbo].[Add_ExternalSensor]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader bigint		 
           ,@IPAddress nvarchar(20)
           ,@PortNo INT
		   ,@UDPPOrt INT
		   ,@IsActive Bit = 1
		   ,@RelayOffCmd varchar(50) = ''
		   ,@RelayOnCmd  varchar(50) = ''
		)		   
	
AS
BEGIN
	
	SET NOCOUNT ON;
	set dateformat dmy 

IF NOT EXISTS(Select * FROM ExternalSensor Where ReaderID = @ID_Reader)
	BEGIN
		INSERT INTO   ExternalSensor(ReaderID,IPAddress,PortNo,UDPPOrt,IsActive,RelayOffCmd,RelayOnCmd)
						  VALUES(@ID_Reader,@IPAddress,@PortNo,@UDPPOrt,@IsActive,@RelayOffCmd,@RelayOnCmd)
	END
ELSE
	BEGIN
		UPDATE   ExternalSensor SET IPAddress = @IPAddress,PortNo = @PortNo,UDPPort=@UDPPort,IsActive=@IsActive,RelayOffCmd= @RelayOffCmd,RelayOnCmd=@RelayOnCmd
					 WHERE ReaderID = @ID_Reader
	END

 

END


----------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Add_State]    Script Date: 06/24/2013 15:04:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Satte
-- =============================================

CREATE PROCEDURE [dbo].[Add_State]
(
	@Name varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Master_States] ([Name], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_State, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_States WHERE (ID_State = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Get_States]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all States
-- =============================================

CREATE PROCEDURE [dbo].[Get_States]
AS
	SET NOCOUNT ON;

--SELECT     -1 as ID_State,'---Select--- ' as Name, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Master_States
--
--UNION

SELECT     ID_State, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_States

WHERE	Is_Deleted=0 AND Is_Active=1

--------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_States_All]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all States
-- =============================================

CREATE PROCEDURE [dbo].[Get_States_All]
AS
	SET NOCOUNT ON;
SELECT     ID_State, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_States

WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Delete_State]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a State
-- =============================================

CREATE PROCEDURE [dbo].[Delete_State]
(
	@ID_State bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Master_States] WHERE (([ID_State] = @ID_State))

UPDATE [Master_States] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_State] = @ID_State))
GO
/****** Object:  StoredProcedure [dbo].[Update_State]    Script Date: 06/24/2013 15:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a State
-- =============================================

CREATE PROCEDURE [dbo].[Update_State]
(
	@Name varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_State bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Master_States] SET [Name] = @Name, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_State] = @ID_State));
	
SELECT ID_State, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_States WHERE (ID_State = @ID_State)
GO
/****** Object:  StoredProcedure [dbo].[Get_State_By_ID]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a State by ID_State
-- =============================================

CREATE PROCEDURE [dbo].[Get_State_By_ID]
	@ID_State bigint
AS
	SET NOCOUNT ON;
SELECT     ID_State, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_States
WHERE		ID_State=@ID_State
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Vernon_DeleteLocation]    Script Date: 06/24/2013 15:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 07 Aug 2009
-- Description:	To ADD a Location from Vernon Locations File
-- =============================================

CREATE PROCEDURE [dbo].[Vernon_DeleteLocation]
(
	 @LocationNo varchar(50),
	@Last_Modified_By bigint
   
)
AS
	SET NOCOUNT OFF;

 
BEGIN
	 	UPDATE [Locations] Set 
			[Date_Modified] = getdate(),
			Last_Modified_By = @Last_Modified_By,
			 Is_Deleted = 1
		WHERE [LocationNo] =  @LocationNo

END
GO
/****** Object:  StoredProcedure [dbo].[Search_Inventory]    Script Date: 06/24/2013 15:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		Vinay Bansal
-- Create date: 15-June-2009
-- Description:	To Search all Inventory
-- Modified By - Vijay
-- Modified On - 30/06/2011
-- =========================================================

CREATE PROCEDURE [dbo].[Search_Inventory] 
 @StartDate Datetime,
 @EndDate Datetime,
 @Parameter nvarchar(50)
AS
Begin
	Set NoCount ON;
		--SET @StartDate = '2009-06-15'
		--SET @EndDate = '2009-06-15'
		-- 
		-- Exec [Search_Inventory] '2011-09-01', '2011-09-08','0000000000000000000000D' 
		
		Declare @LocationID bigint
		Select @LocationID = ID_location FROM Locations WHERE [TagID] = @Parameter
		
	 	SELECT distinct [Name], ISnull(LotNO,'') as LotNO, ISNULL(AssetNo,'') as AssetNo, Case when  ExpiryDate = '1900-01-01' THEN null else ExpiryDate END as ExpiryDate, ISNULL(Allocation,'') as Allocation
		,--Date_Created,Date_Modified,ID_Location,IS_Deleted,
		CASE WHEN ((Date_Created > @StartDate and Date_Created < @endDate) or (ID_Location = @LocationID and Date_Modified > @StartDate AND Date_Modified < @EndDate)
					 AND lotno IS NOT NULL AND  ExpiryDate IS NOT NULL  AND  Allocation IS NOT NULL)
             THEN 1 
			 ELSE 0 END as MustShow--, ISnull( lotno,'') as LotNo,ISnull( Allocation,'') as Allocation,ISnull( ExpiryDate,'') as ExpiryDate,ISnull(assetno,'') as assetno
		
		INTO #TempInventory FROM assets A
		--Inner Join Master_Assets MA ON MA.ID_AssetMAster = A.ID_AssetMAster
		WHERE IS_Deleted = 0 AND Date_Created <= @EndDate --DateAdd(Day,-1,@EndDate)
		Group BY [Name], LotNo,AssetNo, ExpiryDate, Allocation,Date_Created,Date_Modified,ID_Location,IS_Deleted

--  print Convert(varchar(20),getdate(),108);

  SELECT distinct count(A.Name) as QuantityOpen , T.* INTO #TempInventory1 from #TempInventory T
  LEFT OUTER JOIN ASSETS A ON A.Name = T.Name AND  A.Date_Created <= @StartDate and A.IS_Deleted = 0 AND A.ExpiryDate = T.ExpiryDate
 GROUP BY T.[Name], T.LotNo,T.AssetNo, T.ExpiryDate,T.Allocation,T.MustShow --T.Date_Created,T.Date_Modified,T.ID_Location,T.IS_Deleted,

--print Convert(varchar(20),getdate(),108);

Drop table #TempInventory  

 SELECT count(A.Name) as Dispached, T.* INTO #TempInventory2 from #TempInventory1 T
 LEFT OUTER JOIN ASSETS A ON  
 A.Name = T.Name AND A.ID_Location = @LocationID AND A.Date_Modified <= @StartDate AND A.IS_Deleted = 0  
 GROUP BY T.[Name], T.LotNo,T.AssetNo, T.ExpiryDate,T.Allocation,T.MustShow,T.QuantityOpen --T.Date_Created,T.Date_Modified,T.ID_Location,T.IS_Deleted,

Drop table #TempInventory1

--print Convert(varchar(20),getdate(),108);

 SELECT DISTINCT count(A.Name) as QuantityRec, T.* INTO #TempInventory3 from #TempInventory2 T
 LEFT OUTER JOIN ASSETS A ON  
 A.Name = T.Name  AND  A.Date_Created > @StartDate AND A.Date_Created <= @EndDate AND A.IS_Deleted = 0  
 GROUP BY T.[Name], T.LotNo,T.AssetNo, T.ExpiryDate,T.Allocation,T.MustShow,T.QuantityOpen,T.Dispached --T.Date_Created,T.Date_Modified,T.ID_Location,T.IS_Deleted,

--print Convert(varchar(20),getdate(),108);

Drop table #TempInventory2 

 SELECT Distinct count(A.Name) as QuantityDis, T.* INTO #TempInventory4 from #TempInventory3 T
 LEFT OUTER JOIN ASSETS A ON  
 A.Name = T.Name AND A.ID_Location = @LocationID AND A.Date_Modified > @StartDate AND A.Date_Modified < @EndDate  AND A.IS_Deleted = 0  
 GROUP BY T.[Name], T.LotNo,T.AssetNo, T.ExpiryDate,T.Allocation, T.MustShow,T.QuantityOpen,T.Dispached, T.QuantityRec--T.Date_Created,T.Date_Modified,T.ID_Location,T.IS_Deleted,
		
--print Convert(varchar(20),getdate(),108);

Drop table #TempInventory3

SELECT OpenStock = QuantityOpen - Dispached,* into #TempInventory5  from #TempInventory4 

Drop table #TempInventory4

SELECT CloseStock = OpenStock + QuantityRec - QuantityDis, * into #TempInventory6  from #TempInventory5 

--print Convert(varchar(20),getdate(),108);

Drop table #TempInventory5  

	SELECT ROW_NUMBER() OVER (ORDER BY [Name]) SrNo,
    [Name],LotNO,AssetNo,ExpiryDate,Allocation,
	Sum(QuantityOpen) as QuantityOpen,sum(Dispached) as Dispached,sum(OpenStock) as OpenStock,
	 sum(QuantityRec) as QuantityRec,sum(QuantityDis) as QuantityDis,sum(CloseStock) as CloseStock
	FROM #TempInventory6 
	where (CloseStock <>0 or MustShow=1) 
	Group By [Name],LotNO,AssetNO,ExpiryDate,Allocation,QuantityOpen,Dispached,OpenStock,
	 QuantityRec,QuantityDis,CloseStock
	ORDER BY [Name]

Drop table #TempInventory6


END

----  OLD Query --------

		-- Insert All Item into Temp Table
--		SELECT [Name],  LotNO, AssetNo, Case when  ExpiryDate = '1900-01-01' THEN null else ExpiryDate END as ExpiryDate, Allocation
--		,Cast(0 as Int) as QuantityOpen,Cast(0 as Int) as Dispached, Cast(0 as Int) as OpenStock,
--		 Cast(0 as Int) as QuantityRec,Cast(0 as Int) as QuantityDis, Cast(0 as Int) as CloseStock,
----Added
--		CASE WHEN (Select count(*) from Assets SUBA where (
--					(SUBA.Date_Created > @StartDate and SUBA.Date_Created < @endDate) 
--					or (SUBA.ID_Location = @LocationID and SUBA.Date_Modified > @StartDate AND SUBA.Date_Modified < @EndDate)
--					) and SUBA.Name = A.[Name] AND ISnull(SUBA.lotno,'') = ISnull(A.lotno,'') AND ISnull(SUBA.ExpiryDate,'') =ISnull(A.ExpiryDate,'')
--					 AND ISnull(SUBA.Allocation,'') = ISnull(A.Allocation,''))>=1
--             THEN 1 
--			 ELSE 0 END as MustShow
--		INTO #TempInventory FROM assets A
--		--Inner Join Master_Assets MA ON MA.ID_AssetMAster = A.ID_AssetMAster
--		WHERE IS_Deleted = 0 AND Date_Created <= @EndDate --DateAdd(Day,-1,@EndDate)
--		Group BY [Name], LotNo,AssetNo, ExpiryDate, Allocation
--
----Select * from Assets
--
--		-- Update Open Quantity and Dispached into temp
--			Update #TempInventory 
--			SET  QuantityOpen =  (SELECT Count(ID_Asset) FROM Assets WHERE [Name] = A.[Name] AND ISnull(lotno,'') = ISnull(A.lotno,'') AND ISnull(ExpiryDate,'') =ISnull(A.ExpiryDate,'') AND ISnull(Allocation,'') = ISnull(A.Allocation,'') AND Date_Created <= @StartDate and IS_Deleted = 0 )
--			,Dispached = (SELECT Count(ID_Asset) FROM Assets WHERE [Name] = A.[Name] 
--			AND ISnull(lotno,'') = ISnull(A.lotno,'') AND ISnull(assetno,'') = ISnull(A.Assetno,'') 
--			AND ISnull(ExpiryDate,'') =ISnull(A.ExpiryDate,'') AND ISnull(Allocation,'') = ISnull(A.Allocation,'') AND  ID_Location = @LocationID AND Date_Modified <= @StartDate AND IS_Deleted = 0)
--			FROM assets A 
--			WHERE #TempInventory.[Name] = A.[Name]
--					AND ISnull(#TempInventory.lotno,'') = ISnull(A.lotno,'')
--					AND ISnull(#TempInventory.AssetNo,'') = ISnull(A.AssetNo,'')
--					AND ISnull(#TempInventory.ExpiryDate,'') =ISnull(A.ExpiryDate,'')
--					AND ISnull(#TempInventory.Allocation,'') = ISnull(A.Allocation,'')
--
--			-- Update OpenStock into temp
--
--			Update #TempInventory SET OpenStock = QuantityOpen - Dispached
--		
--			-- Update Quantity Recieved between time period into temp
--			Update #TempInventory SET  QuantityRec =  (SELECT Count(ID_Asset) FROM Assets 
--			WHERE [Name] = A.[Name] AND ISnull(lotno,'') = ISnull(A.lotno,'')
--			AND ISnull(assetno,'') = ISnull(A.Assetno,'') 
--			AND ISnull(ExpiryDate,'') =ISnull(A.ExpiryDate,'') AND ISnull(Allocation,'') = ISnull(A.Allocation,'') AND Date_Created > @StartDate AND Date_Created <= @EndDate and IS_Deleted = 0)
--			FROM assets A 
--			WHERE  #TempInventory.[Name] = A.[Name] 
--				AND ISnull(#TempInventory.lotno,'') = ISnull(A.lotno,'')
--				AND ISnull(#TempInventory.AssetNo,'') = ISnull(A.AssetNo,'')
--				AND ISnull(#TempInventory.ExpiryDate,'') =ISnull(A.ExpiryDate,'')
--				AND ISnull(#TempInventory.Allocation,'') = ISnull(A.Allocation,'')
--
--			-- Update Quantity Dispatched between time period into temp
--			Update #TempInventory SET QuantityDis =  (SELECT Count(ID_Asset) FROM Assets WHERE [Name] = A.[Name] 
--					AND ISnull(lotno,'') = ISnull(A.lotno,'') 
--					AND ISnull(assetno,'') = ISnull(A.Assetno,'') 
--					AND ISnull(ExpiryDate,'') =ISnull(A.ExpiryDate,'') AND ISnull(Allocation,'') = ISnull(A.Allocation,'') 
--					AND  ID_Location = @LocationID and IS_Deleted = 0 
--					AND Date_Modified > @StartDate AND Date_Modified < @EndDate)
--			FROM assets A
--			WHERE  
--				#TempInventory.[Name] = A.[Name] 
--				AND ISnull(#TempInventory.lotno,'') = ISnull(A.lotno,'') 
--				AND ISnull(#TempInventory.AssetNo,'') = ISnull(A.AssetNo,'')
--				AND ISnull(#TempInventory.ExpiryDate,'') =ISnull(A.ExpiryDate,'') 
--				AND ISnull(#TempInventory.Allocation,'') = ISnull(A.Allocation,'')
--
--			-- Update Closing Stock into temp	
--			Update #TempInventory SET CloseStock = OpenStock + QuantityRec - QuantityDis
--
--			-- Select data from Temp Table
--			--SELECT ROW_NUMBER() OVER (ORDER BY [Name]) SrNo, * FROM #TempInventory GROUP BY LotNo,Allocation ORDER BY [Name]
--			SELECT ROW_NUMBER() OVER (ORDER BY [Name]) SrNo,
--			[Name],LotNO,AssetNo,ExpiryDate,Allocation,
--			Sum(QuantityOpen) as QuantityOpen,sum(Dispached) as Dispached,sum(OpenStock) as OpenStock,
--			 sum(QuantityRec) as QuantityRec,sum(QuantityDis) as QuantityDis,sum(CloseStock) as CloseStock
--			FROM #TempInventory 
--			where (CloseStock <>0 or MustShow=1) 
--			Group By [Name],LotNO,AssetNO,ExpiryDate,Allocation,QuantityOpen,Dispached,OpenStock,
--			 QuantityRec,QuantityDis,CloseStock
--			ORDER BY [Name]
--			Drop table #TempInventory
GO
/****** Object:  StoredProcedure [dbo].[S_Get_Location_By_Name]    Script Date: 06/24/2013 15:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 10th June 2008
-- Description:	To GET ID_Location by Laction NAme
-- =============================================

create PROCEDURE [dbo].[S_Get_Location_By_Name]
	@Name varchar(50)
AS
	SET NOCOUNT ON;
SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM       Locations
WHERE	   [Name]=@Name
		   AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Update_Location]    Script Date: 06/24/2013 15:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 12 Jan. 2009
-- Description:	To UPDATE a Location
-- =============================================

CREATE PROCEDURE [dbo].[Update_Location]
(
	@TagID varchar(50),
	@Name varchar(50),
	@LocationNo varchar(50),
	@Description varchar(500),
	@ID_LocationGroup bigint,
	@Date_Created datetime, -- Previously Added, So can't be able to remove.
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_Location bigint
)
AS
	SET NOCOUNT OFF;

	UPDATE [Locations] 
	SET [TagID] = @TagID, [Name] = @Name, [LocationNo] = @LocationNo, [Description] = @Description, [ID_LocationGroup] = @ID_LocationGroup, [Date_Modified] = getDate(), [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By
	WHERE (([ID_Location] = @ID_Location));

	
	SELECT ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Locations WHERE (ID_Location = @ID_Location)
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_Locations_All]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 11 August 2008
-- Description:	To GET all Locations
-- =============================================

create PROCEDURE [dbo].[MR_Get_Locations_All]
AS
	SET NOCOUNT ON;

SELECT     -1 as ID_Location,'tagID', '--- Select --- ' as Name, 'LocationNo' as LocationNo, 'Description' as Description, 0 as ID_LocationGroup, 
			getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Asset_Types

UNION

SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Locations
WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_ReaderSettings]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
 
-- Create date: 02 02 2010
-- Description:	To get reader settings 
-- exec [Get_ReaderSettings]
-- =============================================

CREATE PROCEDURE [dbo].[Get_ReaderSettings]
@ID_Reader bigint = 0,
@Option int = 1
AS
SET NOCOUNT ON;
IF @Option = 1 --AND @ID_Reader <> 0
BEGIN
	Select distinct RDR.Id_Reader,RDR.IPAddress,
		RR.ID_TagType,RR.RSSI_Min,RR.RSSI_Max,
		LocIn.ID_Location as ID_LocationIn,LocIn.Name as LocationIn,
		LocOut.ID_Location as ID_LocationOut,LocOut.Name as LocationOut
		from Readers RDR
		LEFT OUTER JOIN RSSI_Range RR
		ON RDR.ID_Reader = RR.ID_Reader
		LEFT OUTER JOIN Locations LocIn
		ON RDR.ID_Location = LocIn.Id_Location
		LEFT OUTER JOIN Locations LocOut
		ON RDR.ID_LocationOut = LocOut.Id_Location
		where (RR.AntennaNo = '-1' OR RR.AntennaNo is Null) AND RDR.ID_Location <> -1 AND RDR.ID_LocationOut <> -1
END

--------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Location_By_TAGID]    Script Date: 06/24/2013 15:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 6th Nov 2008
-- Description:	To GET ID_Location by TAGID
-- =============================================

create PROCEDURE [dbo].[Get_Location_By_TAGID]
	@TagID varchar(50)
AS
	SET NOCOUNT ON;
SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM       Locations
WHERE	   [TagID]=@TagID
		   AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  View [dbo].[Vw_SearchAssets]    Script Date: 06/24/2013 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[Vw_SearchAssets]
AS
SELECT     dbo.Assets.ID_Asset, dbo.Assets.TagID, dbo.Assets.Name, dbo.Assets.AssetNo, dbo.Assets.Description, dbo.Assets.ImageName, 
                      CONVERT(varchar(20), dbo.Assets.PurchaseDate, 103) AS PurchaseDate, dbo.Assets.ID_Location, dbo.Assets.ID_AssetType, 
                      dbo.Assets.ID_AssetStatus, dbo.Assets.ID_AssetGroup, dbo.Assets.ID_AssetComment, dbo.Asset_Groups.ID_AssetGroup AS AssetGroupID, 
                      dbo.Asset_Groups.Name AS AssetGroupName, dbo.Asset_Groups.ParentID, dbo.Asset_Status.ID_AssetStatus AS AssetStatusID, 
                      dbo.Asset_Status.Status, dbo.Asset_Status.Description AS AssetStatusDescription, dbo.Asset_Types.ID_AssetType AS AssetTypeID, 
                      dbo.Asset_Types.Type, dbo.Locations.ID_Location AS LocationID, dbo.Locations.TagID AS LocationTagID, dbo.Locations.Name AS LocationName, 
                      dbo.Locations.LocationNo, dbo.Locations.Description AS LocationDescription, dbo.Locations.ID_LocationGroup, 
                      dbo.Asset_Comments.ID_AssetComment AS AssetCommentID, dbo.Asset_Comments.Comment, CONVERT(varchar(20), dbo.Assets.Date_Created, 103) 
                      AS Date_Created, CONVERT(varchar(20), dbo.Assets.Date_Modified, 103) AS Date_Modified, dbo.Assets.Is_Deleted, dbo.Assets.Is_Active, 
                      dbo.Assets.Last_Modified_By,dbo.Assets.ReferenceNo,PartCode
FROM  dbo.Asset_Comments 
INNER JOIN dbo.Assets ON dbo.Asset_Comments.ID_AssetComment = dbo.Assets.ID_AssetComment 
INNER JOIN dbo.Asset_Types ON dbo.Assets.ID_AssetType = dbo.Asset_Types.ID_AssetType 
INNER JOIN dbo.Asset_Groups ON dbo.Assets.ID_AssetGroup = dbo.Asset_Groups.ID_AssetGroup
INNER JOIN dbo.Asset_Status ON dbo.Assets.ID_AssetStatus = dbo.Asset_Status.ID_AssetStatus 
INNER JOIN dbo.Locations ON dbo.Assets.ID_Location = dbo.Locations.ID_Location
WHERE 
     (dbo.Assets.Is_Deleted = 0) AND (dbo.Assets.Is_Active = 1) AND (dbo.Asset_Groups.Is_Deleted = 0) AND (dbo.Asset_Groups.Is_Active = 1) AND 
     (dbo.Asset_Status.Is_Deleted = 0) AND (dbo.Asset_Status.Is_Active = 1) AND (dbo.Asset_Types.Is_Deleted = 0) AND (dbo.Asset_Types.Is_Active = 1) 
     AND (dbo.Locations.Is_Deleted = 0) AND (dbo.Locations.Is_Active = 1) AND (dbo.Asset_Comments.Is_Deleted = 0) AND 
     (dbo.Asset_Comments.Is_Active = 1)
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[34] 4[12] 2[37] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = -96
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Asset_Comments"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 114
               Right = 207
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Assets"
            Begin Extent = 
               Top = 1
               Left = 542
               Bottom = 278
               Right = 711
            End
            DisplayFlags = 280
            TopColumn = 2
         End
         Begin Table = "Asset_Types"
            Begin Extent = 
               Top = 114
               Left = 38
               Bottom = 222
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Asset_Groups"
            Begin Extent = 
               Top = 114
               Left = 240
               Bottom = 222
               Right = 404
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Asset_Status"
            Begin Extent = 
               Top = 222
               Left = 38
               Bottom = 330
               Right = 202
            End
            DisplayFlags = 280
            TopColumn = 0
         End
         Begin Table = "Locations"
            Begin Extent = 
               Top = 222
               Left = 240
               Bottom = 330
               Right = 406
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
      Begin ColumnWidths = 9
         Width = 284
         Width = 1500
     ' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchAssets'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane2', @value=N'    Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
         Width = 1500
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchAssets'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=2 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'Vw_SearchAssets'
GO
/****** Object:  StoredProcedure [dbo].[Sync_InventoryCheck]    Script Date: 06/24/2013 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <25 June 2008>
-- Description:	<Check Inventory Form SQL CE>
-- =============================================
CREATE PROCEDURE [dbo].[Sync_InventoryCheck] --'00000000000000000456222',1
	-- Add the parameters for the stored procedure here
(
@TagID  varchar(50),
@ID_Location bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



    -- Insert statements for procedure here

IF EXISTS (SELECT ID_Asset FROM dbo.Assets WHERE TagID = @TagID)
	BEGIN
		 IF EXISTS( SELECT ID_Asset FROM dbo.Assets WHERE TagID = @TagID AND ID_Location = @ID_Location)
			BEGIN
				SELECT A.ID_Location as ID,L.Name as Location FROM dbo.Assets A
				INNER JOIN Locations  L  ON L.ID_Location = A.ID_Location 
				WHERE A.TagID = @TagID --------> Synchronised
			END
		 ELSE
			BEGIN
				SELECT A.ID_Location as ID,L.Name as Location FROM dbo.Assets A
				INNER JOIN Locations  L  ON L.ID_Location = A.ID_Location 
				WHERE A.TagID = @TagID --------> Location Mismatch
			END
	END
ELSE
	BEGIN 
			SELECT cast(-1 as bigint) AS ID , '' as Location --------> Tag Id Not Found
	END	

END
GO
/****** Object:  StoredProcedure [dbo].[Sync_GetTags_By_Location]    Script Date: 06/24/2013 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <03 July 2008>
-- Description:	<Get All Tags at Given Location>
-- =============================================
CREATE PROCEDURE [dbo].[Sync_GetTags_By_Location] 
	-- Add the parameters for the stored procedure here
(
@ID_Location BIGINT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT A.TagID,A.ID_Location , L.[Name] ,A.Name as AssetName FROM ASSETS  A
	INNER JOIN Locations  L  ON
	L.ID_Location = A.ID_Location 
	WHERE A.ID_Location = @ID_Location
END
GO
/****** Object:  StoredProcedure [dbo].[Get_Locations_By_ID_Location]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry JAin
-- Create date: 8 Aug 2008
-- Description:	To GET  Locations
-- =============================================

Create PROCEDURE [dbo].[Get_Locations_By_ID_Location]
(
@ID_Location BIGINT
)
AS
	SET NOCOUNT ON;


SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Locations
WHERE	Is_Deleted=0 AND Is_Active=1 and ID_Location = @ID_Location
GO
/****** Object:  StoredProcedure [dbo].[Get_Locations_All]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Locations
-- =============================================

CREATE PROCEDURE [dbo].[Get_Locations_All]
@allORselect int = 0
AS
	SET NOCOUNT ON;

IF @allORselect = 0
BEGIN
SELECT     -1 as ID_Location,'tagID', '--- All --- ' as Name, 'LocationNo' as LocationNo, 'Description' as Description, 0 as ID_LocationGroup, 
			getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Asset_Types

UNION

SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Locations
WHERE	Is_Deleted=0 AND Is_Active=1 and tagid not in ('0000000000000000000000D')
END
ELSE
BEGIN
SELECT     0 as ID_Location,'tagID', '--- Select --- ' as Name, 'LocationNo' as LocationNo, 'Description' as Description, 0 as ID_LocationGroup, 
			getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Asset_Types

UNION

SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Locations
WHERE	Is_Deleted=0 AND Is_Active=1 and tagid not in ('0000000000000000000000D')
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_Location]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Location
-- Modified by Vijay : - To also change the TagID with dummy value on deleting the Location
-- Modified on : - 11-11-11
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Location]
(
	@ID_Location bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Locations] WHERE (([ID_Location] = @ID_Location))

UPDATE [Locations] SET Is_Deleted=1, Date_Modified=getdate(),TagID = 'xxxxxxxxxxxxxxxxxxxxxxxx' 
WHERE (([ID_Location] = @ID_Location))

--------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Locations]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Locations
-- =============================================

CREATE PROCEDURE [dbo].[Get_Locations]
AS
	SET NOCOUNT ON;
--
--SELECT     -1 as ID_Location,'tagID', '---Select--- ' as Name, 'LocationNo' as LocationNo, 'Description' as Description, 0 as ID_LocationGroup, 
--			getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Asset_Types
--
--UNION

SELECT     ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Locations
WHERE	Is_Deleted=0 AND Is_Active=1 and tagid not in ('0000000000000000000000D')


--select * from locations where name like '%disp%'
--update locations set tagid='0000000000000000000000D' where name like '%disp%'

--------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Check_AssetNoAndLocation]    Script Date: 06/24/2013 15:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal	
-- Create date: 06-June-2009
-- Description:	To CHECK for Duplicate Asset Number and Location present or not
-- =============================================

CREATE PROCEDURE [dbo].[Check_AssetNoAndLocation]  --'Tag-0011187'
	-- Add the parameters for the stored procedure here
	@AssetNo varchar(50),
	@Location varchar(50)
AS
BEGIN
	
	-- Exec [Check_AssetNoAndLocation] 'Laptop 6','189'	
/*	if EXISTS(select 1 From Assets where AssetNo = @AssetNo)
		select 'A'	
	ELSE 
*/	if NOT EXISTS(select ID_Location From locations  where [Name] = @Location)
		select 'B'
	Begin
		select ID_Location From locations  where [Name] = @Location	
	End
END
GO
/****** Object:  StoredProcedure [dbo].[Sync_GetFSStatus_By_Location]    Script Date: 06/24/2013 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Deepanshu Jouhari>
-- Create date: <20 July 2009>
-- Description:	<Get All FSTemplate at Given Location>
-- Exec [dbo].[Sync_GetFSStatus_By_Location] 188, '1010,1020' 
-- =============================================
CREATE PROCEDURE [dbo].[Sync_GetFSStatus_By_Location] 
	-- Add the parameters for the stored procedure here
(
@ID_Location BIGINT,
@StrFSStatus nvarchar(2000)
)
AS
BEGIN
	DECLARE @SQL NVARCHAR(4000)
	SET NOCOUNT ON;

	IF (@ID_Location > 0)
		BEGIN
		---Make Expired if task is not performed and Other tasks of same template is due.
		Update FS_AssetStatus 
		set FSStatus=1040 
		where DueDate < getDate() 
		and ID_FSAssetStatus in (Select F.ID_FSAssetStatus  
		from FS_AssetStatus F 
		INNER JOIN Assets A  ON	A.ID_Asset = F.ID_Asset
		INNER JOIN Locations  L  ON	L.ID_Location = A.ID_Location 
		WHERE A.ID_Location = @ID_Location AND F.DueDate >= getDate()
		and F.Is_Active = 1 and F.IS_Deleted != 0 and F.ID_Template=ID_Template)  
		
		--Get Pending task now	
		SET @SQL = 'SELECT F.ID_FSAssetStatus as ServerKey, F.ID_Tasks, F.ID_Asset, F.ID_Template, F.DueDate, F.TaskStatus, F.FSStatus, F.ID_Employee, F.Date_Modified,F.Last_Modified_By AS ModifiedBy, Comments 
		, F.IS_Active,F.IS_Deleted 
		FROM FS_AssetStatus F 
		INNER JOIN Assets A  ON	A.ID_Asset = F.ID_Asset
		INNER JOIN Locations  L  ON	L.ID_Location = A.ID_Location 
		WHERE A.ID_Location = ' + cast(@ID_Location as nvarchar) +' and F.FSStatus in (' + @StrFSStatus +')';
		END
	ELSE --Working for ALL Location
		BEGIN
			Update FS_AssetStatus 
			set FSStatus=1040 
			where DueDate < getDate() 
			and ID_FSAssetStatus in (Select F.ID_FSAssetStatus  
			from FS_AssetStatus F 
			WHERE F.DueDate >= getDate()
			and F.Is_Active = 1 and F.IS_Deleted != 0 and F.ID_Template=ID_Template)  
			
			--Get Pending task now	
			SET @SQL = 'SELECT F.ID_FSAssetStatus as ServerKey, F.ID_Tasks, F.ID_Asset, F.ID_Template, F.DueDate, F.TaskStatus, F.FSStatus, F.ID_Employee, F.Date_Modified,F.Last_Modified_By AS ModifiedBy, Comments 
			, F.IS_Active,F.IS_Deleted 
			FROM FS_AssetStatus F 
			WHERE  F.FSStatus in (' + @StrFSStatus +')';
		END
	Print @SQL
	EXEC SP_EXECUTESQL @SQL
END
/*
SELECT F.ID_FSAssetStatus as ServerKey, F.ID_Tasks, F.ID_Asset, F.ID_Template, F.DueDate,
 F.TaskStatus, F.FSStatus, F.ID_Employee, F.Date_Modified,F.Last_Modified_By AS ModifiedBy,
 Comments FROM FS_AssetStatus F 
	INNER JOIN Assets A  ON	A.ID_Asset = F.ID_Asset
	INNER JOIN Locations  L  ON	L.ID_Location = A.ID_Location 
	WHERE A.ID_Location = 188 and F.IS_Active=1 
    and F.IS_Deleted=0 and F.FSStatus in (1010,1020)

*/
--select * from FS_AssetStatus
/*
SELECT F.ID_FSAssetStatus as ServerKey, F.ID_Tasks, F.ID_Asset, F.ID_Template, F.DueDate, 
F.TaskStatus, F.FSStatus, F.ID_Employee, F.Date_Modified,F.Last_Modified_By AS ModifiedBy, Comments, F.IS_Active,F.IS_Deleted 
			FROM FS_AssetStatus F 
			WHERE  F.FSStatus in  (1010,1020)

select G.ID_AssetGroup,
G.Name from Asset_Groups G,
 Assets A, FS_AssetStatus FS  
where G.ParentID =0 and FS.ID_Asset=A.ID_Asset
and (FS.FSStatus=1010 or FS.FSStatus=1020) 
and ( A.ID_AssetGroup=G.ID_AssetGroup)


select * from Assets where id_Asset=555

*/
GO
/****** Object:  StoredProcedure [dbo].[Get_Location_By_ID]    Script Date: 06/24/2013 15:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 13 Jan 2009
-- Description:	To GET a Location by ID_Location
-- =============================================

CREATE PROCEDURE [dbo].[Get_Location_By_ID]
	@ID_Location bigint
AS

SET NOCOUNT ON;
SELECT l.ID_Location, l.TagID, l.Name, l.LocationNo, l.Description, isNULL(l.ID_LocationGroup,0) as ID_LocationGroup , l.Date_Created, 
	l.Date_Modified, l.Is_Deleted, l.Is_Active, l.Last_Modified_By, lg.Name as LocationGroup
FROM  Locations l
Left outer join location_Groups lg on l.id_locationGroup = lg.id_locationGroup
WHERE  l.Is_Deleted=0 AND l.Is_Active=1 and l.ID_Location=@ID_Location
GO
/****** Object:  StoredProcedure [dbo].[Get_Location_By_LocationNo]    Script Date: 06/24/2013 15:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 6th Nov 2008
-- Description:	To GET ID_Location by TAGID
-- =============================================

CREATE PROCEDURE [dbo].[Get_Location_By_LocationNo]
	@LocationNo varchar(50)
AS
	SET NOCOUNT ON;
SELECT     *
FROM       Locations
WHERE	   [LocationNo]=@LocationNo
		   AND Is_Deleted=0 AND Is_Active=1

/*
ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
*/
GO
/****** Object:  StoredProcedure [dbo].[Search_Assets]    Script Date: 06/24/2013 15:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================
-- Author:		Asif Ali
-- Modified		Deepanshu Jouhari
-- Create date: 21th March 2008
-- Description:	To Search all Assets by NAME, AssetNo., TYPE, LOCATION, GROUP, STATUS
-- Modified On -- 20/06/2012 
-- ======================================================================================


CREATE PROCEDURE [dbo].[Search_Assets] --'', 39
	@SearchCriteria NVARCHAR(1000),
	@ID_AssetGroup bigint,
	@ID_Location bigint = 0
AS
BEGIN
	SET NOCOUNT ON;

--.............................................................................
DECLARE @PARENTID BIGINT, @SUBID VARCHAR(5000),
 @SUBSUBID VARCHAR(5000),
@SQL NVARCHAR(4000)
--SET @ID_AssetGroup =3
SET @PARENTID = 0
SET @SUBID = ltrim(str(@ID_AssetGroup)) --+ ','
SET @SUBSUBID = ''
SET @SQL = ''

--if (@ID_AssetGroup <> -1)
if (@ID_AssetGroup > 0)
BEGIN

--SELECT @PARENTID = ParentID FROM Asset_Groups WHERE ID_AssetGroup = @ID_AssetGroup
--PRINT @PARENTID
--
--if (@PARENTID > 0)
--BEGIN
	SELECT @SUBID=@SUBID + ',' +CONVERT(VARCHAR(5),(ISNULL(ID_AssetGroup,''))) FROM [Asset_Groups] WHERE ParentID = @ID_AssetGroup
	SET @SUBSUBID = @SUBID
	--SET @SUBID= SUBSTRING(@SUBID,0,LEN(@SUBID))
	PRINT @SUBID
--END

IF (LEN(@SUBID)>0)
	SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID + '',''+CONVERT(VARCHAR(5),ID_AssetGroup) FROM [Asset_Groups] WHERE ParentID IN ('+ @SUBID + ')'
ELSE
	SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+ '','' +CONVERT(VARCHAR(5),ID_AssetGroup) FROM [Asset_Groups] WHERE  ID_AssetGroup = ' + LTRIM(STR(@ID_AssetGroup))

PRINT @SQL
EXEC sp_executesql 
			@query = @SQL, 
			@params = N'@SUBSUBID VARCHAR(5000) OUTPUT', 
			@SUBSUBID = @SUBSUBID OUTPUT 


--SET @SUBSUBID= SUBSTRING(@SUBSUBID,0,LEN(@SUBSUBID))
PRINT @SUBSUBID

end

--else
--
--begin
--end
--
--SET @SQL='SELECT   ID_Asset,AssetName,Description, AssetNo, DateMoved, Last_Modified_By, EmployeeName,LocationName,LocationNo, 
--		 ID_Location, ID_AssetType, Type, [ID_AssetGroup],GroupName
--FROM     Vw_Asset_Movement WHERE [ID_AssetGroup] IN ('+ @SUBSUBID +')'
--
--
--PRINT @SQL
--EXEC sp_executesql @Sql


--.............................................................................
--if (Len(@SearchCriteria)>0)
--	set @SearchCriteria = ' WHERE ' + @SearchCriteria
 
--
--	DECLARE @SQL NVARCHAR(4000)

If (@ID_Location > 0)
BEGIN
	Update Locations Set UsageCount = UsageCount +1 Where ID_Location = @ID_Location -- To Update Location Usage Count field ( 20/06/2012 )
END


if (Len(@SearchCriteria)>0)
	set @SearchCriteria = ' AND ' + @SearchCriteria

	SET @SQL = 'SELECT	ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_Location, ID_AssetType, ID_AssetStatus,
						ID_AssetGroup, ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,
						LocationName, Type, Status, AssetGroupName, Comment, isNULL(ReferenceNo,'''') as ReferenceNo,PartCode
				
				FROM    Vw_SearchAssets  WHERE 1=1 ' + @SearchCriteria 


--if (@ID_AssetGroup<>-1)

if (@ID_AssetGroup > 0)

SET @SQL = @SQL + ' AND [ID_AssetGroup] IN ('+ @SUBSUBID +')'


	--PRINT @SQL
	set dateformat dmy 
	EXEC SP_EXECUTESQL @SQL

	

END
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset_Location_by_HH]    Script Date: 06/24/2013 15:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 28th May 2008, 30 July 2008, 1 Jan 2009
-- Description:	To UPDATE a Asset
-- Modified By - Vijay
-- Modified On - 01/07/2011
-- Modified On -- 20/06/2012 
-- =============================================

CREATE PROCEDURE [dbo].[Update_Asset_Location_by_HH] 
--'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(MAX),
	@ID_Location bigint,
	@Last_Modified_By bigint,
	@ID_Reader bigint,
	@ReferenceNo nvarchar(150)='',
	@ID_Reason bigint=0
)
AS
	SET NOCOUNT OFF;
Declare @OldLocation bigint
set @OldLocation = 0

declare @sqlstr1 varchar(max);

--Select @OldLocation = isNULL(ID_Location,-1) from  [Assets] where TagID = @TAGID
--
--print 'Select Location';
--
----print @TAGID + cast(@OldLocation as varchar) + ' ' +  cast(@ID_Location as varchar); 
--if @OldLocation <> @ID_Location
Begin

print 'Insert Update inventory';

-- Delete any exiting assets for given location
set @sqlstr1 = 'delete from Inventory_History where ID_Asset in (select ID_Asset from Assets where TagID in ('+@TagID+') or ID_Location ='+ Cast(@ID_Location as varchar(5))+')'

exec(@sqlstr1)

-- Insert asset 
set @sqlstr1 = 'Insert into Inventory_History (ID_Asset,InventoryDate,InventoryLocation,InventoryStatus,InventoryBy) '+ 
	 'select ID_Asset, getdate(),'+Cast(@ID_Location as varchar(5))+', 
								case when ID_Location != '+Cast(@ID_Location as varchar(5))+' then (select ID_InventoryStatus from Inventory_Status where InventoryStatusName = ''Moved'') 
									when ID_Location = '+Cast(@ID_Location as varchar(5))+' and TagID in ('+@TagID+') then (select ID_InventoryStatus from Inventory_Status  where InventoryStatusName = ''Present'') 
									else (select ID_InventoryStatus from Inventory_Status where InventoryStatusName = ''Missing'') 
								end,
			'+ Cast(@Last_Modified_By as varchar(5)) +'
	from Assets
	where TagID in ('+@TagID+') or ID_Location = '+Cast(@ID_Location as varchar(5))

exec(@sqlstr1)
--------------------------------------
print 'Insert Into Asset Movement';
	SET @sqlstr1 =
			'INSERT INTO [Asset_Movement]
           ([ID_Asset]
           ,[ID_Location]
           ,[Date_Created]
           ,[Date_Modified]
           ,[Is_Deleted]
           ,[Is_Active]
           ,[Last_Modified_By]
           ,[ID_Reader]
           ,[ID_AssetStatus]
		   ,[ID_Employee]
		   ,[ID_TagType]
		   ,[ID_Alert]
			,[ReferenceNo]
			,[ID_LocationNew]
			,[IS_VernonRead]
			,[ID_Reason])
			Select 
			[ID_Asset]
			,[ID_Location]
			,getdate()
			,[Date_Modified]
			,[Is_Deleted]
			,[Is_Active]
			,' + Cast(@Last_Modified_By as varchar(5)) + ' as Last_Modified_By
			,[ID_Reader]
			,[ID_AssetStatus]
			,' + Cast(@Last_Modified_By as varchar(5)) + ' as ID_Employee
			,[ID_TagType]	
			,[ID_Alert]
			,[ReferenceNo]
			,'+ Cast(@ID_Location as varchar(5)) + ' as ID_LocationNew
			,0 as IS_VernonRead
			,'+ Cast(@ID_Reason as varchar(5)) + '  as ID_Reason	
			From [dbo].[Assets] 
			where ID_Location <> ' + Cast(@ID_Location as varchar(5)) + ' AND Is_Active = 1 AND Is_Deleted = 0 AND TAGID IN (' + @TagID + ')'
End 

--print @sqlstr1;

execute(@sqlstr1);

Declare @sqlstr varchar(max);

print 'Update Asset';

set dateformat dmy   
if(@ID_Reader <> 0)
	BEGIN		
		SET @sqlstr = '
			UPDATE [Assets]
			SET [ID_Location] = ' + Cast(@ID_Location as varchar(5)) + ',
			ID_Employee= ' + cast(@Last_Modified_By as varchar(5)) +',
			[Date_Modified] = getdate(),		
			ReferenceNo= ''' + @ReferenceNo + '''
			WHERE Is_Active = 1 AND Is_Deleted = 0 AND TagID IN (' + @TagID + ')'

	END
ELSE
	BEGIN	

		--,ReferenceNo= ''' + @ReferenceNo + '''
		
			SET @sqlstr = '
			UPDATE [Assets]
			SET [ID_Location] = ' + Cast(@ID_Location as varchar(5)) + ',
			ID_Employee= ' + cast(@Last_Modified_By as varchar(5)) +',
			[Date_Modified] = getdate()  	
			WHERE Is_Active = 1 AND Is_Deleted = 0 AND TagID IN (' + @TagID + ')'
	
	END
	
--print @sqlstr

execute(@sqlstr);

print 'Update Asset Completed';

Update Locations Set UsageCount = UsageCount +1 Where ID_Location = @ID_Location AND TagID <> '0000000000000000000000D'-- To Update Location Usage Count field ( 20/06/2012 )
GO
/****** Object:  StoredProcedure [dbo].[Add_Alert1]    Script Date: 06/24/2013 15:03:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Alert1]
(
	@Original_ID_Alert bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Alert] WHERE (([ID_Alert] = @Original_ID_Alert))
GO
/****** Object:  StoredProcedure [dbo].[MR_Update_AlertBy_Asset_AssetMovement]    Script Date: 06/24/2013 15:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Deepanshu July>
-- Create date: <13 August 2008>
-- Description:	<Update Alert>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Update_AlertBy_Asset_AssetMovement] 
	-- Add the parameters for the stored procedure here
(
@ID_Alert bigint,
@ID_Employee bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
begin try
	begin transaction
		Update dbo.Alert set AcknowledgedBy=@ID_Employee where ID_Alert=@ID_Alert
		Update dbo.Assets set Last_Modified_By=@ID_Employee, ID_Employee=@ID_Employee where ID_Alert=@ID_Alert
		Update dbo.Asset_Movement set Last_Modified_By=@ID_Employee, ID_Employee=@ID_Employee where ID_Alert=@ID_Alert
	commit transaction
end try
begin catch
	Rollback Transaction
end catch


END
GO
/****** Object:  StoredProcedure [dbo].[Add_Alert]    Script Date: 06/24/2013 15:03:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Add_Alert]
(
	@ID_Asset bigint,
	@ID_AssetStatus bigint,
	@ID_Employee bigint,
	@ID_Reader bigint,
	@ID_LocationIn bigint,
	@ID_LocationOut bigint,
	@EmployeeID varchar(50),
	@AcknowledgedBy bigint,
	@ID_SentMail bigint,
	@ID_SentSMS bigint,
	@AlertOccured datetime,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Alert] ([ID_Asset], [ID_AssetStatus], [ID_Employee], [ID_Reader], [ID_LocationIn], [ID_LocationOut], [EmployeeID], [AcknowledgedBy], [ID_SentMail], [ID_SentSMS], [AlertOccured], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) 
VALUES (@ID_Asset, @ID_AssetStatus, @ID_Employee, @ID_Reader, @ID_LocationIn, @ID_LocationOut, @EmployeeID, @AcknowledgedBy, @ID_SentMail, @ID_SentSMS, @AlertOccured, GETDATE(), GETDATE(), 0, 1, @Last_Modified_By);
	
---->UPDATE ASSET TO ADD ID_ALERT
DECLARE @ID_ALERT BIGINT
SET @ID_ALERT = SCOPE_IDENTITY();

--Print '@ID_ALERT set';

UPDATE dbo.ASSETS SET ID_Alert = @ID_ALERT WHERE ID_Asset = @ID_Asset;
Print 'Asset Update with AlertID';
-------END UPDATE


SELECT ID_Alert, ID_Asset, ID_AssetStatus, ID_Employee, ID_Reader, ID_LocationIn, ID_LocationOut, EmployeeID, AcknowledgedBy, ID_SentMail, ID_SentSMS, AlertOccured, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By 
FROM Alert WHERE (ID_Alert = @ID_ALERT);
GO
/****** Object:  StoredProcedure [dbo].[Get_Alert]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Alert]
AS
	SET NOCOUNT ON;
SELECT     Alert.*
FROM         Alert
GO
/****** Object:  StoredProcedure [dbo].[LOG_Tags]    Script Date: 06/24/2013 15:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 31th July 2008
-- Description:	To Maintain Complete log of Tag Movement in System.
-- =============================================

Create PROCEDURE [dbo].[LOG_Tags]
(
	@TagID varchar(30),
	@CapturePoint varchar(200),
    @CaptureTime dateTime,
	@EventName varchar(250),
	@RSSI real,
	@Reader varchar(250)
)
AS
begin
SET NOCOUNT OFF;
begin try
	begin transaction;
	

	--insert into history
	insert into histories (ID, CapturePoint,[Time],[Event],RSSI,Reader) 
	values (@TagID,@CapturePoint,@CaptureTime,@EventName,@RSSI,@Reader);
	
	--Clear Tags Database		
	delete from Tags where ID=@TagID;
	delete from Tags where Reader=@Reader;

	--Clear History database
	delete from histories where ((select Count(*) from histories where id=@TagID)>300)
	 and datediff(m,@CaptureTime,[Time])<-1

	--insert into Tags
	insert into tags (ID, CapturePoint, CaptureTime,Event,RSSI,Reader)
	values (@TagID,@CapturePoint,@CaptureTime,@EventName,@RSSI,@Reader);
	
	commit transaction;
end try
begin catch
	print 'Log not saved';
	rollback transaction;
end catch
end
GO
/****** Object:  StoredProcedure [dbo].[Add_Tags]    Script Date: 06/24/2013 15:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 11 Aug 2008
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_Tags] 
	-- Add the parameters for the stored procedure here
	@TagOrigId varchar(30),
	@CapturePointId varchar(200),
	@CaptureTime datetime,
	@Event varchar(250),
	@RSSI real,
	@Reader varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into tags (ID, CapturePoint, CaptureTime,[Event],RSSI,Reader)
    values (@TagOrigId,@CapturePointId,@CaptureTime,@Event,@RSSI,@Reader)
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_Tags]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 11 Aug 2008
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Delete_Tags] 
	-- Add the parameters for the stored procedure here
	@Reader varchar(250)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	
	delete from tags where Reader= @Reader or rTrim(@Reader)='-1'
END
GO
/****** Object:  StoredProcedure [dbo].[DeleteLocationGroup]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 Sept 2011
-- Description:	To Delete a Location Group
-- =============================================

CREATE PROCEDURE [dbo].[DeleteLocationGroup]
(
	@ID_LocationGroup BigInt, 
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

 
		UPDATE  [Location_Groups] SET      [Date_Modified] = GetDate(), 
										[Is_Deleted] = 1, 
										[Is_Active] =0, 
										[Last_Modified_By] = @Last_Modified_By
					WHERE ID_LocationGroup =@ID_LocationGroup
		RETURN 1;
 

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[AddLocationGroup]    Script Date: 06/24/2013 15:04:08 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 Sept 2011
-- Description:	To ADD a Location Group
-- =============================================

CREATE PROCEDURE [dbo].[AddLocationGroup]
(
	@Name varchar(50),
	@ParentID bigint = 0, 
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(SELECT ID_LocationGroup FROM Location_Groups WHERE [Name] = @Name AND ParentID = @ParentID)
	BEGIN
		INSERT INTO Location_Groups ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, GetDate(), GetDate(), 0, 1, @Last_Modified_By);
		RETURN SCOPE_IDENTITY()
	END
ELSE
	BEGIN
		RETURN -1;
	END

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[UpdateLocationGroup]    Script Date: 06/24/2013 15:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 Sept 2011
-- Description:	To update location Group
-- =============================================

CREATE PROCEDURE [dbo].[UpdateLocationGroup]
(
	@ID_LocationGroup BigInt,
	@Name varchar(50),
	@ParentID bigint = 0, 
	@Is_Deleted Bit = 0,
	@Is_Active Bit =1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(SELECT ID_LocationGroup FROM Location_Groups WHERE [Name] = @Name AND ParentID = @ParentID)
	BEGIN
		UPDATE  Location_Groups SET [Name] = @Name, 
										[ParentID] = @ParentID,
									    [Date_Modified] = GetDate(), 
										[Is_Deleted] = @Is_Deleted, 
										[Is_Active] =@Is_Active, 
										[Last_Modified_By] = @Last_Modified_By
					WHERE ID_LocationGroup =@ID_LocationGroup
		RETURN 1;
	END
ELSE
	BEGIN
		RETURN -1;
	END

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Delete_LocationGroup]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a LocationGroup
-- =============================================


CREATE PROCEDURE [dbo].[Delete_LocationGroup]
(
	@ID_LocationGroup bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Location_Groups] WHERE (([ID_LocationGroup] = @ID_LocationGroup))

UPDATE [Location_Groups] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_LocationGroup] = @ID_LocationGroup))
GO
/****** Object:  StoredProcedure [dbo].[Update_LocationGroup]    Script Date: 06/24/2013 15:04:49 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a LocationGroup
-- =============================================

CREATE PROCEDURE [dbo].[Update_LocationGroup]
(
	@Name varchar(50),
	@ParentID bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_LocationGroup bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Location_Groups] SET [Name] = @Name, [ParentID] = @ParentID, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_LocationGroup] = @ID_LocationGroup));
	
SELECT ID_LocationGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Location_Groups WHERE (ID_LocationGroup = @ID_LocationGroup)
GO
/****** Object:  StoredProcedure [dbo].[Add_LocationGroup]    Script Date: 06/24/2013 15:04:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Location Group
-- =============================================

CREATE PROCEDURE [dbo].[Add_LocationGroup]
(
	@Name varchar(50),
	@ParentID bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Location_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_LocationGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Location_Groups WHERE (ID_LocationGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Add_LocationGroupFromFile]    Script Date: 06/24/2013 15:04:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 26 OCT 2009
-- Description:	To ADD a Location Group FROM CSV File
-- =============================================

CREATE PROCEDURE [dbo].[Add_LocationGroupFromFile]
(
	@Name varchar(50),
	@ParentGroup varchar(50) = '',
	@ParentID bigint = null,
	@Date_Created datetime = null,
	@Date_Modified datetime = null,
	@Is_Deleted bit = 0,
	@Is_Active bit = 1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
set dateformat dmy   
IF @ParentGroup = ''
	SET @ParentID = 0
ELSE
BEGIN
--IF @ParentID is null
	BEGIN
		IF NOT EXISTS (Select Top 1 ID_LocationGroup FROM [Location_Groups] WHERE [Name] = @ParentGroup)
			BEGIN
				INSERT INTO [Location_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@ParentGroup, 0, Getdate(), getdate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
				SET @ParentID = SCOPE_IDENTITY()
			END
		ELSE
			BEGIN
				SET @ParentID = (SELECT Top 1 ID_LocationGroup FROM [Location_Groups] WHERE [Name] = @ParentGroup)
			END
	END
END

BEGIN
	IF NOT EXISTS (SELECT Top 1 ID_LocationGroup  FROM [Location_Groups] WHERE [Name] = @Name AND [ParentID] = @ParentID)
		BEGIN
			INSERT INTO [Location_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, getdate(), getdate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
			 SELECT @@IDENTITY
		END
	ELSE
			SELECT -1
END
GO
/****** Object:  StoredProcedure [dbo].[Vernon_GetLocationDefaultValues]    Script Date: 06/24/2013 15:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 07 Aug 2009
-- Description:	To ADD a Location from Vernon Locations File
-- =============================================

CREATE PROCEDURE [dbo].[Vernon_GetLocationDefaultValues]
	--@LocationGroup varchar(200)
AS
	SET NOCOUNT OFF;

	--Declare @ID_LocationGroup bigint

Begin         
        
	IF NOT Exists (Select 1 From Location_Groups where [Name] = 'VernonGroup')
	BEGIN
		INSERT INTO Location_Groups ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) 
							VALUES ('VernonGroup',0,GetDate(),Getdate(),0,1,0)
	END	

    Select ID_LocationGroup From Location_Groups where [Name] = 'VernonGroup'	
	
End
GO
/****** Object:  StoredProcedure [dbo].[Get_LocationGroups_All]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To GET all LocationGroups
-- =============================================

CREATE PROCEDURE [dbo].[Get_LocationGroups_All]
AS
	SET NOCOUNT ON;
select 0 as ID_LocationGroup,'--NONE--' as [Name],-1 as ParentID,getdate() as Date_Created,getDate() as Date_Modified,0 as Is_Deleted,1 as Is_Active,1 as Last_Modified_By
union
SELECT     ID_LocationGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Location_Groups

WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_LocationGroup_By_ID]    Script Date: 06/24/2013 15:04:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===============================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a LocationGroup by ID_LocationGroup
-- ===============================================================

CREATE PROCEDURE [dbo].[Get_LocationGroup_By_ID]
	@ID_LocationGroup bigint
AS
	SET NOCOUNT ON;
SELECT     ID_LocationGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Location_Groups
WHERE		ID_LocationGroup=@ID_LocationGroup
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Departments_All]    Script Date: 06/24/2013 15:04:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Departments
-- =============================================

CREATE PROCEDURE [dbo].[Get_Departments_All]
AS
	SET NOCOUNT ON;

SELECT     -1 as ID_Department, '--- All ---' as Name, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Master_Departments

UNION

SELECT     ID_Department, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Departments

WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Departments]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Departments
-- =============================================

CREATE PROCEDURE [dbo].[Get_Departments]
AS
	SET NOCOUNT ON;

--SELECT     -1 as ID_Department, '--- Select ---' as Name, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Master_Departments
--
--UNION

SELECT     ID_Department, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Departments

WHERE	Is_Deleted=0 AND Is_Active=1

--------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Department_By_ID]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a Department By ID_Department
-- ===================================================

CREATE PROCEDURE [dbo].[Get_Department_By_ID]
	@ID_Department bigint
AS
	SET NOCOUNT ON;
SELECT     ID_Department, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Departments
WHERE		ID_Department=@ID_Department
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Add_Department]    Script Date: 06/24/2013 15:03:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Department
-- =============================================

CREATE PROCEDURE [dbo].[Add_Department]
(
	@Name varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Master_Departments] ([Name], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_Department, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_Departments WHERE (ID_Department = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Delete_Department]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Deparment
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Department]
(
	@ID_Department bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Master_Departments] WHERE (([ID_Department] = @ID_Department))

UPDATE [Master_Departments] SET Is_Deleted=1, Date_Modified=getdate()
WHERE  (([ID_Department] = @ID_Department))
GO
/****** Object:  StoredProcedure [dbo].[Update_Department]    Script Date: 06/24/2013 15:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a Department
-- =============================================


CREATE PROCEDURE [dbo].[Update_Department]
(
	@Name varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_Department bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Master_Departments] SET [Name] = @Name, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_Department] = @ID_Department));
	
SELECT ID_Department, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_Departments WHERE (ID_Department = @ID_Department)
GO
/****** Object:  StoredProcedure [dbo].[Delete_Task]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To DELETE a Task
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Task]
(
	@ID_Tasks bigint
)
AS
	SET NOCOUNT OFF;
--select * from Tasks
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

	begin try
		begin transaction
		UPDATE [Tasks] SET Is_Deleted=1, Date_Modified=getdate()
		WHERE (([ID_Tasks] = @ID_Tasks))
		UPDATE FS_Tasks SET IS_Deleted = 1 Where ID_Tasks = @ID_Tasks
		UPDATE FS_AssetDetails SET IS_Deleted = 1 Where ID_Tasks = @ID_Tasks
		UPDATE FS_Assets SET IS_Deleted = 1 Where ID_FSAsset not in (select ID_FSAsset from FS_AssetDetails where Is_Deleted<>1)
		UPDATE FS_AssetStatus set IS_Deleted=1, Date_Modified=getdate()  
				where (([ID_Tasks] = @ID_Tasks)) and (FSStatus=1010 or FSStatus=1020)
		commit transaction
	end try
	begin catch
	 IF (XACT_STATE()) = -1
		BEGIN
			rollback transaction
		END;
	end catch
GO
/****** Object:  StoredProcedure [dbo].[Update_FieldServiceTemplateTask]    Script Date: 06/24/2013 15:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 20-June- 2009
-- Description:	To Update Field Service Template Task
-- =============================================

CREATE PROCEDURE [dbo].[Update_FieldServiceTemplateTask] 
	-- Add the parameters for the stored procedure here
	@ID_Template Bigint,
	@ID_Task int,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	--Delete From FS_Tasks Where ID_Template = @ID_Template
    -- Insert statements for procedure here
	
	if EXISTS(select * from FS_Tasks where ID_Template=@ID_Template and ID_Tasks=@ID_Task and IS_Deleted=0)
	Begin
		UPDATE FS_Tasks set Last_Modified_By=@Last_Modified_By, Date_Modified=getDate()
	end
	else
	begin
		INSERT INTO FS_Tasks(
		ID_Template,
		ID_Tasks,
		Date_Created,
		Last_Modified_By)
		VALUES(
		@ID_Template,
		@ID_Task,
		GetDate(),
		@Last_Modified_By)
	END
END
GO
/****** Object:  StoredProcedure [dbo].[Add_FieldServiceTemplateTask]    Script Date: 06/24/2013 15:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19-June- 2009
-- Description:	To Add Field Service Template Task
-- =============================================

CREATE PROCEDURE [dbo].[Add_FieldServiceTemplateTask] 
	-- Add the parameters for the stored procedure here
	
	@ID_Template Bigint,
	@ID_Task int,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	INSERT INTO FS_Tasks(
	ID_Template,
	ID_Tasks,
	Date_Created,
	Last_Modified_By)
    VALUES(
	@ID_Template,
	@ID_Task,
	GetDate(),
	@Last_Modified_By)

END
GO
/****** Object:  StoredProcedure [dbo].[Get_FieldServiceTemplateTasks_By_ID]    Script Date: 06/24/2013 15:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19 June 2009
-- Description:	
-- Example: exec [dbo].[Get_FieldServiceTemplateTasks_By_ID] 33
-- =============================================

CREATE PROCEDURE [dbo].[Get_FieldServiceTemplateTasks_By_ID] 
	@ID_Template bigint
AS
	SET NOCOUNT ON;
	-- Select Statement
		SELECT DISTINCT F.ID_FsTask, F.ID_Template,F.ID_Tasks, T.Title, T.Description
				, Case  WHEN FSD.ID_Tasks IS NULL then Cast(1 as bit) else Cast(0 as bit) END as IS_Edit
				--, FSA.ID_Template
		FROM [dbo].FS_Tasks  F
		INNER JOIN  Tasks T ON T.ID_Tasks =  F.ID_Tasks -- To get Task title
		LEFT OUTER JOIN  FS_Assets FSA ON FSA.ID_Template =  F.ID_Template and FSA.IS_Active = 1 AND FSA.IS_Deleted = 0
		LEFT OUTER JOIN  FS_AssetDetails FSD ON FSD.ID_FSAsset =  FSA.ID_FSAsset and FSD.IS_Active = 1 AND FSD.IS_Deleted = 0
		WHERE		F.ID_Template = @ID_Template
					AND F.Is_Deleted=0 AND F.Is_Active=1
					AND T.Is_Deleted = 0				
		Group BY F.ID_FsTask, F.ID_Template,F.ID_Tasks, T.Title
				, T.Description,FSA.ID_Template,FSD.ID_Tasks


--Select * from FS_AssetDetails
GO
/****** Object:  StoredProcedure [dbo].[Delete_FieldServiceTemplate]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 23-June- 2009
-- Description:	To Delete Field Service Template 
-- =============================================

CREATE PROCEDURE [dbo].[Delete_FieldServiceTemplate] 
	-- Add the parameters for the stored procedure here
	@ID_Template Bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin try
		begin transaction
			UPDATE FS_Templates SET IS_Deleted = 1 Where ID_Template = @ID_Template
			UPDATE FS_Tasks SET IS_Deleted = 1 Where ID_Template = @ID_Template
			UPDATE FS_Assets SET IS_Deleted = 1 Where ID_Template = @ID_Template
			UPDATE FS_AssetDetails SET IS_Deleted = 1 Where ID_FSAsset in (select ID_FSAsset from FS_Assets where Is_Deleted=1)
			UPDATE FS_AssetStatus set IS_Deleted=1, Date_Modified=getdate()  
					where (([ID_Template] = @ID_Template)) and (FSStatus=1010 or FSStatus=1020)
		commit transaction
	end try
	begin catch
	 IF (XACT_STATE()) = -1
		BEGIN
			rollback transaction
		END;
	end catch
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_FieldServiceTemplateTask]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 20-June- 2009
-- Description:	To Delete Field Service Template Task
-- =============================================

CREATE PROCEDURE [dbo].[Delete_FieldServiceTemplateTask] 
	-- Add the parameters for the stored procedure here
	@ID_Template Bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin try
		begin transaction
			Delete from FS_Tasks Where ID_Template = @ID_Template 
			--UPDATE FS_Tasks SET IS_Deleted = 1 Where ID_Template = @ID_Template
			--WE are not allowing to delete
			--UPDATE FS_Assets SET IS_Deleted = 1 Where ID_Template = @ID_Template
			--UPDATE FS_AssetDetails SET IS_Deleted = 1 Where ID_FSAsset in (select ID_FSAsset from FS_Assets where Is_Deleted=1)
			--UPDATE FS_AssetStatus set IS_Deleted=1, Date_Modified=getdate()  
			--		where (([ID_Template] = @ID_Template)) and (FSStatus=1010 or FSStatus=1020)			
		commit transaction
	end try
	begin catch
	 IF (XACT_STATE()) = -1
		BEGIN
			rollback transaction
		END;
	end catch
    
END
GO
/****** Object:  StoredProcedure [dbo].[Search_AssetsRefNo_ByDate]    Script Date: 06/24/2013 15:04:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Search_AssetsRefNo_ByDate]	
	@option int = 0,
	@FromDate Datetime = null,
	@ToDate Datetime = null,
	@RefNo varchar(50) = null,
	@StartTagID varchar(30) = null,
	@ItemCount int = 0
AS
BEGIN
IF @option = 0
	BEGIN
		Select Distinct(ReferenceNo) as RefNo,Count(ID_Asset) as ItemsCount,isnull(Allocation,'') as Allocation 
		from Assets 
		Where Cast(Convert(varchar(12),Date_Created,101) AS datetime) >= @FromDate AND Cast(Convert(varchar(12),Date_Created,101) AS datetime) <= @ToDate
		AND Is_active = 1 AND Is_deleted = 0 AND ReferenceNo <> ''
		Group By ReferenceNo,Allocation
	END
ELSE
	BEGIN
		DECLARE @startValue bigint 
		SET @startValue = Convert(bigint,dbo.HexStrToVarBinary(substring(@StartTagID,12,LEN(@StartTagID)-11)))
		IF @ItemCount = 0
			BEGIN
				Select Distinct(ReferenceNo) as RefNo,Count(ID_Asset) as ItemsCount,isnull(Allocation,'') as Allocation 
				from Assets 
				WHERE ReferenceNo = @RefNo AND
				Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) >= @startValue
				AND Is_active = 1 AND Is_deleted = 0 AND ReferenceNo <> ''
				Group By ReferenceNo,Allocation	
			END	
		ELSE
			BEGIN				
				Select Distinct(ReferenceNo) as RefNo,Count(ID_Asset) as ItemsCount,isnull(Allocation,'') as Allocation 
				from Assets 
				WHERE ReferenceNo = @RefNo AND
				(Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) >= @startValue AND Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) < (@startValue + @ItemCount))
				AND Is_active = 1 AND Is_deleted = 0 AND ReferenceNo <> ''
				Group By ReferenceNo,Allocation					
			END
	END
END

----------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Assets_To_Print]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Assets_To_Print] --[Get_Asset_By_TagID] '000000000000000000000150'
	@RefNo varchar(150),
	@Allocation varchar(100),
	@StartTagID varchar(30) = null,
	@ItemCount int = 0
AS
	SET NOCOUNT ON;
BEGIN
IF @StartTagID IS Null OR @StartTagID = ''
	BEGIN
		SELECT     ID_Asset, TagID, [Name],Allocation,ReferenceNo
		FROM         Assets
		WHERE	(ReferenceNo =   @RefNo)  AND (Allocation = @Allocation OR Allocation IS NULL)
		AND Is_active = 1 AND Is_deleted = 0
	END
ELSE
	BEGIN
		DECLARE @startValue bigint 
		SET @startValue = Convert(bigint,dbo.HexStrToVarBinary(substring(@StartTagID,12,LEN(@StartTagID)-11)))
		IF @ItemCount = 0
			BEGIN
				SELECT     ID_Asset, TagID, [Name],Allocation,ReferenceNo
				FROM         Assets
				WHERE	ReferenceNo =   @RefNo  AND (Allocation = @Allocation OR Allocation IS NULL) AND
				Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) >= @startValue
				AND Is_active = 1 AND Is_deleted = 0		
			END
		ELSE
			BEGIN
				SELECT     ID_Asset, TagID, [Name],Allocation,ReferenceNo
				FROM         Assets
				WHERE	ReferenceNo =   @RefNo  AND (Allocation = @Allocation OR Allocation IS NULL) AND
				(Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) >= @startValue AND
				 Convert(bigint,dbo.HexStrToVarBinary(substring(TagID,12,LEN(TagID)-11))) < (@startValue + @ItemCount))
				AND Is_active = 1 AND Is_deleted = 0						
			END		
	END
END

--------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_FSAssets]    Script Date: 06/24/2013 15:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 22-June-2009
-- Description:	To GET Assest to FS
-- =============================================

CREATE PROCEDURE [dbo].[Get_FSAssets]  
@Name nvarchar(100),
@ID_Template bigint,
@ID_Location bigint,
@ID_AssetGroup bigint,
@StartDate Datetime,
@EndDate Datetime

AS
Begin
	-- EXEC [Get_FSAssets]  '%','-1','-1','-1','2009-06-01','2010-06-30'
	SET NOCOUNT ON;
	-- Select Statment
	SELECT A.Name As Assets, T.Title as Tasks, FST.Title as Template, F.Startdate,F.EndDAte, D.ID_FsAsset, D.ID_FSAssetDetail,D.Last_Modified_By
	FROM FS_Assets F
	INNER JOIN FS_AssetDetails D ON D.ID_FSAsset = F.ID_FSAsset AND D.IS_Deleted = 0 AND D.IS_Active = 1
	INNER JOIN Tasks T ON T.ID_Tasks = D.ID_Tasks AND T.IS_Deleted = 0 AND T.IS_Active = 1
	INNER JOIN FS_Templates FST ON FST.ID_Template = F.ID_Template AND FST.IS_Deleted = 0 AND FST.IS_Active = 1
	INNER JOIN Assets A ON A.Id_Asset = F.ID_Asset AND A.IS_Deleted = 0 AND A.IS_Active = 1
	WHERE F.ID_Asset in (SELECT ID_Asset FROM Assets 
						WHERE (@ID_Location = -1 or ID_Location = @ID_Location) 
						AND (@ID_AssetGroup = -1 or ID_AssetGroup = @ID_AssetGroup)
						AND ((@Name = '%' or [Name] like @Name) AND IS_Deleted=0 AND IS_Active = 1)) -- Assets By Location , AssetGroup, NAme
	AND (@ID_Template = -1 OR F.ID_Template=@ID_Template) -- By Template
	AND StartDate >= @StartDate 
	AND EndDate <= @ENDDate
	AND F.IS_Deleted=0 AND F.IS_Active = 1
End
GO
/****** Object:  StoredProcedure [dbo].[Get_Assets_AllByFS]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 21-June-2009
-- Description:	To GET all Fs templates
-- =============================================
CREATE PROCEDURE [dbo].[Get_Assets_AllByFS]
@Date_Created DateTime
AS
	SET NOCOUNT ON;
	-- Select Statement
	Select FSA.ID_Asset, FSA.ID_Template, T.ID_Tasks, StartDate, A.Name As Assets, T.Title as Tasks, FST.Title as Template
	FROM FS_Assets FSA
	Inner JOIN  FS_AssetDetails D ON FSA.ID_FSAsset = D.ID_FSAsset AND FSA.Date_Modified = @Date_Created 
	INNER JOIN Tasks T ON T.ID_Tasks = D.ID_Tasks AND T.IS_Deleted = 0 AND T.IS_Active = 1 -- For task Title
	INNER JOIN FS_Templates FST ON FST.ID_Template = FSA.ID_Template AND FST.IS_Deleted = 0 AND FST.IS_Active = 1 -- For Template Name
	INNER JOIN Assets A ON A.Id_Asset = FSA.ID_Asset AND A.IS_Deleted = 0 AND A.IS_Active = 1 -- For Asset Name
GO
/****** Object:  StoredProcedure [dbo].[Delete_FSAssetTask]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 24-June- 2009
-- Description:	To Delete Field Service Template 
-- exec [dbo].[Delete_FSAssetTask] 5919
-- =============================================

CREATE PROCEDURE [dbo].[Delete_FSAssetTask] 
	-- Add the parameters for the stored procedure here
	@ID_FSAssetDetail Bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin try
		begin transaction
			UPDATE FS_AssetDetails SET IS_Deleted = 1 Where ID_FSAssetDetail = @ID_FSAssetDetail
		    
			Declare @ID_Tasks BIGINT,
			@ID_Asset BIGINT,
			@ID_Template bigint,
			@ID_FSAsset BIGINT
				
			Select @ID_Tasks =  Id_Tasks , @ID_FSAsset = ID_FSAsset 
			From FS_AssetDetails Where ID_FSAssetDetail = @ID_FSAssetDetail
			
			Select @ID_Asset =  ID_Asset,@ID_Template=ID_Template  
			From FS_Assets WHERE ID_FSAsset =  @ID_FSAsset

			Update [FS_AssetStatus] set IS_Deleted = 1 
			WHERE ID_Tasks = @ID_Tasks AND ID_Asset = @ID_Asset AND ID_Template =	@ID_Template 
			and (FSStatus=1010 or FSStatus=1020)
			Print 'Sucess'
		commit transaction
	end try
	begin catch
	 IF (XACT_STATE()) = -1
		BEGIN
			Print 'Error';
			rollback transaction
		END;
	end catch							
END
GO
/****** Object:  StoredProcedure [dbo].[Get_SecurityGroups]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all SecurityGroups
-- =============================================

CREATE PROCEDURE [dbo].[Get_SecurityGroups]
AS
	SET NOCOUNT ON;


SELECT     -1 as ID_SecurityGroup,'---Select--- ' as Name, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Master_SecurityGroups

UNION

SELECT     ID_SecurityGroup, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_SecurityGroups

WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Add_SecurityGroup]    Script Date: 06/24/2013 15:04:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Security Group
-- =============================================

CREATE PROCEDURE [dbo].[Add_SecurityGroup]
(
	@Name varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@AllowedFunctions VarChar(50) =null,
	@ReadEditAccess INT =null,
	@ReadAll Bit =null
	 
)
AS
	SET NOCOUNT OFF;
IF Not Exists (Select ID_SecurityGroup From Master_SecurityGroups Where [Name] = @Name)
	BEGIN
		INSERT INTO [Master_SecurityGroups] ([Name], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By],AllowedFunctions,ReadEditAccess,ReadAll) VALUES (@Name, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By,@AllowedFunctions,@ReadEditAccess,@ReadAll);
	
		SELECT ID_SecurityGroup, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,AllowedFunctions,ReadEditAccess,ReadAll FROM Master_SecurityGroups WHERE (ID_SecurityGroup = SCOPE_IDENTITY())
    END

---------------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_SecurityGroup_By_ID]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a SecurityGroup by ID_SecurityGroup
-- ========================================================

CREATE PROCEDURE [dbo].[Get_SecurityGroup_By_ID]
	@ID_SecurityGroup bigint
AS
	SET NOCOUNT ON;
SELECT     ID_SecurityGroup, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,AllowedFunctions,ReadEditAccess,ReadAll
FROM         Master_SecurityGroups
WHERE		ID_SecurityGroup=@ID_SecurityGroup
			AND Is_Deleted=0 AND Is_Active=1



-------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_SecurityGroups_All]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all SecurityGroups
-- =============================================

CREATE PROCEDURE [dbo].[Get_SecurityGroups_All]
AS
	SET NOCOUNT ON;
SELECT     ID_SecurityGroup, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,AllowedFunctions,ReadEditAccess,ReadAll
FROM         Master_SecurityGroups

WHERE	 Is_Deleted=0 AND Is_Active=1

--------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Delete_SecurityGroup]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a SecurityGroup
-- =============================================

CREATE PROCEDURE [dbo].[Delete_SecurityGroup]
(
	@ID_SecurityGroup bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Master_SecurityGroups] WHERE (([ID_SecurityGroup] = @ID_SecurityGroup))

UPDATE [Master_SecurityGroups] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_SecurityGroup] = @ID_SecurityGroup))
GO
/****** Object:  StoredProcedure [dbo].[Update_SecurityGroup]    Script Date: 06/24/2013 15:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a SecurityGroup
-- =============================================

CREATE PROCEDURE [dbo].[Update_SecurityGroup]
(
	@Name varchar(50),
	--@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_SecurityGroup bigint,
	@AllowedFunctions VarChar(50) =null,
	@ReadEditAccess Int =null,
	@ReadAll Bit =null

)
AS
	SET NOCOUNT OFF;

IF Not Exists (Select ID_SecurityGroup From Master_SecurityGroups Where [Name] = @Name AND ID_SecurityGroup != @ID_SecurityGroup)
	BEGIN

		UPDATE [Master_SecurityGroups] SET [Name] = @Name, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By, AllowedFunctions =@AllowedFunctions,ReadEditAccess= @ReadEditAccess,ReadAll =@ReadAll--,[Date_Created] = @Date_Created
										WHERE (([ID_SecurityGroup] = @ID_SecurityGroup));

		SELECT ID_SecurityGroup, Name, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,AllowedFunctions,ReadEditAccess FROM Master_SecurityGroups WHERE (ID_SecurityGroup = @ID_SecurityGroup)

	END	


--------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Add_PerformedTasks]    Script Date: 06/24/2013 15:04:03 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 14/5/2000
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_PerformedTasks] 
	-- Add the parameters for the stored procedure here
	@ID_Task bigint,
	@ID_Employee bigint,
	@ID_Asset bigint,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into dbo.PerformedTasks (ID_Task,ID_Employee,ID_Asset,Last_Modified_By)
    values (@ID_Task,@ID_Employee,@ID_Asset,@Last_Modified_By)

	SELECT * FROM dbo.PerformedTasks WHERE (ID_PerformedTask = SCOPE_IDENTITY())

END
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetComment]    Script Date: 06/24/2013 15:03:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Asset Comment
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetComment]
(
	@Comment varchar(1000),
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Asset_Comments] ([Comment], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Comment, getdate(), getdate(), 0,1, @Last_Modified_By);
	
SELECT ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Comments WHERE (ID_AssetComment = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Search_AssetComments]    Script Date: 06/24/2013 15:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =========================================================
-- Author:		Asif Ali
-- Create date: 20th March 2008
-- Description:	To Search all AssetComments by Comment Text
-- =========================================================

CREATE PROCEDURE [dbo].[Search_AssetComments] --'a'
	@Comment varchar(1000)    
AS
	SET NOCOUNT ON;
BEGIN
	IF (Len(@Comment)>0)
		BEGIN
			SELECT     ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
			FROM         Asset_Comments
			WHERE	Comment LIKE @Comment  AND Is_Deleted=0 AND Is_Active=1 AND ID_AssetComment<>1
		END
	ELSE
		BEGIN

			SELECT     ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
			FROM         Asset_Comments
			 
			WHERE	 Is_Deleted=0 AND Is_Active=1 AND ID_AssetComment<>1
		END
END
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetComment_By_ID]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a AssetComments By ID_AssetComment
-- ========================================================

CREATE PROCEDURE [dbo].[Get_AssetComment_By_ID]
	@ID_AssetComment bigint
AS
	SET NOCOUNT ON;
SELECT     ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Comments
WHERE		ID_AssetComment = @ID_AssetComment
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetComments_All]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetComments
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetComments_All]
AS
	SET NOCOUNT ON;
SELECT     ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Comments

WHERE		Is_Deleted=0 AND Is_Active=1 --AND ID_AssetComment<>1

ORDER BY Comment
GO
/****** Object:  StoredProcedure [dbo].[Vernon_GetAssetDefaultValues]    Script Date: 06/24/2013 15:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani,Deepanshu Jouhari 
-- Create date: 07 Aug 2009
-- Description:	To ADD a Location from Vernon Locations File
-- =============================================

CREATE PROCEDURE [dbo].[Vernon_GetAssetDefaultValues]
AS
	SET NOCOUNT OFF;

	Declare @ID_AssetType bigint,
			@ID_AssetStatus bigint,	
			@ID_AssetGroup bigint,
			@ID_AssetComments bigint,
			@ID_TagType bigint

Begin         
	SET @ID_AssetType = (SELECT ID_AssetType FROM Asset_Types WHERE [Type] = 'Asset')
	IF 	@ID_AssetType is NULL
	BEGIN
		SET @ID_AssetType = (SELECT top 1 ID_AssetType FROM Asset_Types)
	END

	SET @ID_AssetStatus = (SELECT ID_AssetStatus FROM Asset_Status WHERE [Status] = 'In')	
	IF 	@ID_AssetStatus is NULL
	BEGIN
		SET @ID_AssetStatus = (SELECT top 1 ID_AssetStatus FROM Asset_Status)
	END

	SET @ID_AssetGroup = (SELECT ID_AssetGroup FROM Asset_Groups WHERE [Name] = 'IT Assets')
	IF 	@ID_AssetGroup is NULL
	BEGIN
		SET @ID_AssetGroup = (SELECT Top 1 ID_AssetGroup FROM Asset_Groups)
	END

	SET @ID_AssetComments = (SELECT ID_AssetComment FROM Asset_Comments WHERE Comment = '')
	IF 	@ID_AssetComments is NULL
	BEGIN
		SET @ID_AssetComments = (SELECT Top 1 ID_AssetComment FROM Asset_Comments)
	END

	SET @ID_TagType = (SELECT ID_TagType FROM Tag_Types WHERE TagType = 'Asset')
    IF 	@ID_TagType is NULL
	BEGIN
		SET @ID_TagType = (SELECT Top 1 ID_TagType FROM Tag_Types)
	END

    Select @ID_AssetType as IDType,@ID_AssetStatus as IDStatus,@ID_AssetComments as IDComment,@ID_AssetGroup as IDGroup,
			@ID_TagType as IDTagType	
	
End
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetComment]    Script Date: 06/24/2013 15:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a AssetComment
-- =============================================

CREATE PROCEDURE [dbo].[Update_AssetComment]
(
	@Comment varchar(1000),
	@Last_Modified_By bigint,
	@ID_AssetComment bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Asset_Comments] SET [Comment] = @Comment,  [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By WHERE (([ID_AssetComment] = @ID_AssetComment));
	
SELECT ID_AssetComment, Comment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Comments WHERE (ID_AssetComment = @ID_AssetComment)
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetComment_LastModified]    Script Date: 06/24/2013 15:04:43 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset Comment
-- =============================================

Create PROCEDURE [dbo].[Update_AssetComment_LastModified]
(
	@ID_AssetComment bigint,
	@Last_Modified_By bigint
)
AS
SET NOCOUNT OFF;
--DELETE FROM [Asset_Comments] WHERE (([ID_AssetComment] = @ID_AssetComment))

UPDATE [Asset_Comments] SET  Last_Modified_By=@Last_Modified_By
WHERE (([ID_AssetComment] = @ID_AssetComment))
GO
/****** Object:  StoredProcedure [dbo].[Delete_AssetComment]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset Comment
-- =============================================

CREATE PROCEDURE [dbo].[Delete_AssetComment]
(
	@ID_AssetComment bigint

)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Asset_Comments] WHERE (([ID_AssetComment] = @ID_AssetComment))

UPDATE [Asset_Comments] SET Is_Deleted=1, Date_Modified=getdate() 
WHERE (([ID_AssetComment] = @ID_AssetComment))
GO
/****** Object:  StoredProcedure [dbo].[Add_FieldServiceTemplate]    Script Date: 06/24/2013 15:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19-June- 2009
-- Description:	To Add Field Service Template
-- =============================================

CREATE PROCEDURE [dbo].[Add_FieldServiceTemplate] 
	-- Add the parameters for the stored procedure here
	@Title nvarchar(50),
	@ID_TemplateType int,
	@Description nvarchar(300),
	@Frequency int,
	@Date_Modified datetime,
	@Last_Modified_By bigint,
	@ID_Template bigint output
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF NOT EXISTS(Select Title From FS_Templates Where Title = @Title) 
	BEGIN
		-- Insert statements for procedure here
		INSERT INTO FS_Templates(
		Title,
		ID_TemplateType,
		[Description],
		Frequency,
		Date_Created,
		Last_Modified_By)
		values(
		@Title,
		@ID_TemplateType,
		@Description,
		@Frequency,
		@Date_Modified,
		@Last_Modified_By)
		SELECT @ID_Template = SCOPE_IDENTITY()
	END
	ELSE
		SELECT @ID_Template = -1 -- To Check Title Already Present

		

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetDueDate]    Script Date: 06/24/2013 15:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Deepanshu Jouhari>
-- Create date: <25 June 2009>
-- Description:	exec Select GetDueDate(1,getDate()) as p
-- =============================================
CREATE FUNCTION [dbo].[GetDueDate] 
(
	-- Add the parameters for the function here
	@ID_Template BIGINT,
	@RefDate Datetime
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @DueDate DateTime
	SELECT @DueDate=cast('1900-1-1' as DateTime);
	SELECT @DueDate = Case WHEN ID_TemplateType = 1010 THEN DateAdd(Day, FST.Frequency, @RefDate) -- One Time
			 WHEN ID_TemplateType = 1020 THEN DateAdd(Day, FST.Frequency, @RefDate)  -- Daily
			 WHEN ID_TemplateType = 1030 THEN DateAdd(Week, FST.Frequency, @RefDate) -- Weekly
			 WHEN ID_TemplateType = 1040 THEN DateAdd(Month, FST.Frequency, @RefDate)	--Monthly
			 WHEN ID_TemplateType = 1050 THEN DateAdd(Year, FST.Frequency, @RefDate) END	-- Yearly
			 
	From FS_Templates FST	
	WHERE FST.ID_Template = @ID_Template
	
	
	RETURN @DueDate;
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[FuncGetFsDueDate]    Script Date: 06/24/2013 15:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		VINAY BANSAL
-- Create date: 23-June-2009
-- Description:	<Description,,>
-- =============================================
CREATE FUNCTION [dbo].[FuncGetFsDueDate]
(	
	-- Add the parameters for the function here
	@ID_Template BIGINT,
	@StartDate Datetime
)
RETURNS TABLE 
AS
RETURN 
(
	-- Add the SELECT statement with parameter references here
	SELECT Case WHEN ID_TemplateType = 1010 THEN DateAdd(Day, FST.Frequency, @StartDate) -- One Time
			 WHEN ID_TemplateType = 1020 THEN DateAdd(Day, FST.Frequency, @StartDate)  -- Daily
			 WHEN ID_TemplateType = 1030 THEN DateAdd(Week, FST.Frequency, @StartDate) -- Weekly
			 WHEN ID_TemplateType = 1040 THEN DateAdd(Month, FST.Frequency, @StartDate)	--Monthly
			 WHEN ID_TemplateType = 1050 THEN DateAdd(Year, FST.Frequency, @StartDate) END	-- Yearly
			 as DueDate
	From FS_Templates FST	
	WHERE FST.ID_Template = @ID_Template
)
GO
/****** Object:  StoredProcedure [dbo].[Get_FieldServiceTemplate_By_ID]    Script Date: 06/24/2013 15:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19 June 2009
-- Description:	
-- =============================================

CREATE PROCEDURE [dbo].[Get_FieldServiceTemplate_By_ID]
	@ID_Template bigint
AS
	SET NOCOUNT ON;
	-- Select Statment Here
	SELECT T.*,  Case  WHEN FSA.ID_Template IS NULL then Cast(1 as bit) else Cast(0 as bit) END as IS_Edit
	FROM        [dbo].[FS_Templates] T
	LEFT OUTER JOIN  FS_Assets FSA ON FSA.ID_Template =  T.ID_Template and FSA.IS_Active = 1 AND FSA.IS_Deleted = 0
	WHERE		T.ID_Template=@ID_Template
			AND T.Is_Deleted=0 AND T.Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Update_FieldServiceTemplate]    Script Date: 06/24/2013 15:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 20-June- 2009
-- Description:	To Update Field Service Template
-- =============================================

CREATE PROCEDURE [dbo].[Update_FieldServiceTemplate] 
	-- Add the parameters for the stored procedure here
	@ID_Template bigint, 
	@ID_TemplateType int,
	@Description nvarchar(300),
	@Frequency int,
	@Date_Modified datetime,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	UPDATE FS_Templates 
	SET ID_TemplateType = @ID_TemplateType,
		[Description]=@Description,
		Frequency=@Frequency,
		Last_Modified_By=@Last_Modified_By,
		Date_Modified = @Date_Modified
	WHERE ID_Template = @ID_Template
END
GO
/****** Object:  StoredProcedure [dbo].[Get_FSTemplates_All]    Script Date: 06/24/2013 15:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 21-June-2009
-- Description:	To GET all Fs templates
-- =============================================
/*
One_Time = 1010,
            Daily=1020,
            Weekly=1030,
            Monthly=1040,
            Yearly=1050  
*/

CREATE PROCEDURE [dbo].[Get_FSTemplates_All]
AS
	SET NOCOUNT ON;

select  '--- All ---' as Title, -1 as ID_Template union

SELECT Title + ' ' +(Case 
When Frequency=0 then '[One Time]'
When Frequency=1 and ID_TemplateType=1020 then '[Daily]'
When Frequency=1 and ID_TemplateType=1030 then '[Weekly]'
When Frequency=1 and ID_TemplateType=1040 then '[Monthly]'
When Frequency=1 and ID_TemplateType=1050 then '[Yearly]'
--When Frequency<>1 and ID_TemplateType=1020 then '[In '+ Cast(Frequency as Nvarchar(3)) + ' Days]'
When Frequency<>1 and ID_TemplateType=1020 then '[In '+ Cast(Frequency as Nvarchar(3)) + ' Days]'
When Frequency<>1 and ID_TemplateType=1030 then '[In '+ Cast(Frequency as Nvarchar(3)) + ' Weeks]'
When Frequency<>1 and ID_TemplateType=1040 then '[In '+ Cast(Frequency as Nvarchar(3)) + ' Months]'
When Frequency<>1 and ID_TemplateType=1050 then '[In '+ Cast(Frequency as Nvarchar(3)) + ' Years]'
else 'NA'
end) as Title, ID_Template
FROM   FS_Templates
WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Search_FieldserviceTemplate]    Script Date: 06/24/2013 15:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==============================================================================
-- Author:		Author:		Vinay Bansal
-- Create date: 19-June- 2009
-- Description:	To Search all Field Service Templates
-- Exec [dbo].[Search_FieldserviceTemplate] '%',-1,5,'%'
-- ==============================================================================


CREATE PROCEDURE [dbo].[Search_FieldserviceTemplate]
	@Title nvarchar(100),
	@ID_TemplateType int,
	@Frequency int,
	@Description nvarchar(300)
AS
BEGIN
	Set NOCOUNT ON;
	Select ID_Template, Title, [Description], Frequency, ID_TemplateType
	From FS_Templates
	WHERE (@Title='%' or Title like @Title) -- For title
	AND (@Description='%' or [Description] like @Description) -- For Description
	AND (@ID_TemplateType = 0 or ID_TemplateType=@ID_TemplateType) -- For TemplateType
	AND (@Frequency = 0 or Frequency=@Frequency) -- For Frequency
	AND IS_Deleted = 0	AND IS_Active = 1
END
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetTypes]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetTypes
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetTypes]
AS
	SET NOCOUNT ON;

--SELECT     -1 as ID_AssetType, '--- Select --- ' as Type, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Asset_Types
--
--UNION

SELECT     ID_AssetType, Type, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Types
WHERE	 Is_Deleted=0 AND Is_Active=1


-------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetType_By_ID]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a AssetType by ID_AssetType
-- =================================================

CREATE PROCEDURE [dbo].[Get_AssetType_By_ID]
	@ID_AssetType bigint
AS
	SET NOCOUNT ON;
SELECT     ID_AssetType, Type, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Types
WHERE		ID_AssetType=@ID_AssetType
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetType]    Script Date: 06/24/2013 15:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a AssetType
-- =============================================

CREATE PROCEDURE [dbo].[Update_AssetType]
(
	@Type varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_AssetType bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Asset_Types] SET [Type] = @Type, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_AssetType] = @ID_AssetType));
	
SELECT ID_AssetType, Type, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Types WHERE (ID_AssetType = @ID_AssetType)
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetType]    Script Date: 06/24/2013 15:03:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Asset Type
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetType]
(
	@Type varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Asset_Types] ([Type], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Type, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_AssetType, Type, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Types WHERE (ID_AssetType = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetTypes_All]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetTypes
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetTypes_All]
AS
	SET NOCOUNT ON;

SELECT     -1 as ID_AssetType, '--- All --- ' as Type, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Asset_Types

UNION

SELECT     ID_AssetType, Type, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Types
WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Delete_AssetType]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a AssetType
-- =============================================

CREATE PROCEDURE [dbo].[Delete_AssetType]
(
	@ID_AssetType bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Asset_Types] WHERE (([ID_AssetType] = @ID_AssetType))

UPDATE [Asset_Types] SET Is_Deleted=1, Date_Modified=getdate()
WHERE  (([ID_AssetType] = @ID_AssetType))
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_EmailLog]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MR_Get_EmailLog]
AS
	SET NOCOUNT ON;
SELECT     Email_Log.*
FROM         Email_Log
GO
/****** Object:  StoredProcedure [dbo].[MR_Add_EmailLog]    Script Date: 06/24/2013 15:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MR_Add_EmailLog]
(
	@ID_Alert bigint,
	@Email_To nvarchar(500),
	@Email_From varchar(200),
	@Email_Body nvarchar(2000),
	@Email_Subject varchar(200),
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Email_Log] ([ID_Alert], [Email_To], [Email_From], [Email_Body], [Email_Subject], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) 
VALUES (@ID_Alert, @Email_To, @Email_From, @Email_Body, @Email_Subject, getdate(), getdate(), 0, 1, @Last_Modified_By);
	
SELECT ID_EmailLog, ID_Alert, Email_To, Email_From, Email_Body, Email_Subject, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Email_Log WHERE (ID_EmailLog = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetStatus]    Script Date: 06/24/2013 15:04:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a AssetStatus
-- =============================================

CREATE PROCEDURE [dbo].[Update_AssetStatus]
(
	@Status varchar(50),
	@Description varchar(1000),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_AssetStatus bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Asset_Status] SET [Status] = @Status, [Description] = @Description, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_AssetStatus] = @ID_AssetStatus));
	
SELECT ID_AssetStatus, Status, Description, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Status WHERE (ID_AssetStatus = @ID_AssetStatus)
GO
/****** Object:  StoredProcedure [dbo].[Delete_AssetStatus]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a AssetStatus
-- =============================================

CREATE PROCEDURE [dbo].[Delete_AssetStatus]
(
	@ID_AssetStatus bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Asset_Status] WHERE (([ID_AssetStatus] = @ID_AssetStatus))

UPDATE [Asset_Status] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_AssetStatus] = @ID_AssetStatus))
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetStatus]    Script Date: 06/24/2013 15:03:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Asset Status
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetStatus]
(
	@Status varchar(50),
	@Description varchar(1000),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Asset_Status] ([Status], [Description], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Status, @Description, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_AssetStatus, Status, Description, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Status WHERE (ID_AssetStatus = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetStatus_All]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetStatus
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetStatus_All]
AS
	SET NOCOUNT ON;

SELECT     -1 as ID_AssetStatus, '--- All --- ' as Status,'Description' as Description, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
FROM         Asset_Status


UNION

SELECT     ID_AssetStatus, Status, Description, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Status
WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetStatus]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetStatus
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetStatus]
AS
	SET NOCOUNT ON;

--SELECT     -1 as ID_AssetStatus, '--- Select --- ' as Status,'Description' as Description, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Asset_Status
--
--
--UNION

SELECT     ID_AssetStatus, Status, Description, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Status
WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetStatus_By_ID]    Script Date: 06/24/2013 15:04:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a AssetStatus by ID_AssetStatus
-- =====================================================

CREATE PROCEDURE [dbo].[Get_AssetStatus_By_ID]
	@ID_AssetStatus bigint
AS
	SET NOCOUNT ON;
SELECT     ID_AssetStatus, Status, Description, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Status
WHERE		ID_AssetStatus=@ID_AssetStatus
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Delete_AssetGroup]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[Delete_AssetGroup]
(
	@ID_AssetGroup bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Asset_Groups] WHERE (([ID_AssetGroup] = @ID_AssetGroup))

UPDATE [Asset_Groups] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_AssetGroup] = @ID_AssetGroup))
GO
/****** Object:  UserDefinedFunction [dbo].[GetParentGroups]    Script Date: 06/24/2013 15:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: <Create Date, ,>
-- Example:	select dbo.GetParentGroups(8)
-- =============================================
CREATE FUNCTION [dbo].[GetParentGroups] 
(
	-- Add the parameters for the function here
	@GroupID bigint
)
RETURNS nvarchar(500)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @ResultString nvarchar(500),
	@ID bigint
	
	Select @ID=isNULL(ParentID,0) from Asset_Groups where ID_AssetGroup = @GroupID
	if(@ID<>0)
		BEGIN
			SET @ResultString = ',' + cast(@ID as Nvarchar(5)) + dbo.GetParentGroups(@ID)
		END
	ELSE
		BEGIN
			SET @ResultString = ',-1,';
		END

	RETURN @ResultString;

END
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetGroup]    Script Date: 06/24/2013 15:03:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetGroup]
(
	@Name varchar(50),
	@ParentID bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Asset_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Search_SummaryAssetMovements]    Script Date: 06/24/2013 15:04:38 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================
-- Author:		Deepanshu Jouhari
-- Modified     Deepanshu Jouhari
-- Create date: 2tth March 2008
-- Description:	To Search all Asset Maovements Summary for OnRampAccess
--[dbo].[Search_SummaryAssetMovements] 'Date_Modified BETWEEN ''29/02/2008'' AND ''29/03/2008''', -1,-1
-- ======================================================================================

CREATE PROCEDURE [dbo].[Search_SummaryAssetMovements] 
	@SearchCriteria NVARCHAR(1000),
	@ID_AssetGroup bigint,
	@ID_Location bigint
AS
BEGIN
--,(select count(*) from Assets where id_location = l.ID_Location and ' + @SearchCriteria +' ) as OutStanding
	SET NOCOUNT ON;

	--.............................................................................
	DECLARE @PARENTID BIGINT, @SUBID VARCHAR(5000),
	 @SUBSUBID VARCHAR(5000),
	@SQL NVARCHAR(4000)
	--SET @ID_AssetGroup =3
	SET @PARENTID = 0
	SET @SUBID = ltrim(str(@ID_AssetGroup)) + ','
	SET @SUBSUBID = ''
	SET @SQL = ''
	if (@ID_AssetGroup <> -1)
	begin

		SELECT @PARENTID = ParentID FROM Asset_Groups WHERE ID_AssetGroup = @ID_AssetGroup
		SELECT @SUBID=@SUBID+CONVERT(VARCHAR(5),(ISNULL(ID_AssetGroup,'')))+ ',' FROM [Asset_Groups] WHERE ParentID = @ID_AssetGroup
		SET @SUBSUBID = @SUBID
		SET @SUBID= SUBSTRING(@SUBID,0,LEN(@SUBID))

		IF (LEN(@SUBID)>0)
			SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE ParentID IN ('+ @SUBID + ')'
		ELSE
			SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE  ID_AssetGroup = ' + LTRIM(STR(@ID_AssetGroup))


		EXEC sp_executesql 
					@query = @SQL, 
					@params = N'@SUBSUBID VARCHAR(5000) OUTPUT', 
					@SUBSUBID = @SUBSUBID OUTPUT 


		SET @SUBSUBID= SUBSTRING(@SUBSUBID,0,LEN(@SUBSUBID))
		
		set @SearchCriteria = @SearchCriteria + ' AND [ID_AssetGroup] IN ('+ @SUBSUBID +')'
	end

if (@ID_Location <> -1)
	begin
		SET @SQL =  'select l.TagID,l.Name
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and ' + @SearchCriteria +') as Assigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and '+ @SearchCriteria + ') as CheckedIn
		,(select count(*) from Assets where id_location = l.ID_Location and ' + @SearchCriteria +' ) as OutStanding
		into #abc
		from locations l where id_location='+ LTRIM(STR(@ID_Location))+' 
		select TagID,[Name],Assigned,CheckedIn,(Assigned-CheckedIn) as OutStanding from #abc
		drop table #abc	
	'
	end
else
	begin
		SET @SQL =  'select l.TagID,l.Name
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and ' + @SearchCriteria +') as Assigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and '+ @SearchCriteria + ') as CheckedIn
		into #abc
		from locations l where l.id_location <> (select id_location from locations a where a.name=''CheckIn'')
		select TagID,[Name],Assigned,CheckedIn,(Assigned-CheckedIn) as OutStanding from #abc
		drop table #abc	
	'
	end

	set dateformat dmy 
	PRINT @SQL
	EXEC SP_EXECUTESQL @SQL

END
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetGroups_All]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetGroups
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetGroups_All]
AS

--	SET NOCOUNT ON;
--SELECT     ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
--FROM         Asset_Groups
--
--WHERE	Is_Deleted=0 AND Is_Active=1



		WITH ToplevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select      ID_AssetGroup, Name,ParentID
					from    Asset_Groups  
					where   ParentID = 0
							AND Is_Deleted=0 AND Is_Active=1
			),

			FirstLevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select		Asset_Groups.ID_AssetGroup, ((TL.Name) + '--> '+Asset_Groups.Name) as [Name], Asset_Groups.ParentID
					from	ToplevelGroup TL inner join Asset_Groups  
							ON TL.ID_AssetGroup=Asset_Groups.ParentID
					WHERE	Is_Deleted=0 AND Is_Active=1	
			),

			SecondLevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select		Asset_Groups.ID_AssetGroup, ((FL.Name) + '--> '+Asset_Groups.Name) as [Name], Asset_Groups.ParentID
					from	FirstLevelGroup FL inner join Asset_Groups  
							ON FL.ID_AssetGroup=Asset_Groups.ParentID
					WHERE	Is_Deleted=0 AND Is_Active=1
			)

			select -1 as ID_AssetGroup, '--- All ---' as [Name], -1 as ParentID union
			select ID_AssetGroup, [Name], ParentID from ToplevelGroup union
			select ID_AssetGroup, [Name], ParentID from FirstLevelGroup union
			select ID_AssetGroup, [Name], ParentID from SecondLevelGroup 

			Order by [Name]
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetGroups]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all AssetGroups
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetGroups]
AS

--	SET NOCOUNT ON;
--SELECT     ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
--FROM         Asset_Groups
--
--WHERE	Is_Deleted=0 AND Is_Active=1



		WITH ToplevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select      ID_AssetGroup, Name,ParentID
					from    Asset_Groups  
					where   ParentID = 0
							AND Is_Deleted=0 AND Is_Active=1
			),

			FirstLevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select		Asset_Groups.ID_AssetGroup, ((TL.Name) + '--> '+Asset_Groups.Name) as [Name], Asset_Groups.ParentID
					from	ToplevelGroup TL inner join Asset_Groups  
							ON TL.ID_AssetGroup=Asset_Groups.ParentID
					WHERE	Is_Deleted=0 AND Is_Active=1	
			),

			SecondLevelGroup (ID_AssetGroup, Name, ParentID) as
			(
				select		Asset_Groups.ID_AssetGroup, ((FL.Name) + '--> '+Asset_Groups.Name) as [Name], Asset_Groups.ParentID
					from	FirstLevelGroup FL inner join Asset_Groups  
							ON FL.ID_AssetGroup=Asset_Groups.ParentID
					WHERE	Is_Deleted=0 AND Is_Active=1
			)

			--select -1 as ID_AssetGroup, '--- Select---' as [Name], -1 as ParentID union
			select ID_AssetGroup, [Name], ParentID from ToplevelGroup union
			select ID_AssetGroup, [Name], ParentID from FirstLevelGroup union
			select ID_AssetGroup, [Name], ParentID from SecondLevelGroup 

			Order by [Name]

-------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[AddAssetGroup]    Script Date: 06/24/2013 15:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 19 Sept 2011
-- Description:	To ADD a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[AddAssetGroup]
(
	@Name varchar(50),
	@ParentID bigint = 0, 
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(SELECT ID_AssetGroup FROM Asset_Groups WHERE [Name] = @Name AND ParentID = @ParentID)
	BEGIN
		INSERT INTO [Asset_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, GetDate(), GetDate(), 0, 1, @Last_Modified_By);
		RETURN SCOPE_IDENTITY()
	END
ELSE
	BEGIN
		RETURN -1;
	END

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[DeleteAssetGroup]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 19 Sept 2011
-- Description:	To ADD a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[DeleteAssetGroup]
(
	@ID_AssetGroup BigInt, 
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

 
		UPDATE  [Asset_Groups] SET      [Date_Modified] = GetDate(), 
										[Is_Deleted] = 1, 
										[Is_Active] =0, 
										[Last_Modified_By] = @Last_Modified_By
					WHERE ID_AssetGroup =@ID_AssetGroup
		RETURN 1;
 

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[UpdateAssetGroup]    Script Date: 06/24/2013 15:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 19 Sept 2011
-- Description:	To ADD a Asset Group
-- =============================================

CREATE PROCEDURE [dbo].[UpdateAssetGroup]
(
	@ID_AssetGroup BigInt,
	@Name varchar(50),
	@ParentID bigint = 0, 
	@Is_Deleted Bit = 0,
	@Is_Active Bit =1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(SELECT ID_AssetGroup FROM Asset_Groups WHERE [Name] = @Name AND ParentID = @ParentID)
	BEGIN
		UPDATE  [Asset_Groups] SET [Name] = @Name, 
										[ParentID] = @ParentID,
									    [Date_Modified] = GetDate(), 
										[Is_Deleted] = @Is_Deleted, 
										[Is_Active] =@Is_Active, 
										[Last_Modified_By] = @Last_Modified_By
					WHERE ID_AssetGroup =@ID_AssetGroup
		RETURN 1;
	END
ELSE
	BEGIN
		RETURN -1;
	END

	
--SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Search_AssetMovements]    Script Date: 06/24/2013 15:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================
-- Author:		Asif Ali
-- Modified     Deepanshu Jouhari
-- Create date: 2tth March 2008
-- Description:	To Search all Asset Maovements by TYPE, LOCATION, GROUP & Period
-- ======================================================================================

CREATE PROCEDURE [dbo].[Search_AssetMovements]  --'DateMoved BETWEEN ''29/02/2008'' AND ''29/03/2008''', -1
	@SearchCriteria NVARCHAR(1000),
	@ID_AssetGroup bigint
AS
BEGIN
	SET NOCOUNT ON;

--.............................................................................
DECLARE @PARENTID BIGINT, @SUBID VARCHAR(5000),
 @SUBSUBID VARCHAR(5000),
@SQL NVARCHAR(4000)
--SET @ID_AssetGroup =3
SET @PARENTID = 0
SET @SUBID = ltrim(str(@ID_AssetGroup)) + ','
SET @SUBSUBID = ''
SET @SQL = ''
if (@ID_AssetGroup <> -1)
begin

SELECT @PARENTID = ParentID FROM Asset_Groups WHERE ID_AssetGroup = @ID_AssetGroup
PRINT @PARENTID

SELECT @SUBID=@SUBID+CONVERT(VARCHAR(5),(ISNULL(ID_AssetGroup,'')))+ ',' FROM [Asset_Groups] WHERE ParentID = @ID_AssetGroup
SET @SUBSUBID = @SUBID
SET @SUBID= SUBSTRING(@SUBID,0,LEN(@SUBID))
PRINT @SUBID

IF (LEN(@SUBID)>0)
	SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE ParentID IN ('+ @SUBID + ')'
ELSE
	SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE  ID_AssetGroup = ' + LTRIM(STR(@ID_AssetGroup))

PRINT @SQL
EXEC sp_executesql 
			@query = @SQL, 
			@params = N'@SUBSUBID VARCHAR(5000) OUTPUT', 
			@SUBSUBID = @SUBSUBID OUTPUT 


SET @SUBSUBID= SUBSTRING(@SUBSUBID,0,LEN(@SUBSUBID))
PRINT @SUBSUBID

end

--else
--
--begin
--end
--
--SET @SQL='SELECT   ID_Asset,AssetName,Description, AssetNo, DateMoved, Last_Modified_By, EmployeeName,LocationName,LocationNo, 
--		 ID_Location, ID_AssetType, Type, [ID_AssetGroup],GroupName
--FROM     Vw_Asset_Movement WHERE [ID_AssetGroup] IN ('+ @SUBSUBID +')'
--
--
--PRINT @SQL
--EXEC sp_executesql @Sql


--.............................................................................


--if (Len(@SearchCriteria)>0)
--	set @SearchCriteria = ' WHERE ' + @SearchCriteria
-- 
--
--	DECLARE @SQL NVARCHAR(4000)
	SET @SQL = 'SELECT	 ID_Asset,isnull(AssetName,'''') as AssetName,isnull(Description,'''') as Description, isnull(AssetNo,'''') as AssetNo, isnull(DateMoved,'''') as DateMoved, isnull(Last_Modified_By,0) as Last_Modified_By, isnull(EmployeeName,'''') as EmployeeName,isnull(LocationName,'''') as LocationName,isnull(LocationNo,'''') as LocationNo, 
						 ID_Location, ID_AssetType, isnull(Type,'''') as Type, ID_AssetGroup,isnull(GroupName,'''') as GroupName, isnull(Status,'''') as Status,isnull(ReferenceNo,'''') as  ReferenceNo,isnull(TagID,'''') as  TagID
				
				FROM    Vw_Asset_Movement WHERE 1=1 AND ' + @SearchCriteria 


if (@ID_AssetGroup<>-1)

SET @SQL = @SQL + ' AND [ID_AssetGroup] IN ('+ @SUBSUBID +')'

SET @SQL = @SQL + ' ORDER BY AssetName, DateMoved desc '

	--PRINT @SQL
	set dateformat dmy 
	EXEC SP_EXECUTESQL @SQL
END





--##########################################################################################
--ALTER PROCEDURE [dbo].[Search_AssetMovements]
--	@SearchCriteria NVARCHAR(1000)
--AS
--BEGIN
--	SET NOCOUNT ON;
--
--if (Len(@SearchCriteria)>0)
--	set @SearchCriteria = ' WHERE ' + @SearchCriteria
-- 
--
--	DECLARE @SQL NVARCHAR(4000)
--	SET @SQL = 'SELECT	 ID_Asset,AssetName,Description, AssetNo, DateMoved, Last_Modified_By, EmployeeName,LocationName,LocationNo, 
--						 ID_Location, ID_AssetType, Type, ID_AssetGroup,GroupName
--				
--				FROM    Vw_Asset_Movement ' + @SearchCriteria 
--
--
--	--PRINT @SQL
--	set dateformat dmy 
--	EXEC SP_EXECUTESQL @SQL
--END



------.........................................................................................................................................................
--ALTER PROCEDURE [dbo].[Search_AssetMovements]  --1,1,1,'1/12/2008','12/12/2008'
--	
--	@ID_AssetType bigint,
--	@ID_Location bigint,
--	@ID_AssetGroup	bigint,
--	@FromDate	varchar(50),
--	@ToDate	varchar(50)
----	@FromDate	datetime,
----	@ToDate	datetime
----	@SortBy varchar(50),
----	@SortDirection varchar(50)
--
--
--AS
--	SET NOCOUNT ON;
--
--SELECT   ID_Asset,AssetName,Description, AssetNo, DateMoved, Last_Modified_By, EmployeeName,LocationName,LocationNo, 
--		 ID_Location, ID_AssetType, Type, ID_AssetGroup,GroupName
--	
--
--FROM     Vw_Asset_Movement
--
--WHERE	  (@ID_AssetType IS NULL or ID_AssetType = @ID_AssetType)
--	AND   (@ID_Location IS NULL or ID_Location = @ID_Location)
--	AND   (@ID_AssetGroup IS NULL or ID_AssetGroup = @ID_AssetGroup)
--	AND   (DateMoved BETWEEN @FromDate AND @ToDate)
--	--AND	   Is_Deleted=0 AND Is_Active=1
--	--Order By @SortBy 
----@SortDirection
------.........................................................................................................................................................
--
GO
/****** Object:  StoredProcedure [dbo].[Search_SummaryAssetMovementsNEW]    Script Date: 06/24/2013 15:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =====================================================================================
-- Author:		Deepanshu Jouhari
-- Modified     Deepanshu Jouhari
-- Create date: 2tth March 2008
-- Description:	To Search all Asset Maovements Summary for OnRampAccess
--[dbo].[Search_SummaryAssetMovementsNEW] '1=1', -1,-1,'2008-11-20','2008-12-31'
-- ======================================================================================

CREATE PROCEDURE [dbo].[Search_SummaryAssetMovementsNEW] 
	@SearchCriteria NVARCHAR(1000),
	@ID_AssetGroup bigint,
	@ID_Location bigint,
	@FromDate DateTime,
	@ToDate DateTime
AS
BEGIN
	SET NOCOUNT ON;

	--.............................................................................
	DECLARE @PARENTID BIGINT, @SUBID VARCHAR(5000),
	 @SUBSUBID VARCHAR(5000),
	@SQL NVARCHAR(4000)
	--SET @ID_AssetGroup =3
	SET @PARENTID = 0
	SET @SUBID = ltrim(str(@ID_AssetGroup)) + ','
	SET @SUBSUBID = ''
	SET @SQL = ''
	if (@ID_AssetGroup <> -1)
	begin

		SELECT @PARENTID = ParentID FROM Asset_Groups WHERE ID_AssetGroup = @ID_AssetGroup
		SELECT @SUBID=@SUBID+CONVERT(VARCHAR(5),(ISNULL(ID_AssetGroup,'')))+ ',' FROM [Asset_Groups] WHERE ParentID = @ID_AssetGroup
		SET @SUBSUBID = @SUBID
		SET @SUBID= SUBSTRING(@SUBID,0,LEN(@SUBID))

		IF (LEN(@SUBID)>0)
			SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE ParentID IN ('+ @SUBID + ')'
		ELSE
			SET @SQL = N'SELECT @SUBSUBID=@SUBSUBID+CONVERT(VARCHAR(5),ID_AssetGroup)+ '','' FROM [Asset_Groups] WHERE  ID_AssetGroup = ' + LTRIM(STR(@ID_AssetGroup))


		EXEC sp_executesql 
					@query = @SQL, 
					@params = N'@SUBSUBID VARCHAR(5000) OUTPUT', 
					@SUBSUBID = @SUBSUBID OUTPUT 


		SET @SUBSUBID= SUBSTRING(@SUBSUBID,0,LEN(@SUBSUBID))
		
		set @SearchCriteria = @SearchCriteria + ' AND [ID_AssetGroup] IN ('+ @SUBSUBID +')'
	end

if (@ID_Location <> -1)
	begin
		SET @SQL =  'select l.TagID,l.Name
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and Date_Modified between cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and cast('''+ cast(@ToDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria +') as Assigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and Date_Modified between cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and cast('''+ cast(@ToDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria + ') as CheckedIn
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and Date_Modified < cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria +') as PrevAssigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and Date_Modified < cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria + ') as PrevCheckedIn
		into #abc
		from locations l where id_location='+ LTRIM(STR(@ID_Location))+' 
		select TagID,[Name],Assigned,CheckedIn,(Assigned-CheckedIn) as OutStanding,(PrevAssigned-PrevCheckedIn) as PrevOutStanding from #abc
		drop table #abc	
	'
	end
else
	begin
		SET @SQL =  'select l.TagID,l.Name
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and Date_Modified between cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and cast('''+ cast(@ToDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria +') as Assigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and Date_Modified between cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and cast('''+ cast(@ToDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria + ') as CheckedIn
		,(select count(*) from Vw_Asset_MovementOnly where id_LocationNew=l.ID_Location and Date_Modified < cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria +') as PrevAssigned
		,(select count(*) from Vw_Asset_MovementOnly where id_Location=l.ID_Location and id_LocationNew=(select id_location from locations a where a.name=''CheckIn'') and Date_Modified < cast('''+ cast(@FromDate as nvarchar) +''' as DateTime) and ' + @SearchCriteria + ') as PrevCheckedIn
		into #abc
		from locations l where l.id_location <> (select id_location from locations a where a.name=''CheckIn'')
		select TagID,[Name],Assigned,CheckedIn,(Assigned-CheckedIn) as OutStanding,(PrevAssigned-PrevCheckedIn) as PrevOutStanding from #abc
		drop table #abc	
	'
	end

	--set dateformat dmy 
	PRINT @SQL
	EXEC SP_EXECUTESQL @SQL

END
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetGroup]    Script Date: 06/24/2013 15:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a AssetGroup
-- =============================================

CREATE PROCEDURE [dbo].[Update_AssetGroup]
(
	@Name varchar(50),
	@ParentID bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_AssetGroup bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Asset_Groups] SET [Name] = @Name, [ParentID] = @ParentID, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_AssetGroup] = @ID_AssetGroup));
	
SELECT ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Groups WHERE (ID_AssetGroup = @ID_AssetGroup)
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetGroup_By_ID]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a AssetGroup by ID_AssetGroup
-- ====================================================

CREATE PROCEDURE [dbo].[Get_AssetGroup_By_ID]
	@ID_AssetGroup bigint
AS
	SET NOCOUNT ON;
SELECT     ID_AssetGroup, Name, ParentID, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Asset_Groups
WHERE		ID_AssetGroup=@ID_AssetGroup
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetGroupFromFile]    Script Date: 06/24/2013 15:03:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 26 OCT 2009
-- Description:	To ADD a Asset Group FROM CSV File
-- =============================================

CREATE PROCEDURE [dbo].[Add_AssetGroupFromFile]
(
	@Name varchar(50),
	@ParentGroup varchar(50) = '',
	@ParentID bigint = null,
	@Date_Created datetime = null,
	@Date_Modified datetime = null,
	@Is_Deleted bit = 0,
	@Is_Active bit = 1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
set dateformat dmy   
IF @ParentGroup = ''
	SET @ParentID = 0
ELSE
BEGIN
--IF @ParentID is null
	BEGIN
		IF NOT EXISTS (Select ID_AssetGroup FROM [Asset_Groups] WHERE [Name] = @ParentGroup)
			BEGIN
				INSERT INTO [Asset_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@ParentGroup, 0, Getdate(), getdate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
				SET @ParentID = SCOPE_IDENTITY()
			END
		ELSE
			BEGIN
				SET @ParentID = (SELECT ID_AssetGroup FROM [Asset_Groups] WHERE [Name] = @ParentGroup)
			END
	END
END

BEGIN
	IF NOT EXISTS (SELECT ID_AssetGroup  FROM [Asset_Groups] WHERE [Name] = @Name AND [ParentID] = @ParentID)
		BEGIN
			INSERT INTO [Asset_Groups] ([Name], [ParentID], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Name, @ParentID, getdate(), getdate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
			 SELECT @@IDENTITY
		END
	ELSE
			SELECT -1
END 

------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Add_Reason]    Script Date: 06/24/2013 15:04:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 14/5/2000
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_Reason] 
	-- Add the parameters for the stored procedure here
--	@ReasonNo nvarchar(50),
	@Name nvarchar(300),
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
   declare @rNo int
select @rNo = count(*) from Reasons
	insert into Reasons (ReasonNo,[Name],Last_Modified_By)
    values (@rNo,@Name,@Last_Modified_By)
	
	SELECT * FROM Reasons WHERE (ID_Reason = SCOPE_IDENTITY())

END
GO
/****** Object:  StoredProcedure [dbo].[Update_Reason]    Script Date: 06/24/2013 15:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 14 May 2009
-- Description:	To UPDATE a Task
-- =============================================

CREATE PROCEDURE [dbo].[Update_Reason]
(
	@ID_Reason bigint,
	@ReasonNo nvarchar(300),
	@Name varchar(500),
	@Date_Modified datetime=getDate,
	@Is_Deleted bit=0,
	@Is_Active bit=1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

	UPDATE [Reasons] SET [ReasonNo] = @ReasonNo, [Name] = @Name,
	[Date_Modified] = @Date_Modified,
	[Is_Deleted] = @Is_Deleted,
	[Is_Active] = @Is_Active, 
	[Last_Modified_By] = @Last_Modified_By 
	WHERE (([ID_Reason] = @ID_Reason));

	
	SELECT * FROM Reasons WHERE (ID_Reason = @ID_Reason)
GO
/****** Object:  StoredProcedure [dbo].[Delete_Reason]    Script Date: 06/24/2013 15:04:11 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To DELETE a Task
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Reason]
(
	@ID_Reason bigint
)
AS
	SET NOCOUNT OFF;
--select * from Tasks
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

	 
		 
		UPDATE [Reasons] SET Is_Deleted=1,Is_Active=0, Date_Modified=getdate()
		WHERE (([ID_Reason] = @ID_Reason))
GO
/****** Object:  StoredProcedure [dbo].[Get_Reason_By_ID]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To GET a Tasks by ID_Tasks
-- =============================================

CREATE PROCEDURE [dbo].[Get_Reason_By_ID]
	@ID_Reason bigint
AS
	SET NOCOUNT ON;
SELECT *
FROM        Reasons
WHERE		ID_Reason=@ID_Reason
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Reasons_All]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19-June- 2009
-- Description:	To GET all Tasks
-- =============================================

CREATE PROCEDURE [dbo].[Get_Reasons_All]
AS
	SET NOCOUNT ON;

SELECT     ID_Reason, ReasonNo,Name
FROM         Reasons
WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Skill_By_ID]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a Skill by ID_Skill
-- =============================================

CREATE PROCEDURE [dbo].[Get_Skill_By_ID]
	@ID_Skill bigint
AS
	SET NOCOUNT ON;
SELECT     ID_Skill, Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Skills
WHERE		ID_Skill=@ID_Skill
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Skills]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Skills
-- =============================================

CREATE PROCEDURE [dbo].[Get_Skills]
AS
	SET NOCOUNT ON;

--SELECT     -1 as ID_Skill,'---Select--- ' as Skill, getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
--FROM         Master_Skills
--
--UNION

SELECT     ID_Skill, Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Skills

WHERE	Is_Deleted=0 AND Is_Active=1

--------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Update_Skill]    Script Date: 06/24/2013 15:04:52 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To UPDATE a Skill
-- =============================================

CREATE PROCEDURE [dbo].[Update_Skill]
(
	@Skill varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_Skill bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Master_Skills] SET [Skill] = @Skill, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_Skill] = @ID_Skill));
	
SELECT ID_Skill, Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_Skills WHERE (ID_Skill = @ID_Skill)
GO
/****** Object:  StoredProcedure [dbo].[Delete_Skill]    Script Date: 06/24/2013 15:04:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Skill
-- =============================================

CREATE PROCEDURE [dbo].[Delete_Skill]
(
	@ID_Skill bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Master_Skills] WHERE (([ID_Skill] = @ID_Skill))

UPDATE [Master_Skills] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_Skill] = @ID_Skill))
GO
/****** Object:  StoredProcedure [dbo].[Get_Skills_All]    Script Date: 06/24/2013 15:04:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET all Skills
-- =============================================

CREATE PROCEDURE [dbo].[Get_Skills_All]
AS
	SET NOCOUNT ON;
SELECT     ID_Skill, Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By
FROM         Master_Skills

WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Add_Skill]    Script Date: 06/24/2013 15:04:06 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Skill
-- =============================================

CREATE PROCEDURE [dbo].[Add_Skill]
(
	@Skill varchar(50),
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Master_Skills] ([Skill], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@Skill, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
SELECT ID_Skill, Skill, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Master_Skills WHERE (ID_Skill = SCOPE_IDENTITY())
GO
/****** Object:  StoredProcedure [dbo].[Jobs_GetAssignedJob]    Script Date: 06/24/2013 15:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 31 Dec 2009
-- Description:	Grt Assigned jobs
-- =============================================

CREATE PROCEDURE [dbo].[Jobs_GetAssignedJob]
(
	@Option int = 0,
	@SearchCriteria varchar(500) = null
)
AS
	SET NOCOUNT OFF; 
 
IF @Option = 0 
	BEGIN
		SELECT * FROM AssignedJobs
	END
ELSE IF @Option = 1
	BEGIN
		SELECT * FROM AssignedJobs Where IsActive = 1
	END
ELSE IF @Option = 2
	BEGIN
		SELECT * FROM AssignedJobs Where IsCompleted = 1
	END
ELSE IF @Option = 3
	BEGIN
		SELECT AssignedJobs.*,isnull(Assets.Name,'N/A') as AssetName,Assets.TagID,
		ISNULL(Assets.ID_Location,0) AS ID_Location,isnull(Assets.ID_Reader,0) as ID_Reader,
		Readers.Reader as ReaderName,Readers.IpAddress as ReaderIpAddress
		FROM AssignedJobs 
		LEFT OUTER JOIN Assets
		ON AssignedJobs.ID_Asset = Assets.ID_Asset
		LEFT OUTER JOIN Readers
		ON Assets.ID_Reader = Readers.ID_Reader
		Where IsActive = 1 AND IsCompleted = 0 AND (Assets.TagID is not null AND Assets.TagID <> '')
		--AND Readers.IpAddress <> '00.00.00.00'
	END
ELSE IF @Option = 4
	BEGIN
	DECLARE @SQL Varchar(1000)
		SET @SQL = 'SELECT ID,JobID,Assets.Name,Assets.TagID,IsActive,IsCompleted FROM AssignedJobs
					LEFT OUTER JOIN Assets
					ON AssignedJobs.ID_Asset = Assets.ID_Asset'
		IF @SearchCriteria IS NOT NULL
			SET @SQL = @SQL + ' Where ' + @SearchCriteria
		EXEC(@SQL)
	END

 
 -- exec [Jobs_GetAssignedJob] 3
GO
/****** Object:  StoredProcedure [dbo].[Add_AssetMovement]    Script Date: 06/24/2013 15:03:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified     Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To ADD a AssetMovemnent
-- =============================================


CREATE PROCEDURE [dbo].[Add_AssetMovement]
(
	@ID_Asset bigint,
	@ID_Location bigint,
	@Last_Modified_By bigint,
	@Date_Modified datetime,
	@ID_AssetStatus bigint,
	@ReferenceNo nvarchar(150)=''
)
AS
	SET NOCOUNT OFF;
INSERT INTO [Asset_Movement] ([ID_Asset], [ID_Location], [Last_Modified_By], [Date_Modified], [ID_Reader], [ID_AssetStatus], [ReferenceNo]) VALUES (@ID_Asset, @ID_Location, @Last_Modified_By, @Date_Modified, 0, @ID_AssetStatus,@ReferenceNo);
	
SELECT ID_AssetMovement, ID_Asset, ID_Location, TimeStamp, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ReferenceNo FROM Asset_Movement WHERE (ID_AssetMovement = SCOPE_IDENTITY())






--#######################################################################################################################
--ALTER PROCEDURE [dbo].[Add_AssetMovement]
--(
--	@ID_Asset bigint,
--	@ID_Location bigint,
--	@TimeStamp datetime,
--	@Date_Created datetime,
--	@Date_Modified datetime,
--	@Is_Deleted bit,
--	@Is_Active bit,
--	@Last_Modified_By bigint
--)
--AS
--	SET NOCOUNT OFF;
--INSERT INTO [Asset_Movement] ([ID_Asset], [ID_Location], [TimeStamp], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@ID_Asset, @ID_Location, @TimeStamp, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
--	
--SELECT ID_AssetMovement, ID_Asset, ID_Location, TimeStamp, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Asset_Movement WHERE (ID_AssetMovement = SCOPE_IDENTITY())
--
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetMovement_By_ID]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Author:		Asif Ali
-- Modified		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To GET a AssetMovement by ID_AssetMovement
-- ==========================================================

CREATE PROCEDURE [dbo].[Get_AssetMovement_By_ID]
	@ID_AssetMovement bigint
AS
	SET NOCOUNT ON;
SELECT     ID_AssetMovement, ID_Asset, ID_Location, TimeStamp, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ReferenceNo
FROM         Asset_Movement
WHERE		ID_AssetMovement=@ID_AssetMovement
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Vernon_Get_AssetMovement]    Script Date: 06/24/2013 15:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ==========================================================
-- Modified		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To GET a AssetMovement by ID_AssetMovement
-- Exec [dbo].[Vernon_Get_AssetMovement]
-- ==========================================================

CREATE PROCEDURE [dbo].[Vernon_Get_AssetMovement]
AS

SET NOCOUNT ON;
CREATE TABLE #VernonData 
(
	DateValue nvarchar(50),
	TimeValue nvarchar(50),
	ReasonNo nvarchar(100),
	ReferenceNo nvarchar(100),
	Notes nvarchar(150),
	AssetNo nvarchar(50) ,
	PartCode nvarchar(50) ,
	[Name] nvarchar(250) ,
	OLDLocationNo nvarchar(50),
	FoundFlag nvarchar(50) ,
	LocationNo nvarchar(50),
	createfield nvarchar(50),
	EmployeeNo nvarchar(150) 

--	TagID nvarchar(50),
--	CurrentLocation nvarchar(250),
--	Date_Modified nvarchar(150),
	
) 

Insert into #VernonData
SELECT (
	cast(DATEPART(dd,AM.Date_Modified) as nvarchar(2)) + ' ' +
	cast(DATEPART(mm,AM.Date_Modified) as nvarchar(2)) + ' ' +
	cast(DATEPART(yyyy,AM.Date_Modified) as nvarchar(4))) as DateValue,
	   (
	cast(DATEPART(hh,AM.Date_Modified) as nvarchar(2)) + ':' +
	cast(DATEPART(mi,AM.Date_Modified) as nvarchar(2)) + ':' +
	cast(DATEPART(ss,AM.Date_Modified) as nvarchar(2))) as TimeValue,
	isNull(R.ReasonNo,'') as ReasonNo,
	isNull(AM.ReferenceNo,'') as ReferenceNo,
	'' as Notes,
	A.AssetNo,
	isNULL(A.PartCode,0) as PartCode,A.Name,
	isNULL(OL.LocationNo,'') as OldLocationNo,
	'' as FoundFlag,CL.LocationNo,
	'' as createfield,
	isNull(EMP.EmpNo,'') as EmployeeNo 
--	,A.TagID,CL.Name as CurrentLocation,convert(nvarchar(50),AM.Date_Modified,121) as Date_Modified
	FROM  Asset_Movement AM
	Inner Join Assets A on A.ID_Asset=AM.ID_Asset 
	Inner Join Locations CL on AM.ID_LocationNEW=CL.ID_Location
	Left Outer Join Locations OL on AM.ID_Location=OL.ID_Location 
	Left Outer Join Reasons R on AM.ID_Reason=R.ID_Reason
	Left Outer Join Employees Emp on Emp.ID_Employee=AM.Last_Modified_By  --ID_Employee
where AM.IS_VernonRead=0 AND A.Is_Deleted=0 AND A.Is_Active=1 
Order by AM.Date_Modified

update Asset_Movement set Is_VernonRead=1 
where IS_VernonRead=0 AND Is_Deleted=0 AND Is_Active=1 

select * from #VernonData
drop table #VernonData

/*
Select * from Asset_Movement AM where
AM.IS_VernonRead=0 AND 
AM.Is_Deleted=0 AND AM.Is_Active=1

select * from Assets where ID_Asset=177

select * from locations
Select * from Reasons
select convert(nvarchar(50),Date_Modified,121) as p from Assets

select * from employees

Update Asset_Movement set IS_VernonRead=0 where ID_AssetMovement>=4410
--select * from Asset_Movement where ID_AssetMovement=4410
Exec [dbo].[Vernon_Get_AssetMovement]



*/
GO
/****** Object:  StoredProcedure [dbo].[Delete_AssetMovement]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a AssetMovement
-- =============================================

CREATE PROCEDURE [dbo].[Delete_AssetMovement]
(
	@ID_AssetMovement bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Asset_Movement] WHERE (([ID_AssetMovement] = @ID_AssetMovement))

UPDATE [Asset_Movement] SET Is_Deleted=1, Date_Modified=getdate()
WHERE (([ID_AssetMovement] = @ID_AssetMovement))
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeInStation]    Script Date: 06/24/2013 15:04:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetTimeInStation] 
(	-- Add the parameters for the function here
	@ID_AssetMovement BIGINT,
	@option int = 0	
)
RETURNS VarChar(10)
AS
BEGIN
	-- Declare the return variable here
	DECLARE @InDate DateTime,
			@OutDate DateTime,	
			@ID_Reader BigInt,
			@TimeOut Varchar(10),
			@TotalSecs BigInt,					 
			@hours int,
			@mins int		 
 
	SET @InDate = GETDATE();
	SET @OutDate = @InDate 

	SET @hours = 0;
	 
	SELECT @InDate = [TimeStamp],@OutDate = ISNULL([TimeStampOut],[TimeStamp]) FROM Asset_Movement
	WHERE   ID_AssetMovement = @ID_AssetMovement 

IF(@option = 0)				
	BEGIN
		IF (@InDate IS NOT NULL) AND (@InDate < @OutDate)
			BEGIN
				SET  @TotalSecs = DateDiff(ss,@InDate,@OutDate) 
				
					SET @hours = @TotalSecs/3600

						SET @TotalSecs = (@TotalSecs%3600)

					   SET @mins = @TotalSecs/60

						SET @TotalSecs = @TotalSecs%60
						
							--SET @TimeOut = RIGHT('00' + CAST(@hours AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(@mins AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(@TotalSecs AS VARCHAR(2)),2) 
					
						SET @TimeOut =  CAST(@hours AS VARCHAR) + ':' + RIGHT('00' + CAST(@mins AS VARCHAR(2)),2) + ':' + RIGHT('00' + CAST(@TotalSecs AS VARCHAR(2)),2) 
							
						--	SET @TimeOut = RIGHT('00' + CAST(@mins AS VARCHAR(5)),5) + ':' + RIGHT('00' + CAST(@TotalSecs AS VARCHAR(2)),2) 
					
							--SET @TimeOut = CAST(@TotalSecs  AS Varchar)
			  END						
			  
		ELSE
			SET @TimeOut = '000:00:00';
			---SET @TimeOut = '0';		
	END
ELSE
	BEGIN
		Declare @TSecs float
		IF (@InDate IS NOT NULL) AND (@InDate < @OutDate)
			BEGIN
				SET  @TotalSecs = DateDiff(ss,@InDate,@OutDate)
				set @TSecs = @TotalSecs / (24 * 3600)
				
				--SET @TimeOut = CAST(@TSecs AS VARCHAR(6))
				SET @TimeOut = CAST(@TotalSecs  AS Varchar)
			END
		ELSE
			SET @TimeOut = '0'; 
			
	END

	RETURN @TimeOut;
	
END

-------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[MR_Update_Asset_AssetMovement]    Script Date: 06/24/2013 15:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Modified		Deepanshu Jouhari
-- Create date: <31st July 2008>
-- Description:	<Update Asset And Asset Movement>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Update_Asset_AssetMovement] 
	-- Add the parameters for the stored procedure here
(
@ID_Type int,
@ID bigint,
@ID_Employee bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF(@ID_Type = 1)
		UPDATE ASSETS  SET Last_Modified_By = @ID_Employee,ID_Employee =@ID_Employee WHERE  ID_Asset = @ID
	IF(@ID_Type = 2)
		UPDATE  Asset_Movement  SET Last_Modified_By = @ID_Employee ,ID_Employee=@ID_Employee  WHERE  ID_AssetMovement = @ID

END
GO
/****** Object:  UserDefinedFunction [dbo].[GetNoofReturns]    Script Date: 06/24/2013 15:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetNoofReturns] 
(	-- Add the parameters for the function here
	@JobID Varchar(15),
	@ID_Asset Bigint,
	@ID_Location BigInt 
)
RETURNS Int
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Count Int

	BEGIN	
		SELECT @Count = Count(Id_Asset) FROM Asset_Movement		 
		WHERE ID_Asset = @ID_Asset AND JobID = @JobID AND ID_LocationNew = @ID_Location
    END
	
	RETURN @Count;
	
END
GO
/****** Object:  UserDefinedFunction [dbo].[GetMaxTimeStamp]    Script Date: 06/24/2013 15:04:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetMaxTimeStamp] 
(	-- Add the parameters for the function here
	@ID_AssetMovement BIGINT,
	@In_Out int
	 
)
RETURNS DateTime
AS
BEGIN
	-- Declare the return variable here
	DECLARE @DueDate DateTime
 
   IF @In_Out = 0
	 SELECT @DueDate = [TimeStamp] FROM Asset_Movement WHERE ID_AssetMovement = @ID_AssetMovement
   ELSE
	  SELECT @DueDate = [TimeStampOut] FROM Asset_Movement WHERE ID_AssetMovement = @ID_AssetMovement 		
	
	RETURN @DueDate;
	
END
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetMovement_All]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To GET all AssetMovement
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetMovement_All]
AS
	SET NOCOUNT ON;
SELECT     ID_AssetMovement, ID_Asset, ID_Location, TimeStamp, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,ReferenceNo
FROM         Asset_Movement

WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Update_AssetMovement]    Script Date: 06/24/2013 15:04:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
--Modified		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To UPDATE a AssetMovement 
-- =============================================

CREATE PROCEDURE [dbo].[Update_AssetMovement]
(
	@ID_Asset bigint,
	@ID_Location bigint,
	@TimeStamp datetime,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@ID_AssetMovement bigint,
	@ReferenceNo nvarchar(150)=''  
)
AS
	SET NOCOUNT OFF;

UPDATE [Asset_Movement] SET [ID_Asset] = @ID_Asset, [ID_Location] = @ID_Location, [TimeStamp] = @TimeStamp, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By,[ReferenceNo]=@ReferenceNo   WHERE (([ID_AssetMovement] = @ID_AssetMovement));
	
SELECT ID_AssetMovement, ID_Asset, ID_Location, TimeStamp, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ReferenceNo FROM Asset_Movement WHERE (ID_AssetMovement = @ID_AssetMovement)
GO
/****** Object:  UserDefinedFunction [dbo].[GetTimeIn_Station]    Script Date: 06/24/2013 15:04:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Vijay Mangteni>
-- Create date: <02 01 2010>
-- Description:	 
-- =============================================
CREATE FUNCTION [dbo].[GetTimeIn_Station] 
(	-- Add the parameters for the function here
	@ID_LocationIn BIGINT,
	@JobId varchar(20),
	@FromDate Varchar(10),
	@ToDate Varchar(10),
	@Sum int = 1
)
RETURNS BigInt
AS
BEGIN
	-- Declare the return variable here
	DECLARE  @TotalSecs BigInt	
	
IF(@Sum = 1)				
	BEGIN  
		select @TotalSecs = ISNULL(SUM(DATEDIFF(ss,[TimeStamp],[TimeStampOut])),0) from Asset_Movement 
		WHERE JobID = @JobId AND id_locationnew = @ID_LocationIn
		AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate	
	END
ELSE
	BEGIN  
		SET @TotalSecs = (select Top 1 ISNULL(DATEDIFF(ss,[TimeStamp],[TimeStampOut]),0) from Asset_Movement 
							WHERE JobID = @JobId AND id_locationnew = @ID_LocationIn
						AND Convert(varchar(10),Asset_movement.Date_Modified,101) between @FromDate AND @ToDate)
	END

	RETURN @TotalSecs;
	
END

-------------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Update_Task]    Script Date: 06/24/2013 15:04:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 14 May 2009
-- Description:	To UPDATE a Task
-- =============================================

CREATE PROCEDURE [dbo].[Update_Task]
(
	@ID_Tasks bigint,
	@Title nvarchar(300),
	@Description varchar(500),
	@Date_Modified datetime=getDate,
	@Is_Deleted bit=0,
	@Is_Active bit=1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

	UPDATE [Tasks] SET [Title] = @Title, [Description] = @Description,
	[Date_Modified] = @Date_Modified,
	[Is_Deleted] = @Is_Deleted,
	[Is_Active] = @Is_Active, 
	[Last_Modified_By] = @Last_Modified_By 
	WHERE (([ID_Tasks] = @ID_Tasks));

	
	SELECT * FROM Tasks WHERE (ID_Tasks = @ID_Tasks)
GO
/****** Object:  StoredProcedure [dbo].[Add_Tasks]    Script Date: 06/24/2013 15:04:07 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 14/5/2000
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[Add_Tasks] 
	-- Add the parameters for the stored procedure here
	@Title nvarchar(50),
	@Description nvarchar(300),
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into Tasks (Title,[Description],Last_Modified_By)
    values (@Title,@Description,@Last_Modified_By)
	
	SELECT * FROM Tasks WHERE (ID_Tasks = SCOPE_IDENTITY())

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Tasks_All]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 19-June- 2009
-- Description:	To GET all Tasks
-- =============================================

CREATE PROCEDURE [dbo].[Get_Tasks_All]
AS
	SET NOCOUNT ON;

SELECT     ID_Tasks, Title,Description
FROM         Tasks
WHERE	Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_TaskDescByID]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ===================================================
-- Author:		Vinay Bansal
-- Create date: 25-June-2009
-- Description:	To GET a Description of Task
-- ===================================================

CREATE PROCEDURE [dbo].[Get_TaskDescByID]
	@ID_Tasks bigint
AS
	SET NOCOUNT ON;
SELECT     ISNULL([Description],'') as [Description]
FROM       Tasks
WHERE		ID_Tasks=@ID_Tasks
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_Task_By_ID]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To GET a Tasks by ID_Tasks
-- =============================================

CREATE PROCEDURE [dbo].[Get_Task_By_ID]
	@ID_Tasks bigint
AS
	SET NOCOUNT ON;
SELECT *
FROM        Tasks
WHERE		ID_Tasks=@ID_Tasks
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Add_FSAssets]    Script Date: 06/24/2013 15:04:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 22-June-2009
-- Description:	To Assign Assest to FS
-- =============================================

CREATE PROCEDURE [dbo].[Add_FSAssets]  
@ID_Asset BIGINT,
@ID_Template bigint,
@StartDate Datetime,
@EndDate Datetime,
@TaskIDs nvarchar(Max),
@DateCreated DateTime,
@Last_Modified_By bigint
AS
Begin
	--SET NOCOUNT ON;
	Declare @ID_FSAssets bigint
	Declare @SQl nvarchar(Max)	
	Declare @SQLStatus nvarchar(Max)	
	-- Check Asset, Template exist or not
	--IF NOT EXISTS (Select 1 FROM FS_Assets WHERE ID_Asset = @ID_Asset AND ID_Template = @ID_Template AND IS_Active = 1 AND IS_Deleted =0  )
	IF NOT EXISTS (Select 1 FROM FS_AssetStatus WHERE ID_Asset = @ID_Asset AND ID_Template = @ID_Template AND IS_Active = 1 AND IS_Deleted =0 and (FSSTATUS=1010 or FSSTATUS=1020))
	BEGIN
		-- Insert into Fs_Assetes
		INSERT INTO FS_Assets (ID_Asset,ID_Template, StartDate,EndDate,Last_Modified_By,Date_Created,Date_Modified)
		Values (@ID_Asset,@ID_Template, @StartDate,@EndDate,@Last_Modified_By,@DateCreated,@DateCreated)
		Select @ID_FSAssets = Scope_Identity()
	
		-- Insert Into FS_AssetDetails By All the tasks 
		SET @SQL = 'INSERT INTO FS_AssetDetails (ID_FSAsset,ID_Tasks, IS_Active,Is_Deleted,Last_Modified_By,Date_Modified)
					SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks, 1,0,'+ Cast(@Last_Modified_By as nvarchar) 
					+' , GetDate() From Tasks 
					Where ID_Tasks IN ('+ @TaskIDs +')
					OR ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')'
					--Print @SQL
		Exec sp_executesql @SQL
		
		-- Insert Into [FS_AssetStatus] By All the tasks
		SET @SQLStatus = 'INSERT INTO [FS_AssetStatus] ([ID_FSAsset],[ID_Tasks],[ID_Asset],[ID_Template],[DueDate],[TaskStatus],[FSStatus],[IS_Active],[IS_Deleted],[Last_Modified_By],[Date_Created],[Date_Modified])     
		   			  SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks,'+Cast(@ID_Asset as nvarchar) +
				      ','+ Cast(@ID_Template as nvarchar) +			
					  ', cast('''+ CONVERT(varchar(10),(@StartDate), 101) +			
					  ''' As Datetime) ,1010,1010,1,0,'+ Cast(@Last_Modified_By as nvarchar) 
					  +', GetDate(), GetDate() From Tasks 
					  Where ID_Tasks IN ('+ @TaskIDs +')
					  OR ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')'
	
		Exec sp_executesql @SQLStatus
	END
	--ELSE IF NOT EXISTS (Select 1 FROM FS_AssetStatus WHERE ID_Asset = @ID_Asset AND ID_Template = @ID_Template AND IS_Active = 1 AND IS_Deleted =0 and (FSSTATUS<>1010 and FSSTATUS<>1020))
	ELSE
	BEGIN

		
		-- Update FS_Assets if Already Exists
		Update FS_Assets 
		Set StartDate = @StartDate , EndDate=@EndDate, Date_Modified = @DateCreated
		WHERE ID_Asset = @ID_Asset AND ID_Template = @ID_Template AND IS_Active = 1 AND IS_Deleted =0 
		SELECT @ID_FSAssets = ID_FSAsset
		FROM FS_Assets WHERE ID_Asset = @ID_Asset 
		AND ID_Template = @ID_Template AND IS_Active = 1 AND IS_Deleted =0 
		
		-- Insert Only new task which are not present previously
		SET @SQl=''
		SET @SQL = '
					INSERT INTO FS_AssetDetails (ID_FSAsset,ID_Tasks, IS_Active,Is_Deleted,Last_Modified_By,Date_Modified)
					SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks, 1,0,'+ Cast(@Last_Modified_By as nvarchar) 
					+' , GetDate() From Tasks 
					Where (ID_Tasks IN ('+ @TaskIDs +')
					OR ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +'))
					AND ID_Tasks NOT IN (Select Id_Tasks From FS_AssetDetails Where ID_FSAsset =' + Cast(@ID_FSAssets as nvarchar) +')'
					--Print @SQL
		-- Update [FS_AssetStatus] due date if Due date is greater than NEw startdate
		-- Update [FS_AssetStatus] task status if Due date is smaller than NEw startdate and insert new record
		-- Insert Only new task which are not present previously
		SET @SQLStatus = ''
		SET @SQLStatus = 'Update [FS_AssetStatus] set [DueDate] = cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime)	
						  WHERE (ID_Tasks IN (Select Id_Tasks From FS_tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
						  OR 	ID_Tasks IN ('+ @TaskIDs +'))
						  AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'
						  AND 	[DueDate] > cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime) 
						  AND FsStatus = 1010 AND TaskStatus = 1010 -- Status New
	
						  --
						  Update [FS_AssetStatus] set [TaskStatus] = 1050 , FSStatus=9999
						  WHERE ( ID_Tasks IN (Select Id_Tasks From FS_tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')	
						  OR ID_Tasks IN ('+ @TaskIDs +')	)
						  AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'
						  AND [DueDate] < cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime) 
						  AND FsStatus = 1010 AND TaskStatus = 1010 -- Status New
	
						  INSERT INTO [FS_AssetStatus] ([ID_FSAsset],[ID_Tasks],[ID_Asset],[ID_Template],[DueDate],[TaskStatus],[FSStatus],[IS_Active],[IS_Deleted],[Last_Modified_By],[Date_Created],[Date_Modified])     
		   				  SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks,'+Cast(@ID_Asset as nvarchar) +
						  ','+ Cast(@ID_Template as nvarchar) +			
						  ', cast('''+ CONVERT(varchar(10),(@StartDate), 101) +			
						  ''' As Datetime) ,1010,1010,1,0,'+ Cast(@Last_Modified_By as nvarchar) 
						  +', GetDate(), GetDate() From	[FS_AssetStatus]
						  WHERE ID_Tasks IN (Select Id_Tasks From FS_AssetDetails Where ID_FSAsset =' + Cast(@ID_FSAssets as nvarchar) +')
					      AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'
						  AND [DueDate] < cast('''+ CONVERT(varchar(10),(@StartDate), 101) +''' As Datetime) 
						  AND FsStatus = 9999 -- Status New

						  Update [FS_AssetStatus] set [FSStatus] = 1050 where FSStatus=9999
						  AND ID_Asset = '+ Cast(@ID_Asset as nvarchar) + '
						  AND ID_Template =	'+ Cast(@ID_Template as nvarchar) +	'	

						  INSERT INTO [FS_AssetStatus] ([ID_FSAsset],[ID_Tasks],[ID_Asset],[ID_Template],[DueDate],[TaskStatus],[FSStatus],[IS_Active],[IS_Deleted],[Last_Modified_By],[Date_Created],[Date_Modified])     
		   				  SELECT '+ Cast(@ID_FSAssets as nvarchar) + ',ID_Tasks,'+Cast(@ID_Asset as nvarchar) +
						  ','+ Cast(@ID_Template as nvarchar) +			
						  ', cast('''+ CONVERT(varchar(10),(@StartDate), 101) +			
						  ''' As Datetime) ,1010,1010,1,0,'+ Cast(@Last_Modified_By as nvarchar) 
						  +', GetDate(), GetDate() From Tasks 
						  Where ID_Tasks IN ('+ @TaskIDs +')
						  OR ID_Tasks IN (Select Id_Tasks From FS_Tasks Where ID_Template =' + Cast(@ID_Template as nvarchar) +')
						  AND ID_Tasks NOT IN (Select Id_Tasks From FS_AssetDetails Where ID_FSAsset =' + Cast(@ID_FSAssets as nvarchar) +')'
							
		Exec sp_executesql @SQLStatus
		Exec sp_executesql @SQL
	END 
End
GO
/****** Object:  StoredProcedure [dbo].[DeleteAssetsByRefNo]    Script Date: 06/24/2013 15:04:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[DeleteAssetsByRefNo]  
	@RefNo varchar(150),
	@EmpId	BigInt = null
AS
	SET NOCOUNT ON;
BEGIN
	DECLARE @Count Int 

if @EmpId IS NULL
	BEGIN
		SET @Count = (SELECT COUNT(ID_Asset) FROM Assets WHERE ReferenceNo = @RefNo
						AND Is_Deleted = 0)
		
		UPDATE Assets SET Is_Deleted = 1,Is_Active = 0,TagID = 'xxxxxxxxxxxxxxxxxxxxxxxx'
		WHERE ReferenceNo = @RefNo
	END
ELSE
	BEGIN
			SET @Count = (SELECT COUNT(ID_Asset) FROM Assets WHERE ReferenceNo = @RefNo AND Last_Modified_By = @EmpId
						AND Is_Deleted = 0)
		
		UPDATE Assets SET Is_Deleted = 1,Is_Active = 0, TagID = 'xxxxxxxxxxxxxxxxxxxxxxxx'
		WHERE ReferenceNo = @RefNo AND Last_Modified_By = @EmpId
	END
	SELECT @Count;
END
GO
/****** Object:  StoredProcedure [dbo].[Delete_Asset]    Script Date: 06/24/2013 15:04:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset
-- Modified by Vijay : - To also change the TagID with dummy value on deleting the items
-- Modified on : - 11-11-11
-- =============================================


CREATE PROCEDURE [dbo].[Delete_Asset]
(
	@ID_Asset bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Assets] WHERE (([ID_Asset] = @ID_Asset))

begin try
	begin transaction
		UPDATE [Assets] SET Is_Deleted=1, Date_Modified=getdate(), TagID = 'xxxxxxxxxxxxxxxxxxxxxxxx' 
		WHERE (([ID_Asset] = @ID_Asset))

		UPDATE FS_AssetStatus set IS_Deleted=1, Date_Modified=getdate()  
		where (([ID_Asset] = @ID_Asset)) and (FSStatus=1010 or FSStatus=1020)

--		DELETE FROM Assets WHERE  [ID_Asset] = @ID_Asset
--
--		DELETE FROM Alert WHERE  [ID_Asset] = @ID_Asset
--		
--		DELETE FROM Asset_Movement WHERE  [ID_Asset] = @ID_Asset  
--
--		DELETE FROM AssignedJobs WHERE  [ID_Asset] = @ID_Asset 
--
--		DELETE FROM FS_AssetDetails WHERE ID_FSAsset IN (SELECT ID_FSAsset FROM FS_Assets WHERE [ID_Asset] = @ID_Asset)
--		
--		DELETE FROM FS_Assets WHERE  [ID_Asset] = @ID_Asset
--
--		DELETE FROM FS_AssetStatus  WHERE (([ID_Asset] = @ID_Asset)) AND (FSStatus=1010 or FSStatus=1020)

		

	commit transaction
end try
begin catch
 IF (XACT_STATE()) = -1
    BEGIN
		rollback transaction
	END;
end catch
GO
/****** Object:  StoredProcedure [dbo].[GetAssetsCount]    Script Date: 06/24/2013 15:04:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[GetAssetsCount]
AS
	SET NOCOUNT ON;
Declare @acount bigint;

SET @acount = (SELECT  Count(*) from Assets where Is_Deleted = 0 AND Is_Active = 1)

--print @acount;

select @acount;
GO
/****** Object:  StoredProcedure [dbo].[Get_Assets_All]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified:	Deepanshu Jouhari
-- Create date: 19th March 2008
-- Description:	To GET all Assets
-- =============================================

CREATE PROCEDURE [dbo].[Get_Assets_All]
AS
	SET NOCOUNT ON;
SELECT     ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, 
                      ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,ReferenceNo
FROM         Assets

WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetNames_All]    Script Date: 06/24/2013 15:04:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Create date: 10-June-2009
-- Description:	To GET all AssetNames
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetNames_All]
AS
	SET NOCOUNT ON;

SELECT     '-1' as TAGID, '-- Select -- ' as [Name]

UNION

SELECT     TAGID, [Name]
FROM       Assets
WHERE	 Is_Deleted=0 AND Is_Active=1
ORDER BY [Name]
GO
/****** Object:  StoredProcedure [dbo].[S_Get_AssetID_By_AssetNo]    Script Date: 06/24/2013 15:04:36 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ====================================================
-- Author:		Cherry Jain
-- Create date: 16 June 2008
-- Description:	To GET a AssetID by Asset Number
-- =====================================================

CREATE PROCEDURE [dbo].[S_Get_AssetID_By_AssetNo] --'0000'
	@AssetNo varchar(50)
AS
	SET NOCOUNT ON;
IF EXISTS (SELECT     ID_Asset FROM  Assets WHERE   AssetNo=@AssetNo)
	SELECT     ID_Asset FROM  Assets WHERE   AssetNo=@AssetNo
ELSE
	SELECT  0 as   ID_Asset
GO
/****** Object:  StoredProcedure [dbo].[Get_Asset_By_ID]    Script Date: 06/24/2013 15:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified     Deepanshu Jouhari
-- Modified By : Vinay Bansal ON 12-June- 2009	
-- Create date: 19th March 2008
-- Description:	To GET a Asset By ID_Asset
-- =============================================

CREATE PROCEDURE [dbo].[Get_Asset_By_ID] 
	@ID_Asset bigint
AS
	SET NOCOUNT ON;
SELECT     ID_Asset, TagID, Name, AssetNo, Description, ImageName, 
			CONVERT(varchar(20), PurchaseDate, 103) as PurchaseDate , ID_TagType, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, 
                      ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ID_Employee, ReferenceNo
			,LotNo,Isnull(ExpiryDate,'') as ExpiryDate,Allocation,PartCode,ID_AssetMaster -- Added By Vinay
FROM         Assets
WHERE		ID_Asset=@ID_Asset
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[S_Check_DuplicateAssetNo]    Script Date: 06/24/2013 15:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 16 th June 2008
-- Description:	To CHECK for Duplicate Asset No.
-- =============================================

create PROCEDURE [dbo].[S_Check_DuplicateAssetNo]  
	-- Add the parameters for the stored procedure here
	@AssetNo varchar(50)
	
AS
BEGIN
	if EXISTS(select ID_Asset From dbo.Assets where AssetNo = @AssetNo)
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Asset_By_AssetNo]    Script Date: 06/24/2013 15:04:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Asset_By_AssetNo] --[Get_Asset_By_TagID] '000000000000000000000150'
	@AssetNo varchar(50),
	@PartCode nvarchar(20) = 0
AS
	SET NOCOUNT ON;
SELECT  *
FROM     Assets
WHERE	AssetNo = @AssetNo And isNULL(PartCode,0)=@PartCode AND Is_Deleted=0 AND Is_Active=1

/*
ID_Asset, TagID, Name, AssetNo, Description, ImageName, 
			PurchaseDate , ID_TagType, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, 
                      ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ID_Employee,ReferenceNo
					,LotNo,Isnull(ExpiryDate,'') as ExpiryDate,Allocation,PartCode,ID_AssetMaster,IS_Deleted,IS_Active
*/
GO
/****** Object:  StoredProcedure [dbo].[Get_Asset_By_TagID]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Asset_By_TagID] --[Get_Asset_By_TagID] '000000000000000000000150'
	@TagID varchar(6000),
	@optionCSV int = 0
AS
	SET NOCOUNT ON;

IF @optionCSV = 0
	BEGIN
		SELECT     ID_Asset, TagID, Name, AssetNo, Description, ImageName, 
				PurchaseDate , ID_TagType, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, 
                      ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By, ID_Employee,ReferenceNo
					,LotNo,Isnull(ExpiryDate,'') as ExpiryDate,Allocation,PartCode,ID_AssetMaster
		FROM         Assets
		WHERE		TagID like '%'+ @TagID +'%'
	END
ELSE
	BEGIN
		Declare @SQL varchar(MAX)
		SET @SQL = 'SELECT ID_Asset, TagID, Name,Is_Deleted, Is_Active,ID_TagType,ID_Location,ID_Employee,isnull(Description,'''') as Description FROM Assets WHERE Is_Deleted = 0 AND Is_Active = 1 AND TagID IN (' + @TagID + ')'
		EXEC(@SQL);
	END


-------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[S_Delete_Asset]    Script Date: 06/24/2013 15:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset
-- =============================================


CREATE PROCEDURE [dbo].[S_Delete_Asset] --'TagID Value '
(
	@TagID varchar(50)
	
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Assets] WHERE (([ID_Asset] = @ID_Asset))

UPDATE [Assets] SET Is_Deleted=1, Date_Modified=getdate() 
WHERE LTRIM(RTRIM(TagID)) = LTRIM(RTRIM(@TagID))
GO
/****** Object:  StoredProcedure [dbo].[Get_Asset_By_RefNo]    Script Date: 06/24/2013 15:04:15 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Get_Asset_By_RefNo] --[Get_Asset_By_TagID] '000000000000000000000150'
	@RefNo varchar(150)
AS
	SET NOCOUNT ON;
SELECT     ID_Asset, TagID, [Name], ReferenceNo
FROM         Assets
WHERE	ReferenceNo =   @RefNo  
AND Is_active = 1 AND Is_deleted = 0

--ReferenceNo like '%'+ @RefNo +'%'
-- exec [Get_Asset_By_RefNo] 'weryuiop'
GO
/****** Object:  StoredProcedure [dbo].[S_Check_DuplicateAssetNo_forUPDATE]    Script Date: 06/24/2013 15:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 16 th June 2008
-- Description:	To CHECK for Duplicate Asset No.
-- =============================================

create PROCEDURE [dbo].[S_Check_DuplicateAssetNo_forUPDATE]  
	-- Add the parameters for the stored procedure here
	@AssetNo varchar(50),
	@ID_Asset bigint
	

	
AS
BEGIN

if EXISTS(select ID_Asset From dbo.Assets where AssetNo = @AssetNo AND ID_Asset <> @ID_Asset)
		select cast (1 as bit);	
	else 
		select cast (0 as bit);	

END
GO
/****** Object:  StoredProcedure [dbo].[Get_AssetsFS_All]    Script Date: 06/24/2013 15:04:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 21-June-2009
-- Description:	To GET all Assets
-- =============================================

CREATE PROCEDURE [dbo].[Get_AssetsFS_All]
AS
	SET NOCOUNT ON;
select -1 as ID_Asset, '--- All ---' as [Name] union
SELECT     ID_Asset, [Name]
FROM         Assets

WHERE	 Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset_LastModified]    Script Date: 06/24/2013 15:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To DELETE a Asset
-- =============================================


Create PROCEDURE [dbo].[Update_Asset_LastModified]
(
	@ID_Asset bigint,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
--DELETE FROM [Assets] WHERE (([ID_Asset] = @ID_Asset))

UPDATE [Assets] SET  Last_Modified_By=@Last_Modified_By
WHERE (([ID_Asset] = @ID_Asset))
GO
/****** Object:  StoredProcedure [dbo].[Sync_WriteAsset_TagID]    Script Date: 06/24/2013 15:04:41 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 24 June 2008
-- Description:	Update Tag ID of Asset
-- =============================================

CREATE PROCEDURE [dbo].[Sync_WriteAsset_TagID] --'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(50),
	@ID_Asset bigint,
	@ID_Employee bigint
)
AS
	SET NOCOUNT OFF;


UPDATE [Assets] SET [TagID] = @TagID ,
[ID_Employee]=@ID_Employee,
[Date_Modified]=getDate()
 WHERE (([ID_Asset] = @ID_Asset));







SELECT ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Assets WHERE (ID_Asset = @ID_Asset)
GO
/****** Object:  StoredProcedure [dbo].[Get_FSAssetStatus]    Script Date: 06/24/2013 15:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 23 May 2009
-- Description:	To UPDATE a Task
/*			New = 1010,
            InProcess = 1020,
            Completed = 1030,
            Expired=1040,
            Cancel = 1050    
*/
-- =============================================

CREATE PROCEDURE [dbo].[Get_FSAssetStatus]
(
	@ID_FSAssetStatus bigint
)
AS
SET NOCOUNT ON;
BEGIN
	Select * from [dbo].[FS_AssetStatus] 
    WHERE [ID_FSAssetStatus] = @ID_FSAssetStatus;
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_AlertType_All]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <26 July 2008>
-- Description:	<GET Alert Type>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Get_AlertType_All]
AS
	SET NOCOUNT ON;

SELECT  -1 as     [ID_AlertType]
      ,'---Select---'[Alert_Name]
      ,'' as [Description]
      ,getdate() as [Date_Created]
      ,getdate() as [Date_Modified]
      ,0 as [Is_Deleted]
      ,1 as[Is_Active]
      ,0 as[Last_Modified_By]

UNION

SELECT      [ID_AlertType]
      ,[Alert_Name]
      ,[Description]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,[Last_Modified_By]
FROM         Alert_Type
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_AlertType_By_ID_AlertType]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <26 July 2008>
-- Description:	<GET Alert Type>
-- =============================================
Create PROCEDURE [dbo].[MR_Get_AlertType_By_ID_AlertType]

(
@ID_AlertType BIGINT
)
AS
	SET NOCOUNT ON;


SELECT      [ID_AlertType]
      ,[Alert_Name]
      ,[Description]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,[Last_Modified_By]
FROM         Alert_Type
WHERE ID_AlertType = @ID_AlertType
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_RSSIRange_ByTagTypeAndReader]    Script Date: 06/24/2013 15:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <24 July 2008>
-- Description:	<Get RSSI Range by TagType ID and Reader ID>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Get_RSSIRange_ByTagTypeAndReader]
	-- Add the parameters for the stored procedure here
(
@ID_TagType BIGINT,
@ID_Reader BIGINT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT [ID_RSSIRange]
      ,RR.[ID_TagType]
      ,RR.[ID_Reader]
      ,[RSSI_Min]
      ,[RSSI_Max]
      ,RR.[Date_Created]
      ,RR.[Date_Modified]
      ,RR.[Is_Deleted]
      ,RR.[Is_Active]
      ,RR.[Last_Modified_By]
	  ,TT.TagType
	  ,Rtrim(RD.Reader) as Reader
  FROM [RSSI_Range] RR
INNER JOIN dbo.Tag_Types TT on TT.ID_TagType = RR.ID_TagType
INNER JOIN dbo.Readers  RD on RD.ID_Reader = RR.ID_Reader
WHERE RR.ID_TagType= @ID_TagType AND RR.ID_Reader = @ID_Reader
AND RR.[Is_Deleted] = 0 AND RR.[Is_Active] =1
END
GO
/****** Object:  StoredProcedure [dbo].[Get_TagTypes]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 2nd May 2008
-- Description:	To GET all TAG Types
-- =============================================

CREATE PROCEDURE [dbo].[Get_TagTypes]
AS
	SET NOCOUNT ON;


	SELECT  
		-1 AS ID_TagType, 
		'--- Select --- ' AS TagType, 
		getdate() AS Date_Created, 
		getdate() AS Date_Modified, 
		0 AS Is_Deleted, 
		1 AS Is_Active, 
		0 AS Last_Modified_By
	
	FROM    Tag_Types

UNION


	SELECT
		ID_TagType,
		TagType,
		Date_Created,
		Date_Modified,
		Is_Deleted,
		Is_Active,
		Last_Modified_By

	FROM Tag_Types

	WHERE Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_TageType_All]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <24 July 2008>
-- Description:	<Select Tag Type>
-- =============================================
CREATE PROCEDURE [dbo].[Get_TageType_All]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here


SELECT     -1 as ID_TagType,'--- Select --- ' as TagType, 
			getdate() as Date_Created, getdate() as Date_Modified, 0 as Is_Deleted, 1 as Is_Active, 0 as Last_Modified_By
 

UNION

	SELECT [ID_TagType]
      ,[TagType]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,[Last_Modified_By]
  FROM  [Tag_Types]
Where [Is_Deleted] = 0 AND [Is_Active] =1
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_RSSIRange_By_IDRSSI]    Script Date: 06/24/2013 15:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <25 July 2008>
-- Description:	<Get RSSI Range by RSSI ID>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Get_RSSIRange_By_IDRSSI]
	-- Add the parameters for the stored procedure here
(
@ID_RSSIRange BIGINT
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
SELECT [ID_RSSIRange]
      ,RR.[ID_TagType]
      ,RR.[ID_Reader]
      ,[RSSI_Min]
      ,[RSSI_Max]
      ,RR.[Date_Created]
      ,RR.[Date_Modified]
      ,RR.[Is_Deleted]
      ,RR.[Is_Active]
      ,RR.[Last_Modified_By]
	  ,TT.TagType
	  ,Rtrim(RD.Reader) as Reader
  FROM [RSSI_Range] RR
INNER JOIN dbo.Tag_Types TT on TT.ID_TagType = RR.ID_TagType
INNER JOIN dbo.Readers  RD on RD.ID_Reader = RR.ID_Reader
WHERE RR.[Is_Deleted] = 0 AND RR.[Is_Active] =1 AND ID_RSSIRange =@ID_RSSIRange
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_RSSIRange_All]    Script Date: 06/24/2013 15:04:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <24 July 2008>
-- Description:	<Get RSSI Range by TagType ID and Reader ID>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Get_RSSIRange_All]
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	SELECT [ID_RSSIRange]
      ,RR.[ID_TagType]
      ,RR.[ID_Reader]
      ,[RSSI_Min]
      ,[RSSI_Max]
      ,RR.[Date_Created]
      ,RR.[Date_Modified]
      ,RR.[Is_Deleted]
      ,RR.[Is_Active]
      ,RR.[Last_Modified_By]
	  ,TT.TagType
	  ,Rtrim(RD.Reader) as Reader
  FROM [RSSI_Range] RR
INNER JOIN dbo.Tag_Types TT on TT.ID_TagType = RR.ID_TagType
INNER JOIN dbo.Readers  RD on RD.ID_Reader = RR.ID_Reader
WHERE RR.[Is_Deleted] = 0 AND RR.[Is_Active] =1
END
GO
/****** Object:  StoredProcedure [dbo].[MR_Add_RSSI_Range]    Script Date: 06/24/2013 15:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <24 July 2008>
-- Description:	<Add RSSI Range>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Add_RSSI_Range]
	-- Add the parameters for the stored procedure here
	 (		
			@ID_TagType bigint
           ,@ID_Reader bigint
           ,@RSSI_Min float
           ,@RSSI_Max float
           ,@Last_Modified_By bigint
	)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	Set dateformat dmy
IF EXISTS (SELECT ID_RSSIRange FROM [RSSI_Range] WHERE ID_TagType = @ID_TagType AND ID_Reader = @ID_Reader)
	BEGIN
		  UPDATE [RSSI_Range]
		   SET [ID_TagType] = @ID_TagType
			  ,[ID_Reader] = @ID_Reader
			  ,[RSSI_Min] = @RSSI_Min
			  ,[RSSI_Max] = @RSSI_Max
			  ,[Date_Modified] = GETDATE()
			 -- ,[Is_Deleted] = 0
			 -- ,[Is_Active] = 1
			  ,[Last_Modified_By] = @Last_Modified_By
		  WHERE ID_TagType = @ID_TagType AND ID_Reader = @ID_Reader
	END

ELSE
	BEGIN
		INSERT INTO  [RSSI_Range]
				   ([ID_TagType]
				   ,[ID_Reader]
				   ,[RSSI_Min]
				   ,[RSSI_Max]
				   ,[Date_Created]
				   ,[Date_Modified]
				   ,[Is_Deleted]
				   ,[Is_Active]
				   ,[Last_Modified_By])
			 VALUES
				   (@ID_TagType 
				   ,@ID_Reader 
				   ,@RSSI_Min 
				   ,@RSSI_Max 
				   ,GETDATE() 
				   ,GETDATE() 
				   ,0 
				   ,1 
				   ,@Last_Modified_By )

	END

-->SELECT RSSI_RANGE 

SELECT [ID_RSSIRange]
      ,RR.[ID_TagType]
      ,RR.[ID_Reader]
      ,[RSSI_Min]
      ,[RSSI_Max]
      ,RR.[Date_Created]
      ,RR.[Date_Modified]
      ,RR.[Is_Deleted]
      ,RR.[Is_Active]
      ,RR.[Last_Modified_By]
	  ,TT.TagType
	  ,Rtrim(RD.Reader) as Reader
  FROM [RSSI_Range] RR
INNER JOIN dbo.Tag_Types TT on TT.ID_TagType = RR.ID_TagType
INNER JOIN dbo.Readers  RD on RD.ID_Reader = RR.ID_Reader
WHERE RR.ID_TagType= @ID_TagType AND RR.ID_Reader = @ID_Reader


END
GO
/****** Object:  StoredProcedure [dbo].[Get_TagTypesByName]    Script Date: 06/24/2013 15:04:26 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 20 Oct 2009
-- Description:	To GET TAG Types by name
-- =============================================

CREATE PROCEDURE [dbo].[Get_TagTypesByName]
	@TagType varchar(50)
AS
	SET NOCOUNT ON;

	SELECT  ID_TagType, TagType	
	FROM    Tag_Types
	WHERE	TagType Like '%' + @TagType + '%' AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[Get_RSSIRange_By_Reader_TagType]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ========================================================
-- Author:		Deepanshu Jouhari
-- Create date: 5th May 2008
-- Description:	To GET a Employee By TAGID
-- ========================================================

Create PROCEDURE [dbo].[Get_RSSIRange_By_Reader_TagType]
	@ID_TagType bigint,
	@ID_Reader bigint
AS
SET NOCOUNT ON;
SELECT  * FROM RSSI_Range R
WHERE R.ID_TagType=@ID_TagType
 and R.ID_Reader=@ID_Reader
 AND R.Is_Deleted=0 AND R.Is_Active=1

--select * from [Employees]
GO
/****** Object:  StoredProcedure [dbo].[Delete_FieldService]    Script Date: 06/24/2013 15:04:10 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 14 May 2009
-- Description:	To DELETE a FieldService
-- =============================================
CREATE PROCEDURE [dbo].[Delete_FieldService]
(
	@ID_FieldService bigint
)
AS
	SET NOCOUNT OFF;
--select * from Tasks
--DELETE FROM [Employees] WHERE (([ID_Employee] = @ID_Employee))

UPDATE [dbo].[FieldService] 
SET Is_Deleted=1, 
	Date_Modified=getdate()
WHERE (([ID_FieldService] = @ID_FieldService))
GO
/****** Object:  StoredProcedure [dbo].[Add_FieldService]    Script Date: 06/24/2013 15:03:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		deepanshu Jouhari
-- Create date: 22/5/2009
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[Add_FieldService] 
	-- Add the parameters for the stored procedure here
	@Title nvarchar(50),
	@DueDate datetime,
	@ID_Employee bigint,
	@ID_Location bigint,
	@Description nvarchar(300),
	@Date_Modified datetime,
	@Last_Modified_By bigint
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	insert into FieldService(
	Title,
	DueDate,
	ID_Employee,
	ID_Location,
	[Description],
	Date_Modified,
	Last_Modified_By)
    values(
	@Title,
	@DueDate,
	@ID_Employee,
	@ID_Location,
	@Description,
	@Date_Modified,
	@Last_Modified_By)
	
	SELECT * FROM FieldService WHERE (ID_FieldService = SCOPE_IDENTITY())

END
GO
/****** Object:  StoredProcedure [dbo].[Update_FieldService]    Script Date: 06/24/2013 15:04:47 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 23 May 2009
-- Description:	To UPDATE a Task
-- =============================================

CREATE PROCEDURE [dbo].[Update_FieldService]
(
	@ID_FieldService bigint,
	@Title nvarchar(50),
	@DueDate datetime,
	@ID_Employee bigint,
	@ID_Location bigint,
	@Description nvarchar(300),
	@Date_Modified datetime=getdate,
	@Last_Modified_By bigint,
	@Is_Deleted bit=0,
	@Is_Active bit=1 
)
AS
	SET NOCOUNT OFF;

	UPDATE [dbo].[FieldService] 
	SET [Title] = @Title,
	[DueDate] = @DueDate,
	[ID_Employee] = @ID_Employee,
	[ID_Location] = @ID_Location,
	[Description] = @Description,
	[Date_Modified] = @Date_Modified,
	[Is_Deleted] = @Is_Deleted,
	[Is_Active] = @Is_Active, 
	[Last_Modified_By] = @Last_Modified_By 
	WHERE (([ID_FieldService] = @ID_FieldService));

	
	SELECT * FROM dbo.FieldService WHERE (ID_FieldService = @ID_FieldService)
GO
/****** Object:  StoredProcedure [dbo].[Get_FieldService_By_ID]    Script Date: 06/24/2013 15:04:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 23 May 2009
-- Description:	Get_FieldService_By_ID
-- =============================================

CREATE PROCEDURE [dbo].[Get_FieldService_By_ID]
	@ID_FieldService bigint
AS
	SET NOCOUNT ON;
SELECT *
FROM        [dbo].[FieldService] 
WHERE		ID_FieldService=@ID_FieldService
			AND Is_Deleted=0 AND Is_Active=1
GO
/****** Object:  StoredProcedure [dbo].[MR_Update_AlertDetail]    Script Date: 06/24/2013 15:04:34 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <26 July 2008>
-- Description:	<Update Alert Details>
-- =============================================
CREATE PROCEDURE [dbo].[MR_Update_AlertDetail]
(
	@ID_AlertType bigint,
	@Asset_Status nvarchar(150),
	@Is_Email bit,
	@Email_To varchar(500),
	@Email_From varchar(200),
	@Email_CC varchar(500),
	@Email_BCC varchar(500),
	@Email_Subject varchar(200),
	@Email_Body nvarchar(2000),
	@Is_SMS bit,
	@SMS_To varchar(20),
	@SMS_From varchar(20),
	@SMS_Body nvarchar(800),
	@Is_Reader bit,
	@Reader_Light bit,
	@Reader_LightDuration int,
	@Reader_Sound bit,
	@Reader_SoundDuration int,
	@Is_Server bit,
	@Server_Screen bit,
	@Server_Beep bit,
	@Melody nvarchar(150),
	@MelodyDuration int,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint,
	@Original_ID_AlertDetail bigint,
	@ID_AlertDetail bigint
)
AS
	SET NOCOUNT OFF;
UPDATE [Alert_Detail] SET [ID_AlertType] = @ID_AlertType, [Asset_Status] = @Asset_Status, [Is_Email] = @Is_Email, [Email_To] = @Email_To, [Email_From] = @Email_From, [Email_CC] = @Email_CC, [Email_BCC] = @Email_BCC, [Email_Subject] = @Email_Subject, [Email_Body] = @Email_Body, [Is_SMS] = @Is_SMS, [SMS_To] = @SMS_To, [SMS_From] = @SMS_From, [SMS_Body] = @SMS_Body, [Is_Reader] = @Is_Reader, [Reader_Light] = @Reader_Light, [Reader_LightDuration] = @Reader_LightDuration, [Reader_Sound] = @Reader_Sound, [Reader_SoundDuration] = @Reader_SoundDuration, [Is_Server] = @Is_Server, [Server_Screen] = @Server_Screen, [Server_Beep] = @Server_Beep, [Melody] = @Melody, [MelodyDuration] = @MelodyDuration, [Date_Created] = @Date_Created, [Date_Modified] = @Date_Modified, [Is_Deleted] = @Is_Deleted, [Is_Active] = @Is_Active, [Last_Modified_By] = @Last_Modified_By WHERE (([ID_AlertDetail] = @Original_ID_AlertDetail));
	
SELECT ID_AlertDetail, ID_AlertType, Asset_Status, Is_Email, Email_To, Email_From, Email_CC, Email_BCC, Email_Subject, Email_Body, Is_SMS, SMS_To, SMS_From, SMS_Body, Is_Reader, Reader_Light, Reader_LightDuration, Reader_Sound, Reader_SoundDuration, Is_Server, Server_Screen, Server_Beep, Melody, MelodyDuration, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Alert_Detail WHERE (ID_AlertDetail = @ID_AlertDetail)
GO
/****** Object:  StoredProcedure [dbo].[MR_Delete_AlertDetail]    Script Date: 06/24/2013 15:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[MR_Delete_AlertDetail]
(
	@Original_ID_AlertDetail bigint
)
AS
	SET NOCOUNT OFF;
DELETE FROM [Alert_Detail] WHERE (([ID_AlertDetail] = @Original_ID_AlertDetail))
GO
/****** Object:  StoredProcedure [dbo].[MR_ADD_AlertDetail]    Script Date: 06/24/2013 15:04:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <26 July 2008>
-- Description:	<ADD Alert Details>
-- Modified Deepanshu Jouhari
-- =============================================


CREATE PROCEDURE [dbo].[MR_ADD_AlertDetail]
(
	@ID_AlertType bigint,
	@ID_Reader bigint,
	@Asset_Status nvarchar(150) = null,
	@Is_Email bit,
	@Email_To varchar(500),
	@Email_From varchar(200),
	@Email_CC varchar(500),
	@Email_BCC varchar(500),
	@Email_Subject varchar(200) = null,
	@Email_Body nvarchar(2000) = null,
	@Is_SMS bit,
	@SMS_To varchar(20),
	@SMS_From varchar(20),
	@SMS_Body nvarchar(800) = null ,
	@Is_Reader bit,
	@Reader_Light bit,
	@Reader_LightDuration int,
	@Reader_Sound bit,
	@Reader_SoundDuration int,
	@Is_Server bit,
	@Server_Screen bit,
	@Server_Beep bit,
	@Melody nvarchar(150) = null ,
	@MelodyDuration int = null ,
	@Last_Modified_By bigint,
	@Is_Active bit=1
)
AS
	SET NOCOUNT OFF;

IF EXISTS (SELECT ID_AlertDetail FROM [Alert_Detail] WHERE ID_AlertType = @ID_AlertType AND ID_Reader = @ID_Reader)
	BEGIN
		SET DATEFORMAT dmy;
		UPDATE [Alert_Detail]
		SET 
			[Asset_Status]=@Asset_Status,
			[Is_Email]=@Is_Email,
			[Email_To]=@Email_To,
			[Email_From]=@Email_From,
			[Email_CC]=@Email_CC,
			[Email_BCC]=@Email_BCC,
			[Email_Subject]=@Email_Subject,
			[Email_Body]=@Email_Body,
			[Is_SMS]=@Is_SMS,
			[SMS_To]=@SMS_To,
			[SMS_From]=@SMS_From,
			[SMS_Body]=@SMS_Body,
			[Is_Reader]=@Is_Reader,
			[Reader_Light]=@Reader_Light,
			[Reader_LightDuration]=@Reader_LightDuration,
			[Reader_Sound]=@Reader_Sound,
			[Reader_SoundDuration]=@Reader_SoundDuration,
			[Is_Server]=@Is_Server,
			[Server_Screen]=@Server_Screen,
			[Server_Beep]=@Server_Beep,
			[Melody]=@Melody,
			[MelodyDuration]=@MelodyDuration,
			[Date_Created]=getdate(),
			[Date_Modified]=getdate(),
			[Is_Deleted]=0,
			[Is_Active]=@Is_Active,
			[Last_Modified_By]=@Last_Modified_By 
			WHERE ID_AlertType = @ID_AlertType  AND ID_Reader = @ID_Reader;
			
			SELECT ID_AlertDetail, ID_AlertType, Asset_Status, Is_Email, Email_To, Email_From, Email_CC, Email_BCC, Email_Subject, Email_Body, Is_SMS, SMS_To, SMS_From, SMS_Body, Is_Reader, Reader_Light, Reader_LightDuration, Reader_Sound, Reader_SoundDuration, Is_Server, Server_Screen, Server_Beep, Melody, MelodyDuration, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By 
			FROM Alert_Detail 
			WHERE ID_AlertType = @ID_AlertType  AND ID_Reader = @ID_Reader;
	END
ELSE
	BEGIN
		SET DATEFORMAT dmy;

		INSERT INTO [Alert_Detail] ([ID_AlertType],[ID_Reader ], [Asset_Status], [Is_Email],
			[Email_To], [Email_From], [Email_CC], [Email_BCC], [Email_Subject], 
			[Email_Body], [Is_SMS], [SMS_To], [SMS_From], [SMS_Body], [Is_Reader],
			[Reader_Light], [Reader_LightDuration], [Reader_Sound], [Reader_SoundDuration], 
			[Is_Server], [Server_Screen], [Server_Beep], [Melody], [MelodyDuration],
			[Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) 
		VALUES (@ID_AlertType,@ID_Reader, @Asset_Status, @Is_Email,
			@Email_To, @Email_From, @Email_CC, @Email_BCC, @Email_Subject,
			@Email_Body, @Is_SMS, @SMS_To, @SMS_From, @SMS_Body, @Is_Reader,
			@Reader_Light, @Reader_LightDuration, @Reader_Sound, @Reader_SoundDuration,
			@Is_Server, @Server_Screen, @Server_Beep, @Melody, @MelodyDuration, 
			getdate(), getdate(), 0, 1, @Last_Modified_By);
	
		SELECT ID_AlertDetail, ID_AlertType, Asset_Status, Is_Email, Email_To, Email_From, Email_CC, Email_BCC, Email_Subject, Email_Body, Is_SMS, SMS_To, SMS_From, SMS_Body, Is_Reader, Reader_Light, Reader_LightDuration, Reader_Sound, Reader_SoundDuration, Is_Server, Server_Screen, Server_Beep, Melody, MelodyDuration, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Alert_Detail WHERE (ID_AlertDetail = SCOPE_IDENTITY())
	END
GO
/****** Object:  StoredProcedure [dbo].[MR_Get_AlertDetail_By_AlertType]    Script Date: 06/24/2013 15:04:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry JAin>
-- Create date: <26 July 2008>
-- Description:	<GET Alert Details>
-- =============================================

CREATE PROCEDURE [dbo].[MR_Get_AlertDetail_By_AlertType] --1,4
(
@ID_AlertType bigint,
@ID_Reader bigint
)

AS
	SET NOCOUNT ON;
SELECT     Alert_Detail.*
FROM         Alert_Detail
WHERE ID_AlertType = @ID_AlertType AND
ID_Reader = @ID_Reader

--update Alert_detail set ID_Reader=4 where ID_AlertDetail=17
GO
/****** Object:  StoredProcedure [dbo].[Sync_GetReaderIDByIP]    Script Date: 06/24/2013 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry Jain
-- Create date: 25 June 2008
-- Description: Retreive ID_REader FROM IP Address
-- =============================================
create PROCEDURE [dbo].[Sync_GetReaderIDByIP]   -- 'employees','2008-01-23 00:00:00','2008-06-23 16:15:11'
	-- Add the parameters for the stored procedure here
(
@IPAddress nvarchar(15)

)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	IF EXISTS (SELECT ID_Reader  FROM dbo.Readers	WHERE RTRIM(LTRIM(IPAddress)) = RTRIM(LTRIM(@IPAddress)))
	BEGIN
		SELECT ID_Reader  
		FROM dbo.Readers
		WHERE RTRIM(LTRIM(IPAddress)) = RTRIM(LTRIM(@IPAddress))
	END
ELSE
	SELECT 0 AS ID_Reader

END
GO
/****** Object:  StoredProcedure [dbo].[Get_Readers_All]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <19 July 2008>
-- Description:	<Get Readers Information>
--Deepanshu
-- =============================================
CREATE PROCEDURE [dbo].[Get_Readers_All]
	@ISSelectTag int=1
	-- Add the parameters for the stored procedure here
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
if (@ISSelectTag <> 1)
begin
	SELECT     -1 as ID_Reader,'--- All --- ' as Reader, '' as IPAddress
			  ,0 as [ID_Location]
			  ,0 as [ID_LocationOut]
			  ,getdate() as [Date_Created]
			  ,getdate() as [Date_Modified]
			  ,0 as[Is_Deleted]
			  ,1 as [Is_Active]
			  ,0 as [Last_Modified_By]
			  ,'' as [URI]
			  ,'' as [LoginName]
			  ,'' as [Password]
			  ,'' as [TCP_Port]
			  ,0.0 as [TxPower]
			  ,0 as [HTTP_Connection]
			  ,0 as [TCP_Connection]
			  ,0 as [Log_Level]
			  ,0 as [Ant1]
			  ,0 as [Ant2]
			  ,0 as [Ant3]
			  ,0 as [Ant4]
			  ,'' as [RegKey]
			  ,'' as [SerialNo]
			  ,getdate() as [ExpiredOn]
			  ,0 as [NeverExpire]
			  ,0 as [UseDefault]
			  ,0 as [CurrentState]
	          ,' ' as [Terminal]	
			,1 as [ReaderType]		
			,'NA' as ChangeLocationOn
			,'-1' as ReaderMode
		  ,0 as [RxSensitivity]
		 ,0 as [RxSensitivityValue1] 
		,0 as [RxSensitivityValue2] 
		,0 as [RxSensitivityValue3] 
		 ,0 as [RxSensitivityValue4] 
UNION

	 SELECT [ID_Reader]
	  ,Rtrim(Reader) as Reader
      ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
	  ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
      ,[CurrentState]
	  ,[Terminal]	
	,[ReaderType]
	,ChangeLocationOn
	,isnull(ReaderMode,ReaderType) as ReaderMode
	,[RxSensitivity]
	  ,[RxSensitivityValue1] 
	  ,[RxSensitivityValue2] 
	  ,[RxSensitivityValue3] 
	  ,[RxSensitivityValue4] 
  FROM [Readers] where [Is_Deleted] = 0 AND [Is_Active] = 1 
end
else
begin
SELECT     -1 as ID_Reader,'--- Select --- ' as Reader, '' as IPAddress
			  ,0 as [ID_Location]
			  ,0 as [ID_LocationOut]
			  ,getdate() as [Date_Created]
			  ,getdate() as [Date_Modified]
			  ,0 as[Is_Deleted]
			  ,1 as [Is_Active]
			  ,0 as [Last_Modified_By]
			  ,'' as [URI]
			  ,'' as [LoginName]
			  ,'' as [Password]
			  ,'' as [TCP_Port]
			  ,0.0 as [TxPower]
			  ,0 as [HTTP_Connection]
			  ,0 as [TCP_Connection]
			  ,0 as [Log_Level]
			  ,0 as [Ant1]
			  ,0 as [Ant2]
			  ,0 as [Ant3]
			  ,0 as [Ant4]
			  ,'' as [RegKey]
			  ,'' as [SerialNo]
			  ,getdate() as [ExpiredOn]
			  ,0 as [NeverExpire]
			  ,0 as [UseDefault]
			  ,0 as [CurrentState]
	          ,' ' as [Terminal]
			,1 as [ReaderType]
		    ,'NA' as ChangeLocationOn
			,'-1' as ReaderMode	
			,0 as [RxSensitivity]
			,0 as [RxSensitivityValue1] 
			,0 as [RxSensitivityValue2] 
			,0 as [RxSensitivityValue3] 
			,0 as [RxSensitivityValue4] 			
		
UNION

	 SELECT [ID_Reader]
	  ,Rtrim(Reader) as Reader
      ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
	  ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
      ,[CurrentState]
	  ,[Terminal]	
	,[ReaderType]
	,ChangeLocationOn	
	,isnull(ReaderMode,ReaderType) as ReaderMode
		,[RxSensitivity]
	  ,[RxSensitivityValue1] 
	  ,[RxSensitivityValue2] 
	  ,[RxSensitivityValue3] 
	  ,[RxSensitivityValue4] 
  FROM [Readers] where [Is_Deleted] = 0 AND [Is_Active] = 1 
END
end

---------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Readers]    Script Date: 06/24/2013 15:04:24 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <19 July 2008>
-- Description:	<Get Readers Information>
-- =============================================
CREATE PROCEDURE [dbo].[Get_Readers]
	-- Add the parameters for the stored procedure here
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    -- Insert statements for procedure here
	 SELECT [ID_Reader]
      ,[Reader]
      ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
      ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
	  ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
	  ,[CurrentState]
	  ,[Terminal]	
		,[ReaderType]
	 ,isnull(ChangeLocationOn,'NA')	 as ChangeLocationOn
	 ,isnull(ReaderMode,ReaderType) as ReaderMode
	  ,[RxSensitivity]
	  ,[RxSensitivityValue1] 
	  ,[RxSensitivityValue2] 
	  ,[RxSensitivityValue3] 
	  ,[RxSensitivityValue4] 
  FROM [Readers] where [Is_Deleted] = 0 AND [Is_Active] = 1 
END

-----------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Get_Reader_By_IPAddress]    Script Date: 06/24/2013 15:04:23 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To GET a State by ID_State
-- =============================================

CREATE PROCEDURE [dbo].[Get_Reader_By_IPAddress]
	@IPAddress varchar(15)
AS
	SET NOCOUNT ON;
SELECT * from Readers
WHERE		IpAddress=@IPAddress
AND Is_Deleted=0 AND Is_Active=1

---------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Update_Reader]    Script Date: 06/24/2013 15:04:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <23 July 2008>
-- Description:	<Update Reader>
-- =============================================
CREATE PROCEDURE [dbo].[Update_Reader]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader bigint
		   ,@Reader nchar(50)
           ,@IPAddress nchar(15)
           ,@ID_Location bigint
		   ,@ID_LocationOut bigint
           ,@Last_Modified_By bigint
           ,@URI nvarchar(200)
           ,@LoginName nvarchar(100)
           ,@Password nvarchar(50)
           ,@TCP_Port nvarchar(50)
           ,@TxPower decimal(18,2)
           ,@HTTP_Connection int
           ,@TCP_Connection int
           ,@Log_Level int
           ,@Ant1 bit
           ,@Ant2 bit
           ,@Ant3 bit
           ,@Ant4 bit
           ,@RegKey nvarchar(24)
           ,@SerialNo nvarchar(50)
           ,@ExpiredOn datetime
           ,@NeverExpire bit
           ,@UseDefault bit
		   ,@CurrentState bit
	       ,@Terminal nvarchar(150)	
		   ,@ReaderType int = 1	
		   ,@ReaderMode int
			,@RxSensitivity int = 0
	  ,@RxSensitivityValue1 int = -70
	  ,@RxSensitivityValue2 int = -70
	  ,@RxSensitivityValue3 int = -70
	  ,@RxSensitivityValue4 int = -70
 )
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set dateformat dmy 
    -- Insert statements for procedure here
	UPDATE [Readers]
   SET [Reader] = @Reader
      ,[IPAddress] = @IPAddress
      ,[ID_Location] = @ID_Location
	  ,[ID_LocationOut] = @ID_LocationOut
      ,[Date_Modified] = getdate()
     -- ,[Is_Deleted] = @Is_Deleted
     -- ,[Is_Active] = @Is_Active
      ,[Last_Modified_By] = @Last_Modified_By
      ,[URI] = @URI
      ,[LoginName] = @LoginName
      ,[Password] = @Password
      ,[TCP_Port] = @TCP_Port
      ,[TxPower] = @TxPower
      ,[HTTP_Connection] = @HTTP_Connection
      ,[TCP_Connection] = @TCP_Connection
      ,[Log_Level] = @Log_Level
      ,[Ant1] = @Ant1
      ,[Ant2] = @Ant2
      ,[Ant3] = @Ant3
      ,[Ant4] = @Ant4
      ,[RegKey] = @RegKey
      ,[SerialNo] = @SerialNo
      ,[ExpiredOn] = @ExpiredOn
      ,[NeverExpire] = @NeverExpire
      ,[UseDefault] = @UseDefault
      ,[CurrentState] = @CurrentState
	  ,[Terminal]= @Terminal
	  ,[ReaderType] = @ReaderType
	  ,[ReaderMode] = @ReaderMode	
		,[RxSensitivity] = @RxSensitivity
	  ,[RxSensitivityValue1] = @RxSensitivityValue1
	  ,[RxSensitivityValue2] = @RxSensitivityValue2
	  ,[RxSensitivityValue3] = @RxSensitivityValue3
	  ,[RxSensitivityValue4] = @RxSensitivityValue4
 WHERE ID_Reader=@ID_Reader

------Select Reader
SELECT [ID_Reader] ,[Reader] ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
      ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
      ,[CurrentState]
	  ,[Terminal]
	  ,[ReaderType]
	  ,isnull(ChangeLocationOn,'NA') as ChangeLocationOn
	,isnull(ReaderMode,ReaderType) as ReaderMode
	,[RxSensitivity]  
	  ,[RxSensitivityValue1]  
	  ,[RxSensitivityValue2]  
	  ,[RxSensitivityValue3]  
	  ,[RxSensitivityValue4]  
FROM [Readers] WHERE (ID_Reader=@ID_Reader)
END


---------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Update_ReaderStatus]    Script Date: 06/24/2013 15:04:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <23 July 2008>
-- Description:	<Update Reader Status>
-- =============================================
CREATE PROCEDURE [dbo].[Update_ReaderStatus]
	-- Add the parameters for the stored procedure here
		(  
			@ID_Reader bigint		   
		   ,@CurrentState bit
	       ,@Terminal nvarchar(150)			  
		)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	
	set dateformat dmy 
    -- Insert statements for procedure here
	UPDATE [Readers]
   SET [CurrentState] = @CurrentState
	  ,[Terminal]= @Terminal	   	
 WHERE ID_Reader=@ID_Reader

------Select Reader
SELECT [ID_Reader] ,[Reader] ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
      ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
      ,[CurrentState]
	  ,[Terminal]
	  ,[ReaderType]
	  ,ISNULL(ChangeLocationOn,'NA') as ChangeLocationOn
	  ,isnull(ReaderMode,ReaderType) as ReaderMode
	,[RxSensitivity]  
	  ,[RxSensitivityValue1]  
	  ,[RxSensitivityValue2]  
	  ,[RxSensitivityValue3]  
	  ,[RxSensitivityValue4]  
FROM [Readers] WHERE (ID_Reader=@ID_Reader)
END

---------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Jobs_AssignJob]    Script Date: 06/24/2013 15:04:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 23 Dec 2009
-- Description:	Assigning job to asset(tray) 
-- =============================================

CREATE PROCEDURE [dbo].[Jobs_AssignJob]
(
	 --@ID bigint,
	 @AssetTagID varchar(50),
	 @JobID varchar(50),	 
	 @ID_Asset bigint = 0,
	 @ID_Antenna int = 0
	 	 
)
AS
	SET NOCOUNT OFF;

Declare @ID_Location bigint
Declare @ID_Reader bigint
Declare @ID bigint

SET @ID = 0



IF @ID_Asset = 0 
BEGIN
	IF EXISTS(SELECT ID_Asset FROM Assets WHERE TagID = @AssetTagID)
		SET @ID_Asset = (SELECT ID_Asset FROM Assets WHERE TagID = @AssetTagID)
	ELSE
		BEGIN
			SELECT -1;
			RETURN;
		END
END



IF NOT EXISTS(Select ID From AssignedJobs Where JobID = @JobID AND IsActive = 1 AND IsCompleted = 0) 
	BEGIN
		SET @ID_Reader = (SELECT TOP 1 ID_Reader FROM Readers WHERE IpAddress LIKE '%00.00.00.00%')

		IF @ID_Reader IS NULL
			BEGIN
				SET @ID_Reader = 0
			END

		SET @ID_Location = (SELECT TOP 1 ID_LocationIn FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND AntennaNo = 'Antenna1')

		IF @ID_Location IS NULL
			BEGIN
				SET @ID_Location = (SELECT TOP 1 ID_Location FROM Locations WHERE [Description] LIKE '%Assign%')
			END
	
		IF NOT EXISTS(Select ID From AssignedJobs Where ID_Asset = @ID_Asset AND IsActive = 1 AND IsCompleted = 0) 
			Begin 

				INSERT INTO AssignedJobs (JobID,ID_Asset) VALUES (@JobID,@ID_Asset);
				
				SET @ID = SCOPE_IDENTITY()
				--UPDATE Assets SET ID_Location = @ID_Location WHERE ID_Asset = @ID_Asset	
				exec [Update_Asset_Location_by_Reader] @AssetTagID,@ID_Location,0,@ID_Reader,'',0,@JobID

				SELECT @ID

			End
		ELSE
			BEGIN
				UPDATE AssignedJobs SET JobID = @JobID Where ID_Asset = @ID_Asset AND IsActive = 1 AND IsCompleted = 0
			
				SET @ID = (SELECT ID From AssignedJobs Where ID_Asset = @ID_Asset AND IsActive = 1 AND IsCompleted = 0)
				--UPDATE Assets SET ID_Location = @ID_Location WHERE ID_Asset = @ID_Asset	
				exec [Update_Asset_Location_by_Reader] @AssetTagID,@ID_Location,0,@ID_Reader,'',0,@JobID	 
				
				SELECT @ID
				
			END
	END
ELSE
	BEGIN
			SELECT -2;
			RETURN;
	END
GO
/****** Object:  StoredProcedure [dbo].[Jobs_UpdateJob]    Script Date: 06/24/2013 15:04:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 08 Dec 2009
-- Description:	Updating jobs
-- =============================================

CREATE PROCEDURE [dbo].[Jobs_UpdateJob]
(
	 --@ID bigint,
	 @AssetTagID varchar(50),
	 @JobID varchar(50),	 
	 @IsActive int,
	 @IsCompleted int	 	 
)
AS
	SET NOCOUNT OFF; 

Declare @ID_Asset Bigint,
		@ID_Reader BigInt,
		@ID_Location BigInt

BEGIN
	IF EXISTS(SELECT ID_Asset FROM Assets WHERE TagID = @AssetTagID)
		SET @ID_Asset = (SELECT ID_Asset FROM Assets WHERE TagID = @AssetTagID)
	ELSE
		BEGIN
			SELECT -1;
			RETURN;
		END
END

--print @ID_Asset 


IF EXISTS(Select ID From AssignedJobs Where ID_Asset = @ID_Asset AND JobID = @JobID) 
	Begin		
			SET @ID_Reader = (SELECT TOP 1 ID_Reader FROM Readers WHERE IpAddress LIKE '%00.00.00.00%')

			IF @ID_Reader IS NULL
				SET @ID_Reader = 0

			SET @ID_Location = (SELECT TOP 1 ID_LocationIn FROM AntennaSettings WHERE ID_Reader = @ID_Reader AND AntennaNo = 'Antenna2')

			IF @ID_Location IS NULL
				SET @ID_Location = (SELECT TOP 1 ID_Location FROM Locations WHERE [Description] LIKE '%Unassign%')
			
		UPDATE AssignedJobs SET IsActive = @IsActive,IsCompleted = @IsCompleted
		Where ID_Asset = @ID_Asset AND JobID = @JobID
		
		exec [Update_Asset_Location_by_Reader] @AssetTagID,@ID_Location,0,@ID_Reader,'',0,@JobID

			SELECT 0;
			RETURN;		
	End
ELSE
	BEGIN
			SELECT -2;
			RETURN;
	END

-- exec [Jobs_UpdateJob] '000000000000000000000003','J000002',1,0
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset]    Script Date: 06/24/2013 15:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Modified		Deepanshu Jouhari
-- Modified By : Vinay Bansal ON 12-June- 2009	
-- Create date: 19th March 2008, 1 Jan 2009
-- Last Modified By : Vijay on 26 May 2010
-- Description:	To UPDATE a Asset
-- Modified On -- 20/06/2012 
-- =============================================

CREATE PROCEDURE [dbo].[Update_Asset] --'Tag','Deepesh' ,'123','Desc', '','12/12/2008',2,1,1,1,1,2,16
(
	@TagID varchar(50),
	@Name varchar(50),
	@AssetNo varchar(50),
	@Description varchar(1000),
	@ImageName varchar(500),
	@PurchaseDate datetime,
	@ID_TagType bigint,
	@ID_Location bigint,
	@ID_AssetType bigint,
	@ID_AssetStatus bigint,
	@ID_AssetGroup bigint,
	@ID_AssetComment bigint,
	@Last_Modified_By bigint,
	@ID_Asset bigint,
	@ID_Employee bigint,
	@ReferenceNo nvarchar(150)='',
	@Allocation nvarchar(100)='', -- Added By Vinay
	@LotNo	nvarchar(50)='',		-- Added By Vinay
	@ExpiryDate datetime = '1900-01-01',		-- Added By Vinay
	@ID_AssetMaster bigint = 0,  
	@PartCode nvarchar(10) = '0',  --Added By Deep
	@IsVernonUpdate bit = 0,
	@ID_Reason bigint =0
)
AS
	SET NOCOUNT OFF;
BEGIN

IF @ID_Asset <= 0
BEGIN
	EXEC [Add_Asset] @TagID,@Name,@AssetNo,@Description,@ImageName,@PurchaseDate,@ID_TagType,@ID_Location,@ID_AssetType,@ID_AssetStatus,@ID_AssetGroup,@ID_AssetComment,@Last_Modified_By,0,@ReferenceNo,@Allocation,@LotNo, @ExpiryDate,@ID_AssetMaster,@PartCode
 RETURN;
END

IF NOT EXISTS(Select 1 From Vw_Unique_TagID Where (TagID = @TagID and [Type]<>'Asset') or (TagID = @TagID and [Type]='Asset' and ID<>@ID_Asset)) 
	Begin 

		if(@ID_AssetMaster <=0)
		BEGIN
			Select @ID_AssetMaster = ID_AssetMaster From Assets where TagID = @TagID
		END
	
		if(@ID_AssetMaster <=0)
		BEGIN
			Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = @Name
		END

		if(@ID_AssetMaster <=0)
		BEGIN
			IF NOT Exists (Select 1 From Master_Assets where [Description] = 'Default')
			BEGIN
				INSERT INTO Master_Assets (SNo,[Description],Is_Active,IS_Deleted,CreatedBy, Date_Created)
				Values ('Default','Default',1,0,@Last_Modified_By, Getdate())
			END	
			Select @ID_AssetMaster = ID_AssetMaster From Master_Assets where [Description] = 'Default'
		END


		Declare @OldLocation bigint
		set @OldLocation = 0
		Select @OldLocation= ID_Location from  [Assets] where ID_Asset = @ID_Asset
		if (@OldLocation <> @ID_Location )
		Begin
				INSERT INTO [dbo].[Asset_Movement]
				   ([ID_Asset]
				   ,[ID_TagType]
				   ,[ID_Location]
				   ,[Date_Created]
				   ,[Date_Modified]
				   ,[Is_Deleted]
				   ,[Is_Active]
				   ,[Last_Modified_By]
				   ,[ID_Reader]
				   ,[ID_AssetStatus]
				   ,[ID_Employee]
				   ,[ReferenceNo]
				   ,[ID_LocationNew]
				   ,[IS_VernonRead]
				   ,[ID_Reason]
					)
					Select 
					[ID_Asset]
				   ,[ID_TagType]
				   ,[ID_Location]
				   ,getDate() 
				   ,[Date_Modified] 
				   ,[Is_Deleted]
				   ,[Is_Active]
				   ,@Last_Modified_By as Last_Modified_By
				   ,[ID_Reader]
				   ,[ID_AssetStatus]
				   ,@ID_Employee as ID_Employee
				   ,[ReferenceNo]	
				   ,@ID_Location as ID_LocationNew
				   ,@IsVernonUpdate as IS_VernonRead
				   ,@ID_Reason as ID_Reason	
					From [Assets] where [ID_Asset] = @ID_Asset

			Update Locations Set UsageCount = UsageCount +1 Where ID_Location = @ID_Location -- To Update Location Usage Count field ( 20/06/2012 )
		   
		End 

		IF (Len(@ImageName) != 0)
		 begin
			set dateformat dmy   
			UPDATE [Assets] SET [ID_Location] = @ID_Location , [TagID] = @TagID, [Name] = @Name, [AssetNo] = @AssetNo, [Description] = @Description, [ImageName] = @ImageName, [PurchaseDate] = @PurchaseDate, [ID_AssetType] = @ID_AssetType, [ID_AssetStatus] = @ID_AssetStatus, [ID_AssetGroup] = @ID_AssetGroup, [ID_AssetComment] = @ID_AssetComment, [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By, [ID_Employee]=@ID_Employee, [ReferenceNo]=@ReferenceNo,LotNo=@LotNo,ExpiryDate=@ExpiryDate,Allocation=@Allocation,PartCode=@PartCode,ID_AssetMaster=@ID_AssetMaster WHERE (([ID_Asset] = @ID_Asset)); -- [ID_TagType]=@ID_TagType,
		 end
		ELSE
		 begin
			set dateformat dmy   
			UPDATE [Assets] SET [ID_Location] = @ID_Location, [TagID] = @TagID, [Name] = @Name, [AssetNo] = @AssetNo, [Description] = @Description, [PurchaseDate] = @PurchaseDate, [ID_AssetType] = @ID_AssetType, [ID_AssetStatus] = @ID_AssetStatus, [ID_AssetGroup] = @ID_AssetGroup, [ID_AssetComment] = @ID_AssetComment, [Date_Modified] = getdate(), [Last_Modified_By] = @Last_Modified_By, [ID_Employee]=@ID_Employee, [ReferenceNo]=@ReferenceNo ,LotNo=@LotNo,ExpiryDate=@ExpiryDate,Allocation=@Allocation,PartCode=@PartCode,ID_AssetMaster=@ID_AssetMaster WHERE (([ID_Asset] = @ID_Asset)); -- [ID_TagType]=@ID_TagType,
		 end
			
		SELECT ID_Asset, TagID, Name, AssetNo, Description, ImageName, PurchaseDate, ID_Location, ID_AssetType, ID_AssetStatus, ID_AssetGroup, ID_AssetComment, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By,ID_AssetMaster,PartCode FROM Assets WHERE (ID_Asset = @ID_Asset)
	End
else
	Begin
		RaisError(N'Duplicate TAG ID',18,1);
	end
END
GO
/****** Object:  StoredProcedure [dbo].[Update_Asset_By_TagID]    Script Date: 06/24/2013 15:04:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Cherry JAin	
-- Create date: 25 th July 2008
-- Description:	To UPDATE a Asset
--Modified Deepanshu Jouhari
-- Update_Asset_By_TagID '000000000000000000000894',1,4,28,''
-- =============================================

CREATE PROCEDURE [dbo].[Update_Asset_By_TagID] 
(
	@TagID varchar(50),
	@ID_Location bigint,
	@ID_Reader bigint,
	@ID_Employee bigint,
	@EmployeeID varchar(50),
	@ReferenceNo nvarchar(150)='',
	@JobID varchar(50) = null,
	@UpdateAllMovements int = 0  
)
AS
	SET NOCOUNT OFF;


Print 'Update_Asset_Location_by_Reader';


Exec Update_Asset_Location_by_Reader @TagID,@ID_Location,@ID_Employee,@ID_Reader,@ReferenceNo,0,@JobID,@UpdateAllMovements;

print 'Asset Movement Added';


DECLARE @EMPID VARCHAR(24)
DECLARE @ID_ASSET BIGINT
DECLARE @ID_ASSETSTATUS BIGINT
DECLARE @ID_LocationIn BIGINT
DECLARE @ID_LocationOut BIGINT
DECLARE @AlertOccured DATETIME

SET @AlertOccured = GETDATE()


--FATCH EMP ID & CHECK FOR MULTPLE OR UNKNOWN
SELECT @EMPID = RTRIM(TagID) FROM dbo.Employees WHERE ID_Employee = @ID_Employee;

print 'Employee -';

--IF(@EMPID  = '00000000000000000000000M' OR @EMPID  = '000000000000000000000000')
--BEGIN

SELECT @ID_ASSET = ID_Asset FROM dbo.Assets WHERE TagID = @TagID
SELECT @ID_ASSETSTATUS = ID_AssetStatus FROM dbo.Assets WHERE TagID = @TagID
SELECT @ID_LocationIn = ID_Location FROM dbo.Readers WHERE ID_Reader = @ID_Reader
SELECT @ID_LocationOut = ID_LocationOut FROM dbo.Readers WHERE ID_Reader = @ID_Reader

print 'Variable Set';

--

IF (@EMPID  = '00000000000000000000000M' AND (EXISTS (SELECT ID_AlertDetail FROM dbo.Alert_Detail WHERE ID_AlertType=1 and IS_Active=1 and ID_Reader = @ID_Reader)))  
		OR (@EMPID  = '000000000000000000000000' AND (EXISTS (SELECT ID_AlertDetail FROM dbo.Alert_Detail WHERE ID_AlertType=2 and IS_Active=1 and ID_Reader = @ID_Reader)))
			 OR (EXISTS (SELECT ID_AlertDetail FROM dbo.Alert_Detail WHERE ID_Reader = @ID_Reader AND IS_Active=1 AND  (',' + Asset_Status + ',' like '%,'+ RTRIM(cast(@ID_ASSETSTATUS as nvarchar)) +',%')))
	
BEGIN
Print 'Exe AddAlert';

	EXEC Add_Alert @ID_ASSET,
					@ID_ASSETSTATUS ,
					@ID_Employee ,
					@ID_Reader ,
					@ID_LocationIn,
					@ID_LocationOut, 
					@EmployeeID, 
					0, 
					0,
					0,
					@AlertOccured, 
					0
--END
 
END

------------------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Sync_GetData]    Script Date: 06/24/2013 15:04:40 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 23 June 2008
-- Description: Retreive Records By time Stamp
-- Exec [dbo].[Sync_GetData] 'Assets','2009-07-16 15:11:02.920', '2011-06-23 16:15:11'
-- =============================================
CREATE PROCEDURE [dbo].[Sync_GetData]--   'employees','2008-01-23 00:00:00','2010-07-16'
	-- Add the parameters for the stored procedure here
(
@Table_Name nvarchar(200),
@Intime datetime,
@OutTime datetime OutPut,
@itemLimit bigint=0
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	begin try
	insert into Logs values(getdate(),'Entered SP');
		insert into Logs values(getdate(),@Table_Name+' | '+convert(varchar,@Intime)+' | '+convert(varchar,@itemLimit));
	end try
	begin catch
	end catch;

	SET @OutTime = DateAdd(Year,1,getdate()) 

    -- Insert statements for procedure here
	
	Declare @sSQL nvarchar(3000)
 
BEGIN TRY

	set @sSQL = 'update ' + @Table_Name+ ' set SyncDate=Date_Modified where SyncDate is NULL'
	exec sp_executesql @sSQL
	set @sSQL = 'update ' + @Table_Name+ ' set SyncDate=Date_Modified where Date_Modified > SyncDate'
	exec sp_executesql @sSQL

END TRY
BEGIN CATCH
END CATCH
	--print @sSQL


------> ASSETS
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'assets')
BEGIN
	if(@itemLimit=0)
	begin
		SELECT ID_Asset as ServerKey,TagID,[Name],AssetNo,ID_Reader,ID_Employee,ID_TagType,Date_Modified,
		ID_Location,ID_AssetGroup,Last_Modified_By AS ModifiedBy,ReferenceNo, dbo.GetParentGroups(ID_AssetGroup) as GroupIDs ,
		Is_Deleted,Is_Active,LotNo,ID_AssetStatus,[Description]
		FROM ASSETS
		WHERE SyncDate BETWEEN @Intime AND  @OutTime
		--AND Is_Deleted = 0 AND Is_Active = 1
		order by Date_Modified
	end
	else
	begin
		SELECT TOP (@itemLimit + (select count(*) 
			from Assets where  SyncDate BETWEEN @Intime AND  @OutTime and
		(IS_Deleted=1 OR IS_Active=0))) ID_Asset as ServerKey,TagID,[Name],AssetNo,ID_Reader,ID_Employee,ID_TagType,Date_Modified,
		ID_Location,ID_AssetGroup,Last_Modified_By AS ModifiedBy,ReferenceNo, dbo.GetParentGroups(ID_AssetGroup) as GroupIDs ,
		Is_Deleted,Is_Active,LotNo,ID_AssetStatus,[Description]
		FROM ASSETS
		WHERE SyncDate BETWEEN @Intime AND  @OutTime
		--AND Is_Deleted = 0 AND Is_Active = 1
		order by Date_Modified
	end
END

------> ASSET_GROUP
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'asset_groups')
BEGIN
	SELECT ID_AssetGroup AS ServerKey,[Name],ParentID,Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active 
	FROM Asset_Groups
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
    order by Date_Modified
END
------> EMPLOYEE
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'employees')
BEGIN
	SELECT ID_Employee AS ServerKey,TagID,EmpNo,UserName,[Password],ID_SecurityGroup,ID_TagType,Date_Modified ,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active           
	FROM Employees
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> LCATIONS
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'locations')
BEGIN
	SELECT ID_Location AS ServerKey,TagID,[Name],LocationNo,Date_Modified ,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active,UsageCount, 0 as OfflineUsageCount     
	FROM Locations
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> Master_SecurityGroups
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'master_securitygroups')
BEGIN
	SELECT ID_SecurityGroup AS ServerKey,[Name],Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active     
	FROM Master_SecurityGroups
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> Menu_Options
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'menu_options')
BEGIN
	SELECT ID_MenuOptions AS ServerKey,Name, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active      
	FROM Menu_Options
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> Authority
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'authority')
BEGIN
	SELECT ID_Authority  AS ServerKey,ID_MenuOptions,ID_SecurityGroups, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Authority
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> FieldService
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'fieldservice')
BEGIN
	SELECT ID_FieldService  AS ServerKey,Title,ID_Employee,ID_Location, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM FieldService
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END


------> FS_Templates
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'fs_templates')
BEGIN
	SELECT ID_Template  AS ServerKey,Title,ID_TemplateType,Frequency, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM FS_Templates
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> Tasks
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'tasks')
BEGIN
	SELECT ID_Tasks AS ServerKey,Title, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Tasks
	WHERE SyncDate BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

	SET @OutTime = getdate();

	--DEclare @sSQL nvarchar(3000)
	--set @sSQL = 'select * from ' + @Table_Name+ ' where Date_Modified between cast('''+cast(@Intime as varchar(50))+''' as datetime) AND   cast('''+cast(@OutTime as varchar(50))+''' as datetime)'
	--exec sp_executesql @sSQL
	--print @sSQL

END

------> Reasons
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'reasons')
BEGIN
	SELECT ID_Reason AS ServerKey,ReasonNo,Name, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Reasons
	WHERE SyncDate BETWEEN @Intime AND @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> Asset_Status
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'asset_status')
BEGIN
	SELECT ID_AssetStatus AS ServerKey,Status, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Asset_Status
	WHERE Is_Deleted = 0 AND Is_Active = 1
	--AND SyncDate BETWEEN @Intime AND  @OutTime 
	order by Date_Modified
END
GO
/****** Object:  StoredProcedure [dbo].[Get_FSAssetsReport]    Script Date: 06/24/2013 15:04:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vinay Bansal
-- Modified:	
-- Create date: 23-June-2009
-- Description:	To GET Report of FS Assest
-- =============================================

CREATE PROCEDURE [dbo].[Get_FSAssetsReport]  
@Name nvarchar(100),
@ID_Template bigint,
@ID_Location bigint,
@ID_AssetGroup bigint,
@StartDate Datetime,
@EndDate Datetime,
@Opt int
AS
Begin
	-- EXEC [Get_FSAssetsReport]  '%','-1','-1','-1','2009-06-01','2010-06-30',1
	-- @opt = 1 For Item History Report
	-- @opt = 0 For Item outstanding Report
	SET NOCOUNT ON;
	SELECT Distinct FSAS.[ID_FSAssetStatus],FSA.[ID_FSAsset] 
      ,FSAS.[ID_Tasks],A.Name As Assets, T.Title as Tasks, FST.Title as Template,E.Name as Employee, L.Name as Location
      ,FSA.[ID_Asset]
      ,FSA.[ID_Template]
      ,FSA.StartDate
	  ,FSA.EndDate	
	  ,A.ID_Location	
      ,CASE WHEN FSAS.[TaskStatus] IS NULL THEN 1010 ELSE FSAS.[TaskStatus] END as TaskStatus
      ,CASE WHEN FSAS.[FSStatus] IS NULL THEN 1010 ELSE FSAS.[FSStatus] END as [FSStatus] 
	  ,FSAS.[StatusDate]
      ,FSAS.[ID_Employee]
      ,FSAS.[Comments]
      ,FSA.[IS_Active]
      ,FSA.[IS_Deleted]
      ,FSA.[Last_Modified_By]
      ,FSA.[Date_Created]
      ,FSA.[Date_Modified]
	  ,(select DueDate from [FuncGetFsDueDate](FST.ID_Template,FSA.StartDate)) as DUEDATE
  FROM FS_AssetDetails	FSD
  LEFT OUTER JOIN [FS_AssetStatus] FSAS ON FSAS.ID_FSAsset = FSD.ID_FSAsset AND FSAS.IS_Active = 1 AND FSAS.IS_Deleted = 0
  INNER JOIN FS_Assets FSA ON FSA.ID_FSAsset = FSD.ID_FSAsset AND FSA.IS_Active = 1 AND FSA.IS_Deleted = 0
  INNER JOIN FS_Templates FST ON FSA.ID_Template = 	FST.ID_Template AND FST.IS_Active = 1 AND FST.IS_Deleted = 0
  INNER JOIN Assets A ON A.Id_Asset = FSA.ID_Asset
  INNER JOIN Tasks T ON T.ID_Tasks = FSAS.ID_Tasks
 -- INNER JOIN FS_Templates FT ON FT.ID_Template = FSA.ID_Template
  LEFt Outer JOIN Employees E ON FSAS.ID_Employee = E.ID_Employee
  INNER Join Locations L ON L.ID_Location = A.ID_Location
  WHERE FSA.ID_Asset in
	(SELECT ID_Asset FROM Assets 
		 WHERE (@ID_Location = -1 or ID_Location = @ID_Location) 
		 AND (@ID_AssetGroup = -1 or ID_AssetGroup = @ID_AssetGroup)
		 AND (@Name = '%' or [Name] like @Name AND IS_Deleted=0 AND IS_Active = 1))
  AND (@ID_Template = -1 OR FSA.ID_Template=@ID_Template)
  AND (select Count (DueDate) from [FuncGetFsDueDate](FST.ID_Template,FSA.StartDate) Where DueDate >= @StartDate) > 0 
  AND (select Count (DueDate) from [FuncGetFsDueDate](FST.ID_Template,FSA.StartDate) Where DueDate <= @ENDDate) > 0 
  --  AND (select DueDate from [FuncGetFsDueDate](FST.ID_TemplateType,FSA.StartDate)) <= @ENDDate
  AND FSA.IS_Deleted=0 AND FSA.IS_Active = 1	
  AND ((@Opt = 1 AND 	ISNULL(FSAS.[TaskStatus],1010) <> 1010) OR (@Opt = 0 AND ISNULL(FSAS.[TaskStatus],1010) = 1010) )
 
END
GO
/****** Object:  StoredProcedure [dbo].[old_Sync_GetData]    Script Date: 06/24/2013 15:04:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Create date: 23 June 2008
-- Description: Retreive Records By time Stamp
-- Exec [dbo].[Sync_GetData] 'Assets','2009-07-16 15:11:02.920', '2011-06-23 16:15:11'
-- =============================================
CREATE PROCEDURE [dbo].[old_Sync_GetData]--   'employees','2008-01-23 00:00:00','2010-07-16'
	-- Add the parameters for the stored procedure here
(
@Table_Name nvarchar(200),
@Intime datetime,
@OutTime datetime OutPut,
@itemLimit bigint=0
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	SET @OutTime = DateAdd(Year,1,getdate()) 

    -- Insert statements for procedure here
	
	--Declare @sSQL nvarchar(3000)
	--set @sSQL = 'update ' + @Table_Name+ ' set Date_Modified=Date_Modified where Date_Modified is NULL'
	--exec sp_executesql @sSQL
	--print @sSQL


------> ASSETS
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'assets')
BEGIN
	if(@itemLimit=0)
	begin
		SELECT ID_Asset as ServerKey,TagID,[Name],AssetNo,ID_Reader,ID_Employee,ID_TagType,Date_Modified,
		ID_Location,ID_AssetGroup,Last_Modified_By AS ModifiedBy,ReferenceNo, dbo.GetParentGroups(ID_AssetGroup) as GroupIDs ,
		Is_Deleted,Is_Active 
		FROM ASSETS
		WHERE Date_Modified BETWEEN @Intime AND  @OutTime
		--AND Is_Deleted = 0 AND Is_Active = 1
		order by Date_Modified
	end
	else
	begin
		SELECT TOP (@itemLimit + (select count(*) 
			from Assets where  Date_Modified BETWEEN @Intime AND  @OutTime and
		(IS_Deleted=1 OR IS_Active=0))) ID_Asset as ServerKey,TagID,[Name],AssetNo,ID_Reader,ID_Employee,ID_TagType,Date_Modified,
		ID_Location,ID_AssetGroup,Last_Modified_By AS ModifiedBy,ReferenceNo, dbo.GetParentGroups(ID_AssetGroup) as GroupIDs ,
		Is_Deleted,Is_Active 
		FROM ASSETS
		WHERE Date_Modified BETWEEN @Intime AND  @OutTime
		--AND Is_Deleted = 0 AND Is_Active = 1
		order by Date_Modified
	end
END

------> ASSET_GROUP
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'asset_groups')
BEGIN
	SELECT ID_AssetGroup AS ServerKey,[Name],ParentID,Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active 
	FROM Asset_Groups
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
    order by Date_Modified
END
------> EMPLOYEE
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'employees')
BEGIN
	SELECT ID_Employee AS ServerKey,TagID,EmpNo,UserName,[Password],ID_SecurityGroup,ID_TagType,Date_Modified ,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active           
	FROM Employees
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> LCATIONS
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'locations')
BEGIN
	SELECT ID_Location AS ServerKey,TagID,[Name],LocationNo,Date_Modified ,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active      
	FROM Locations
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> Master_SecurityGroups
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'master_securitygroups')
BEGIN
	SELECT ID_SecurityGroup AS ServerKey,[Name],Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active     
	FROM Master_SecurityGroups
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END
------> Menu_Options
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'menu_options')
BEGIN
	SELECT ID_MenuOptions AS ServerKey,Name, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active      
	FROM Menu_Options
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> Authority
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'authority')
BEGIN
	SELECT ID_Authority  AS ServerKey,ID_MenuOptions,ID_SecurityGroups, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Authority
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> FieldService
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'fieldservice')
BEGIN
	SELECT ID_FieldService  AS ServerKey,Title,ID_Employee,ID_Location, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM FieldService
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END


------> FS_Templates
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'fs_templates')
BEGIN
	SELECT ID_Template  AS ServerKey,Title,ID_TemplateType,Frequency, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM FS_Templates
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

------> Tasks
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'tasks')
BEGIN
	SELECT ID_Tasks AS ServerKey,Title, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Tasks
	WHERE Date_Modified BETWEEN @Intime AND  @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

	SET @OutTime = getdate();

	--DEclare @sSQL nvarchar(3000)
	--set @sSQL = 'select * from ' + @Table_Name+ ' where Date_Modified between cast('''+cast(@Intime as varchar(50))+''' as datetime) AND   cast('''+cast(@OutTime as varchar(50))+''' as datetime)'
	--exec sp_executesql @sSQL
	--print @sSQL

END

------> Reasons
IF(LOWER(RTRIM(LTRIM(@Table_Name))) = 'reasons')
BEGIN
	SELECT ID_Reason AS ServerKey,ReasonNo,Name, Date_Modified,Last_Modified_By AS ModifiedBy,Is_Deleted,Is_Active         
	FROM Reasons
	WHERE Date_Modified BETWEEN @Intime AND @OutTime
--	AND Is_Deleted = 0 AND Is_Active = 1
	order by Date_Modified
END

--Select * from Assets where Date_Created > getdate()
/*
SELECT TOP (100 + (select count(*) 
			from Assets where
		(IS_Deleted=1 OR IS_Active=0)))
		 ID_Asset as ServerKey,TagID,[Name],AssetNo,ID_Reader,ID_Employee,ID_TagType,Date_Modified,
		ID_Location,ID_AssetGroup,Last_Modified_By AS ModifiedBy,ReferenceNo, dbo.GetParentGroups(ID_AssetGroup) as GroupIDs ,
		Is_Deleted,Is_Active 
		FROM ASSETS
		order by Date_Modified
*/
GO
/****** Object:  StoredProcedure [dbo].[Vernon_Validate]    Script Date: 06/24/2013 15:04:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Modified: Deepanshu Jouhari	
-- Create date: 06-June-2009
-- Modify Date: 14 Sep 2009
-- Description:	To CHECK for Duplicate Asset Number and Location present or not
-- =============================================

CREATE PROCEDURE [dbo].[Vernon_Validate]  --'Tag-0011187'
	-- Add the parameters for the stored procedure here
	 @ValidateID int,   -- 0 for object records and 1 for Location records
	 @TagID varchar(50)='',
	 @AssetID varchar(50)='',
	 @PartCode varchar(10) = 0,
	 @LocationID varchar(50)='',
	 @LocationName varchar(100)=''	

AS
BEGIN

  Declare @Error int,
		  @ErrorMsg varchar(800),
		  @ID_Location bigint

  Set @Error = 0
  Set @ErrorMsg = ''
  Set @ID_Location = 0		
	
  IF EXISTS(select @TagID From Vw_Unique_TagID where TagID = @TagID)
		BEGIN
			--Select 1 as Error, 'Duplicate TagID' as ErrorMsg
			Set @Error = 1
			Set @ErrorMsg = @ErrorMsg + ' ''Duplicate Tag ID'''			
		 END  
	
  IF @ValidateID = 0    -- checks for object records
		BEGIN
			-- checking Asset no uniqueness
			IF EXISTS(select AssetNo From Assets where AssetNo = @AssetID AND isNULL(PartCode,'0') = @PartCode) 
				--(PartCode = @PartCode OR PartCode = '0'))
				BEGIN
					--Select 1 as Error, 'Location does not exist' as ErrorMsg
					Set @Error = 1
					Set @ErrorMsg = @ErrorMsg + ' ''Item No. already exist'''								 
				 END
			
			-- checking location exixtense
			IF Not EXISTS(select ID_Location From Locations where LocationNo = @LocationID)
				BEGIN
					--Select 1 as Error, 'Location does not exist' as ErrorMsg
					Set @Error = 1
					Set @ErrorMsg = @ErrorMsg + ' ''Location No. does not exist'''
					Set @ID_Location = 0								 
				 END
			 ELSE
				BEGIN
					-- checking lOCATION NAME AND lOCATIONNO CORRECTNESS
					 IF Not EXISTS(select LocationNo From Locations where LocationNo = @LocationID AND [Name] = @LocationName)
						BEGIN					 
							Set @Error = 1
							Set @ErrorMsg = @ErrorMsg + ' ''Location Name does not corresponds to Location No.'''
							Set @ID_Location = 0										 
						END
					 ELSE
							Set @ID_Location = (select ID_Location From Locations where LocationNo = @LocationID)

				END	
		 END

  ELSE   -- checks for Location records
		BEGIN		
				-- checking lOCATION No uniqueness		
				 IF EXISTS(select LocationNo From Locations where LocationNo = @LocationID)
					BEGIN					 
						Set @Error = 1
						Set @ErrorMsg = @ErrorMsg + ' ''Location No. already exist'''								 
					END				
		END

 Select @Error as Error, @ErrorMsg as ErrorMsg,@ID_Location as IDLocation  

END


--delete from assets
--where Id_asset >= 588
GO
/****** Object:  StoredProcedure [dbo].[Add_LocationFromFile]    Script Date: 06/24/2013 15:04:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 26 Oct 2009
-- Description:	To ADD a Location From CSV file
-- =============================================

CREATE PROCEDURE [dbo].[Add_LocationFromFile]
(
	@TagID varchar(50),
	@Name varchar(50),
	@LocationNo varchar(50),
	@Description varchar(500),
	@LocationGroup varchar(200),
	@Date_Created datetime = null,
	@Date_Modified datetime = null,
	@Is_Deleted bit = 0,
	@Is_Active bit = 1,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;
set dateformat dmy   
Declare @ID_LocationGroup bigint

IF NOT EXISTS(Select ID_LocationGroup From Location_Groups Where [Name] = @LocationGroup) 
	Begin
		Insert into Location_Groups ([Name],ParentID,Date_Created,Date_Modified,Is_Deleted,Is_Active,Last_Modified_By)
					Values(@LocationGroup,0,Getdate(),Getdate(),0,1,@Last_Modified_By)
		SET @ID_LocationGroup = SCOPE_IDENTITY()
	End
	else
		SET @ID_LocationGroup = (SELECT  ID_LocationGroup From Location_Groups Where [Name] = @LocationGroup)

IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID AND @TagID <> '') 
	 
	BEGIN
		INSERT INTO [Locations] ([TagID], [Name], [LocationNo], [Description], [ID_LocationGroup], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@TagID, @Name, @LocationNo, @Description, @ID_LocationGroup, getdate(), getdate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
		SELECT @@IDENTITY;
	END
		 
ELSE
	SELECT -1;


-----------------------------------------------------------------------------------------------------------
GO
/****** Object:  StoredProcedure [dbo].[Sync_AddLocation]    Script Date: 06/24/2013 15:04:39 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <26 June 2008>
-- Description:	<Add Location >
-- =============================================
CREATE PROCEDURE [dbo].[Sync_AddLocation]
	-- Add the parameters for the stored procedure here
(
@TagID varchar(50),
@Name varchar(50),
@LocationNo varchar(50),
@Date_Modified datetime,
@Last_Modified_By bigint,
@Description varchar(500),
@ID_LocationGroup bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
		BEGIN 

		INSERT INTO  [Locations]
				   ([TagID]
				   ,[Name]
				   ,[LocationNo]
				   ,[Date_Created]
				   ,[Date_Modified]
				   ,[Last_Modified_By]
				   ,[Description]
				   ,[ID_LocationGroup]
					)
			 VALUES
				   (@TagID ,
				   @Name ,
				   @LocationNo ,
				   @Date_Modified ,
				   @Date_Modified ,
				   @Last_Modified_By,
				   @Description,
				   @ID_LocationGroup
					)
		-- Insert statements for procedure here
		END 

---->select Inserted location
SELECT [ID_Location]
      ,[TagID]
      ,[Name]
      ,[LocationNo]
      ,[Description]
      ,[ID_LocationGroup]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,[Last_Modified_By]
  FROM [Locations]  WHERE (ID_Location = SCOPE_IDENTITY())
END
GO
/****** Object:  StoredProcedure [dbo].[Add_Location]    Script Date: 06/24/2013 15:04:02 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Asif Ali
-- Create date: 19th March 2008
-- Description:	To ADD a Location
-- =============================================

CREATE PROCEDURE [dbo].[Add_Location]
(
	@TagID varchar(50),
	@Name varchar(50),
	@LocationNo varchar(50),
	@Description varchar(500),
	@ID_LocationGroup bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit,
	@Is_Active bit,
	@Last_Modified_By bigint
)
AS
	SET NOCOUNT OFF;

IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
	Begin 

		INSERT INTO [Locations] ([TagID], [Name], [LocationNo], [Description], [ID_LocationGroup], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@TagID, @Name, @LocationNo, @Description, @ID_LocationGroup, @Date_Created, @Date_Modified, @Is_Deleted, @Is_Active, @Last_Modified_By);
	
		SELECT ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Locations WHERE (ID_Location = SCOPE_IDENTITY())

	End
GO
/****** Object:  StoredProcedure [dbo].[Vernon_AddUpdateLocation]    Script Date: 06/24/2013 15:04:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Vijay Mangtani
-- Create date: 07 Aug 2009
-- Description:	To ADD a Location from Vernon Locations File
-- =============================================

CREATE PROCEDURE [dbo].[Vernon_AddUpdateLocation]
(
	@TagID varchar(50),
	@Name varchar(50),
	@LocationNo varchar(50),
	@Description varchar(500),
	@ID_LocationGroup bigint,
	@Date_Created datetime,
	@Date_Modified datetime,
	@Is_Deleted bit = 0,
	@Is_Active bit = 1,
	@Last_Modified_By bigint,
    @Flag varchar(10)
)
AS
	SET NOCOUNT OFF;
 
Declare @Result INT

SET @Result = 0;
 
IF @Flag = 'A'
BEGIN
	IF @TagID <> ''
		BEGIN
			IF NOT EXISTS(Select TagID From Vw_Unique_TagID Where TagID = @TagID) 
				Begin

					IF NOT EXISTS(Select TagID From [Locations] Where [LocationNo] =  @LocationNo) 
						BEGIN
							INSERT INTO [Locations] ([TagID], [Name], [LocationNo], [Description], [ID_LocationGroup], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@TagID, @Name, @LocationNo, @Description, @ID_LocationGroup, Getdate(), GetDate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
							SET @Result = @@Identity
						END	
					ELSE
						BEGIN
							SET @Result = -2 -- Location no already exists
						END
				 
					--SELECT ID_Location, TagID, Name, LocationNo, Description, ID_LocationGroup, Date_Created, Date_Modified, Is_Deleted, Is_Active, Last_Modified_By FROM Locations WHERE (ID_Location = SCOPE_IDENTITY())
				End
			ELSE
					BEGIN
						SET @Result = -1 -- Tag ID already Exists
					END
		END
	ELSE IF NOT EXISTS(Select TagID From [Locations] Where [LocationNo] =  @LocationNo) 
			BEGIN
				INSERT INTO [Locations] ([TagID], [Name], [LocationNo], [Description], [ID_LocationGroup], [Date_Created], [Date_Modified], [Is_Deleted], [Is_Active], [Last_Modified_By]) VALUES (@TagID, @Name, @LocationNo, @Description, @ID_LocationGroup, Getdate(), GetDate(), @Is_Deleted, @Is_Active, @Last_Modified_By);
				SET @Result = @@Identity
			END	
		 ELSE
			BEGIN
				SET @Result = -2 -- Location no already exists
			END

END
ELSE IF @Flag = 'C'
	Begin

		SET @Result = 1
		
		UPDATE [Locations] Set 
			[Date_Modified] = Getdate(),
			Last_Modified_By = @Last_Modified_By,
			[Name] = @Name,
			[Description] = @Description,
			[TagID] = @TagID
		WHERE [LocationNo] =  @LocationNo
	End

SELECT @Result;
GO
/****** Object:  StoredProcedure [dbo].[Update_FSAssetStatus]    Script Date: 06/24/2013 15:04:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Deepanshu Jouhari
-- Modify date: 23 May 2009
-- Description:	To UPDATE a Task
/*			New = 1010,
            InProcess = 1020,
            Completed = 1030,
            Expired=1040,
            Cancel = 1050    
*/
-- =============================================

CREATE PROCEDURE [dbo].[Update_FSAssetStatus]
(
	@ID_FSAssetStatus bigint,
	@ID_FSAsset bigint,
	@ID_Tasks bigint,
	@ID_Template bigint,
	@TaskStatus bigint,
	@FSStatus bigint,
	@Comments nvarchar(150),
	@ID_Employee bigint,
	@ID_Asset bigint,
	@Last_Modified_By bigint,
	@StatusDate DateTime=getDate
)
AS
	SET NOCOUNT OFF;
	Declare @OLD_FSStatus bigint,
	@OLD_DueDate datetime,
	@StartDate datetime,
	@EndDate datetime,
	@NewDueDate DateTime,
	@ID_TemplateType bigint

	Select @OLD_FSStatus = FSStatus, @OLD_DueDate=DueDate 
    from [dbo].[FS_AssetStatus] 
    WHERE [ID_FSAssetStatus] = @ID_FSAssetStatus;

	Select @StartDate = StartDate, @EndDate=EndDate 
    from [dbo].[FS_Assets] 
    WHERE [ID_FSAsset] = @ID_FSAsset;

	UPDATE [dbo].[FS_AssetStatus] 
	SET 
	[ID_FSAsset] = @ID_FSAsset,
	[ID_Tasks] = @ID_Tasks,
	[ID_Template] = @ID_Template,
	[TaskStatus] = @TaskStatus,
	[FSStatus] = @FSStatus,
	[Comments] = @Comments,
	[ID_Employee] = @ID_Employee,
	[ID_Asset] = @ID_Asset,
	[Last_Modified_By] = @Last_Modified_By,
	[StatusDate]=@StatusDate
	WHERE [ID_FSAssetStatus] = @ID_FSAssetStatus;

	if(@OLD_FSStatus<>@FSStatus and (@FSStatus=1030 or @FSStatus=1040 or @FSStatus=1050))
	BEGIN
		
		Select @NewDueDate=dbo.GetDueDate([ID_Template],@OLD_DueDate),
		@ID_TemplateType = ID_TemplateType
		from FS_Templates where ID_Template=@ID_Template
		--Insert New due task
		if((@ID_TemplateType<>1010) and (@StartDate<=@NewDueDate and @EndDate>=@NewDueDate))
		BEGIN
			INSERT INTO [dbo].[FS_AssetStatus]
				 ([ID_FSAsset],[ID_Tasks],[ID_Asset],[ID_Template],[DueDate],[TaskStatus],[FSStatus],[Last_Modified_By])     
			SELECT FS.[ID_FSAsset],FS.[ID_Tasks],FS.[ID_Asset],FS.[ID_Template],@NewDueDate,1010,1010,0 
			FROM [dbo].[FS_AssetStatus] FS, Assets A, FS_AssetDetails FSAD
			WHERE (([ID_FSAssetStatus] = @ID_FSAssetStatus) and FS.ID_Asset=A.ID_Asset and A.IS_Deleted=0 and A.Is_Active=1 and FS.ID_FSAsset=FSAD.ID_FSAsset and FS.ID_Tasks=FSAD.ID_Tasks and FSAD.IS_Deleted=0 and FSAD.Is_Active=1 );
		END
	END
GO
/****** Object:  StoredProcedure [dbo].[Add_Reader]    Script Date: 06/24/2013 15:04:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Deepanshu Jouhari>
-- Create date: <10 Oct 2009>
-- Description:	<Add Reader>
-- =============================================
CREATE PROCEDURE [dbo].[Add_Reader]
	-- Add the parameters for the stored procedure here
		(  @ID_Reader bigint
		   ,@Reader nchar(50) =null
           ,@IPAddress nchar(15)
           ,@ID_Location bigint
           ,@ID_LocationOut bigint
           ,@Last_Modified_By bigint
           ,@URI nvarchar(200)
           ,@LoginName nvarchar(100)
           ,@Password nvarchar(50)
           ,@TCP_Port nvarchar(50) =null
           ,@TxPower decimal(18,2) 
           ,@HTTP_Connection int
           ,@TCP_Connection int
           ,@Log_Level int
           ,@Ant1 bit
           ,@Ant2 bit
           ,@Ant3 bit
           ,@Ant4 bit
           ,@RegKey char(24)
           ,@SerialNo nvarchar(50)
           ,@ExpiredOn datetime = null
           ,@NeverExpire bit  
           ,@UseDefault bit 
		   ,@CurrentState bit
		   ,@Terminal nvarchar(150)	= null
		   ,@ReaderType int	= 1
		   ,@ReaderMode int = 0
			,@RxSensitivity int = 0
			,@RxSensitivityValue1 int = -70
			,@RxSensitivityValue2 int = -70
			,@RxSensitivityValue3 int = -70
			,@RxSensitivityValue4 int = -70
)		   
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	set dateformat dmy 

	Declare @idTagType INT;

IF (@ID_Reader <> 0)

--IF EXISTS (SELECT ID_Reader FROM dbo.Readers WHERE RTRIM(IPAddress) = RTRIM(@IPAddress))
		BEGIN

			IF(@Reader is null OR @Reader = '')
			BEGIN
				Select @Reader = Reader from [Readers] Where ID_Reader = @ID_Reader
			END

			UPDATE [Readers]
		   SET [Reader] = @Reader
			  ,[IPAddress] = @IPAddress
			  ,[ID_Location] = @ID_Location
			  ,[ID_LocationOut] = @ID_LocationOut
			  ,[Date_Modified] = getdate()
			 -- ,[Is_Deleted] = @Is_Deleted
			 -- ,[Is_Active] = @Is_Active
			  ,[Last_Modified_By] = @Last_Modified_By
			  ,[URI] = @URI
			  ,[LoginName] = @LoginName
			  ,[Password] = @Password
			  ,[TCP_Port] = @TCP_Port
			  ,[TxPower] = @TxPower
			  ,[HTTP_Connection] = @HTTP_Connection
			  ,[TCP_Connection] = @TCP_Connection
			  ,[Log_Level] = @Log_Level
			  ,[Ant1] = @Ant1
			  ,[Ant2] = @Ant2
			  ,[Ant3] = @Ant3
			  ,[Ant4] = @Ant4
			  ,[RegKey] = @RegKey
			  ,[SerialNo] = @SerialNo
			  ,[ExpiredOn] = @ExpiredOn
			  ,[NeverExpire] = @NeverExpire
			  ,[UseDefault] = @UseDefault
			  ,[CurrentState] = @CurrentState
		      ,[Terminal] = @Terminal	
			  ,[ReaderType] = @ReaderType
			  ,[ReaderMode] = @ReaderMode
			  ,[RxSensitivity] = @RxSensitivity
			  ,[RxSensitivityValue1] = @RxSensitivityValue1
			  ,[RxSensitivityValue2] = @RxSensitivityValue2
			  ,[RxSensitivityValue3] = @RxSensitivityValue3
			  ,[RxSensitivityValue4] = @RxSensitivityValue4
		WHERE ID_Reader = @ID_Reader

		 	

		END
ELSE
	BEGIN


		-- Insert statements for procedure here
		INSERT INTO  [Readers]
			   ([Reader]
			   --,[ID_Reader]
			   ,[IPAddress]
			   ,[ID_Location]
			   ,[ID_LocationOut]
			   ,[Date_Created]
			   ,[Date_Modified]
			   ,[Is_Deleted]
			   ,[Is_Active]
			   ,[Last_Modified_By]
			   ,[URI]
			   ,[LoginName]
			   ,[Password]
			   ,[TCP_Port]
			   ,[TxPower]
			   ,[HTTP_Connection]
			   ,[TCP_Connection]
			   ,[Log_Level]
			   ,[Ant1]
			   ,[Ant2]
			   ,[Ant3]
			   ,[Ant4]
			   ,[RegKey]
			   ,[SerialNo]
			   ,[ExpiredOn]
			   ,[NeverExpire]
			   ,[UseDefault]
			   ,[CurrentState]
		      ,[Terminal]
			  ,[ReaderType]
			  ,[ReaderMode]
			  ,[RxSensitivity]
			  ,[RxSensitivityValue1] 
			  ,[RxSensitivityValue2] 
			  ,[RxSensitivityValue3] 
			  ,[RxSensitivityValue4] 
)
		 VALUES
			   (@Reader 
			  -- ,@ID_Reader
			   ,@IPAddress 
			   ,@ID_Location 
			   ,@ID_LocationOut
			   ,getdate() 
			   ,getdate() 
			   ,0 
			   ,1 
			   ,@Last_Modified_By 
			   ,@URI 
			   ,@LoginName 
			   ,@Password 
			   ,@TCP_Port 
			   ,@TxPower 
			   ,@HTTP_Connection 
			   ,@TCP_Connection 
			   ,@Log_Level 
			   ,@Ant1 
			   ,@Ant2 
			   ,@Ant3 
			   ,@Ant4 
			   ,@RegKey 
			   ,@SerialNo 
			   ,@ExpiredOn 
			   ,@NeverExpire 
			   ,@UseDefault
			   ,@CurrentState
		       ,@Terminal	
			   ,@ReaderType	
			   ,@ReaderMode
			   ,@RxSensitivity
			  ,@RxSensitivityValue1
			  ,@RxSensitivityValue2 
			  ,@RxSensitivityValue3 
			  ,@RxSensitivityValue4
	 )

		-->Set ID_Reader Value
		SET @ID_Reader = SCOPE_IDENTITY()

	END


------Select Reader
SELECT [ID_Reader] ,[Reader] ,[IPAddress]
      ,ISNULL([ID_Location],0) as [ID_Location]
      ,ISNULL([ID_LocationOut],0) as [ID_LocationOut]
      ,[Date_Created]
      ,[Date_Modified]
      ,[Is_Deleted]
      ,[Is_Active]
      ,isnull([Last_Modified_By],0) as [Last_Modified_By]
      ,[URI]
      ,[LoginName]
      ,[Password]
      ,[TCP_Port]
      ,[TxPower]
      ,[HTTP_Connection]
      ,[TCP_Connection]
      ,[Log_Level]
      ,[Ant1]
      ,[Ant2]
      ,[Ant3]
      ,[Ant4]
      ,[RegKey]
      ,[SerialNo]
      ,[ExpiredOn]
      ,[NeverExpire]
	  ,[UseDefault]
	  ,[CurrentState]
	  ,[Terminal]
	  ,[ReaderType]
	  ,isnull(ChangeLocationOn,'NA') as ChangeLocationOn
	  ,isnull(ReaderMode,ReaderType) as ReaderMode
	  ,[RxSensitivity]
	  ,[RxSensitivityValue1] 
	  ,[RxSensitivityValue2] 
	  ,[RxSensitivityValue3] 
	  ,[RxSensitivityValue4] 
FROM [Readers] WHERE (ID_Reader = @ID_Reader)

	SET @idTagType = 1
	SELECT @idTagType = ID_TagType FROM Tag_Types Where TagType = 'Asset'
	
	IF @idTagType IS NULL
		SET @idTagType = 1;

	EXEC [MR_Add_RSSI_Range] @idTagType,@ID_Reader,-100,0,@Last_Modified_By

	SET @idTagType = 2;
	SELECT @idTagType = ID_TagType FROM Tag_Types Where TagType = 'Employee'
	
	IF @idTagType IS NULL
		SET @idTagType = 2;

	EXEC [MR_Add_RSSI_Range] @idTagType,@ID_Reader,-100,0,@Last_Modified_By
	


END
GO
