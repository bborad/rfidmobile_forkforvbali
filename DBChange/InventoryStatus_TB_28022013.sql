USE [OnRamp]
GO
/****** Object:  Table [dbo].[Inventory_Status]    Script Date: 02/28/2013 13:17:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Inventory_Status](
	[ID_InventoryStatus] [bigint] IDENTITY(1,1) NOT NULL,
	[InventoryStatusName] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Inventory_Status] PRIMARY KEY CLUSTERED 
(
	[ID_InventoryStatus] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF