USE [OnRamp]
GO
/****** Object:  Table [dbo].[Inventory_History]    Script Date: 02/28/2013 13:17:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventory_History](
	[ID_Asset] [bigint] NOT NULL,
	[InventoryDate] [datetime] NOT NULL,
	[InventoryLocation] [bigint] NOT NULL,
	[InventoryStatus] [bigint] NOT NULL,
	[InventoryBy] [bigint] NULL,
 CONSTRAINT [PK_InventoryHistroy] PRIMARY KEY CLUSTERED 
(
	[ID_Asset] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
