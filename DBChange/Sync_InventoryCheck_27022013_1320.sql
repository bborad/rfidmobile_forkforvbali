USE [OnRamp]
GO
/****** Object:  StoredProcedure [dbo].[Sync_InventoryCheck]    Script Date: 02/28/2013 13:18:37 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Cherry Jain>
-- Create date: <25 June 2008>
-- Description:	<Check Inventory Form SQL CE>
-- =============================================
ALTER PROCEDURE [dbo].[Sync_InventoryCheck] --'00000000000000000456222',1
	-- Add the parameters for the stored procedure here
(
@TagID  varchar(50),
@ID_Location bigint
)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;



    -- Insert statements for procedure here

IF EXISTS (SELECT ID_Asset FROM dbo.Assets WHERE TagID = @TagID)
	BEGIN
		 IF EXISTS( SELECT ID_Asset FROM dbo.Assets WHERE TagID = @TagID AND ID_Location = @ID_Location)
			BEGIN
				SELECT A.ID_Location as ID,L.Name as Location FROM dbo.Assets A
				INNER JOIN Locations  L  ON L.ID_Location = A.ID_Location 
				WHERE A.TagID = @TagID --------> Synchronised
			END
		 ELSE
			BEGIN
				SELECT A.ID_Location as ID,L.Name as Location FROM dbo.Assets A
				INNER JOIN Locations  L  ON L.ID_Location = A.ID_Location 
				WHERE A.TagID = @TagID --------> Location Mismatch
			END
	END
ELSE
	BEGIN 
			SELECT cast(-1 as bigint) AS ID , '' as Location --------> Tag Id Not Found
	END	

END
