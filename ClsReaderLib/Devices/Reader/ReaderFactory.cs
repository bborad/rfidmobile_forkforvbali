﻿using System;
using System.Collections.Generic;
 
using System.Text;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
//using ClsReaderLib.Devices.Reader.RFID;
using OnRamp;
using CS101ReaderLib;
using AT870ReaderLib;



namespace ClsReaderLib
{
    public static class ReaderFactory
    {
          static CS101ReaderLib.RFID_Reader objCS101Rdr;
          static AT870ReaderLib.RFID_Reader objAT870Rdr;

        public static Reader GetReader()
        {
            Reader rdr = null;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
            {
                if (objCS101Rdr == null)
                {
                    objCS101Rdr = new CS101ReaderLib.RFID_Reader();
                }
                rdr = (Reader)objCS101Rdr;
            }
            else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                if (objAT870Rdr == null)
                {
                    objAT870Rdr = new AT870ReaderLib.RFID_Reader();
                }

                rdr = (Reader)objAT870Rdr;
            }
            return rdr;
        }

        public static void Dispose()
        {
            try
            {
                if (objCS101Rdr != null)
                {
                    objCS101Rdr.Dispose();
                    objCS101Rdr = null;
                }
            }
            catch
            {
            }

            try
            {
                if (objAT870Rdr != null)
                {
                    objAT870Rdr.Dispose();
                    objAT870Rdr = null;
                }
            }
            catch
            {
            }

            
        }

    }


}
