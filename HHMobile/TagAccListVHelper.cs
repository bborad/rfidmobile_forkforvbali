using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    class TagAccListVHelper
    {
        #region Private Classes/Types
        // minimal class to represent tag+bankdata for each row of ListView
        private class ListVItemData
        {
            public UINT96_T EPC; // key
            public String[] BnkData;
            public UInt32 AccCnt; // Access Count

            public ListVItemData(ref UINT96_T tagEPC)
            {
                this.EPC = tagEPC;  // shallow copy
                BnkData = new String[4];
                AccCnt = 0;
            }
        }
        #endregion

        #region Private Member Variables
        private ListView EPCListV;
        #endregion

        #region constructor
        public TagAccListVHelper(ListView listV)
        {
            EPCListV = listV;
        }
        #endregion

        private bool TagAlreadyInListV(ref UINT96_T tag, out ListViewItem foundItem)
        {
            bool Matched = false;
            foundItem = null;
            foreach (ListViewItem item in EPCListV.Items)
            {
                if (item.Tag is ListVItemData)
                {
                    UINT96_T EPC = (UINT96_T)((ListVItemData)item.Tag).EPC;
                    Matched = EPC.Equals(tag);
                    if (Matched)
                    {
                        foundItem = item;
                        break; // from foreach
                    }
                }
                else
                {
                    throw new ApplicationException("ListVItem not of type ListVItemData");
                }
            }
            return Matched;
        }

        public ListViewItem AddEPC(UInt16 CRC, UInt16 PC, UINT96_T EPC,string Name,string location)
        {
            ListViewItem FoundItem = null;
            ListViewItem RetItem = null;

            if (TagAlreadyInListV(ref EPC, out FoundItem))
            {
                IncrAccCnt(FoundItem);
                RetItem = FoundItem;
            }
            else
            {
                int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
                // If not found, add new item
                ListViewItem item = new ListViewItem(new String[4] { (CurNumRows+1).ToString(),Name,location, 
                    EPC.ToString()});
                EPCListV.Items.Add(item);

                //Add ItemName and other column




                if (item.Tag != null)
                    throw new ApplicationException("ListViewItem Tag is already occupied");
                else //initialize array of 4 Strings (for each memory bank)
                {
                    ListVItemData RowData = new ListVItemData(ref EPC);
                    item.Tag = RowData;
                    IncrAccCnt(item);
                    // No need to specifically Read PC/Bank
                    StoreData(item, MemoryBanks4Op.One,
                        //CRC + PC + EPC 
                        CRC.ToString("X4") + PC.ToString("X4") + EPC.ToString());
                }
                RetItem = item;
            }

            return RetItem;
        }

        public ListViewItem AddEPC(UInt16 CRC, UInt16 PC, UINT96_T EPC)
        {
            ListViewItem FoundItem = null;
            ListViewItem RetItem = null;

            if (TagAlreadyInListV(ref EPC, out FoundItem))
            {
                IncrAccCnt(FoundItem);
                RetItem = FoundItem;
            }
            else
            {
                int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
                // If not found, add new item
                ListViewItem item = new ListViewItem(new String[2] { (CurNumRows+1).ToString(),
                    EPC.ToString()});
                EPCListV.Items.Add(item);

                //Add ItemName and other column
                



                if (item.Tag != null)
                    throw new ApplicationException("ListViewItem Tag is already occupied");
                else //initialize array of 4 Strings (for each memory bank)
                {
                    ListVItemData RowData = new ListVItemData(ref EPC);
                    item.Tag = RowData;
                    IncrAccCnt(item);
                    // No need to specifically Read PC/Bank
                    StoreData(item, MemoryBanks4Op.One,
                        //CRC + PC + EPC 
                        CRC.ToString("X4") + PC.ToString("X4") + EPC.ToString());
                }
                RetItem = item;
            }

            return RetItem;
        }

        
        public bool StoreData(ListViewItem item, MemoryBanks4Op bnk, ushort wdOffset, String tagData)
        {
            bool NewFld = false;

            if (item.Tag is ListVItemData)
            {
                String[] MemBanks = ((ListVItemData)item.Tag).BnkData;

                int ArrIdx = 0;
                switch (bnk)
                {
                    case MemoryBanks4Op.Zero:
                        ArrIdx = 0;
                        break;
                    case MemoryBanks4Op.One:
                        ArrIdx = 1;
                        break;
                    case MemoryBanks4Op.Two:
                        ArrIdx = 2;
                        break;
                    case MemoryBanks4Op.Three:
                        ArrIdx = 3;
                        break;
                    default:
                        throw new ApplicationException("Unexpected bank number");
                }
                NewFld = (MemBanks[ArrIdx] == null || MemBanks[ArrIdx].Length == 0);
                
                if (MemBanks[ArrIdx] == null)
                {
                    // How to represent "don't care" bits? Use '0' for now
                    MemBanks[ArrIdx] = tagData.PadLeft (wdOffset * 4 + tagData.Length, '0');
                }
                else
                {
                    String CurData = String.Copy(MemBanks[ArrIdx]);
                    MemBanks[ArrIdx] = CurData.Substring(0, wdOffset * 4) + tagData;
                    int NewLength = (wdOffset*4) + tagData.Length;
                    if (CurData.Length > NewLength)
                        // something after the tagData
                        MemBanks[ArrIdx] = MemBanks[ArrIdx] + CurData.Substring(NewLength);
                }
            }
            else
            {
                throw new ApplicationException("ListVItem not of type ListVItemData");
            }

            return NewFld;
        }

        public bool StoreData(ListViewItem item, MemoryBanks4Op bnk, String tagData)
        {
            return this.StoreData(item, bnk, 0, tagData);   
        }

        // Update ListView UI and ListVItemData at the same time
        private void IncrAccCnt(ListViewItem item)
        {
            if (item.Tag is ListVItemData)
            {
                ListVItemData RowData = (ListVItemData)item.Tag;
                RowData.AccCnt++;
                /* Column removed
                ListViewItem.ListViewSubItem AccCntCell = item.SubItems[2];
                AccCntCell.Text = RowData.AccCnt.ToString();
                 */
            }
            else
            {
                throw new ApplicationException("ListVItem not of type ListVItemData");
            }
        }

        public void GetEPC(ListViewItem item, out UINT96_T epc)
        {
            if (item.Tag is ListVItemData)
            {
                ListVItemData RowData = (ListVItemData)item.Tag;
                epc = RowData.EPC;
            }
            else
            {
                epc = new UINT96_T(0,0,0);
            }
        }

        // Replace Existing EPC and update ListViewItem Text
        public void SetEPC(ListViewItem item, ref UINT96_T epc)
        {
            if (item.Tag is ListVItemData)
            {
                ListVItemData RowData = (ListVItemData)item.Tag;
                RowData.EPC = epc;
                
                ListViewItem.ListViewSubItem EPCSubItem = item.SubItems[1]; // Column 2
                EPCSubItem.Text = epc.ToString();
            }
            else
            {
                throw new ApplicationException("ListVItem not of type ListVItemData");
            }
        }

        public ListViewItem GetRow (ref UINT96_T EPC)
        {
            ListViewItem row = null;
            TagAlreadyInListV(ref EPC, out row);
            return row;
        }

        public String[] GetBnkData(ListViewItem row)
        {
            String[] MemBnks;

            if (row.Tag is ListVItemData)
            {
                MemBnks = ((ListVItemData)row.Tag).BnkData;
               
            }
            else
            {
                MemBnks = null;
            }
            return MemBnks;
        }

        public String GetBnkData(ListViewItem row, int bnkNum)
        {
            return GetBnkData(row)[bnkNum];
        }
    }
}
