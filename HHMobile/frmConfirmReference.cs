/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Dispatch functionality
 **************************************************************************************/

using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using ClsLibBKLogs;
using ClsRampdb;
using ClsReaderLib;
using ClsReaderLib.Devices;


namespace OnRamp
{
    public partial class TagConfirmForm : Form
    {
        private String ItemTags = "-1";
        private String InTagList = "";
        Reader Rdr;
        private StringBuilder csvTags;

        private ArrayList tagArray, pagetagArray;
        private ArrayList pageArray_DispatchedItems;

        public bool barCodeScan = false;

        public ListView BCList;

        private StringBuilder allTags;
        //float LocationRSSI;
        //String ScannedTags = "-1";
        List<Assets> lstItems;
        DataTable dtTotal;

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        private TagAccListVHelper ListVHelper;
        private TagAccErrSummary errSummary;

        public TagConfirmForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            InitScanButtonState();

            InitRead1ButtonState();
            Rdr = ReaderFactory.GetReader();
            //ListVHelper = new TagAccListVHelper(this.EPCListV);

            errSummary = new TagAccErrSummary();

            lstItems = new List<Assets>();

            dtTotal = new DataTable();

        }

        private string _selectedDocumentID;

        public string SelectedDocumentID
        {
            get { return _selectedDocumentID; }
            set { _selectedDocumentID = value; }
        }

        #region AddBarCOdeTag

        //public void AddTagInfo(String sTagList)
        //{

        //    foreach (String sTagID in sTagList.Split(new char[] { ',' }))
        //    {
        //        if (sTagID != "-1")
        //        {
        //            try
        //            {
        //                TagInfo T = new TagInfo(sTagID);
        //                if (T.isAssetTag())
        //                {
        //                    ItemTags = ItemTags + "," + sTagID;
        //                }
        //                else if (T.isLocationTag())
        //                {
        //                    locationTags = locationTags + "," + sTagID;
        //                }
        //                else if (T.isEmployeeTag())
        //                {
        //                    EmployeeTags = EmployeeTags + "," + sTagID;
        //                }
        //                else
        //                {
        //                    UnknownTags = UnknownTags + "," + sTagID;
        //                }
        //            }
        //            catch (Exception ep)
        //            {
        //                throw ep;
        //            }
        //            finally
        //            {
        //                GotTagInfo = GotTagInfo + 1;
        //            }
        //        }
        //    }
        //    //Program.NoOfThreads = (Int16)(Program.NoOfThreads - 1);  
        //}

        public void AddListViewItems(ListView.ListViewItemCollection lAr)
        {
            try
            {
                ListViewItem lstItem;
                ListViewItem.ListViewSubItem ls;
                Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);
                lstAsset.CheckBoxes = true;
                Int32 iTag = 0;
                String TgNo;
                Assets Ast;

                foreach (ListViewItem li in lAr)
                {
                    iCount++;
                    iTag++;
                    //lstItem = new ListViewItem(iCount.ToString());
                    lstItem = new ListViewItem();

                    TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                    Ast = new Assets(TgNo);
                    if (Ast.ServerKey != 0)
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.Name.ToString());
                        lstItem.SubItems.Add(ls);

                        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString(Ast.LotNo.Trim());
                        lstItem.SubItems.Add(ls1);

                        lstItem.Checked = true;
                    }
                    else
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);

                        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls1);
                    }

                    ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                    ls2.Text = Convert.ToString(TgNo);
                    lstItem.SubItems.Add(ls2);

                    lstAsset.Items.Add(lstItem);
                }
                lstAsset.Refresh();

                lstAsset.Visible = true;
                epclistbox.Visible = false;
                lsvTotal.Visible = false;

                btnScreen.Visible = true;
                btnDispatchAll.Visible = true;
                btnDispatch.Visible = true;
                btnClear.Visible = true;
                btnAll.Visible = true;

                ScanButton.Visible = false;
                btnScanBarcode.Visible = false;

                //ScanButton.Enabled = true;
                //btnScnBarcd.Enabled = true;
                btnDispatch.Enabled = true;
                btnClear.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message);
                MessageBox.Show(ep.Message.ToString());
            }
        }

        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        #region Action State variables
        private int fldRdCnt = 0; // TBD: find a better method to store/manage this.
        #endregion

        #region Scan Button Routines
        private void InitScanButtonState()
        {
            OpState State = OpState.Stopped;
            this.ScanButton.Tag = State; // boxing
        }

        //private OpState ScanState()
        //{
        //    OpState state = OpState.Stopped;
        //    if (ScanButton.Tag is OpState)
        //    {
        //        state = (OpState)ScanButton.Tag;
        //    }
        //    else
        //    {
        //        throw new ApplicationException("ScanButton Tag is not OpState");
        //    }

        //    return state;
        //}

        //private void SetScanState(OpState newState)
        //{
        //    OpState CurState = ScanState();
        //    // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

        //    System.ComponentModel.ComponentResourceManager resources;

        //    if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
        //    {
        //        resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagRdForm));
        //    }
        //    else
        //    {
        //        resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
        //    }

        //    switch (newState)
        //    {
        //        case OpState.Starting:
        //            // disable scan button
        //            ScanButton.Enabled = false;
        //            // btnScnBarcd.Enabled = false;
        //            // disable read1 button


        //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
        //            break;
        //        case OpState.Started:
        //            ScanButton.Text = "Stop";
        //            ScanButton.Enabled = true;
        //            // btnScnBarcd.Enabled = true;

        //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        //            break;
        //        case OpState.Stopping:
        //            ScanButton.Enabled = false;
        //            // btnScnBarcd.Enabled = false;

        //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        //            break;
        //        case OpState.Stopped:
        //            ScanButton.Text = resources.GetString("ScanButton.Text");
        //            // btnScnBarcd.Enabled = true;
        //            ScanButton.Enabled = true;

        //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
        //            break;
        //    }

        //    ScanButton.Tag = newState;
        //}

        private void setScanTags()
        {
            int fromRec, toRec;

            fromRec = ((curPage - 1) * recPerPage) + 1;
            toRec = curPage * recPerPage;

            csvTags = new StringBuilder("'-1'");
            Int32 iTag;

            if (tagArray == null)
                tagArray = new ArrayList(recPerPage);
            else
                tagArray.Clear();

            if (pagetagArray == null)
                pagetagArray = new ArrayList(recPerPage);
            else
                pagetagArray.Clear();

            //if (EPCListV.Items.Count <= toRec)
            //    toRec = EPCListV.Items.Count;

            //for (iTag = fromRec - 1; iTag < toRec; iTag++)
            //{
            //    csvTags.Append("," + "'" + EPCListV.Items[iTag].SubItems[1].Text + "'");
            //    tagArray.Add(EPCListV.Items[iTag].SubItems[1].Text);
            //}
            //pagetagArray.AddRange(tagArray); 
            //lstItems.Clear();

            if (barCodeScan == false)
            {
                if (epclistbox.Items.Count <= toRec)
                    toRec = epclistbox.Items.Count;
                try
                {
                    for (iTag = fromRec - 1; iTag < toRec; iTag++)
                    {
                        csvTags.Append("," + "'" + epclistbox.Items[iTag].ToString() + "'");
                        tagArray.Add(epclistbox.Items[iTag].ToString());
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError("Inventory -- SetScanTags : " + ex.Message);
                }
            }
            else
            {
                if (BCList.Items.Count <= toRec)
                    toRec = BCList.Items.Count;

                string tagid;

                try
                {
                    int CurNumRows = epclistbox.Items.Count;

                    for (iTag = fromRec - 1; iTag < toRec; iTag++)
                    {
                        tagid = BCList.Items[iTag].SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                        csvTags.Append("," + "'" + tagid + "'");
                        tagArray.Add(tagid);
                        allTags.Append("," + tagid);
                        //ListViewItem item = new ListViewItem(new String[] { (CurNumRows + 1).ToString(), tagid });

                        //// Insert new item to the top of the list for easy viewing
                        epclistbox.Items.Insert(0, tagid);
                        CurNumRows++;
                    }
                }
                catch (Exception ex)
                {
                    Logger.LogError("Inventory -- SetScanTags : " + ex.Message);
                }
            }

            pagetagArray.AddRange(tagArray);
            lstItems.Clear();

        }

        private void FillDispatchData()
        {

            if (lstItems.Count <= 0 && InTagList.Length > 0)
            {
                //String a = "FFFFFFFF000000000000017D,FFFFFFFF000000000000017E,FFFFFFFF000000000000017F,FFFFFFFF0000000000000180,FFFFFFFF0000000000000181,FFFFFFFF0000000000000182,FFFFFFFF0000000000000183,FFFFFFFF0000000000000184,FFFFFFFF0000000000000185,FFFFFFFF0000000000000186,FFFFFFFF0000000000000187,FFFFFFFF0000000000000188,FFFFFFFF0000000000000189,FFFFFFFF000000000000018A,FFFFFFFF000000000000018B,FFFFFFFF000000000000018C,FFFFFFFF000000000000018D,FFFFFFFF000000000000018E,FFFFFFFF000000000000018F,FFFFFFFF0000000000000190,FFFFFFFF0000000000000191,FFFFFFFF0000000000000192,FFFFFFFF0000000000000193,FFFFFFFF0000000000000194,FFFFFFFF0000000000000195,FFFFFFFF0000000000000196,FFFFFFFF0000000000000197,FFFFFFFF0000000000000198,FFFFFFFF0000000000000199,FFFFFFFF000000000000019A,FFFFFFFF000000000000019B,FFFFFFFF000000000000019C,FFFFFFFF000000000000019D,FFFFFFFF000000000000019E,FFFFFFFF000000000000019F,FFFFFFFF00000000000001A0,FFFFFFFF00000000000001A1,FFFFFFFF00000000000001A2,FFFFFFFF00000000000001A3,FFFFFFFF00000000000001A4,FFFFFFFF00000000000001A5,FFFFFFFF00000000000001A5,FFFFFFFF00000000000001A7,FFFFFFFF00000000000001A8,FFFFFFFF00000000000001A9,FFFFFFFF00000000000001AA,FFFFFFFF00000000000001AB,FFFFFFFF00000000000001AC,FFFFFFFF00000000000001AD,FFFFFFFF00000000000001AE,FFFFFFFF00000000000001AF,FFFFFFFF00000000000001B0,FFFFFFFF00000000000001B1,FFFFFFFF00000000000001B2,FFFFFFFF00000000000001B3,FFFFFFFF00000000000001B4,FFFFFFFF00000000000001B5,FFFFFFFF00000000000001B6,FFFFFFFF00000000000001B7,FFFFFFFF00000000000001B8,FFFFFFFF00000000000001B9,FFFFFFFF00000000000001BA,FFFFFFFF00000000000001BB,FFFFFFFF00000000000001BC,FFFFFFFF00000000000001BD,FFFFFFFF00000000000001BE,FFFFFFFF00000000000001BF,FFFFFFFF00000000000001C0,FFFFFFFF00000000000001C1,FFFFFFFF00000000000001C2,FFFFFFFF00000000000001C3,FFFFFFFF00000000000001C4,FFFFFFFF00000000000001C5,FFFFFFFF00000000000001C6,FFFFFFFF00000000000001C7,FFFFFFFF00000000000001C8,FFFFFFFF00000000000001C9,FFFFFFFF00000000000001CA,FFFFFFFF00000000000001CB,FFFFFFFF00000000000001CC,FFFFFFFF00000000000001CD,FFFFFFFF00000000000001CE,FFFFFFFF00000000000001CF,FFFFFFFF00000000000001D0,FFFFFFFF00000000000001D1,FFFFFFFF00000000000001D2,FFFFFFFF00000000000001D3,FFFFFFFF00000000000001D4,FFFFFFFF00000000000001D5,FFFFFFFF00000000000001D6,FFFFFFFF00000000000001D7,FFFFFFFF00000000000001D8,FFFFFFFF00000000000001D9,FFFFFFFF00000000000001DA,FFFFFFFF00000000000001DB,FFFFFFFF00000000000001DC,FFFFFFFF00000000000001DD,FFFFFFFF00000000000001DE,FFFFFFFF00000000000001DF,FFFFFFFF00000000000001E0,FFFFFFFF00000000000001E1,FFFFFFFF00000000000001E2,FFFFFFFF00000000000001E3,FFFFFFFF00000000000001E4,FFFFFFFF00000000000001E5,FFFFFFFF00000000000001E6,FFFFFFFF00000000000001E7,FFFFFFFF00000000000001E8,FFFFFFFF00000000000001E9,FFFFFFFF00000000000001EA,FFFFFFFF00000000000001EB,FFFFFFFF00000000000001EC,FFFFFFFF00000000000001ED,FFFFFFFF00000000000001EE,FFFFFFFF00000000000001EF,FFFFFFFF00000000000001F0,FFFFFFFF00000000000001F1,FFFFFFFF00000000000001F2,FFFFFFFF00000000000001F3,FFFFFFFF00000000000001F4,FFFFFFFF00000000000001F5,FFFFFFFF00000000000001F6,FFFFFFFF00000000000001F7,FFFFFFFF00000000000001F8,FFFFFFFF00000000000001F9,FFFFFFFF00000000000001FA,FFFFFFFF00000000000001FB,FFFFFFFF00000000000001FC,FFFFFFFF00000000000001FD,FFFFFFFF00000000000001FE,FFFFFFFF00000000000001FF,FFFFFFFF0000000000000200,FFFFFFFF0000000000000201,FFFFFFFF0000000000000202,FFFFFFFF0000000000000203,FFFFFFFF0000000000000204,FFFFFFFF0000000000000205,FFFFFFFF0000000000000206,FFFFFFFF0000000000000207,FFFFFFFF0000000000000208,FFFFFFFF0000000000000209,FFFFFFFF000000000000020A,FFFFFFFF000000000000020B,FFFFFFFF000000000000020C,FFFFFFFF000000000000020D,FFFFFFFF000000000000020E,FFFFFFFF000000000000020F,FFFFFFFF0000000000000210,FFFFFFFF0000000000000211,FFFFFFFF0000000000000212,FFFFFFFF0000000000000213,FFFFFFFF0000000000000214,FFFFFFFF0000000000000215,FFFFFFFF0000000000000216,FFFFFFFF0000000000000217,FFFFFFFF0000000000000218,FFFFFFFF0000000000000219,FFFFFFFF000000000000021A,FFFFFFFF000000000000021B,FFFFFFFF000000000000021C,FFFFFFFF000000000000021D,FFFFFFFF000000000000021E,FFFFFFFF000000000000021F,FFFFFFFF0000000000000220,FFFFFFFF0000000000000221,FFFFFFFF0000000000000222,FFFFFFFF0000000000000223,FFFFFFFF0000000000000224,FFFFFFFF0000000000000225,FFFFFFFF0000000000000226,FFFFFFFF0000000000000227,FFFFFFFF0000000000000228,FFFFFFFF0000000000000229,FFFFFFFF000000000000022A,FFFFFFFF000000000000022B,FFFFFFFF000000000000022C,FFFFFFFF000000000000022D,FFFFFFFF000000000000022E,FFFFFFFF000000000000022F,FFFFFFFF0000000000000230,FFFFFFFF0000000000000231,FFFFFFFF0000000000000232,FFFFFFFF0000000000000233,FFFFFFFF0000000000000234,FFFFFFFF0000000000000235,FFFFFFFF0000000000000236,FFFFFFFF0000000000000237,FFFFFFFF0000000000000238,FFFFFFFF0000000000000239,FFFFFFFF000000000000023A,FFFFFFFF000000000000023B,FFFFFFFF000000000000023C,FFFFFFFF000000000000023D,FFFFFFFF000000000000023E,FFFFFFFF000000000000023F,FFFFFFFF0000000000000240,FFFFFFFF0000000000000241,FFFFFFFF0000000000000242,FFFFFFFF0000000000000243,FFFFFFFF0000000000000244";
                //lstItems = Assets.GetAssetsByTagID(ItemTags);//
                lstItems = Assets.GetAssetsByTagID("-1" + InTagList);//
            }
            //Program.NoOfThreads = (Int16)(Program.NoOfThreads - 1);
        }

        private void FillDispatchView()
        {
            lstAsset.Items.Clear();
            Cursor.Current = Cursors.WaitCursor;
            try
            {
                if (lstItems.Count <= 0 && csvTags.Length > 0)
                {
                    // lstItems = Assets.GetAssetsByTagID("-1" + InTagList);  
                    lstItems = Assets.GetAssets_ByTagID(csvTags.ToString(), recPerPage);
                }

                if (lstAsset.Items.Count <= 0)
                {
                    lstAsset.CheckBoxes = true;
                    ListViewItem lstItem;
                    ListViewItem.ListViewSubItem ls;
                    ListViewItem.ListViewSubItem ls1;
                    ListViewItem.ListViewSubItem ls2;

                    int index = 0;

                    ItemTags = "-1";
                    // string dispatched_page = pageArray_DispatchedItems[curPage - 1].ToString();
                    // string[] dispatchedItems = dispatched_page.Split(',');

                    foreach (Assets Ast in lstItems)
                    {
                        lstItem = new ListViewItem();
                        if (Ast.ServerKey != 0)
                        {
                            //ItemTags = ItemTags + "," + Ast.TagID.Trim();  

                            tagArray.Remove(Ast.TagID.Trim());

                            ls = new ListViewItem.ListViewSubItem();
                            ls.Text = Convert.ToString(Ast.Name.ToString());
                            lstItem.SubItems.Add(ls);

                            ls1 = new ListViewItem.ListViewSubItem();
                            ls1.Text = Convert.ToString(Ast.LotNo.ToString());
                            lstItem.SubItems.Add(ls1);

                            lstItem.Checked = true;
                            //foreach (string itemindex in dispatchedItems)
                            //{
                            //    if (index.ToString() == itemindex)
                            //    {
                            //        lstItem.BackColor = Color.Green;
                            //        lstItem.Checked = false;
                            //        break;
                            //    }
                            //}

                        }

                        ls2 = new ListViewItem.ListViewSubItem();
                        ls2.Text = Convert.ToString(Ast.TagID.Trim());
                        lstItem.SubItems.Add(ls2);

                        lstAsset.Items.Add(lstItem);
                        index++;
                    }

                    // String[] TagAr = Convert.ToString("-1" + InTagList).Split(new char[] { ',' });


                    foreach (String uTag in tagArray)
                    {
                        //if (uTag.Trim().IndexOf("-1") < 0)
                        // {
                        // if (ItemTags.IndexOf(uTag.Trim()) < 0)
                        //{
                        //Unknown Tag May be Employee or Locations
                        lstItem = new ListViewItem();

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);

                        ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString("");
                        lstItem.SubItems.Add(ls1);

                        ls2 = new ListViewItem.ListViewSubItem();
                        ls2.Text = Convert.ToString(uTag.Trim());
                        lstItem.SubItems.Add(ls2);

                        lstAsset.Items.Add(lstItem);
                        // }
                        //  else
                        //  {
                        //Known Items
                        //  }
                        //}
                    }
                    lstAsset.Refresh();
                }

                //ScanButton.Enabled = true;
                btnClear.Enabled = true;

                btnDispatch.Enabled = true;
                btnScreen.Tag = "Total";
                btnScreen.Text = "Total";
                lstAsset.Visible = true;
                lsvTotal.Visible = false;
                btnAll.Visible = true;
                //btnClear.Visible = false;
                Cursor.Current = Cursors.Default;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void FillTotalView()
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                ListViewItem lstItem;
                ListViewItem.ListViewSubItem lstSubItem;

                if (lsvTotal.Items.Count <= 0)
                {
                    if (dtTotal.Rows.Count <= 0)
                    {
                        //dtTotal = Assets.GetLotNoGroups("-1" + InTagList);  
                        dtTotal = Assets.GetLotNoGroups(allTags.ToString());
                    }

                    Int32 iCount = 0;
                    foreach (DataRow drLot in dtTotal.Rows)
                    {
                        lstItem = new ListViewItem();
                        lstItem.Text = drLot["LotNo"].ToString();
                        lstSubItem = new ListViewItem.ListViewSubItem();
                        lstSubItem.Text = drLot["Total"].ToString();
                        iCount = iCount + Convert.ToInt32(drLot["Total"]);
                        lstItem.SubItems.Add(lstSubItem);
                        lsvTotal.Items.Add(lstItem);
                    }

                    // String[] TagAr = Convert.ToString("-1" + InTagList).Split(new char[] { ',' });

                    // String[] TagAr = allTags.ToString().Split(new char[] { ',' });  

                    int totalCnt = epclistbox.Items.Count;

                    if (totalCnt > iCount)
                    {
                        lstItem = new ListViewItem();
                        lstItem.Text = "NA";

                        lstSubItem = new ListViewItem.ListViewSubItem();
                        lstSubItem.Text = Convert.ToString(totalCnt - iCount);

                        lstItem.SubItems.Add(lstSubItem);
                        lsvTotal.Items.Add(lstItem);
                    }

                    lsvTotal.Refresh();
                    this.Text = "Dispatch: Total Read - " + TagCntLabel.Text;

                }



                //ScanButton.Enabled = true;
                btnClear.Enabled = true;

                btnDispatch.Enabled = false;
                btnScreen.Tag = "Dispatch";
                //  btnScreen.Text = "Dispatch Screen";
                btnScreen.Text = "Details";
                lstAsset.Visible = false;
                lsvTotal.Visible = true;
                btnAll.Visible = false;
                btnClear.Visible = true;

                pnlDispatch.Visible = false;
                lsvTotal.BringToFront();

                Cursor.Current = Cursors.Default;
            }
            catch (Exception ep)
            {
                throw ep;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void OnScanButtonClicked(object sender, EventArgs e)
        {
            ScanButton.Enabled = false;
            try
            {
                barCodeScan = false;
                if (isReading)
                {
                    //stop reading
                    Rdr.TagInventoryStop();
                    ScanButton.Enabled = false;
                    isReading = false;
                    setState(isReading);
                    ScanButton.Enabled = false;
                    if (epclistbox.Items.Count > 0)
                    {
                        epclistbox.Visible = false;
                        Program.RefreshStatusLabel(this, "Data Processing..");

                        btnDispatch.Visible = true;
                        btnScreen.Enabled = true;
                        TagCntLabel.Visible = false;
                       
                        btnScreen.Visible = true;
                        btnDispatchAll.Visible = true;
                        btnDispatch.Visible = true;
                        btnClear.Visible = true;
                        btnAll.Visible = true;

                        btnAll.Enabled = false;
                        btnDispatch.Enabled = false;
                        btnDispatchAll.Enabled = false;

                        ScanButton.Visible = false;
                        btnScanBarcode.Visible = false;

                        lsvTotal.Visible = true;
                        Application.DoEvents();

                        SetupPaging(epclistbox.Items.Count);

                        //setScanTags(); 

                        FillTotalView();

                        //Program.NoOfThreads = (Int16)(Program.NoOfThreads + 1);
                        // System.Threading.Thread objDispatchData = new System.Threading.Thread(new System.Threading.ThreadStart(FillDispatchData));
                        // objDispatchData.Start();

                        //FillTotalView();
                        //if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
                        //{
                        //    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                        //}
                    }
                    else
                    {
                        this.Text = "Dispatch : Total Read - " + TagCntLabel.Text;
                    }
                    ScanButton.Enabled = true;
                }
                else
                {
                    //strart reading
                    if (btnScreen.Tag != null && btnScreen.Tag.ToString() == "Total")
                    {
                        btnScreen_Click(sender, e);
                    }


                    if (epclistbox.Items.Count < 1)
                    {
                        ItemTags = "-1";
                        InTagList = "";
                        allTags = new StringBuilder("-1");
                        //allTags = new StringBuilder("'-1'");
                    }
                    TagCntLabel.Visible = true;
                    btnDispatch.Visible = false;
                    dtTotal.Clear();
                    lstItems.Clear();
                    lstAsset.Items.Clear();
                    lsvTotal.Items.Clear();
                    btnScreen.Enabled = false;
                    lstAsset.Visible = false;
                    epclistbox.Visible = true;
                    lsvTotal.Visible = false;
                    if (Rdr.TagInventoryStart(new Reader.EPCEvent(TagReadEvent)))
                    {
                        isReading = true;
                        setState(isReading);
                    }
                    else
                    {
                        btnDispatch.Enabled = true;
                        btnClear.Enabled = true;
                    }
                    btnDispatch.Enabled = false;
                    btnClear.Enabled = false;

                }


                //CheckedTag = "";
                //LocationRSSI = 10000;
                // Scan or Stop
                //RFIDRdr Rdr = RFIDRdr.GetInstance();

                //switch (ScanState())
                //{
                //    case OpState.Stopped:
                //        if (EPCListV.Items.Count <= 0)
                //        {
                //            ItemTags = "-1";
                //            InTagList = "";
                //            allTags = new StringBuilder("-1");
                //            //allTags = new StringBuilder("'-1'");
                //        }

                //        if (btnScreen.Tag != null && btnScreen.Tag.ToString() == "Total")
                //        {
                //            btnScreen_Click(sender, e);
                //        }

                //        barCodeScan = false;

                //        TagCntLabel.Visible = true;
                //        btnDispatch.Visible = false;
                //        dtTotal.Clear();
                //        lstItems.Clear();
                //        lstAsset.Items.Clear();
                //        lsvTotal.Items.Clear();
                //        btnScreen.Enabled = false;
                //        lstAsset.Visible = false;
                //        EPCListV.Visible = true;
                //        lsvTotal.Visible = false;




                //        //Rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                //        //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                //        Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                //        Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                //        SetScanState(OpState.Starting);
                //        Program.RefreshStatusLabel(this, "Starting...");
                //        //Count Status Message
                //        if (!Rdr.SetRepeatedTagObsrvMode(true))
                //            // Continue despite error
                //            Program.ShowError("Disable Repeated Tag Observation mode failed");
                //        UserPref Pref = UserPref.GetInstance();

                //        bool Succ = Rdr.TagInventoryStart(5);
                //        if (!Succ)
                //        {
                //            SetScanState(OpState.Stopped);
                //            Program.RefreshStatusLabel(this, "Error Stopped");
                //            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                //            Program.ShowError("Error: Inventory Failed to Start\n");
                //            Application.DoEvents();
                //            btnDispatch.Enabled = true;
                //            btnClear.Enabled = true;

                //        }
                //        btnDispatch.Enabled = false;
                //        btnClear.Enabled = false;

                //        break;
                //    case OpState.Started:
                //        // Stop Tag Read
                //        SetScanState(OpState.Stopping);
                //        Program.RefreshStatusLabel(this, "Stopping...");
                //        if (!Rdr.TagInventoryStop())
                //        {
                //            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                //            SetScanState(OpState.Stopped); // give up
                //            Program.RefreshStatusLabel(this, "Error Stopped...");
                //            Program.ShowError("Error: TagRead Failed to Stop\n");
                //            btnDispatch.Enabled = false;
                //            btnClear.Enabled = false;

                //        }
                //        else
                //        {
                //            ScanButton.Enabled = false;
                //            //btnScnBarcd.Enabled = false;

                //            //lstAsset.Items.Clear();


                //            #region "commented"
                //            //lstAsset.CheckBoxes = true;
                //            //ListViewItem lstItem;
                //            //ListViewItem.ListViewSubItem ls;
                //            //Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);
                //            //Int32 iTag;
                //            //String TgNo;
                //            //Assets Ast;
                //            //DataRow drTotal;


                //            //for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                //            //{
                //            //    ScannedTags = "," + EPCListV.Items[iTag].SubItems[1].Text;


                //            //    iCount++;
                //            //    //lstItem = new ListViewItem(iCount.ToString());
                //            //    lstItem = new ListViewItem();

                //            //    //ls = new ListViewItem.ListViewSubItem();
                //            //    //ls.Text = Convert.ToString(iCount.ToString());
                //            //    //lstItem.SubItems.Add(ls);

                //            //    TgNo = EPCListV.Items[iTag].SubItems[1].Text;

                //            //    Ast = new Assets(TgNo);
                //            //    if (Ast.ServerKey != 0)
                //            //    {
                //            //        ls = new ListViewItem.ListViewSubItem();
                //            //        ls.Text = Convert.ToString(Ast.Name.ToString());
                //            //        lstItem.SubItems.Add(ls);

                //            //        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                //            //        ls1.Text = Convert.ToString(Ast.LotNo.ToString());
                //            //        lstItem.SubItems.Add(ls1);

                //            //        lstItem.Checked = true; 

                //            //        //Add Data in dtTotal to show Lot No wise Item Count
                //            //        drTotalAr = dtTotal.Select("LotNo='" + Ast.LotNo.Trim() + "'");
                //            //        if (drTotalAr.Length == 0)
                //            //        {
                //            //            drTotal = dtTotal.NewRow();
                //            //            drTotal["LotNo"] = Ast.LotNo.Trim();
                //            //            drTotal["Total"] = 1;
                //            //            dtTotal.Rows.Add(drTotal);
                //            //            dtTotal.AcceptChanges();  
                //            //        }
                //            //        else
                //            //        {
                //            //            drTotalAr[0]["Total"] = (Int32)drTotalAr[0]["Total"] + 1;
                //            //            drTotalAr[0].AcceptChanges();  
                //            //        }
                //            //    }
                //            //    else
                //            //    {
                //            //        ls = new ListViewItem.ListViewSubItem();
                //            //        ls.Text = Convert.ToString("NA");
                //            //        lstItem.SubItems.Add(ls);

                //            //        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                //            //        ls1.Text = Convert.ToString("NA");
                //            //        lstItem.SubItems.Add(ls1);
                //            //    }

                //            //    ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                //            //    ls2.Text = Convert.ToString(TgNo);
                //            //    lstItem.SubItems.Add(ls2);

                //            //    lstAsset.Items.Add(lstItem);
                //            //}
                //            //lstAsset.Refresh();
                //            #endregion "end commented"



                //        }

                //        break;
                //    case OpState.Starting:
                //        // Stop Tag Read
                //        SetScanState(OpState.Stopping);
                //        Program.RefreshStatusLabel(this, "Stopping...");
                //        if (!Rdr.TagInventoryStop())
                //        {
                //            // Restore
                //            SetScanState(OpState.Starting);
                //            Program.RefreshStatusLabel(this, "Starting...");
                //            Program.ShowWarning("Inventory Stop failed during Starting phase, please try again");
                //        }

                //        break;
                //    default:
                //        // ignore
                //        break;
                //}
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                //ScanButton.Enabled = true; 
            }

        }

        private void setState(bool reading)
        {
            if (reading)
            {
                //in reading mode
                ScanButton.Text = "Stop";
                ScanButton.Enabled = true;
            }
            else
            {
                //in ideal mode
                ScanButton.Text = "Scan Tags";
                // btnScnBarcd.Enabled = true;
                ScanButton.Enabled = true;

            }
        }

        private void btnScanBarcode_Click(object sender, EventArgs e)
        {


            //if (locationId != 0)
            //{
            InTagList = null;
            //epclistbox.Items.Clear();
            this.Text = "Inventory";
            lstAsset.Items.Clear();
            //tagList.Clear();
            //lstItems.Clear();

            if (epclistbox.Items.Count < 1)
            {
                ItemTags = "-1";
                InTagList = "";
                allTags = new StringBuilder("-1");
                //allTags = new StringBuilder("'-1'");
            }

            barCodeScan = true;
            // Open new BarCode Scanning Window
            frmBCScanForm fBCSFm = new frmBCScanForm();
            fBCSFm.Owner = this;
            fBCSFm.OpenMode = frmBCScanForm.FormType.Inventoryform;
            fBCSFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fBCSFm.Closed += new EventHandler(fBCSFm_Closed);
            //}
            //else
            //    MessageBox.Show("Please select reference location first.");
        }

        void fBCSFm_Closed(object sender, EventArgs e)
        {
            try
            {
                this.Enabled = true;
                AddListViewItems();
            }
            catch (Exception ex)
            {
            }
        }

        public void AddListViewItems()
        {
            try
            {
                if (BCList == null)
                {
                    return;
                }

                TagCntLabel.Text = BCList.Items.Count.ToString();

                // Int32 iTag = 0;
                String TgNo;
                //Assets Ast;

                if (BCList.Items.Count > 0)
                {
                    barCodeScan = true;
                    epclistbox.Visible = false;

                    //epclistbox.Items.Clear();
                    Program.RefreshStatusLabel(this, "Data Processing..");

                    btnDispatch.Visible = true;
                    btnScreen.Enabled = true;
                    TagCntLabel.Visible = false;

                    btnScreen.Visible = true;
                    btnDispatchAll.Visible = true;
                    btnDispatch.Visible = true;
                    btnClear.Visible = true;
                    btnAll.Visible = true;

                    btnAll.Enabled = false;
                    btnDispatch.Enabled = false;
                    btnDispatchAll.Enabled = false;

                    ScanButton.Visible = false;
                    btnScanBarcode.Visible = false;

                    lsvTotal.Visible = true;
                    Application.DoEvents();

                    SetupPaging(BCList.Items.Count);

                    setScanTags();

                    FillTotalView();

                    //Program.NoOfThreads = (Int16)(Program.NoOfThreads + 1);
                    // System.Threading.Thread objDispatchData = new System.Threading.Thread(new System.Threading.ThreadStart(FillDispatchData));
                    // objDispatchData.Start();

                    //FillTotalView();
                    if (epclistbox.SelectedIndex > 0)
                    {
                        ListViewItem row = new ListViewItem(epclistbox.Items[epclistbox.SelectedIndex].ToString());// EPCListV.Items[EPCListV.SelectedIndices[0]];
                    }
                }
                else
                {
                    this.Text = "Dispatch : Total Read - " + TagCntLabel.Text;
                }

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                ScanButton.Enabled = true;
                btnScanBarcode.Enabled = true;
            }
        }

        #endregion

        #region Paging Code

        int curPage, totalPages;
        public int recPerPage;

        DataTable dtPages;
        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;

                //pageArray_DispatchedItems = new ArrayList();
            }

            //cmbPage.Items.Clear();

            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
                //pageArray_DispatchedItems.Add("-1");
            }

            dtPages.AcceptChanges();

            cmbPage.DataSource = dtPages;
            cmbPage.DisplayMember = "Page";
            cmbPage.ValueMember = "Index";

            if (totalPages > 0)
                cmbPage.SelectedValue = 1;

            cmbPage.Visible = true;
            lblPage.Visible = true;
            cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void cmbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (curPage == Convert.ToInt32(cmbPage.SelectedValue))
                return;
            curPage = Convert.ToInt32(cmbPage.SelectedValue);
            setScanTags();
            FillDispatchView();
        }

        #endregion

        #region EPCListV Routines

        //
        // Helper function for RowGetEmptyBanks
        //
        private void CheckAndMarkEmptyBank(String[] memBnks, int bIdx,
            MemoryBanks4Op bnkToChk, ref MemoryBanks4Op markBnks)
        {
            if (memBnks[bIdx] == null || memBnks[bIdx].Length == 0)
                markBnks |= bnkToChk;
        }

        private MemoryBanks4Op RowGetEmptyBanks(ListViewItem item)
        {
            MemoryBanks4Op EmptyBanks = MemoryBanks4Op.None;
            String[] MemBanks = ListVHelper.GetBnkData(item);
            CheckAndMarkEmptyBank(MemBanks, 0, MemoryBanks4Op.Zero,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 1, MemoryBanks4Op.One,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 2, MemoryBanks4Op.Two,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 3, MemoryBanks4Op.Three,
                ref EmptyBanks);
            return EmptyBanks;
        }

        private void OnEPCListVSelChanged(object sender, EventArgs e)
        {
            //To be Delete
        }

        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text = epclistbox.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
            //Application.DoEvents();  
        }
        #endregion

        #region ScanRead Event Handlers
        //private void ScanRdOpStEventHandler(object sender, RdOpEventArgs e)
        //{
        //    // RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (e.Status)
        //    {
        //        case RdOpStatus.started:
        //            SetScanState(OpState.Started);
        //            Program.RefreshStatusLabel(this, "Scanning...");
        //            break;
        //        case RdOpStatus.error:
        //            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                MessageBox.Show("Error: " + e.ErrMsg);
        //            break;
        //        case RdOpStatus.tagAccError:
        //            errSummary.Rec(e.TagAccErr);
        //            break;
        //        case RdOpStatus.errorStopped:
        //        case RdOpStatus.stopped:
        //            //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
        //            //Rdr.RdOpStEvent -= ScanRdOpStEventHandler;
        //            Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
        //            Rdr.UnregisterRdOpStEvent(ScanRdOpStEventHandler);
        //            SetScanState(OpState.Stopped);
        //            if (fldRdCnt > 0)
        //            {
        //                if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
        //                {
        //                    EPCListV.Items[0].Selected = true; // select and focus on the first one
        //                    EPCListV.Focus();
        //                }
        //                Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " new Tags");
        //            }
        //            else
        //                Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
        //            if (e.Status == RdOpStatus.errorStopped)
        //            {
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                        MessageBox.Show("Error: " + e.ErrMsg);
        //            }
        //            break;
        //    }
        //}

        private void MemBnkRdEvtHandler(object sender, MemBnkRdEventArgs e)
        {
            bool FldAdded;
            // Debug: Add/Update Tag to EPCListV 
            // Save/Update Assciated Data with Row
            ListViewItem Item = ListVHelper.AddEPC(e.CRC, e.PC, e.EPC);
            RefreshTagCntLabel();
            // associate bank data with row
            if (e.BankNum == MemoryBanks4Op.One)
                FldAdded = false; // ignore this bank
            else
            {
                if (BnkRdBuf == null)
                    BnkRdBuf = new StringBuilder();
                if (BnkRdOffset == 0 && BnkRdBuf.Length > 0)
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                BnkRdBuf = BnkRdBuf.Append(e.Data);
                BnkRdOffset += e.Data.Length / 4; // 4 hex chars per Word
                // if full-bank is read, store to list and reset the buffer
                if ((BnkRdBuf.Length / 4) == EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(e.BankNum)))
                {
                    FldAdded = ListVHelper.StoreData(Item, e.BankNum, BnkRdBuf.ToString());
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                    BnkRdOffset = 0;
                }
                else
                {
                    FldAdded = false;
                }
            }
            if (FldAdded)
            {
                fldRdCnt++;
                Program.RefreshStatusLabel(this, "Fields Added : " + fldRdCnt);
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }


        }

        //private bool Bnk2ReqCb(out MemoryBanks4Op bnks2Rd)
        //{
        //    bnks2Rd = MemoryBanks4Op.Two;
        //    if (errSummary.UnrecoverableErrOccurred)
        //        return false;
        //    if (ScanState() == OpState.Stopping || ScanState() == OpState.Stopped)
        //        return false;
        //    // Should we continue if invalid address error / too many errors occurs?
        //    return true; // until user stops
        //}
        #endregion

        #region Tag Inventory Event Handler

        private TagRangeList tagList = new TagRangeList();

        private void TagReadEvent(string epc, string rssi)
        {
            this.BeginInvoke(new Action<string>((string e)=>{
                if (!epclistbox.Items.Contains(e))
                {
                    epclistbox.Items.Insert(0, e);
                    allTags.Append("," + e);
                    TagCntLabel.Text = epclistbox.Items.Count.ToString();
                }
            }),epc);
        }
        //private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        //{
        //    //The Event where Scanned Tags are coming and can form some operation on it.
        //    // Add new TagID to list (or update occurrence)
        //    if (e.Cnt != 0)
        //    {
        //        if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
        //        {
        //            // ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);

        //            // allTags.Append("," + "'" + e.EPC + "'"); 
        //            allTags.Append("," + e.EPC);

        //            AddEPCToListV(e.PC, e.EPC);

        //            RefreshTagCntLabel();
        //            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
        //        }
        //    }
        //}

        //private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        //{
        //    OpState State = ScanState();
        //    //RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (e.status)
        //    {
        //        case InvtryOpStatus.started:
        //            SetScanState(OpState.Started);
        //            Program.RefreshStatusLabel(this, "Scanning...");
        //            break;
        //        case InvtryOpStatus.stopped:
        //        case InvtryOpStatus.errorStopped:
        //        case InvtryOpStatus.macErrorStopped:
        //            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
        //            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //            if (e.status == InvtryOpStatus.errorStopped)
        //            {
        //                // Display error or Restart Radio (if necessary)
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    Program.ShowError("Inventory Stopped with Error: " + e.msg);
        //            }
        //            else if (e.status == InvtryOpStatus.macErrorStopped)
        //            {
        //                //ushort MacErrCode;
        //                if (Rdr.GetMacError() && Reader.macerr != 0)
        //                {
        //                    // if fatal mac error, display message
        //                    if (Rdr.MacErrorIsFatal() || (ScanState() != OpState.Stopping))
        //                        Program.ShowError(e.msg);
        //                }
        //                else
        //                    Program.ShowError("Unknown HW error. Abort");
        //            }
        //            // restore StartButton
        //            SetScanState(OpState.Stopped);
        //            Program.RefreshStatusLabel(this, "Finished");
        //            break;
        //        case InvtryOpStatus.error:
        //            // restore StartButton
        //            switch (State)
        //            {
        //                case OpState.Starting:
        //                    MessageBox.Show("Check connection and try again.", "Inventory Start Error");
        //                    //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
        //                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //                    break;
        //                case OpState.Started:
        //                    // TBD
        //                    MessageBox.Show(e.msg, "Inventry Error");
        //                    break;
        //                case OpState.Stopping:
        //                    MessageBox.Show(e.msg, "Inventory Stop Error");
        //                    //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
        //                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
        //                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
        //                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
        //                    break;
        //                case OpState.Stopped:
        //                    throw new ApplicationException("Unexpected error return during Stopped State");
        //            }
        //            SetScanState(OpState.Stopped);
        //            Program.RefreshStatusLabel(this, "Error Stopped");
        //            break;
        //    }
        //    // Re-enable Field Input Textboxes (disabled during inventory operation)
        //    if (ScanState() == OpState.Stopped)
        //    {
        //        ScanButton.Enabled = false;
        //        Cursor.Current = Cursors.WaitCursor;
        //        // Enable (Avail) Text Fields
        //        try
        //        {
        //            if (EPCListV.Items.Count > 0)
        //            {
        //                EPCListV.Visible = false;
        //                Program.RefreshStatusLabel(this, "Data Processing..");

        //                btnDispatch.Visible = true;
        //                btnScreen.Enabled = true;
        //                TagCntLabel.Visible = false;
        //                barCodeScan = false;
        //                btnScreen.Visible = true;
        //                btnDispatchAll.Visible = true;
        //                btnDispatch.Visible = true;
        //                btnClear.Visible = true;
        //                btnAll.Visible = true;

        //                btnAll.Enabled = false;
        //                btnDispatch.Enabled = false;
        //                btnDispatchAll.Enabled = false;

        //                ScanButton.Visible = false;
        //                btnScanBarcode.Visible = false;

        //                lsvTotal.Visible = true;
        //                Application.DoEvents();

        //                SetupPaging(EPCListV.Items.Count);

        //                // setScanTags(); 

        //                FillTotalView();

        //                //Program.NoOfThreads = (Int16)(Program.NoOfThreads + 1);
        //                // System.Threading.Thread objDispatchData = new System.Threading.Thread(new System.Threading.ThreadStart(FillDispatchData));
        //                // objDispatchData.Start();

        //                //FillTotalView();
        //                if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
        //                {
        //                    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
        //                }
        //            }
        //            else
        //            {
        //                this.Text = "Dispatch : Total Read - " + TagCntLabel.Text;
        //            }
        //        }
        //        catch (Exception epx)
        //        {
        //            throw epx;
        //        }
        //        finally
        //        {
        //            ScanButton.Enabled = true;
        //            Cursor.Current = Cursors.Default;
        //        }
        //    }
        //}

        //private void AddEPCToListV(UInt16 PC, UINT96_T EPC)
        //{
        //    // Assuming that the Tag of this EPC is not already in list
        //    int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
        //    ListViewItem item = new ListViewItem(new String[] {
        //         (CurNumRows+1).ToString(), EPC.ToString() });
        //    // Insert new item to the top of the list for easy viewing
        //    EPCListV.Items.Insert(0, item);
        //    item.Tag = PC;
        //    // EPCListV.Refresh();
        //    // RefreshTagCntLabel();
        //}


        #endregion

        #region Read1 Button Routines
        private void InitRead1ButtonState()
        {
            //OpState State = OpState.Stopped;
        }

        private OpState Read1State()
        {
            OpState state = OpState.Stopped;
            return state;
        }

        private void SetRead1State(OpState newState)
        {
            OpState CurState = Read1State();
            // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagRdForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            }

            switch (newState)
            {
                case OpState.Starting:


                    // disable scan button
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:


                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:

                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ScanButton.Enabled = true;
                    // btnScnBarcd.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }
        }

        //private bool GetCurrRowUnreadBnk(out MemoryBanks4Op bnk2Rd, out UINT96_T epc)
        //{
        //    bool Succ = false;
        //    epc = new UINT96_T();
        //    bnk2Rd = MemoryBanks4Op.None;
        //    MemoryBanks4Op UnreadBnks = MemoryBanks4Op.None;
        //    if ((EPCListV.Items.Count > 0) && EPCListV.SelectedIndices.Count > 0)
        //    {
        //        Succ = true;
        //        ListViewItem SelectedRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
        //        ListVHelper.GetEPC(SelectedRow, out epc);
        //        UnreadBnks = RowGetEmptyBanks(SelectedRow)
        //            & EPCTag.AvailBanks;
        //        bnk2Rd = EPCTag.CurBankToAcc(UnreadBnks);
        //    }
        //    else
        //        Succ = false;

        //    return Succ;
        //}

        //private void OnRead1ButtonClicked(object sender, EventArgs e)
        //{
        //    // Read1 or Stop
        //    // RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (Read1State())
        //    {
        //        case OpState.Stopped:
        //            // Target EPC to Read (currently selected) 
        //            // TBD: strongest RSSI (sorted list?)
        //            UINT96_T TgtEPC = new UINT96_T();
        //            MemoryBanks4Op NxtBnk2Rd = MemoryBanks4Op.None;
        //            if (GetCurrRowUnreadBnk(out NxtBnk2Rd, out TgtEPC))
        //            {
        //                if (NxtBnk2Rd == MemoryBanks4Op.None)
        //                {
        //                    MessageBox.Show("No Empty Banks to Read");
        //                    return;
        //                }
        //            }
        //            else
        //            {
        //                MessageBox.Show("Select a tag from the list first");
        //                return;
        //            }
        //            // Prompt for password if required
        //            UInt32 CurAccPasswd = 0;
        //            if (PwdReqChkBx.Visible == true && PwdReqChkBx.Checked == true)
        //            {
        //                if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
        //                {
        //                    // user cancel
        //                    return;
        //                }
        //            }
        //            //Rdr.RdOpStEvent += new EventHandler<RdOpEventArgs>(TgtRdOpStEventHandler);
        //            //Rdr.MemBnkRdEvent += MemBnkRdEvtHandler;
        //            Rdr.RegisterRdOpStEvent(TgtRdOpStEventHandler);
        //            Rdr.RegisterMemBnkRdEvent(MemBnkRdEvtHandler);
        //            SetRead1State(OpState.Starting);
        //            Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
        //            fldRdCnt = 0;
        //            BnkRdOffset = 0;
        //            errSummary.Clear();
        //            UserPref Pref = UserPref.GetInstance();
        //            Rdr.TagtEPC = TgtEPC;
        //            // Rdr.TagReadBankData += TgtBnkReqCb;
        //            Rdr.RegisterTagReadBankDataEvent(TgtBnkReqCb);
        //            Rdr.CurAccPasswd = CurAccPasswd;
        //            if (!Rdr.TagReadBanksStart())
        //            {
        //                SetRead1State(OpState.Stopped);
        //                Program.RefreshStatusLabel(this, "Error Stopped");
        //                MessageBox.Show("Error: TagRead Failed to Start\n");
        //                //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
        //                //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
        //                Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
        //                Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);

        //            }
        //            break;
        //        case OpState.Started:
        //            // Stop Tag Read
        //            SetRead1State(OpState.Stopping);
        //            Program.RefreshStatusLabel(this, "Stopping...");

        //            if (!Rdr.TagReadStop())
        //            {
        //                //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
        //                //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
        //                Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
        //                Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
        //                MessageBox.Show("Error: TagRead Failed to Stop\n");
        //                SetRead1State(OpState.Stopped); // give up
        //                Program.RefreshStatusLabel(this, "Error Stopped...");

        //            }
        //            break;
        //        default:
        //            // ignore
        //            break;
        //    }
        //}


        #endregion


        #region TgtRead Event Handlers
        //private void TgtRdOpStEventHandler(object sender, RdOpEventArgs e)
        //{
        //    // RFIDRdr Rdr = RFIDRdr.GetInstance();
        //    Reader Rdr = ReaderFactory.GetReader();
        //    switch (e.Status)
        //    {
        //        case RdOpStatus.started:
        //            SetRead1State(OpState.Started);
        //            UINT96_T EPC;
        //            MemoryBanks4Op CurEmptyBnk;
        //            GetCurrRowUnreadBnk(out CurEmptyBnk, out EPC);
        //            Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(CurEmptyBnk) + "." + BnkRdOffset);
        //            break;
        //        case RdOpStatus.completed:
        //            break;
        //        case RdOpStatus.error:
        //            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                MessageBox.Show("Error: " + e.ErrMsg);
        //            break;
        //        case RdOpStatus.tagAccError:
        //            // Add error to summary
        //            errSummary.Rec(e.TagAccErr);
        //            // display error immediately upon Unrecoverable Errors
        //            switch (e.TagAccErr)
        //            {
        //                case AccErrorTypes.Unauthorized:
        //                    MessageBox.Show("Password Required", "Tag Bank Read Failed");
        //                    break;
        //                case AccErrorTypes.AccessPasswordError:
        //                    MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Read Failed");
        //                    break;
        //                case AccErrorTypes.InvalidAddr:
        //                    MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
        //                        "Tag Bank Read Failed");
        //                    break;
        //            }
        //            break;
        //        case RdOpStatus.stopped:
        //        case RdOpStatus.errorStopped:
        //            //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
        //            //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
        //            Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
        //            Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);

        //            SetRead1State(OpState.Stopped);
        //            if (fldRdCnt > 0)
        //                Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " Banks");
        //            else
        //                Program.RefreshStatusLabel(this, "Stopped:  " + fldRdCnt + " Banks");
        //            if (e.Status == RdOpStatus.errorStopped)
        //            {
        //                if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
        //                    RestartRfidDevice();
        //                else
        //                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
        //                        MessageBox.Show("Error: " + e.ErrMsg);
        //            }
        //            break;
        //    }
        //}

        StringBuilder BnkRdBuf = null;
        int BnkRdOffset = 0;

        private ushort OptNumWdsToRd(MemoryBanks4Op bnk2Rd, int wdOffset)
        {
            ushort MaxNumWds = EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk2Rd));
            return (ushort)Math.Min(4, MaxNumWds - wdOffset);
        }

        //private bool TgtBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        //{
        //    bnk2Rd = MemoryBanks4Op.None;
        //    wdOffset = 0;
        //    wdCnt = 0;

        //    // if unrecoverable error from last read (such as password), stop
        //    if (errSummary.UnrecoverableErrOccurred)
        //        return false;
        //    if (Read1State() == OpState.Stopping || Read1State() == OpState.Stopped)
        //        return false;
        //    UINT96_T TgtEPC;
        //    if (GetCurrRowUnreadBnk(out bnk2Rd, out TgtEPC) == false)
        //        return false;
        //    bool MoreBnks2Rd = (bnk2Rd != MemoryBanks4Op.None);
        //    if (MoreBnks2Rd)
        //    {
        //        wdOffset = (ushort)BnkRdOffset;
        //        wdCnt = OptNumWdsToRd(bnk2Rd, wdOffset);
        //        Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(bnk2Rd)
        //            + "." + wdOffset);
        //    }
        //    return MoreBnks2Rd;
        //}
        #endregion

        public bool isReading { get; set; }
        private bool AnyOperationRunning()
        {
            return isReading;
        }

        private void OnTagRdFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            if (AnyOperationRunning())
            {
                MessageBox.Show("Please stop the scan operation first");
                e.Cancel = true;
            }

            // TBD: Ask for confirmation here
        }

        private void OnTagRdFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnTagRdFormLoad(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }
            //pnlDispatch.BringToFront();
            pnlDispatch.Visible = false;

            btnScreen.Visible = false;
            btnDispatchAll.Visible = false;
            btnDispatch.Visible = false;
            btnClear.Visible = false;
            btnAll.Visible = false;

            barCodeScan = false;

            ScanButton.Visible = true;
            btnScanBarcode.Visible = true;

            //Set Document Combo Deepanshu
            DataTable dtList = new DataTable();
            dtList = Documents.getRows();

            DataRow dr = dtList.NewRow();
            dr["DocumentID"] = 0;
            dr["DocumentNo"] = "Select Document";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            curPage = 1;
            // Scan Rd
            //this.OnScanButtonClicked(this, null);
        }

        #region F11/F4/F5 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (!this.Enabled)
            {
                return;
            }

            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if ((!F11Depressed) && (!AnyOperationRunning()))
                            {
                                OnScanButtonClicked(this, null);
                            }
                            F11Depressed = true;
                        }
                        else // up
                        {
                            // Stop running Scan-Rd Op
                            if (isReading)
                            {
                                OnScanButtonClicked(this, null);
                            }
                            F11Depressed = false;
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                // case Keys.F7:
                //  case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (AnyOperationRunning())
                    {
                        MessageBox.Show("Please stop any Tag Operation first", "Denied");

                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        #endregion

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void btnDispatch_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string dispatchLocationID = Locations.getDispatchLocation();

            if (dispatchLocationID == "")
            {
                string t = "0000000000000000000000D";
                Locations.AddLocation(t, "D001", "Dispatched");
                Locations L = new Locations(t);
                dispatchLocationID = Convert.ToString(L.ServerKey);
            }

            ArrayList tmpList = new ArrayList();
            StringBuilder tags = new StringBuilder("'-1'");
            ArrayList csvArray = new ArrayList();

            int itemChecked = 0;

            //Change Here 
            try
            {
                if (_selectedDocumentID == null)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("No reference no.");
                    return;
                }
                ////////////////////////////////////

                int iOther = 0, iCount = 0;
                foreach (ListViewItem lv in lstAsset.Items)
                {
                    if (lv.Checked && lv.BackColor != Color.Green)
                    {
                        iCount++;

                        if (lv.SubItems[1].Text.ToString().Trim() != "NA")
                        {
                            itemChecked = 1;
                            if (tags.Length >= 6000)
                            {
                                csvArray.Add(tags.ToString());
                                tags = new StringBuilder("'-1'");
                            }
                            tags.Append("," + "'" + lv.SubItems[3].Text + "'");

                            // tmpList.Add(lv.Index);

                            tmpList.Add(pagetagArray.IndexOf(lv.SubItems[3].Text));

                            //Assets.UpdateAssetLocation(lv.SubItems[3].Text.ToString().Trim(), Convert.ToInt32(dispatchLocationID), SelectedDocumentID, true);
                        }
                        else
                            iOther++;

                    }
                }

                if (iCount == 0)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Please select first.");
                }
                else if (itemChecked == 1)
                {
                    int result = 0;
                    csvArray.Add(tags.ToString());
                    foreach (string strCsvTag in csvArray)
                    {
                        result = result + Assets.UpdateAssetLocation(strCsvTag, Convert.ToInt64(dispatchLocationID), SelectedDocumentID, true);
                    }

                    tmpList.Sort();
                    int i;
                    int page = (curPage - 1) * recPerPage;
                    // string itemlist = pageArray_DispatchedItems[curPage - 1].ToString();
                    for (i = tmpList.Count - 1; i >= 0; i--)
                    {
                        //lstAsset.Items.RemoveAt(Convert.ToInt32(tmpList[i]));
                        // lstAsset.Items[Convert.ToInt32(tmpList[i])].BackColor = Color.Green;
                        // lstAsset.Items[Convert.ToInt32(tmpList[i])].Checked = false;
                        // itemlist = itemlist + "," + tmpList[i].ToString();

                        epclistbox.Items.RemoveAt(page + Convert.ToInt32(tmpList[i]));
                        

                    }
                    //pageArray_DispatchedItems[curPage - 1] = itemlist;                   

                    Cursor.Current = Cursors.Default;

                    MessageBox.Show("All selected items are dispatched.");

                    if (epclistbox.Items.Count > 0)
                    {
                        SetupPaging(epclistbox.Items.Count);
                        curPage = 1;
                        setScanTags();
                        FillDispatchView();
                    }
                    else
                        btnClear_Click(sender, e);
                }
                else if (iOther > 0)
                    MessageBox.Show("Please select valid item(s).");


                lstAsset.Refresh();

                //if (iOther > 0)
                //    MessageBox.Show("Descripencies removed only for Location_Mismatch error code.");
                //else
                //    MessageBox.Show("Descripencies removed successfully.");

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void PwdReqChkBx_CheckStateChanged(object sender, EventArgs e)
        {

        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tagList.Clear();
            epclistbox.Items.Clear();
            lstAsset.Items.Clear();
            lsvTotal.Items.Clear();

            lsvTotal.Visible = false;

            lstItems.Clear();
            epclistbox.Visible = true;
            btnDispatch.Enabled = false;
            RefreshTagCntLabel();

            barCodeScan = false;

            pnlDispatch.Visible = false;
            // EPCListV.BringToFront();

            btnScreen.Visible = false;
            btnDispatchAll.Visible = false;
            btnDispatch.Visible = false;
            btnClear.Visible = false;
            btnAll.Visible = false;

            ScanButton.Visible = true;
            ScanButton.Enabled = true;

            btnScanBarcode.Visible = true;
            btnScanBarcode.Enabled = true;

            this.Text = "Dispatch - " + UserPref.CurVersionNo;

        }

        private void btnScreen_Click(object sender, EventArgs e)
        {
            if (btnScreen.Tag.ToString() == "Dispatch")
            {
                pnlDispatch.Visible = true;
                pnlDispatch.BringToFront();

                if (lstAsset.Items.Count <= 0)
                {
                    curPage = 1;
                    if (!barCodeScan)
                        setScanTags();
                    FillDispatchView();
                }

                // FillDispatchView();
                btnScreen.Tag = "Total";
                btnScreen.Text = "Total";

                lstAsset.Visible = true;
                lsvTotal.Visible = false;

                btnAll.Enabled = true;
                btnDispatch.Enabled = true;
                btnDispatchAll.Enabled = true;

            }
            else
            {
                //FillTotalView(); 

                btnScreen.Tag = "Dispatch";
                //  btnScreen.Text = "Dispatch Screen";
                btnScreen.Text = "Details";
                lstAsset.Visible = false;
                lsvTotal.Visible = true;

                pnlDispatch.Visible = false;
                lsvTotal.BringToFront();

                btnAll.Enabled = false;
                btnDispatch.Enabled = false;
                btnDispatchAll.Enabled = false;

            }
        }

        private void btnAll_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem LI in lstAsset.Items)
            {
                if (LI.SubItems[1].Text != "NA")
                {
                    LI.Checked = true;
                }
            }
            lstAsset.Refresh();
        }

        private void btnDispatchAll_Click(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            string dispatchLocationID = Locations.getDispatchLocation();

            if (dispatchLocationID == "")
            {
                string t = "0000000000000000000000D";
                Locations.AddLocation(t, "D001", "Dispatched");
                Locations L = new Locations(t);
                dispatchLocationID = Convert.ToString(L.ServerKey);
            }

            ArrayList tmpList = new ArrayList();
            StringBuilder tags = new StringBuilder("'-1'");
            ArrayList csvArray = new ArrayList();


            //Change Here 
            try
            {
                if (_selectedDocumentID == null)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("No reference no.");
                    return;
                }

                foreach (string lv in epclistbox.Items)
                {
                    if (tags.Length >= 6000)
                    {
                        csvArray.Add(tags.ToString());
                        tags = new StringBuilder("'-1'");
                    }
                    tags.Append("," + "'" + lv + "'");
                    // tmpList.Add(lv.Index);
                }

                csvArray.Add(tags.ToString());

                if (csvArray[0].ToString().Length > 4)
                {

                    foreach (string strCsvTag in csvArray)
                    {
                        Assets.UpdateAssetLocation(strCsvTag, Convert.ToInt64(dispatchLocationID), SelectedDocumentID, true);
                    }
                    MessageBox.Show("All valid items are dispatched.");

                    lstAsset.Refresh();

                    btnClear_Click(sender, e);
                }
                else
                {
                    MessageBox.Show("No items to dispatch.");
                }



                //tmpList.Sort();
                //int i;
                //for (i = tmpList.Count - 1; i >= 0; i--)
                //{
                //    lstAsset.Items.RemoveAt(Convert.ToInt32(tmpList[i]));
                //}

                Cursor.Current = Cursors.Default;

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
    }


}