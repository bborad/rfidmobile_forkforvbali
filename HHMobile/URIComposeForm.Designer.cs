namespace OnRamp
{
    partial class URIComposeForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(URIComposeForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ProtoCmbBx = new System.Windows.Forms.ComboBox();
            this.HostNmTxtBx = new System.Windows.Forms.TextBox();
            this.RemPathTxtBx = new System.Windows.Forms.TextBox();
            this.PortNumTxtBx = new System.Windows.Forms.TextBox();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.UsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PasswdTxtBx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // ProtoCmbBx
            // 
            this.ProtoCmbBx.Items.Add(resources.GetString("ProtoCmbBx.Items"));
            this.ProtoCmbBx.Items.Add(resources.GetString("ProtoCmbBx.Items1"));
            resources.ApplyResources(this.ProtoCmbBx, "ProtoCmbBx");
            this.ProtoCmbBx.Name = "ProtoCmbBx";
            this.ProtoCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnProtocolSelChanged);
            // 
            // HostNmTxtBx
            // 
            resources.ApplyResources(this.HostNmTxtBx, "HostNmTxtBx");
            this.HostNmTxtBx.Name = "HostNmTxtBx";
            // 
            // RemPathTxtBx
            // 
            resources.ApplyResources(this.RemPathTxtBx, "RemPathTxtBx");
            this.RemPathTxtBx.Name = "RemPathTxtBx";
            // 
            // PortNumTxtBx
            // 
            resources.ApplyResources(this.PortNumTxtBx, "PortNumTxtBx");
            this.PortNumTxtBx.Name = "PortNumTxtBx";
            this.PortNumTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPortTxtBxKeyPressed);
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // UsrNameTxtBx
            // 
            resources.ApplyResources(this.UsrNameTxtBx, "UsrNameTxtBx");
            this.UsrNameTxtBx.Name = "UsrNameTxtBx";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // PasswdTxtBx
            // 
            resources.ApplyResources(this.PasswdTxtBx, "PasswdTxtBx");
            this.PasswdTxtBx.Name = "PasswdTxtBx";
            // 
            // URIComposeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PasswdTxtBx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.UsrNameTxtBx);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.PortNumTxtBx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RemPathTxtBx);
            this.Controls.Add(this.HostNmTxtBx);
            this.Controls.Add(this.ProtoCmbBx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "URIComposeForm";
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.URIComposeForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ProtoCmbBx = new System.Windows.Forms.ComboBox();
            this.HostNmTxtBx = new System.Windows.Forms.TextBox();
            this.RemPathTxtBx = new System.Windows.Forms.TextBox();
            this.PortNumTxtBx = new System.Windows.Forms.TextBox();
            this.OKBttn = new System.Windows.Forms.Button();
            this.CancelBttn = new System.Windows.Forms.Button();
            this.UsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.PasswdTxtBx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // ProtoCmbBx
            // 
            this.ProtoCmbBx.Items.Add(resources.GetString("ProtoCmbBx.Items"));
            this.ProtoCmbBx.Items.Add(resources.GetString("ProtoCmbBx.Items1"));
            resources.ApplyResources(this.ProtoCmbBx, "ProtoCmbBx");
            this.ProtoCmbBx.Name = "ProtoCmbBx";
            this.ProtoCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnProtocolSelChanged);
            // 
            // HostNmTxtBx
            // 
            resources.ApplyResources(this.HostNmTxtBx, "HostNmTxtBx");
            this.HostNmTxtBx.Name = "HostNmTxtBx";
            // 
            // RemPathTxtBx
            // 
            resources.ApplyResources(this.RemPathTxtBx, "RemPathTxtBx");
            this.RemPathTxtBx.Name = "RemPathTxtBx";
            // 
            // PortNumTxtBx
            // 
            resources.ApplyResources(this.PortNumTxtBx, "PortNumTxtBx");
            this.PortNumTxtBx.Name = "PortNumTxtBx";
            this.PortNumTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPortTxtBxKeyPressed);
            // 
            // OKBttn
            // 
            this.OKBttn.DialogResult = System.Windows.Forms.DialogResult.OK;
            resources.ApplyResources(this.OKBttn, "OKBttn");
            this.OKBttn.Name = "OKBttn";
            // 
            // CancelBttn
            // 
            this.CancelBttn.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            resources.ApplyResources(this.CancelBttn, "CancelBttn");
            this.CancelBttn.Name = "CancelBttn";
            // 
            // UsrNameTxtBx
            // 
            resources.ApplyResources(this.UsrNameTxtBx, "UsrNameTxtBx");
            this.UsrNameTxtBx.Name = "UsrNameTxtBx";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // PasswdTxtBx
            // 
            resources.ApplyResources(this.PasswdTxtBx, "PasswdTxtBx");
            this.PasswdTxtBx.Name = "PasswdTxtBx";
            // 
            // URIComposeForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.PasswdTxtBx);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.UsrNameTxtBx);
            this.Controls.Add(this.CancelBttn);
            this.Controls.Add(this.OKBttn);
            this.Controls.Add(this.PortNumTxtBx);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.RemPathTxtBx);
            this.Controls.Add(this.HostNmTxtBx);
            this.Controls.Add(this.ProtoCmbBx);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "URIComposeForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox ProtoCmbBx;
        private System.Windows.Forms.TextBox HostNmTxtBx;
        private System.Windows.Forms.TextBox RemPathTxtBx;
        private System.Windows.Forms.TextBox PortNumTxtBx;
        private System.Windows.Forms.Button OKBttn;
        private System.Windows.Forms.Button CancelBttn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox UsrNameTxtBx;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox PasswdTxtBx;
    }
}