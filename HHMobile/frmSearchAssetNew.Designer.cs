namespace OnRamp
{
    partial class frmSearchAssetNew
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSearchAssetNew));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.lnkSubGroup = new System.Windows.Forms.LinkLabel();
            this.btnSubGroup = new System.Windows.Forms.Button();
            this.lnkGroups = new System.Windows.Forms.LinkLabel();
            this.btnGroups = new System.Windows.Forms.Button();
            this.lnkLocation = new System.Windows.Forms.LinkLabel();
            this.btnSelectLoc = new System.Windows.Forms.Button();
            this.cmbColumn3 = new System.Windows.Forms.ComboBox();
            this.cmbColumn2 = new System.Windows.Forms.ComboBox();
            this.cmbColumn1 = new System.Windows.Forms.ComboBox();
            this.btnSearchOptions = new System.Windows.Forms.Button();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtValue3 = new System.Windows.Forms.TextBox();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.pnlSearchOptions = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.rdbtnExactString = new System.Windows.Forms.RadioButton();
            this.rdbtnWholeString = new System.Windows.Forms.RadioButton();
            this.rdbtnAnyWordDigit = new System.Windows.Forms.RadioButton();
            this.btnentertagid = new System.Windows.Forms.Button();
            this.pnlSearch.SuspendLayout();
            this.pnlSearchOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.White;
            this.pnlSearch.Controls.Add(this.btnentertagid);
            this.pnlSearch.Controls.Add(this.lnkSubGroup);
            this.pnlSearch.Controls.Add(this.btnSubGroup);
            this.pnlSearch.Controls.Add(this.lnkGroups);
            this.pnlSearch.Controls.Add(this.btnGroups);
            this.pnlSearch.Controls.Add(this.lnkLocation);
            this.pnlSearch.Controls.Add(this.btnSelectLoc);
            this.pnlSearch.Controls.Add(this.cmbColumn3);
            this.pnlSearch.Controls.Add(this.cmbColumn2);
            this.pnlSearch.Controls.Add(this.cmbColumn1);
            this.pnlSearch.Controls.Add(this.btnSearchOptions);
            this.pnlSearch.Controls.Add(this.txtValue2);
            this.pnlSearch.Controls.Add(this.label4);
            this.pnlSearch.Controls.Add(this.label3);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.btnUpload);
            this.pnlSearch.Controls.Add(this.txtValue3);
            this.pnlSearch.Controls.Add(this.txtValue1);
            this.pnlSearch.Controls.Add(this.label2);
            this.pnlSearch.Controls.Add(this.cboSubGroup);
            this.pnlSearch.Controls.Add(this.cboGroup);
            this.pnlSearch.Controls.Add(this.cboLoc);
            resources.ApplyResources(this.pnlSearch, "pnlSearch");
            this.pnlSearch.Name = "pnlSearch";
            // 
            // lnkSubGroup
            // 
            this.lnkSubGroup.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkSubGroup, "lnkSubGroup");
            this.lnkSubGroup.Name = "lnkSubGroup";
            this.lnkSubGroup.Click += new System.EventHandler(this.lnkSubGroup_Click);
            // 
            // btnSubGroup
            // 
            resources.ApplyResources(this.btnSubGroup, "btnSubGroup");
            this.btnSubGroup.Name = "btnSubGroup";
            this.btnSubGroup.Click += new System.EventHandler(this.btnSubGroup_Click);
            // 
            // lnkGroups
            // 
            this.lnkGroups.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkGroups, "lnkGroups");
            this.lnkGroups.Name = "lnkGroups";
            this.lnkGroups.Click += new System.EventHandler(this.lnkGroups_Click);
            // 
            // btnGroups
            // 
            resources.ApplyResources(this.btnGroups, "btnGroups");
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // lnkLocation
            // 
            this.lnkLocation.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkLocation, "lnkLocation");
            this.lnkLocation.Name = "lnkLocation";
            this.lnkLocation.Click += new System.EventHandler(this.lnkLocation_Click);
            // 
            // btnSelectLoc
            // 
            resources.ApplyResources(this.btnSelectLoc, "btnSelectLoc");
            this.btnSelectLoc.Name = "btnSelectLoc";
            this.btnSelectLoc.Click += new System.EventHandler(this.btnSelectLoc_Click);
            // 
            // cmbColumn3
            // 
            resources.ApplyResources(this.cmbColumn3, "cmbColumn3");
            this.cmbColumn3.Name = "cmbColumn3";
            // 
            // cmbColumn2
            // 
            resources.ApplyResources(this.cmbColumn2, "cmbColumn2");
            this.cmbColumn2.Name = "cmbColumn2";
            // 
            // cmbColumn1
            // 
            resources.ApplyResources(this.cmbColumn1, "cmbColumn1");
            this.cmbColumn1.Name = "cmbColumn1";
            // 
            // btnSearchOptions
            // 
            resources.ApplyResources(this.btnSearchOptions, "btnSearchOptions");
            this.btnSearchOptions.Name = "btnSearchOptions";
            this.btnSearchOptions.Click += new System.EventHandler(this.btnSearchOptions_Click);
            // 
            // txtValue2
            // 
            resources.ApplyResources(this.txtValue2, "txtValue2");
            this.txtValue2.Name = "txtValue2";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnUpload
            // 
            resources.ApplyResources(this.btnUpload, "btnUpload");
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // txtValue3
            // 
            resources.ApplyResources(this.txtValue3, "txtValue3");
            this.txtValue3.Name = "txtValue3";
            // 
            // txtValue1
            // 
            resources.ApplyResources(this.txtValue1, "txtValue1");
            this.txtValue1.Name = "txtValue1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // cboSubGroup
            // 
            resources.ApplyResources(this.cboSubGroup, "cboSubGroup");
            this.cboSubGroup.Name = "cboSubGroup";
            // 
            // cboGroup
            // 
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // pnlSearchOptions
            // 
            this.pnlSearchOptions.BackColor = System.Drawing.SystemColors.Window;
            this.pnlSearchOptions.Controls.Add(this.btnOk);
            this.pnlSearchOptions.Controls.Add(this.rdbtnExactString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnWholeString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnAnyWordDigit);
            resources.ApplyResources(this.pnlSearchOptions, "pnlSearchOptions");
            this.pnlSearchOptions.Name = "pnlSearchOptions";
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdbtnExactString
            // 
            resources.ApplyResources(this.rdbtnExactString, "rdbtnExactString");
            this.rdbtnExactString.Name = "rdbtnExactString";
            this.rdbtnExactString.Click += new System.EventHandler(this.rdbtnExactString_Click);
            // 
            // rdbtnWholeString
            // 
            resources.ApplyResources(this.rdbtnWholeString, "rdbtnWholeString");
            this.rdbtnWholeString.Name = "rdbtnWholeString";
            this.rdbtnWholeString.Click += new System.EventHandler(this.rdbtnWholeString_Click);
            // 
            // rdbtnAnyWordDigit
            // 
            resources.ApplyResources(this.rdbtnAnyWordDigit, "rdbtnAnyWordDigit");
            this.rdbtnAnyWordDigit.Name = "rdbtnAnyWordDigit";
            this.rdbtnAnyWordDigit.Click += new System.EventHandler(this.rdbtnAnyWordDigit_Click);
            // 
            // btnentertagid
            // 
            resources.ApplyResources(this.btnentertagid, "btnentertagid");
            this.btnentertagid.Name = "btnentertagid";
            this.btnentertagid.Click += new System.EventHandler(this.btnentertagid_Click);
            // 
            // frmSearchAssetNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlSearchOptions);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchAssetNew";
            this.Load += new System.EventHandler(this.frmSearchAsset_Load);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmSearchAssetNew_KeyUp);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearchOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.frmSearchAssetNew));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.pnlSearch = new System.Windows.Forms.Panel();
            this.btnSearchOptions = new System.Windows.Forms.Button();
            this.txtValue2 = new System.Windows.Forms.TextBox();
            this.cboSubGroup = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cboGroup = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.btnUpload = new System.Windows.Forms.Button();
            this.txtValue3 = new System.Windows.Forms.TextBox();
            this.txtValue1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.uploadCSV = new System.Windows.Forms.OpenFileDialog();
            this.pnlSearchOptions = new System.Windows.Forms.Panel();
            this.btnOk = new System.Windows.Forms.Button();
            this.rdbtnExactString = new System.Windows.Forms.RadioButton();
            this.rdbtnWholeString = new System.Windows.Forms.RadioButton();
            this.rdbtnAnyWordDigit = new System.Windows.Forms.RadioButton();
            this.cmbColumn1 = new System.Windows.Forms.ComboBox();
            this.cmbColumn2 = new System.Windows.Forms.ComboBox();
            this.cmbColumn3 = new System.Windows.Forms.ComboBox();
            this.lnkSubGroup = new System.Windows.Forms.LinkLabel();
            this.btnSubGroup = new System.Windows.Forms.Button();
            this.lnkGroups = new System.Windows.Forms.LinkLabel();
            this.btnGroups = new System.Windows.Forms.Button();
            this.lnkLocation = new System.Windows.Forms.LinkLabel();
            this.btnSelectLoc = new System.Windows.Forms.Button();
            this.btnentertagid = new System.Windows.Forms.Button();
            this.pnlSearch.SuspendLayout();
            this.pnlSearchOptions.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlSearch
            // 
            this.pnlSearch.BackColor = System.Drawing.Color.White;
            this.pnlSearch.Controls.Add(this.lnkSubGroup);
            this.pnlSearch.Controls.Add(this.btnSubGroup);
            this.pnlSearch.Controls.Add(this.btnentertagid);
            this.pnlSearch.Controls.Add(this.lnkGroups);
            this.pnlSearch.Controls.Add(this.btnGroups);
            this.pnlSearch.Controls.Add(this.lnkLocation);
            this.pnlSearch.Controls.Add(this.btnSelectLoc);
            this.pnlSearch.Controls.Add(this.cmbColumn3);
            this.pnlSearch.Controls.Add(this.cmbColumn2);
            this.pnlSearch.Controls.Add(this.cmbColumn1);
            this.pnlSearch.Controls.Add(this.btnSearchOptions);
            this.pnlSearch.Controls.Add(this.txtValue2);
            this.pnlSearch.Controls.Add(this.cboSubGroup);
            this.pnlSearch.Controls.Add(this.label4);
            this.pnlSearch.Controls.Add(this.cboGroup);
            this.pnlSearch.Controls.Add(this.label3);
            this.pnlSearch.Controls.Add(this.cboLoc);
            this.pnlSearch.Controls.Add(this.btnSearch);
            this.pnlSearch.Controls.Add(this.btnUpload);
            this.pnlSearch.Controls.Add(this.txtValue3);
            this.pnlSearch.Controls.Add(this.txtValue1);
            this.pnlSearch.Controls.Add(this.label2);
            resources.ApplyResources(this.pnlSearch, "pnlSearch");
            this.pnlSearch.Name = "pnlSearch";
            // 
            // btnSearchOptions
            // 
            resources.ApplyResources(this.btnSearchOptions, "btnSearchOptions");
            this.btnSearchOptions.Name = "btnSearchOptions";
            this.btnSearchOptions.Click += new System.EventHandler(this.btnSearchOptions_Click);
            // 
            // txtValue2
            // 
            resources.ApplyResources(this.txtValue2, "txtValue2");
            this.txtValue2.Name = "txtValue2";
            // 
            // cboSubGroup
            // 
            resources.ApplyResources(this.cboSubGroup, "cboSubGroup");
            this.cboSubGroup.Name = "cboSubGroup";
            this.cboSubGroup.Visible = false;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // cboGroup
            // 
            resources.ApplyResources(this.cboGroup, "cboGroup");
            this.cboGroup.Name = "cboGroup";
            //this.cboGroup.SelectedIndexChanged += new System.EventHandler(this.cboGroup_SelectedIndexChanged);
            this.cboGroup.Visible = false;
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            this.cboLoc.Visible = false;
            // 
            // btnSearch
            // 
            resources.ApplyResources(this.btnSearch, "btnSearch");
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // btnUpload
            // 
            resources.ApplyResources(this.btnUpload, "btnUpload");
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Location = new System.Drawing.Point(115, 230);
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            //
            //btnentertagid
            //
            resources.ApplyResources(this.btnentertagid, "btnentertagid");
            this.btnentertagid.Name = "btnentertagid";
            this.btnentertagid.Text = "Enter TagID";
            this.btnentertagid.Top = 230;
            this.btnentertagid.Left = 3;
            this.btnentertagid.Size = new System.Drawing.Size(106, 25);
            this.btnentertagid.Click += new System.EventHandler(this.btnentertagid_Click);
            
            // 
            // txtValue3
            // 
            resources.ApplyResources(this.txtValue3, "txtValue3");
            this.txtValue3.Name = "txtValue3";
            // 
            // txtValue1
            // 
            resources.ApplyResources(this.txtValue1, "txtValue1");
            this.txtValue1.Name = "txtValue1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // uploadCSV
            // 
            this.uploadCSV.FileName = "openFileDialog1";
            // 
            // pnlSearchOptions
            // 
            this.pnlSearchOptions.BackColor = System.Drawing.SystemColors.Window;
            this.pnlSearchOptions.Controls.Add(this.btnOk);
            this.pnlSearchOptions.Controls.Add(this.rdbtnExactString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnWholeString);
            this.pnlSearchOptions.Controls.Add(this.rdbtnAnyWordDigit);
            resources.ApplyResources(this.pnlSearchOptions, "pnlSearchOptions");
            this.pnlSearchOptions.Name = "pnlSearchOptions";
            // 
            // btnOk
            // 
            resources.ApplyResources(this.btnOk, "btnOk");
            this.btnOk.Name = "btnOk";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // rdbtnExactString
            // 
            resources.ApplyResources(this.rdbtnExactString, "rdbtnExactString");
            this.rdbtnExactString.Name = "rdbtnExactString";
            this.rdbtnExactString.Click += new System.EventHandler(this.rdbtnExactString_Click);
            // 
            // rdbtnWholeString
            // 
            resources.ApplyResources(this.rdbtnWholeString, "rdbtnWholeString");
            this.rdbtnWholeString.Name = "rdbtnWholeString";
            this.rdbtnWholeString.Click += new System.EventHandler(this.rdbtnWholeString_Click);
            // 
            // rdbtnAnyWordDigit
            // 
            resources.ApplyResources(this.rdbtnAnyWordDigit, "rdbtnAnyWordDigit");
            this.rdbtnAnyWordDigit.Name = "rdbtnAnyWordDigit";
            this.rdbtnAnyWordDigit.Click += new System.EventHandler(this.rdbtnAnyWordDigit_Click);
            this.rdbtnAnyWordDigit.Text = "Contains any word";
            // 
            // lnkSubGroup
            // 
            this.lnkSubGroup.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkSubGroup, "lnkSubGroup");
            this.lnkSubGroup.Name = "lnkSubGroup";
            this.lnkSubGroup.Click += new System.EventHandler(this.lnkSubGroup_Click);
            this.lnkSubGroup.Location = new System.Drawing.Point(59, 166);
            this.lnkSubGroup.Size = new System.Drawing.Size(112, 15);
            this.lnkSubGroup.BringToFront();
            this.lnkSubGroup.Text = "All SubGroups";
            this.lnkSubGroup.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lnkSubGroup.ForeColor = System.Drawing.Color.Black;
            // 
            // btnSubGroup
            // 
            resources.ApplyResources(this.btnSubGroup, "btnSubGroup");
            this.btnSubGroup.Name = "btnSubGroup";
            this.btnSubGroup.Click += new System.EventHandler(this.btnSubGroup_Click);
            // 
            // lnkGroups
            // 
            this.lnkGroups.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkGroups, "lnkGroups");
            this.lnkGroups.Name = "lnkGroups";
            this.lnkGroups.Click += new System.EventHandler(this.lnkGroups_Click);
            // 
            // btnGroups
            // 
            resources.ApplyResources(this.btnGroups, "btnGroups");
            this.btnGroups.Name = "btnGroups";
            this.btnGroups.Click += new System.EventHandler(this.btnGroups_Click);
            // 
            // lnkLocation
            // 
            this.lnkLocation.ForeColor = System.Drawing.Color.Black;
            resources.ApplyResources(this.lnkLocation, "lnkLocation");
            this.lnkLocation.Name = "lnkLocation";
            this.lnkLocation.Click += new System.EventHandler(this.lnkLocation_Click);
            
            // 
            // btnSelectLoc
            // 
            resources.ApplyResources(this.btnSelectLoc, "btnSelectLoc");
            this.btnSelectLoc.Name = "btnSelectLoc";
            this.btnSelectLoc.Click += new System.EventHandler(this.btnSelectLoc_Click); 
            // 
            // cmbColumn1
            // 
            resources.ApplyResources(this.cmbColumn1, "cmbColumn1");
            this.cmbColumn1.Name = "cmbColumn1";
            // 
            // cmbColumn2
            // 
            resources.ApplyResources(this.cmbColumn2, "cmbColumn2");
            this.cmbColumn2.Name = "cmbColumn2";
            // 
            // cmbColumn3
            // 
            resources.ApplyResources(this.cmbColumn3, "cmbColumn3");
            this.cmbColumn3.Name = "cmbColumn3";
            // 
            // frmSearchAssetNew
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.pnlSearch);
            this.Controls.Add(this.pnlSearchOptions);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSearchAssetNew";
            this.Load += new System.EventHandler(this.frmSearchAsset_Load);
            this.pnlSearch.ResumeLayout(false);
            this.pnlSearchOptions.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlSearch;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnUpload;
        private System.Windows.Forms.TextBox txtValue3;
        private System.Windows.Forms.TextBox txtValue1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboGroup;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cboSubGroup;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.OpenFileDialog uploadCSV;
        private System.Windows.Forms.TextBox txtValue2;
        private System.Windows.Forms.Button btnSearchOptions;
        private System.Windows.Forms.Panel pnlSearchOptions;
        private System.Windows.Forms.RadioButton rdbtnAnyWordDigit;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.RadioButton rdbtnExactString;
        private System.Windows.Forms.RadioButton rdbtnWholeString;
        private System.Windows.Forms.ComboBox cmbColumn3;
        private System.Windows.Forms.ComboBox cmbColumn2;
        private System.Windows.Forms.ComboBox cmbColumn1;
        private System.Windows.Forms.LinkLabel lnkSubGroup;
        private System.Windows.Forms.Button btnSubGroup;
        private System.Windows.Forms.LinkLabel lnkGroups;
        private System.Windows.Forms.Button btnGroups;
        private System.Windows.Forms.LinkLabel lnkLocation;
        private System.Windows.Forms.Button btnSelectLoc;
        private System.Windows.Forms.Button btnentertagid;
    }
}