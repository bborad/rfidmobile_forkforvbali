using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Windows.Forms;
using System.Drawing;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    class TempMonitor
    {

        private UInt16 ambThrsh;
        private UInt16 xcvrThrsh;
        private UInt16 paThrsh;
        private UInt16 deltaThrsh;

        private UInt16 ambLatest;
        private UInt16 xcvrLatest;
        private UInt16 paLatest;

        private UInt16 xcvrTempHi = UInt16.MinValue;
        
        public void ResetXcvrTempHi()
        {
            xcvrTempHi = xcvrLatest;
        }

        public void ResetPATempHi()
        {
            paTempHi = paLatest;
        }

        public UInt16 XcvrTempHi
        {
            get
            {
                return xcvrTempHi;
            }
        }

        private UInt16 paTempHi = UInt16.MinValue;
        public UInt16 PATempHi
        {
            get
            {
                return paTempHi;
            }
        }
       
        public UInt16 LastRecXcvrTemp
        {
            get
            {
                return xcvrLatest;
            }
        }

        public UInt16 LastRecPATemp
        {
            get
            {
                return paLatest;
            }
        }

        public UInt16 LastRecAmbTemp
        {
            get
            {
                return ambLatest;
            }
        }

        public UInt16 XcvrThrshTemp
        {
            get
            {
                return xcvrThrsh;
            }
        }

        private bool TempOvrLimit(UInt16 temp, UInt16 thrsh)
        {
            int DiffFromThrsh = temp - thrsh;
            if ((DiffFromThrsh < 0) && (Math.Abs(DiffFromThrsh) < 2))
                return true; // assume
            return (DiffFromThrsh >= 0);
        }

        public bool OverHeat
        {
            get
            {
                return TempOvrLimit(xcvrLatest, xcvrThrsh) || TempOvrLimit(paLatest, paThrsh);
            }
        }

        private void ThrshTempGetNotify(UInt16 amb, UInt16 xcvr, UInt16 pa, UInt16 delta)
        {
            ambThrsh = amb;
            xcvrThrsh = xcvr;
            paThrsh = pa;
            deltaThrsh = delta;
        }

        private bool LoadThrshTemp()
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            //Rdr.ThrshTempGetNotification += ThrshTempGetNotify;
            Rdr.RegisterThrshTempGetNotificationEvent(ThrshTempGetNotify);
            if (!Rdr.GetTempThreshold())
                return false;
            return true;
        }

        private void ThrshTempChanged(object sender, EventArgs e)
        {
            if (!LoadThrshTemp())
                throw new ApplicationException("Unable to load Temperature Thresholds");
        }

        private void SensorTempChanged(object sender, TempUpdateEventArgs e)
        {
            ambLatest = e.Amb;
            xcvrLatest = e.Xcvr;
            paLatest = e.PA;

            // Record Hi-Temp if necessary
            bool XcvrHiTempUpdated = false;
            if ((XcvrHiTempUpdated = (xcvrLatest > xcvrTempHi)) == true)
            {
                xcvrTempHi = xcvrLatest;
            }
            bool PAHiTempUpdated = false;
            if ((PAHiTempUpdated = (paLatest > paTempHi)) == true)
            {
                paTempHi = paLatest;
            }
            // Log to file
            if (XcvrHiTempUpdated)
            {
                Datalog.LogStr(
                    ": TempMonitor record new high transciever temp at " + XcvrTempHi
                    + " (Amb: " + ambLatest + "; PA: " + paLatest + ")");
            }
            if (PAHiTempUpdated)
            {
                Datalog.LogStr(
                    ": TempMonitor record new high of power-amp temp at " + PATempHi
                    + " (Amb: " + ambLatest + "; Xcvr: " + xcvrLatest + ")");
            }
        }

        static TempMonitor theMon = null;
        public static TempMonitor GetInstance()
        {
            if (theMon == null)
                theMon = new TempMonitor();
            return theMon;
        }

        private TempMonitor()
        {
            // Get Current Threshold
            if (!LoadThrshTemp())
                throw new ApplicationException("Unable to loadTemperature Thresholds");

            // Set up threshold change setting change listener
            DevStatForm.OvrHtProtTabChanged += ThrshTempChanged;

            // Setup Up-to-date Temp Listening
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
           // Rdr.TempUpdateEvt += SensorTempChanged;
            Rdr.RegisterTempUpdateEvt(SensorTempChanged);

        }

        #region Control Flasher (Overheat warning)
        Timer FlashTmr = null;
        Color[] OrigBackColors = null;
        Control[] CtrlsToFlash = null;

        /// <summary>
        /// Only supports one group of controls at a time
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="ctrls"></param>
        /// <returns>false if a list of controls already set up</returns>
        public bool OverHeatFlashAlertStart(params Control[] ctrls)
        {
            bool Busy = false;
            bool InvalidArgs = false;

            if (CtrlsToFlash != null)
            {
                Busy = true; // already in use
            }
            else
            {
                Busy = false;
                if (ctrls.Length > 0)
                {
                    CtrlsToFlash = (Control[])ctrls.Clone();
                    OrigBackColors = new Color[ctrls.Length];
                    for (int i = 0; i < CtrlsToFlash.Length; i++)
                        OrigBackColors[i] = CtrlsToFlash[i].BackColor;
                    FlashTmrStart();
                }
                else
                    InvalidArgs = true;
            }
            return ((!Busy) && (!InvalidArgs));
        }

        // Return false if no control was setup to flash overheat warning
        public bool OverHeatFlashAlertStop()
        {
            if (CtrlsToFlash != null)
            {
                // Restore Back Color
                for (int i = 0; i < CtrlsToFlash.Length; i++)
                    CtrlsToFlash[i].BackColor = OrigBackColors[i];
            }

            // Ready for next time
            CtrlsToFlash = null;
            OrigBackColors = null;

            // Stop Timer
            FlashTmrStop();

            return true;
        }

        private void FlashTmrStart()
        {
            if (FlashTmr == null)
            {
                FlashTmr = new Timer();
                FlashTmr.Interval = 500;
                FlashTmr.Tick += OnFlashTmrTick;
            }
            FlashTmr.Enabled = true;
        }

        private void FlashTmrStop()
        {
            if (FlashTmr != null)
                FlashTmr.Enabled = false;
        }

        private void OnFlashTmrTick(object sender, EventArgs e)
        {
            bool FillOrigColor = false;

            // use this condition as cue to whether flash is on or off
            FillOrigColor = (CtrlsToFlash[0].BackColor != OrigBackColors[0]);
            for (int i = 0; i < CtrlsToFlash.Length; i++)
                CtrlsToFlash[i].BackColor = FillOrigColor ? OrigBackColors[i] : Color.Red;
        }
        #endregion

        #region Periodic Temp Get
        private Timer tempGetTmr = null;

        // return false if already running
        public bool PeriodicTempGetStart(int secs)
        {
            bool Busy = false;

            if (tempGetTmr == null)
            {
                TempGetTmrInit();
            }
            Busy = (tempGetTmr.Enabled == true);
            if (! Busy)
            {
                tempGetTmr.Interval = secs * 1000;
                tempGetTmr.Enabled = true;
            }
            return (Busy == false);
        }

        public void PeriodicTempGetStop()
        {
            if (tempGetTmr != null)
            {
                tempGetTmr.Enabled = false;
            }
        }

        private void TempGetTmrInit()
        {
            tempGetTmr = new Timer();
            tempGetTmr.Tick += OnTempGetTmrTick;
            tempGetTmr.Enabled = false;
        }

        private void OnTempGetTmrTick(Object sender, EventArgs e)
        {
            // Although getting temperature here, still relies on
            // the RFIDRdr.TempUpdateEvt to receive temp read.
            // This simplifies the record keeping flow (passive versus active
            // monitoring)
           // ushort amb, xcvr, pamp;
           // Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.GetCurrTemp();
           // RFIDRdr.GetInstance().GetCurrTemp(out amb, out xcvr, out pamp);
        }

        #endregion
    }
}
