using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class BankPermDlg : Form
    {
        private bool selectBnkPerm = false;
        private bool selectPwdPerm = false;

        public RFID_18K6C_TAG_MEM_PERM MemPerm
        {
            get
            {
                RFID_18K6C_TAG_MEM_PERM chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE;
                if (selectBnkPerm)
                {
                    if (Bnk0Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_PERM_WRITEABLE;
                    else if (Bnk1Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_WRITEABLE;
                    else if (Bnk2Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_SECURED_WRITEABLE;
                    else if (Bnk3Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_NOT_WRITEABLE;
                    else if (Bnk4Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE;
                }
                return chosenPerm;
            }
        }

        public RFID_18K6C_TAG_PWD_PERM PwdPerm
        {
            get
            {
                RFID_18K6C_TAG_PWD_PERM chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE;
                if (selectPwdPerm)
                {
                    if (Pwd0Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_ACCESSIBLE;
                    else if (Pwd1Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_ACCESSIBLE;
                    else if (Pwd2Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_SECURED_ACCESSIBLE;
                    else if (Pwd3Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_NOT_ACCESSIBLE;
                    else if (Pwd4Bttn.Checked)
                        chosenPerm = RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE;
                }
                return chosenPerm;
            }
        }

        public BankPermDlg(RFID_18K6C_TAG_PWD_PERM pwdPerm)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            SetTitleBarString("Password");
            PasswdPanel.Location = new Point(1, 1);
            PasswdPanel.Visible = true;

            switch (pwdPerm)
            {
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_ACCESSIBLE:
                    Pwd0Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_ACCESSIBLE:
                    Pwd1Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_SECURED_ACCESSIBLE:
                    Pwd2Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_ALWAYS_NOT_ACCESSIBLE:
                    Pwd3Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_PWD_PERM.RFID_18K6C_TAG_PWD_PERM_NO_CHANGE:
                    Pwd4Bttn.Checked = true;
                    break;
            }
            selectPwdPerm = true;
        }

        public BankPermDlg(RFID_18K6C_TAG_MEM_PERM memPerm)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            SetTitleBarString("Memory Banks");
            MemBnkPanel.Location = new Point(1, 1);
            MemBnkPanel.Visible = true;

            switch (memPerm)
            {
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_PERM_WRITEABLE:
                    Bnk0Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_WRITEABLE:
                    Bnk1Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_SECURED_WRITEABLE:
                    Bnk2Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_ALWAYS_NOT_WRITEABLE:
                    Bnk3Bttn.Checked = true;
                    break;
                case RFID_18K6C_TAG_MEM_PERM.RFID_18K6C_TAG_MEM_NO_CHANGE:
                    Bnk4Bttn.Checked = true;
                    break;
            }
            selectBnkPerm = true;
        }

        #region Form Routines
        private void SetTitleBarString(String ItemName)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.BankPermDlg));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(BankPermDlg));
            }

            // Replace <Type> in Title bar with 'ItemName'
            String DefTitle = resources.GetString(@"$this.Text");
            StringBuilder TitleBldr = new StringBuilder(DefTitle);
            TitleBldr = TitleBldr.Replace("<Type>", ItemName);
            this.Text = TitleBldr.ToString();
        }
        #endregion

    }
}