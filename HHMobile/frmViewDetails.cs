/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Dispatch functionality (old)
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Data;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class frmViewDetails : Form
    {
        private String CheckedlocationTag = "";
        private String CheckedTag = "";
        private String InTagList = "";
        private Assets SelectedAsset;

        float LocationRSSI;

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        private TagAccListVHelper ListVHelper;
        private TagAccErrSummary errSummary;

        public frmViewDetails()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            InitScanButtonState();

            InitRead1ButtonState();

            ListVHelper = new TagAccListVHelper(this.EPCListV);

            errSummary = new TagAccErrSummary();
        }

        private Int32 _selectedDocumentID;

        public Int32 SelectedDocumentID
        {
            get { return _selectedDocumentID; }
            set { _selectedDocumentID = value; }
        }
        
        #region AddBarCOdeTag
        public void AddListViewItems(ListView.ListViewItemCollection lAr)
        {
            try
            {
                ListViewItem lstItem;
                ListViewItem.ListViewSubItem ls;
                Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);

                Int32 iTag = 0;
                String TgNo;
                Assets Ast;

                foreach (ListViewItem li in lAr)
                {
                    iCount++;
                    iTag++;
                    lstItem = new ListViewItem(iCount.ToString());

                    TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                    Ast = new Assets(TgNo);
                    if (Ast.ServerKey != 0)
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.Name.ToString());
                        lstItem.SubItems.Add(ls);
                    }
                    else
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);
                    }

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString(TgNo);
                    lstItem.SubItems.Add(ls);

                    lstAsset.Items.Add(lstItem);
                }
                lstAsset.Refresh();

                lstAsset.Visible = true;
                EPCListV.Visible = false;
                ScanButton.Enabled = true;
                //btnScnBarcd.Enabled = true;
                button1.Enabled = true;
                btnClear.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
            }
            catch (System.Data.SqlServerCe.SqlCeException)
            {
                MessageBox.Show("Data File is not able to access.");
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Web exception occured.");
            }
            catch (Exception ep)
            {

                MessageBox.Show(ep.Message.ToString());
            }
        }
        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        #region Action State variables
        private int fldRdCnt = 0; // TBD: find a better method to store/manage this.
        #endregion

        #region Scan Button Routines
        private void InitScanButtonState()
        {
            OpState State = OpState.Stopped;
            this.ScanButton.Tag = State; // boxing
        }

        private OpState ScanState()
        {
            OpState state = OpState.Stopped;
            if (ScanButton.Tag is OpState)
            {
                state = (OpState)ScanButton.Tag;
            }
            else
            {
                throw new ApplicationException("ScanButton Tag is not OpState");
            }

            return state;
        }

        private void SetScanState(OpState newState)
        {
            OpState CurState = ScanState();
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagRdForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            }

            switch (newState)
            {
                case OpState.Starting:
                    // disable scan button
                    ScanButton.Enabled = false;
                    // btnScnBarcd.Enabled = false;
                    // disable read1 button
                    Read1Button.Enabled = false;

                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    ScanButton.Text = "Stop";
                    ScanButton.Enabled = true;
                    // btnScnBarcd.Enabled = true;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    ScanButton.Enabled = false;
                    // btnScnBarcd.Enabled = false;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ScanButton.Text = resources.GetString("ScanButton.Text");
                    // btnScnBarcd.Enabled = true;
                    ScanButton.Enabled = true;
                    Read1Button.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            ScanButton.Tag = newState;
        }


        private void OnScanButtonClicked(object sender, EventArgs e)
        {

            try
            {
                lstAsset.Visible = false;
                EPCListV.Visible = true;

                
                CheckedlocationTag = "";
                InTagList = "";
                CheckedTag = "";
                LocationRSSI = 10000;
                // Scan or Stop
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                switch (ScanState())
                {
                    case OpState.Stopped:
                        //Rdr.InvtryOpStEvent += InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                        Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                        SetScanState(OpState.Starting);
                        Program.RefreshStatusLabel(this, "Starting..."); 
                    //Count Status Message
                        if (!Rdr.SetRepeatedTagObsrvMode(true))
                            // Continue despite error
                            Program.ShowError("Disable Repeated Tag Observation mode failed");
                        //UserPref Pref = UserPref.GetInstance();
                        //Byte[] Mask; uint MaskOffset;
                        //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                        bool Succ = Rdr.TagInventoryStart(5);
                        if (!Succ)
                        {
                            SetScanState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            Program.ShowError("Error: Inventory Failed to Start\n");
                            Application.DoEvents();
                            button1.Enabled = true;
                            btnClear.Enabled = true;

                        }
                        button1.Enabled = false;
                        btnClear.Enabled = false;
                        btnScnBarcd.Enabled = false;
                        break;
                    case OpState.Started:
                        // Stop Tag Read
                        SetScanState(OpState.Stopping);
                        Program.RefreshStatusLabel(this, "Stopping...");
                        if (!Rdr.TagInventoryStop())
                        {
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            SetScanState(OpState.Stopped); // give up
                            Program.RefreshStatusLabel(this, "Error Stopped...");
                            Program.ShowError("Error: TagRead Failed to Stop\n");
                            button1.Enabled = false;
                            btnClear.Enabled = false;

                        }
                        else
                        {
                            ScanButton.Enabled = false;
                            //btnScnBarcd.Enabled = false;

                            //lstAsset.Items.Clear();
                            ListViewItem lstItem;
                            ListViewItem.ListViewSubItem ls;
                            Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);

                            Int32 iTag;
                            String TgNo;
                            Assets Ast;
                            for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                            {
                                iCount++;
                                lstItem = new ListViewItem(iCount.ToString());

                                //ls = new ListViewItem.ListViewSubItem();
                                //ls.Text = Convert.ToString(iCount.ToString());
                                //lstItem.SubItems.Add(ls);

                                TgNo = EPCListV.Items[iTag].SubItems[1].Text;
                                Ast = new Assets(TgNo);
                                if (Ast.ServerKey != 0)
                                {
                                    ls = new ListViewItem.ListViewSubItem();
                                    ls.Text = Convert.ToString(Ast.Name.ToString());
                                    lstItem.SubItems.Add(ls);
                                }
                                else
                                {
                                    ls = new ListViewItem.ListViewSubItem();
                                    ls.Text = Convert.ToString("NA");
                                    lstItem.SubItems.Add(ls);
                                }

                                ls = new ListViewItem.ListViewSubItem();
                                ls.Text = Convert.ToString(TgNo);
                                lstItem.SubItems.Add(ls);

                                lstAsset.Items.Add(lstItem);
                            }
                            lstAsset.Refresh();
                            lstAsset.Visible = true;

                            EPCListV.Visible = false;
                            ScanButton.Enabled = true;
                            // btnScnBarcd.Enabled = true;
                            button1.Enabled = true;
                            btnClear.Enabled = true;

                        }
                        btnScnBarcd.Enabled = true;
                        break;
                    case OpState.Starting:
                        // Stop Tag Read
                        SetScanState(OpState.Stopping);
                        Program.RefreshStatusLabel(this, "Stopping...");
                        if (!Rdr.TagInventoryStop())
                        {
                            // Restore
                            SetScanState(OpState.Starting);
                            Program.RefreshStatusLabel(this, "Starting...");
                            Program.ShowWarning("Inventory Stop failed during Starting phase, please try again");
                        }
                        btnScnBarcd.Enabled = true;
                        break;
                    default:
                        // ignore
                        break;
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
            }
            catch (System.Data.SqlServerCe.SqlCeException)
            {
                MessageBox.Show("Data File is not able to access.");
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Web exception occured.");
            }
            catch (Exception ep)
            {

                MessageBox.Show(ep.Message.ToString());
            }
        }
        #endregion

        #region Read1 Button Routines
        private void InitRead1ButtonState()
        {
            OpState State = OpState.Stopped;
            this.Read1Button.Tag = State; // boxing
        }

        private OpState Read1State()
        {
            OpState state = OpState.Stopped;
            if (Read1Button.Tag is OpState)
            {
                state = (OpState)Read1Button.Tag;
            }
            else
            {
                throw new ApplicationException("Read1Button Tag is not OpState");
            }

            return state;
        }

        private void SetRead1State(OpState newState)
        {
            OpState CurState = Read1State();
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagRdForm));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            }

            switch (newState)
            {
                case OpState.Starting:
                    // disable read1 button
                    Read1Button.Enabled = false;
                    // disable scan button
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    Read1Button.Text = "Stop";
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    Read1Button.Enabled = false;
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    Read1Button.Text = resources.GetString("Read1Button.Text");
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = true;
                    // btnScnBarcd.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            Read1Button.Tag = newState;
        }

        private bool GetCurrRowUnreadBnk(out MemoryBanks4Op bnk2Rd, out UINT96_T epc)
        {
            bool Succ = false;
            epc = new UINT96_T();
            bnk2Rd = MemoryBanks4Op.None;
            MemoryBanks4Op UnreadBnks = MemoryBanks4Op.None;
            if ((EPCListV.Items.Count > 0) && EPCListV.SelectedIndices.Count > 0)
            {
                Succ = true;
                ListViewItem SelectedRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
                ListVHelper.GetEPC(SelectedRow, out epc);
                UnreadBnks = RowGetEmptyBanks(SelectedRow)
                    & EPCTag.AvailBanks ;
                bnk2Rd = EPCTag.CurBankToAcc(UnreadBnks);
            }
            else
                Succ = false;

            return Succ;
        }

        private void OnRead1ButtonClicked(object sender, EventArgs e)
        {
            // Read1 or Stop
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (Read1State())
            {
                case OpState.Stopped:
                    // Target EPC to Read (currently selected) 
                    // TBD: strongest RSSI (sorted list?)
                    UINT96_T TgtEPC = new UINT96_T();
                    MemoryBanks4Op NxtBnk2Rd = MemoryBanks4Op.None;
                    if (GetCurrRowUnreadBnk(out NxtBnk2Rd, out TgtEPC))
                    {
                        if (NxtBnk2Rd == MemoryBanks4Op.None)
                        {
                            MessageBox.Show("No Empty Banks to Read");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select a tag from the list first");
                        return;
                    }
                    // Prompt for password if required
                    UInt32 CurAccPasswd = 0;
                    if (PwdReqChkBx.Visible == true && PwdReqChkBx.Checked == true)
                    {
                        if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
                        {
                            // user cancel
                            return;
                        }
                    }
                    //Rdr.RdOpStEvent += new EventHandler<RdOpEventArgs>(TgtRdOpStEventHandler);
                    //Rdr.MemBnkRdEvent += MemBnkRdEvtHandler;
                    Rdr.RegisterRdOpStEvent(TgtRdOpStEventHandler);
                    Rdr.RegisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    SetRead1State(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                    fldRdCnt = 0;
                    BnkRdOffset = 0;
                    errSummary.Clear();
                    UserPref Pref = UserPref.GetInstance();
                    Rdr.TagtEPC = TgtEPC;
                    //Rdr.TagReadBankData += TgtBnkReqCb;
                    Rdr.RegisterTagReadBankDataEvent(TgtBnkReqCb);
                    Rdr.CurAccPasswd = CurAccPasswd;
                    if (!Rdr.TagReadBanksStart())
                    {
                        SetRead1State(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        MessageBox.Show("Error: TagRead Failed to Start\n");
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    }
                    break;
                case OpState.Started:
                    // Stop Tag Read
                    SetRead1State(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    
                    if (!Rdr.TagReadStop())
                    {
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                        MessageBox.Show("Error: TagRead Failed to Stop\n");
                        SetRead1State(OpState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped...");
                        
                    }
                    break;
                default:
                    // ignore
                    break;
            }
        }


        #endregion

        #region EPCListV Routines

        //
        // Helper function for RowGetEmptyBanks
        //
        private void CheckAndMarkEmptyBank(String[] memBnks, int bIdx,
            MemoryBanks4Op bnkToChk, ref MemoryBanks4Op markBnks)
        {
            if (memBnks[bIdx] == null || memBnks[bIdx].Length == 0)
                markBnks |= bnkToChk;
        }

        private MemoryBanks4Op RowGetEmptyBanks(ListViewItem item)
        {
            MemoryBanks4Op EmptyBanks = MemoryBanks4Op.None;
            String[] MemBanks = ListVHelper.GetBnkData(item);
            CheckAndMarkEmptyBank(MemBanks, 0, MemoryBanks4Op.Zero,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 1, MemoryBanks4Op.One,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 2, MemoryBanks4Op.Two,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 3, MemoryBanks4Op.Three,
                ref EmptyBanks);
            return EmptyBanks;
        }

        private void OnEPCListVSelChanged(object sender, EventArgs e)
        {
            //To be Delete
        }

        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text = EPCListV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
        }
        #endregion

        #region ScanRead Event Handlers
        private void ScanRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetScanState(OpState.Started);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    errSummary.Rec(e.TagAccErr);
                    break;
                case RdOpStatus.errorStopped:
                case RdOpStatus.stopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= ScanRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(ScanRdOpStEventHandler);
                    SetScanState(OpState.Stopped);
                    if (fldRdCnt > 0)
                    {
                        if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
                        {
                            EPCListV.Items[0].Selected = true; // select and focus on the first one
                            EPCListV.Focus();
                        }
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " new Tags");
                    }
                    else
                        Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        private void MemBnkRdEvtHandler(object sender, MemBnkRdEventArgs e)
        {
            bool FldAdded;
            // Debug: Add/Update Tag to EPCListV 
            // Save/Update Assciated Data with Row
            ListViewItem Item = ListVHelper.AddEPC(e.CRC, e.PC, e.EPC);
            RefreshTagCntLabel();
            // associate bank data with row
            if (e.BankNum == MemoryBanks4Op.One)
                FldAdded = false; // ignore this bank
            else
            {
                if (BnkRdBuf == null)
                    BnkRdBuf = new StringBuilder();
                if (BnkRdOffset == 0 && BnkRdBuf.Length > 0)
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                BnkRdBuf = BnkRdBuf.Append(e.Data);
                BnkRdOffset += e.Data.Length / 4; // 4 hex chars per Word
                // if full-bank is read, store to list and reset the buffer
                if ((BnkRdBuf.Length / 4) == EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(e.BankNum)))
                {
                    FldAdded = ListVHelper.StoreData(Item, e.BankNum, BnkRdBuf.ToString());
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                    BnkRdOffset = 0;
                }
                else
                {
                    FldAdded = false;
                }
            }
            if (FldAdded)
            {
                fldRdCnt++;
                Program.RefreshStatusLabel(this, "Fields Added : " + fldRdCnt);
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }

            
        }

        private bool Bnk2ReqCb(out MemoryBanks4Op bnks2Rd)
        {
            bnks2Rd = MemoryBanks4Op.Two;
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (ScanState() == OpState.Stopping || ScanState() == OpState.Stopped)
                return false;
            // Should we continue if invalid address error / too many errors occurs?
            return true; // until user stops
        }
        #endregion

        #region Tag Inventory Event Handler

        private TagRangeList tagList = new TagRangeList();

        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            //The Event where Scanned Tags are coming and can form some operation on it.
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                if (InTagList.IndexOf(e.EPC.ToString()) <= 0)
                {
                    InTagList += "," + e.EPC.ToString();
                    if (CheckedlocationTag.IndexOf(e.EPC.ToString()) <= 0)
                    {
                        //Check e.EPC here, if it is location tag, set the location combo, if it is not set, eles skip the tag.
                        TagInfo T = new TagInfo(e.EPC.ToString());
                        if (CheckedTag.IndexOf(e.EPC.ToString()) <= 0 && T.isDocumentTag())  //If Tag is assosiated with any location we will not add it in List View.
                        {
                            CheckedlocationTag += "," + e.EPC.ToString();
                            if (LocationRSSI > e.RSSI)
                            {
                                LocationRSSI = e.RSSI;
                                if (Convert.ToInt32(cboLoc.SelectedValue) == 0)
                                    cboLoc.SelectedValue = T.DataKey;
                            }
                            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                        }
                        else
                        {
                            //string loc = "";
                            //string nm = "";
                            if (T.isEmployeeTag())
                            {
                                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                            }
                            else
                            {
                                if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                                {
                                    // New Tag (Note: missing CRC data)
                                    CheckedTag += "," + e.EPC.ToString();
                                    ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);
                                    //ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC,nm,loc);
                                    RefreshTagCntLabel();

                                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                                }
                            }
                        }
                    }
                    else
                    {
                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    }
                }
                else
                {
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                }
            }
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState State = ScanState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    SetScanState(OpState.Started);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        // Display error or Restart Radio (if necessary)
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            Program.ShowError("Inventory Stopped with Error: " + e.msg);
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        //ushort MacErrCode;
                        if (Rdr.GetMacError() && Reader.macerr != 0)
                        {
                            // if fatal mac error, display message
                            if (Rdr.MacErrorIsFatal() || (ScanState() != OpState.Stopping))
                                Program.ShowError(e.msg);
                        }
                        else
                            Program.ShowError("Unknown HW error. Abort");
                    }
                    // restore StartButton
                    SetScanState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Finished");
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (State)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Inventory Start Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Inventory Stop Error");
                            //Rdr.InvtryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetScanState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }
            // Re-enable Field Input Textboxes (disabled during inventory operation)
            if (ScanState() == OpState.Stopped)
            {
                if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
                {
                    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                    // Enable (Avail) Text Fields
                    
                }
            }
        }
        #endregion

        #region TgtRead Event Handlers
        private void TgtRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetRead1State(OpState.Started);
                    UINT96_T EPC;
                    MemoryBanks4Op CurEmptyBnk;
                    GetCurrRowUnreadBnk(out CurEmptyBnk, out EPC);
                    Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(CurEmptyBnk) + "." + BnkRdOffset);
                    break;
                case RdOpStatus.completed:
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    // Add error to summary
                    errSummary.Rec(e.TagAccErr);
                    // display error immediately upon Unrecoverable Errors
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.InvalidAddr:
                            MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                "Tag Bank Read Failed");
                            break;
                    }
                    break;
                case RdOpStatus.stopped:
                case RdOpStatus.errorStopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                    SetRead1State(OpState.Stopped);
                    if (fldRdCnt > 0)
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " Banks");
                    else
                        Program.RefreshStatusLabel(this, "Stopped:  " + fldRdCnt + " Banks");
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        StringBuilder BnkRdBuf = null;
        int BnkRdOffset = 0;

        private ushort OptNumWdsToRd(MemoryBanks4Op bnk2Rd, int wdOffset)
        {
            ushort MaxNumWds = EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk2Rd));
            return (ushort)Math.Min(4, MaxNumWds - wdOffset);
        }

        private bool TgtBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        {
            bnk2Rd = MemoryBanks4Op.None;
            wdOffset = 0;
            wdCnt = 0;

            // if unrecoverable error from last read (such as password), stop
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (Read1State() == OpState.Stopping || Read1State() == OpState.Stopped)
                return false;
            UINT96_T TgtEPC;
            if (GetCurrRowUnreadBnk(out bnk2Rd, out TgtEPC) == false)
                return false;
            bool MoreBnks2Rd = (bnk2Rd != MemoryBanks4Op.None);
            if (MoreBnks2Rd)
            {
                wdOffset = (ushort)BnkRdOffset;
                wdCnt = OptNumWdsToRd(bnk2Rd, wdOffset);
                Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(bnk2Rd)
                    + "." + wdOffset);
            }
            return MoreBnks2Rd;
        }
        #endregion

        private bool AnyOperationRunning()
        {
            return ((ScanState() != OpState.Stopped)
                         || (Read1State() != OpState.Stopped));
        }

        private void OnTagRdFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            if (AnyOperationRunning())
            {
                MessageBox.Show("Please stop the scan operation first");
                e.Cancel = true;
            }

            // TBD: Ask for confirmation here
        }

        private void OnTagRdFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnTagRdFormLoad(object sender, EventArgs e)
        {
            // Set up to receive HotKey event
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                this.KeyPreview = true;
                this.KeyDown += On_KeyDown;
                this.KeyUp += On_KeyUp;
            }
            else
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            }

            //Set Document Combo Deepanshu
            DataTable dtList = new DataTable();
            dtList = Documents.getRows();

            DataRow dr = dtList.NewRow();
            dr["DocumentID"] = 0;
            dr["DocumentNo"] = "Select Document";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "DocumentID";
            cboLoc.DisplayMember = "DocumentNo";
            cboLoc.DataSource = dtList;

            cboLoc.SelectedValue = _selectedDocumentID;

            if (Login.OnLineMode)
                button1.Text = "Show All";

            // Scan Rd
            //this.OnScanButtonClicked(this, null);
        }

        #region F11/F4/F5 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (!this.Enabled)
            {
                return;
            }

            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                 case eVKey.VK_F19:
                    HardwareSelection selHW = UserPref.GetInstance().SelectedHardware;
                    if ((selHW == HardwareSelection.AT870Reader && keyCode == eVKey.VK_F19) || (selHW == HardwareSelection.CS101Reader && keyCode == eVKey.VK_F11))
                    {
                        if (down)
                        {
                            // fake 'Start' key press if not already running
                            if ((!F11Depressed) && (!AnyOperationRunning()))
                            {
                                OnScanButtonClicked(this, null);
                            }
                            F11Depressed = true;
                        }
                        else // up
                        {
                            // Stop running Scan-Rd Op
                            if (ScanState() == OpState.Starting
                                || ScanState() == OpState.Started)
                            {
                                OnScanButtonClicked(this, null);
                            }
                            F11Depressed = false;
                        }
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }

        private void On_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
               // case Keys.F7:
              //  case Keys.F8:
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, true);
                    break;
                default:
                    break;
            }
        }

        private void On_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:
                    if (AnyOperationRunning())
                    {
                        MessageBox.Show("Please stop any Tag Operation first", "Denied");
                        
                    }
                    else
                    {
                        Program.On_KeyUp(sender, e);
                    }
                    break;
                case Keys.F19:
                    HotKeyEvtHandler(eVKey.VK_F19, false);
                    break;
                default:
                    break;
            }
        }


        #endregion

        
        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void cboLoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        #region "This is Verify button."
        private void button1_Click(object sender, EventArgs e)
        {
            //Change Here 
            try
            {
                if (Convert.ToInt32(cboLoc.SelectedValue) == 0)
                {
                    MessageBox.Show("Please select the document first.");
                    return;
                }
                Int32 iTag;
                String TgNo;
                DataTable dtOnLine = new DataTable("dtOnLine");
                
                dtOnLine.Columns.Add("AssetTagID", typeof(string));
                dtOnLine.Columns.Add("ServerKey", typeof(Int32));
                dtOnLine.AcceptChanges();
                
                for (iTag = 0; iTag <= (lstAsset.Items.Count - 1); iTag++)
                {
                    TgNo = lstAsset.Items[iTag].SubItems[2].Text;
                
                    DataRow dr;
                    dr = dtOnLine.NewRow();
                    dr["ServerKey"] = Convert.ToInt32(cboLoc.SelectedValue);
                    dr["AssetTagID"] = TgNo;
                    dtOnLine.Rows.Add(dr);
                }
                
                // Open new Form
                frmDocDesdripency InvFm = new frmDocDesdripency(); 
                InvFm.searchedData = dtOnLine;
                InvFm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                InvFm.Closed += new EventHandler(this.OnOperFrmClosed);

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
            }
            catch (System.Data.SqlServerCe.SqlCeException)
            {
                MessageBox.Show("Data File is not able to access.");
            }
            catch (System.Net.WebException)
            {
                MessageBox.Show("Web exception occured.");
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
            }
        }
        #endregion
        
        private void PwdReqChkBx_CheckStateChanged(object sender, EventArgs e)
        {

        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        //redirect to scan barcode form.
        private void btnScnBarcd_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboLoc.SelectedValue) != 0)
            {
                // Open new BarCode Scanning Window
                frmBCScanForm fBCSFm = new frmBCScanForm();
                fBCSFm.Owner = this;
                fBCSFm.OpenMode = frmBCScanForm.FormType.Inventoryform;
                fBCSFm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                fBCSFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
                MessageBox.Show("Please select refrence document first.");

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tagList.Clear();  
            EPCListV.Items.Clear();
            lstAsset.Items.Clear();
            EPCListV.Visible = true;
        }
    }

    
}