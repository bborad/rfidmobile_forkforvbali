using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class RnDSettingsForm : Form
    {
        public RnDSettingsForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void OnApplyBttnClicked(object sender, EventArgs e)
        {
            UInt32 bandNum = 0;

            if (Bnd1Bttn.Checked)
                bandNum = 1;
            else if (Bnd2Bttn.Checked)
                bandNum = 2;
            else if (Bnd3Bttn.Checked)
                bandNum = 3;
            else if (Bnd4Bttn.Checked)
                bandNum = 4;
            else if (Bnd6Bttn.Checked)
                bandNum = 6;

            if (bandNum == 0)
            {
                MessageBox.Show("Please select a band number first", "Notice");
            }
            else
            {
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
               // Reader Rdr = ReaderFactory.GetReader();
                Rdr.bandNum = bandNum;
                bool Succ = Rdr.CustomFreqBandNumSet();
                if (Succ)
                    Program.ShowSuccess("Setting frequency band number");
                else
                    Program.ShowError("Setting frequency band number");
            }
        }


        private void FreqBandNumGetNotify(bool succ, UInt32 bandNum, String errMsg)
        {
            if (succ)
            {
                switch (bandNum)
                {
                    case 1:
                        Bnd1Bttn.Checked = true; break;
                    case 2:
                        Bnd2Bttn.Checked = true; break;
                    case 3:
                        Bnd3Bttn.Checked = true; break;
                    case 4:
                        Bnd4Bttn.Checked = true; break;
                    case 6:
                        Bnd6Bttn.Checked = true; break;
                    default:
                        MessageBox.Show("Unexpected band number: " + bandNum, "Error Notice");
                        break;
                }
            }
            else
            {
                MessageBox.Show("Fail to get current Frequency band number", "Error");
            }
            this.Enabled = true;
        }

        private void OnLoad(object sender, EventArgs e)
        {
            // Initialize CheckButton
            this.Enabled = false;
            //UInt32 BandNum = 2;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            //Reader Rdr = ReaderFactory.GetReader();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.bandNum = 2;
            if (!Rdr.CustomFreqBandNumGet())
            {
                Program.ShowError("Error getting current Frequency band number: " + Rdr.LastErrCode.ToString("F"));
            }
            else
            {
                switch (Rdr.bandNum)
                {
                    case 1:
                        Bnd1Bttn.Checked = true; break;
                    case 2:
                        Bnd2Bttn.Checked = true; break;
                    case 3:
                        Bnd3Bttn.Checked = true; break;
                    case 4:
                        Bnd4Bttn.Checked = true; break;
                    case 6:
                        Bnd6Bttn.Checked = true; break;
                    default:
                        Program.ShowError("Unexpected band number: " + Rdr.bandNum);
                        break;
                }
            }
            this.Enabled = true;
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnClosed(object sender, EventArgs e)
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region F1/F4/F5 Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F1:
                    if (down && this.Enabled) // Form Enabled implies no RFID operation is running
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }
        #endregion

    
    }
}