using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Drawing;
using System.Windows.Forms;
using CS101UILib;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagWrBnkInput : UserControl
    {
        public TagWrBnkInput()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            
            InitBxTxtBxState();
        }

        public event EventHandler TextModified;

        public String TagNo
        {
            get
            {
                return B1_2TxtBx.Text;  
            }
        }

        /// <summary>
        /// callee return whether the textbox is different from original
        /// </summary>
        /// <param name="bnk"></param>
        /// <param name="part">part of the bank(zero-based)</param>
        /// <param name="curInputStr"></param>
        /// <returns></returns>
        public delegate bool BnkIsModified(MemoryBanks4Op bnk, int part,  String curInputStr);
        private BnkIsModified bnkIsModified = null;

        public void SetBnkModifiedCb(BnkIsModified modifyChk)
        {
            bnkIsModified = modifyChk;
        }

        private void OnMBTxtBxFocusLost(object sender, EventArgs e)
        {
           // System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrBnkInput));

            System.ComponentModel.ComponentResourceManager resources;

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.TagWrBnkInput));
            }
            else
            {
                resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrBnkInput));
            }

            TextBox txtBx = (TextBox)sender;
            if (TxtBxLoaded(txtBx))
                BxTxBxPadZero(txtBx);
            else // Restore to original
            {
                if (Object.Equals(sender, B0_1TxtBx))
                    B0_1TxtBx.Text = resources.GetString("B0_1TxtBx.Text");
                else if (Object.Equals(sender, B0_2TxtBx))
                    B0_2TxtBx.Text = resources.GetString("B0_2TxtBx.Text");
                else if (Object.Equals(sender, B1_1TxtBx))
                    B1_1TxtBx.Text = resources.GetString("B1_1TxtBx.Text");
                else if (Object.Equals(sender, B1_2TxtBx))
                    B1_2TxtBx.Text = resources.GetString("B1_2TxtBx.Text");
                else if (Object.Equals(sender, B3TxtBx))
                    B3TxtBx.Text = resources.GetString("B3TxtBx.Text");
            }
        }

        private void OnMBTxtBxGotFocus(object sender, EventArgs e)
        {
            TextBox txtBx = (TextBox)sender;
            // clear text box if it's not loaded (still showing initial text cue)
            if (!TxtBxLoaded(txtBx))
                txtBx.Text = "";
        }

        private void HexKeyPressChk(object sender, KeyPressEventArgs e)
        {
            Program.HexKeyPressChk(sender, e);
            if (e.Handled == false)
            {
                // Valid input
                SetTxtBxLoadedState((TextBox)sender, true);
            }
        }

        // Change Text Label color to Red if different from last-read-value
        private void BxTxtBxChangeHandler(TextBox txtBx, MemoryBanks4Op bnk, int part)
        {
            if (bnkIsModified != null)
            {
                bool Modified = bnkIsModified(bnk, part, txtBx.Text);
                SetTxtBxModifiedState(txtBx, Modified);   
            }
        }

        private void OnB0_1TxtBxChanged(object sender, EventArgs e)
        {
            BxTxtBxChangeHandler((TextBox)sender, MemoryBanks4Op.Zero, 0);
        }
        private void OnB0_2TxtBxChanged(object sender, EventArgs e)
        {
            BxTxtBxChangeHandler((TextBox)sender, MemoryBanks4Op.Zero, 1);
        }

        private void OnB1_1TxtBxChanged(object sender, EventArgs e)
        {
            BxTxtBxChangeHandler((TextBox)sender, MemoryBanks4Op.One, 0);
        }

        private void OnB1_2TxtBxChanged(object sender, EventArgs e)
        {
            BxTxtBxChangeHandler((TextBox)sender, MemoryBanks4Op.One, 1);
            if (TextModified != null && ((TextBox)sender).BackColor != Color.Cyan)
            {
                TextModified(sender, e);
            }
        }

        private void OnB3TxtBxChanged(object sender, EventArgs e)
        {
            BxTxtBxChangeHandler((TextBox)sender, MemoryBanks4Op.Three, 0);
        }

        #region TextBox States
        [Flags]
        enum TxtBxState
        {
            Init = 0x00,
            Loaded = 0x01,
            Modified = 0x02,
        };

        private void InitBxTxtBxState()
        {
            TxtBxState State;

            State = TxtBxState.Init;
            
            B0_1TxtBx.Tag = State; // boxing
            B0_2TxtBx.Tag = State; // boxing
            B1_1TxtBx.Tag = State; // boxing
            B1_2TxtBx.Tag = State; // boxing
            B3TxtBx.Tag = State; // boxing

            SetBnksRdOnly(EPCTag.AvailBanks);
            EnableBnks(EPCTag.AvailBanks);
            DisableBnks((~EPCTag.AvailBanks));
        }


        private bool TxtBxModified(TextBox txtBx)
        {
            TxtBxState CurState = TxtBxState.Init;

            if (txtBx.Tag is TxtBxState)
            {
                CurState = (TxtBxState)txtBx.Tag;
            }
            return ((CurState & TxtBxState.Modified) == TxtBxState.Modified);
        }

        private bool TxtBxLoaded(TextBox txtBx)
        {
            TxtBxState CurState = TxtBxState.Init;

            if (txtBx.Tag is TxtBxState)
            {
                CurState = (TxtBxState)txtBx.Tag;
            }
            return ((CurState & TxtBxState.Loaded) == TxtBxState.Loaded);
        }

           // Update both TextBox State Variables and Color Indicator
        private bool SetTxtBxModifiedState(TextBox txtBx, bool modified)
        {
            bool DirtyFlg;
            bool Changed = false;

            if (txtBx.Tag is TxtBxState)
            {
                TxtBxState CurState = ((TxtBxState)txtBx.Tag);

                DirtyFlg = ((CurState & TxtBxState.Modified) == TxtBxState.Modified);

                if (DirtyFlg == modified)
                {
                    Changed = false;
                }
                else
                {
                    DirtyFlg = modified;
                    TxtBxState NewState = (modified) ? (CurState | TxtBxState.Modified) 
                                    : (CurState & (~TxtBxState.Modified));
                    txtBx.Tag = NewState;
                    Changed = true;
                }
            }
            // Modified: Cyan or Yellow depending on length of current input
            // No change: White
            if (Changed)
            {
                // Don't know how to retrieve original color from 'resources'.
                txtBx.BackColor = (modified) ?
                    ((txtBx.Text.Length != txtBx.MaxLength) ? Color.Cyan : Color.Yellow)
                : Color.White;
            }
            else if (modified)
            {
                if (txtBx.Text.Length != txtBx.MaxLength)
                {
                    if (txtBx.BackColor != Color.Cyan)
                        txtBx.BackColor = Color.Cyan;
                }
                else
                {
                    if (txtBx.BackColor != Color.Yellow)
                        txtBx.BackColor = Color.Yellow;
                }
            }
            else // not modified (unchanged due to some reason)
            {
                txtBx.BackColor = Color.White;
            }

            return Changed;
        }

        private void SetTxtBxLoadedState(TextBox txtBx, bool loaded)
        {
            if (txtBx.Tag is TxtBxState)
            {
                TxtBxState State = (TxtBxState)txtBx.Tag;
                State = (loaded) ? (State | TxtBxState.Loaded)
                    : (State & (~TxtBxState.Loaded));
                txtBx.Tag = State;
            }
        }

        public bool TxtBxesHasModifiedData()
        {
            return TxtBxModified(B0_1TxtBx) || TxtBxModified(B0_2TxtBx)
                || TxtBxModified(B1_1TxtBx) || TxtBxModified(B1_2TxtBx)
                || TxtBxModified(B3TxtBx);
        }

        public bool TagNoModified()
        {
            return TxtBxModified(B1_2TxtBx);
        }


        public MemoryBanks4Op ModifiedBnksToEnum()
        {
            MemoryBanks4Op bnks = MemoryBanks4Op.None;

            if (TxtBxModified(B0_1TxtBx) || TxtBxModified(B0_2TxtBx))
                bnks |= MemoryBanks4Op.Zero;
            if (TxtBxModified(B1_1TxtBx) || TxtBxModified(B1_2TxtBx))
                bnks |= MemoryBanks4Op.One;
            if (TxtBxModified(B3TxtBx))
                bnks |= MemoryBanks4Op.Three;

            return bnks;
        }

        #endregion

        #region TextBox properties (Enabled, ReadOnly)

        // support multiple selections
        public void DisableBnks(MemoryBanks4Op bnk)
        {
            if ((bnk & MemoryBanks4Op.Zero) == MemoryBanks4Op.Zero)
            {
                B0_1TxtBx.Enabled = false; B0_2TxtBx.Enabled = false;
            }
            if ((bnk & MemoryBanks4Op.One) == MemoryBanks4Op.One)
            {
                B1_1TxtBx.Enabled = false; B1_2TxtBx.Enabled = false;
            }
            if ((bnk & MemoryBanks4Op.Three) == MemoryBanks4Op.Three)
            {
                B3TxtBx.Enabled = false;
            }
        }

        // support multiple selections
        public void EnableBnks(MemoryBanks4Op bnk)
        {
            if ((bnk & MemoryBanks4Op.Zero) == MemoryBanks4Op.Zero)
            {
                B0_1TxtBx.Enabled = true; B0_2TxtBx.Enabled = true;
            }
            if ((bnk & MemoryBanks4Op.One) == MemoryBanks4Op.One)
            {
                B1_1TxtBx.Enabled = true; B1_2TxtBx.Enabled = true;
            }
            if ((bnk & MemoryBanks4Op.Three) == MemoryBanks4Op.Three)
            {
                int UsrAreaWdSz = EPCTag.MaxBnkMemSizeInWds(RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_USER);
                B3TxtBx.MaxLength = UsrAreaWdSz * 4; // 1 word = 2 bytes = 4 hex chars
                B3TxtBx.Enabled = true;
            }
        }

        public void SetBnksWriteable(MemoryBanks4Op bnk)
        {
            if ((bnk & MemoryBanks4Op.Zero) == MemoryBanks4Op.Zero)
            {
                B0_1TxtBx.ReadOnly = false; B0_2TxtBx.ReadOnly = false;
            }
            if ((bnk & MemoryBanks4Op.One) == MemoryBanks4Op.One)
            {
                B1_1TxtBx.ReadOnly = false; B1_2TxtBx.ReadOnly = false;
            }
            if ((bnk & MemoryBanks4Op.Three) == MemoryBanks4Op.Three)
            {
                B3TxtBx.ReadOnly = false;
            }
        }

        public void SetBnksRdOnly(MemoryBanks4Op bnk)
        {
            if ((bnk & MemoryBanks4Op.Zero) == MemoryBanks4Op.Zero)
            {
                B0_1TxtBx.ReadOnly = true; B0_2TxtBx.ReadOnly = true;
            }
            if ((bnk & MemoryBanks4Op.One) == MemoryBanks4Op.One)
            {
                B1_1TxtBx.ReadOnly = true; B1_2TxtBx.ReadOnly = true;
            }
            if ((bnk & MemoryBanks4Op.Three) == MemoryBanks4Op.Three)
            {
                B3TxtBx.ReadOnly = true;
            }
        }

        #endregion

        #region TextBox Content

        private void BxTxBxPadZero(TextBox txtBx)
        {
            if (txtBx.Text.Length < txtBx.MaxLength)
                txtBx.Text = txtBx.Text.PadRight(txtBx.MaxLength, '0');
        }

        // Overrides the Modified State
        public void LoadTxtBx(MemoryBanks4Op bnk, int part, String text)
        {
            TextBox TxtBx = null;
            switch (bnk)
            {
                case MemoryBanks4Op.Zero:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B0_1TxtBx;
                            break;
                        case 1:
                            TxtBx = B0_2TxtBx;
                            break;
                    }
                    break;
                case MemoryBanks4Op.One:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B1_1TxtBx;
                            break;
                        case 1:
                            TxtBx = B1_2TxtBx;
                            break;
                    }
                    break;
                case MemoryBanks4Op.Three:
                    TxtBx = B3TxtBx;
                    break;
            }
            if (TxtBx != null)
            {
                TxtBx.Text = text;
                SetTxtBxLoadedState(TxtBx, true);
                SetTxtBxModifiedState(TxtBx, false);
            }
        }

        public void setDataFromDatabase(String Text)
        {
            txtName.Text = Text; 
        }

        
        /// <summary>
        /// This partial update would leave the 'Modified State' alone, letting
        /// the checking done when TextBox ValueChanged handler
        /// </summary>
        /// <param name="bnk"></param>
        /// <param name="part"></param>
        /// <param name="text"></param>
        /// <param name="offset">char offset within the 'part'</param>
        public void SetTxtBx(MemoryBanks4Op bnk, int part, int offset, String text)
        {
            TextBox TxtBx = null;
            switch (bnk)
            {
                case MemoryBanks4Op.Zero:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B0_1TxtBx;
                            break;
                        case 1:
                            TxtBx = B0_2TxtBx;
                            break;
                    }
                    break;
                case MemoryBanks4Op.One:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B1_1TxtBx;
                            break;
                        case 1:
                            TxtBx = B1_2TxtBx;
                            break;
                    }
                    break;
                case MemoryBanks4Op.Three:
                    TxtBx = B3TxtBx;
                    break;
            }
            if (TxtBx != null)
            {
                String OrigText = String.Copy(TxtBx.Text);
                int LastSegSz = OrigText.Length - (offset + text.Length);
                TxtBx.Text = OrigText.Substring(0, offset) + text
                    + ((LastSegSz > 0) ? OrigText.Substring(offset + text.Length) : String.Empty);
                SetTxtBxLoadedState(TxtBx, true);
                // Update 'Modified' flag (indirectly)
                BxTxtBxChangeHandler(TxtBx, bnk, part);
            }
        }

        private String GetTxtBxStr(MemoryBanks4Op bnk, int part)
        {
            TextBox TxtBx = null ;

            switch (bnk)
            {
                case MemoryBanks4Op.Zero:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B0_1TxtBx; break;
                        case 1:
                            TxtBx = B0_2TxtBx; break;
                    }
                    break;
                case MemoryBanks4Op.One:
                    switch (part)
                    {
                        case 0:
                            TxtBx = B1_1TxtBx; break;
                        case 1:
                            TxtBx = B1_2TxtBx; break;
                    }
                    break;
                case MemoryBanks4Op.Three:
                    TxtBx = B3TxtBx; break;
                default:
                    MessageBox.Show("TxtBxGetStr(): unexpected bank number" + bnk.ToString("F"));
                    break;
            }
    
            String txt = null;
            if (TxtBx != null)
            {
                if (TxtBxLoaded(TxtBx))
                    txt = TxtBx.Text;
            }

            return txt;
        }

        // TBD: Return string less than full bank with offset
        public bool GetBnkStr(MemoryBanks4Op bnk, out ushort wdOffset, out String bnkDataStr)
        {
            String Str0, Str1;
            bool Succ = false;

            wdOffset = 0;
            bnkDataStr = null;
            switch (bnk)
            {
                case MemoryBanks4Op.Zero:
                    Str0 = GetTxtBxStr(bnk, 0);
                    Str1 = GetTxtBxStr(bnk, 1);
                    wdOffset = 0;
                    if (Str0 == null || Str0.Length == 0)
                    {
                        wdOffset += 2; // Kill Password not entered
                        if (Str1 == null || Str1.Length == 0)
                            Succ = false; // the whole bank is empty
                        else
                        {
                            bnkDataStr = Str1;
                            Succ = true;
                        }
                    }
                    else 
                    {
                        if (Str1 == null || Str1.Length == 0)
                        {
                            bnkDataStr = Str0;
                        }
                        else
                        {
                            // Concatenation
                            if (Str0.Length < 8)
                                Str0 = Str0.PadRight(8, '0');
                            bnkDataStr = String.Concat(Str0, Str1);
                        }
                        Succ = true;
                    }
                    break;
                case MemoryBanks4Op.One:
                    Str0 = GetTxtBxStr(bnk, 0);
                    Str1 = GetTxtBxStr(bnk, 1);
                    wdOffset = EPCTag.CRC16FldSz; // Bank starts with CRC
                    if (Str0 == null || Str0.Length == 0)
                    {
                        wdOffset += EPCTag.PCFldSz; // PC not entered
                        if (Str1 == null || Str1.Length == 0)
                            Succ = false; // the whole bank is empty
                        else
                        {
                            bnkDataStr = Str1;
                            Succ = true;
                        }
                    }
                    else
                    {
                        if (Str1 == null || Str1.Length == 0)
                        {
                            bnkDataStr = Str0;
                        }
                        else
                        {
                            // Concatenation
                            if (Str0.Length < (EPCTag.PCFldSz * 4))
                                Str0 = Str0.PadRight((EPCTag.PCFldSz * 4), '0');
                            bnkDataStr = String.Concat(Str0, Str1);
                        }
                        Succ = true;
                    }
                    break;
                case MemoryBanks4Op.Three:
                    wdOffset = 0;
                    Str0 = GetTxtBxStr(bnk, 0);
                    bnkDataStr = Str0;
                    Succ = (Str0 != null && Str0.Length > 0);
                    break;
            }

            return Succ;
        }
        #endregion

        
    }
}
