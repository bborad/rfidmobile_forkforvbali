using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;


namespace OnRamp
{
    public partial class RdrInfoSummaryForm : Form
    {
        
        private HandHeldHotKeyNotify callerHKNotify;

        public static double[,] FreqBndCountrySpecRanges; // more than actual frequency ranges

        public RdrInfoSummaryForm(HandHeldHotKeyNotify hkNotify)
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }

            callerHKNotify = hkNotify;

            InitFreqBndCountrySpecRanges();
        }

        private void OnLoad(object sender, EventArgs e)
        {
            if (callerHKNotify != null)
                ClsHotkey.SubHotKeyDelegate(callerHKNotify);
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);

            this.Enabled = false;

            UserPref Pref = UserPref.GetInstance();
           //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            
            RdrNameLbl.Text = Pref.RdrName + " (Ver:  " + "0." + Program.SvnVer + ", " + Rdr.MacVer + ")";

            UInt32 BandNum = LoadBandNumToDspy();

            ShowCountry(BandNum);

            LoadLinkProfToDspy(); // last one to arrive

            ShowAntennaPwr();

            ShowNetworkStatus();
        }

        private const int IPV4_ADDR_STRBUF_SZ = 16;
        private const int WLAN_MAC_ADDR_STRBUF_SZ = 16; // 8 bytes
        private String lastDhcpAddr;
        private SystemTime lastDhcpLeaseTm;
        private SystemTime lastDhcpExpireTm;

        private String IPHWAddrDspyStr(String hwAddr, String ipAddr)
        {
            StringBuilder Sb = new StringBuilder();
            Sb = Sb.Append(ipAddr);
            Sb = Sb.Append("(");
            for (int i = 0; i < hwAddr.Length; i += 2)
            {
                if (i > 0)
                    Sb = Sb.Append(" ");
                Sb = Sb.Append(hwAddr.Substring(i, 2));
            }
            Sb = Sb.Append(")");

            return Sb.ToString();
        }

        private void ShowNetworkStatus()
        {
            SettingMgt.f_SettingMgt_RfidSpSetting_Read(); // to-be-removed
            bool WifiEn = false, APconnected = false, DhcpEn = false;
            StringBuilder HwAddr = new StringBuilder(WLAN_MAC_ADDR_STRBUF_SZ);
            StringBuilder IpAddr = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            StringBuilder IpMask = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            StringBuilder GwAddr = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            StringBuilder DhcpAddr = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            StringBuilder DNS1Addr = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            StringBuilder DNS2Addr = new StringBuilder(IPV4_ADDR_STRBUF_SZ);
            SystemTime DhcpLeaseTm = new SystemTime();
            SystemTime DhcpExpireTm = new SystemTime();
            HRESULT_RFID HRes = SettingMgt.f_CollectWifiTCPIPStatus(ref WifiEn, ref APconnected, HwAddr,
                IpAddr, IpMask, GwAddr, DNS1Addr, DNS2Addr, ref DhcpEn, DhcpAddr,DhcpLeaseTm, DhcpExpireTm);
            if (RfidSp.SUCCEEDED(HRes))
            {
                if (!WifiEn)
                {
                    IPAddrLbl.Text = "WIFI Adapter Disabled";
                    GWLbl.Text = "WIFI Adapter Disabled";
                }
                else
                {
                    if (APconnected)
                    {
                        IPAddrLbl.Text = IPHWAddrDspyStr(HwAddr.ToString(), IpAddr.ToString());
                        GWLbl.Text = GwAddr.ToString();
                        DhcpLnkLbl.Visible = DhcpEn;
                        if (DhcpEn)
                        {
                            lastDhcpAddr = DhcpAddr.ToString();
                            lastDhcpLeaseTm = DhcpLeaseTm;
                            lastDhcpExpireTm = DhcpExpireTm;
                        }
                    }
                    StringBuilder DNSDspyStr = new StringBuilder();
                    if (DNS1Addr.Length > 0)
                    {
                        DNSDspyStr = DNSDspyStr.Append(DNS1Addr.ToString());
                        if (DNS2Addr.Length > 0)
                            DNSDspyStr.Append("; ");
                    }
                    if (DNS2Addr.Length > 0)
                        DNSDspyStr.Append(DNS2Addr.ToString());
                    DNSLbl.Text = DNSDspyStr.ToString();
                }
                
            }

        }

        // assume value already is local-time
        private String DHCPLeaseDatesDspyStr(SystemTime sysTm)
        {
            DateTime DT = new DateTime(sysTm.wYear, sysTm.wMonth, sysTm.wDay,
                sysTm.wHour, sysTm.wMinute, sysTm.wSecond, sysTm.wMilliseconds, DateTimeKind.Local);
            return DT.ToString("G");
        }

        private void OnDhcpLnkClicked(object sender, EventArgs e)
        {
            TransientMsgDlg Dlg = new TransientMsgDlg(0); // stays visible until close
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                Dlg.Size = new Size(210, 150);
            }
            String DhcpDetailsMsg = "DHCP Server IP: " + lastDhcpAddr + "\n"
                            + "Lease obtained: " + DHCPLeaseDatesDspyStr(lastDhcpLeaseTm) + "\n"
                            + "Lease expires: " + DHCPLeaseDatesDspyStr(lastDhcpExpireTm);
            Dlg.MsgHAlign = ContentAlignment.TopLeft;
            Dlg.SetDpyMsg(DhcpDetailsMsg, "DHCP Lease detail:");
            Dlg.ShowDialog();
        }

        private void ShowAntennaPwr()
        {
            UserPref Pref = UserPref.GetInstance();

            String PwrStr = (Pref.AntennaPwr).ToString("F1");

            PwrLbl.Text = PwrStr;
        }

        private void LnkProfDetailGetNotify(bool succ, ref RFID_RADIO_LINK_PROFILE lnkPrf, String errMsg)
        {
            if (succ)
            {
                String ModStr = String.Empty;
                switch (lnkPrf.iso18K6C.modulationType)
                {
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_PR_ASK:
                        ModStr = "PR-ASK"; break;
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_SSB_ASK:
                        ModStr = "SSB-ASK"; break;
                    case RFID_18K6C_MODULATION_TYPE.RFID_18K6C_MODULATION_TYPE_DSB_ASK:
                        ModStr = "DSB-ASK"; break;
                }
                String TariStr = "tari=" + lnkPrf.iso18K6C.tari/1000D + "us";
                 String EncStr = null;
                switch (lnkPrf.iso18K6C.millerNumber)
                {
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_FM0:
                        EncStr = "FM0"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_2:
                        EncStr = "Miller-2"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_4:
                        EncStr = "Miller-4"; break;
                    case RFID_18K6C_MILLER_NUMBER.RFID_18K6C_MILLER_NUMBER_8:
                        EncStr = "Miller-8"; break;
                }
                String LnkFreqStr =  "LF=" + lnkPrf.iso18K6C.trLinkFrequency / 1000D + "kHz";
                UserPref Pref = UserPref.GetInstance();
                String DspyStr = "#" + Pref.LinkProf + "; " + TariStr + "; " + ModStr + "; " +  LnkFreqStr + "; " + EncStr;

                LnkPrfLbl.Text = DspyStr;
            }

            this.Enabled = true;
        }

        private void LoadLinkProfToDspy()
        {
            UserPref Pref = UserPref.GetInstance();

            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            
           // Rdr.LnkProfInfoGetNotification+=LnkProfDetailGetNotify;
            Rdr.RegisterLnkProfInfoGetNotificationEvent(LnkProfDetailGetNotify);
            Rdr.ProfNum = Pref.LinkProf;
            Rdr.LinkProfInfoGet();
        }

        private void ShowCountry(UInt32 bandNum)
        {
            UserPref Pref = UserPref.GetInstance();
            String CountryStr = String.Empty;

            //switch (Pref.FreqProf)
            //{
            //    case ClslibRfidSp.CustomFreqGrp.CN:
            //        CountryStr = "China"; break;
            //    case ClslibRfidSp.CustomFreqGrp.ETSI:
            //        CountryStr = "ETSI"; break;
            //    case ClslibRfidSp.CustomFreqGrp.FCC:
            //        CountryStr = "USA(FCC)"; break;
            //    case ClslibRfidSp.CustomFreqGrp.HK:
            //        CountryStr = "Hong Kong"; break;
            //    case ClslibRfidSp.CustomFreqGrp.JPN:
            //        CountryStr = "Japan"; break;
            //    case ClslibRfidSp.CustomFreqGrp.KR:
            //        CountryStr = "Korea"; break;
            //    case ClslibRfidSp.CustomFreqGrp.TW:
            //        CountryStr = "Taiwan"; break;
            //}

            String CntryFreqRangeStr = DevStatForm.FreqBndCountryDisplayRanges[(int)Pref.FreqProf, 0].ToString("F1")
                                    + "-" + DevStatForm.FreqBndCountryDisplayRanges[(int)Pref.FreqProf, 1].ToString("F1") + "Mhz";
            String ChnStr = "; All Chns";
            if (Pref.SingleFreqChn >= 0)
            {
                float Freq = DevStatForm.FreqLists[(int)Pref.FreqProf][Pref.SingleFreqChn];
                ChnStr = "; Chn " + Pref.SingleFreqChn + " (" + Freq.ToString("F3") + " Mhz)";
            }
            String LBTStr = String.Empty;
            if (Pref.EnFreqChnLBT)
                LBTStr = "; LBT=Y";

            if (DevStatForm.FreqBndCountries[(int)DevStatForm.FreqBandNumGetIndex((int)bandNum)].Length > 1)
                CountryLbl.Text = CountryStr + "(" + CntryFreqRangeStr + ")" + ChnStr + LBTStr;
            else
                CountryLbl.Text = CountryStr + ChnStr + LBTStr;
        }

        private void InitFreqBndCountrySpecRanges()
        {
            // Array arranged according to definition of CustomFreqGrp enum def
            FreqBndCountrySpecRanges = new double[7, 2] { { 902.75, 927.50 }, { 920.5D, 924.5D }, 
                { 922.25, 927.75 }, { 865.7, 867.5 }, { 910.2D, 913.8D }, { 920.250, 924.750 },
                { 952.2, 953.8 } };
        }

        private UInt32 LoadBandNumToDspy()
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
           // UInt32 BandNum = 2;
            Rdr.bandNum = 2;
            if (Rdr.CustomFreqBandNumGet())
            {
                String DspyStr = String.Empty;

                switch (Rdr.bandNum)
                {
                    case 1:
                        DspyStr = "1 (865-868 Mhz)";
                        break;
                    case 2:
                        DspyStr = "2 (902-928Mhz)";
                        break;
                    case 3:
                        DspyStr = "3 (952-954Mhz)";
                        break;
                    case 4:
                        DspyStr = "4 (919-928Mhz)";
                        break;
                    case 6:
                        DspyStr = "6 (910-914Mhz)";
                        break;
                }
                FrqBndLbl.Text = DspyStr;
            }
            else
            {
                FrqBndLbl.Text = "Unavailable";
                Program.ShowError("Failed to retrieve frequency band number: " + Rdr.LastErrCode.ToString("F"));
            }
            return Rdr.bandNum;
        }

        private void OnKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                e.Handled = true;
                this.Close();
            }
        }

        // Close requested
        private void OnClosing(object sender, CancelEventArgs e)
        {
            if (this.Enabled == false)
                e.Cancel = true; // ignore 
            else
            {
                ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
                if (callerHKNotify != null)
                    ClsHotkey.AddHotKeyDelegate(callerHKNotify);
            }
        }
        private bool F1Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F1:
                    if (down && !F1Depressed)
                    {
                        this.Close();
                    }
                    F1Depressed = down;
                    break;
            }
        }

      

    }
}