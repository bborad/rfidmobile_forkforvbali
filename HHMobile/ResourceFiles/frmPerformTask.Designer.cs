﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.4959
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace OnRamp.ResourceFiles {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    internal class frmPerformTask {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        internal frmPerformTask() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("OnRamp.ResourceFiles.frmPerformTask", typeof(frmPerformTask).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        internal static System.Drawing.Point B0Label_Location {
            get {
                object obj = ResourceManager.GetObject("B0Label.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size B0Label_Size {
            get {
                object obj = ResourceManager.GetObject("B0Label.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Item Name.
        /// </summary>
        internal static string B0Label_Text {
            get {
                return ResourceManager.GetString("B0Label.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point btnCancel_Location {
            get {
                object obj = ResourceManager.GetObject("btnCancel.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnCancel_Size {
            get {
                object obj = ResourceManager.GetObject("btnCancel.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnCancel_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnCancel.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cancel.
        /// </summary>
        internal static string btnCancel_Text {
            get {
                return ResourceManager.GetString("btnCancel.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point btnSave_Location {
            get {
                object obj = ResourceManager.GetObject("btnSave.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size btnSave_Size {
            get {
                object obj = ResourceManager.GetObject("btnSave.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int btnSave_TabIndex {
            get {
                object obj = ResourceManager.GetObject("btnSave.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save.
        /// </summary>
        internal static string btnSave_Text {
            get {
                return ResourceManager.GetString("btnSave.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point cboTemplate_Location {
            get {
                object obj = ResourceManager.GetObject("cboTemplate.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size cboTemplate_Size {
            get {
                object obj = ResourceManager.GetObject("cboTemplate.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int cboTemplate_TabIndex {
            get {
                object obj = ResourceManager.GetObject("cboTemplate.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point chkComplete_Location {
            get {
                object obj = ResourceManager.GetObject("chkComplete.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size chkComplete_Size {
            get {
                object obj = ResourceManager.GetObject("chkComplete.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int chkComplete_TabIndex {
            get {
                object obj = ResourceManager.GetObject("chkComplete.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Mark as Completed.
        /// </summary>
        internal static string chkComplete_Text {
            get {
                return ResourceManager.GetString("chkComplete.Text", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to DueDate.
        /// </summary>
        internal static string DueDate_Text {
            get {
                return ResourceManager.GetString("DueDate.Text", resourceCulture);
            }
        }
        
        internal static int DueDate_Width {
            get {
                object obj = ResourceManager.GetObject("DueDate.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point label1_Location {
            get {
                object obj = ResourceManager.GetObject("label1.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label1_Size {
            get {
                object obj = ResourceManager.GetObject("label1.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tasks.
        /// </summary>
        internal static string label1_Text {
            get {
                return ResourceManager.GetString("label1.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label2_Location {
            get {
                object obj = ResourceManager.GetObject("label2.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label2_Size {
            get {
                object obj = ResourceManager.GetObject("label2.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Service.
        /// </summary>
        internal static string label2_Text {
            get {
                return ResourceManager.GetString("label2.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point label3_Location {
            get {
                object obj = ResourceManager.GetObject("label3.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size label3_Size {
            get {
                object obj = ResourceManager.GetObject("label3.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Comments.
        /// </summary>
        internal static string label3_Text {
            get {
                return ResourceManager.GetString("label3.Text", resourceCulture);
            }
        }
        
        internal static System.Drawing.Point lstTask_Location {
            get {
                object obj = ResourceManager.GetObject("lstTask.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size lstTask_Size {
            get {
                object obj = ResourceManager.GetObject("lstTask.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int lstTask_TabIndex {
            get {
                object obj = ResourceManager.GetObject("lstTask.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point mainMenu1_TrayLocation {
            get {
                object obj = ResourceManager.GetObject("mainMenu1.TrayLocation", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to X.
        /// </summary>
        internal static string Mark_Text {
            get {
                return ResourceManager.GetString("Mark.Text", resourceCulture);
            }
        }
        
        internal static int Mark_Width {
            get {
                object obj = ResourceManager.GetObject("Mark.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point panel1_Location {
            get {
                object obj = ResourceManager.GetObject("panel1.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static System.Drawing.Size panel1_Size {
            get {
                object obj = ResourceManager.GetObject("panel1.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to ID.
        /// </summary>
        internal static string Tasks_Text {
            get {
                return ResourceManager.GetString("Tasks.Text", resourceCulture);
            }
        }
        
        internal static int Tasks_Width {
            get {
                object obj = ResourceManager.GetObject("Tasks.Width", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point txtComments_Location {
            get {
                object obj = ResourceManager.GetObject("txtComments.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static int txtComments_MaxLength {
            get {
                object obj = ResourceManager.GetObject("txtComments.MaxLength", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static bool txtComments_Multiline {
            get {
                object obj = ResourceManager.GetObject("txtComments.Multiline", resourceCulture);
                return ((bool)(obj));
            }
        }
        
        internal static System.Drawing.Size txtComments_Size {
            get {
                object obj = ResourceManager.GetObject("txtComments.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int txtComments_TabIndex {
            get {
                object obj = ResourceManager.GetObject("txtComments.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Point txtName_Location {
            get {
                object obj = ResourceManager.GetObject("txtName.Location", resourceCulture);
                return ((System.Drawing.Point)(obj));
            }
        }
        
        internal static int txtName_MaxLength {
            get {
                object obj = ResourceManager.GetObject("txtName.MaxLength", resourceCulture);
                return ((int)(obj));
            }
        }
        
        internal static System.Drawing.Size txtName_Size {
            get {
                object obj = ResourceManager.GetObject("txtName.Size", resourceCulture);
                return ((System.Drawing.Size)(obj));
            }
        }
        
        internal static int txtName_TabIndex {
            get {
                object obj = ResourceManager.GetObject("txtName.TabIndex", resourceCulture);
                return ((int)(obj));
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Asset Name.
        /// </summary>
        internal static string txtName_Text {
            get {
                return ResourceManager.GetString("txtName.Text", resourceCulture);
            }
        }
    }
}
