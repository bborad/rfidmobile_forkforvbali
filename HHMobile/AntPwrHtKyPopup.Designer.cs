namespace OnRamp
{
    partial class AntPwrHtKyPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(AntPwrHtKyPopup));
            System.Windows.Forms.Label label2;
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.AntPwrLbl = new System.Windows.Forms.Label();
            this.DebounceTmr = new System.Windows.Forms.Timer();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // AntPwrLbl
            // 
            resources.ApplyResources(this.AntPwrLbl, "AntPwrLbl");
            this.AntPwrLbl.Name = "AntPwrLbl";
            // 
            // DebounceTmr
            // 
            this.DebounceTmr.Interval = 500;
            this.DebounceTmr.Tick += new System.EventHandler(this.OnDebounceTmrTick);
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // AntPwrHtKyPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.ControlBox = false;
            this.Controls.Add(label2);
            this.Controls.Add(this.AntPwrLbl);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AntPwrHtKyPopup";
            this.TopMost = true;
            this.EnabledChanged += new System.EventHandler(this.OnEnabled);
            this.Closed += new System.EventHandler(this.OnClosed);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.AntPwrHtKyPopup));
            System.Windows.Forms.Label label2;
            this.label1 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.AntPwrLbl = new System.Windows.Forms.Label();
            this.DebounceTmr = new System.Windows.Forms.Timer();
            label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // AntPwrLbl
            // 
            resources.ApplyResources(this.AntPwrLbl, "AntPwrLbl");
            this.AntPwrLbl.Name = "AntPwrLbl";
            // 
            // DebounceTmr
            // 
            this.DebounceTmr.Interval = 500;
            this.DebounceTmr.Tick += new System.EventHandler(this.OnDebounceTmrTick);
            // 
            // label2
            // 
            resources.ApplyResources(label2, "label2");
            label2.Name = "label2";
            // 
            // AntPwrHtKyPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.PaleGoldenrod;
            this.ControlBox = false;
            this.Controls.Add(label2);
            this.Controls.Add(this.AntPwrLbl);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "AntPwrHtKyPopup";
            this.TopMost = true;
            this.EnabledChanged += new System.EventHandler(this.OnEnabled);
            this.Closed += new System.EventHandler(this.OnClosed);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OnMouseUp);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            this.MouseDown += new System.Windows.Forms.MouseEventHandler(this.OnMouseDown);
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label AntPwrLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer DebounceTmr;

    }
}