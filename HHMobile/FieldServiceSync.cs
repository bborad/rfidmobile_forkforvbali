using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class FieldServiceSync : Form
    {
        public delegate void stateHandler(Int64 status);

        TransientMsgDlg msgbox = null;

        int SelectedLocationID;

        public FieldServiceSync()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            try
            {
               // if (Convert.ToInt64(cboLoc.SelectedValue) != 0)
                if (SelectedLocationID >= 0)
                {

                    Sync ss = new Sync();
                    ss.SyncFieldServiceData(SelectedLocationID);   


                    // Open new Tag Write window
                    //frmInventory InvFm = new frmInventory();
                    //InvFm.searchedData = dtResult;
                    //InvFm.Show();
                    // disable form until this new form closed
                    syncProgress.Value = syncProgress.Maximum;

                    if (ss.strError.Length != 0)
                    {
                        MessageBox.Show("Synchronization found Following errors. \r" + ss.strError);
                    }
                    else
                    {
                        MessageBox.Show("Synchronization completed sucessfully.");
                        this.Close();
                    }
                }
                else
                {
                    MessageBox.Show("Please select location first.");
                }
                //this.Enabled = false;
                //InvFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                MessageBox.Show("Error occured " + ep.Message.ToString());
                Logger.LogError(ep.Message); 
            }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void FieldServiceSync_Load(object sender, EventArgs e)
        {
            this.Text = this.Text + " - " + UserPref.CurVersionNo;
            //Cursor.Current = Cursors.WaitCursor;
            //#region"Set Location Combo"
            //DataTable dtList = new DataTable();
            //dtList = Locations.getLocationList();   

            //DataRow dr = dtList.NewRow();
            //dr["ID_Location"] = 0;
            //dr["Name"] = "--Select--";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges();

            //dr = dtList.NewRow();
            //dr["ID_Location"] = -1;
            //dr["Name"] = "--ALL--";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges();

            //cboLoc.ValueMember = "ID_Location";
            //cboLoc.DisplayMember = "Name";
            //cboLoc.DataSource = dtList;

            //cboLoc.SelectedValue = 0;
            //#endregion

            lnkLocName.Tag = lnkLocName.Text;

           // Cursor.Current = Cursors.Default;

            ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState);

        }

        void ProgressStatus_progressState(long status)
        {
            if (this.InvokeRequired)
            {
                //ProgressStatus_progressState(status);
                this.Invoke(new stateHandler(this.ProgressStatus_progressState), new Object[] { status });
                return;
            }
            try
            {
                syncProgress.Value = (int)status;
            }
            catch { }
            return;
        }

        private void FieldServiceSync_Closing(object sender, CancelEventArgs e)
        {
            ProgressStatus.progressState -= new ProgressStatus.progressStateHandler(ProgressStatus_progressState);
        }

        private void label2_ParentChanged(object sender, EventArgs e)
        {

        }

        private void lnkLocName_Click(object sender, EventArgs e)
        {
            try
            {
                ShowMessage(lnkLocName.Tag.ToString());
            }
            catch
            {
            }
        }

        private void btnSelLoc_Click(object sender, EventArgs e)
        {
            string locName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Locations, "All Locations", 0, true);
            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            {
                locName = frmSel.objectName;
                SelectedLocationID = frmSel.objectID;
            }
            else if (SelectedLocationID == frmSel.defaultID)
            {
                locName = frmSel.defaultName;
            }
            else
            {
                return;
            }


            if (locName.Length > 15)
            {
                lnkLocName.Text = locName.Substring(0, 13);
                lnkLocName.Text = lnkLocName.Text + "..";
            }
            else
            {
                lnkLocName.Text = locName;
            }
            lnkLocName.Tag = locName;
        }

        void ShowMessage(string message)
        {
            try
            {
                if (msgbox != null)
                {
                    msgbox.Close();
                    //msgbox = null;
                }
                else
                {

                }

                msgbox = new TransientMsgDlg(0);

                msgbox.Closed += new EventHandler(msgbox_Closed);

                int dispLength;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    dispLength = 23;
                    msgbox.Size = new Size(210, 110);
                }
                else
                {
                    dispLength = 30;
                    msgbox.Height = 110;
                }

                if (message.Trim().Length > dispLength)
                {
                    char[] splitParams = new char[1];
                    splitParams[0] = ' ';
                    string[] msgSplit = message.Split(splitParams);

                    int end = 0;

                    if (msgSplit != null && msgSplit.Length > 0)
                    {
                        StringBuilder strValue = new StringBuilder();

                        foreach (string value in msgSplit)
                        {
                            end = strValue.Length;
                            strValue.Append(value);

                            if (value.Length > dispLength)
                            {
                                try
                                {
                                    strValue.Insert(end + dispLength, " ");
                                    end = dispLength + 1;
                                    //strValue.Append(value.Insert(dispLength, " "));
                                    if (value.Substring(dispLength).Length > dispLength)
                                    {
                                        //strValue.Append(value.Insert(dispLength * 2, " "));
                                        strValue.Insert(end + dispLength, " ");
                                    }
                                }
                                catch
                                {
                                }
                            }
                            //else
                            //{
                            //    strValue.Append(value);
                            //}

                            strValue.Append(" ");
                        }

                        message = strValue.ToString().Trim();
                    }

                }

                msgbox.MsgHAlign = ContentAlignment.TopLeft;
                msgbox.SetTimeout(3);
                msgbox.TopMost = true;
                msgbox.AllowUserClose = true;
                msgbox.SetDpyMsg(message, "Full Text");

                msgbox.AutoScroll = false;

                msgbox.Show();
            }
            catch
            {
            }
        }

        void msgbox_Closed(object sender, EventArgs e)
        {
            try
            {
                msgbox = null;

            }
            catch
            {
            }
        }

    }
}