using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.Reflection;
using System.IO;

namespace OnRamp
{
    public partial class SplashScreen : Form
    {
        private Bitmap splashImg;
        // don't forget to add namespace before the file name!
        private string splashImgFile = @"OnRamp.Ramp_CSL (Splash).jpg";
#if SPLASHSCREEN_SELFCLOSE
        private Timer closeTimer;
        private const int SPLASHSCREEN_SHOW_PERIOD_MS = 1000;
#endif

        public SplashScreen()
        {
            int ImgH = 0, ImgW = 0, ImgX, ImgY;
            InitializeComponent();

            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                if (UserPref.forClient == UserPref.Client.OnRamp)
                {
                    splashImgFile = @"OnRamp.Ramp_CSL (Splash)-AT870.jpg";
                }
                else if (UserPref.forClient == UserPref.Client.SmartTrack)
                {
                    splashImgFile = @"OnRamp.ST_CSL-(Splash)-AT870.jpg";
                }
                else if (UserPref.forClient == UserPref.Client.Access)
                {
                    splashImgFile = @"OnRamp.Access_CSL-(Splash)-AT870.jpg";
                }
            }
            else
            {

                if (UserPref.forClient == UserPref.Client.OnRamp)
                {
                    splashImgFile = @"OnRamp.Ramp_CSL (Splash).jpg";
                }
                else if (UserPref.forClient == UserPref.Client.SmartTrack)
                {
                    splashImgFile = @"OnRamp.ST_CSL (Splash).jpg";
                }
                else if (UserPref.forClient == UserPref.Client.Access)
                {
                    splashImgFile = @"OnRamp.Access_CSL-(Splash).jpg";
                }
            }

            try
            {
                /* For this to work, the splash image must be configured
                 * as 'embedded resource'
                 */
                splashImg = new Bitmap(Assembly.GetExecutingAssembly()
                .GetManifestResourceStream(splashImgFile));
                ImgH = splashImg.Height;
                ImgW = splashImg.Width;
            }
            catch (Exception e)
            {
                ImgH = Screen.PrimaryScreen.WorkingArea.Height / 2;
                ImgW = Screen.PrimaryScreen.WorkingArea.Width / 2;
                Console.WriteLine(e.ToString());
                
            }
            finally
            {
                /* center image */
                int ScrnH = Screen.PrimaryScreen.WorkingArea.Height;
                int ScrnW = Screen.PrimaryScreen.WorkingArea.Width;
                ImgX = (ScrnW - ImgW) / 2;
                ImgY = (ScrnH - ImgH) / 2;
            }

            this.Bounds = new Rectangle(ImgX, ImgY, ImgW, ImgH);

            this.Load += new EventHandler(OnSplScrnLoaded);
        }

        private void OnSplScrnLoaded(object sender, EventArgs e)
        {
#if SPLASHSCREEN_SELFCLOSE
            // Set up timer to close the splash screen
            closeTimer = new Timer();
            closeTimer.Tick += new EventHandler(OnTimerExpired);
            closeTimer.Interval = SPLASHSCREEN_SHOW_PERIOD_MS;
            closeTimer.Enabled = true;
#endif
        }
#if SPLASHSCREEN_SELFCLOSE
        private void OnTimerExpired(object sender, EventArgs e)
        {
            closeTimer.Enabled = false; // one-shot timer
            // close the splashs screen
            this.Close();
        }
#endif
        private void SplashScreen_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;

            if (splashImg != null)
            {
                g.DrawImage(splashImg, e.ClipRectangle.X, e.ClipRectangle.Y,
                    e.ClipRectangle, GraphicsUnit.Pixel);
            }
            else
            {
                // How to calculate the display length (pixels) of a string
                // with a certain font in C#/.Net?
                g.DrawString("Missing Splash Pic", this.Font, new SolidBrush(Color.Black),
                    (float)0.0, (float)((this.Height - this.Font.Size) / 2.0));
            }

#if SPLASHSCREEN_SELFCLOSE
            // Handle case where timer expired before event loop rans
            if (closeTimer.Enabled == false)
                this.Close();
#endif
        }
    }
}