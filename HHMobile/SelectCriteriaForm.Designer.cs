namespace OnRamp
{
    partial class SelectCriteriaForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SelectCriteriaForm));
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.CritNumLbl = new System.Windows.Forms.Label();
            this.SelCritMaskPosCmbBx = new System.Windows.Forms.ComboBox();
            this.SelCritMaskTxtBx = new System.Windows.Forms.TextBox();
            this.SelCritBankCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedActTypeCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedFlagCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedTgtCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedSLValCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedSLValCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedTgtCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedFlagCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedActTypeCmbBx = new System.Windows.Forms.ComboBox();
            this.RstLnk = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // CritNumLbl
            // 
            resources.ApplyResources(this.CritNumLbl, "CritNumLbl");
            this.CritNumLbl.Name = "CritNumLbl";
            // 
            // SelCritMaskPosCmbBx
            // 
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items"));
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items1"));
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items2"));
            resources.ApplyResources(this.SelCritMaskPosCmbBx, "SelCritMaskPosCmbBx");
            this.SelCritMaskPosCmbBx.Name = "SelCritMaskPosCmbBx";
            this.SelCritMaskPosCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnSelCritMaskPosChanged);
            // 
            // SelCritMaskTxtBx
            // 
            resources.ApplyResources(this.SelCritMaskTxtBx, "SelCritMaskTxtBx");
            this.SelCritMaskTxtBx.Name = "SelCritMaskTxtBx";
            this.SelCritMaskTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            // 
            // SelCritBankCmbBx
            // 
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items"));
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items1"));
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items2"));
            resources.ApplyResources(this.SelCritBankCmbBx, "SelCritBankCmbBx");
            this.SelCritBankCmbBx.Name = "SelCritBankCmbBx";
            // 
            // MatchedActTypeCmbBx
            // 
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items"));
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items1"));
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items2"));
            resources.ApplyResources(this.MatchedActTypeCmbBx, "MatchedActTypeCmbBx");
            this.MatchedActTypeCmbBx.Name = "MatchedActTypeCmbBx";
            this.MatchedActTypeCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedActTypeChanged);
            // 
            // MatchedFlagCmbBx
            // 
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items1"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items2"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items3"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items4"));
            resources.ApplyResources(this.MatchedFlagCmbBx, "MatchedFlagCmbBx");
            this.MatchedFlagCmbBx.Name = "MatchedFlagCmbBx";
            this.MatchedFlagCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedFlagChanged);
            // 
            // MatchedTgtCmbBx
            // 
            this.MatchedTgtCmbBx.Items.Add(resources.GetString("MatchedTgtCmbBx.Items"));
            this.MatchedTgtCmbBx.Items.Add(resources.GetString("MatchedTgtCmbBx.Items1"));
            resources.ApplyResources(this.MatchedTgtCmbBx, "MatchedTgtCmbBx");
            this.MatchedTgtCmbBx.Name = "MatchedTgtCmbBx";
            this.MatchedTgtCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedTgtChanged);
            // 
            // MatchedSLValCmbBx
            // 
            this.MatchedSLValCmbBx.Items.Add(resources.GetString("MatchedSLValCmbBx.Items"));
            this.MatchedSLValCmbBx.Items.Add(resources.GetString("MatchedSLValCmbBx.Items1"));
            resources.ApplyResources(this.MatchedSLValCmbBx, "MatchedSLValCmbBx");
            this.MatchedSLValCmbBx.Name = "MatchedSLValCmbBx";
            this.MatchedSLValCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedSLValChanged);
            // 
            // NonMatchedSLValCmbBx
            // 
            this.NonMatchedSLValCmbBx.Items.Add(resources.GetString("NonMatchedSLValCmbBx.Items"));
            this.NonMatchedSLValCmbBx.Items.Add(resources.GetString("NonMatchedSLValCmbBx.Items1"));
            resources.ApplyResources(this.NonMatchedSLValCmbBx, "NonMatchedSLValCmbBx");
            this.NonMatchedSLValCmbBx.Name = "NonMatchedSLValCmbBx";
            this.NonMatchedSLValCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedSLValChanged);
            // 
            // NonMatchedTgtCmbBx
            // 
            this.NonMatchedTgtCmbBx.Items.Add(resources.GetString("NonMatchedTgtCmbBx.Items"));
            this.NonMatchedTgtCmbBx.Items.Add(resources.GetString("NonMatchedTgtCmbBx.Items1"));
            resources.ApplyResources(this.NonMatchedTgtCmbBx, "NonMatchedTgtCmbBx");
            this.NonMatchedTgtCmbBx.Name = "NonMatchedTgtCmbBx";
            this.NonMatchedTgtCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedTgtChanged);
            // 
            // NonMatchedFlagCmbBx
            // 
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items1"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items2"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items3"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items4"));
            resources.ApplyResources(this.NonMatchedFlagCmbBx, "NonMatchedFlagCmbBx");
            this.NonMatchedFlagCmbBx.Name = "NonMatchedFlagCmbBx";
            this.NonMatchedFlagCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedFlagChanged);
            // 
            // NonMatchedActTypeCmbBx
            // 
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items"));
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items1"));
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items2"));
            resources.ApplyResources(this.NonMatchedActTypeCmbBx, "NonMatchedActTypeCmbBx");
            this.NonMatchedActTypeCmbBx.Name = "NonMatchedActTypeCmbBx";
            this.NonMatchedActTypeCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedActTypeChanged);
            // 
            // RstLnk
            // 
            resources.ApplyResources(this.RstLnk, "RstLnk");
            this.RstLnk.Name = "RstLnk";
            this.RstLnk.Click += new System.EventHandler(this.OnRstLnkClicked);
            // 
            // SelectCriteriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.RstLnk);
            this.Controls.Add(this.NonMatchedSLValCmbBx);
            this.Controls.Add(this.NonMatchedTgtCmbBx);
            this.Controls.Add(this.NonMatchedFlagCmbBx);
            this.Controls.Add(this.NonMatchedActTypeCmbBx);
            this.Controls.Add(this.MatchedSLValCmbBx);
            this.Controls.Add(this.MatchedTgtCmbBx);
            this.Controls.Add(this.MatchedFlagCmbBx);
            this.Controls.Add(this.MatchedActTypeCmbBx);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.SelCritMaskPosCmbBx);
            this.Controls.Add(this.SelCritMaskTxtBx);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.SelCritBankCmbBx);
            this.Controls.Add(this.CritNumLbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectCriteriaForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.SelectCriteriaForm));
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.CritNumLbl = new System.Windows.Forms.Label();
            this.SelCritMaskPosCmbBx = new System.Windows.Forms.ComboBox();
            this.SelCritMaskTxtBx = new System.Windows.Forms.TextBox();
            this.SelCritBankCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedActTypeCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedFlagCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedTgtCmbBx = new System.Windows.Forms.ComboBox();
            this.MatchedSLValCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedSLValCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedTgtCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedFlagCmbBx = new System.Windows.Forms.ComboBox();
            this.NonMatchedActTypeCmbBx = new System.Windows.Forms.ComboBox();
            this.RstLnk = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // CritNumLbl
            // 
            resources.ApplyResources(this.CritNumLbl, "CritNumLbl");
            this.CritNumLbl.Name = "CritNumLbl";
            // 
            // SelCritMaskPosCmbBx
            // 
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items"));
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items1"));
            this.SelCritMaskPosCmbBx.Items.Add(resources.GetString("SelCritMaskPosCmbBx.Items2"));
            resources.ApplyResources(this.SelCritMaskPosCmbBx, "SelCritMaskPosCmbBx");
            this.SelCritMaskPosCmbBx.Name = "SelCritMaskPosCmbBx";
            this.SelCritMaskPosCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnSelCritMaskPosChanged);
            // 
            // SelCritMaskTxtBx
            // 
            resources.ApplyResources(this.SelCritMaskTxtBx, "SelCritMaskTxtBx");
            this.SelCritMaskTxtBx.Name = "SelCritMaskTxtBx";
            this.SelCritMaskTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            // 
            // SelCritBankCmbBx
            // 
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items"));
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items1"));
            this.SelCritBankCmbBx.Items.Add(resources.GetString("SelCritBankCmbBx.Items2"));
            resources.ApplyResources(this.SelCritBankCmbBx, "SelCritBankCmbBx");
            this.SelCritBankCmbBx.Name = "SelCritBankCmbBx";
            // 
            // MatchedActTypeCmbBx
            // 
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items"));
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items1"));
            this.MatchedActTypeCmbBx.Items.Add(resources.GetString("MatchedActTypeCmbBx.Items2"));
            resources.ApplyResources(this.MatchedActTypeCmbBx, "MatchedActTypeCmbBx");
            this.MatchedActTypeCmbBx.Name = "MatchedActTypeCmbBx";
            this.MatchedActTypeCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedActTypeChanged);
            // 
            // MatchedFlagCmbBx
            // 
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items1"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items2"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items3"));
            this.MatchedFlagCmbBx.Items.Add(resources.GetString("MatchedFlagCmbBx.Items4"));
            resources.ApplyResources(this.MatchedFlagCmbBx, "MatchedFlagCmbBx");
            this.MatchedFlagCmbBx.Name = "MatchedFlagCmbBx";
            this.MatchedFlagCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedFlagChanged);
            // 
            // MatchedTgtCmbBx
            // 
            this.MatchedTgtCmbBx.Items.Add(resources.GetString("MatchedTgtCmbBx.Items"));
            this.MatchedTgtCmbBx.Items.Add(resources.GetString("MatchedTgtCmbBx.Items1"));
            resources.ApplyResources(this.MatchedTgtCmbBx, "MatchedTgtCmbBx");
            this.MatchedTgtCmbBx.Name = "MatchedTgtCmbBx";
            this.MatchedTgtCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedTgtChanged);
            // 
            // MatchedSLValCmbBx
            // 
            this.MatchedSLValCmbBx.Items.Add(resources.GetString("MatchedSLValCmbBx.Items"));
            this.MatchedSLValCmbBx.Items.Add(resources.GetString("MatchedSLValCmbBx.Items1"));
            resources.ApplyResources(this.MatchedSLValCmbBx, "MatchedSLValCmbBx");
            this.MatchedSLValCmbBx.Name = "MatchedSLValCmbBx";
            this.MatchedSLValCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnMatchedSLValChanged);
            // 
            // NonMatchedSLValCmbBx
            // 
            this.NonMatchedSLValCmbBx.Items.Add(resources.GetString("NonMatchedSLValCmbBx.Items"));
            this.NonMatchedSLValCmbBx.Items.Add(resources.GetString("NonMatchedSLValCmbBx.Items1"));
            resources.ApplyResources(this.NonMatchedSLValCmbBx, "NonMatchedSLValCmbBx");
            this.NonMatchedSLValCmbBx.Name = "NonMatchedSLValCmbBx";
            this.NonMatchedSLValCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedSLValChanged);
            // 
            // NonMatchedTgtCmbBx
            // 
            this.NonMatchedTgtCmbBx.Items.Add(resources.GetString("NonMatchedTgtCmbBx.Items"));
            this.NonMatchedTgtCmbBx.Items.Add(resources.GetString("NonMatchedTgtCmbBx.Items1"));
            resources.ApplyResources(this.NonMatchedTgtCmbBx, "NonMatchedTgtCmbBx");
            this.NonMatchedTgtCmbBx.Name = "NonMatchedTgtCmbBx";
            this.NonMatchedTgtCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedTgtChanged);
            // 
            // NonMatchedFlagCmbBx
            // 
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items1"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items2"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items3"));
            this.NonMatchedFlagCmbBx.Items.Add(resources.GetString("NonMatchedFlagCmbBx.Items4"));
            resources.ApplyResources(this.NonMatchedFlagCmbBx, "NonMatchedFlagCmbBx");
            this.NonMatchedFlagCmbBx.Name = "NonMatchedFlagCmbBx";
            this.NonMatchedFlagCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedFlagChanged);
            // 
            // NonMatchedActTypeCmbBx
            // 
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items"));
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items1"));
            this.NonMatchedActTypeCmbBx.Items.Add(resources.GetString("NonMatchedActTypeCmbBx.Items2"));
            resources.ApplyResources(this.NonMatchedActTypeCmbBx, "NonMatchedActTypeCmbBx");
            this.NonMatchedActTypeCmbBx.Name = "NonMatchedActTypeCmbBx";
            this.NonMatchedActTypeCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnNonMatchedActTypeChanged);
            // 
            // RstLnk
            // 
            resources.ApplyResources(this.RstLnk, "RstLnk");
            this.RstLnk.Name = "RstLnk";
            this.RstLnk.Click += new System.EventHandler(this.OnRstLnkClicked);
            // 
            // SelectCriteriaForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.RstLnk);
            this.Controls.Add(this.NonMatchedSLValCmbBx);
            this.Controls.Add(this.NonMatchedTgtCmbBx);
            this.Controls.Add(this.NonMatchedFlagCmbBx);
            this.Controls.Add(this.NonMatchedActTypeCmbBx);
            this.Controls.Add(this.MatchedSLValCmbBx);
            this.Controls.Add(this.MatchedTgtCmbBx);
            this.Controls.Add(this.MatchedFlagCmbBx);
            this.Controls.Add(this.MatchedActTypeCmbBx);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.SelCritMaskPosCmbBx);
            this.Controls.Add(this.SelCritMaskTxtBx);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.SelCritBankCmbBx);
            this.Controls.Add(this.CritNumLbl);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SelectCriteriaForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label CritNumLbl;
        private System.Windows.Forms.ComboBox SelCritMaskPosCmbBx;
        private System.Windows.Forms.TextBox SelCritMaskTxtBx;
        private System.Windows.Forms.ComboBox SelCritBankCmbBx;
        private System.Windows.Forms.ComboBox MatchedActTypeCmbBx;
        private System.Windows.Forms.ComboBox MatchedFlagCmbBx;
        private System.Windows.Forms.ComboBox MatchedTgtCmbBx;
        private System.Windows.Forms.ComboBox MatchedSLValCmbBx;
        private System.Windows.Forms.ComboBox NonMatchedSLValCmbBx;
        private System.Windows.Forms.ComboBox NonMatchedTgtCmbBx;
        private System.Windows.Forms.ComboBox NonMatchedFlagCmbBx;
        private System.Windows.Forms.ComboBox NonMatchedActTypeCmbBx;
        private System.Windows.Forms.LinkLabel RstLnk;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label21;
    }
}