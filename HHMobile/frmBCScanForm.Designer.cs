namespace OnRamp
{
    partial class frmBCScanForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmBCScanForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.BCListV = new System.Windows.Forms.ListView();
            this.SetupBttn = new System.Windows.Forms.Button();
            this.SaveBttn = new System.Windows.Forms.Button();
            this.btnInventorySave = new System.Windows.Forms.Button();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // BCListV
            // 
            this.BCListV.Columns.Add(columnHeader2);
            this.BCListV.FullRowSelect = true;
            this.BCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.BCListV, "BCListV");
            this.BCListV.Name = "BCListV";
            this.BCListV.View = System.Windows.Forms.View.Details;
            // 
            // SetupBttn
            // 
            resources.ApplyResources(this.SetupBttn, "SetupBttn");
            this.SetupBttn.Name = "SetupBttn";
            this.SetupBttn.Click += new System.EventHandler(this.OnSetupBttnClicked);
            // 
            // SaveBttn
            // 
            resources.ApplyResources(this.SaveBttn, "SaveBttn");
            this.SaveBttn.Name = "SaveBttn";
            this.SaveBttn.Click += new System.EventHandler(this.OnSaveBttnClicked);
            // 
            // btnInventorySave
            // 
            resources.ApplyResources(this.btnInventorySave, "btnInventorySave");
            this.btnInventorySave.Name = "btnInventorySave";
            this.btnInventorySave.Click += new System.EventHandler(this.btnInventorySave_Click);
            // 
            // frmBCScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnInventorySave);
            this.Controls.Add(this.SaveBttn);
            this.Controls.Add(this.SetupBttn);
            this.Controls.Add(this.BCListV);
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.ScanBttn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBCScanForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.frmBCScanForm_KeyUp);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.frmBCScanForm_KeyDown);
            this.ResumeLayout(false);

        }

        private void InitializeComponent_AT870()
        {
            System.Windows.Forms.ColumnHeader columnHeader2;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OnRamp.ResourceFiles.frmBCScanForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.ScanBttn = new System.Windows.Forms.Button();
            this.ClrBttn = new System.Windows.Forms.Button();
            this.BCListV = new System.Windows.Forms.ListView();
            this.SetupBttn = new System.Windows.Forms.Button();
            this.SaveBttn = new System.Windows.Forms.Button();
            this.btnInventorySave = new System.Windows.Forms.Button();
            columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // columnHeader2
            // 
            resources.ApplyResources(columnHeader2, "columnHeader2");
            // 
            // ScanBttn
            // 
            resources.ApplyResources(this.ScanBttn, "ScanBttn");
            this.ScanBttn.Name = "ScanBttn";
            this.ScanBttn.Click += new System.EventHandler(this.OnScanBttnClicked);
            // 
            // ClrBttn
            // 
            resources.ApplyResources(this.ClrBttn, "ClrBttn");
            this.ClrBttn.Name = "ClrBttn";
            this.ClrBttn.Click += new System.EventHandler(this.OnClrBttnClicked);
            // 
            // BCListV
            // 
            this.BCListV.Columns.Add(columnHeader2);
            this.BCListV.FullRowSelect = true;
            this.BCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.BCListV, "BCListV");
            this.BCListV.Name = "BCListV";
            this.BCListV.View = System.Windows.Forms.View.Details;
            // 
            // SetupBttn
            // 
            resources.ApplyResources(this.SetupBttn, "SetupBttn");
            this.SetupBttn.Name = "SetupBttn";
            this.SetupBttn.Click += new System.EventHandler(this.OnSetupBttnClicked);
            // 
            // SaveBttn
            // 
            resources.ApplyResources(this.SaveBttn, "SaveBttn");
            this.SaveBttn.Name = "SaveBttn";
            this.SaveBttn.Click += new System.EventHandler(this.OnSaveBttnClicked);
            // 
            // btnInventorySave
            // 
            resources.ApplyResources(this.btnInventorySave, "btnInventorySave");
            this.btnInventorySave.Name = "btnInventorySave";
            this.btnInventorySave.Click += new System.EventHandler(this.btnInventorySave_Click);
            // 
            // frmBCScanForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.btnInventorySave);
            this.Controls.Add(this.SaveBttn);
            this.Controls.Add(this.SetupBttn);
            this.Controls.Add(this.BCListV);
            this.Controls.Add(this.ClrBttn);
            this.Controls.Add(this.ScanBttn);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmBCScanForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.ResumeLayout(false);

        }


        #endregion

        private System.Windows.Forms.Button ScanBttn;
        private System.Windows.Forms.Button ClrBttn;
        private System.Windows.Forms.ListView BCListV;
        private System.Windows.Forms.Button SetupBttn;
        private System.Windows.Forms.Button SaveBttn;
        private System.Windows.Forms.Button btnInventorySave;

    }
}