﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using HHDeviceInterface.RFIDSp;

using ReaderTypes;
using ClsLibBKLogs;
using ClsReaderLib;

using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class frmConfig : Form
    {
        public frmConfig()
        {
            InitializeComponent();
        }

        uint powerLevel;

        private void frmConfig_Load(object sender, EventArgs e)
        {
           // this.Text = this.Text + " - " + UserPref.CurVersionNo;

            this.MaximizeBox = false;
            this.MinimizeBox = false;

            UserPref upref = UserPref.GetInstance();
            if (upref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                PwrLvlTrkBr.Maximum = 3;
                PwrLvlTrkBr.Minimum = 1;

                this.KeyPreview = true;
                PwrLvlValLbl.Size = new Size(70, 25);

                PwrLvlValLbl.Text = upref.AntennaPowerLevel.ToString();
                PwrLvlTrkBr.Value = (int)upref.AntennaPowerLevel;

                txtLowValue.Text = upref.LowPowerValue.ToString();
                txtMedValue.Text = upref.MediumPowerValue.ToString();
                txtHighValue.Text = upref.HighPowerValue.ToString();

                txtLowValue.KeyPress += NumKeyOnly;
                txtMedValue.KeyPress += NumKeyOnly;
                txtHighValue.KeyPress += NumKeyOnly;

                txtLowValue.TextChanged += txt_TextChanged;
                txtMedValue.TextChanged += txt_TextChanged;
                txtHighValue.TextChanged += txt_TextChanged; 

                if (upref.ScannerType == 1)
                {
                    rb1DScanner.Checked = true;
                    rb2DScanner.Checked = false;
                }
                else if (upref.ScannerType == 2)
                {
                    rb1DScanner.Checked = false;
                    rb2DScanner.Checked = true;
                }

                rb2DScanner.CheckedChanged += new EventHandler(rb2DScanner_CheckedChanged);
                rb1DScanner.CheckedChanged += new EventHandler(rb1DScanner_CheckedChanged);
                           
                if (upref.ScannerType == 1)
                {
                    rb1DScanner.Checked = true;
                }
                else
                {
                    rb2DScanner.Checked = true;
                }
           
                //MessageBox.Show("Functionality is under construction.");               
                txtServiceUrl.Text = upref.ServiceURL;
                txtPageSize.Text = upref.PageSize.ToString();
                txtMsgDisplayTimeOut.Text = upref.MsgWndTimeOut.ToString();

                rbErrLogEnable.Checked = upref.EnableErrorLogging;

                Reader Rdr = ReaderFactory.GetReader();
                ReaderTypes.CONTROL_VALUE ctrlValue = Rdr.GetConfiguration();
           
                //txtSession.Text = upref.ATIDSession.ToString();
                //txtQValue.Text = upref.QValue.ToString();
                //txtScantime.Text = upref.ScanTime.ToString();
                //txtLBTTime.Text = upref.LBTTime.ToString();

                txtSession.Text = ctrlValue.SessionValue.ToString();
                txtQValue.Text = ctrlValue.Qvalue.ToString();
                txtScantime.Text = ctrlValue.ScanTime.ToString();
                txtLBTTime.Text = ctrlValue.LBT_Time.ToString();

                txtProtocolVer.Text = ctrlValue.Version;
                txtHoppingMode.Text = ctrlValue.Hopping.ToString();

                txtSession.KeyPress += NumKeyOnly;
                txtQValue.KeyPress += NumKeyOnly;
                txtScantime.KeyPress += NumKeyOnly;
                txtLBTTime.KeyPress += NumKeyOnly;

                lblFirmWareVer.Text = lblFirmWareVer.Text + ctrlValue.FirmWareVersion;

                if (upref.IDInputMethod == 0)
                {
                    rbManual.Checked = true;

                }
                else if (upref.IDInputMethod == 1)
                {
                    rbBarcode.Checked = true;
                }
                else if (upref.IDInputMethod == 2)
                {
                    rbRfid.Checked = true;
                }

                if (upref.DDSelectionOption == 0)
                {
                    rbDropDown.Checked = true;

                }
                else if (upref.DDSelectionOption == 1)
                {
                    rbInstantSearch.Checked = true;
                }
                txtObjectListPageSize.Text = upref.SelectScreen_PageSize.ToString();

                if (upref.ApplicationMode == UserPref.AppMode.IntegratedMode)
                {
                    rdbintegratedmode.Checked = true;
                    pnlstandalonemode.Enabled = false;
                }
                else
                {
                    rdbstandalonemode.Checked = true;
                    pnlstandalonemode.Enabled = true;

                }
                txttagprefix.Text = upref.TagPreFix;
                txtcsvpath.Text = upref.CSVPath;
                txtdays.Text = upref.BackupDays.ToString();
            } 
        }

        private void txt_TextChanged(object sender, EventArgs e)
        {

            // ApplyButton.Enabled = ValidatePowerLevelValues(false); 
            ApplyButton.Enabled = true;
        }

        public static void NumKeyOnly(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar >= '0' && e.KeyChar <= '9') || (e.KeyChar == (char)Keys.Back))
            {
                e.Handled = false; // accept
            }
            else
            {
                e.Handled = true; 

            }
        }

        public bool ValidatePowerLevelValues(bool apply)
        {
            bool result = true;
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                try
                {
                    string value1, value2, value3;
                    int low, medium, high;

                    value1 = txtLowValue.Text;
                    value2 = txtMedValue.Text;
                    value3 = txtHighValue.Text;

                    if (value1 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtLowValue.Focus();
                        result = false;
                    }
                    else if (value2 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtMedValue.Focus();
                        result = false;
                    }
                    else if (value3 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtHighValue.Focus();
                        result = false;
                    }

                    low = Convert.ToInt32(value1);
                    medium = Convert.ToInt32(value2);
                    high = Convert.ToInt32(value3);

                    if (low > 31 || medium > 31 || high > 31)
                    {
                        MessageBox.Show("Value cannot be greater than 30.");
                        result = false;
                    }
                    else if (low > medium || low > high)
                    {
                        MessageBox.Show("Low value cannot be greater than Medium OR Hign value.");
                        txtLowValue.Focus();
                        result = false;
                    }
                    else if (medium > high)
                    {
                        MessageBox.Show("Medium value cannot be greater than Hign value.");
                        txtMedValue.Focus();
                        result = false;
                    }

                    if (apply == true && result == true)
                    {
                        UserPref pref = UserPref.GetInstance();

                        pref.LowPowerValue = low;
                        pref.MediumPowerValue = medium;
                        pref.HighPowerValue = high;

                        RFID_ANTENNA_PWRLEVEL level = ((RFID_ANTENNA_PWRLEVEL)PwrLvlTrkBr.Value);
                        if (level == RFID_ANTENNA_PWRLEVEL.LOW)
                        {
                            powerLevel = Convert.ToUInt32(pref.LowPowerValue);
                        }
                        else if (level == RFID_ANTENNA_PWRLEVEL.MEDIUM)
                        {
                            powerLevel = Convert.ToUInt32(pref.MediumPowerValue);
                        }
                        else if (level == RFID_ANTENNA_PWRLEVEL.HIGH)
                        {
                            powerLevel = Convert.ToUInt32(pref.HighPowerValue);
                        }

                        pref.AntennaPowerLevel = level;

                    }

                }
                catch
                {
                }
            }
            return result;
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {

        }

        private void PwrLvlTrkBr_ValueChanged(object sender, EventArgs e)
        {
            UserPref pref = UserPref.GetInstance();

            if (pref.SelectedHardware == HardwareSelection.AT870Reader)
            {
                RFID_ANTENNA_PWRLEVEL level = ((RFID_ANTENNA_PWRLEVEL)PwrLvlTrkBr.Value);
                if (level == RFID_ANTENNA_PWRLEVEL.LOW)
                {
                    powerLevel = Convert.ToUInt32(pref.LowPowerValue);
                }
                else if (level == RFID_ANTENNA_PWRLEVEL.MEDIUM)
                {
                    powerLevel = Convert.ToUInt32(pref.MediumPowerValue);
                }
                else if (level == RFID_ANTENNA_PWRLEVEL.HIGH)
                {
                    powerLevel = Convert.ToUInt32(pref.HighPowerValue);
                }

                // update display label
                PwrLvlValLbl.Text = level.ToString();

            }

        }

        private void OnApplyButtonClicked(object sender, EventArgs e)
        {
            Program.RefreshStatusLabel(this, "Applying Changes");
          
            TabPage SelPage = TabCtrl.TabPages[TabCtrl.SelectedIndex];
            if (SelPage == AntTab)
            {
                this.Enabled = false;

                if (ValidatePowerLevelValues(true))
                {
                    Program.SetAntennaPower(Convert.ToInt32(powerLevel));

                }
                this.Enabled = true;
            }
            else if (SelPage == tabBarcodeSettings)
            {
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();

                if (rb1DScanner.Checked)
                    Pref.ScannerType = 1;
                else if (rb2DScanner.Checked)
                    Pref.ScannerType = 2;
                //Cursor.Current = Cursors.WaitCursor;
                //Program.barInitialized = false;
                //if (Program.BRdr != null)
                //{
                //    Program.BRdr.StopReader();
                //    Program.BRdr.CloseReader();
                //}

                //Program.InitBarcodeReader();
                //Cursor.Current = Cursors.Default;
                MessageBox.Show("Please restart the application.");
                this.Enabled = true;
            }
            else if (SelPage == ServiceSettings)
            {
                //MessageBox.Show("Functionality is under construction.");

                UserPref Pref = UserPref.GetInstance();
                Pref.ServiceURL = txtServiceUrl.Text;
                if (String.IsNullOrEmpty(txtServiceUrl.Text))
                {
                    Program.ShowWarning("Please input Service URL");
                    return;
                }

                try
                {
                    if (String.IsNullOrEmpty(txtPageSize.Text))
                    {
                        Program.ShowWarning("Please input Page Size");
                        txtPageSize.Focus();
                        return;
                    }

                    Int32 pagesize = Convert.ToInt32(txtPageSize.Text);
                    if (pagesize <= 0)
                    {
                        Program.ShowWarning("Page size must be greater than zero.");
                        txtPageSize.Focus();
                        return;
                    }
                    Pref.PageSize = pagesize;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Program.ShowWarning("Page Size must be a numeric value.");
                    txtPageSize.Focus();
                    return;
                }

                try
                {
                    if (String.IsNullOrEmpty(txtMsgDisplayTimeOut.Text))
                    {
                        Program.ShowWarning("Please input Msg Display TimeOut value");
                        txtMsgDisplayTimeOut.Focus();
                        return;
                    }

                    Int32 MsgDisplayTimeOut = Convert.ToInt32(txtMsgDisplayTimeOut.Text);
                    if (MsgDisplayTimeOut <= 0)
                    {
                        Program.ShowWarning("Msg Display TimeOut must be greater than zero.");
                        txtPageSize.Focus();
                        return;
                    }
                    Pref.MsgWndTimeOut = MsgDisplayTimeOut;
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message);
                    Program.ShowWarning("Page Size must be a numeric value.");
                    txtPageSize.Focus();
                    return;
                }

                if (rbErrLogEnable.Checked)
                {
                    Pref.EnableErrorLogging = true;
                }
                else
                {
                    Pref.EnableErrorLogging = false;
                }

                Logger.enableErrorLogging = Pref.EnableErrorLogging;

                //InvtrySetupUpdateApplyBttnEnabled();
                ApplyButton.Enabled = true;
                Program.RefreshStatusLabel(this, "Changes Saved");

            }
            else if (SelPage == tpControlSettings)
            {
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();

                if (ValidateControlValues(true))
                {
                    Reader Rdr = ReaderFactory.GetReader();
                    ReaderTypes.CONTROL_VALUE ctrlValue = Rdr.GetConfiguration();
                    ctrlValue.SessionValue = Pref.ATIDSession;
                    ctrlValue.LBT_Time = Pref.LBTTime;
                    ctrlValue.Qvalue = Pref.QValue;
                    ctrlValue.ScanTime = Pref.ScanTime;

                    Rdr.SetConfiguration(ctrlValue);
                }


                this.Enabled = true;
            }
            else if (SelPage == tpDDSelectionSettings)
            {
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();
                if (rbDropDown.Checked)
                {
                    Pref.DDSelectionOption = 0; // pre load
                    
                }
                else
                {
                    Pref.DDSelectionOption = 1; // non pre Load
                }

                if (rbManual.Checked)
                {
                    Pref.IDInputMethod = 0; // Manual Input only ( no Scan Button )
                }
                else if (rbBarcode.Checked)
                {
                    Pref.IDInputMethod = 1; // Barcode Input
                }
                else if (rbRfid.Checked)
                {
                    Pref.IDInputMethod = 2; // RFID Input
                }
                if (txtObjectListPageSize.Text.Trim() != "")
                {
                    Pref.SelectScreen_PageSize = Convert.ToInt32(txtObjectListPageSize.Text);
                }
                else
                {
                    Pref.SelectScreen_PageSize = 0;
                }

                this.Enabled = true;
            }
            else if (SelPage == tpaddmode)
            {
                #region AppMode page
                    this.Enabled = false;
                        UserPref Pref = UserPref.GetInstance();
                        Pref.TagPreFix = txttagprefix.Text.Trim();
                        if (rdbstandalonemode.Checked)
                        {
                            Pref.ApplicationMode = UserPref.AppMode.StandAloneMode;
                           
                            Pref.CSVPath = txtcsvpath.Text.Trim();
                            try
                            {
                                Pref.BackupDays = System.Convert.ToInt32(txtdays.Text.Trim());
                            }
                            catch
                            {
                                Pref.BackupDays = 7;
                            }

                        }
                        else
                        {
                            Pref.ApplicationMode = UserPref.AppMode.IntegratedMode;
                        }
                    this.Enabled = true;
                #endregion

            }

            ApplyButton.Enabled = false;
            Program.RefreshStatusLabel(this, "Changes Saved");

        }

        void rb1DScanner_CheckedChanged(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().ScannerType != 1)
                ApplyButton.Enabled = true;
            else
                ApplyButton.Enabled = false;
        }

        void rb2DScanner_CheckedChanged(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().ScannerType != 2)
                ApplyButton.Enabled = true;
            else
                ApplyButton.Enabled = false;
        }

        private void OnForm_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F7:
                case Keys.F8:

                    Program.On_KeyUp(sender, e);

                    break;
                default:
                    break;
            }
        }

        private void rbErrLogEnable_CheckedChanged(object sender, EventArgs e)
        {
            if (UserPref.GetInstance().EnableErrorLogging == true)
                ApplyButton.Enabled = true;
            else
                ApplyButton.Enabled = false;
        }


        public bool ValidateControlValues(bool apply)
        {
            bool result = true;
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                try
                {
                    string value1, value2, value3, value4;

                    value1 = txtQValue.Text;
                    value2 = txtScantime.Text; 
                    value3 = txtSession.Text;
                    value4 = txtLBTTime.Text;

                    if (value1 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtQValue.Focus();
                        result = false;
                    }
                    else if (value2 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtLBTTime.Focus();
                        result = false;
                    }
                    else if (value3 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtSession.Focus();
                        result = false;
                    }
                    else if (value4 == "")
                    {
                        MessageBox.Show("Value cannot be blank");
                        txtScantime.Focus();
                        result = false;
                    }

                    if (Convert.ToInt32(value1) > 15)
                    {
                        MessageBox.Show("Value cannot be greater than 15");
                        txtQValue.Focus();
                        result = false;
                    }
                    else if (Convert.ToInt32(value2) > 60)
                    {
                        MessageBox.Show("Value cannot be greater than 60");
                        txtScantime.Focus();
                        result = false;
                    }
                    else if (Convert.ToInt32(value3) > 3)
                    {
                        MessageBox.Show("Value cannot be greater than 3");
                        txtSession.Focus();
                        result = false;
                    }
                    else if (Convert.ToInt32(value4) > 4000)
                    {
                        MessageBox.Show("Value cannot be greater than 4000");
                        txtLBTTime.Focus();
                        result = false;
                    }

                    if (apply == true && result == true)
                    {
                        UserPref pref = UserPref.GetInstance();

                        pref.QValue = Convert.ToInt32(value1);
                        pref.ScanTime = Convert.ToInt32(value2);
                        pref.ATIDSession = Convert.ToInt32(value3);
                        pref.LBTTime = Convert.ToInt32(value4);

                    }

                }
                catch
                {
                }
            }
            return result;
        }

        private void RstButton_Click(object sender, EventArgs e)
        {
            TabPage SelPage = TabCtrl.TabPages[TabCtrl.SelectedIndex];
            UserPref upref = UserPref.GetInstance();

            if (SelPage == AntTab)
            {
                PwrLvlValLbl.Text = upref.AntennaPowerLevel.ToString();
                PwrLvlTrkBr.Value = (int)upref.AntennaPowerLevel;

                txtLowValue.Text = upref.LowPowerValue.ToString();
                txtMedValue.Text = upref.MediumPowerValue.ToString();
                txtHighValue.Text = upref.HighPowerValue.ToString();
            }
            else if (SelPage == tabBarcodeSettings)
            {
                if (upref.ScannerType == 1)
                {
                    rb1DScanner.Checked = true;
                }
                else
                {
                    rb2DScanner.Checked = true;
                }
            }
            else if (SelPage == ServiceSettings)
            {
                //MessageBox.Show("Functionality is under construction.");               
                txtServiceUrl.Text = upref.ServiceURL;
                txtPageSize.Text = upref.PageSize.ToString();
                txtMsgDisplayTimeOut.Text = upref.MsgWndTimeOut.ToString();

                rbErrLogEnable.Checked = upref.EnableErrorLogging;

            }
            else if (SelPage == tpControlSettings)
            {
                txtSession.Text = upref.ATIDSession.ToString();
                txtQValue.Text = upref.QValue.ToString();
                txtScantime.Text = upref.ScanTime.ToString();
                txtLBTTime.Text = upref.LBTTime.ToString();

            }
            else if (SelPage == tpDDSelectionSettings)
            {


                if (upref.IDInputMethod == 0)
                {
                    rbManual.Checked = true;

                }
                else if (upref.IDInputMethod == 1)
                {
                    rbBarcode.Checked = true;
                }
                else if (upref.IDInputMethod == 2)
                {
                    rbRfid.Checked = true;
                }

                if (upref.DDSelectionOption == 0)
                {
                    rbDropDown.Checked = true;

                }
                else if (upref.DDSelectionOption == 1)
                {
                    rbInstantSearch.Checked = true;
                }

                txtObjectListPageSize.Text = upref.SelectScreen_PageSize.ToString();
            }
        }

        private void txtPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        private void txtObjectListPageSize_TextChanged(object sender, EventArgs e)
        {
            ApplyButton.Enabled = true;
        }

        private void txtObjectListPageSize_KeyPress(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        private void tpaddmode_Click(object sender, EventArgs e)
        {

        }

        private void rdbstandalonemode_CheckedChanged(object sender, EventArgs e)
        {
            pnlstandalonemode.Enabled = rdbstandalonemode.Checked;
        }

       
      
    }
}