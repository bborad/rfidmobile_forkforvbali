using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;

namespace OnRamp
{
    public partial class FactoryDefaultsForm : Form
    {
        public FactoryDefaultsForm()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        private bool ValidateSelections()
        {
            return (RFIDCfgChkBx.Checked == true || SysCfgChkBx.Checked == true
            || DataDirChkBx.Checked == true);
        }

        private void OnRestoreBttnClicked(object sender, EventArgs e)
        {
            if (ValidateSelections() == false)
            {
                Program.ShowWarning("Please select at least one item from the list");
                return;
            }
            // Restore to Factory Default setting
            if (Program.AskUserConfirm("Restore selected settings to factory default.\n Proceed?") == DialogResult.OK)
            {
                Program.RefreshStatusLabel(this, "Restoring settings to factory defaults...");
                this.Enabled = false;

                UserPref Pref = UserPref.GetInstance();
                try
                {
                    if (RFIDCfgChkBx.Checked)
                    {
                        Pref.RestoreDefaultByCategories(UserPref.Category.InventorySetup,
                            UserPref.Category.TagMemory, UserPref.Category.FreqLinkProfAnt,
                            UserPref.Category.OverheatProtect);
                    }
                    if (SysCfgChkBx.Checked)
                    {
                        Pref.RestoreDefaultByCategories(UserPref.Category.ReaderIdentity,
                            UserPref.Category.UserMgmt, UserPref.Category.Sound,
                            UserPref.Category.Diagnostics, UserPref.Category.Network);
                    }
                    Program.ShowSuccess("Factory Setting now in use");
                }
                catch (ApplicationException ae)
                {
                    MessageBox.Show(ae.Message, "Restore Failed");
                }
                Program.RefreshStatusLabel(this, String.Empty);
                this.Enabled = true;
            }
        }

        private void OnCancelBttnClicked(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}