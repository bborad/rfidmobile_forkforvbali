﻿using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmChangeLocation : Form
    {
        public frmChangeLocation()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
        }

        public ListViewItem lvItem;

        public string itemName, tagId, locName, locId;

        public bool locChanged;

        private void btnChangeLoc_Click(object sender, EventArgs e)
        {
            try
            {
                Int32 checkLocationID;

                LocationItem li = (LocationItem)cboLoc.SelectedItem;
                checkLocationID = Convert.ToInt32(li.ID_Location);

                if (checkLocationID <= 0)
                {
                    MessageBox.Show("Please select the new location.");
                    return;
                }
                else if (checkLocationID == Convert.ToInt32(locId))
                {
                    MessageBox.Show("Please select the different location.");
                    return;
                }
                

                string strCsvTag = "'" + tagId + "'";
                int result = Assets.UpdateAssetLocation(strCsvTag, checkLocationID, 0);
                if (result > 0)
                {
                    MessageBox.Show("Location changed successfully.");
                    locChanged = true;
                }
            }
            catch (Exception ex)
            {
                Program.ShowError(ex.Message.ToString());
                Logger.LogError(ex.Message);
            }
            this.Close();
        }

        private void frmChangeLocation_Load(object sender, EventArgs e)
        {
            cboLoc.KeyUp += cboLoc_KeyUp;
            cboLoc.DropDownStyle = ComboBoxStyle.DropDown;
            //  cboLoc.Tag = "";

            //DataTable dtList = new DataTable(); 

            //dtList.Columns.Add("ID_Location");
            //dtList.Columns.Add("Name");

            //DataRow dr = dtList.NewRow();
            //dr["ID_Location"] = 0;
            //dr["Name"] = "Select Location";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges();

            //cboLoc.ValueMember = "ID_Location";
            //cboLoc.DisplayMember = "Name";
            //cboLoc.DataSource = dtList;

            //cboLoc.SelectedValue = 0;

            LocationItem item = new LocationItem("Select Location", "0");
            cboLoc.Items.Add(item);
            cboLoc.SelectedItem = item;


            locChanged = false;

            txtItemName.Text = itemName;

            txtTagId.Text = tagId;
            txtLocName.Text = locName;

            if (tagId == "")
            {
                btnChangeLoc.Enabled = false;
            }
            else
            {
                btnChangeLoc.Enabled = true;
            }

        }

        private void cboLoc_KeyUp(object sender, KeyEventArgs e)
        {
            try
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                    case Keys.Right:
                    case Keys.Tab:
                    case Keys.Up:
                    case Keys.Delete:
                    case Keys.Down:
                    case Keys.ShiftKey:
                    case Keys.Shift:
                        return;
                }

                string typedSoFar;

                if (e.KeyCode == Keys.Back)
                {
                    typedSoFar = Convert.ToString(cboLoc.Tag);

                    if (cboLoc.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboLoc.Text.Trim();
                    }
                    else
                    {
                        return;
                    }

                    //if (typedSoFar.Length > 0 && typedSoFar.Length == cboLoc.Text.Length)
                    //{
                    //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
                    //}
                }
                else
                {
                    typedSoFar = Convert.ToString(cboLoc.Tag);
                    if (cboLoc.Text.Trim() != typedSoFar)
                    {
                        typedSoFar = cboLoc.Text.Trim();
                    }
                    // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
                }

                cboLoc.Tag = typedSoFar;

                DataTable dtList = new DataTable();

                if (typedSoFar.Trim().Length >= 1)
                {
                    cboLoc.DataSource = null;

                    bool setSelection = false;
                    int selValue = 0;

                    try
                    {
                        if (typedSoFar.ToLower() == "all")
                        {
                            dtList = Locations.getLocationList(cboLoc);
                        }
                        else
                        {
                            dtList = Locations.getLocationList(typedSoFar.Trim(), cboLoc);
                        }

                        //dtList = Locations.getLocationList(typedSoFar.Trim());

                        if (dtList != null)
                        {
                            DataRow dr;

                            //dr = dtList.NewRow();
                            //dr["ID_Location"] = 0;
                            //dr["Name"] = "--ALL--";
                            //dtList.Rows.Add(dr);
                            //dtList.AcceptChanges();

                            dr = dtList.NewRow();
                            dr["ID_Location"] = 0;
                            dr["Name"] = "Select Location";
                            dtList.Rows.Add(dr);
                            dtList.AcceptChanges();

                            if (dtList.Rows.Count == 1)
                            {
                                dr = dtList.NewRow();

                                selValue = -2;

                                dr["ID_Location"] = selValue;

                                dr["Name"] = typedSoFar;
                                dtList.Rows.Add(dr);
                                dtList.AcceptChanges();

                                setSelection = true;

                            }

                            cboLoc.ValueMember = "ID_Location";
                            cboLoc.DisplayMember = "Name";
                            cboLoc.DataSource = dtList;

                        }
                        else
                        {

                            LocationItem item = new LocationItem("Select Location", "0");
                            cboLoc.Items.Add(item);
                            if (cboLoc.Items.Count == 1)
                            {
                                selValue = -2;

                                item = new LocationItem(typedSoFar, selValue.ToString());

                                cboLoc.Items.Insert(0, item);

                                setSelection = true;

                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }

                    //'Select the Appended Text

                    Utility.setDropDownHeight(cboLoc.Handle, 20);

                    Utility.setShowDropDown(cboLoc.Handle);

                    if (setSelection)
                    {
                        cboLoc.SelectedValue = selValue;
                    }

                    Utility.setSelectionStart((short)typedSoFar.Length, cboLoc.Handle);

                }
                else
                {
                    //dtList.Columns.Add("ID_Location");
                    //dtList.Columns.Add("Name");

                    //DataRow dr = dtList.NewRow();
                    //dr["ID_Location"] = 0;
                    //dr["Name"] = "Select Location";
                    //dtList.Rows.Add(dr);
                    //dtList.AcceptChanges();

                    //cboLoc.ValueMember = "ID_Location";
                    //cboLoc.DisplayMember = "Name";
                    //cboLoc.DataSource = dtList;

                    //cboLoc.SelectedValue = 0;

                    LocationItem item = new LocationItem("Select Location", "0");
                    cboLoc.Items.Add(item);
                    cboLoc.SelectedItem = item;


                }
            }
            catch
            {
            }
        }

    }
}