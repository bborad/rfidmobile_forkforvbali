/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Add Asset functionality
 **************************************************************************************/
using System;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmAddAsset : Form
    {
        public bool newAsset;

        TransientMsgDlg msgbox = null;

        public enum FormType
        {
            Add,
            OverWiteTag
        };
        private FormType _OpenMode;

        public FormType OpenMode
        {
            get
            {
                return _OpenMode;
            }
            set
            {
                _OpenMode = value;
            }
        }

        private string _OldAssetNo;

        public bool IsDeleting { get; set; }

        public string OldAssetNo
        {
            get { return _OldAssetNo; }
            set { _OldAssetNo = value; }
        }

        public frmAddAsset()
        {
            if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
            {
                InitializeComponent_AT870();
            }
            else
            {
                InitializeComponent();
            }
            newAsset = true;
        }

        private string _LocationID;

        public String LocationID
        {
            get
            {
                return _LocationID;
            }
            set
            {
                _LocationID = value;
                //cboLoc.SelectedValue = Convert.ToInt32(value);
            }
        }

        private string _Description;

        public String Description
        {
            get
            {
                return _Description;
            }
            set
            {
                _Description = value;
                txtdesc.Text = value;
            }
        }

        public String AssetName
        {
            set
            {
                txtName.Text = value;
            }
        }

        public String RefNo
        {
            set
            {
                txtRefNo.Text = value;
            }
        }

        public Int32 ServerKey { get; set; }


        public String TagNo
        {
            set
            {
                txtTag.Text = value;
            }
        }
        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (!(txtTag.Text.Trim().Length <= 24 && txtTag.Text.Trim().Length >= 0))
            {
                MessageBox.Show("Invalid Tag No.");
                return;
            }

           // LocationItem li = (LocationItem)cboLoc.SelectedItem;Convert.ToInt32(li.ID_Location);
            int locationID = Convert.ToInt32(_LocationID);
            
            if (locationID == 0 || locationID == -2)
            {
                MessageBox.Show("Please Select Location");
                return;
            }

            try
            {
                if (newAsset == true)
                {
                    Assets.AddAsset(txtTag.Text.Trim(), "", txtName.Text.Trim(), locationID, txtdesc.Text.Trim(), txtRefNo.Text.Trim());
                }
                else if (IsDeleting)
                {
                    //Delete
                    Assets.DeleteAsset(ServerKey);
                }
                else
                {
                    Assets.EditAsset(txtTag.Text.Trim(), OldAssetNo, txtName.Text.Trim(), locationID, ServerKey, txtdesc.Text.Trim(), txtRefNo.Text.Trim());
                }

                MessageBox.Show("Item Sucessfully " + ((IsDeleting) ? "Deleted." : "Saved."));
                this.Close();
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message);
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");

                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {

                Logger.LogError(ep.Message);
                MessageBox.Show(ep.Message.ToString());
            }

        }

        private void cboLoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmAddAsset_Load(object sender, EventArgs e)
        {
            //Set Location Combo
            //cboLoc.KeyUp += cboLoc_KeyUp;
            //cboLoc.DropDownStyle = ComboBoxStyle.DropDown;
            //  cboLoc.Tag = "";

            //DataTable dtList = new DataTable();
            

            //dtList.Columns.Add("ID_Location");
            //dtList.Columns.Add("Name");

            //DataRow dr = dtList.NewRow();
            //dr["ID_location"] = 0;
            //dr["Name"] = "Select Location";
            //dtList.Rows.Add(dr);
            //dtList.AcceptChanges();

            //cboLoc.ValueMember = "ID_Location";
            //cboLoc.DisplayMember = "Name";
            //cboLoc.DataSource = dtList;

            //cboLoc.SelectedValue = 0;

            //LocationItem item = new LocationItem("Select Location", "0");
            //cboLoc.Items.Add(item);
            //cboLoc.SelectedItem = item;
            this.Text = this.Text + " - " + UserPref.CurVersionNo;

            txtName.MaxLength = 100; 

            //DataTable dtStatus = AssetStatus.GetAssetStatus();

            //cbAssetStatus.ValueMember = "ID_AssetStatus";
            //cbAssetStatus.DisplayMember = "Status";
            //cbAssetStatus.DataSource = dtStatus;

            if (newAsset == false)
            {
                txtTag.Enabled = false;

                int locID = Convert.ToInt32(LocationID);

                Locations l = new Locations(locID);

                lknLocName.Text = l.Name;

                //dr = dtList.NewRow();
                //dr["ID_location"] = locID;
                //dr["Name"] = l.Name;
                //dtList.Rows.Add(dr);
                //dtList.AcceptChanges();

                //cboLoc.DataSource = dtList;
                //cboLoc.SelectedValue = locID;

                //item = new LocationItem(l.Name, locID.ToString());
                //cboLoc.Items.Insert(0,item);
                // cboLoc.SelectedItem = item;
                txtdesc.Text = Description;
                //cbAssetStatus.SelectedValue = Convert.ToInt32(AssetStatusID);
            }
            else
            {
                LocationID = "0";
                lknLocName.Text = "Select Location";
            }

            lknLocName.Tag = lknLocName.Text;

            if (lknLocName.Text.Length > 25)
            {
                lknLocName.Text = lknLocName.Text.Substring(0, 23);
                lknLocName.Text = lknLocName.Text + "..";
            }

            if (IsDeleting)
            {
                btnSave.Text = "Delete";
                txtTag.Enabled = false;
                txtName.Enabled = false;
                txtRefNo.Enabled = false;
                txtdesc.Enabled = false;
                btnSelLoc.Enabled = false;
            }
           

        } 
    

        private void btnSelLoc_Click(object sender, EventArgs e)
        {
            string locName;
            frmDDSelection frmSel = new frmDDSelection(SelectionObjectType.Locations, lknLocName.Tag.ToString(), Convert.ToInt32(_LocationID), false);
            frmSel.ShowDialog();

            if (frmSel.objectID != -99999)
            {
                locName = frmSel.objectName;

                _LocationID = Convert.ToString(frmSel.objectID);
            }
            else
            {
                return;

            } 

            if (locName.Length > 25)
            {
                lknLocName.Text = locName.Substring(0, 23);
                lknLocName.Text = lknLocName.Text + "..";
            }
            else
            {
                lknLocName.Text = locName;
            }
            lknLocName.Tag = locName;
        }

        private void lknLocName_Click(object sender, EventArgs e)
        {
            try
            {
                ShowMessage(lknLocName.Tag.ToString());
            }
            catch
            {
            }
        }

        // TransientMsgDlg msgbox = null;

        void ShowMessage(string message)
        {
            try
            {
                if (msgbox != null)
                {
                    msgbox.Close();
                    //msgbox = null;
                }
                else
                {

                }

                msgbox = new TransientMsgDlg(0);

                msgbox.Closed += new EventHandler(msgbox_Closed);

                int dispLength;

                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    dispLength = 23;
                    msgbox.Size = new Size(210, 110);
                }
                else
                {
                    dispLength = 30;
                    msgbox.Height = 110;
                }

                if (message.Trim().Length > dispLength)
                {
                    char[] splitParams = new char[1];
                    splitParams[0] = ' ';
                    string[] msgSplit = message.Split(splitParams);

                    int end = 0;

                    if (msgSplit != null && msgSplit.Length > 0)
                    {
                        StringBuilder strValue = new StringBuilder();

                        foreach (string value in msgSplit)
                        {
                            end = strValue.Length;
                            strValue.Append(value);

                            if (value.Length > dispLength)
                            {
                                try
                                {
                                    strValue.Insert(end + dispLength, " ");
                                    end = dispLength + 1;
                                    //strValue.Append(value.Insert(dispLength, " "));
                                    if (value.Substring(dispLength).Length > dispLength)
                                    {
                                        //strValue.Append(value.Insert(dispLength * 2, " "));
                                        strValue.Insert(end + dispLength, " ");
                                    }
                                }
                                catch
                                {
                                }
                            }
                            //else
                            //{
                            //    strValue.Append(value);
                            //}

                            strValue.Append(" ");
                        }

                        message = strValue.ToString().Trim();
                    }

                }

                msgbox.MsgHAlign = ContentAlignment.TopLeft;
                msgbox.SetTimeout(3);
                msgbox.TopMost = true;
                msgbox.AllowUserClose = true;
                msgbox.SetDpyMsg(message, "Full Text");

                msgbox.AutoScroll = false;

                msgbox.Show();
            }
            catch
            {
            }
        }

        void msgbox_Closed(object sender, EventArgs e)
        {
            try
            {
                msgbox = null;

            }
            catch
            {
            }
        }

        private void panel1_GotFocus(object sender, EventArgs e)
        {

        }

        //private void cboLoc_KeyUp(object sender, KeyEventArgs e)
        //{
        //    try
        //    {
        //        switch (e.KeyCode)
        //        {
        //            case Keys.Left:
        //            case Keys.Right:
        //            case Keys.Tab:
        //            case Keys.Up:
        //            case Keys.Delete:
        //            case Keys.Down:
        //            case Keys.ShiftKey:
        //            case Keys.Shift:
        //                return;
        //        }

        //        string typedSoFar;

        //        if (e.KeyCode == Keys.Back)
        //        {
        //            typedSoFar = Convert.ToString(cboLoc.Tag);

        //            if (cboLoc.Text.Trim() != typedSoFar)
        //            {
        //                typedSoFar = cboLoc.Text.Trim();
        //            }
        //            else
        //            {
        //                return;
        //            }

        //            //if (typedSoFar.Length > 0 && typedSoFar.Length == cboLoc.Text.Length)
        //            //{
        //            //    typedSoFar = typedSoFar.Remove(typedSoFar.Length - 1, 1);
        //            //}
        //        }
        //        else
        //        {
        //            typedSoFar = Convert.ToString(cboLoc.Tag);
        //            if (cboLoc.Text.Trim() != typedSoFar)
        //            {
        //                typedSoFar = cboLoc.Text.Trim();
        //            }


        //            // typedSoFar = typedSoFar + Convert.ToChar(e.KeyValue);
        //        }

        //        cboLoc.Tag = typedSoFar;

        //        DataTable dtList = new DataTable();

        //        if (typedSoFar.Trim().Length >= 1)
        //        {
        //            bool setSelection = false;
        //            int selValue = 0;

        //            try
        //            {
        //                if (typedSoFar.ToLower() == "all")
        //                {
        //                    dtList = Locations.getLocationList(cboLoc);
        //                }
        //                else
        //                {
        //                    dtList = Locations.getLocationList(typedSoFar.Trim(), cboLoc);
        //                }

        //                //dtList = Locations.getLocationList(typedSoFar.Trim());

        //                if (dtList != null)
        //                {
        //                    DataRow dr;

        //                    dr = dtList.NewRow();
        //                    dr["ID_Location"] = 0;
        //                    dr["Name"] = "Select Location";
        //                    dtList.Rows.Add(dr);
        //                    dtList.AcceptChanges();

        //                    if (dtList.Rows.Count == 1)
        //                    {
        //                        dr = dtList.NewRow();

        //                        selValue = -2;

        //                        dr["ID_Location"] = selValue;

        //                        dr["Name"] = typedSoFar;
        //                        dtList.Rows.Add(dr);
        //                        dtList.AcceptChanges();

        //                        setSelection = true;

        //                    }

        //                    cboLoc.ValueMember = "ID_Location";
        //                    cboLoc.DisplayMember = "Name";
        //                    cboLoc.DataSource = dtList;

        //                }
        //                else
        //                {

        //                    LocationItem item = new LocationItem("Select Location", "0");
        //                    cboLoc.Items.Add(item);
        //                    if (cboLoc.Items.Count == 1)
        //                    {
        //                        selValue = -2;

        //                        item = new LocationItem(typedSoFar, selValue.ToString());

        //                        cboLoc.Items.Insert(0, item);

        //                        setSelection = true;

        //                    }
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //            }

        //            //'Select the Appended Text

        //            Utility.setDropDownHeight(cboLoc.Handle, 20);

        //            Utility.setShowDropDown(cboLoc.Handle);

        //            if (setSelection)
        //            {
        //                cboLoc.SelectedValue = selValue;
        //            }

        //            Utility.setSelectionStart((short)typedSoFar.Length, cboLoc.Handle);

        //        }
        //        else
        //        {
        //            //dtList.Columns.Add("ID_Location");
        //            //dtList.Columns.Add("Name");

        //            //DataRow dr = dtList.NewRow();
        //            //dr["ID_Location"] = 0;
        //            //dr["Name"] = "Select Location";
        //            //dtList.Rows.Add(dr);
        //            //dtList.AcceptChanges();

        //            //cboLoc.ValueMember = "ID_Location";
        //            //cboLoc.DisplayMember = "Name";
        //            //cboLoc.DataSource = dtList;

        //            //cboLoc.SelectedValue = 0;

        //            LocationItem item = new LocationItem("Select Location", "0");
        //            cboLoc.Items.Add(item);
        //            cboLoc.SelectedItem = item;

        //        }
        //    }
        //    catch
        //    {
        //    }
        //}

    }
}