extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;
using System.Data;
using System.Data.SqlServerCe;
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

using ClsLibBKLogs;
using System.Threading;

namespace ClsRampdb
{
    public class CEConn : IDisposable
    {
        #region "Varibles"

        public static String _dbFilePath;

        public static String dbFilePath
        {
            get
            {
                UserPref Pref = UserPref.GetInstance();
                if (Pref.UsingNewDBLib)
                {
                    _dbFilePath = NewLib.CEConn.dbFilePath;
                }
                else
                {
                    _dbFilePath = OldLib.CEConn.dbFilePath;
                }

                return _dbFilePath;
            }
            set
            {
                _dbFilePath = value;

                NewLib.CEConn.dbFilePath = value;
                OldLib.CEConn.dbFilePath = value;

            }
        }

        public static DataSet dsInMemeoryData = new DataSet();

        NewLib.CEConn newLibCEConn;
        OldLib.CEConn oldLibCEConn;

        // //private  SqlCeConnection  _CEConnection;
        //  private SqlCeTransaction _CETransaction;

        // private static SqlCeConnection _CEConnection;
        //// private static SqlCeTransaction _CETransaction;

        // //private String _CEConnStr;

        // #region "Properties"
        // public SqlCeConnection CEConnection
        // {
        //     get
        //     {
        //         return _CEConnection; 
        //     }
        // }
        // #endregion
        #endregion

        public static int StatusDataLoaded;
        public static Thread tLoadData;
        public CEConn()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {

                    newLibCEConn = new NewLib.CEConn();
                }
            }
            else
            {
                if (oldLibCEConn == null)
                {

                    oldLibCEConn = new OldLib.CEConn();
                }
            }
        }

        public int runQuery(string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.runQuery(sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.runQuery(sqlText);
            }
        }

        public int runQueryWithoutTransaction(string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.runQueryWithoutTransaction(sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.runQueryWithoutTransaction(sqlText);
            }
        }

        public void runQuery(string spName, SqlCeParameter[] paramAr)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                newLibCEConn.runQuery(spName, paramAr);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                oldLibCEConn.runQuery(spName, paramAr);
            }
        }

        public void FillDataTable(ref DataTable dataTbl, string spName, SqlCeParameter[] paramAr)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                newLibCEConn.FillDataTable(ref   dataTbl, spName, paramAr);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                oldLibCEConn.FillDataTable(ref   dataTbl, spName, paramAr);
            }
        }

        public void FillDataTable(ref DataTable dataTbl, string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                newLibCEConn.FillDataTable(ref   dataTbl, sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                oldLibCEConn.FillDataTable(ref   dataTbl, sqlText);
            }

        }

        public SqlCeDataReader getReader(string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.getReader(sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.getReader(sqlText);
            }
        }

        public object getScalerValue(string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.getScalerValue(sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.getScalerValue(sqlText);
            }
        }

        public SqlCeDataReader getReader(string spName, SqlCeParameter[] paramAr)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.getReader(spName, paramAr);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.getReader(spName, paramAr);
            }
        }

        public object getScalerValue(string spName, SqlCeParameter[] paramAr)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.getScalerValue(spName, paramAr);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.getScalerValue(spName, paramAr);
            }
        }

        public void commitTransaction()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                newLibCEConn.commitTransaction();
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                oldLibCEConn.commitTransaction();
            }
        }

        public void rollbackTransaction()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                newLibCEConn.rollbackTransaction();
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                oldLibCEConn.rollbackTransaction();
            }
        }

        public void Dispose()
        {

        }

        public DataTable GetGroups(string sqlText)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                if (newLibCEConn == null)
                {
                    newLibCEConn = new NewLib.CEConn();
                }

                return newLibCEConn.GetGroups(sqlText);
            }
            else
            {
                if (oldLibCEConn == null)
                {
                    oldLibCEConn = new OldLib.CEConn();
                }
                return oldLibCEConn.GetGroups(sqlText);
            }
        }

        public static int StartLoadingData()
        {
            if (StatusDataLoaded == 1)
            {
                // load in progress
                return StatusDataLoaded;
            }

            String[] tblArr = { "reasons", "employees", "locations", "fs_templates", "fieldservice", "tasks", "menu_options", "asset_groups", "master_securitygroups", "authority", "asset_status", "asset_types" }; //, "asset_status", "asset_types"
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.CEConn.StartLoadingData();
            }
            else
            {
                return 0;
            }
        }

        public static void LoadDataInToDataset(String[] tblArr)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.CEConn.LoadDataInToDataset(tblArr);
            }



        }

    }
}
