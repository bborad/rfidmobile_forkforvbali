
extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;
using System.Threading;
using System.Windows.Forms;

namespace ClsRampdb
{
    public class Locations
    {
        #region "variables & Procedures."
        String _TagNo;
        String _Name;
        Int32 _RowStatus;
        Int32 _ServerKey;
        string _LocNo;

        public Int32 ServerKey
        {
            get
            {
                return _ServerKey;
            }
        }

        public String TagNo
        {
            get
            {
                return _TagNo;
            }
        }

        public String Name
        {
            get
            {
                return _Name;
            }
        }

        public string LocNo
        {
            get
            {
                return _LocNo;
            }
        }

        public RowStatus Status
        {
            get
            {
                return (RowStatus)_RowStatus;
            }
        }

        #endregion

        public static String TableName = "locations";

        public Locations(Int32 LocID)
        {

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations loc = new NewLib.Locations(LocID);
                _ServerKey = LocID;
                _TagNo = loc.TagNo;
                _Name = loc.Name;
                _RowStatus = Convert.ToInt32(loc.Status);

                _LocNo = loc.LocNo;
            }
            else
            {

                OldLib.Locations loc = new OldLib.Locations(LocID);
                _ServerKey = LocID;
                _TagNo = loc.TagNo;
                _Name = loc.Name;
                _RowStatus = Convert.ToInt32(loc.Status);

                _LocNo = loc.LocNo;
            }


        }

        public Locations(String TagID)
        {

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations loc = new NewLib.Locations(TagID);
                _ServerKey = loc.ServerKey;
                _TagNo = loc.TagNo;
                _Name = loc.Name;
                _RowStatus = Convert.ToInt32(loc.Status);

                _LocNo = loc.LocNo;
            }
            else
            {

                OldLib.Locations loc = new OldLib.Locations(TagID);
                _ServerKey = loc.ServerKey;
                _TagNo = loc.TagNo;
                _Name = loc.Name;
                _RowStatus = Convert.ToInt32(loc.Status);

                _LocNo = loc.LocNo;
            }

        }

        public static DataTable getLocationList()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Locations.getLocationList();

            }
            else
            {

                return OldLib.Locations.getLocationList();

            }
        }

        public static DataTable getLocationList(ComboBox cb)
        {
            UserPref Pref = UserPref.GetInstance();
            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Locations.getLocationList(cb);

                }
                else
                {

                    return OldLib.Locations.getLocationList(cb);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while fetching the data - " + ex.Message);
            }
        }

        public static DataTable getLocationList(string searchString)
        {
            UserPref Pref = UserPref.GetInstance();
            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Locations.getLocationList(searchString);

                }
                else
                {

                    return OldLib.Locations.getLocationList(searchString);

                }
            }
              
                catch (Exception ex)
                {
                    throw new Exception("Error while fetching the data - " + ex.Message);
                }
        }

        public static DataTable getLocationList(string searchString, ComboBox cb)
        {
            UserPref Pref = UserPref.GetInstance();
            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Locations.getLocationList(searchString, cb);

                }
                else
                {

                    return OldLib.Locations.getLocationList(searchString, cb);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while fetching the data - " + ex.Message);
            }
        }

        public static object getLocationList(ListView lv)
        {
            UserPref Pref = UserPref.GetInstance();
            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Locations.getLocationList(lv);

                }
                else
                {

                    return OldLib.Locations.getLocationList(lv);

                }
            }
              
                catch (Exception ex)
                {
                    throw new Exception("Error while fetching the data - " + ex.Message);
                }
        }

        public static object getLocationList(string searchString, bool serachForTagID, ListView lv)
        {
            UserPref Pref = UserPref.GetInstance();

            try
            {
                if (Pref.UsingNewDBLib)
                {
                    return NewLib.Locations.getLocationList(searchString, serachForTagID, lv);

                }
                else
                {

                    return OldLib.Locations.getLocationList(searchString, serachForTagID, lv);

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error while fetching the data - " + ex.Message);
            }
        }

        public void AssignNewTag(String TagNo)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations loc = new NewLib.Locations(_TagNo);
                loc.AssignNewTag(TagNo);
            }
            else
            {

                OldLib.Locations loc = new OldLib.Locations(_TagNo);
                loc.AssignNewTag(TagNo);
            }
        }

        public static void AddLocation(String TagID, String LocationNo, String LocationName)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations.AddLocation(TagID, LocationNo, LocationName);

            }
            else
            {

                OldLib.Locations.AddLocation(TagID, LocationNo, LocationName);

            }
        }

        public static void RunQuery(object sqlQueryText)
        {
            try
            {
                using (CEConn localDB = new CEConn())
                {
                    localDB.runQuery(sqlQueryText.ToString());
                }
            }
            catch
            {
            }
        }

        public static void EditLocation(String TagID, String LocationNo, String LocationName, Int32 ServerKey)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations.EditLocation(TagID, LocationNo, LocationName, ServerKey);

            }
            else
            {

                OldLib.Locations.EditLocation(TagID, LocationNo, LocationName, ServerKey);

            }
        }

        public static void EditLocation(String TagID, String LocationNo, String LocationName, Int32 ServerKey, int usageCount)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Locations.EditLocation(TagID, LocationNo, LocationName, ServerKey,usageCount);

            }
            else
            {

                OldLib.Locations.EditLocation(TagID, LocationNo, LocationName, ServerKey,usageCount);

            }
        }

        public static void DeleteLocation(Int32 serverKey)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
               //add method to delete location
                throw new Exception();

            }
            else
            {

                OldLib.Locations.DeleteLocation( serverKey);

            }
        }

        private static Int32 minServerKey()
        {

            DataTable dt = CEConn.dsInMemeoryData.Tables[TableName];

            object value = dt.Compute("Min(ServerKey)", "");

            int serverKey = -1;

            if (value != null)
            {
                serverKey = serverKey - Convert.ToInt32(value);
            }
            return serverKey;

            //using (CEConn localDB = new CEConn())
            //{
            //    string strSql;
            //    strSql = "select min(serverKey) from Locations";
            //    object o;
            //    o = localDB.getScalerValue(strSql);
            //    if (DBNull.Value == o)
            //        return -1;
            //    else if (Convert.ToInt32(o) < 0)
            //        return (Convert.ToInt32(o) - 1);
            //    else
            //        return -1;
            //}
        }

        public static DataTable getNewRows()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Locations.getNewRows();

            }
            else
            {

                return OldLib.Locations.getNewRows();
            }

        }

        public static DataTable getModifiedRows()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Locations.getModifiedRows();

            }
            else
            {

                return OldLib.Locations.getModifiedRows();
            }

        }

        public static string getDispatchLocation()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Locations.getDispatchLocation();

            }
            else
            {

                return OldLib.Locations.getDispatchLocation();
            }
        }

        public static string getLocationByName(string locName)
        {

            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Locations.getLocationByName(locName);

            }
            else
            {

                return OldLib.Locations.getLocationByName(locName);
            }
        }
    }

}
