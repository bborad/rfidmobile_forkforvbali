extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;

namespace  ClsRampdb
{
    public class FS_AssetStatus
    {
        #region "variables & Procedures."
        
        Int32 _ID_Tasks;
        Int32 _ID_Asset;
        Int32 _ID_Template;
        DateTime _DueDate;
        Int32 _TaskStatus;
        Int32 _FSStatus;
        Int32 _ID_Employee;
        Int32 _ServerKey;
        Int32 _RowStatus;

        public Int32 ServerKey
        {
            get
            {
                return _ServerKey;
            }
        }


        public Int32 ID_Tasks
        {
            get
            {
                return _ID_Tasks;
            }
        }
        public Int32 ID_Template
        {
            get
            {
                return _ID_Template;
            }
        }
        public DateTime DueDate
        {
            get
            {
                return _DueDate;
            }
        }
        public Int32 TaskStatus
        {
            get
            {
                return _TaskStatus;
            }
        }
        public Int32 FSStatus
        {
            get
            {
                return _FSStatus;
            }
        }
        public Int32 ID_Employee
        {
            get
            {
                return _ID_Employee;
            }
        }


        public Int32 ID_Asset
        {
            get
            {
                return _ID_Asset;
            }
        }

        
        public RowStatus Status
        {
            get
            {
                return (RowStatus)_RowStatus;
            }
        }

        #endregion 

        public static string TableName = "FS_AssetStatus";

        public FS_AssetStatus(Int32 FSID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.FS_AssetStatus fs = new NewLib.FS_AssetStatus(FSID);
                _ServerKey = FSID;
                _ServerKey = FSID;
                _ID_Tasks = fs.ID_Tasks;
                _ID_Asset = fs.ID_Asset;
                _ID_Template = fs.ID_Template;
                _DueDate = fs.DueDate;
                _FSStatus = fs.FSStatus;
                _TaskStatus = fs.TaskStatus;
                _ID_Employee = fs.ID_Employee;
                _RowStatus = Convert.ToInt32(fs.Status);
            }
            else
            {
                OldLib.FS_AssetStatus fs = new OldLib.FS_AssetStatus(FSID);
                _ServerKey = FSID;
                _ID_Tasks = fs.ID_Tasks;
                _ID_Asset = fs.ID_Asset;
                _ID_Template = fs.ID_Template;
                _DueDate = fs.DueDate;
                _FSStatus = fs.FSStatus;
                _TaskStatus = fs.TaskStatus;
                _ID_Employee = fs.ID_Employee;
                _RowStatus = Convert.ToInt32(fs.Status);
            }

        
        }

        public static string Update(Int32 FSID,FSStatusType fst,FSStatusType tst,String Comments)
        {
             UserPref Pref = UserPref.GetInstance();
             if (Pref.UsingNewDBLib)
             {
                 return NewLib.FS_AssetStatus.Update(FSID, fst, tst, Comments);
             }
             else
             {
                 return OldLib.FS_AssetStatus.Update(FSID, fst, tst, Comments);
             }
        }
        

        /* Commented Constructor will uncomment if required to get information by title.
        public Locations(String Title)
        {

            if (!Login.OnLineMode)
            {
                using (CEConn localDB = new CEConn())
                {
                    string strSql;
                    strSql = " select * from Locations where Title='" + Title + "'";
                    SqlCeDataReader dr;
                    dr = localDB.getReader(strSql);
                    while (dr.Read())
                    {
                        _ServerKey = Convert.ToInt32(dr["ServerKey"].ToString().Trim());
                        _Title = (String)dr["Title"];
                        _Name = (String)dr["Name"];
                        _RowStatus = Convert.ToInt32(dr["RowStatus"]);
                    }
                    dr.Close();
                }
            }
            else
            {
                RConnection.RConnection OnConn = new RConnection.RConnection();
                OnConn.Url = Login.webConURL;
                string strSql;
                strSql = " select Title,Name,ID_Location from Locations where Title='" + Title +"'";
                DataTable dt;
                dt = OnConn.getDataTable(strSql);
                if (dt.Rows.Count != 0)
                {
                    _ServerKey = (int)dt.Rows[0]["ID_Location"];
                    _Title = (String)dt.Rows[0]["Title"];
                    _Name = (String)dt.Rows[0]["Name"];
                    _RowStatus = Convert.ToInt32(RowStatus.Synchronized);
                }
                //throw new ApplicationException("Online functionality not implemented yet.");
            }
        }
        */
        
        //This location list generated with relation to FieldService table entries and employee [and due date when online].
        public static DataTable getLocationList()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getLocationList();
            }
            else
            {
                return OldLib.FS_AssetStatus.getLocationList();
            }
        }

        //This Group list generate with relation to FieldService table.
        public static DataTable getGroups(Int32 ParentID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getGroups(  ParentID) ;
            }
            else
            {
                return OldLib.FS_AssetStatus.getGroups(ParentID);
            }
        }

        //This Asset list generate with relation to fieldservice table.
        public static DataTable getAssets(Int64 ID_Location,Int64 ID_AssetGroup,Int64 ID_Asset)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getAssets(ID_Location, ID_AssetGroup, ID_Asset);
            }
            else
            {
                return OldLib.FS_AssetStatus.getAssets(ID_Location, ID_AssetGroup, ID_Asset);
            }

        }

        public static DataTable getRows(RowStatus Stat)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getRows(Stat);
            }
            else
            {
                return OldLib.FS_AssetStatus.getRows(Stat);
            }
        }

        public static DataTable getTemplateList(Int64 ID_Asset)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getTemplateList(  ID_Asset);
            }
            else
            {
                return OldLib.FS_AssetStatus.getTemplateList(ID_Asset);
            }
        }

        public static DataTable getTaskList(Int64 ID_Asset,Int64 ID_Template)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.FS_AssetStatus.getTaskList(  ID_Asset,  ID_Template);
            }
            else
            {
                return OldLib.FS_AssetStatus.getTaskList(  ID_Asset,  ID_Template);
            }
        }



        //public void AssignNewTag(String Title)
        //{
        //    if (!Login.OnLineMode)
        //    {
        //        using (CEConn localDB = new CEConn())
        //        {
        //            string strSql;
        //            if (_RowStatus != Convert.ToInt32(RowStatus.New))
        //            {
        //                _RowStatus = Convert.ToInt32(RowStatus.TagWrite);
        //            }
        //            strSql = " update Locations set Title='" + Title + "', Date_Modified=getDate(), ModifiedBy=" + Login.ID + ", RowStatus=" + _RowStatus + " where ServerKey=" + _ServerKey;
        //            localDB.runQuery(strSql);
        //            _Title = Title;
        //        }
        //    }
        //    else
        //    {
        //        throw new ApplicationException("Online Location functionality not implemented yet.");
        //    }
        //}

        //public static void AddLocation(String Title, String LocationNo, String LocationName)
        //{
        //    if (!Login.OnLineMode)
        //    {
        //        Int32 Skey;
        //        Skey = minServerKey();
        //        using (CEConn localDB = new CEConn())
        //        {
        //            string strSql;
        //            strSql = " select count(*) from Assets A,Employees E,Locations L where A.Title='" + TagID + "' or E.Title='" + Title + "' or L.Title='" + Title + "'";
        //            //select count(*) from Locations where Title='" + Title + "'";
        //            if (Convert.ToInt32(localDB.getScalerValue(strSql)) > 0)
        //                throw new ApplicationException("Duplicate Tag ID.");

        //            strSql = " insert into Locations(Title,Name,LocationNo,Date_Modified,ModifiedBy,RowStatus,serverKey)";
        //            strSql += " values('" + Title + "','" + LocationName.Replace("'", "''") + "','" + LocationNo.Replace("'", "''") + "',getDate()," + Login.ID + "," + Convert.ToInt32(RowStatus.New) + "," + Skey + ")";
        //            localDB.runQuery(strSql);
        //        }
        //    }
        //    else
        //    {
        //        DataTable dtLocation = new DataTable("dtLocation");

        //        dtLocation.Columns.Add("Title", typeof(String));
        //        dtLocation.Columns.Add("Name", typeof(String));
        //        dtLocation.Columns.Add("LocationNo", typeof(String));
        //        dtLocation.Columns.Add("Date_Modified", typeof(DateTime));
        //        dtLocation.Columns.Add("ModifiedBy", typeof(Int32));
        //        dtLocation.AcceptChanges();

        //        DataRow dr;
        //        dr = dtLocation.NewRow();

        //        dr["Title"] = Title;
        //        dr["Name"] = LocationName;
        //        dr["LocationNo"] = LocationNo;
        //        dr["Date_Modified"] = DateTime.Now;
        //        dr["ModifiedBy"] = Login.ID;

        //        dtLocation.Rows.Add(dr);
        //        dtLocation.AcceptChanges();

        //        Synchronize.Synchronise OnConn = new Synchronize.Synchronise();
        //        OnConn.Url = Login.webURL;
        //        DataTable dtResult;
        //        dtResult = OnConn.AddLocation(dtLocation);

        //        if (dtResult.Rows.Count != 0)
        //        {
        //            if (Convert.ToInt32(dtResult.Rows[0]["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //            {
        //                throw new ApplicationException("Insert Failed.");
        //            }
        //            else
        //            {
        //                //Inserted Sucessfully
        //            }
        //        }
        //        //throw new ApplicationException("Online functionality of Location not implemented yet.");
        //    }
        //}

        //private static Int32 minServerKey()
        //{
        //    using (CEConn localDB = new CEConn())
        //    {
        //        string strSql;
        //        strSql = "select min(serverKey) from Locations";
        //        object o;
        //        o = localDB.getScalerValue(strSql);
        //        if (DBNull.Value == o)
        //            return -1;
        //        else if (Convert.ToInt32(o) < 0)
        //            return (Convert.ToInt32(o) - 1);
        //        else
        //            return -1;
        //    }
        //}

        //public static DataTable getNewRows()
        //{
        //    using (CEConn localDB = new CEConn())
        //    {
        //        DataTable dtLocation = new DataTable("dtLocation");
        //        string strSql;


        //        strSql = "select * from Locations where RowStatus=" + Convert.ToInt32(RowStatus.New);
        //        localDB.FillDataTable(ref dtLocation, strSql);

        //        //strSql = "update Locations set RowStatus=" + Convert.ToInt32(RowStatus.InProcess) + " where RowStatus=" + Convert.ToInt32(RowStatus.New);
        //        //localDB.runQuery(strSql);

        //        return dtLocation;
        //    }

        //}

        //public static string getDispatchLocation()
        //{
        //    string strSql;
        //    if (Login.OnLineMode)
        //    {
        //        RConnection.RConnection OnConn = new RConnection.RConnection();
        //        OnConn.Url = Login.webConURL;
       
        //        strSql = " select isNULL(ID_Location,'') from Locations where Name like '%dispatch%' and is_deleted=0 and is_active=1";
        //        return OnConn.getScalerValue(strSql);
        //    }
        //    else
        //    {
        //        using (CEConn localDB = new CEConn())
        //        {
        //            strSql = " select ServerKey as ID_Location from Locations where Name like '%dispatch%' and ServerKey<>0 ";
        //            return Convert.ToString(localDB.getScalerValue(strSql));
        //        }
        //    }
        //}
    }
}
