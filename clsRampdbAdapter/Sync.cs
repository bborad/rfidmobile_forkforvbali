extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsRampdb;
 
using System.Collections;
using ClsLibBKLogs;
using System.Threading;

namespace ClsRampdb
{
    public class Sync
    {

        public static bool IsSyncCompleted = true;

        public static Thread tSync;

        public String strError = "";

        #region Variable Declaration
        String[] tblArr = { "reasons", "employees", "locations", "fs_templates", "fieldservice", "tasks", "assets", "menu_options", "asset_groups", "master_securitygroups", "authority", "asset_status" }; //, "inventory" , "FS_AssetStatus"
        //Synchronize.Synchronise syn;
        public Sync()
        {
            //syn = new Synchronize.Synchronise();
            //syn.Url = Login.webURL;
        }

        #endregion

        #region Methods

        /// <summary>
        ///Sync Inventory 
        /// </summary>
        /// <returns></returns>
        /// 

        public DataTable SyncInventory()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Sync objSync = new NewLib.Sync();
                return objSync.SyncInventory();
            }
            else
            {
                OldLib.Sync objSync = new OldLib.Sync();
                return objSync.SyncInventory();
            }
        }

        /// <summary>
        /// Synchronise All Tables
        /// </summary>
        /// <returns></returns>   
        //public string _SyncTables()
        //{
        //    ProgressStatus.minVal = 10;
        //    ProgressStatus.maxVal = 100;
        //    //System.Threading.Thread pr = new System.Threading.Thread(ProgressStatus.autoStart);
        //    //pr.Start();
        //    string strSql;
        //    //D 'ProgressStatus.pauseAutoIncrement(); 

        //    //Call of the tables one by one in the order Master to Detail
        //    //Update other table if Sync table id has some reference at it's detail table
        //    //Call detail table sync
        //    //Send Only new rows and return their ID 
        //    //...........perform New Operation................
        //    //Employee Table
        //    //Modify Employee table with Synchronized Status

        //    DataTable dtEmployee = new DataTable("newemployee");
        //    DataTable dtLocation = new DataTable("newlocation");
        //    DataTable dtAssets = new DataTable("newassets");
        //    DataTable dtFS = new DataTable("newfs");
        //    DataTable dtResult = new DataTable("result");

        //    #region "New Employee Add"
        //    ProgressStatus.increment();
        //    dtEmployee = Employee.getNewRows();
        //    ProgressStatus.increment();
        //    if (dtEmployee.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.AddEmployee(dtEmployee);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            //DataRow[] errRow;
        //            //errRow = dtResult.Select("RowStatus=" + Convert.ToInt32(RowStatus.Error));    
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtEmployee.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                        strError += "Employee " + drArr[0]["Name"].ToString() + "not added.\r";
        //                    strSql = "update Employees set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    strSql = "update Employees set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                    //for location
        //                    strSql = "update Locations set ModifiedBy=" + Convert.ToInt32(dr["PKey"]) + " where ModifiedBy=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                    //For Assets
        //                    strSql = "update Assets set ModifiedBy=" + Convert.ToInt32(dr["PKey"]) + " where ModifiedBy=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                    //For Inventory
        //                    strSql = "update inventory set ModifiedBy=" + Convert.ToInt32(dr["PKey"]) + " where ModifiedBy=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                }
        //            }
        //            strSql = "delete from Employees where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);

        //        }
        //    }
        //    #endregion

        //    #region "New Location Add"


        //    dtLocation = Locations.getNewRows();
        //    if (dtLocation.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.AddLocation(dtLocation);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            //DataRow[] errRow;
        //            //errRow = dtResult.Select("RowStatus=" + Convert.ToInt32(RowStatus.Error));    
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtLocation.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                        strError += "Location " + drArr[0]["Name"].ToString() + "not added.\r";

        //                    strSql = "update Locations set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    strSql = "update Locations set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                    //For Assets
        //                    strSql = "update Assets set ID_Location=" + Convert.ToInt32(dr["PKey"]) + " where ID_Location=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                    //For Inventory
        //                    strSql = "update inventory set ID_Location=" + Convert.ToInt32(dr["PKey"]) + " where ID_Location=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //            }
        //            strSql = "delete from Locations where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);

        //        }
        //    }
        //    #endregion

        //    #region "Modified Locations"

        //    dtLocation = null;

        //    dtLocation = Locations.getModifiedRows();
        //    if (dtLocation != null && dtLocation.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        // dtResult = syn.AddLocation(dtLocation);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            //DataRow[] errRow;
        //            //errRow = dtResult.Select("RowStatus=" + Convert.ToInt32(RowStatus.Error));    
        //            foreach (DataRow dr in dtLocation.Rows)
        //            {
        //                ProgressStatus.increment();

        //                try
        //                {
        //                    Locations.EditLocation(Convert.ToString(dr["TagID"]), Convert.ToString(dr["LocationNo"]), Convert.ToString(dr["Name"]), Convert.ToInt32(dr["ServerKey"]));

        //                    strSql = "update Locations set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);

        //                }
        //                catch (Exception ex)
        //                {
        //                    Logger.LogError("Sync Operation - Location Modify " + ex.Message + "\n" + ex.StackTrace);
        //                    strError += "Location " + dr["Name"].ToString() + " not Modified.\r";
        //                    strSql = "update Locations set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }

        //            }

        //            strSql = "delete from Locations where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);

        //        }
        //    }
        //    #endregion

        //    #region "New Asset Add"

        //    dtAssets = Assets.getNewRows();
        //    if (dtAssets.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.AddAssets(dtAssets);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtAssets.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                        strError += "Asset " + drArr[0]["Name"].ToString() + "not added.\r";

        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //            }

        //        }
        //    }
        //    #endregion

        //    #region "Asset Modify"
        //    dtAssets = Assets.getRows(RowStatus.Modify);
        //    if (dtAssets.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.ModifyAssets(dtAssets);
        //        //ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtAssets.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                        strError += "Tag no " + drArr[0]["TagID"].ToString() + "not update.\r";

        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //            }
        //            strSql = "delete from Assets where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);
        //        }
        //    }
        //    #endregion

        //    #region"Tag Write functionality"
        //    dtAssets = Assets.getRows(RowStatus.TagWrite);
        //    if (dtAssets.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.TagWrite(dtAssets);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 

        //        using (CEConn localDB = new CEConn())
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtAssets.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                        strError += "Tag no " + drArr[0]["TagID"].ToString() + "not write.\r";

        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    strSql = "update Assets set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //            }
        //            strSql = "delete from Assets where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);
        //        }
        //    }
        //    #endregion

        //    #region "FieldServicePosting"
        //    dtFS = FS_AssetStatus.getRows(RowStatus.Modify);
        //    if (dtFS.Rows.Count > 0)
        //    {
        //        ProgressStatus.increment();
        //        dtResult = syn.UpdateFS_AssetStatus(dtFS);
        //        using (CEConn localDB = new CEConn())
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                ProgressStatus.increment();
        //                if (Convert.ToInt32(dr["RowStatus"]) == Convert.ToInt32(RowStatus.Error))
        //                {
        //                    DataRow[] drArr;
        //                    drArr = dtFS.Select("ServerKey='" + dr["ServerKey"] + "'");
        //                    if (drArr.Length > 0)
        //                    {
        //                        //Assets ast = new Assets(drArr[0]["TagID"].ToString());
        //                        strError += "FS Task for Item " + drArr[0]["Name"].ToString() + " not updated.\r";
        //                    }

        //                    strSql = "update FS_AssetStatus set RowStatus=" + Convert.ToInt32(RowStatus.Error) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //                else
        //                {
        //                    //Instead of Update just delete field service data.
        //                    //strSql = "update FS_AssetStatus set RowStatus=" + Convert.ToInt32(RowStatus.Synchronized) + ", ServerKey=" + Convert.ToInt32(dr["PKey"]) + " where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    strSql = "delete from FS_AssetStatus where ServerKey=" + Convert.ToInt32(dr["ServerKey"]);
        //                    localDB.runQuery(strSql);
        //                }
        //            }
        //            strSql = "delete from FS_AssetStatus where RowStatus= " + Convert.ToInt32(RowStatus.Error);
        //            localDB.runQuery(strSql);
        //        }
        //    }
        //    #endregion

        //    //Run the query and fetch all the data and OverWrite
        //    DateTime lstSyncedDate;

        //    StringBuilder sqlQueries;
        //    StringBuilder keys;

        //    SqlCeBulkCopyOptions options = new SqlCeBulkCopyOptions();
        //    options = SqlCeBulkCopyOptions.KeepIdentity;
        //    options = options |= SqlCeBulkCopyOptions.KeepNulls;

        //    ArrayList delItems;
        //    int rowsCount = 0;

        //    foreach (String tbname in tblArr)
        //    {
        //        ProgressStatus.increment();
        //        lstSyncedDate = Util.getLastSyncedDate(tbname);
        //        DateTime outDate = lstSyncedDate;
        //        String errMsg;
        //        ProgressStatus.increment();
        //        Int64 ItemLimit = 0;
        //        ItemLimit = Login.ItemLimit - Assets.getNoOfItems();
        //        if (ItemLimit == 0)
        //            ItemLimit = -1;
        //        dtResult = (DataTable)syn.SyncTableswithItemLimit(lstSyncedDate.ToString(), tbname, ItemLimit, out outDate);
        //        //D 'ProgressStatus.pauseAutoIncrement(); 
        //        try
        //        {
        //            if (dtResult != null && dtResult.Rows.Count > 0)
        //            {
        //                // Util.updateTables(tbname, dr, outDate, ref sqlQueries);

        //                DataColumn dc, dcc;
        //                dc = dtResult.Columns["Date_Modified"];
        //                if (dc == null)
        //                {
        //                    dc = new DataColumn("Date_Modified");
        //                    dc.DefaultValue = outDate;
        //                    dtResult.Columns.Add(dc);
        //                }
        //                else
        //                    dc.DefaultValue = outDate;

        //                dcc = dtResult.Columns["RowStatus"];
        //                if (dcc == null)
        //                {
        //                    dcc = new DataColumn("RowStatus", Type.GetType("System.Int16"));
        //                    dcc.DefaultValue = Convert.ToInt16(RowStatus.Synchronized);
        //                    dtResult.Columns.Add(dcc);
        //                }
        //                else
        //                    dcc.DefaultValue = Convert.ToInt32(RowStatus.Synchronized);

        //                dtResult.AcceptChanges();

        //                keys = new StringBuilder("-1");

        //                delItems = new ArrayList();
        //                rowsCount = 0;

        //                //sqlQueries = new StringBuilder();

        //                // check if the table has any rows 

        //                int count = Util.getRecordCount(tbname);

        //                if (count > 0)
        //                {
        //                    foreach (DataRow dr in dtResult.Rows)
        //                    {

        //                        rowsCount++;

        //                        if (rowsCount >= 800)
        //                        {
        //                            delItems.Add(keys.ToString());
        //                            keys = new StringBuilder("-1");
        //                            rowsCount = 0;
        //                        }

        //                        if (dr["ServerKey"] != DBNull.Value)
        //                            keys.Append("," + dr["ServerKey"].ToString());

        //                    }

        //                    if (rowsCount > 0)
        //                        delItems.Add(keys.ToString());

        //                    try
        //                    {
        //                        using (CEConn localDB = new CEConn())
        //                        {
        //                            try
        //                            {
        //                                foreach (string delList in delItems)
        //                                {
        //                                    strSql = "delete from " + tbname + " where serverKey IN (" + delList + ");";
        //                                    localDB.runQuery(strSql);
        //                                    localDB.commitTransaction();
        //                                }
        //                            }
        //                            catch (Exception ex)
        //                            {
        //                                localDB.rollbackTransaction();
        //                                strError += "Error while deleting records in table " + tbname + ".\n";
        //                                Logger.LogError(ex.Message);
        //                            }

        //                        }

        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        // strError += "\n";
        //                        Logger.LogError(ex.Message);
        //                    }

        //                }

        //                try
        //                {
        //                    int totalRows = 0;
        //                    using (CEConn localDB = new CEConn())
        //                    {
        //                        SqlCeBulkCopy bc = new SqlCeBulkCopy(localDB.CEConnection, options);
        //                        bc.DestinationTableName = tbname;
        //                        totalRows = bc.WriteToServer(dtResult, 0, "");

        //                    }
        //                }
        //                catch (Exception ex)
        //                {
        //                    strError += "Error while inserting records in table " + tbname + ".\n";
        //                    Logger.LogError("Error while inserting records in table " + tbname + ".\n" + ex.Message);
        //                }

        //            }
        //        }
        //        catch (System.Web.Services.Protocols.SoapException ex)
        //        {

        //            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
        //                strError += "Request from innvalid IP address.\n";
        //            else
        //                strError += "Soap Exception\n";

        //            Logger.LogError(ex.Message);

        //        }
        //        catch (Exception ep)
        //        {
        //            strError += ep.ToString() + "\n";
        //            Logger.LogError(ep.Message);
        //        }

        //        //Update the Row Status of Table
        //        //Call Web Service
        //        //If got dataset delete all rows of table 
        //        //Insert new Row in table
        //    }

        //    //Sync Inventory is differen process need no i
        //    ProgressStatus.finish();
        //    return "";

        //}

        public string SyncTables()
        {
            strError = "";
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Sync objSync = new NewLib.Sync();
                strError = objSync.SyncTables();
               
            }
            else
            {
                OldLib.Sync objSync = new OldLib.Sync();
                strError = objSync.SyncTables();
            }
            return strError;
        }

        public static void StartSyncFromDStoDB()
        {
            Sync.IsSyncCompleted = false;

             UserPref Pref = UserPref.GetInstance();
             if (Pref.UsingNewDBLib)
             {
                 NewLib.Sync.StartSyncFromDStoDB();
             }
             else
             {
                // OldLib.Sync.StartSyncFromDStoDB();
             }
            Sync.IsSyncCompleted = true;
        }


        /// <summary>
        ///Sync Inventory 
        /// </summary>
        /// <returns></returns>
        /// 

        public DataTable SyncFieldService()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Sync objSync = new NewLib.Sync();
                return objSync.SyncFieldService();
            }
            else
            {
                OldLib.Sync objSync = new OldLib.Sync();
                return objSync.SyncFieldService();
            }
        }

        //Currently Running
        public string SyncFieldServiceData(Int64 locationID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Sync objSync = new NewLib.Sync();
                return objSync.SyncFieldServiceData(locationID);
            }
            else
            {
                OldLib.Sync objSync = new OldLib.Sync();
                return objSync.SyncFieldServiceData(locationID);
            }

        }

        #endregion
    }
}
