extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using ClsLibBKLogs;

namespace ClsRampdb
{
    public class Documents
    {
        #region "Private variables and Properties"
        Int32 _ServerKey;
        Int64 _DocumentID;
        List<Assets> _lstAsset;
        List<double> _lstQty;
        String _DocumentNo;
        String _TagID;
        DateTime _DocumentDate;
        String _Notes;
        int _DocumentTypeID;
        DateTime _DispatchDate;
        Int32 _RowStatus;

        public Int32 ServerKey
        {
            get { return _ServerKey; }
        }
        public Int64 DocumentID
        {
            get { return _DocumentID; }
            set { _DocumentID = value; }
        }


        public List<Assets> LstAsset
        {
            get { return _lstAsset; }
            set { _lstAsset = value; }
        }



        public List<double> LstQty
        {
            get { return _lstQty; }
            set { _lstQty = value; }
        }



        public String DocumentNo
        {
            get { return _DocumentNo; }
            set { _DocumentNo = value; }
        }

        public String TagID
        {
            get { return _TagID; }
            set { _TagID = value; }
        }


        public DateTime DocumentDate
        {
            get { return _DocumentDate; }
            set { _DocumentDate = value; }
        }

        public String Notes
        {
            get { return _Notes; }
            set { _Notes = value; }
        }


        public int DocumentTypeID
        {
            get { return _DocumentTypeID; }
            set { _DocumentTypeID = value; }
        }


        public DateTime DispatchDate
        {
            get { return _DispatchDate; }
            set { _DispatchDate = value; }
        }

        public RowStatus Status
        {
            get { return (RowStatus)_RowStatus; }
        }
        #endregion

        public static String TableName = "documents";

        public Documents(Int32 ServerKey)
        {
            List<string> lstAssetTagID = new List<string>();
            _ServerKey = ServerKey;
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Documents d = new NewLib.Documents(ServerKey);

                _TagID = d.TagID;

                _DocumentNo = d.DocumentNo;

                _DocumentID = d.DocumentID;
                _DocumentDate = d.DocumentDate;

                _Notes = d.Notes;
                _DocumentTypeID = d.DocumentTypeID;
                _DispatchDate = d.DispatchDate;

                _RowStatus = Convert.ToInt32(d.Status);
            }
            else
            {
                OldLib.Documents d = new OldLib.Documents(ServerKey);

                _TagID = d.TagID;

                _DocumentNo = d.DocumentNo;

                _DocumentID = d.DocumentID;
                _DocumentDate = d.DocumentDate;

                _Notes = d.Notes;
                _DocumentTypeID = d.DocumentTypeID;
                _DispatchDate = d.DispatchDate;

                _RowStatus = Convert.ToInt32(d.Status);
            }



        }

        public Documents(String TagID)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                NewLib.Documents d = new NewLib.Documents(TagID);

                _TagID = d.TagID;

                _DocumentNo = d.DocumentNo;

                _DocumentID = d.DocumentID;
                _DocumentDate = d.DocumentDate;

                _Notes = d.Notes;
                _DocumentTypeID = d.DocumentTypeID;
                _DispatchDate = d.DispatchDate;
                _ServerKey = d.ServerKey;
                _RowStatus = Convert.ToInt32(d.Status);
            }
            else
            {
                OldLib.Documents d = new OldLib.Documents(TagID);

                _TagID = d.TagID;

                _DocumentNo = d.DocumentNo;

                _DocumentID = d.DocumentID;
                _DocumentDate = d.DocumentDate;

                _Notes = d.Notes;
                _DocumentTypeID = d.DocumentTypeID;
                _DispatchDate = d.DispatchDate;
                _ServerKey = d.ServerKey;
                _RowStatus = Convert.ToInt32(d.Status);
            }

           
        }



        public static void AddDocument(String TagID,
                        List<string> lstAssetTags, List<float> lstQty, String documentNo,
                        DateTime documentDate, String notes, int documentTypeID,
                        DateTime dispatchDate)
        {
           
              UserPref Pref = UserPref.GetInstance();
              if (Pref.UsingNewDBLib)
              {
                  NewLib.Documents.AddDocument(TagID,
                         lstAssetTags, lstQty, documentNo,
                          documentDate, notes, documentTypeID,
                          dispatchDate);
              }
              else
              {
                  OldLib.Documents.AddDocument(TagID,
                       lstAssetTags, lstQty, documentNo,
                        documentDate, notes, documentTypeID,
                        dispatchDate);
              }

           
        }

        private static Int32 minServerKey()
        {
            using (CEConn localDB = new CEConn())
            {
                string strSql;
                strSql = "select min(serverKey) from Documents";
                object o;
                o = localDB.getScalerValue(strSql);
                if (DBNull.Value == o)
                    return -1;
                else if (Convert.ToInt32(o) < 0)
                    return (Convert.ToInt32(o) - 1);
                else
                    return -1;
            }
        }

        public static DataTable getRows()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Documents.getRows();
            }
            else
            {
                return OldLib.Documents.getRows();
            }
        }
    }
}
