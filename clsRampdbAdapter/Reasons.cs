extern alias RampDBNew;
extern alias RampDB;
using NewLib = RampDBNew::ClsRampdb;
using OldLib = RampDB::ClsRampdb;

using OnRamp;

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
 
using ClsLibBKLogs;

namespace  ClsRampdb
{
    public class Reasons
    {
        public static string TableName = "reasons";

        //public Reasons(Int32 ReasonID)
        //{

        //    if (!Login.OnLineMode)
        //    {
        //        using (CEConn localDB = new CEConn())
        //        {
        //            string strSql;
        //            strSql = " select * from Locations where ServerKey=" + LocID;
        //            SqlCeDataReader dr;
        //            dr = localDB.getReader(strSql);
        //            while (dr.Read())
        //            {
        //                _ServerKey = LocID;
        //                _TagNo = (String)dr["TagID"];
        //                _Name = (String)dr["Name"];
        //                _RowStatus = Convert.ToInt32(dr["RowStatus"]);
        //            }
        //            dr.Close();
        //        }
        //    }
        //    else
        //    {
        //        RConnection.RConnection OnConn = new RConnection.RConnection();
        //        OnConn.Url = Login.webConURL;
        //        string strSql;
        //        strSql = " select TagID,Name from Locations where ID_Location=" + LocID;
        //        DataTable dt;
        //        dt = OnConn.getDataTable(strSql);
        //        if (dt.Rows.Count != 0)
        //        {
        //            _ServerKey = LocID;
        //            _TagNo = (String)dt.Rows[0]["TagID"];
        //            _Name = (String)dt.Rows[0]["Name"];
        //            _RowStatus = Convert.ToInt32(RowStatus.Synchronized);
        //        }
        //        //throw new ApplicationException("Online functionality not implemented yet.");
        //    }
        //}

        public static DataTable getReasonsList()
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.UsingNewDBLib)
            {
                return NewLib.Reasons.getReasonsList();
            }
            else
            {
                return OldLib.Reasons.getReasonsList();
            }
        }
       
    }
}
