﻿using System;
using System.Collections.Generic;

using System.Text;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using OnRamp;
using ClsLibBKLogs;
using System.Threading;

namespace AT870ReaderLib
{
    public class RFID_Reader : Reader
    {
        //**********************************Variables**************************************//
        public static AT_UHF_NET.CUHFHost rfid;
        public static ClsCallBack objCallBack;
        //Util_PowerOn_NET.CUtil_PowerOn PowerNotify;
        public EventToHandle eventToHandle;
       
        public EPCEvent callbackevent;
        public RFID_Reader()
        {
            InstantiateReader();
        }

        void InstantiateReader()
        {
            if (rfid == null)
            {
                rfid = new AT_UHF_NET.CUHFHost();
            }
        }

        //**********************************Properties**************************************//
        #region Property
        // public static ushort macerr;
        public RFID_VERSION DriverVer
        {
            get
            {
                return Rfid.st_RfidSpReq_Startup.LibraryVersion;
            }
        }
        public RFID_VERSION MacVer
        {
            get
            {
                return Rfid.st_RfidSpReq_MacGetVersion.version;
            }
        }
        public override HRESULT_RFID LastErrCode
        {

            get
            {
                return HRESULT_RFID.E_ABORT;
            }


        }
        public override ushort LastMacErrCode
        {
            get
            {
                return 0;
            }

        }

        #endregion
        //*******************************Private Methods************************************//
        #region Private Methods
        /// <summary>
        /// Private Method to Start Reader Operations.
        /// </summary>
        /// <returns>true if Operation opened/false</returns>
        /// 
        private bool StartReader()
        {
            bool result = false;
            try
            {
                // Turn on RFID Module

                if (!rfid.IsOpen())
                {
                    if (rfid.Open())
                    {
                         rfid.SET_ReportMode(AT_UHF_NET.ReportMode.EXTENDED_INFORMATION, true);
                        rfid.ActivatedForm = null;

                        if(objCallBack == null)
                            objCallBack = new ClsCallBack(this);

                        rfid.ActivatedForm = objCallBack;
                        result = true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    //rfid.SET_ReportMode(AT_UHF_NET.ReportMode.EXTENDED_INFORMATION, true);
                    result = true;
                }

              


            }
            catch (Exception ex)
            {
                Logger.LogError("StartReader : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Private Method to Stop the Reader Operation.
        /// </summary>
        private void StopReader()
        {
            try
            {
                //rfid.ActivatedForm = null;
                rfid.Stop();
                // rfid.Close();
                // rfid.PowerOFF(); 
            }
            catch (Exception ex)
            {
                Logger.LogError("StopReader : " + ex.Message);

            }
        }

        

        private bool OpenRadio()
        {
            bool result = false;
            try
            {
                rfid.PowerON();
                rfid.PowerOnInit();

                // Open Port
                if (rfid.Open())
                {   
                    //Logger.LogError("Open Radio Antenna Power  - " + rfid.GET_ControlValue().Power.ToString());

                   rfid.SET_PowerControl(0);
                   rfid.SET_ReportMode(AT_UHF_NET.ReportMode.EXTENDED_INFORMATION, true);
                   rfid.ActivatedForm = null;

                   if (objCallBack == null)
                       objCallBack = new ClsCallBack(this);

                   rfid.ActivatedForm = objCallBack;
                    //rfid.SET_Default();
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("OpenRadio : " + ex.Message);
            }

            return result;
        }

        private bool CloseRadio()
        {
            bool result = false;
            try
            {
                rfid.ActivatedForm = null;
                rfid.Stop();
                rfid.Close();
                rfid.PowerOFF();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("CloseRadio : " + ex.Message);
            }

            return result;
        }

        public override CONTROL_VALUE GetConfiguration()
        {
            CONTROL_VALUE ctrlValue = new CONTROL_VALUE();
           
            try
            {
               
                if (rfid != null)
                {
                    ctrlValue.FirmWareVersion = rfid.FirmwareVersion;
                    AT_UHF_NET.CUHFHost.CONTROL_VALUE cv = rfid.GET_ControlValue();
                   
                    ctrlValue.Buzzer = cv.Buzzer;
                    ctrlValue.CHNumber = cv.CHNumber;
                      if (cv.Hopping == AT_UHF_NET.HOPPING_CODE.EURO_LBT || cv.Hopping == AT_UHF_NET.HOPPING_CODE.JAPAN_LBT)            
                      {
                          ctrlValue.ChState.CH1 = cv.ChState.CH1;
                          ctrlValue.ChState.CH2 = cv.ChState.CH2;
                          ctrlValue.ChState.CH3 = cv.ChState.CH3;
                          ctrlValue.ChState.CH4 = cv.ChState.CH4;
                          ctrlValue.ChState.CH5 = cv.ChState.CH5;
                          ctrlValue.ChState.CH6 = cv.ChState.CH6;
                          ctrlValue.ChState.CH7 = cv.ChState.CH7;
                          ctrlValue.ChState.CH8 = cv.ChState.CH8;
                          ctrlValue.ChState.CH9 = cv.ChState.CH9;
                          ctrlValue.ChState.CH10 = cv.ChState.CH10;
                      }
                      ctrlValue.ContinueMode = cv.ContinueMode;
                      ctrlValue.Hopping = (HOPPING_CODE)cv.Hopping;
                      ctrlValue.HoppingOn = cv.HoppingOn;
                      ctrlValue.LBT_Time = cv.LBT_Time;
                      ctrlValue.Power = cv.Power;
                      ctrlValue.Qvalue = cv.Qvalue;
                      ctrlValue.ScanTime = cv.ScanTime;
                      ctrlValue.SessionValue = cv.SessionValue;
                      ctrlValue.Timeout = cv.Timeout;
                      ctrlValue.Version = cv.Version;
                }
            }
            catch
            {
            }
            return ctrlValue;
        }

        public override void SetConfiguration(CONTROL_VALUE cv)
        {
            try
            {
                if (rfid != null)
                {
                    rfid.SET_QValue(Convert.ToUInt16(cv.Qvalue));
                    rfid.SET_ScanTime(Convert.ToUInt16(cv.ScanTime));
                    rfid.SET_LBT_Time(Convert.ToUInt16(cv.LBT_Time));
                    rfid.SET_Session(Convert.ToUInt16(cv.SessionValue));
                }
            }
            catch
            {
            }
        }

        #endregion

        //*************************************Events***************************************//
        #region  Events
        public override void RegisterDscvrTagEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs = handler;
        }
        public override void UnregisterDscvrTagEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs -= handler;
        }
        public override void RegisterInventoryOpStEvent(EventHandler<InvtryOpEventArgs> handler)
        {
            this.InventoryOpEventArgs = handler;
        }
        public override void UnregisterInventoryOpStEvent(EventHandler<InvtryOpEventArgs> handler)
        {
            this.InventoryOpEventArgs -= handler;
        }
        public override void RegisterLckOpStEvent(EventHandler<LckOpEventArgs> handler)
        {
            this.LockOpEventArgs = handler;
        }
        public override void UnregisterLckOpStEvent(EventHandler<LckOpEventArgs> handler)
        {
            this.LockOpEventArgs -= handler;
        }
        public override void RegisterMemBnkRdEvent(EventHandler<MemBnkRdEventArgs> handler)
        {
            this.MemberBankReadEvenArgs = handler;
        }
        public override void UnregisterMemBnkRdEvent(EventHandler<MemBnkRdEventArgs> handler)
        {
            this.MemberBankReadEvenArgs -= handler;
        }
        public override void RegisterMemBnkWrEvent(EventHandler<MemBnkWrEventArgs> handler)
        {
            this.MemberBankWriteEventArgs = handler;
        }
        public override void UnregisterMemBnkWrEvent(EventHandler<MemBnkWrEventArgs> handler)
        {
            this.MemberBankWriteEventArgs -= handler;
        }
        public override void RegisterRdOpStEvent(EventHandler<RdOpEventArgs> handler)
        {
            this.ReadOpEventArgs = handler;
        }
        public override void UnregisterRdOpStEvent(EventHandler<RdOpEventArgs> handler)
        {
            this.ReadOpEventArgs -= handler;
        }
        public override void RegisterTagPermSetEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs = handler;
        }
        public override void UnregisterTagPermSetEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs -= handler;
        }
        public override void RegisterTagRateEvent(EventHandler<TagRateEventArgs> handler)
        {
            this.TagRateEventArgs = handler;
        }
        public override void UnregisterTagRateEvent(EventHandler<TagRateEventArgs> handler)
        {
            this.TagRateEventArgs -= handler;
        }
        public override void RegisterTagRssiEvent(EventHandler<TagRssiEventArgs> handler)
        {
            this.TagRessiEventArgs = handler;
        }
        public override void UnregisterTagRssiEvent(EventHandler<TagRssiEventArgs> handler)
        {
            this.TagRessiEventArgs -= handler;
        }
        public override void RegisterTempUpdateEvt(EventHandler<TempUpdateEventArgs> handler)
        {
            this.TempUpdateEventArgs = handler;
        }
        public override void UnregisterTempUpdateEvt(EventHandler<TempUpdateEventArgs> handler)
        {
            this.TempUpdateEventArgs -= handler;
        }
        public override void RegisterWrOpStEvent(EventHandler<WrOpEventArgs> handler)
        {
            this.WriteOpEventArgs = handler;
        }
        public override void UnregisterWrOpStEvent(EventHandler<WrOpEventArgs> handler)
        {
            this.WriteOpEventArgs -= handler;
        }
        public override void RegisterRadioStatusNotificationEvent(RadioStatusNotify handler)
        {
            this.RadioStatusNotification = handler;
        }
        public override void UnregisterRadioStatusNotificationEvent(RadioStatusNotify handler)
        {
            this.RadioStatusNotification -= handler;
        }


        public override void RegisterAntPotCfgGetOneNotificationEvent(AntPortCfgGetOneNotify handler)
        {
            this.AntPotCfgGetOneNotification = handler;
        }
        public override void UnregisterAntPotCfgGetOneNotificationEvent(AntPortCfgGetOneNotify handler)
        {
            this.AntPotCfgGetOneNotification -= handler;
        }
        public override void RegisterAntPotCfgSetOneNotificationEvent(AntPortCfgSetOneNotify handler)
        {
            this.AntPotCfgSetOneNotification = handler;
        }
        public override void UnregisterAntPotCfgSetOneNotificationEvent(AntPortCfgSetOneNotify handler)
        {
            this.AntPotCfgSetOneNotification -= handler;
        }
        public override void RegisterCustomFreqGetNotificationEvent(CustomFreqGetNotify handler)
        {
            //this.CustomFreqGetNotification = handler;
        }
        public override void UnregisterCustomFreqGetNotificationEvent(CustomFreqGetNotify handler)
        {
            //this.CustomFreqGetNotification -= handler;
        }
        public override void RegisterThrshTempGetNotificationEvent(ThrshTempGetNotify handler)
        {
            this.ThrshTempGetNotification = handler;
        }
        public override void UnregisterThrshTempGetNotificationEvent(ThrshTempGetNotify handler)
        {
            this.ThrshTempGetNotification -= handler;
        }
        public override void RegisterHealthCheckStatusNotificationEvent(HealthCheckStatusNotify handler)
        {
            this.HealthCheckStatusNotification = handler;
        }
        public override void UnregisterHealthCheckStatusNotificationEvent(HealthCheckStatusNotify handler)
        {
            this.HealthCheckStatusNotification -= handler;
        }
        public override void RegisterLnkProfNumoSetNotificationEvent(LnkProfNumSetNotify handler)
        {
            this.LnkProfNumoSetNotification = handler;
        }
        public override void UnregisterLnkProfNumoSetNotificationEvent(LnkProfNumSetNotify handler)
        {
            this.LnkProfNumoSetNotification -= handler;
        }
        public override void RegisterLnkProfInfoGetNotificationEvent(LnkProfInfoGetNotify handler)
        {
            this.LnkProfInfoGetNotification = handler;
        }
        public override void UnregisterLnkProfInfoGetNotificationEvent(LnkProfInfoGetNotify handler)
        {
            this.LnkProfInfoGetNotification -= handler;
        }

        public override void RegisterTagWriteBankDataEvent(ReqTagWrBnkData handler)
        {
            this.TagWriteBankData = handler;
            //RFIDRdr.GetInstance()
        }
        public override void UnregisterTagWriteBankDataEvent(ReqTagWrBnkData handler)
        {
            this.TagWriteBankData -= handler;
        }
        public override void RegisterTagReadBankDataEvent(ReqTagRdBnkInfo handler)
        {
            this.TagReadBankData = handler;
        }
        public override void UnregisterTagReadBankDataEvent(ReqTagRdBnkInfo handler)
        {
            this.TagReadBankData -= handler;
        }
        public override void RegistereventRespDataModeEvent(RespDatModeSetNotify handler)
        {
            this.eventRespDataMode = handler;
        }
        public override void UnregistereventRespDataModeEvent(RespDatModeSetNotify handler)
        {
            this.eventRespDataMode -= handler;
        }

        #endregion
        //********************************Public Methods************************************//
        #region Inventory
        /// <summary>
        /// Method for start Searching Tags
        /// </summary>
        /// <returns>true/false</returns>
        public override bool TagRangeStart()
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    eventToHandle = EventToHandle.DscvrTagEvent;
                    //rfid.SET_ContinueModeONOff(true);
                    rfid.ReadEPC_Continuous();
                    //  rfid.ReadUID(AT_UHF_NET.UIDREAD_CODE.EPC_GEN2_MULTI_TAG);
                    result = true;

                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagRangeStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for start Inventory
        /// </summary>
        /// <param name="option"></param>
        /// <returns>true/false</returns>
        public override bool TagInventoryStart(int option)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    
                     eventToHandle = EventToHandle.DscvrTagEvent;
                    //rfid.SET_ContinueModeONOff(true);
                    rfid.ReadEPC_Continuous();
                    //rfid.ReadUID(AT_UHF_NET.UIDREAD_CODE.EPC_GEN2_MULTI_TAG);
                    result = true;

                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }

                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryStart : " + ex.Message);
            }
            return result;
        }
      
        public override bool TagInventoryStart(EPCEvent evt)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    callbackevent = evt;
                    //eventToHandle = EventToHandle.DscvrTagEvent;
                    //rfid.SET_ContinueModeONOff(true);
                    
                      rfid.ReadEPC_Continuous();
                    //rfid.ReadUID(AT_UHF_NET.UIDREAD_CODE.EPC_GEN2_MULTI_TAG);
                    result = true;

                    //if (InventoryOpEventArgs != null)
                    //{
                    //    InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    //}

                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for stop Tag Read
        /// </summary>
        /// <returns></returns>
        public override bool TagReadStop()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
                if (ReadOpEventArgs != null)
                {
                    ReadOpEventArgs(new object(), new RdOpEventArgs(RdOpStatus.stopped, "", AccErrorTypes.None));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagReadStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        ///  Method for stop Inventory Rssi tag
        /// </summary>
        /// <returns></returns>
        public override bool TagInvtryRssiStop()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
                if (InventoryOpEventArgs != null)
                {
                    InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.stopped));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInvtryRssiStop  : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        ///  Method for stop Inventory tag
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryAbort()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryAbort : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for stop Inventory tag
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryStop()
        {
            bool result = false;
            try
            {
                StopReader();
                try
                {
                    foreach (Reader.EPCEvent d in callbackevent.GetInvocationList())
                        callbackevent -= d;
                    callbackevent = null;
                }
                catch { }
                result = true;
                if (InventoryOpEventArgs != null)
                {
                    InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.stopped));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for start Inventory tag RdRate.
        /// </summary>
        /// <param name="session"></param>
        /// <param name="search1Only"></param>
        /// <param name="epcBnkMask"></param>
        /// <param name="maskOffset"></param>
        /// <returns>true/false</returns>
        public override bool TagInvtryRdRateStart(RFID_18K6C_INVENTORY_SESSION session, bool search1Only, byte[] epcBnkMask, uint maskOffset)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    string epc;
                    StringBuilder txtBxStr = new StringBuilder(EPCTag.EPCFldSz * 2);
                    // Check ByteArr Size
                    for (int bi = 0; bi < epcBnkMask.Length && bi < EPCTag.EPCFldSz; bi++)
                        txtBxStr.Append(epcBnkMask[bi].ToString("X2"));
                    if (epcBnkMask.Length < EPCTag.EPCFldSz)
                    {
                        int NumBytes2Pad = epcBnkMask.Length & 0x01;
                        if (NumBytes2Pad == 1)
                            txtBxStr.Append((Char)'0');
                    }
                    epc = txtBxStr.ToString();
                    eventToHandle = EventToHandle.TagRssiEvent;
                    rfid.ReadUID_TagSelect(AT_UHF_NET.UIDREAD_CODE.EPC_GEN2_MULTI_TAG_SELECT, epc);
                    result = true;

                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInvtryRdRateStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for stop Inventory tag RdRate.
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryRdRateStop()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
                if (InventoryOpEventArgs != null)
                {
                    InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.stopped));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryRdRateStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Inventory RSSI Start
        /// </summary>
        /// <param name="session"></param>
        /// <param name="search1Only"></param>
        /// <param name="epcBnkMask"></param>
        /// <param name="maskOffset"></param>
        /// <param name="filterRes"></param>
        /// <returns>true/false</returns>
        public override bool TagInventoryRssiStart(RFID_18K6C_INVENTORY_SESSION session, bool search1Only, byte[] epcBnkMask, uint maskOffset, bool filterRes)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    string epc;
                    StringBuilder txtBxStr = new StringBuilder(EPCTag.EPCFldSz * 2);
                    // Check ByteArr Size
                    for (int bi = 0; bi < epcBnkMask.Length; bi++)
                        txtBxStr.Append(epcBnkMask[bi].ToString("X2"));
                    if (epcBnkMask.Length < EPCTag.EPCFldSz)
                    {
                        int NumBytes2Pad = epcBnkMask.Length & 0x01;
                        if (NumBytes2Pad == 1)
                            txtBxStr.Append((Char)'0');
                    }
                    epc = txtBxStr.ToString();
                    if (epc.Length > 24)
                    {
                        epc = epc.Substring(4, epc.Length - 4);
                    }
                    eventToHandle = EventToHandle.TagRssiEvent;
                    rfid.ReadUID_TagSelect(AT_UHF_NET.UIDREAD_CODE.EPC_GEN2_MULTI_TAG_SELECT, epc);
                    result = true;
                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryRssiStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start InventoryFirst Tag.
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryFirstStart()
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    eventToHandle = EventToHandle.DscvrTagEvent;
                    rfid.SET_ContinueModeONOff(true);
                    rfid.ReadEPC_Continuous();
                    result = true;
                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryFirstStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start InventoryOnnce Tag
        /// </summary>
        /// <param name="perfSelCmd"></param>
        /// <returns></returns>
        public override bool TagInventoryOnceStart(bool perfSelCmd)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    eventToHandle = EventToHandle.DscvrTagEvent;
                    rfid.SET_ContinueModeONOff(true);
                    rfid.ReadEPC_Continuous();
                    result = true;
                    if (InventoryOpEventArgs != null)
                    {
                        InventoryOpEventArgs(new object(), new InvtryOpEventArgs(InvtryOpStatus.started));
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryOnceStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start SetPerm Tag
        /// </summary>
        /// <param name="perm"></param>
        /// <param name="PC"></param>
        /// <param name="EPC"></param>
        /// <param name="accPasswd"></param>
        /// <returns>true/false</returns>
        public override bool TagSetPermStart(ref RFID_18K6C_TAG_PERM perm, ushort PC, ref UINT96_T EPC, uint accPasswd)
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    if (rfid.Permalock_TagSelect(AT_UHF_NET.ActField.EPC, true, accPasswd.ToString(), TagtEPC.ToString()))
                    {
                        DscvrTagEventArgs e = new DscvrTagEventArgs(0, ref TagtEPC);
                        GetTagEventArgs(null, e);
                        result = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("TagSetPermStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start SetPerm Tag
        /// </summary>
        /// <returns></returns>
        public override bool TagSetPermStart()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        /// <summary>
        /// Method for Stop SetPerm Tag
        /// </summary>
        /// <returns></returns>
        public override bool TagSetPermStop()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("TagSetPermStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start to write Tag.
        /// </summary>
        /// <param name="Option"></param>
        /// <returns></returns>
        public override bool TagWriteStart(int Option)
        {
            bool result = false;
            UserPref Pref = UserPref.GetInstance();
            try
            {
                if (StartReader())
                {
                    eventToHandle = EventToHandle.ReqTagWrBnkData;
                    if (AccessPassword > 0)
                        result = rfid.BankSelectWrite_TagSelect(AT_UHF_NET.MEMBANK_CODE.BANK_EPC, 0, WriteTagEPC, TagtEPC.ToString(), AccessPassword.ToString());
                    else
                        result = rfid.BankSelectWrite_TagSelect(AT_UHF_NET.MEMBANK_CODE.BANK_EPC, 0, WriteTagEPC, TagtEPC.ToString());

                    //if (TagWriteBankData != null)
                    //{
                    //    MemoryBanks4Op bnk = MemoryBanks4Op.None;
                    //    ushort wdOffSet = 0;
                    //    string dataStr = null;

                    //    result = TagWriteBankData(out bnk, out wdOffSet, out dataStr); 

                    //}

                    //WrOpStatus opStatus; 
                    // string errMsg = "";

                    //   if (result)
                    //     {
                    //        opStatus = WrOpStatus.completed;
                    //     }
                    //     else
                    //     {
                    //        opStatus = WrOpStatus.error;
                    //       errMsg = "Error";
                    //     }

                    //   WriteOpEventArgs(new object(), new WrOpEventArgs(opStatus, errMsg));


                }

            }
            catch (Exception ex)
            {
                Logger.LogError("TagWriteStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        ///  Method for Stop to write Tag.
        /// </summary>
        /// <returns></returns>
        public override bool TagWriteStop()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("TagWriteStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for Start Read Bank Tag.
        /// </summary>
        /// <returns></returns>
        public override bool TagReadBanksStart()
        {
            bool result = false;
            try
            {
                if (StartReader())
                {
                    rfid.BankSelectRead_TagSelect(AT_UHF_NET.MEMBANK_CODE.BANK_EPC, 0, (uint)TagtEPC.ToByteArray().Length, TagtEPC.ToString());
                    result = true;
                    if (ReadOpEventArgs != null)
                    {
                        ReadOpEventArgs(new object(), new RdOpEventArgs(RdOpStatus.started, "", AccErrorTypes.None));
                    }
                }
            }
            catch
            {

            }
            return result;
        }
        /// <summary>
        /// Method for Clear Inventory Tag.
        /// </summary>
        /// <returns></returns>
        public override bool TagInvtryClr()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool SetRepeatedTagObsrvMode(bool enable)
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool SetResponseDataMode(int Option)
        {
            bool result = true;
            try
            {
                if (Option == 2)
                {
                    if (eventRespDataMode != null)
                    {
                        eventRespDataMode(true, "");
                    }
                }
            }
            catch
            {

            }
            return result;
        }
        #endregion

        #region Mac Error
        public override bool GetMacError()
        {
            bool result = false;
            macerr = 0;
            try
            {
                string error;
                error = rfid.GET_LAST_ERROR();
                if (error == "Other Error")
                {
                    result = true;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("GetMacError : " + ex.Message);
            }
            return result;
        }

        public override bool MacErrorIsOverheat()
        {
            bool result = false;
            try
            {
                string error;
                error = rfid.GET_LAST_ERROR();
                if (error == "Other Error")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsOverheat : " + ex.Message);
            }
            return result;
        }

        public override bool MacErrorIsFatal()
        {
            bool result = false;
            try
            {
                string error;
                error = rfid.GET_LAST_ERROR();
                if (error == "Other Error")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsFatal : " + ex.Message);
            }
            return result;
        }
        public override bool MacErrorIsNegligible()
        {
            bool result = false;
            try
            {
                string error;
                error = rfid.GET_LAST_ERROR();
                if (error == "Other Error")
                {
                    result = true;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsNegligible : " + ex.Message);
            }
            return result;
        }
        public override HRESULT_RFID f_CFlow_RfidDev_RadioGetConfigurationParameter()
        {
            return HRESULT_RFID.E_ABORT;
        }
        public override bool ClearMacError()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        #endregion

        #region Antina Configuration
        public override bool AntPortCfgGetOne()
        {
            bool result = true;
            //try
            //{
                
            //    uint portNum = 0;
            //    if (AntPotCfgGetOneNotification != null)
            //    {
            //        RFID_ANTENNA_PORT_CONFIG config = Rfid.st_RfidSpReq_AntennaPortGetConfiguration[portNum].config;
            //        config.powerLevel = (uint)rfid.GET_ControlValue().Power;

            //        Logger.LogError("Get Antenna Power  - " + config.powerLevel.ToString());

            //        AntPotCfgGetOneNotification(true, portNum, ref config, null);
            //    }
            //}
            //catch
            //{

            //}
            return result;
        }
        public override bool AntPortCfgSetPwr()
        {
            bool result = true;
            try
            {
                uint powerlevel = (uint)(31 - power);

                if (powerlevel > 30)
                {
                    powerlevel = 30;
                }
                else if (powerlevel < 0)
                {
                    powerlevel = 0;
                }

                rfid.SET_PowerControl(powerlevel);
                //rfid.SET_PowerControl(0);
                Logger.LogError("Antenna Power setting - " + powerlevel.ToString());

                if (AntPotCfgSetOneNotification != null)
                    AntPotCfgSetOneNotification(true, 0, "");
            }
            catch
            {

            }
            return result;
        }
        public override bool AntPortCfgSetOne(uint portNum, ref RFID_ANTENNA_PORT_CONFIG cfg, AntPortCfgSetOneNotify notify)
        {
            bool result = true;
            try
            {
                uint powerlevel = 31 - cfg.powerLevel; // modified on 30 sept, High power level = low value, Low power level = High Value

                if (powerlevel > 30)
                {
                    powerlevel = 30;
                }
                else if (powerlevel < 0)
                {
                    powerlevel =  0;
                }

               rfid.SET_PowerControl(powerlevel);
               // rfid.SET_PowerControl(0);
                Logger.LogError("Set Antenna Power - " + cfg.powerLevel.ToString());

                if (notify != null)
                {
                    notify(true, Rfid.st_RfidSpReq_AntennaPortSetConfiguration.antennaPort, null);
                }
            }
            catch
            {

            }
            return result;
        }
        #endregion

        #region Sound/vol
        public override void BuzzerBeep(int option)
        {
            try
            {
                AT_UHF_NET.CUHFHost.PlaySuccess();
            }
            catch (Exception ex)
            {
                Logger.LogError("BuzzerBeep : " + ex.Message);
            }
        }
        public override void MelodyRing()
        {
            try
            {
                AT_UHF_NET.CUHFHost.PlaySuccess();
            }
            catch (Exception ex)
            {
                Logger.LogError("MelodyRing : " + ex.Message);
            }
        }
        #endregion

        #region Custom Freq Band
        public override bool CustomFreqBandNumGet()
        {
            // UInt32 bandNum;
            bool result = true;
            // bandNum = 0;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool CustomFreqBandNumSet()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool CustomFreqGet()
        {
            bool result = true;
            try
            {
                UserPref pref = UserPref.GetInstance();

                if (CustomFreqGetNotification != null)
                {
                    CustomFreqGetNotification(true, pref.FreqProf, 0, pref.EnFreqChnLBT);
                }
            }
            catch
            {

            }
            return result;
        }
        public override bool CustomFreqSet(CustomFreqGrp freqSet, int chn, bool enLBT, CustomFreqSetNotify notify)
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool CustomFreqSet(CustomFreqGrp freqSet, bool enLBT, CustomFreqSetNotify notify)
        {
            bool result = true;
            try
            {
                if (notify != null)
                    notify(true, null);
            }
            catch
            {

            }
            return result;
        }
        public override bool CustomFreqSet(CustomFreqGrp freqSet, CustomFreqSetNotify notify)
        {
            bool result = true;
            try
            {
                if (notify != null)
                    notify(true, null);
            }
            catch
            {

            }
            return result;
        }

        #endregion

        public override bool GetTempThreshold()
        {
            bool result = true;
            try
            {
                if (ThrshTempGetNotification != null)
                    ThrshTempGetNotification(Rfid.st_RfidSpPkt_CustomTemp.amb,
                           Rfid.st_RfidSpPkt_CustomTemp.xceiver,
                           Rfid.st_RfidSpPkt_CustomTemp.pow_amp,
                           Rfid.st_RfidSpPkt_CustomTemp.pow_amp_delta);

            }
            catch
            {

            }
            return result;
        }
        public override bool SetTempThreshold(ushort xcvrLmt)
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool SetTempThreshold(ushort xcvrLmt, ushort ambLmt, ushort paLmt, ushort deltaLmt)
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }

        public override bool GetCurrTemp()
        {
            //ushort amb, xcvr, pamp;
            bool result = true;
            //amb = 0;
            //xcvr = 0;
            //pamp = 0;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool CancelRunningOp()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("CancelRunningOp : " + ex.Message);
            }
            return result;
        }

        public override bool PerformHealthCheck()
        {
            bool result = true;
            try
            {
                if (HealthCheckStatusNotification != null)
                    HealthCheckStatusNotification(HealthChkOpStatus.LnkProf, "Setting Link Profile to 2");
            }
            catch
            {

            }
            return result;
        }
        public override bool StopHealthCheck()
        {
            bool result = false;
            try
            {
                StopReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("StopHealthCheck : " + ex.Message);
            }
            return result;
        }

        public override bool GetPwrLvlRegVals()
        {
            bool result = true;
            PwrLevRegVal = 0;
            RevPwrLevRegVal = 0;
            RevPwrThrshRegVal = 0;
            try
            {

            }
            catch (Exception ex)
            {
                Logger.LogError("GetPwrLvlRegVals : " + ex.Message);
            }
            return result;
        }

        public override void Dispose()
        {
            try
            {

            }
            catch
            {

            }
        }

        #region Set/Get LinkProfNum
        public override bool LinkProfNumSet()
        {
            bool result = true;
            try
            {
                if (LnkProfNumoSetNotification != null)
                    LnkProfNumoSetNotification(true, "");

            }
            catch
            {

            }
            return result;
        }
        public override bool LinkProfNumGet()
        {
            bool result = true;
            ProfNum = 0;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool LinkProfInfoGet()
        {
            bool result = true;
            try
            {
                if (LnkProfInfoGetNotification != null)
                    LnkProfInfoGetNotification(true, ref Rfid.st_RfidSpReq_RadioGetLinkProfile.linkProfileInfo, null);

            }
            catch
            {

            }
            return result;
        }
        #endregion

        #region RF

        public override bool RFGenerateRandomData()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool RFGenerateRandomDataSetup()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }

        public override bool CarrierWaveOn()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }
        public override bool CarrierWaveOff()
        {
            bool result = true;
            try
            {

            }
            catch
            {

            }
            return result;
        }

        public override bool RadioReady()
        {
            bool result = false;
            try
            {
                if (rfid.IsOpen())
                    result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioReady : " + ex.Message);
            }
            return result;
        }

        private void RadioOpenResultNotifyCb(RadioOpRes res, RadioStatus status, String msg)
        {
            if (RadioStatusNotification != null)
                RadioStatusNotification(res, status, msg);
        }

        public override bool RadioOpen()
        {
            bool result = false;
            try
            {
                result = OpenRadio();

                if (result)
                {
                    RadioOpenResultNotifyCb(RadioOpRes.StateChanged, RadioStatus.Configured, "ok");
                }
                else
                {
                    RadioOpenResultNotifyCb(RadioOpRes.Error, RadioStatus.Opening, "error");
                }
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioOpen : " + ex.Message);
            }
            return result;
        }
        public override bool RadioReOpen()
        {
            bool result = false;
            try
            {
                StartReader();
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioReOpen : " + ex.Message);
            }
            return result;
        }
        public override bool RadioClose()
        {
            bool result = false;
            try
            {
                result = CloseRadio();
                if (result)
                {
                    RadioOpenResultNotifyCb(RadioOpRes.StateChanged, RadioStatus.Closed, "ok");
                }
                else
                {
                    RadioOpenResultNotifyCb(RadioOpRes.Error, RadioStatus.ErrorStopped, "error");
                }
                result = true;
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioClose : " + ex.Message);
            }
            return result;
        }
        # endregion
    }


    public class ClsCallBack : AT_UHF_NET.IUHFHost
    {
        RFID_Reader objReader;
        
        public ClsCallBack(RFID_Reader rdr)
        {
            objReader = rdr;
             
        }

        //************************************CallBack**************************************//
        #region CallBack
        /// <summary>
        /// Get Reply. (Result of operation)
        /// </summary>
        /// <param name="Reply"></param>
        public void GetReply(string Reply)
        {
            try
            {
                if (Reply.CompareTo("OK") == 0)
                    AT_UHF_NET.CUHFHost.PlaySuccess();
                else
                    AT_UHF_NET.CUHFHost.PlayFail();

                if (objReader.eventToHandle == EventToHandle.ReqTagWrBnkData)
                {
                    MemoryBanks4Op mbank = MemoryBanks4Op.One;
                    ushort wdoffset = 0;
                    string datastr = null;
                    objReader.TagWriteBankData(out mbank, out wdoffset, out datastr);

                    WrOpStatus opStatus;
                    string errMsg = "";

                    if (Reply.CompareTo("OK") == 0)
                    {
                        opStatus = WrOpStatus.stopped;
                    }
                    else
                    {
                        opStatus = WrOpStatus.errorStopped;
                        errMsg = "Error";
                    }

                    if (objReader.WriteOpEventArgs != null)
                        objReader.WriteOpEventArgs(new object(), new WrOpEventArgs(opStatus, errMsg));
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("GetReply : " + ex.Message);
            }
        }
        /// <summary>
        /// Get MemoryBank Value. (Result of access’ operation)
        /// </summary>
        /// <param name="MemoryData"></param>
        public void GetMemoryData(string MemoryData)
        {
            try
            {
                UINT96_T epcUint96 = new UINT96_T();

                string[] temp = MemoryData.Split(",".ToCharArray());
                string epc = temp[0];

                epc = epc.Substring(4, epc.Length - 4);

                epc = epc.PadLeft(24, '0');

                epcUint96.ParseString(epc);

                string RSSI = "0";

                if (temp[1] != null)
                    RSSI = temp[1].Split("=".ToCharArray())[1];


                switch (objReader.eventToHandle)
                {
                    case EventToHandle.DscvrTagEvent:
                        {
                            DscvrTagEventArgs e = new DscvrTagEventArgs(0, ref epcUint96, 1, Convert.ToSingle(RSSI), new FileTime());
                            if (objReader.GetTagEventArgs != null)
                                objReader.GetTagEventArgs(null, e);
                        }
                        break;
                    case EventToHandle.MemBnkRdEvent:
                        {

                        }
                        break;
                }


            }
            catch (Exception ex)
            {
                Logger.LogError("GetMemoryData : " + ex.Message);
            }
        }
        /// <summary>
        /// Get accessed EPC of tag.
        /// </summary>
        /// <param name="EPC"></param>
       
        public void GetAccessEPC(string EPC)
        {
            try
            {
                 
                //Console.WriteLine(EPC);
                AT_UHF_NET.CUHFHost.PlaySuccess();

               UINT96_T epcUint96 = new UINT96_T();

                string[] temp = EPC.Split(",".ToCharArray());

                string epc = "";
                if(temp.Length > 0)
                epc=temp[0];


                epc = epc.Substring(4);

                epc = epc.PadLeft(24, '0');

                 epcUint96.ParseString(epc);

                string RSSI = "0";

                if (temp.Length > 1 && temp[1] != null)
                    RSSI = temp[1].Split("=".ToCharArray())[1];

                if (objReader.callbackevent != null)
                    objReader.callbackevent(epc, RSSI);

                if (objReader.GetTagEventArgs != null || objReader.TagRessiEventArgs != null)
                {
                    switch (objReader.eventToHandle)
                    {
                        case EventToHandle.DscvrTagEvent:
                            {
                                DscvrTagEventArgs e = new DscvrTagEventArgs(0, ref epcUint96, 1, Convert.ToSingle(RSSI), new FileTime());
                                if (objReader.GetTagEventArgs != null)
                                    objReader.GetTagEventArgs(null, e);
                            }
                            break;
                        case EventToHandle.MemBnkRdEvent:
                            {

                            }
                            break;
                        case EventToHandle.TagRssiEvent:
                            {
                                TagRssiEventArgs tagRSSIEventArg = new TagRssiEventArgs(Convert.ToSingle(RSSI));
                                if (objReader.TagRessiEventArgs != null)
                                    objReader.TagRessiEventArgs(null, tagRSSIEventArg);
                            }
                            break;
                    }
                }


            }
            catch (Exception ex)
            {
                //Logger.LogError("GetAccessEPC : " + ex.Message);
            }
        }

         
       
        #endregion
    }

    public enum EventToHandle
    {
        DscvrTagEvent,
        WrOpStEvent,
        MemBnkRdEvent,
        MemBnkWrEvent,
        ReqTagWrBnkData,
        TagRssiEvent

    }


    //    #endregion
    //#region  Events

    //void CS101_Reader_DscvrTagEvent(object sender, DscvrTagEventArgs e)
    //{
    //    try
    //    {
    //        if (GetTagEventArgs != null)
    //            GetTagEventArgs(sender, e);
    //    }
    //    catch
    //    { }
    //}
    //    #region Tag Inventory Request 

    //    //public uint AccessPassword { get; set; }
    //    //public uint NumberTags { get; set; }
    //    //public byte[] LastWrittenEPC { get; set; }
    //    //public UINT96_T TagtEPC;
    //    //public UInt32 CurAccPasswd { get; set; }
    //    //public UInt32 ProfNum;
    //    //public UInt32 bandNum;
    //    //public UInt32 PwrLevRegVal;
    //    //public UInt32 RevPwrLevRegVal;
    //    //public UInt32 RevPwrThrshRegVal;
    //    //public UInt32 Size;
    //    //public int power;
    //    //public UInt32 PortNum;
    //    //public ushort amb, xcvr, pamp;
    //    //public int frequency;
    //    //public SoundVol Svol;
    //    //public RingTone RingingID;
    //    //public UInt32 lnkProfNum { get; set; }

    //    // public event ReqTagWrBnkData TagWriteBankData;
    //    //public event ReqTagRdBnkInfo TagReadBankData;
    //    //public event CustomFreqGetNotify CustomerFreqGetNotification;
    //    //public event AntPortCfgGetOneNotify AntPotCfgGetonNotification;
    //    //public event AntPortCfgSetOneNotify AntPotCfgSetonNotification;
    //    //public event RadioStatusNotify RadioStatusNotification;
    //    //public event LnkProfInfoGetNotify LnkProfInfoGetNotification;
    //    //public event LnkProfNumSetNotify LnkProfNumoSetNotification;
    //    //// public event CustomFreqSetNotify CustomFreqSetNotification;
    //    //public event HealthCheckStatusNotify HealthCheckStatusNotification;
    //    //public event ThrshTempGetNotify ThrshTempGetNotification;



    //    private bool TagWrBnkDataReqCb(out MemoryBanks4Op bnk, out ushort wdOffSet,
    //       out String dataStr)
    //    {
    //        bool result = false;
    //        bnk = MemoryBanks4Op.None;
    //        wdOffSet = 0;
    //        dataStr = null;
    //        result = TagWriteBankData(out bnk, out wdOffSet, out dataStr);
    //        return result;
    //    }

    //   






    //    private bool TgtRdBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
    //    {
    //        bnk2Rd = MemoryBanks4Op.None;
    //        wdOffset = 0;
    //        wdCnt = 0;

    //        bool result = false;

    //        if (TagReadBankData != null)
    //        {
    //            result = TagReadBankData(out bnk2Rd, out   wdOffset, out   wdCnt);
    //        }
    //        return result;
    //    }





    //    private void RespDataModeCb(bool succ, string errmsg)
    //    {
    //        eventRespDataMode(succ, errmsg);
    //    }


    //

    /*
        
       void CS101_Reader_InvtryOpStEvent(object sender, InvtryOpEventArgs e)
       {
           try
           {
               if (InventoryOpEventArgs != null)
                   InventoryOpEventArgs(sender, e);
           }
           catch
           { }
       }
        
        
       void CS101_Reader_LckOpStEvent(object sender, LckOpEventArgs e)
       {
           try
           {
               if (LockOpEventArgs != null)
                   LockOpEventArgs(sender, e);
           }
           catch
           { }
       }
        

        
       void CS101_Reader_MemBnkRdEvent(object sender, MemBnkRdEventArgs e)
       {
           try
           {
               if (MemberBankReadEvenArgs != null)
                   MemberBankReadEvenArgs(sender, e);
           }
           catch
           { }
       }
        

        
       void CS101_Reader_MemBnkWrEvent(object sender, MemBnkWrEventArgs e)
       {
           try
           {
               if (MemberBankWriteEventArgs != null)
                   MemberBankWriteEventArgs(sender, e);
           }
           catch
           { }
       }
        

        
       void CS101_Reader_RdOpStEvent(object sender, RdOpEventArgs e)
       {
           try
           {
               if (ReadOpEventArgs != null)
                   ReadOpEventArgs(sender, e);
           }
           catch
           { }
       }
         
       void CS101_Reader_TagPermSetEvent(object sender, DscvrTagEventArgs e)
       {
           try
           {
               if (GetTagEventArgs != null)
                   GetTagEventArgs(sender, e);
           }
           catch
           { }
       }
        
       void CS101_Reader_TagRateEvent(object sender, TagRateEventArgs e)
       {
           try
           {
               if (TagRateEventArgs != null)
                   TagRateEventArgs(sender, e);
           }
           catch
           { }
       }
        
       void CS101_Reader_TagRssiEvent(object sender, TagRssiEventArgs e)
       {
           try
           {
               if (TagRessiEventArgs != null)
                   TagRessiEventArgs(sender, e);
           }
           catch
           { }
       }
        
       void CS101_Reader_TempUpdateEvt(object sender, TempUpdateEventArgs e)
       {
           try
           {
               if (TempUpdateEventArgs != null)
                   TempUpdateEventArgs(sender, e);
           }
           catch
           { }
       }
        
       void CS101_Reader_WrOpStEvent(object sender, WrOpEventArgs e)
       {
           try
           {
               if (WriteOpEventArgs != null)
                   WriteOpEventArgs(sender, e);
           }
           catch
           { }
       }
        
      */


    /* Set/Get LinkProfNum 
    
         private void AntPotCfgGetOneNotificationCb(bool succ, uint portNum, ref RFID_ANTENNA_PORT_CONFIG confg, string msg)
         {
          AntPotCfgGetOneNotification(succ, portNum, ref   confg, msg);
         }
         private void AntPotCfgSetOneNotificationCb(bool succ, uint PortNum, string errMsg)
         {
          AntPotCfgSetOneNotification(succ, PortNum, errMsg);
         }
         private void CustomerFreqGetNotificationCb(bool succ, CustomFreqGrp freqGrp, int chn, bool enLBT)
         {
         //CustomFreqGetNotification(succ, freqGrp, chn, enLBT);
         }

         private void ThrshTempGetNotificationCb(ushort amb, ushort xcvr, ushort pwrAmp, ushort paDelta)
         {
         this.ThrshTempGetNotification(amb, xcvr, pwrAmp, paDelta);
         }

         private void HealthCheckStatusNotificationCb(HealthChkOpStatus status, string msg)
         {
         this.HealthCheckStatusNotification(status, msg);
         }


         public void LnkProfNumoSetNotificationCb(bool succ, string errMsg)
         {
         LnkProfNumoSetNotification(succ, errMsg);
         }


         void LnkProfInfoGetNotificationCb(bool succ, ref RFID_RADIO_LINK_PROFILE InkPrf, string errMsg)
         {
         LnkProfInfoGetNotification(succ, ref InkPrf, errMsg);
         }
     */

}
