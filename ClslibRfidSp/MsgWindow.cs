using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Microsoft.WindowsCE.Forms; 
using System.Runtime.InteropServices;
using System.Diagnostics;
using ReaderTypes;
using SP = ReaderTypes.RfidSp;
/// MessageWindow handles message from Dll (usually trigger by CallFlow)
/// MessageWindow is in  Microsoft.WindowsCE.Forms, allow to use Hwnd
/// Message       is in  Microsoft.WindowsCE.Forms. Result  HWnd Msg LParam WParam


namespace ClslibRfidSp 
{
    #region delegates
    public delegate void RFIDMsgNotify(RFID_MSGID msgID);
    public delegate void RFIDMsgNotifyHRes (RFID_MSGID msgID, HRESULT_RFID hRes);
    public delegate void RFIDMsgNotifyTagArr (RFID_MSGID msgID, UInt16[] pcArr, UINT96_T[] epcArr, float[] rssiArr, FileTime[] ftArr);
    public delegate void RFIDMsgNotifyFreqGrp(RFID_MSGID msgID, CustomFreqGrp freqGrp, int chn, bool enLBT);
    public delegate void RFIDMsgNotifyUINT32(RFID_MSGID msgID, uint arg1);
    public delegate void RFIDMsgNotifyFloat(RFID_MSGID msgID, float arg1);
    public delegate void RFIDMsgNotifySelCrit (RFID_MSGID msgID, HRESULT_RFID hRes, RFID_18K6C_SELECT_CRITERION[] criteria);
    public delegate void RFIDMsgNotifyTagRate(RFID_MSGID msgID, UInt32 period, UInt32 cnt);
    #endregion

    /// unsafe code for some unsafe structs 
    public class MsgWindow : MessageWindow
    {
        public const int WM_USER = 0x0400;
        public RFIDMsgNotify MsgNotify;
        public RFIDMsgNotifyHRes MsgNotifyHRes;
        public RFIDMsgNotifyTagArr MsgNotifyTagArr;
        public RFIDMsgNotifyFreqGrp MsgNotifyFreqGrp;
        public RFIDMsgNotifyUINT32 MsgNotifyUINT32;
        public RFIDMsgNotifyFloat MsgNotifyFloat;
        public RFIDMsgNotifySelCrit MsgNotifySelCrit;
        public RFIDMsgNotifyTagRate MsgNotifyTagRate;
        // private int msgcnt0; 
#if false // FF
        private Form1 msgform;
        public MsgWindow( Form1 msgform)
        {
            this.msgform = msgform;
        }
#else
        private void DummyMsgNotifyHandler(RFID_MSGID msgID)
        {
            // exist to support instantiation of MsgNotify
            switch (msgID)
            {
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_NONCRITICAL_FAULT:
                    // better handling later
                    MessageBox.Show("NonCritical Fault : " + "Type " + Rfid.st_RfidSpPkt_NonCritFault.fault_type
                        + " SubType " + Rfid.st_RfidSpPkt_NonCritFault.fault_subtype);
                    break;
            }
        }
        private void DummyMsgNotifyHResHandler(RFID_MSGID msgID, HRESULT_RFID hRes)
        {
            // exist to support instantiation of MsgNotifyHRes
        }
        private void DummyMsgNotifyTagArrHandler(RFID_MSGID msgID, UInt16[] pcArr, UINT96_T[] epcArr,
            float[] rssiArr, FileTime[] ftArr)
        {
            // exist to support instantiation of MsgNotifyTagArr
        }
        private void DummyMsgNotifyFreqGrpHandler(RFID_MSGID msgID, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            // exist to support instantiation of MsgNotifyFreqGrp
        }
        private void DummyMsgNotifyUINT32Handler(RFID_MSGID msgID, uint arg1)
        {
            // exist to support instantiation of MsgNotifyAntNum
        }
        private void DummyMsgNotifyFloatHandler(RFID_MSGID msgID, float arg1)
        {
            // exist to support instantiation of MsgNotifyAntNum
        }
        private void DummyMsgNotifySelCritHandler(RFID_MSGID msgID, HRESULT_RFID hRes,
                                    RFID_18K6C_SELECT_CRITERION[] criteria)
        {
            // exist to support instantiation of MsgNotifySelCrit
        }
        public void DummyMsgNotifyTagRateHandler(RFID_MSGID msgID, UInt32 period, UInt32 cnt)
        {
            // exist to support instantiation of MsgNotifySelCrit
        }

        public MsgWindow()
        {
            MsgNotify = new RFIDMsgNotify(DummyMsgNotifyHandler);
            MsgNotifyHRes = new RFIDMsgNotifyHRes(DummyMsgNotifyHResHandler);
            MsgNotifyTagArr = new RFIDMsgNotifyTagArr(DummyMsgNotifyTagArrHandler);
            MsgNotifyFreqGrp = new RFIDMsgNotifyFreqGrp(DummyMsgNotifyFreqGrpHandler);
            MsgNotifyUINT32 = new RFIDMsgNotifyUINT32(DummyMsgNotifyUINT32Handler);
            MsgNotifyFloat = new RFIDMsgNotifyFloat(DummyMsgNotifyFloatHandler);
            MsgNotifySelCrit = new RFIDMsgNotifySelCrit(DummyMsgNotifySelCritHandler);
            MsgNotifyTagRate = new RFIDMsgNotifyTagRate(DummyMsgNotifyTagRateHandler);
        }
#endif
        protected override void WndProc(ref Message msg)
        {
            RFID_MSGID  Rfid_Msgid = (RFID_MSGID) msg.Msg;
            UInt32      tisiz = 0;
            int         iRet       = (int) msg.WParam; 
            IntPtr      MsgWParam  =       msg.WParam;
            IntPtr      MsgLParam  =       msg.LParam;
            HRESULT_RFID hr;
            switch (Rfid_Msgid)
            {   ////Request Response Messages
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_Startup :
                    Rfid.st_RfidSpReq_Startup    = (RFID_Startup_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_Startup_T));
                    Datalog.LogStr( "RfidDev msg Startup\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_Startup);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_Shutdown :
                    Rfid.st_RfidSpReq_Shutdown   = (RFID_Shutdown_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_Shutdown_T));
                    Datalog.LogStr("RfidDev msg Shutdown\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_Shutdown);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RetrieveAttachedRadiosList :
                    Rfid.st_RfidSpReq_RetrieveAttachedRadiosList = (RFID_RetrieveAttachedRadiosList_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RetrieveAttachedRadiosList_T));
                    if (SP.SUCCEEDED(Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.status))
                    {
                        Rfid.rfid_cookie = Rfid.st_RfidSpReq_RetrieveAttachedRadiosList.radio_enum._RadioInfo.cookie; 
                    };
                    Datalog.LogStr("RfidDev msg RetrieveAttachedRadiosList\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RetrieveAttachedRadiosList);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioOpen :
                    Rfid.st_RfidSpReq_RadioOpen = (RFID_RadioOpen_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioOpen_T));
                    if (SP.SUCCEEDED(Rfid.st_RfidSpReq_RadioOpen.status))
                    {   Rfid.rfid_handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                    } //else{ Rfid.rfid_handle = 0; };
                    Datalog.LogStr("RfidDev msg RadioOpen\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioOpen);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioClose :
                    Rfid.st_RfidSpReq_RadioClose = (RFID_RadioClose_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioClose_T));
                    if (SP.SUCCEEDED(Rfid.st_RfidSpReq_RadioClose.status))
                    {   Rfid.rfid_handle = 0; //Invalid Handle
                    };                    
                    Datalog.LogStr("RfidDev msg RadioClose\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioClose);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetConfigurationParameter :
                    Rfid.st_RfidSpReq_RadioSetConfigurationParameter = (RFID_RadioGetSetConfigurationParameter_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetConfigurationParameter_T));
                    Datalog.LogStr("RfidDev msg RadioSetConfigurationParameter\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetConfigurationParameter);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetConfigurationParameter :
                    Rfid.st_RfidSpReq_RadioGetConfigurationParameter = (RFID_RadioGetSetConfigurationParameter_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetConfigurationParameter_T));
                    Datalog.LogStr("RfidDev msg RadioGetConfigurationParameter\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetConfigurationParameter);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetOperationMode :
                    Rfid.st_RfidSpReq_RadioSetOperationMode = (RFID_RadioGetSetOperationMode_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetOperationMode_T));
                    Datalog.LogStr("RfidDev msg RadioSetOperationMode\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetOperationMode);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetOperationMode :
                    Rfid.st_RfidSpReq_RadioGetOperationMode = (RFID_RadioGetSetOperationMode_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetOperationMode_T));
                    Datalog.LogStr("RfidDev msg RadioGetOperationMode\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetPowerState :
                    Rfid.st_RfidSpReq_RadioSetPowerState = (RFID_RadioGetSetPowerState_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetPowerState_T));
                    Datalog.LogStr("RfidDev msg RadioSetPowerState\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetPowerState :
                    Rfid.st_RfidSpReq_RadioGetPowerState = (RFID_RadioGetSetPowerState_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetPowerState_T));
                    Datalog.LogStr("RfidDev msg RadioGetPowerState\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetCurrentLinkProfile :
                    Rfid.st_RfidSpReq_RadioSetCurrentLinkProfile = (RFID_RadioGetSetCurrentLinkProfile_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetCurrentLinkProfile_T));
                    Datalog.LogStr("RfidDev msg RadioSetCurrentLinkProfile\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetCurrentLinkProfile);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetCurrentLinkProfile :
                    Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile = (RFID_RadioGetSetCurrentLinkProfile_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetCurrentLinkProfile_T));
                    Rfid.curr_link_profile = Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.profile; //[out] 
                  // Rfid.st_RfidSpReq_RadioGetLinkProfile.profile = Rfid.st_RfidSpReq_RadioGetCurrentLinkProfile.profile; //[in] 
                  // CallFlow.f_CFlow_RfidDev_RadioGetLinkProfile(ref Rfid.st_RfidSpReq_RadioGetLinkProfile);
                    Datalog.LogStr("RfidDev msg RadioGetCurrentLinkProfile\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetCurrentLinkProfile);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetLinkProfile :
                    Rfid.st_RfidSpReq_RadioGetLinkProfile = (RFID_RadioGetLinkProfile_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetLinkProfile_T));
                    //st_RfidSpReq_RadioSetLinkProfile.linkProfileInfo
                    //st_RfidSpReq_RadioSetCurrentLinkProfile.profile 
                  Datalog.LogStr("RfidDev msg RadioGetLinkProfile\n");
                  MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetLinkProfile);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetStatus :
                    RFID_AntennaPortGetStatus_T AntStatus;
                    AntStatus = (RFID_AntennaPortGetStatus_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_AntennaPortGetStatus_T));
                    AntStatus.Clone(
                        out Rfid.st_RfidSpReq_AntennaPortGetStatus[AntStatus.antennaPort]);
                    Datalog.LogStr("RfidDev msg AntennaPortGetStatus\n");
                    MsgNotifyUINT32(RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetStatus,
                        AntStatus.antennaPort);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortSetState :
                    Rfid.st_RfidSpReq_AntennaPortSetState = (RFID_AntennaPortSetState_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_AntennaPortSetState_T));
                    Datalog.LogStr("RfidDev msg AntennaPortSetState\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortSetConfiguration :
                    Rfid.st_RfidSpReq_AntennaPortSetConfiguration = (RFID_AntennaPortGetSetConfiguration_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_AntennaPortGetSetConfiguration_T));
                    Datalog.LogStr("RfidDev msg AntennaPortSetConfiguration\n");
                    MsgNotifyUINT32(
                        RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortSetConfiguration,
                        Rfid.st_RfidSpReq_AntennaPortSetConfiguration.antennaPort);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetConfiguration :
                    RFID_AntennaPortGetSetConfiguration_T AntConfig;
                    AntConfig = (RFID_AntennaPortGetSetConfiguration_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_AntennaPortGetSetConfiguration_T));
                    AntConfig.Clone(out Rfid.st_RfidSpReq_AntennaPortGetConfiguration[AntConfig.antennaPort]);
                    Datalog.LogStr("RfidDev msg AntennaPortGetConfiguration\n");
                    MsgNotifyUINT32(RFID_MSGID.RFID_REQEND_TYPE_MSGID_AntennaPortGetConfiguration,
                        AntConfig.antennaPort);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria: // The Criterions are not returned
                    RFID_18K6CSetSelectCriteria_T CallArg = new RFID_18K6CSetSelectCriteria_T();
                    CallArg = (RFID_18K6CSetSelectCriteria_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CSetSelectCriteria_T));
                    Datalog.LogStr("RfidDev msg 18K6CSetSelectCriteria\n");
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetSelectCriteria,
                        CallArg.status);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetSelectCriteria:  // The Criterions are not returned
                    Rfid.st_RfidSpReq_18K6CGetSelectCriteria = (RFID_18K6CGetSelectCriteria_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CGetSelectCriteria_T));
                    ////////////////////////////////////////////////////////////////////
                    //(LPBYTE)  pB_18K6CGetSelectCriteria  //4+4 + n*56 Bytes  n<=4   =232B
                    // RFID_RADIO_HANDLE     handle               4
                    // RFID_18K6C_SELECT_CRITERIA
                    //  countCriteria; //INT32U                   4
                    //  RFID_18K6C_SELECT_CRITERION* pCriteria
                    //    RFID_18K6C_SELECT_MASK     mask;
                    //        bank;   //RFID_18K6C_MEMORY_BANK    4
                    //        offset; //INT32U                    4
                    //        count;  //INT32U                    4
                    //        mask[RFID_18K6C_SELECT_MASK_BYTE_LEN]; //INT8U*32
                    //    RFID_18K6C_SELECT_ACTION  action;
                    //        target; //RFID_18K6C_TARGET         4
                    //        action; //RFID_18K6C_ACTION         4
                    //        enableTruncate; //BOOL32            4
//fixed RFID_18K6C_SELECT_CRITERION          arySelectCrit[SELECTCRITERIA_COUNT];       //4
                    Datalog.LogStr("RfidDev msg 18K6CGetSelectCriteria\n");
                    RFID_18K6C_SELECT_CRITERION[] criteria = MarshalSelectCriteria (ref Rfid.st_RfidSpReq_18K6CGetSelectCriteria.criteria);
                    MsgNotifySelCrit(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetSelectCriteria,
                        Rfid.st_RfidSpReq_18K6CGetSelectCriteria.status, criteria);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria:  // The Criterions are not returned
                    RFID_18K6CSetPostMatchCriteria_T callArg = new RFID_18K6CSetPostMatchCriteria_T();
                     callArg = (RFID_18K6CSetPostMatchCriteria_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CSetPostMatchCriteria_T));
                    Datalog.LogStr("RfidDev msg 18K6CSetPostMatchCriteria\n");
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetPostMatchCriteria, callArg.status);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetPostMatchCriteria:   // The Criterions are not returned
                    Rfid.st_RfidSpReq_18K6CGetPostMatchCriteria = (RFID_18K6CGetPostMatchCriteria__T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CGetPostMatchCriteria__T));
                    ////////////////////////////////////////////////////////////////////
                    //(LPBYTE)  pB_18K6CGetPostMatchCriteria  //4+4+n*74               =304B
                    // RFID_RADIO_HANDLE      handle              4
                    // RFID_18K6C_SINGULATION_CRITERIA
                    //  countCriteria; //INT32U                   4
                    //  RFID_18K6C_SINGULATION_CRITERION* pCriteria;
                    //    match;      //BOOL32                    4
                    //    RFID_18K6C_SINGULATION_MASK mask;
                    //        offset; //INT32U                    4
                    //        count;  //INT32U                    4
                    //        mask[RFID_18K6C_SINGULATION_MASK_BYTE_LEN]; //INT8U*62
//fixed RFID_18K6C_SINGULATION_CRITERION     aryPostMatchCrit[POSTMATCHCRITERIA_COUNT]; //4
                    Datalog.LogStr("RfidDev msg 18K6CGetPostMatchCriteria\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters :
                    Rfid.st_RfidSpReq_18K6CSetQueryParameters.status = (HRESULT_RFID)MsgLParam;
                    Datalog.LogStr("RfidDev msg 18K6CSetQueryParameters\n");
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CSetQueryParameters,
                        Rfid.st_RfidSpReq_18K6CSetQueryParameters.status);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetQueryParameters:
                    Rfid.st_RfidSpReq_18K6CGetQueryParameters = (RFID_18K6CGetQueryParameters_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CGetQueryParameters_T));
                    Datalog.LogStr("RfidDev msg 18K6CGetQueryParameters\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CGetQueryParameters);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory :
                    Rfid.st_RfidSpReq_18K6CTagInventory = (RFID_18K6CTagInventory_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CTagInventory_T));
                    Datalog.LogStr1("RfidDev msg 18K6CTagInventory\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagInventory);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagRead :
                    Rfid.st_RfidSpReq_18K6CTagRead = (RFID_18K6CTagRead_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CTagRead_T));
                    Datalog.LogStr1("RfidDev msg 18K6CTagRead\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagRead);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagWrite :
                    Rfid.st_RfidSpReq_18K6CTagWrite = (RFID_18K6CTagWrite_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CTagWrite_T));
                    Datalog.LogStr1("RfidDev msg 18K6CTagWrite\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagWrite);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagKill :
                    Rfid.st_RfidSpReq_18K6CTagKill = (RFID_18K6CTagKill_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CTagKill_T));
                    Datalog.LogStr1("RfidDev msg 18K6CTagKill\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagLock :
                    Rfid.st_RfidSpReq_18K6CTagLock = (RFID_18K6CTagLock_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_18K6CTagLock_T));
                    Datalog.LogStr1("RfidDev msg 18K6CTagLock\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_18K6CTagLock);
                    break;
                  case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation :
                    Rfid.st_RfidSpReq_RadioCancelOperation = (RFID_RadioCancelOperation_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioCancelOperation_T));
                    Datalog.LogStr1("RfidDev msg RadioCancelOperation\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioCancelOperation);
                    break;
                  case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation :
                    Rfid.st_RfidSpReq_RadioAbortOperation = (RFID_RadioAbortOperation_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioAbortOperation_T));
                    Datalog.LogStr1("RfidDev msg RadioAbortOperation\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioAbortOperation);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetResponseDataMode :
                    Rfid.st_RfidSpReq_RadioSetResponseDataMode = (RFID_RadioGetSetResponseDataMode_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetResponseDataMode_T));
                    Datalog.LogStr("RfidDev msg RadioSetResponseDataMode\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetResponseDataMode);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetResponseDataMode :
                    Rfid.st_RfidSpReq_RadioGetResponseDataMode = (RFID_RadioGetSetResponseDataMode_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetSetResponseDataMode_T));
                    Datalog.LogStr("RfidDev msg RadioGetResponseDataMode\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacUpdateFirmware :
                    Rfid.st_RfidSpReq_MacUpdateFirmware = (RFID_MacUpdateFirmware_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacUpdateFirmware_T));
                    Datalog.LogStr("RfidDev msg MacUpdateFirmware\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacGetVersion :
                    Rfid.st_RfidSpReq_MacGetVersion = (RFID_MacGetVersion_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacGetVersion_T));
                    Datalog.LogStr("RfidDev msg MacGetVersion\n");
                    MsgNotify(RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacGetVersion);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacReadOemData :
                    Rfid.st_RfidSpReq_MacReadOemData = (RFID_MacReadWriteOemData_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacReadWriteOemData_T));
                    Datalog.LogStr("RfidDev msg MacReadOemData\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacWriteOemData :
                    Rfid.st_RfidSpReq_MacWriteOemData = (RFID_MacReadWriteOemData_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacReadWriteOemData_T));
                    Datalog.LogStr("RfidDev msg MacWriteOemData\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacReset :
                    Rfid.st_RfidSpReq_MacReset = (RFID_MacReset_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacReset_T));
                    Datalog.LogStr("RfidDev msg MacReset\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacClearError :
                    Rfid.st_RfidSpReq_MacClearError = (RFID_MacClearError_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacClearError_T));
                    Datalog.LogStr("RfidDev msg MacClearError\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacBypassWriteRegister :
                    Rfid.st_RfidSpReq_MacBypassWriteRegister = (RFID_MacBypassReadWriteRegister_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacBypassReadWriteRegister_T));
                    Datalog.LogStr("RfidDev msg MacBypassWriteRegister\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacBypassReadRegister :
                    Rfid.st_RfidSpReq_MacBypassReadRegister = (RFID_MacBypassReadWriteRegister_T)Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacBypassReadWriteRegister_T));
                    Datalog.LogStr("RfidDev msg MacBypassReadRegister\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacSetRegion :
                    Rfid.st_RfidSpReq_MacSetRegion = (RFID_MacGetSetRegion_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacGetSetRegion_T));
                    Datalog.LogStr("RfidDev msg MacSetRegion\n");
                    break;
                case  RFID_MSGID.RFID_REQEND_TYPE_MSGID_MacGetRegion :
                    Rfid.st_RfidSpReq_MacGetRegion = (RFID_MacGetSetRegion_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_MacGetSetRegion_T));
                    Datalog.LogStr("RfidDev msg MacGetRegion\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioSetGpioPinsConfiguration :
                    Rfid.st_RfidSpReq_RadioSetGpioPinsConfiguration = (RFID_RadioSetGpioPinsConfiguration_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioSetGpioPinsConfiguration_T));
                    Datalog.LogStr("RfidDev msg RadioSetGpioPinsConfiguration\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioGetGpioPinsConfiguration :
                    Rfid.st_RfidSpReq_RadioGetGpioPinsConfiguration = (RFID_RadioGetGpioPinsConfiguration_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioGetGpioPinsConfiguration_T));
                    Datalog.LogStr("RfidDev msg RadioGetGpioPinsConfiguration\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioReadGpioPins :
                      Rfid.st_RfidSpReq_RadioReadGpioPins = (RFID_RadioReadWriteGpioPins_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioReadWriteGpioPins_T));
                    Datalog.LogStr("RfidDev msg RadioReadGpioPins\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_RadioWriteGpioPins :
                      Rfid.st_RfidSpReq_RadioWriteGpioPins = (RFID_RadioReadWriteGpioPins_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_RadioReadWriteGpioPins_T));
                    Datalog.LogStr("RfidDev msg RadioWriteGpioPins\n");
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetTemperature:
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetThrshTemperature:
                    // WParam: HRESULT_RFID, LParam: RFID_CUSTOMMSG_TEMP
                    // Even if HRESULT_RFID is NG, still needs to marshall LParam to avoid memory leak
                    Rfid.st_RfidSpPkt_CustomTemp = (RFID_CUSTOMMSG_TEMP)Marshal.PtrToStructure(MsgLParam, typeof(RFID_CUSTOMMSG_TEMP));
                    Marshal.FreeHGlobal(MsgLParam);
                    HRESULT_RFID hRes = (HRESULT_RFID)MsgWParam;
                    if (SP.SUCCEEDED(hRes))
                        MsgNotify(Rfid_Msgid);
#if DEBUG // only enable in DEBUG version (too many message for production version)
                    Datalog.LogStr("RfidDev msg " + Rfid_Msgid.ToString("F") + "\n");
#endif
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRssi:
                    Datalog.LogStr("RfidDev msg CustomTagInvtryRssi\n");
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRssi,
                        (HRESULT_RFID)MsgLParam);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetFreqBandNum:
                    Datalog.LogStr("RfidDev msg CustomGetFreqBandNum\n");
                    MsgNotifyUINT32(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetFreqBandNum, (UInt32)MsgWParam);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetRadioProfile:
                    RFID_CUSTOMMSG_FREQ FreqInfo = (RFID_CUSTOMMSG_FREQ)Marshal.PtrToStructure(MsgWParam, typeof(RFID_CUSTOMMSG_FREQ));
                    Marshal.FreeHGlobal(MsgWParam);
                    Datalog.LogStr("RfidDev msg CustomGetRadioProfile\n");
                    MsgNotifyFreqGrp(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomGetRadioProfile, 
                        FreqInfo.freqGrp, FreqInfo.chnNum, FreqInfo.enLBT);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioChn:
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioChn, (HRESULT_RFID)MsgLParam);
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioProfile:
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomSetRadioProfile, (HRESULT_RFID)MsgLParam);
                    Datalog.LogStr("RfidDev msg CustomSetRadioProfile\n");
                    break;
            //////// Packets; free the Msg Heap Memory after handling
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_COMMAND_BEGIN :
                  //int size = RfidSp.f_RfidSpDll_HeapSize((IntPtr)MsgLParam);
//                    Rfid.st_RfidSpPkt_CmdBegin = (RFID_PACKETMSG_COMMAND_BEGIN_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_COMMAND_BEGIN_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam );
                    Datalog.LogStr("RfidDev msg COMMAND_BEGIN\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_COMMAND_END :
//                    Rfid.st_RfidSpPkt_CmdEnd = (RFID_PACKETMSG_COMMAND_END_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_COMMAND_END_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg COMMAND_END\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_ANTENNA_CYCLE_BEGIN :
//                    Rfid.st_RfidSpPkt_AntCycBegin = (RFID_PACKETMSG_ANTENNA_CYCLE_BEGIN_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_ANTENNA_CYCLE_BEGIN_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg ANTENNA_CYCLE_BEGIN\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_ANTENNA_BEGIN :
//                    Rfid.st_RfidSpPkt_AntBegin = (RFID_PACKETMSG_ANTENNA_BEGIN_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_ANTENNA_BEGIN_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg ANTENNA_BEGIN\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_INVENTORY_ROUND_BEGIN :
//                    Rfid.st_RfidSpPkt_InvenRndBegin = (RFID_PACKETMSG_18K6C_INVENTORY_ROUND_BEGIN_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_18K6C_INVENTORY_ROUND_BEGIN_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg 18K6C_INVENTORY_ROUND_BEGIN\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_BEGIN:
                    //Rfid.st_RfidSpPkt_InvenCycBegin = (RFID_PACKETMSG_INVENTORY_CYCLE_BEGIN_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_INVENTORY_CYCLE_BEGIN_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg INVENTORY_CYCLE_BEGIN\n");
                    MsgNotify(RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_BEGIN);
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_INVENTORY :
                    Rfid.st_RfidSpPkt_InvenData = (RFID_PACKETMSG_18K6C_INVENTORY_AND_DATA_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_18K6C_INVENTORY_AND_DATA_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr1("RfidDev msg 18K6C_INVENTORY\n"); 
                    break;                
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS :
                    // should only instantiate once
                    if (Rfid.st_RfidSpPkt_TagAccessData.tag_data == null)
                    {
                        Rfid.st_RfidSpPkt_TagAccessData.tag_data = new UInt32[ReaderTypes.RfidSp.RFID_PACKET_18K6C_TAG_ACCESS__DATA_MAXSIZ];
                    }
                    Rfid.st_RfidSpPkt_TagAccessData = (RFID_PACKETMSG_18K6C_TAG_ACCESS_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_18K6C_TAG_ACCESS_T));
                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr1("RfidDev msg 18K6C_TAG_ACCESS\n"); //" + Rfid.st_RfidSpPkt_InvenData.inv_data + "
                    MsgNotify(RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_TAG_ACCESS);
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_ANTENNA_CYCLE_END :
//                    Rfid.st_RfidSpPkt_AntCycEnd = (RFID_PACKETMSG_ANTENNA_CYCLE_END_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_ANTENNA_CYCLE_END_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg ANTENNA_CYCLE_END\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_ANTENNA_END :
//                    Rfid.st_RfidSpPkt_AntEnd = (RFID_PACKETMSG_ANTENNA_END_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_ANTENNA_END_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg ANTENNA_END\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_18K6C_INVENTORY_ROUND_END :
//                    Rfid.st_RfidSpPkt_InvenRndEnd = (RFID_PACKETMSG_18K6C_INVENTORY_ROUND_END_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_18K6C_INVENTORY_ROUND_END_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg 18K6C_INVENTORY_ROUND_END\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_END :
//                    Rfid.st_RfidSpPkt_InvenCycEnd = (RFID_PACKETMSG_INVENTORY_CYCLE_END_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_INVENTORY_CYCLE_END_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg INVENTORY_CYCLE_END\n");
                    MsgNotify(RFID_MSGID.RFID_PACKET_TYPE_MSGID_INVENTORY_CYCLE_END);
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CARRIER_INFO :
//                    Rfid.st_RfidSpPkt_CarrierInfo = (RFID_PACKETMSG_CARRIER_INFO_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_CARRIER_INFO_T));
//                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                    Datalog.LogStr("RfidDev msg CARRIER_INFO\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_NONCRITICAL_FAULT :
                    Rfid.st_RfidSpPkt_NonCritFault = (RFID_PACKETMSG_NONCRITICAL_FAULT_T) Marshal.PtrToStructure(MsgLParam, typeof(RFID_PACKETMSG_NONCRITICAL_FAULT_T));
                     Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
                     MsgNotify(RFID_MSGID.RFID_PACKET_TYPE_MSGID_NONCRITICAL_FAULT);
                    Datalog.LogStr("RfidDev msg NONCRITICAL_FAULT\n");
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TAGRATE:
                    UInt32 Period = (UInt32)MsgWParam;
                    UInt32 Cnt = (UInt32)MsgLParam;
                    Datalog.LogStr("RfidDev msg CUSTOM_TAGRATE\n");
                    MsgNotifyTagRate(RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TAGRATE,
                        Period, Cnt);
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_RSSI:
                    UInt32 Rssi = ((UInt32)MsgLParam & 0x0000ffff);
                    Datalog.LogStr("RfidDev msg CUSTOM_RSSI\n");
                    MsgNotifyFloat(RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_RSSI, 
                        RfidSp.RssiRegToDb((ushort)Rssi));
                    break;
                case RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRdRate:
                    Datalog.LogStr("RfidDev msg CustomTagInvtryRdRate\n");
                    MsgNotifyHRes(RFID_MSGID.RFID_REQEND_TYPE_MSGID_CustomTagInvtryRdRate, (HRESULT_RFID)MsgLParam);
                    break;
                case RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TEMP:
                    Rfid.st_RfidSpPkt_CustomTemp = (RFID_CUSTOMMSG_TEMP)Marshal.PtrToStructure(MsgLParam, typeof(RFID_CUSTOMMSG_TEMP));
                    Marshal.FreeHGlobal(MsgLParam);
                    MsgNotify(RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_TEMP);
                    // time elapsed is piggybacked here
                    MsgNotifyUINT32(RFID_MSGID.RFID_PACKET_TYPE_MSGID_CUSTOM_ELAPSEDTM, (UInt32)MsgWParam);
#if DEBUG // only enable in DEBUG version (too many message for production version)
                    Datalog.LogStr("RfidDev msg CUSTOM_TEMP\n");
#endif
                    break;
                ////////////////////////////////////////
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_SetAllTaglist:
                    // tisiz  = (UInt32) (msg.WParam);
                    Marshal.FreeHGlobal(MsgLParam); //free the IntPtr
                    Datalog.LogStr("RfidMw msg TagInv_SetAllTaglist\n");
                    break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_AddATag: //this msg may represent a response or rfid_lib indicaiton 
                    //iRet = (int) (msg.WParam); 
                    Rfid.st_RfidMw_AddATag_PecRec = (PECRECORD_T)Marshal.PtrToStructure(MsgLParam, typeof(PECRECORD_T));
                    Marshal.FreeHGlobal(MsgLParam); /// RfidSp.f_RfidSpDll_HeapFree(ref MsgLParam);
#if DEBUG // only enable in DEBUG version (too many message for production version)
                    Datalog.LogStr1("RfidMw msg TagInv_AddATag:Pc-Epc"+Rfid.st_RfidMw_AddATag_PecRec.m_Pc +
                        " - " + Rfid.st_RfidMw_AddATag_PecRec.m_Epc.m_MSB + "  "
                        + Rfid.st_RfidMw_AddATag_PecRec.m_Epc.m_CSB + "  "
                        + Rfid.st_RfidMw_AddATag_PecRec.m_Epc.m_LSB + "  "
                        + " Cnt" + Rfid.st_RfidMw_AddATag_PecRec.m_Cnt + "\n"); //" + iRet +"
                            //Datalog.LogStr1("RfidMw msg TagInv_AddATag\n");
#endif
            
                    MsgNotify(RFID_MSGID.RFIDMW_REQUEST_TYPE_MSGID_TagInv_AddATag);
                    break;

                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_FindATag:
                    iRet = (int)(msg.WParam); 
                    Rfid.st_RfidMw_FindATag_PecRec = (PECRECORD_T)Marshal.PtrToStructure(MsgLParam, typeof(PECRECORD_T));
                    ///For static No need Marshal.FreeHGlobal(MsgLParam);
                    Datalog.LogStr("RfidMw msg TagInv_FindATag:" + iRet + ":Pc-Epc" + Rfid.st_RfidMw_FindATag_PecRec.m_Pc +
                        Rfid.st_RfidMw_FindATag_PecRec.m_Epc.m_MSB + Rfid.st_RfidMw_FindATag_PecRec.m_Epc.m_CSB + Rfid.st_RfidMw_FindATag_PecRec.m_Epc.m_LSB + 
                        " Cnt" + Rfid.st_RfidMw_FindATag_PecRec.m_Cnt + "\n");
                    break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_ClearAllTaglist:
                    // Note: Too lazy to handle failure/success (there is none at this moment)
                    Datalog.LogStr("RfidMw msg TagInv_ClearAllTaglist\n");
                    break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_UpdateAllTaglistToFile:

                    Datalog.LogStr("RfidMw msg TagInv_UpdateAllTaglistToFile\n");
                    break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_GetUpdateTaglist:
                    //Rfid.st_RfidMw_PecRec; 
                  tisiz  = (UInt32) (msg.WParam);
                  if (tisiz > 0)
                  {
                      PECRECORD_T[] aryst_RfidMw_PecRec = new PECRECORD_T[tisiz];
                      hr = CallFlow.f_handlemsg_RfidMw_TagInv_GetTaglist(tisiz, ref MsgLParam,
                        ref aryst_RfidMw_PecRec); /// PECRECORD_T[];
                  };
                  Datalog.LogStr("RfidMw msg TagInv_GetUpdateTaglist\n");
                  break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_GetAllTaglist:
                  //Rfid.st_RfidMw_PecRec; 
                  tisiz  = (UInt32) (msg.WParam);
                  if (tisiz > 0)
                  {
                      UInt16[] PCArr = new UInt16[tisiz];
                      UINT96_T[] EPCArr = new UINT96_T[tisiz];
                      float[] RSSIArr = new float[tisiz];
                      FileTime[] FTArr = new FileTime[tisiz];
                      CallFlow.f_handlemsg_RfidMw_TagInv_GetTaglist( tisiz, ref MsgLParam,
                          PCArr, EPCArr, RSSIArr, FTArr); ///
                      MsgNotifyTagArr(RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_GetAllTaglist,
                          PCArr, EPCArr, RSSIArr, FTArr);
                  }
                  else
                      MsgNotifyTagArr(RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_GetAllTaglist, 
                          null, null, null, null);
                  Datalog.LogStr("RfidMw msg TagInv_GetAllTaglist\n");
                  break;
                case RFID_MSGID.RFIDMW_REQEND_TYPE_MSGID_TagInv_SetMsgMode:
                    Datalog.LogStr("RfidMw msg TagInv_SetMsgMode\n"); 
                    // skip MsgNotify(), assume always successful
                    break;
                ////////////////////////////////////////
                default:    
                    break;
            }
            Debug.Flush(); 
            base.WndProc(ref msg);
        } // WndProc

        private RFID_18K6C_SELECT_CRITERION[] MarshalSelectCriteria (ref RFID_18K6C_SELECT_CRITERION_T criterion)
        {
            // Allocate Array
            RFID_18K6C_SELECT_CRITERION[] SelCritArr = new RFID_18K6C_SELECT_CRITERION[criterion.countCriteria];
            IntPtr Current = criterion.criteria;

            for (int i = 0; i < criterion.countCriteria; i++)
            {
                // TBD: Allocate SelCritArr[i].mask.mask array?
                SelCritArr[i] = (RFID_18K6C_SELECT_CRITERION)Marshal.PtrToStructure(Current, typeof(RFID_18K6C_SELECT_CRITERION));
                Current = (IntPtr)((long)Current + Marshal.SizeOf (SelCritArr[i]));
            }

            return SelCritArr;
        }
    }
    //To send another C#-managed-memory message to other C# component,
    // Please uses // Message msg = Message.Create(MsgWin.Hwnd, MsgWindow.WM_CUSTOMMSG, (IntPtr)e.X, (IntPtr)e.Y);
    // MessageWindow.SendMessage(ref msg);	/// public struct Message

}

