/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Login functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using System.Threading;
using ClsRampdb;
using ClslibSerialNo;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();

            /* Select the prompt message */
            this.UsrNameTxtBx.SelectAll();

        }

        private void SkipButtonClicked(object sender, EventArgs e)
        {
            /* 
            // Close LoginForm and Open MainMenuForm
            MainMenuForm MainMenu = new MainMenuForm();
            MainMenu.Show();
            this.Close();
            */
            this.Close();
            Program.ReqEnd = true;
        }

        private void OnOKButtonClicked(object sender, EventArgs e)
        {
            /* 
              if (VerifyPassword() == true)
                 SkipButtonClicked(sender, e);
             */           

            try
            {
                Cursor.Current = Cursors.WaitCursor;  
                UserPref Pref = UserPref.GetInstance();
                
                Login.urlStr = Pref.ServiceURL;
                Login.OnLineMode = chkOnline.Checked;

                bool IsRegistered = true;
                if (Login.verifyPassword(UsrNameTxtBx.Text, PwdTxtBx.Text))
                {
                    Pref.UserName = UsrNameTxtBx.Text.Trim();
                    Pref.Passwd = PwdTxtBx.Text.Trim();
                    String appCode;
                    DateTime ExpiryDate;
                    string NoOfItemCode;
                    if (chkRegistration.Checked || Pref.Key.Trim().Length ==0 )
                    {
                        IsRegistered = false;  
                    }
                    else if (Encrypt.IsValidKey(Pref.Key, SerialNo.GetDeviceID(),out appCode,out ExpiryDate,out NoOfItemCode))
                    {
                        ///
                        if (appCode.Length != 4 || appCode.Substring(1, 1) != "1")
                        {
                            MessageBox.Show("Invalid Registration key, Please contact your vendor.");
                            IsRegistered = false;
                        }
                        else if (ExpiryDate <= DateTime.Now)
                        {
                            MessageBox.Show("Your subscription has expired. Please contact your vendor.");
                            IsRegistered = false;
                        }
                        else if(ExpiryDate.ToString("dd/MM/yyyy").Trim() != Pref.OnExpired.Trim())
                        {
                            MessageBox.Show("Corrupted Expiry Date.");
                            IsRegistered = false;
                        }
                        else if (Pref.ItemLimit.Trim() != NoOfItemCode.Trim())
                        {
                            MessageBox.Show("Corrupted Item Limit.");
                            IsRegistered = false;
                        }
                        if (IsRegistered)
                        {
                            Int32 NoOfItems = Encrypt.GetMaxNoofItems(Pref.ItemLimit);
                            if (NoOfItems != 0)
                            {
                                if (!Login.OnLineMode)
                                {
                                    Login._ItemLimit = NoOfItems - Assets.getNoOfItems();
                                    if (Login.ItemLimit < 0)
                                    {
                                        MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                                        IsRegistered = false;
                                    }
                                }
                                else
                                {
                                    Login._ItemLimit = NoOfItems;
                                    if (Assets.getNoOfItems() > NoOfItems)
                                    {
                                        MessageBox.Show("You are crossing registered item limit(" + NoOfItems + "). Please upgrade your registration.");
                                        IsRegistered = false;
                                    }
                                }
                            }
                            else
                            {
                                Login._ItemLimit = 50000;
                            }
                        }
                    }
                    Cursor.Current = Cursors.Default;  
                    if (IsRegistered)
                    {
                        MainMenuForm MainMenu = new MainMenuForm();
                        MainMenu.Show();
                        this.Close(); 
                    }
                    else
                    {
                        frmRegister fReg = new frmRegister();
                        fReg.Show();
                        this.Close();
                    }
                    /*
                     * MainMenuForm MainMenu = new MainMenuForm();
                    MainMenu.Show();
                    this.Close(); 
                     * */
                    //this.Enabled = false;
                    //MainMenu.Closed += new EventHandler(MainMenu_Closed);
                }
                else
                {
                    Cursor.Current = Cursors.Default;  
                    if (Login.err.Length != 0)
                        MessageBox.Show(Login.err);
                    else
                        MessageBox.Show("Invalid User Name or Password.");

                    Logger.LogError("Invalid User Name or Password.");

                }
                //Login.OnLineMode = false;// chkOnline.Checked;  
            }

            catch (ApplicationException AP)
            {
                Logger.LogError(AP.Message);  
                Program.ShowError(AP.Message);   
            }
            catch (System.Data.SqlServerCe.SqlCeException sEx)
            {
                Logger.LogError(sEx.Message);  
                Program.ShowError("Data File is not able to access.");
            }
            catch (System.Net.WebException wEx)
            {
                Logger.LogError(wEx.Message);  
                Program.ShowError("Web exception occured.");
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message);  
                Program.ShowError(ep.Message.ToString());
            }

            Cursor.Current = Cursors.Default;  
        }

        void MainMenu_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;
            //throw new NotImplementedException();
        }

        private bool VerifyPassword()
        {
            UserPref Pref = UserPref.GetInstance();

            if (Pref.UserName == null || Pref.UserName.Length == 0)
                return true;
            if (String.Compare(Pref.UserName, UsrNameTxtBx.Text, true) != 0)
            {
                MessageBox.Show("Unrecognized Login Name", "Failed");
                return false;
            }
            if (Pref.Passwd == null || Pref.Passwd.Length == 0)
                return true;
            if (String.Compare(Pref.Passwd, PwdTxtBx.Text, false) != 0)
            {
                MessageBox.Show("Incorrect Password", "Failed");
                return false;
            }

            return true;
        }

        private void OnUsrNameTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void OnPwdTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void PwdTxtBx_TextChanged(object sender, EventArgs e)
        {

        }

        private void LoginForm_Closed(object sender, EventArgs e)
        {
           
        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            lblVersion.Text = UserPref.CurVersionNo;

            UserPref Pref = UserPref.GetInstance();

            UsrNameTxtBx.Text = Pref.UserName;
            PwdTxtBx.Text = Pref.Passwd;

            Logger.enableErrorLogging = Pref.EnableErrorLogging;
        }
    }
}