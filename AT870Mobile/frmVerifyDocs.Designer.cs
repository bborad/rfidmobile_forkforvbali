namespace OnRamp
{
    partial class frmVerifyDocs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVerifyDocs));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.EPCListV = new System.Windows.Forms.ListView();
            this.EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.ScanButton = new System.Windows.Forms.Button();
            this.Read1Button = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.btnScnBarcd = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // EPCListV
            // 
            this.EPCListV.Columns.Add(RowNumHdr);
            this.EPCListV.Columns.Add(this.EPCColHdr);
            this.EPCListV.FullRowSelect = true;
            this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.EPCListV, "EPCListV");
            this.EPCListV.Name = "EPCListV";
            this.EPCListV.View = System.Windows.Forms.View.Details;
            this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(this.EPCColHdr, "EPCColHdr");
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // Read1Button
            // 
            resources.ApplyResources(this.Read1Button, "Read1Button");
            this.Read1Button.Name = "Read1Button";
            this.Read1Button.Click += new System.EventHandler(this.OnRead1ButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            this.PwdReqChkBx.CheckStateChanged += new System.EventHandler(this.PwdReqChkBx_CheckStateChanged);
            // 
            // button1
            // 
            resources.ApplyResources(this.button1, "button1");
            this.button1.Name = "button1";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cboLoc
            // 
            resources.ApplyResources(this.cboLoc, "cboLoc");
            this.cboLoc.Name = "cboLoc";
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.columnHeader2);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // AssetName
            // 
            resources.ApplyResources(this.AssetName, "AssetName");
            // 
            // columnHeader2
            // 
            resources.ApplyResources(this.columnHeader2, "columnHeader2");
            // 
            // btnScnBarcd
            // 
            resources.ApplyResources(this.btnScnBarcd, "btnScnBarcd");
            this.btnScnBarcd.Name = "btnScnBarcd";
            this.btnScnBarcd.Click += new System.EventHandler(this.btnScnBarcd_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // frmVerifyDocs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.cboLoc);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.lstAsset);
            this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.btnScnBarcd);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.Read1Button);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmVerifyDocs";
            this.Load += new System.EventHandler(this.OnTagRdFormLoad);
            this.Closed += new System.EventHandler(this.OnTagRdFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagRdFormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.ColumnHeader EPCColHdr;
        private System.Windows.Forms.Button ScanButton;
        private System.Windows.Forms.Button Read1Button;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader AssetName;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Button btnScnBarcd;
        private System.Windows.Forms.Button btnClear;
    }
}