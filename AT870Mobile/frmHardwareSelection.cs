﻿using System;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ReaderTypes;

namespace OnRamp
{
    public partial class frmHardwareSelection : Form
    {
        public bool hwSelected = false;

        public frmHardwareSelection()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            if (rdBtnAT870.Checked == false && rdBtnCS101.Checked == false)
            {
                hwSelected = false;
                MessageBox.Show("Please select the required Reader Type");
                return;
            }
            else
            {
                UserPref Pref = UserPref.GetInstance();
                if (rdBtnCS101.Checked)
                {
                    Pref.SelectedHardware =  HardwareSelection.CS101Reader;
                    Pref.RdrName = "CS101-1";
                }
                else if (rdBtnAT870.Checked)
                {
                    Pref.SelectedHardware =  HardwareSelection.AT870Reader;
                    Pref.RdrName = "AT870-1";
                }
                hwSelected = true;

                this.Close();
            }
        }

        private void frmHardwareSelection_Load(object sender, EventArgs e)
        {
            UserPref Pref = UserPref.GetInstance();
            if (Pref.SelectedHardware != HardwareSelection.None)
            {
                if (Pref.SelectedHardware == HardwareSelection.CS101Reader)
                {
                    rdBtnCS101.Checked = true;
                }
                else if (Pref.SelectedHardware == HardwareSelection.AT870Reader)
                {
                    rdBtnAT870.Checked = false;
                }
                hwSelected = true;
            }
            else
            {
                rdBtnCS101.Checked = false;
                rdBtnAT870.Checked = false;

                hwSelected = false;
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}