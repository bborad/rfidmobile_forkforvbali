namespace OnRamp
{
    partial class EPCMaskInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(EPCMaskInput));
            this.EPCChkBx = new System.Windows.Forms.CheckBox();
            this.PCChkBx = new System.Windows.Forms.CheckBox();
            this.EPCTxtBx = new System.Windows.Forms.TextBox();
            this.PCTxtBx = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // EPCChkBx
            // 
            resources.ApplyResources(this.EPCChkBx, "EPCChkBx");
            this.EPCChkBx.Name = "EPCChkBx";
            this.EPCChkBx.CheckStateChanged += new System.EventHandler(this.OnEPCChkBxChecked);
            // 
            // PCChkBx
            // 
            resources.ApplyResources(this.PCChkBx, "PCChkBx");
            this.PCChkBx.Name = "PCChkBx";
            this.PCChkBx.CheckStateChanged += new System.EventHandler(this.OnPCChkBxChecked);
            // 
            // EPCTxtBx
            // 
            resources.ApplyResources(this.EPCTxtBx, "EPCTxtBx");
            this.EPCTxtBx.Name = "EPCTxtBx";
            this.EPCTxtBx.TextChanged += new System.EventHandler(this.OnEPCTxtBxChanged);
            this.EPCTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.EPCTxtBx.LostFocus += new System.EventHandler(this.OnEPCTxtBxFocusLost);
            // 
            // PCTxtBx
            // 
            resources.ApplyResources(this.PCTxtBx, "PCTxtBx");
            this.PCTxtBx.Name = "PCTxtBx";
            this.PCTxtBx.TextChanged += new System.EventHandler(this.OnPCTxtBxChanged);
            this.PCTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.PCTxtBx.LostFocus += new System.EventHandler(this.OnPCTxtBxFocusLost);
            // 
            // EPCMaskInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.EPCChkBx);
            this.Controls.Add(this.PCChkBx);
            this.Controls.Add(this.EPCTxtBx);
            this.Controls.Add(this.PCTxtBx);
            this.Name = "EPCMaskInput";
            resources.ApplyResources(this, "$this");
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.CheckBox EPCChkBx;
        private System.Windows.Forms.CheckBox PCChkBx;
        private System.Windows.Forms.TextBox EPCTxtBx;
        private System.Windows.Forms.TextBox PCTxtBx;
    }
}
