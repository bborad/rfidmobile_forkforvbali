namespace OnRamp
{
    partial class frmReferenceNo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.txtReferenceNo = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnOk = new System.Windows.Forms.Button();
            this.Read1Button = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtReferenceNo
            // 
            this.txtReferenceNo.Location = new System.Drawing.Point(6, 101);
            this.txtReferenceNo.Name = "txtReferenceNo";
            this.txtReferenceNo.Size = new System.Drawing.Size(229, 23);
            this.txtReferenceNo.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(30, 77);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(129, 21);
            this.label1.Text = "Enter Reference No.";
            // 
            // btnOk
            // 
            this.btnOk.Location = new System.Drawing.Point(81, 130);
            this.btnOk.Name = "btnOk";
            this.btnOk.Size = new System.Drawing.Size(62, 21);
            this.btnOk.TabIndex = 1;
            this.btnOk.Text = "OK";
            this.btnOk.Click += new System.EventHandler(this.btnOk_Click);
            // 
            // Read1Button
            // 
            this.Read1Button.Enabled = false;
            this.Read1Button.Location = new System.Drawing.Point(55, 157);
            this.Read1Button.Name = "Read1Button";
            this.Read1Button.Size = new System.Drawing.Size(104, 21);
            this.Read1Button.TabIndex = 2;
            this.Read1Button.Text = "Read Selected";
            this.Read1Button.Visible = false;
            // 
            // frmReferenceNo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.Read1Button);
            this.Controls.Add(this.btnOk);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtReferenceNo);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmReferenceNo";
            this.Text = "Reference No.";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox txtReferenceNo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnOk;
        private System.Windows.Forms.Button Read1Button;
    }
}