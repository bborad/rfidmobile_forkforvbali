namespace OnRamp
{
    partial class FieldServiceSync
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.lblStatus = new System.Windows.Forms.Label();
            this.syncProgress = new System.Windows.Forms.ProgressBar();
            this.btnStart = new System.Windows.Forms.Button();
            this.cboLoc = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lnkLocName = new System.Windows.Forms.LinkLabel();
            this.btnSelLoc = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // lblStatus
            // 
            this.lblStatus.Location = new System.Drawing.Point(81, 115);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(75, 20);
            this.lblStatus.Text = "Status";
            // 
            // syncProgress
            // 
            this.syncProgress.Location = new System.Drawing.Point(6, 138);
            this.syncProgress.Name = "syncProgress";
            this.syncProgress.Size = new System.Drawing.Size(218, 26);
            // 
            // btnStart
            // 
            this.btnStart.Location = new System.Drawing.Point(53, 185);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(104, 26);
            this.btnStart.TabIndex = 2;
            this.btnStart.Text = "Start";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // cboLoc
            // 
            this.cboLoc.Location = new System.Drawing.Point(208, 109);
            this.cboLoc.Name = "cboLoc";
            this.cboLoc.Size = new System.Drawing.Size(26, 23);
            this.cboLoc.TabIndex = 51;
            this.cboLoc.Visible = false;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(78, 48);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(78, 23);
            this.label2.Text = "Location";
            this.label2.ParentChanged += new System.EventHandler(this.label2_ParentChanged);
            // 
            // lnkLocName
            // 
            this.lnkLocName.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Underline);
            this.lnkLocName.ForeColor = System.Drawing.Color.Black;
            this.lnkLocName.Location = new System.Drawing.Point(6, 74);
            this.lnkLocName.Name = "lnkLocName";
            this.lnkLocName.Size = new System.Drawing.Size(181, 20);
            this.lnkLocName.TabIndex = 55;
            this.lnkLocName.Text = "All Locations";
            this.lnkLocName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.lnkLocName.Click += new System.EventHandler(this.lnkLocName_Click);
            // 
            // btnSelLoc
            // 
            this.btnSelLoc.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular);
            this.btnSelLoc.Location = new System.Drawing.Point(192, 74);
            this.btnSelLoc.Name = "btnSelLoc";
            this.btnSelLoc.Size = new System.Drawing.Size(41, 20);
            this.btnSelLoc.TabIndex = 56;
            this.btnSelLoc.Text = "Select";
            this.btnSelLoc.Click += new System.EventHandler(this.btnSelLoc_Click);
            // 
            // FieldServiceSync
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.AutoScroll = true;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(238, 270);
            this.Controls.Add(this.btnSelLoc);
            this.Controls.Add(this.lnkLocName);
            this.Controls.Add(this.cboLoc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.syncProgress);
            this.Controls.Add(this.lblStatus);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FieldServiceSync";
            this.Text = "Field Service Synchronize";
            this.Load += new System.EventHandler(this.FieldServiceSync_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.ProgressBar syncProgress;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.ComboBox cboLoc;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.LinkLabel lnkLocName;
        private System.Windows.Forms.Button btnSelLoc;
    }
}