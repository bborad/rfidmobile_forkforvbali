/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For First Menu Display functionality
 **************************************************************************************/


using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;
using ClsRampdb;
using System.Reflection;
using ClsLibBKLogs;
using ClsReaderLib;using ClsReaderLib.Devices;
using CS101UILib;

namespace OnRamp
{
    public partial class MainMenuForm : Form
    {
        private Bitmap LogoImg;
        // don't forget to add namespace before the file name!
        private string strLogoImg = @"OnRamp.OnRampMenuLogo.jpg";

        public static class MenuOption
        {
            //level0
            public const String FieldService = "fiedlservice";
            public const String Dispatch = "dispatch";

            public const String Inventory = "inventory";
            public const String SearchItem = "searchitem";
            public const String Synchronize = "synchronize";
            //More Button

            //level01
            public const String AddAsset = "addasset";
            public const String AddEmployee = "addemployee";
            public const String AddLocation = "addlocation";
            public const String WriteTag = "writetag";
            //Back & More Button

            //level1
            public const String ReadBarCode = "readbarcode";
            public const String Configuration = "configuration";
            public const String InventorySync = "syncinventory";
            public const String FieldServiceSync = "fieldservicesync";
            //Back

        }

        //MenuOption mop;

        public MainMenuForm()
        {
            InitializeComponent();

            //if (UserPref.AppModuleImplementation)
            //{
            //    GetNSetAppModules();
            //}

            //SetLogoImage();

            MoreBttnInitState();

            #region "Make Visible false all buttons."
            this.TagRdBttn.Visible = false; this.TagWrBttn.Visible = false; this.TagInvtryBttn.Visible = false; this.TagRangeBttn.Visible = false;
            this.TagSrchBttn.Visible = false; this.TagCommBttn.Visible = false; this.TagAuthBttn.Visible = false; this.DBMgmtBttn.Visible = false;
            this.DevStatBttn.Visible = false; this.BCScnBttn.Visible = false;
            this.TagPermBttn.Visible = false; this.CertBttn.Visible = false; this.SysCfgBttn.Visible = false; this.DefSettingBttn.Visible = false;
            this.btnAddLocation.Visible = false; this.btnAddEmployee.Visible = false; btnInventory.Visible = false; btnDispatch.Visible = false;
            this.btnFieldService.Visible = false; this.btnSyncFieldService.Visible = false;
            #endregion

            DataTable dtOption = Login.getMenuOptionTable();

            MoreBttn.Visible = true;

            l0Buttons = new Button[5];
            l01Buttons = new Button[5];
            l1Buttons = new Button[5];

            /*
            if (dtOption.Rows.Count <= 5)
            {
                l0Buttons = new Button[dtOption.Rows.Count];
                MoreBttn.Visible = false;
            }
            else
            {
                l0Buttons = new Button[4];
                if (dtOption.Rows.Count <= 9)
                {
                    l01Buttons = new Button[dtOption.Rows.Count - 4];
                }
                else
                {
                    l01Buttons = new Button[4];
                    l1Buttons = new Button[dtOption.Rows.Count - 5];
                }
            }
            */

            //Int16 optionCount = 0;
            this.btnFieldService.Visible = true; this.btnDispatch.Visible = true;
            this.TagInvtryBttn.Visible = true; this.TagSrchBttn.Visible = true;
            this.TagRdBttn.Visible = true; btnInventory.Visible = true;
            this.btnSync.Visible = true; MoreBttn.Visible = true;

            //this.TagRdBttn.Visible = true;this.TagWrBttn.Visible = true; this.TagInvtryBttn.Visible = true;
            //this.TagSrchBttn.Visible = true; this.BCScnBttn.Visible = true; this.btnAddLocation.Visible = true;
            //this.btnAddEmployee.Visible = true; this.DevStatBttn.Visible = true; this.btnInventory.Visible = true;    

            //this.TagRdBttn.Enabled = false; this.TagWrBttn.Enabled = false; this.TagInvtryBttn.Enabled = false;
            //this.TagSrchBttn.Enabled = false; this.BCScnBttn.Enabled = false; this.btnAddLocation.Enabled = false;
            //this.btnAddEmployee.Enabled = false; this.DevStatBttn.Enabled = false; this.btnInventory.Enabled = false;


            if (!Login.OnLineMode)
            {
                btnSync.Enabled = false;
                btnInventory.Enabled = false;
                btnSyncFieldService.Enabled = false;
                btnDispatch.Enabled = true;
            }
            else
            {
                btnDispatch.Enabled = true;
            }

            //if (Login.appModules == AppModules.RemoveDispatch || Login.appModules == AppModules.RemoveDispatchandFS)
            //if (AppModules.RemoveDispatch)
            //{
            //    btnDispatch.Enabled = false; 
            //}

            ////if (Login.appModules == AppModules.RemoveFieldService || Login.appModules == AppModules.RemoveDispatchandFS)
            //if (AppModules.RemoveFieldService)
            //{
            //    btnFieldService.Enabled = false;
            //    btnSyncFieldService.Enabled = false; 
            //}            

            //I assumed here that there should not be increase option more than 10, else we need to extra code.



            foreach (DataRow dr in dtOption.Rows)
            {
                switch (Convert.ToString(dr["Name"]).ToLower().Trim())
                {
                    case MenuOption.FieldService:
                        this.btnFieldService.Enabled = true;
                        break;
                    case MenuOption.Dispatch:
                        this.btnDispatch.Enabled = true;
                        break;
                    case MenuOption.Inventory:
                        this.TagRdBttn.Enabled = true;
                        break;
                    case MenuOption.SearchItem:
                        this.TagSrchBttn.Enabled = true;
                        break;
                    case MenuOption.Synchronize:
                        if (Login.OnLineMode)
                            this.btnSync.Enabled = true;
                        break;
                    case MenuOption.AddAsset:
                        this.TagInvtryBttn.Enabled = true;
                        break;
                    case MenuOption.AddLocation:
                        this.btnAddLocation.Enabled = true;
                        break;
                    case MenuOption.AddEmployee:
                        this.btnAddEmployee.Enabled = true;
                        break;
                    case MenuOption.WriteTag:
                        this.TagWrBttn.Enabled = true;
                        break;
                    case MenuOption.FieldServiceSync:
                        if (Login.OnLineMode)
                            this.btnInventory.Enabled = true;
                        break;
                    case MenuOption.InventorySync:
                        if (Login.OnLineMode)
                            this.btnInventory.Enabled = true;
                        break;
                    case MenuOption.ReadBarCode:
                        this.BCScnBttn.Enabled = true;
                        break;
                    case MenuOption.Configuration:
                        this.DevStatBttn.Enabled = true;
                        break;
                    default:
                        break;
                }
            }

            if (UserPref.AppModuleImplementation)
            {
                GetNSetAppModules();
            }

            if (!Login.OnLineMode)
            {
                btnSync.Enabled = false;
                btnInventory.Enabled = false;
                btnSyncFieldService.Enabled = false;
            }

            SetLogoImage();

            l0Buttons[0] = this.btnFieldService;
            l0Buttons[1] = this.btnDispatch;
            l0Buttons[2] = this.TagRdBttn;
            l0Buttons[3] = this.TagSrchBttn;
            l0Buttons[4] = this.btnSync;

            l01Buttons[0] = this.TagInvtryBttn;
            l01Buttons[1] = this.btnAddLocation;
            l01Buttons[2] = this.btnAddEmployee;
            l01Buttons[3] = this.TagWrBttn;

            l1Buttons[0] = this.btnSyncFieldService;
            l1Buttons[1] = this.btnInventory;
            l1Buttons[2] = this.BCScnBttn;
            l1Buttons[3] = this.DevStatBttn;

            MoreBttn.Enabled = true;
            btnBack.Enabled = true;

            //l0Buttons  = new Button[10] 
            //{ 
            //    this.TagRdBttn, this.TagWrBttn, this.TagInvtryBttn, this.TagRangeBttn,
            //    this.TagSrchBttn, this.TagCommBttn, this.TagAuthBttn, this.DBMgmtBttn,
            //    this.DevStatBttn,  this.BCScnBttn,
            //};

            //l1Buttons = new Button[4] {
            //    this.TagPermBttn, this.CertBttn, this.SysCfgBttn, this.DefSettingBttn,
            //    /*this.RndBttn*/
            //};

            // Level1 button location finalized here

            //Set Left and Top position of button by refrence to level0
            for (int b = 0; b < 4; b++)
            {

                if (l1Buttons[b] != null)
                    l1Buttons[b].Location = l0Buttons[b].Location;
                if (l01Buttons[b] != null)
                    l01Buttons[b].Location = l0Buttons[b].Location;

            }

            //

            this.Closed += new EventHandler(OnMainMenuClosed);

#if BURN_IN
            BuildCfgLbl.Text = "Burn-in";
            BuildCfgLbl.ForeColor = Color.Red;
            BuildCfgLbl.Visible = true;
#elif DEBUG
            BuildCfgLbl.Text = "Dbg";
            BuildCfgLbl.ForeColor = Color.Red;
            BuildCfgLbl.Visible = true;
#else
            BuildCfgLbl.Visible = false;
#endif
            VerLbl.Text = "V 0." + Program.SvnVer;
            VerLbl.ForeColor = Color.Blue;
            VerLbl.Visible = true;
        }

        void SetLogoImage()
        {
            try
            {
                /* For this to work, the splash image must be configured
                 * as 'embedded resource'
                 */
                if (UserPref.GetInstance().SelectedHardware == HardwareSelection.AT870Reader)
                {
                    if (UserPref.forClient == UserPref.Client.OnRamp)
                    {
                        strLogoImg = @"OnRamp.OnRampMenuLogo_AT870.jpg";
                        pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
                    }
                    else if (UserPref.forClient == UserPref.Client.SmartTrack)
                    {
                        strLogoImg = @"OnRamp.STMenuLogo_AT870.jpg";
                        pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
                    }
                }
                else
                {
                    if (UserPref.forClient == UserPref.Client.OnRamp)
                    {
                        strLogoImg = @"OnRamp.OnRampMenuLogo.jpg";
                        pictureBox1.SizeMode = PictureBoxSizeMode.Normal;
                    }
                    else if (UserPref.forClient == UserPref.Client.SmartTrack)
                    {
                        strLogoImg = @"OnRamp.STMenuLogo.jpg";
                        pictureBox1.SizeMode = PictureBoxSizeMode.CenterImage;
                    }
                }

                LogoImg = new Bitmap(Assembly.GetExecutingAssembly()
                .GetManifestResourceStream(strLogoImg));

                pictureBox1.Image = Image.FromHbitmap(LogoImg.GetHbitmap());

            }
            catch (Exception eLogo)
            {
                // Console.WriteLine(eLogo.ToString());
                Logger.LogError(eLogo.Message);
            }
            finally
            {
                /* center image */
            }
        }

        void GetNSetAppModules()
        {
            try
            {
                int index;
                UserPref Pref = UserPref.GetInstance();
                Pref.GetAppModules();
                // AppModules.RemoveDispatch = Pref.RemoveDispatch;
                // AppModules.RemoveFieldService = Pref.RemoveFieldService;

                char[] arrAppcodes = Pref.allApplicationModules.ToCharArray();

                foreach (Control ctrl in this.Controls)
                {
                    try
                    {
                        if (ctrl.GetType().FullName == "System.Windows.Forms.Button")
                        {
                            if (ctrl.Tag != null && ctrl.Tag.ToString() != "")
                            {

                                index = Convert.ToInt32(ctrl.Tag);
                                if (arrAppcodes[index - 1] == '1')
                                {
                                    ctrl.Enabled = true;
                                }
                                else
                                {
                                    ctrl.Enabled = false;
                                }

                            }
                            else
                                ctrl.Enabled = false;
                        }
                    }
                    catch (Exception ex)
                    {
                        ctrl.Enabled = false;
                        ClsLibBKLogs.Logger.LogError("Error in enabling/disabling buttons. \n" + ex.Message);
                    }
                }

                if (arrAppcodes[Pref.ImmunoSearch.ModuleNo - 1] == '1')
                {
                    Pref.ImmunoSearch.Enable = true;
                    AppModules.ImmunoSearch = Pref.ImmunoSearch.Enable;
                }

                if (arrAppcodes[Pref.VernonFlag.ModuleNo - 1] == '1')
                {
                    Pref.VernonFlag.Enable = true;
                    AppModules.VernonFlag = Pref.VernonFlag.Enable;
                }

            }
            catch (Exception ex)
            {
                ClsLibBKLogs.Logger.LogError("Error in enabling/disabling buttons. \n" + ex.Message);
            }
        }

        private void OnMainMenuLoad(object sender, EventArgs e)
        {
            this.Enabled = false; // Prevent user from clicking on MainMenu buttons

            applyRFIDCfgDlg = new TransientMsgDlg(0);
            applyRFIDCfgDlg.TopMost = true; // on top of other windows
            applyRFIDCfgDlg.Closing += new CancelEventHandler(OnApplyRFIDCfgDlgClosing);
            applyRFIDCfgDlg.SetDpyMsg("Applying RFID Config parameters...", "Notice");
            applyRFIDCfgDlg.Show();
            if (!ApplyRFIDConfig(RFIDConfigApplyCb))
            {
                applyRFIDCfgDlg.SetDpyMsg("Unable to apply RFID Config parameters.", "Warning Notice");
                applyRFIDCfgDlg.Closing -= OnApplyRFIDCfgDlgClosing;
                this.Enabled = true;
            }
            cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
            cbGoOnline.Checked = Login.OnLineMode;
            cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);

            ClsHotkey.AddHotKeyDelegate(HotkeyNotify);

            //SystemState s = new SystemState(SystemProperty.ActiveSyncStatus);
            //s.Changed += new ChangeEventHandler(s_Changed);

        }

        //void s_Changed(object sender, ChangeEventArgs args)
        //{

        //}

        private void OnMainMenuClosing(object sender, CancelEventArgs e)
        {
            // confirm application
            DialogResult res = Program.AskUserConfirm("This would quit application\r\nAre you sure?");

            e.Cancel = (res == DialogResult.Cancel);
            if (e.Cancel == false)
                ClsHotkey.SubHotKeyDelegate(HotkeyNotify);
        }

        private void OnMainMenuClosed(object sender, EventArgs e)
        {
            try
            {
                ReaderFactory.Dispose();
            }
            catch
            {
            }
            Program.ReqEnd = true;
        }


        private void OnFormResize(object sender, EventArgs e)
        {
            // TBD: Move the links to fit portrait and landscape modes.
            // Somehow this event happen too many times that expected
            Control control = (Control)sender;
            Console.WriteLine("Form Resized to  {0},{0} {0}x{0}", control.Location.X,
                control.Location.Y, control.Width, control.Height);
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void OnTagRdClicked(object sender, EventArgs e)
        {
            // Open new Tag Read window
            TagRdForm TagRdFm = new TagRdForm();
            TagRdFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagRdFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }



        private void OnTagInvtryClicked(object sender, EventArgs e)
        {
            //// Open new Tag Invtry window
            //TagInvtryForm TagInvtryFm = new TagInvtryForm();
            //TagInvtryFm.Show();
            //// disable form until this new form closed
            //this.Enabled = false;
            //TagInvtryFm.Closed += new EventHandler(this.OnOperFrmClosed);

            // Open new Tag Write window
            TagWrForm TagWrFm = new TagWrForm();
            TagWrFm.OpenMode = TagWrForm.FormType.AddAsset;
            TagWrFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagWrFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnTagRangeClicked(object sender, EventArgs e)
        {
            // Open new Tag Invtry window
            TagRangingForm TagRangingFm = new TagRangingForm();
            TagRangingFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnSysCfgClicked(object sender, EventArgs e)
        {
            // disable form until this new form closed
            this.Enabled = false;
            // Open new System Configuration window
            SettingsForm SettingsFm = new SettingsForm();
            SettingsFm.Closed += new EventHandler(this.OnOperFrmClosed);
            SettingsFm.Show();
        }

        private void OnQuitClicked(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OnTagWrClicked(object sender, EventArgs e)
        {
            // Open new Tag Write window
            TagWrForm TagWrFm = new TagWrForm();
            TagWrFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagWrFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnCertClicked(object sender, EventArgs e)
        {
            // Open new Certification Window
            CertificationForm CertFm = new CertificationForm();
            CertFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            CertFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnTagAuthenClicked(object sender, EventArgs e)
        {
            // Open new Tag Authentication Window
            TagAuthenForm TagAuthenFm = new TagAuthenForm();
            TagAuthenFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagAuthenFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        #region More/Less Button
        enum ExtendedMenuState
        {
            level0,
            level1,
            level01
        };

        private void MoreBttnInitState()
        {
            ExtendedMenuState state = ExtendedMenuState.level0;
            MoreBttn.Tag = state;
            btnBack.Visible = false;
        }

        private ExtendedMenuState MoreBttnState()
        {
            ExtendedMenuState state = ExtendedMenuState.level0;

            if (MoreBttn.Tag is ExtendedMenuState)
            {
                state = (ExtendedMenuState)MoreBttn.Tag;
            }
            else
                throw new ApplicationException("More Button Tag is not of ExtendedMenuState type");

            return state;
        }

        private void MoreBttnSetState(ExtendedMenuState newState)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenuForm));

            MoreBttn.Tag = newState;
            switch (newState)
            {
                case ExtendedMenuState.level0:
                    //restore to original
                    MoreBttn.Visible = true;
                    MoreBttn.Text = resources.GetString("MoreBttn.Text");
                    btnBack.Visible = false;
                    btnRecieveDispatch.Visible = false;
                    break;
                case ExtendedMenuState.level01:
                    //restore to original
                    MoreBttn.Visible = true;
                    btnBack.Visible = true;
                    btnRecieveDispatch.Visible = false;
                    MoreBttn.Text = resources.GetString("MoreBttn.Text");
                    break;
                case ExtendedMenuState.level1:
                    //MoreBttn.Text = "Back...";
                    MoreBttn.Text = resources.GetString("MoreBttn.Text");
                    MoreBttn.Visible = false;
                    btnRecieveDispatch.Visible = true;
                    break;

            }
        }

        private Button[] l0Buttons;

        private Button[] l1Buttons;

        private Button[] l01Buttons;


        // Two State: More... or Less...
        private void OnMoreClicked(object sender, EventArgs e)
        {
            #region "commented..."
            //Developed By Cherry, Commented by deepanshu due to add more screen date May 2009
            // if (MoreBttn.Text.ToLower().Trim() == "more...")
            // {
            //     MoreBttn.Text = "Back...";

            //     this.TagInvtryBttn.Visible = false; 
            //     this.TagSrchBttn.Visible = false;
            //     this.TagRdBttn.Visible = false;
            //     btnInventory.Visible = false;
            //     this.btnSync.Visible = false; 

            //     this.btnAddLocation.Visible = true; this.btnAddEmployee.Visible = true;
            //     this.TagWrBttn.Visible = true; this.DevStatBttn.Visible = true;
            //     //this.BCScnBttn.Visible = true;
            //     this.btnDispatch.Visible = true; 

            //     //this.TagRangeBttn.Visible = false;
            //     //this.TagCommBttn.Visible = false;
            //     //this.TagAuthBttn.Visible = false; this.DBMgmtBttn.Visible = false;
            //     //this.TagPermBttn.Visible = false; this.CertBttn.Visible = false; 
            //     //this.SysCfgBttn.Visible = false; this.DefSettingBttn.Visible = false;
            // }
            //else
            // {
            //     MoreBttn.Text = "More...";

            //     this.TagInvtryBttn.Visible = true; this.TagSrchBttn.Visible = true;
            //     this.TagRdBttn.Visible = true; btnInventory.Visible = true;
            //     this.btnSync.Visible = true; MoreBttn.Visible = true;

            //     this.btnAddLocation.Visible = false; this.btnAddEmployee.Visible = false;
            //     this.TagWrBttn.Visible = false; this.DevStatBttn.Visible = false;
            //     //this.BCScnBttn.Visible = false;
            //     this.btnDispatch.Visible = false;  
            // }
            #endregion
            //*********Commented by cherry on 13 Aug 08
            int b;
            switch (MoreBttnState())
            {
                case ExtendedMenuState.level0: //First Screen
                    // Hide Button 1 - 6
                    for (b = 0; b < l0Buttons.Length; b++)
                        if (l0Buttons[b] != null)
                            l0Buttons[b].Visible = false;
                    // Show Button 7 - 12
                    for (b = 0; b < l01Buttons.Length; b++)
                        if (l01Buttons[b] != null)
                            l01Buttons[b].Visible = true;
                    MoreBttnSetState(ExtendedMenuState.level01);
                    break;
                case ExtendedMenuState.level01: //Second Screen
                    // Hide Button 7 - 12
                    for (b = 0; b < l01Buttons.Length; b++)
                        if (l01Buttons[b] != null)
                            l01Buttons[b].Visible = false;

                    if ((Button)sender == btnBack)
                    {
                        for (b = 0; b < l0Buttons.Length; b++)
                            if (l0Buttons[b] != null)
                                l0Buttons[b].Visible = true;
                        MoreBttnSetState(ExtendedMenuState.level0);
                    }
                    else //Go Forward
                    {
                        // Show Button 13 - 18
                        for (b = 0; b < l1Buttons.Length; b++)
                            if (l1Buttons[b] != null)
                                l1Buttons[b].Visible = true;
                        MoreBttnSetState(ExtendedMenuState.level1);
                    }
                    break;
                case ExtendedMenuState.level1: //Third Screen
                    // Hide Button 13 - 18
                    for (b = 0; b < l1Buttons.Length; b++)
                        if (l1Buttons[b] != null)
                            l1Buttons[b].Visible = false;
                    // Show Button 7 - 12
                    for (b = 0; b < l01Buttons.Length; b++)
                        if (l01Buttons[b] != null)
                            l01Buttons[b].Visible = true;
                    MoreBttnSetState(ExtendedMenuState.level01);
                    break;
            }
            //******End comment
        }
        #endregion

        private void OnBCScnClicked(object sender, EventArgs e)
        {
            // Open new BarCode Scanning Window
            BCScanForm BCSFm = new BCScanForm();
            BCSFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            BCSFm.Closed += new EventHandler(this.OnOperFrmClosed);

        }

        private void OnTagSearchClicked(object sender, EventArgs e)
        {
            //// Open new Tag Search Window
            //TagSearchForm TagSrchFm = new TagSearchForm();
            //TagSrchFm.Show();
            //this.Enabled = false;
            //TagSrchFm.Closed += new EventHandler(this.OnOperFrmClosed);

            //if (AppModules.ImmunoSearch)
            //{
            //    frmSearchAssetImmuno sAsset = new frmSearchAssetImmuno();
            //    sAsset.Show();
            //    this.Enabled = false;
            //    sAsset.Closed += new EventHandler(this.OnOperFrmClosed);
            //}
            //else
            //{
            //    frmSearchAssetNew sAsset = new frmSearchAssetNew();
            //    sAsset.Show();
            //    this.Enabled = false;
            //    sAsset.Closed += new EventHandler(this.OnOperFrmClosed);
            //}

            frmSearchAssetNew sAsset = new frmSearchAssetNew();
            sAsset.Show();
            this.Enabled = false;
            sAsset.Closed += new EventHandler(this.OnOperFrmClosed);

        }

        private void OnTagPermClicked(object sender, EventArgs e)
        {
            TagPermForm TagPermFm = new TagPermForm();
            TagPermFm.Show();
            this.Enabled = false;
            TagPermFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnTagCommClicked(object sender, EventArgs e)
        {
            TagCommForm TagCommFm = new TagCommForm();
            TagCommFm.Show();
            this.Enabled = false;
            TagCommFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnDevStatClicked(object sender, EventArgs e)
        {
            DevStatForm DevStatFm = new DevStatForm();
            this.Enabled = false;
            DevStatFm.Closed += new EventHandler(this.OnOperFrmClosed);
            DevStatFm.Show();
        }

        private void OnDefSettingClicked(object sender, EventArgs e)
        {
            FactoryDefaultsForm FactDefFm = new FactoryDefaultsForm();
            this.Enabled = false;
            FactDefFm.Closed += new EventHandler(this.OnOperFrmClosed);
            FactDefFm.Show();
        }

        private void OnDBMgmtClicked(object sender, EventArgs e)
        {
            DBMgmtForm DBMgmtFm = new DBMgmtForm();
            this.Enabled = false;
            DBMgmtFm.Closed += new EventHandler(this.OnOperFrmClosed);
            DBMgmtFm.Show();
        }

        #region Routines applying  RFIDConfig
        // Apply RFIDConfig settings
        private delegate void RFIDConfigApplyNotify(bool succ, String errMsg);
        private RFIDConfigApplyNotify rfidCfgApplyNotify;
        private TransientMsgDlg applyRFIDCfgDlg = null;
        private void RFIDConfigApplyCb(bool succ, String errMsg)
        {
            if (succ)
            {
                applyRFIDCfgDlg.SetDpyMsg("RFID Config applied", "Notice");
                applyRFIDCfgDlg.SetTimeout(1);
            }
            else
            {
                applyRFIDCfgDlg.SetDpyMsg("Error: " + errMsg, "Error Notice");
                applyRFIDCfgDlg.SetTimeout(0); // display until user close dialog
            }

            applyRFIDCfgDlg.Closing -= OnApplyRFIDCfgDlgClosing; // allow user close the dialog
            this.Enabled = true;  // Re-enable MainMenu
        }


        void OnApplyRFIDCfgDlgClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = true; // prevent user from closing
        }

        public void AntPortCfgSetOneNotify(bool succ, uint portNum, String errMsg)
        {
            // give up
            if (rfidCfgApplyNotify != null)
            {
                rfidCfgApplyNotify(succ, errMsg);
            }
            else
            {
                if (succ)
                {
                    applyRFIDCfgDlg.SetDpyMsg("RFID Config applied", "Notice");
                    applyRFIDCfgDlg.SetTimeout(1);
                }
                else
                {
                    applyRFIDCfgDlg.SetDpyMsg("RFID Config apply failed: " + errMsg, "Fatal Error Notice");
                    applyRFIDCfgDlg.SetTimeout(0);
                }
                applyRFIDCfgDlg.Closing -= OnApplyRFIDCfgDlgClosing;
                this.Enabled = false;
            }
        }

        private void LinkProfSetCb(bool succ, String errMsg)
        {
            if (!succ)
            {
                applyRFIDCfgDlg.SetDpyMsg("Link Profile setup error: " + errMsg, "Error Notice");
                //continue despite error
            }
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.power = (int)(UserPref.GetInstance().AntennaPwr * 10.0F);
            //Rdr.AntPotCfgSetonNotification += AntPortCfgSetOneNotify;
            Rdr.RegisterAntPotCfgSetOneNotificationEvent(AntPortCfgSetOneNotify);
            Rdr.PortNum = 0;
            if (!Rdr.AntPortCfgSetPwr())
            {
                if (rfidCfgApplyNotify != null)
                    rfidCfgApplyNotify(false, "Unable to start Antenna power setup.");
                else
                {
                    applyRFIDCfgDlg.SetDpyMsg("Unable to start Antenna power setup.", "Fatal Error Notice");
                    applyRFIDCfgDlg.SetTimeout(0);
                    applyRFIDCfgDlg.Closing -= OnApplyRFIDCfgDlgClosing;
                }
            }
        }

        private void CustomFreqSetCb(bool succ, String errMsg)
        {
            if (!succ)
            {
                applyRFIDCfgDlg.SetDpyMsg("Frequency setup error: " + errMsg, "Error Notice");
                //continue despite error
            }
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.ProfNum=UserPref.GetInstance().LinkProf;
            //Rdr.LnkProfNumoSetNotification+=LinkProfSetCb;
            Rdr.RegisterLnkProfNumoSetNotificationEvent(LinkProfSetCb);
            if (!Rdr.LinkProfNumSet())
            {
                // give up
                if (rfidCfgApplyNotify != null)
                    rfidCfgApplyNotify(false, "Unable to start Link Profile setup.");
                else
                {
                    applyRFIDCfgDlg.SetDpyMsg("Unable to start Link Profile setup.", "Fatal Error Notice");
                    applyRFIDCfgDlg.SetTimeout(0);
                    applyRFIDCfgDlg.Closing -= OnApplyRFIDCfgDlgClosing;
                }
            }
        }

        /// <summary>
        /// Antenna Power, Frequency and Link Profile
        /// </summary>
        /// <param name="applyCb"></param>
        /// <returns></returns>
        private bool ApplyRFIDConfig(RFIDConfigApplyNotify applyCb)
        {
            rfidCfgApplyNotify = applyCb;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            bool Started = false;
            //UInt32 BandNum = 2;
            Rdr.bandNum = 2;
            UserPref Pref = UserPref.GetInstance();

            // Resolve BandNum Frequency with Preferred Freq Grp if necessary
            if (Rdr.CustomFreqBandNumGet())
            {
                // Limit the Max Antenna Pwr
                if (Rdr.bandNum == RfidSp.AntPwrLimitingBndNum)
                {
                    if (Pref.AntennaPwr > RfidSp.MAX_BAND4_ANTENNA_PWR)
                        Pref.AntennaPwr = RfidSp.MAX_BAND4_ANTENNA_PWR;
                }
                if (Pref.LinkProf == 1)
                {
                    if (RfidSp.LinkProf1xBnds[DevStatForm.FreqBandNumGetIndex((int)Rdr.bandNum)] == false)
                    {
                        Program.ShowWarning("Band number " + Rdr.bandNum + ":\n"
                                                    + "Forcing Link Profile from 1 to 0");
                        Pref.LinkProf = 0;
                    }
                }
                else if (Pref.LinkProf == 4)
                {
                    if (RfidSp.LinkProf4xBnds[DevStatForm.FreqBandNumGetIndex((int)Rdr.bandNum)] == false)
                    {
                        Program.ShowWarning("Band number " + Rdr.bandNum + ":\n"
                                                    + "Forcing Link Profile from 4 to 0");
                        Pref.LinkProf = 0;
                    }
                }
                Datalog.LogStr("DEBUG: BandNumGet : " + Rdr.bandNum);
                // Is preferred FreqGrp available in current FreqBand (HW)
                bool FreqBndInclPrefFreqGrp = false;
                CustomFreqGrp PreferredFreqGrp = Pref.FreqProf;
                int PreferredFreqChn = Pref.SingleFreqChn;
                bool PreferredLBTEn = Pref.EnFreqChnLBT;

                int BndNumIdx = DevStatForm.FreqBandNumGetIndex((int)Rdr.bandNum);
                for (int i = 0; i < DevStatForm.FreqBndCountries[BndNumIdx].Length; i++)
                {
                    if (DevStatForm.FreqBndCountries[BndNumIdx][i] == PreferredFreqGrp)
                    {
                        FreqBndInclPrefFreqGrp = true;
                        break;
                    }
                }
                if (!FreqBndInclPrefFreqGrp)
                {
                    PreferredFreqGrp = DevStatForm.FreqBndCountries[BndNumIdx][0]; // pick the first one
                    PreferredFreqChn = -1;
                    PreferredLBTEn = false; // really?
                    // Modify user preference
                    Pref.FreqProf = PreferredFreqGrp; // Save for F1 hotkey information
                    Pref.SingleFreqChn = PreferredFreqChn;
                    Pref.EnFreqChnLBT = PreferredLBTEn;
                }

                // Avoid Frequency Table setup if possible (takes long time to complete)
                if (Pref.PwrOnDefaultFreqProf == PreferredFreqGrp
                    && Pref.PwrOnDefaultSingleFreqChn == PreferredFreqChn
                    && Pref.PwrOnDefaultChnLBT == PreferredLBTEn)
                {
                    // go setup Link Profile instead
                    Datalog.LogStr("DEBUG: Go directly to CustFreqSetCb Profile");
                    CustomFreqSetCb(true, String.Empty); // borrow this callback
                    Started = true;
                }
                else
                {
                    Datalog.LogStr("DEBUG: Getting Frequency Profile");
                    if (PreferredFreqChn >= 0)
                        Started = Rdr.CustomFreqSet(PreferredFreqGrp, PreferredFreqChn, PreferredLBTEn, CustomFreqSetCb);
                    else
                        Started = Rdr.CustomFreqSet(PreferredFreqGrp, CustomFreqSetCb);
                }
            }
            else
            {
                Datalog.LogStr("DEBUG: Failed To Get Frequency Band Number : " + Rdr.bandNum);
                applyCb(false, "Unable to retrieve frequency band number: " + Rdr.LastErrCode.ToString("F"));
                Started = false;
            }
            return Started;
        }

        #endregion

        private void OnRnDClicked(object sender, EventArgs e)
        {
#if false
            RnDSettingsForm RndSettingsFm = new RnDSettingsForm();
            this.Enabled = false;
            RndSettingsFm.Closed += new EventHandler(this.OnOperFrmClosed);
            RndSettingsFm.Show();
#endif
        }

        #region Hotkey (F1) Delegate
        private void HotkeyNotify(eVKey keyCode, bool down)
        {
            if (down == false)
            {
                switch (keyCode)
                {
                    case eVKey.VK_F1:
                        //if (down && this.Enabled) // implies that no menu-item is currently running
                        //    Program.ShowRdrSummaryDisplayWindow(HotkeyNotify);
                        switch (MoreBttnState())
                        {
                            case ExtendedMenuState.level0: //First Screen
                                if (btnFieldService.Enabled)
                                    btnFieldService_Click((Button)l0Buttons[0], new EventArgs());
                                break;
                            case ExtendedMenuState.level01: //Second Screen
                                OnTagInvtryClicked((Button)l01Buttons[0], new EventArgs());
                                break;
                            case ExtendedMenuState.level1: //Third Screen
                                if (btnSyncFieldService.Enabled)
                                    btnSyncFieldService_Click((Button)l1Buttons[0], new EventArgs());
                                break;
                        }
                        break;
                    case eVKey.VK_F2:
                        //if (down)// implies that no menu-item is currently running
                        //    Program.ShowMacError(HotkeyNotify);
                        switch (MoreBttnState())
                        {
                            case ExtendedMenuState.level0: //First Screen
                                if (btnDispatch.Enabled)
                                    button1_Click((Button)l0Buttons[1], new EventArgs());
                                break;
                            case ExtendedMenuState.level01: //Second Screen
                                btnAddLocation_Click((Button)l01Buttons[1], new EventArgs());
                                break;
                            case ExtendedMenuState.level1: //Third Screen
                                if (btnInventory.Enabled)
                                    btnInventory_Click((Button)l1Buttons[1], new EventArgs());
                                break;
                        }
                        break;
                    case eVKey.VK_F3:
                        switch (MoreBttnState())
                        {
                            case ExtendedMenuState.level0: //First Screen
                                OnTagRdClicked((Button)l0Buttons[2], new EventArgs());
                                break;
                            case ExtendedMenuState.level01: //Second Screen
                                btnAddEmployee_Click((Button)l01Buttons[2], new EventArgs());
                                break;
                            case ExtendedMenuState.level1: //Third Screen
                                OnBCScnClicked((Button)l1Buttons[2], new EventArgs());
                                break;
                        }
                        break;
                    case eVKey.VK_F4:
                        switch (MoreBttnState())
                        {
                            case ExtendedMenuState.level0: //First Screen
                                OnTagSearchClicked((Button)l0Buttons[3], new EventArgs());
                                break;
                            case ExtendedMenuState.level01: //Second Screen
                                OnTagWrClicked((Button)l01Buttons[3], new EventArgs());
                                break;
                            case ExtendedMenuState.level1: //Third Screen
                                OnDevStatClicked((Button)l1Buttons[3], new EventArgs());
                                break;
                        }
                        break;
                    case eVKey.VK_F5:
                        switch (MoreBttnState())
                        {
                            case ExtendedMenuState.level0: //First Screen
                                if (btnSync.Enabled)
                                    btnSync_Click((Button)l0Buttons[4], new EventArgs());
                                break;
                            case ExtendedMenuState.level01: //Second Screen
                                break;
                            case ExtendedMenuState.level1: //Third Screen
                                if (btnRecieveDispatch.Enabled)
                                    btnRecieveDispatch_Click((Button)btnRecieveDispatch, new EventArgs());
                                break;
                        }
                        break;
                }
            }
        }
        #endregion

        private void btnAddLocation_Click(object sender, EventArgs e)
        {
            TagWrForm TagWrFm = new TagWrForm();
            TagWrFm.OpenMode = TagWrForm.FormType.AddLocation;
            TagWrFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagWrFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            TagWrForm TagWrFm = new TagWrForm();
            TagWrFm.OpenMode = TagWrForm.FormType.AddEmployee;
            TagWrFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            TagWrFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void btnSync_Click(object sender, EventArgs e)
        {
            frmSynchronize fSync = new frmSynchronize();
            fSync.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fSync.Closed += new EventHandler(this.OnOperFrmClosed);

        }

        private void btnInventory_Click(object sender, EventArgs e)
        {
            frmInveventorySync fSync = new frmInveventorySync();
            fSync.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fSync.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // Open new Dispatch Doc Window
            //frmDispatchDoc frmDispatch = new frmDispatchDoc(); 
            //frmDispatch.Show();
            //this.Enabled = false;
            //frmDispatch.Closed += new EventHandler(frmDispatch_Closed); 

            frmReferenceNo frm = new frmReferenceNo();
            frm.Show();
            this.Enabled = false;
            frm.Closed += new EventHandler(frm_Closed);
        }

        void frm_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        void frmDispatch_Closed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnFieldService_Click(object sender, EventArgs e)
        {
            //// Open new Tag Read window
            //TagFSForm TagFSFm = new TagFSForm();
            //TagFSFm.Show();
            //// disable form until this new form closed
            //this.Enabled = false;
            //TagFSFm.Closed += new EventHandler(this.OnOperFrmClosed);
            frmSelectItems FSForm = new frmSelectItems();
            FSForm.Show();
            this.Enabled = false;
            FSForm.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void btnSyncFieldService_Click(object sender, EventArgs e)
        {
            FieldServiceSync fSync = new FieldServiceSync();
            fSync.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fSync.Closed += new EventHandler(this.OnOperFrmClosed);
        }

        private void OnKeyUp(object sender, KeyEventArgs e)
        {
            //Keys.Decimal;
            if (e.KeyValue == 188) //Go Left
            {
                OnMoreClicked(btnBack, new EventArgs());
                //MessageBox.Show("Key Press Left");  
            }
            else if (e.KeyValue == 190) // Go Right
            {
                OnMoreClicked(MoreBttn, new EventArgs());
                //MessageBox.Show("Key Press Right");  
            }
            else
            {

            }
        }

        private void btnRecieveDispatch_Click(object sender, EventArgs e)
        {
            UserPref pref = UserPref.GetInstance();
            frmReceiveDispatch frmRD = new frmReceiveDispatch();
            frmRD.recPerPage = pref.PageSize;
            frmRD.Show();
            this.Enabled = false;
            frmRD.Closed += new EventHandler(frm_Closed);
        }

        private void cbGoOnline_CheckStateChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;
            if (cbGoOnline.Checked)
            {
                try
                {
                    Login.OnLineMode = true;

                    Login.urlStr = UserPref.GetInstance().ServiceURL;

                    if (Login.verifyPassword(Login.UserName, Login.Password))
                    {
                        GoOnlineCheck(true);
                        if (btnSync.Enabled)
                        {
                            if (MessageBox.Show("Do you want to perform database Synchronization now?", "Sync Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                            {
                                btnSync_Click(sender, e);
                            }
                        }
                    }
                    else
                    {
                        if (Login.err.Length != 0)
                        {
                            Login.OnLineMode = false;
                            cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
                            MessageBox.Show(Login.err);
                            cbGoOnline.Checked = false;
                            cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);
                        }
                        else
                        {
                            Login.OnLineMode = false;
                            cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
                            MessageBox.Show("Invalid User Name or Password.", "Online Mode");
                            cbGoOnline.Checked = false;
                            cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);

                        }

                    }

                }
                catch (Exception ex)
                {
                    Login.OnLineMode = false;
                    cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
                    MessageBox.Show(ex.Message);
                    cbGoOnline.Checked = false;
                    cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);

                }

            }
            else
            {
                // GoOnlineCheck(false);

                Login.OnLineMode = false;
                if (Login.verifyPassword(Login.UserName, Login.Password))
                {
                    if (btnSync.Enabled)
                    {
                        if (MessageBox.Show("Do you want to perform database Synchronization now?", "Sync Option", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.Yes)
                        {
                            btnSync_Click(sender, e);
                        }
                    }

                    GoOnlineCheck(false);
                }
                else
                {
                    if (Login.err.Length != 0)
                    {
                        Login.OnLineMode = true;
                        cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
                        MessageBox.Show(Login.err);
                        cbGoOnline.Checked = true;
                        cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);
                    }
                    else
                    {
                        Login.OnLineMode = true;
                        cbGoOnline.CheckStateChanged -= new EventHandler(cbGoOnline_CheckStateChanged);
                        MessageBox.Show("Invalid User Name or Password.", "Offline Mode");
                        cbGoOnline.Checked = true;
                        cbGoOnline.CheckStateChanged += new EventHandler(cbGoOnline_CheckStateChanged);

                    }

                }
            }
            Cursor.Current = Cursors.Default;
        }

        void GoOnlineCheck(bool online)
        {
            btnSync.Enabled = online;
            btnInventory.Enabled = online; // Sync Inventory
            btnSyncFieldService.Enabled = online;
            // btnDispatch.Enabled = online;

            Login.OnLineMode = online;

            if (online == true)
            {
                int index = 0;

                char[] arrAppcode = UserPref.GetInstance().allApplicationModules.ToCharArray();

                index = Convert.ToInt32(btnSync.Tag);
                if (arrAppcode[index - 1] == '0')
                    btnSync.Enabled = false;

                index = Convert.ToInt32(btnInventory.Tag);
                if (arrAppcode[index - 1] == '0')
                    btnInventory.Enabled = false;

                index = Convert.ToInt32(btnSyncFieldService.Tag);
                if (arrAppcode[index - 1] == '0')
                    btnSyncFieldService.Enabled = false;

            }

            //if (Login.appModules == AppModules.RemoveDispatch || Login.appModules == AppModules.RemoveDispatchandFS)
            //if (AppModules.RemoveDispatch)
            //{
            //    btnDispatch.Enabled = false;
            //}

            ////if (Login.appModules == AppModules.RemoveFieldService || Login.appModules == AppModules.RemoveDispatchandFS)
            //if (AppModules.RemoveFieldService)
            //{
            //    btnFieldService.Enabled = false;
            //    btnSyncFieldService.Enabled = false;
            //}            



        }



    }
}