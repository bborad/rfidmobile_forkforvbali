using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;
//using ClsReaderLib.Devices.Barcode;
using HHDeviceInterface.BarCode;

namespace OnRamp
{
    public partial class frmBCScanForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion

        //private static String LastSaveDir = null;
        private static bool ContScan = true; // Continue scanning after capture
        public enum FormType
        {
            Addform,
            Inventoryform
        };

        private FormType _OpenMode;

        public FormType OpenMode
        {
            get
            {
                return _OpenMode;
            }
            set
            {
                _OpenMode = value;
            }
        }

        public frmBCScanForm()
        {
            InitializeComponent();

            ScanBttnInitState();

        }

        #region BarCode List
        private ListViewItem BarCodeListAdd(String BarCode)
        {
            ListViewItem item = null;

            int index = -1;

            index = BarCode.IndexOf(Convert.ToChar(29));

            if (index >= 0)
            {
                BarCode = BarCode.Remove(0, index + 1);
            }

            foreach (ListViewItem row in BCListV.Items)
            {
                if (String.Compare(BarCode, row.Text, false) == 0) // Case Matters(?)
                {
                    item = row;
                    break;
                }
            }

            if (item != null) // If already exists, move to beginning
            {
                BCListV.BeginUpdate();
                if (item.Index > 0)
                {
                    BCListV.Items.Remove(item); // assuming just detach
                    BCListV.Items.Insert(0, item);

                }
                BCListV.EndUpdate();
            }
            else // Otherwise, Add to ListView (Top)
            {
                item = new ListViewItem(BarCode);
                BCListV.Items.Insert(0, item);

            }

            return item;
        }

        #endregion

        private void FlashRow(ListViewItem row)
        {
            Color OrigColor = row.BackColor;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            row.BackColor = Color.Green;
            row.ListView.Refresh();

            // instead of sleep, do counting to avoid (serial port)thread switch
            //System.Threading.Thread.Sleep(150);
            for (int i = 0; i < 1000000; i++) ;
            row.BackColor = OrigColor;

        }

        #region BCodeRdr i/f routines
        private bool BarCodeNotify(bool succ, String bcStr, String errMsg)
        {
            if (succ)
            {
                Program.RefreshStatusLabel(this, "Barcode Captured...");
                ListViewItem Row = BarCodeListAdd(bcStr);
                // Blink something to indicate?
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                FlashRow(Row);
            }
            else
            {
                Program.RefreshStatusLabel(this, "Barcode Capture Error...");
                MessageBox.Show(errMsg, "Bar Code Reader Error");
            }

            bool ToCont = (ContScan) && (ScanBttnState() != ScnState.Stopping);
            if (!ToCont)
            {
                Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
                ScanBttnSetState(ScnState.Idle);
            }
            return ToCont;
        }
        #endregion


        #region Scan Button routines
        enum ScnState
        {
            Idle,
            Running,
            Stopping,
        }

        private void ScanBttnInitState()
        {
            ScnState state = ScnState.Idle;

            ScanBttn.Tag = state;
        }

        private ScnState ScanBttnState()
        {
            ScnState state = ScnState.Idle;

            if (ScanBttn.Tag is ScnState)
            {
                state = (ScnState)ScanBttn.Tag;
            }
            else
                throw new ApplicationException("Scan Button Tag not of  type ScnState");

            return state;
        }

        private void ScanBttnSetState(ScnState newState)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BCScanForm));

            ScanBttn.Tag = newState;
            switch (newState)
            {
                case ScnState.Idle:
                    ScanBttn.Text = resources.GetString("ScanBttn.Text");
                    ScanBttn.Enabled = true;
                    ClrBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
                case ScnState.Running:
                    ScanBttn.Text = "Stop";
                    ScanBttn.Enabled = true;
                    ClrBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case ScnState.Stopping:
                    ScanBttn.Enabled = false;
                    ClrBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
            }

        }
        #endregion

        private void ScanStopped()
        {
            Program.RefreshStatusLabel(this, "Barcode Scan Canceled...");
            ScanBttnSetState(ScnState.Idle);
        }

        private void OnScanBttnClicked(object sender, EventArgs e)
        {
            //BarCodeRdr Rdr = null;
            BarCodeReader Rdr = null;
            switch (ScanBttnState())
            {
                case ScnState.Idle:
                    Program.RefreshStatusLabel(this, "Setting up...");
                    //Rdr = BarCodeRdr.GetInstance();
                    Rdr = BarCodeFactory.GetBarCodeRdr();
                    Program.RefreshStatusLabel(this, "Scanning...");
                    ScanBttnSetState(ScnState.Running);
                    //if(this.InvokeRequired)
                    //{
                    //    Rdr.notifyee = this;
                    //}
                    Rdr.notifyee = this;
                    Rdr.RegisterCodeRcvdNotificationEvent(BarCodeNotify);
                    Rdr.ScanStart();
                    //Rdr.ScanStart(BarCodeNotify, this);
                    break;

                case ScnState.Running:
                    //Rdr = BarCodeRdr.GetInstance();
                    Rdr = BarCodeFactory.GetBarCodeRdr();
                    Program.RefreshStatusLabel(this, "Stopping Barcode Scan...");
                    ScanBttnSetState(ScnState.Stopping);
                    //if (this.InvokeRequired)
                    //{
                    //    Rdr.stopNotifee = this;
                    //}
                    Rdr.stopNotifee = this;
                    Rdr.RegisterStopNotificationEvent(ScanStopped);
                    //if (Rdr.ScanTryStop(ScanStopped, this)) // use TryStop to avoid deadlock
                    if (Rdr.ScanTryStop())
                    {
                        Program.RefreshStatusLabel(this, "Barcode Scan Canceled..."); 
                    
                    }
                    else
                    {
                        // Scanner busy at the moment
                        // Leave the 'Stopping' State as to return 'not-to-continue'
                        //  on the next bar-code read notification
                        // MessageBox.Show("Scanner Busy", "Stop Denied");
                    }

                    if (formID == "ReceiveDispatch")
                    {
                        frmReceiveDispatch ftagRd = (frmReceiveDispatch)this.Owner;
                        ftagRd.BCList = BCListV;
                        //ftagRd.AddListViewItems(BCListV.Items);
                        this.Close();
                    }
                    else
                    {
                        ScanBttnSetState(ScnState.Idle);
                    }

                    break;
            }
        }

        private void OnClrBttnClicked(object sender, EventArgs e)
        {
            if (Program.AskUserConfirm("Remove all rows from table?") == DialogResult.OK)
            {
                // Remove all items from BCListV
                if (BCListV.Items != null)
                    BCListV.Items.Clear();
            }
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (ScanBttnState() == ScnState.Running)
            {
                MessageBox.Show("Please stop barcode scanning process first", "Request Denied");
                e.Cancel = true;
            }
        }

        #region Scanner Device Configuration
        private void CheckScnrSetting(String tag, String subTag)
        {
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
           // BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            bool Succ;
            String Msg;

            Succ = Rdr.GetCurrentSetting(tag, subTag, out Msg);
            if (Succ)
            {
                MessageBox.Show("Result: " + Msg, "Succeed");
            }
            else
            {
                MessageBox.Show(Msg, "Query Error");
            }
        }

        private void ChangeScnrPersistentSetting(String tag, String subTag, String data)
        {
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            bool Succ;
            String Msg;
            Succ = Rdr.ChangeSavedSetting(tag, subTag, data, out Msg);
            if (!Succ)
            {
                MessageBox.Show(Msg, "Change BarCode Setting Error");
            }
        }

        private void ChangeScnrSetting(String tag, String subTag, String data)
        {
            BarCodeReader Rdr = BarCodeFactory.GetBarCodeRdr();
            //BarCodeRdr Rdr = BarCodeRdr.GetInstance();
            bool Succ;
            String Msg;
            Succ = Rdr.ChangeCurrentSetting(tag, subTag, data, out Msg);
            if (!Succ)
            {
                MessageBox.Show(Msg, "Change BarCode Setting Error");
            }
        }

        private void OnSetupBttnClicked(object sender, EventArgs e)
        {
            this.SetupBttn.Enabled = false;

            // OCR-B
            CheckScnrSetting("OCR", "ENA");
            //Postnet
            CheckScnrSetting("NET", "ENA");
            // EAN-UCC Composite
            CheckScnrSetting("COM", "ENA");
            // British Post
            CheckScnrSetting("BPO", "ENA");
            // Japanese Post
            CheckScnrSetting("JAP", "ENA");
            // China Post
            CheckScnrSetting("CPC", "ENA");
            // Other available Post (Canadian, Netherlands, Australian, Korea)

            this.SetupBttn.Enabled = true;
        }
        #endregion

        private void OnLsrOnClicked(object sender, EventArgs e)
        {
            PosSp.ScnrLaserOn();
        }

        private void OnLsrOffClicked(object sender, EventArgs e)
        {
            PosSp.ScnrLaserOff();
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormLoad(object sender, EventArgs e)
        {
            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler); 
            btnInventorySave.Visible = false;
            switch (this.OpenMode)
            {
                  
                case FormType.Addform:
                    SaveBttn.Visible = true;
                    break;
                case FormType.Inventoryform:
                    btnInventorySave.Visible = true;
                    SaveBttn.Visible = false;
                    break;
                default:
                    //
                    break;
            }

            if (formID == "ReceiveDispatch")
            {
                disableButtons();
            }

        }

        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            if (keyCode == eVKey.VK_F11)
            {
                if (down)
                {
                    // fake 'Start' key press if not already running
                    if (ScanBttnState() == ScnState.Idle)
                    {
                        OnScanBttnClicked(this, null);
                    }
                }
                else // up
                {
                    if (ScanBttnState() == ScnState.Running)
                    {
                        // Stop!
                        OnScanBttnClicked(this, null);
                    }
                }
            }
            // ignore other keys
        }

        /// <summary>
        /// Save List to File
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OnSaveBttnClicked(object sender, EventArgs e)
        {
            if (ScanBttnState() == ScnState.Running)
                MessageBox.Show("Please stop scan first.");
            else
            {
                if (BCListV.Items != null && BCListV.Items.Count > 0)
                {
                    // Open new BarCode Scanning Window
                    TagWrForm ftagWrForm = (TagWrForm)this.Owner;
                    ftagWrForm.AddListViewItems(BCListV.Items);
                    this.Close();
                }
                else
                    MessageBox.Show("Please scan barcodes first.");
            }

            //*********************commented by cherry***************************//
            //if (BCListV.Items != null && BCListV.Items.Count > 0)
            //{
            //    SaveFileDialog SaveFileDlg = new SaveFileDialog();

            //    SaveFileDlg.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            //    SaveFileDlg.FilterIndex = 1;
            //    if (String.IsNullOrEmpty(LastSaveDir) == false)
            //        SaveFileDlg.InitialDirectory = LastSaveDir;
            //    else // "Application Data" as default folder
            //        SaveFileDlg.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            //    DialogResult Res = SaveFileDlg.ShowDialog();
            //    if (Res == DialogResult.OK)
            //    {
            //        try
            //        {
            //            LastSaveDir = Path.GetDirectoryName(SaveFileDlg.FileName);
            //            SaveListToCSVFile(SaveFileDlg.FileName);
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show(ex.Message, "Save to File Failed");
            //        }
            //    }
            //    SaveFileDlg.Dispose();
            //}
            //else
            //    MessageBox.Show("Please scan barcodes first.");
            //*********************End commented by cherry***************************//
        }

        // return false if not written
        private bool SaveListToCSVFile(String DestFile)
        {
            bool Written = false;

#if false // SaveFileDialog already prompt user if they want it overwritten
            if (File.Exists(DestFile))
            {
                SimpleModalDialog OverwriteDlg = new SimpleModalDialog("File Already Exists. Overwrite?");
                DialogResult Overwrite = OverwriteDlg.ShowDialog();
                if (Overwrite != DialogResult.OK)
                {
                    return false;
                }
            }
#endif
            using (StreamWriter sw = File.CreateText(DestFile))
            {
                foreach (ListViewItem item in BCListV.Items)
                {
                    sw.Write(item.Text + "," + "\r\n");
                }
                sw.Close();
                Written = true;
            }
            return Written;
        }

        /// <summary>
        /// It will save Scannes barcode on inventory list tables
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnInventorySave_Click(object sender, EventArgs e)
        {
            if (ScanBttnState() == ScnState.Running)
                MessageBox.Show("Please stop scan first.");
            else
            {
                if (BCListV.Items != null && BCListV.Items.Count > 0)
                {
                    // Open new BarCode Scanning Window
                    TagRdForm ftagRdfrm = (TagRdForm)this.Owner;
                    ftagRdfrm.BCList = BCListV;
                   // ftagRdfrm.AddListViewItems(BCListV.Items);
                    this.Close();
                }
                else
                    MessageBox.Show("Please scan barcodes first.");
            }
        }

        #region Code for Receive / Dispatch Form

        public string formID = "";

        public void disableButtons()
        {
            btnInventorySave.Visible = false;
            SaveBttn.Visible = false;
            ClrBttn.Visible = false;
        }

        #endregion

    }
}
