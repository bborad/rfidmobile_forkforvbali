using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using TimeSync;
using ClsReaderLib;using ClsReaderLib.Devices;
 
/// Short Cuts
//using CF = ClslibRfidSp.CallFlow;


namespace OnRamp
{
    public partial class SettingsForm : Form
    {
        private const int DeviceNameMaxLength = 30;

        public SettingsForm()
        {
            InitializeComponent();

            InitSituationListV();

            RdrNameTxtBx.MaxLength = DeviceNameMaxLength;
        }

        #region Debug Routines
        private void DumpQueryParmResultsToScrn(bool succ, 
            ref RFID_18K6C_QUERY_PARMS parms,
            String errMsg)
        {
               if (!succ)
                DisplayResult("Failed : " + errMsg);
            else
            {
                DisplayResult(
                    parms.tagGroup.selected.ToString("F") + "\r\n"
                + parms.tagGroup.session.ToString("F") + "\r\n"
                + parms.tagGroup.target.ToString("F") + "\r\n"
                + parms.singulationParms.singulationAlgorithm.ToString("F") + "\r\n"
                + ((parms.singulationParms.singulationAlgorithm == RFID_18K6C_SINGULATION_ALGORITHM.RFID_18K6C_SINGULATION_ALGORITHM_FIXEDQ) ?
                    ("qVal: " + parms.singulationParms.fixedQ.qValue + "\r\n"
                      + "retryCnt: " + parms.singulationParms.fixedQ.retryCount + "\r\n"
                      + "toggleTgt: " + parms.singulationParms.fixedQ.toggleTarget + "\r\n"
                      + "rpt: " + parms.singulationParms.fixedQ.repeatUntilNoTags)
                      :
                      ("startQVal: " + parms.singulationParms.dynamicQ.startQValue + "\r\n"
                + "minQVal: " + parms.singulationParms.dynamicQ.minQValue + "\r\n"
                + "maxQVal: " + parms.singulationParms.dynamicQ.maxQValue + "\r\n"
                + "retryCnt: " + parms.singulationParms.dynamicQ.retryCount + "\r\n"
                + "toggleTgt: " + parms.singulationParms.dynamicQ.toggleTarget + "\r\n"
                + "maxRpt: " + parms.singulationParms.dynamicQ.maxQueryRepCount)
                )
                );       
            }
        }
#endregion

        private void DisplayResult(String resStr)
        {
            SttgsTabCtrl.Visible = false;

            Graphics g = this.CreateGraphics();
            // clear screen
            g.Clear(this.BackColor);
            g.DrawString(resStr, this.Font, new SolidBrush(Color.Black), 5, 5);

            g.Dispose();
        }

        private void CustomFreqGetNotify(bool succ, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            if (succ)
            {
                CntryLbl.Text = freqGrp.ToString("F");
            }
            else
            {
                Program.ShowError("Error getting Freq. Profile:\n" + freqGrp);
            }
            this.Enabled = true; // no more RFIDRdr requests pending
        }


        private void OnFormLoad(object sender, EventArgs e)
        {
            // Rdr ID
            StringBuilder Sb = new StringBuilder(DeviceNameMaxLength);
            if (SettingMgt.f_GetWINSHostName(Sb, DeviceNameMaxLength) == 0)
                loadedRdrName = Sb.ToString();
            else
                loadedRdrName = String.Empty;
            RdrIDTabResetVal();

            // User Auth Page that does not require Radio
            UserAuthResetVal();

            // Diagnostics Page items that does not require Radio       
            DisplayRecHiTemp();
            DiagTabResetVal();
            MacRegAddrTxtBx.SelectAll();

            // Sound Page
            SndTabResetVal();

            // NTP Page
            TimeSyncTabResetVal();

            // HTTP Page
            HttpTabResetVal();

            // Versions Page
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            AppVerLbl.Text = "Build " + Program.SvnVer;
            DrvVerLbl.Text = Rdr.DriverVer.ToString();
            MACVerLbl.Text = Rdr.MacVer.ToString();
            // This requires querying the RFIDRdr for Band and Country Profile Info
            // Disable form to prevent user from closing the form pre-maturely
            this.Enabled = false;
            //UInt32 BandNum = 2;
            Rdr.bandNum = 2;
            if (Rdr.CustomFreqBandNumGet() == false)
            {
                this.Enabled = true;
                Program.ShowError("Unable to get Freq Band info");
            }
            else
            {
                FrqBndLbl.Text = Rdr.bandNum.ToString();
               // Rdr.CustomerFreqGetNotification += CustomFreqGetNotify;
                Rdr.RegisterCustomFreqGetNotificationEvent(CustomFreqGetNotify);
               // if (Rdr.CustomFreqGet(CustomFreqGetNotify) == false)
                if (Rdr.CustomFreqGet() == false)
                {
                    Program.ShowError("Unable to get Frequency Profile information");
                    this.Enabled = true;
                }
            }

            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnFormClosed(object sender, EventArgs e)
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }
       
        private void OnApplyButtonClicked(object sender, EventArgs e)
        {
            if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.DiagTab)
            {
                UserPref Pref = UserPref.GetInstance();   
                // assuming that the Apply button is disabled if there is no user changes
                if (TraceLogChkBx.Checked)
                {
                    if (!Datalog.LogInit(Program.RfidLogFilePath))
                    {
                        MessageBox.Show(Datalog.ErrMsg, "Start Trace Log Failed");
                        TraceLogChkBx.Checked = false;
                    }
                }
                else
                    Datalog.LogUninit();

                Pref.LogTraceOn = TraceLogChkBx.Checked;
                DiagTabUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.UserAuthTab)
            {
                UserAuthTabApplyChanges();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.SoundTab)
            {
                SndTabApplyChanges();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.RdrIDTab)
            {
                RdrIDTabApplyChanges();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.TimeSyncTab)
            {
                TimeSyncTabApplyChanges();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == this.HTTPTab)
            {
                HttpTabApplyChanges();
            }
        }

        private void OnRstButtonClicked(object sender, EventArgs e)
        {
            RdrIDTabResetVal();
            UserAuthResetVal();
            DiagTabResetVal();
            SndTabResetVal();
            TimeSyncTabResetVal();
            HttpTabResetVal();
            Program.RefreshStatusLabel(this, String.Empty);
        }
  
        private void OnSelectedPageChanged(object sender, EventArgs e)
        {
            // update 'Enabled' status of the 'Apply' Button
            if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == UserAuthTab)
            {
                UserAuthUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == DiagTab)
            {
                DiagTabUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == SoundTab)
            {
                SndTabUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == TimeSyncTab)
            {
                TimeSyncTabUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == HTTPTab)
            {
                HttpTabUpdateApplyBttnEnabled();
            }
            else if (SttgsTabCtrl.TabPages[SttgsTabCtrl.SelectedIndex] == VerTab)
            {
                ApplyButton.Enabled = false;
            }
            Program.RefreshStatusLabel(this, String.Empty);
        }

        #region Identity (RdrName) page
        private String loadedRdrName;
        private void RdrIDTabResetVal()
        {
            RdrNameTxtBx.Text = loadedRdrName;
        }

        private bool RdrIDIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            bool Modified = false;

            // case in-sensitive
            Modified = (String.Compare(loadedRdrName, RdrNameTxtBx.Text, true) != 0);

            return Modified;
        }

        private void RdrIDUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = RdrIDIsModified();
        }

        private void RdrIDTabApplyChanges()
        {
            UserPref Pref = UserPref.GetInstance();

            Pref.RdrName = RdrNameTxtBx.Text;

            RdrIDUpdateApplyBttnEnabled();

            if (SettingMgt.f_SetWINSHostName(RdrNameTxtBx.Text) != 0)
                MessageBox.Show("Failed");

            Program.RefreshStatusLabel(this, "Changes Saved");
        }

        private void OnRdrNameTxtBxChanged(object sender, EventArgs e)
        {
            RdrIDUpdateApplyBttnEnabled();
        }

        #endregion

        #region UserAuth Page
        private void UserAuthResetVal()
        {
            UserPref Pref = UserPref.GetInstance();

            SignOnChkBx.Checked = Pref.UserSignOnRequired;

            UsrNameTxtBx.Text = Pref.UserName;
            PwdTxtBx.Text = Pref.Passwd;
        }

        private bool UserAuthIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            bool Modified = false;

            Modified = (SignOnChkBx.Checked != Pref.UserSignOnRequired)
                || (String.Compare(UsrNameTxtBx.Text, Pref.UserName, true) != 0)
                || (String.Compare(PwdTxtBx.Text, Pref.Passwd, false) != 0);

            return Modified;
        }

        private void UserAuthUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = UserAuthIsModified();
        }

        private void UserAuthTabApplyChanges()
        {
            UserPref Pref = UserPref.GetInstance();

            Pref.UserSignOnRequired = SignOnChkBx.Checked;
            Pref.UserName = UsrNameTxtBx.Text;
            Pref.Passwd = PwdTxtBx.Text;

            UserAuthUpdateApplyBttnEnabled();
            
            Program.RefreshStatusLabel(this, "Changes Saved");
        }

        private void OnUsrNameTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }

        private void OnPwdTxtBxKeyPress(object sender, KeyPressEventArgs e)
        {
            Program.AlphaNumKeyPressChk(sender, e);
        }


        private void OnSignOnChkBxChanged(object sender, EventArgs e)
        {
            UserAuthUpdateApplyBttnEnabled();
        }

        private void OnUsrNameTxtBxChanged(object sender, EventArgs e)
        {
            UserAuthUpdateApplyBttnEnabled();
        }

        private void OnPwdTxtBxChanged(object sender, EventArgs e)
        {
            UserAuthUpdateApplyBttnEnabled();
        }

        #endregion

        #region Diagnostics Page

        private void DiagTabResetVal()
        {
            UserPref Pref = UserPref.GetInstance();

            TraceLogChkBx.Checked = Pref.LogTraceOn;
        }

        private bool DiagTabIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            return (Pref.LogTraceOn != TraceLogChkBx.Checked);
        }

        private void DiagTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = DiagTabIsModified();
        }

        private void OnTraceLogChanged(object sender, EventArgs e)
        {
            DiagTabUpdateApplyBttnEnabled();
        }

        private void DisplayRecHiTemp()
        {
            TempMonitor TempMon = TempMonitor.GetInstance();
            XcvrTempRecHiLbl.Text = TempMon.XcvrTempHi.ToString();
            PATempRecHiLbl.Text = TempMon.PATempHi.ToString();
        }

        private void OnRstTempBttnClicked(object sender, EventArgs e)
        {
            if (Program.AskUserConfirm("This would reset record-hi temperature with current temperatures.\n Proceed?") == DialogResult.OK)
            {
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
               // ushort amb, xcvr, pamp;

                if (Rdr.GetCurrTemp() == false)
                {
                    Program.ShowWarning("Failed to Get Current Temperature");
                }
                else
                {
                    TempMonitor TempMon = TempMonitor.GetInstance();
                    // this assumes that the EvtHandler in TempMonitor is dispatched before this one,
                    // meaning that the latest temp is already loaded to TempMon object
                    TempMon.ResetPATempHi();
                    TempMon.ResetXcvrTempHi();
                    DisplayRecHiTemp();
                }
            }
        }

        #endregion

        #region Sound Tab
        // For some reason, adding rows from Visual Studio does not display correctly.
        private String[] SituationStrs = new string[6] // corresponds to UserPref.SndSituations enum def
            {
                "Tag Inventory",
                "Tag Ranging",
                "Tag Read/Write",
                "Tag Commissioning",
                "Tag Authentication",
                "Tag Set Permission",
            };
        private enum ListVOps : int
        {
            Invtry,
            Ranging,
            RdWr,
            Commission,
            Authentication,
            SetPerm
        };
        private UserPref.SndSituations[] OpsFlags = new UserPref.SndSituations[6]
            {
                UserPref.SndSituations.Invtry,
                UserPref.SndSituations.Ranging,
                UserPref.SndSituations.RdWr,
                UserPref.SndSituations.Commission,
                UserPref.SndSituations.Authentication,
                UserPref.SndSituations.SetPerm,
            };
        private bool sndDisable = false;

        private void InitSituationListV()
        {
            for (int i = 0; i < SituationStrs.Length; i++)
                SituationLstV.Items.Add(new ListViewItem(SituationStrs[i]));

        }


        private SoundVol SndVolBrVal
        {
            set
            {
                int BrVal = 1;
                switch (value)
                {
                    case SoundVol.Low:
                        BrVal = 1;
                        break;
                    case SoundVol.Med:
                        BrVal = 2;
                        break;
                    case SoundVol.High:
                        BrVal = 3;
                        break;
                    case SoundVol.Mute:
                        BrVal = 1; // for now
                        break;
                }
                SndVolBr.Value = BrVal;
            }
            get
            {
                SoundVol SndVol = SoundVol.Med;
                switch (SndVolBr.Value)
                {
                    case 1:
                        SndVol = SoundVol.Low; break;
                    case 2:
                        SndVol = SoundVol.Med; break;
                    case 3:
                        SndVol = SoundVol.High; break;
                    default:
                        throw new ApplicationException("Programming Error");
                }
                return SndVol;
            }

        }

        private void OnVolChanged(object sender, EventArgs e)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //SoundVol SndVol = SndVolBrVal;
            Rdr.Svol = SndVolBrVal;
            if (!sndDisable)
                Rdr.BuzzerBeep(1);

            SndTabUpdateApplyBttnEnabled();
        }

        private RingTone MldyCmbBxVal(ComboBox CmbBx)
        {
            RingTone toneID = RingTone.T1;
            switch (CmbBx.SelectedIndex)
            {
                case 0:
                    toneID = RingTone.T1; break;
                case 1:
                    toneID = RingTone.T2; break;
                case 2:
                    toneID = RingTone.T3; break;
                case 3:
                    toneID = RingTone.T4; break;
                case 4:
                    toneID = RingTone.T5; break;
            }
            return toneID;
        }

        private int RingToneToLstIdx(RingTone toneID)
        {
            return (int)(toneID - RingTone.T1);
        }

        private RingTone StartMldy
        {
            set
            {
                StartMldyCmbBx.SelectedIndex = RingToneToLstIdx(value);
            }
            get
            {
                return MldyCmbBxVal(StartMldyCmbBx);
            }
        }

        private RingTone EndMldy
        {
            set
            {
                EndMldyCmbBx.SelectedIndex = RingToneToLstIdx(value);
            }
            get
            {
                return MldyCmbBxVal(EndMldyCmbBx);
            }
        }

        private UserPref.SndSituations SndNotifications
        {
            set
            {
                for (int i = 0; i < SituationStrs.Length; i++)
                {
                    SituationLstV.Items[i].Checked = ((value & OpsFlags[i]) == OpsFlags[i]);
                }
            }
            get
            {
                UserPref.SndSituations situations = UserPref.SndSituations.None;
                for (int i = 0; i < SituationStrs.Length; i++)
                {
                    if (SituationLstV.Items[i].Checked)
                        situations |= OpsFlags[i];
                    else
                        situations &= (~OpsFlags[i]);
                }
                return situations;
            }
        }

        private void OnStartMldyChanged(object sender, EventArgs e)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //SoundVol SndVol = SndVolBrVal;
            Rdr.RingingID = StartMldy;
            Rdr.Svol = SndVolBrVal;
            if (!sndDisable)
                Rdr.MelodyRing();

            SndTabUpdateApplyBttnEnabled();
        }

        private void OnEndMldyChanged(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            //SoundVol SndVol = SndVolBrVal;
            Rdr.RingingID = EndMldy;
            Rdr.Svol = SndVolBrVal;
            if (!sndDisable)
                Rdr.MelodyRing();

            SndTabUpdateApplyBttnEnabled();
        }



        private void OnSituationChanged(object sender, ItemCheckEventArgs e)
        {

            // Cannot use SndTabCheckModified() because the checkbox states
            // is not yet changed.
            UserPref Pref = UserPref.GetInstance();
            UserPref.SndSituations loadedSit = Pref.SndNotifications;
            bool Modified = false;
            for (int i = 0; (!Modified) && (i < SituationLstV.Items.Count); i++)
            {
                if (i != e.Index)
                    Modified = (loadedSit & OpsFlags[i]) != (SndNotifications & OpsFlags[i]);
                else
                    Modified = ((loadedSit & OpsFlags[e.Index]) == OpsFlags[e.Index]) != (e.NewValue == CheckState.Checked);
            }
            ApplyButton.Enabled = Modified;
        }

        // Will this conflict with 'X'
        private void OnSituationActivate(object sender, EventArgs e)
        {
            // Toggle the 'Checkbox'
            SituationLstV.FocusedItem.Checked = !SituationLstV.FocusedItem.Checked;
        }


        private void SndTabResetVal()
        {
            UserPref Pref = UserPref.GetInstance();

            // Disable handler to avoid sound, a cleaner
            // way of doing this?
            sndDisable = true;

            SndVolBrVal = Pref.SndVol;
            StartMldy = Pref.OperStartMldy;
            EndMldy = Pref.OperEndMldy;
            SndNotifications = Pref.SndNotifications;

            sndDisable = false;
        }

        private bool SndTabIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            return ((SndVolBrVal != Pref.SndVol)
                            || (StartMldy != Pref.OperStartMldy)
                            || (EndMldy != Pref.OperEndMldy)
                            || (SndNotifications != Pref.SndNotifications));
        }

        private void SndTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = SndTabIsModified();
        }

        static public EventHandler SndTabChanged;

        private void SndTabApplyChanges()
        {
            UserPref Pref = UserPref.GetInstance();
            Pref.SndVol = SndVolBrVal;
            Pref.OperStartMldy = StartMldy;
            Pref.OperEndMldy = EndMldy;
            Pref.SndNotifications = SndNotifications;

            SndTabUpdateApplyBttnEnabled();

            if (SndTabChanged != null)
                SndTabChanged(this, new EventArgs());

            Program.RefreshStatusLabel(this, "Changes Saved");
        }
        #endregion      

        #region Time Sync Tab
        private void TimeSyncTabResetVal()
        {
            NTPSrvrTxtBx.Text = UserPref.GetInstance().NTPServer;
            TimeSyncTabUpdateApplyBttnEnabled();
        }

        private bool TimeSyncTabIsModified()
        {
            return (String.Compare(UserPref.GetInstance().NTPServer,
                NTPSrvrTxtBx.Text, true) != 0);
        }

        private void TimeSyncTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = TimeSyncTabIsModified();
        }

        private void TimeSyncTabApplyChanges()
        {
            UserPref.GetInstance().NTPServer = NTPSrvrTxtBx.Text;
            TimeSyncTabUpdateApplyBttnEnabled();
            Program.RefreshStatusLabel(this, "Changes Saved");
        }

       
        private void OnTimeSyncBttnClicked(object sender, EventArgs e)
        {
            NTPTimeSyncForm SyncFm = new NTPTimeSyncForm(NTPSrvrTxtBx.Text);
            SyncFm.ShowDialog(); // display until user closes.
            SyncFm.Dispose();
        }

        private void OnNtpSrvrTxtBxChanged(object sender, EventArgs e)
        {
            TimeSyncTabUpdateApplyBttnEnabled();
        }
        #endregion

        private int BeepTstCntr = 0;
        private void OnBeepTstClicked(object sender, EventArgs e)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            if (BeepTstTmr.Enabled)
            {
                // stop
                BeepTstTmr.Enabled = false;
                BeepTstBttn.Text = resources.GetString("BeepTstBttn.Text");
            }
            else
            {
                // start
                BeepTstCntr = 0;
                BeepTstTmr.Enabled = true;
                BeepTstBttn.Text = "Stop";
            }
           
        }

        private void OnBeepTstTmrTick(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            Rdr.Svol = SoundVol.Low;
            Rdr.BuzzerBeep(1);

            BeepTstCntr++;

            BeepTstCntLbl.Text = BeepTstCntr.ToString();
        }

        #region F1/F4/F5 Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F1:                 
                    if (down && this.Enabled) // Form Enabled implies no RFID operation is running
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    break;
            }
        }
        #endregion       

        #region HTTP (Proxy) Tab

        private bool HttpTabIsModified()
        {
            UserPref Pref = UserPref.GetInstance();
            UserPref.HTTPProxy LoadedHttpProxy = Pref.HttpProxy;
            bool Modified = false;
            try
            {
                UserPref.HTTPProxy ChosenHttpProxy = new UserPref.HTTPProxy(
                   ManProxyBttn.Checked, PrxySrvrTxtBx.Text,
                   UInt16.Parse(PrxyPrtNumTxtBx.Text, System.Globalization.NumberStyles.Integer),
                   PrxyBypassesTxtBx.Text, PrxyUsrNameTxtBx.Text, PrxyPasswdTxtBx.Text);
                Modified = (LoadedHttpProxy.IdenticalTo(ref ChosenHttpProxy) == false);
            }
            catch (Exception e)
            {
                MessageBox.Show("Unexpected Exception caught: " + e.GetType().Name);
            }

            return Modified;
        }

        private void HttpTabUpdateApplyBttnEnabled()
        {
            ApplyButton.Enabled = HttpTabIsModified();
        }

        private void HttpTabResetVal()
        {
            UserPref Pref = UserPref.GetInstance();
            UserPref.HTTPProxy LoadedHttpProxy = Pref.HttpProxy;
            ManProxyBttn.Checked = LoadedHttpProxy.Required;
            PrxySrvrTxtBx.Text = LoadedHttpProxy.SrvrAddr;
            PrxyPrtNumTxtBx.Text = LoadedHttpProxy.SrvrPrt.ToString();
            PrxyBypassesTxtBx.Text = LoadedHttpProxy.Bypasses;
            PrxyUsrNameTxtBx.Text = LoadedHttpProxy.Uname;
            PrxyPasswdTxtBx.Text = LoadedHttpProxy.Passwd;
        }

        private void HttpTabApplyChanges()
        {
            // Save to UserPref
            UserPref Pref = UserPref.GetInstance();
            try
            {
                UserPref.HTTPProxy ChosenHttpProxy = new UserPref.HTTPProxy(
                    ManProxyBttn.Checked, PrxySrvrTxtBx.Text,
                    UInt16.Parse(PrxyPrtNumTxtBx.Text, System.Globalization.NumberStyles.Integer),
                    PrxyBypassesTxtBx.Text, PrxyUsrNameTxtBx.Text, PrxyPasswdTxtBx.Text);

                Pref.HttpProxy = ChosenHttpProxy;
                Program.RefreshStatusLabel(this, "Changes Saved");
            }
            catch (Exception e)
            {
                MessageBox.Show("Unexpected Exception caught: " + e.GetType().Name);
            }

        }
        private void OnNoProxyBttnChecked(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnManProxyBttnChecked(object sender, EventArgs e)
        {
            PrxyCfgPanel.Enabled = ManProxyBttn.Checked;
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnPrxySrvrAddrKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.PrintableNoSpaceKeyPressChk(sender, e);
        }

        private void OnPrxyPrtKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.NumKeyPressChk(sender, e);
        }

        private void OnPrxyUsrNameKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.PrintableNoSpaceKeyPressChk(sender, e);
        }

        private void OnPrxyPasswdKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.PrintableNoSpaceKeyPressChk(sender, e);
        }



        private void OnPrxyPrtTxtBxChanged(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnPrxySrvrAddrTxtBxChanged(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnPrxyBypassesTxtBxChanged(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnPrxyUsrNameTxtBxChanged(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }

        private void OnPrxyPasswdTxtBxChanged(object sender, EventArgs e)
        {
            HttpTabUpdateApplyBttnEnabled();
        }
        #endregion

        private TabPage ActivateModifiedTab()
        {
            TabPage FirstModifiedTab = null;

            if (RdrIDIsModified())
            {
                FirstModifiedTab = RdrIDTab;
            }
            else if (UserAuthIsModified())
            {
                FirstModifiedTab = UserAuthTab;
            }
            else if (SndTabIsModified())
            {
                FirstModifiedTab = SoundTab;
            }
            else if (DiagTabIsModified())
            {
                FirstModifiedTab = DiagTab;
            }
            else if (TimeSyncTabIsModified())
            {
                FirstModifiedTab = TimeSyncTab;
            }
            else if (HttpTabIsModified())
            {
                FirstModifiedTab = HTTPTab;
            }

            if (FirstModifiedTab != null)
                SttgsTabCtrl.SelectedIndex = SttgsTabCtrl.TabPages.IndexOf(FirstModifiedTab);
            
            return FirstModifiedTab;
        }

        private void OnFormClosing(object sender, CancelEventArgs e)
        {
            if (ActivateModifiedTab() != null)
            {
                if (Program.AskUserConfirm("There are unsaved changes.\n" + "Ignore and leave?")
             != DialogResult.OK)
                {
                    e.Cancel = true;
                }
            }

        }
        #region Health Check Button

        private void OnHlthChkBttnClicked(object sender, EventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (CurHlthChkStatus)
            {
                case HealthChkOpStatus.Idle:
                    this.Enabled = false;
                    HlthChkStatusDlg = new TransientMsgDlg(0);
                    HlthChkStatusDlg.Closed += OnHlthChkStatusDlgClosed;
                    HlthChkStatusDlg.Closing += UserCloseHlthChkStatusDlg;
                    HlthChkStatusDlg.Show();
                   // Rdr.HealthCheckStatusNotification += HlthChkStatusCb;
                    Rdr.RegisterHealthCheckStatusNotificationEvent(HlthChkStatusCb);
                    if (Rdr.PerformHealthCheck() == false)
                    {
                        this.Enabled = true;
                        HlthChkStatusDlg.Closing -= UserCloseHlthChkStatusDlg;
                        HlthChkStatusDlg.Close();
                    }
                    break;
                case HealthChkOpStatus.TagInvtryStarted:
                    this.Enabled = false; // disable until actually stopped
                    if (Rdr.StopHealthCheck() == false)
                    {
                        Program.ShowError("Unable stop Health Check Operation");
                        Program.RefreshStatusLabel(this, "Stopping...");
                        this.Enabled = true;
                    }
                    break;
            }
        }

        #region Restoring To Original Settings
        private void RestoreAntPwrSet(bool succ, uint portNum, String errMsg)
        {
            if (!succ)
            {
                HlthChkStatusDlg.SetDpyMsg("Error restoring to preferred Antenna Power: " + errMsg,
                    "Restoring preferred setting Failed!");
                HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
            }
            else
            {
                HlthChkStatusDlg.SetDpyMsg("Restore to preferred settings finished!");
                HlthChkStatusDlg.SetTimeout(3);
                HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
            }
            Program.RefreshStatusLabel(this, "Health Check Finished");
        }

        private void RestoreFreqSet(bool succ, String errMsg)
        {
            if (!succ)
            {
                HlthChkStatusDlg.SetDpyMsg("Error restoring to preferred Frequency channels and LBT: " + errMsg,
                    "Restoring preferred setting Failed!");
               HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
            }
            else
            {
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                UserPref Pref = UserPref.GetInstance();
                Rdr.power = (int)(Pref.AntennaPwr * 10.0F);
               // Rdr.AntPotCfgSetonNotification += RestoreAntPwrSet;
                Rdr.RegisterAntPotCfgSetOneNotificationEvent(RestoreAntPwrSet);
                Rdr.PortNum = 0;
                if (Rdr.AntPortCfgSetPwr() == false)
                {
                    HlthChkStatusDlg.SetDpyMsg("Unable to restore to preferred Antenna Power level", 
                        "Restoring preferred setting Failed!");
                   HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
                }
                else
                    HlthChkStatusDlg.SetDpyMsg("Restoring preferred Antenna power level");
            }
        }

        private void RestoreLnkPrfNumSet(bool succ, String errMsg)
        {
            if (!succ)
            {
                HlthChkStatusDlg.SetDpyMsg("Error restoring to preferred Link Profile: " + errMsg, 
                    "Restoring preferred setting Failed!");
                HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
            }
            else
            {
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                UserPref Pref = UserPref.GetInstance();
                bool Dispatched = false;
                if (Pref.SingleFreqChn >= 0)
                    Dispatched = Rdr.CustomFreqSet(Pref.FreqProf, Pref.SingleFreqChn, Pref.EnFreqChnLBT, RestoreFreqSet);
                else
                    Dispatched = Rdr.CustomFreqSet(Pref.FreqProf, Pref.EnFreqChnLBT, RestoreFreqSet);
                if (Dispatched == false)
                {
                    HlthChkStatusDlg.SetDpyMsg("Failed to restore Frequency to preferred Channel and LBT setting",
                        "Restoring preferred setting Failed!");
                    HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
                }
                else
                    HlthChkStatusDlg.SetDpyMsg("Restoring preferred Frequency Channels and LBT settings");
            }
        }


        private void OnHlthChkStatusDlgClosed(object sender, EventArgs e)
        {
            HlthChkStatusDlg = null;
            this.Enabled = true; // just in case

            this.Activate();

            switch (CurHlthChkStatus)
            {
                case HealthChkOpStatus.TagInvtryStopped:
                case HealthChkOpStatus.errorStopped:
                    // Restore current setting
                    this.Enabled = false; // for now
                    HlthChkStatusDlg = new TransientMsgDlg(0);
                    HlthChkStatusDlg.Closing += UserCloseHlthChkStopNoticeDlg;
                    HlthChkStatusDlg.Closed += HlthChkStopNoticedDlgClosed;
                    HlthChkStatusDlg.Show();
                    HlthChkStatusDlg.SetDpyMsg("Restoring preferred Link Profile...", "Restoring preferred settings...");
                    // Link Profile
                   // RFIDRdr Rdr = RFIDRdr.GetInstance();
                    Reader Rdr = ReaderFactory.GetReader(); 
                   UserPref Pref = UserPref.GetInstance();
                    Rdr.ProfNum=Pref.LinkProf;
                    //Rdr.LnkProfNumoSetNotification+=RestoreLnkPrfNumSet;
                    Rdr.RegisterLnkProfNumoSetNotificationEvent(RestoreLnkPrfNumSet);
                    if (Rdr.LinkProfNumSet() == false)
                    {
                        HlthChkStatusDlg.SetDpyMsg("Unable to restore to preferred Link Profile number", 
                            "Restoring preferred setting Failed!");
                        HlthChkStatusDlg.Closing -= UserCloseHlthChkStopNoticeDlg;
                    }
                    break;
            }
        }

        private void UserCloseHlthChkStopNoticeDlg(object sender, CancelEventArgs e)
        {
            e.Cancel = true; // disallow until this handler is explicity removed
        }

        private void HlthChkStopNoticedDlgClosed(object sender, EventArgs e)
        {
            HlthChkStatusDlg = null;
            CurHlthChkStatus = HealthChkOpStatus.Idle;
            this.Enabled = true;
        }
        #endregion

        private void UserCloseHlthChkStatusDlg(object sender, CancelEventArgs e)
        {
            this.Enabled = false;
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            if (Rdr.StopHealthCheck() == false)
            {
                HlthChkStatusDlg.SetDpyMsg("Unable to Stop Health Check Failed", "Stop Health Check...");
            }
            e.Cancel = true;
        }

        TransientMsgDlg HlthChkStatusDlg = null;
        HealthChkOpStatus CurHlthChkStatus = HealthChkOpStatus.Idle;

        private void HlthChkStatusCb(HealthChkOpStatus status, String statusMsg)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(this.GetType());
            switch (status)
            {
                case HealthChkOpStatus.LnkProf:
                case HealthChkOpStatus.FreqChns:
                case HealthChkOpStatus.AntPwr:
                case HealthChkOpStatus.TagInvtrySetup:
                    // Display Status
                    HlthChkStatusDlg.SetDpyMsg(statusMsg, "Setting up factory default parameters");
                    break;
                case HealthChkOpStatus.TagInvtryStarted:
                    HlthChkStatusDlg.SetDpyMsg(statusMsg, "Please place RFID Tag near Reader");
                    // Allow user to stop
                    this.Enabled = true;
                    HlthChkBttn.Text = "Cancel";
                    break;
                case HealthChkOpStatus.TagInvtryGotTag:
                    HlthChkStatusDlg.SetDpyMsg(statusMsg, "Tag Read Siccess!");
                    break;
                case HealthChkOpStatus.TagInvtryStopped:
                    HlthChkStatusDlg.SetDpyMsg(statusMsg + "\n" + "Close to continue", "Tag Inventory Finished!");
                    // Allow user to close dialog (auto-close)
                    HlthChkStatusDlg.Closing -= UserCloseHlthChkStatusDlg;
                    HlthChkStatusDlg.SetTimeout(3);
                    // re-enable Form
                    this.Enabled = true;
                    // Restore Button Label Text
                    HlthChkBttn.Text = resources.GetString("HlthChkBttn.Text");
                    break;
                case HealthChkOpStatus.errorStopped:
                    HlthChkStatusDlg.SetDpyMsg(statusMsg + "\n" + "Close to continue", "Tag Inventory Error!");
                    // Allow user to close dialog
                    HlthChkStatusDlg.Closing -= UserCloseHlthChkStatusDlg;
                    // re-enable Form
                    this.Enabled = true;
                    // Restore Button Label Text
                    HlthChkBttn.Text = resources.GetString("HlthChkBttn.Text");
                    break;
            }

            CurHlthChkStatus = status; // save last status
        }

      

        #endregion

        private void OnMacRegAddrTxtBxKeyPressed(object sender, KeyPressEventArgs e)
        {
            Program.HexKeyPressChk(sender, e);
        }

        private void OnMacRegRdBttnClicked(object sender, EventArgs e)
        {
            try
            {
                UInt16 RegAddr = UInt16.Parse(MacRegAddrTxtBx.Text, System.Globalization.NumberStyles.HexNumber);
               // RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                Rfid.st_RfidSpReq_RadioGetConfigurationParameter.handle = Rfid.st_RfidSpReq_RadioOpen.handle;
                Rfid.st_RfidSpReq_RadioGetConfigurationParameter.parameter = RegAddr;
               // HRESULT_RFID HRes = CF.f_CFlow_RfidDev_RadioGetConfigurationParameter(ref Rfid.st_RfidSpReq_RadioGetConfigurationParameter);
                HRESULT_RFID HRes = Rdr.f_CFlow_RfidDev_RadioGetConfigurationParameter();
                
                if (RfidSp.SUCCEEDED(HRes))
                {
                    MacRegValLbl.Text = "0x" + Rfid.st_RfidSpReq_RadioGetConfigurationParameter.value.ToString("X8");
                }
                else
                    Program.ShowError("Failed to read register: " + HRes.ToString("F"));
            }
            catch (ArgumentNullException ane)
            {
                Program.ShowError("Invalid hex digits input in Address field: " + ane.Message);
            }
            catch (ArgumentException ae)
            {
                Program.ShowError("Please input 16-bit Hex Digits (x4): " + ae.Message);
            }
            catch (FormatException fe)
            {
                Program.ShowError("Invalid hex digits input in Address field: " + fe.Message);
            }
            catch (OverflowException oe)
            {
                Program.ShowError("Overflow? " + oe.Message);
            }
        }


    }
}