/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Asset Search functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmSearchAsset_New : Form
    {
        private int searchOption;

        private DataTable dtAssetColumns;

        List<string> lstAssetColumns;       

        public frmSearchAsset_New()
        {
            InitializeComponent();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {

        }

        private void frmSearchAsset_Load(object sender, EventArgs e)
        {
            pnlSearchOptions.Visible = false;

            pnlSearch.Visible = true;
            pnlSearch.BringToFront();
           

            Cursor.Current = Cursors.WaitCursor;

            searchOption = UserPref.GetInstance().SearchOption;

            SetRadioButtons(searchOption);

           
           
#region"Set Location Combo"
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();

            DataRow dr = dtList.NewRow();
            dr["ID_Location"] = 0;
            dr["Name"] = "--ALL--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            dr = dtList.NewRow();
            dr["ID_Location"] = -2;
            dr["Name"] = "--SELECT--";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "ID_Location";
            cboLoc.DisplayMember = "Name";
            cboLoc.DataSource = dtList;

          //  cboLoc.SelectedValue = -2;
            cboLoc.SelectedValue = 0;
#endregion

            #region"Set Parent Group"
            //Set Location Combo
            DataTable dtGList = new DataTable();
            dtGList = AssetGroups.getGroups(0);

            DataRow drG = dtGList.NewRow();
            drG["ID_AssetGroup"] = -2;
            drG["Name"] = "--SELECT--";
            dtGList.Rows.Add(drG);
            dtGList.AcceptChanges();

            drG = dtGList.NewRow();
            drG["ID_AssetGroup"] = 0;
            drG["Name"] = "--ALL--";
            dtGList.Rows.Add(drG);
            dtGList.AcceptChanges();

            cboGroup.ValueMember = "ID_AssetGroup";
            cboGroup.DisplayMember = "Name";
            cboGroup.DataSource = dtGList;

            //cboGroup.SelectedValue = -2;
            cboGroup.SelectedValue = 0;

            #endregion


            #region"Set SUB Group"
            //Set Location Combo
            DataTable dtSGList = new DataTable();
            dtSGList = AssetGroups.getGroups(-3);

            DataRow drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = -2;
            drSG["Name"] = "--SELECT--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            drSG = dtSGList.NewRow();
            drSG["ID_AssetGroup"] = 0;
            drSG["Name"] = "--ALL--";
            dtSGList.Rows.Add(drSG);
            dtSGList.AcceptChanges();

            cboSubGroup.ValueMember = "ID_AssetGroup";
            cboSubGroup.DisplayMember = "Name";
            cboSubGroup.DataSource = dtSGList;

            //cboSubGroup.SelectedValue = -2;
            cboSubGroup.SelectedValue = 0;
            #endregion

            CreateDataTable();

            cmbColumn1.DataSource = dtAssetColumns;
            cmbColumn1.DisplayMember = "ColumnDispName";
            cmbColumn1.ValueMember = "ColumnName";

            cmbColumn2.DataSource = dtAssetColumns.Copy();
            cmbColumn2.DisplayMember = "ColumnDispName";
            cmbColumn2.ValueMember = "ColumnName";

            cmbColumn3.DataSource = dtAssetColumns.Copy();
            cmbColumn3.DisplayMember = "ColumnDispName";
            cmbColumn3.ValueMember = "ColumnName";

            Cursor.Current = Cursors.Default; 
        }

        private void cboGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboGroup.SelectedValue) != 0 && Convert.ToInt32(cboGroup.SelectedValue) != -2)
                {
                    #region"Set Child Group"
                    DataTable dtGList = new DataTable();

                    dtGList = AssetGroups.getGroups(Convert.ToInt32(cboGroup.SelectedValue));
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                    #endregion
                }
                else
                {
                    DataTable dtGList = new DataTable();
                    dtGList = AssetGroups.getGroups(-2);
                    DataRow drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = -2;
                    drG["Name"] = "--SELECT--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    drG = dtGList.NewRow();
                    drG["ID_AssetGroup"] = 0;
                    drG["Name"] = "--ALL--";
                    dtGList.Rows.Add(drG);
                    dtGList.AcceptChanges();

                    cboSubGroup.ValueMember = "ID_AssetGroup";
                    cboSubGroup.DisplayMember = "Name";
                    cboSubGroup.DataSource = dtGList;

                    cboSubGroup.SelectedValue = 0;
                }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (Convert.ToInt32(cboLoc.SelectedValue) == -2)
                {
                    MessageBox.Show("Please Select any Location.");
                    return; 
                }
                if (Convert.ToInt32(cboGroup.SelectedValue) == -2)
                {
                    MessageBox.Show("Please Select any Group.");
                    return; 
                }

                if (Convert.ToInt32(cboSubGroup.SelectedValue) == -2)
                {
                    MessageBox.Show("Please Select any Sub Group.");
                    return; 
                }

                Dictionary<string, string> colValPair = new Dictionary<string, string>();

                if (txtValue1.Text.Trim().Length > 0)
                {
                    if (cmbColumn1.SelectedValue.ToString() != "0")
                    {
                        colValPair.Add(cmbColumn1.SelectedValue.ToString(), txtValue1.Text.Trim());
                    }

                }
                if (txtValue2.Text.Trim().Length > 0)
                {
                    if (cmbColumn2.SelectedValue.ToString() != "0")
                    {
                        if (colValPair.ContainsKey(cmbColumn2.SelectedValue.ToString()))
                        {
                            MessageBox.Show("Please select distinct fields.");
                            cmbColumn2.SelectedValue = "0";
                            cmbColumn2.Focus();
                            return;
                        }
                        else
                        {
                            colValPair.Add(cmbColumn2.SelectedValue.ToString(), txtValue2.Text.Trim());
                        }
                    }

                }
                if (txtValue3.Text.Trim().Length > 0)
                {
                    if (cmbColumn3.SelectedValue.ToString() != "0")
                    {
                        if (colValPair.ContainsKey(cmbColumn3.SelectedValue.ToString()))
                        {
                            MessageBox.Show("Please select distinct fields.");
                            cmbColumn3.SelectedValue = "0";
                            cmbColumn3.Focus();
                            return;
                        }
                        else
                        {
                            colValPair.Add(cmbColumn3.SelectedValue.ToString(), txtValue3.Text.Trim());
                        }
                    }

                }

            #region"Search Asset"
                DataTable dtGList = new DataTable();
                try
                {
                    Cursor.Current = Cursors.WaitCursor;                   
                    //dtGList = Assets.getSearchReasult(txtAssetNo.Text, txtAssetName.Text,Convert.ToInt32(cboLoc.SelectedValue), Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue),txtLotNo.Text);

                   // dtGList = Assets.getSearchReasult(txtAssetNo.Text, txtAssetName.Text, Convert.ToInt32(cboLoc.SelectedValue), Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue), txtLotNo.Text, searchOption);

                    dtGList = Assets.getSearchReasultNew(colValPair, Convert.ToInt32(cboLoc.SelectedValue), Convert.ToInt32(cboGroup.SelectedValue), Convert.ToInt32(cboSubGroup.SelectedValue), searchOption);
                                     
                   
                }
                catch (Exception ex)
                {
                    Logger.LogError(ex.Message); 
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            #endregion
            if (dtGList.Rows.Count != 0)
            {
                // Open new Tag Invtry window
                frmSearchlist TagList = new frmSearchlist();
                TagList.searchedData = dtGList;
                TagList.Show();  
                // disable form until this new form closed
                this.Enabled = false;
                TagList.Closed += new EventHandler(this.OnOperFrmClosed);
                //    // Open new Tag Invtry window
                //    TagRangingForm TagRangingFm = new TagRangingForm();
                //    TagRangingFm.SearchTagList = csvData.ToString();   
                //    TagRangingFm.Show();
                //    // disable form until this new form closed
                //    this.Enabled = false;
                //    TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Search data not found.");  
            }
        }
        catch (ApplicationException ap)
        {
            Program.ShowError(ap.Message.ToString());
            Logger.LogError(ap.Message); 
        }
        catch (System.Web.Services.Protocols.SoapException ex)
        {
            if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                Program.ShowError("Request from innvalid IP address.");
            else
                Program.ShowError("Network Protocol Failure.");
            Logger.LogError(ex.Message); 
        }
        catch (System.Data.SqlServerCe.SqlCeException)
        {
            MessageBox.Show("Data File is not able to access.");
        }
        catch (System.Net.WebException)
        {
            MessageBox.Show("Web exception occured.");
        }
        catch (Exception ep)
        {

            MessageBox.Show(ep.Message.ToString());
        }
        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnUpload_Click(object sender, EventArgs e)
        {
            DialogResult res;
            uploadCSV.Filter = "CSV|*.csv"; 
            res = uploadCSV.ShowDialog();
            String csvData = "";
            System.IO.StreamReader oRead;
            if (System.IO.File.Exists(uploadCSV.FileName))
            {
                oRead = System.IO.File.OpenText(uploadCSV.FileName);
                csvData = oRead.ReadToEnd();
                if (csvData.Trim().Length != 0)
                {
                    Cursor.Current = Cursors.WaitCursor; 

                    DataTable dtGList = new DataTable();
                    dtGList.Columns.Add("TagID");
                    dtGList.Columns.Add("Name");
                    dtGList.AcceptChanges();  
                    DataRow dr;
                    foreach(String tag in csvData.Split(",".ToCharArray()))
                    {
                        if (tag.Trim().Length != 0)
                        {
                            dr = dtGList.NewRow();
                            dr["TagID"] = tag.Trim();
                            dr["Name"] = tag.Trim();
                            dtGList.Rows.Add(dr);
                        }
                    }
                    dtGList.AcceptChanges();

                    Cursor.Current = Cursors.Default; 
//                    dtGList.LoadDataRow(, true);  

                    frmSearchlist TagList = new frmSearchlist();
                    TagList.searchedData = dtGList;
                    TagList.IsUploadFromFile = true;   
                    TagList.Show();
                    // disable form until this new form closed
                    this.Enabled = false;
                    TagList.Closed += new EventHandler(this.OnOperFrmClosed);


                    //// Open new Tag Invtry window
                    //TagRangingForm TagRangingFm = new TagRangingForm();
                    //TagRangingFm.SearchTagList = csvData;
                    //TagRangingFm.Show();
                    //// disable form until this new form closed
                    //this.Enabled = false;
                    //TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);
                }
                else
                {
                    MessageBox.Show("Invalid File.");
                }
            }
            //System.IO.File.OpenText(uploadCSV.FileName);
//System.IO.File.te

            //uploadCSV.FileName   
        }

        private void rdbtnAnyWordDigit_Click(object sender, EventArgs e)
        {
            searchOption = 1;
            SetRadioButtons(searchOption);          
        }

        private void SetRadioButtons(int option)
        {
            switch (option)
            {
                case 1:
                    rdbtnAnyWordDigit.Checked = true;                    
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = false;
                    break;
                case 2:
                    rdbtnAnyWordDigit.Checked = false;                   
                    rdbtnWholeString.Checked = true;
                    rdbtnExactString.Checked = false;
                    break;
                case 3:
                    rdbtnAnyWordDigit.Checked = false;                   
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = true;
                    break;
                default :
                    rdbtnAnyWordDigit.Checked = true;
                    rdbtnWholeString.Checked = false;
                    rdbtnExactString.Checked = false;
                    break;
            }

           
        }

        private void rdbtnWholeString_Click(object sender, EventArgs e)
        {
            searchOption = 2;
            SetRadioButtons(searchOption);       
        }

        private void rdbtnExactString_Click(object sender, EventArgs e)
        {
            searchOption = 3;
            SetRadioButtons(searchOption);       
        }

        private void btnSearchOptions_Click(object sender, EventArgs e)
        {
            pnlSearch.Visible = false;

            pnlSearchOptions.Visible = true;            
            pnlSearchOptions.BringToFront();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            UserPref.GetInstance().SearchOption = searchOption;
            pnlSearchOptions.Visible = false;

            pnlSearch.Visible = true;
            pnlSearch.BringToFront();
        }

        void CreateDataTable()
        {
            dtAssetColumns = new DataTable();

            DataColumn[] dcc = new DataColumn[] {new DataColumn("ColumnDispName"),
                                                 new DataColumn("ColumnName")
            };

            dtAssetColumns.Columns.AddRange(dcc);

            dtAssetColumns.AcceptChanges();

            DataRow dr;

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "0";
            dr["ColumnDispName"] = "--Select--";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "TagID";
            dr["ColumnDispName"] = "Tag ID";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Name";
            dr["ColumnDispName"] = "Item Name";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "AssetNo";
            dr["ColumnDispName"] = "Item No.";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Description";
            dr["ColumnDispName"] = "Item Description";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "LotNo";
            dr["ColumnDispName"] = "Lot No";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "ReferenceNo";
            dr["ColumnDispName"] = "Reference No";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "Allocation";
            dr["ColumnDispName"] = "Allocation";
            dtAssetColumns.Rows.Add(dr);

            dr = dtAssetColumns.NewRow();
            dr["ColumnName"] = "PartCode";
            dr["ColumnDispName"] = "Part Code";
            dtAssetColumns.Rows.Add(dr);


        }

        private void btnSelectLoc_Click(object sender, EventArgs e)
        {

        }
    }
}