namespace OnRamp
{
    partial class TagInvtryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagInvtryForm));
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.StartButton = new System.Windows.Forms.Button();
            this.LngLstButton = new System.Windows.Forms.Button();
            this.TagCntPrmptLabel = new System.Windows.Forms.Label();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.EPCListV = new System.Windows.Forms.ListView();
            this.EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.ClrButton = new System.Windows.Forms.Button();
            this.AmbTempLbl = new System.Windows.Forms.Label();
            this.TempPanel = new System.Windows.Forms.Panel();
            this.LimitLbl = new System.Windows.Forms.Label();
            this.PATempLbl = new System.Windows.Forms.Label();
            this.XcvrTempLbl = new System.Windows.Forms.Label();
            this.DutyCycTmr = new System.Windows.Forms.Timer();
            this.TagIDLbl = new System.Windows.Forms.Label();
            this.BttnHlfPanel = new System.Windows.Forms.Panel();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            this.TempPanel.SuspendLayout();
            this.BttnHlfPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // StartButton
            // 
            resources.ApplyResources(this.StartButton, "StartButton");
            this.StartButton.Name = "StartButton";
            this.StartButton.Click += new System.EventHandler(this.OnStartButtonClicked);
            // 
            // LngLstButton
            // 
            resources.ApplyResources(this.LngLstButton, "LngLstButton");
            this.LngLstButton.Name = "LngLstButton";
            this.LngLstButton.Click += new System.EventHandler(this.OnLongListButtonClicked);
            // 
            // TagCntPrmptLabel
            // 
            resources.ApplyResources(this.TagCntPrmptLabel, "TagCntPrmptLabel");
            this.TagCntPrmptLabel.Name = "TagCntPrmptLabel";
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // EPCListV
            // 
            this.EPCListV.Columns.Add(RowNumHdr);
            this.EPCListV.Columns.Add(this.EPCColHdr);
            this.EPCListV.FullRowSelect = true;
            this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.EPCListV, "EPCListV");
            this.EPCListV.Name = "EPCListV";
            this.EPCListV.View = System.Windows.Forms.View.Details;
            this.EPCListV.ItemActivate += new System.EventHandler(this.OnItemActivated);
            this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnSelIdxChanged);
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(this.EPCColHdr, "EPCColHdr");
            // 
            // ClrButton
            // 
            resources.ApplyResources(this.ClrButton, "ClrButton");
            this.ClrButton.Name = "ClrButton";
            this.ClrButton.Click += new System.EventHandler(this.OnClrButtonClicked);
            // 
            // AmbTempLbl
            // 
            resources.ApplyResources(this.AmbTempLbl, "AmbTempLbl");
            this.AmbTempLbl.Name = "AmbTempLbl";
            // 
            // TempPanel
            // 
            this.TempPanel.BackColor = System.Drawing.Color.White;
            this.TempPanel.Controls.Add(this.LimitLbl);
            this.TempPanel.Controls.Add(this.label6);
            this.TempPanel.Controls.Add(this.label5);
            this.TempPanel.Controls.Add(this.label4);
            this.TempPanel.Controls.Add(this.label3);
            this.TempPanel.Controls.Add(this.PATempLbl);
            this.TempPanel.Controls.Add(this.XcvrTempLbl);
            this.TempPanel.Controls.Add(this.AmbTempLbl);
            resources.ApplyResources(this.TempPanel, "TempPanel");
            this.TempPanel.Name = "TempPanel";
            // 
            // LimitLbl
            // 
            resources.ApplyResources(this.LimitLbl, "LimitLbl");
            this.LimitLbl.ForeColor = System.Drawing.Color.SaddleBrown;
            this.LimitLbl.Name = "LimitLbl";
            // 
            // PATempLbl
            // 
            resources.ApplyResources(this.PATempLbl, "PATempLbl");
            this.PATempLbl.Name = "PATempLbl";
            // 
            // XcvrTempLbl
            // 
            resources.ApplyResources(this.XcvrTempLbl, "XcvrTempLbl");
            this.XcvrTempLbl.Name = "XcvrTempLbl";
            // 
            // TagIDLbl
            // 
            resources.ApplyResources(this.TagIDLbl, "TagIDLbl");
            this.TagIDLbl.Name = "TagIDLbl";
            // 
            // BttnHlfPanel
            // 
            this.BttnHlfPanel.BackColor = System.Drawing.SystemColors.Window;
            this.BttnHlfPanel.Controls.Add(this.TagIDLbl);
            resources.ApplyResources(this.BttnHlfPanel, "BttnHlfPanel");
            this.BttnHlfPanel.Name = "BttnHlfPanel";
            // 
            // TagInvtryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.BttnHlfPanel);
            this.Controls.Add(this.TempPanel);
            this.Controls.Add(this.ClrButton);
            this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.TagCntPrmptLabel);
            this.Controls.Add(this.LngLstButton);
            this.Controls.Add(this.StartButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagInvtryForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.TempPanel.ResumeLayout(false);
            this.BttnHlfPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button StartButton;
        private System.Windows.Forms.Button LngLstButton;
        private System.Windows.Forms.Label TagCntPrmptLabel;
        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.ColumnHeader EPCColHdr;
        private System.Windows.Forms.Button ClrButton;
        private System.Windows.Forms.Label AmbTempLbl;
        private System.Windows.Forms.Panel TempPanel;
        private System.Windows.Forms.Label PATempLbl;
        private System.Windows.Forms.Label XcvrTempLbl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Timer DutyCycTmr;
        private System.Windows.Forms.Label LimitLbl;
        private System.Windows.Forms.Label TagIDLbl;
        private System.Windows.Forms.Panel BttnHlfPanel;
    }
}