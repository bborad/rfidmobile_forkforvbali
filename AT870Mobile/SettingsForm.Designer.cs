namespace OnRamp
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader columnHeader1;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.SttgsTabCtrl = new System.Windows.Forms.TabControl();
            this.RdrIDTab = new System.Windows.Forms.TabPage();
            this.RdrNameTxtBx = new System.Windows.Forms.TextBox();
            this.UserAuthTab = new System.Windows.Forms.TabPage();
            this.PwdTxtBx = new System.Windows.Forms.TextBox();
            this.UsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.SignOnChkBx = new System.Windows.Forms.CheckBox();
            this.SoundTab = new System.Windows.Forms.TabPage();
            this.EndMldyCmbBx = new System.Windows.Forms.ComboBox();
            this.StartMldyCmbBx = new System.Windows.Forms.ComboBox();
            this.SituationLstV = new System.Windows.Forms.ListView();
            this.SndVolBr = new System.Windows.Forms.TrackBar();
            this.DiagTab = new System.Windows.Forms.TabPage();
            this.MacRegPnl = new System.Windows.Forms.Panel();
            this.MacRegRdBttn = new System.Windows.Forms.Button();
            this.MacRegAddrTxtBx = new System.Windows.Forms.TextBox();
            this.MacRegValLbl = new System.Windows.Forms.Label();
            this.HlthChkBttn = new System.Windows.Forms.Button();
            this.RstHiTempBttn = new System.Windows.Forms.Button();
            this.PATempRecHiLbl = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.XcvrTempRecHiLbl = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.BeepTstCntLbl = new System.Windows.Forms.Label();
            this.BeepTstBttn = new System.Windows.Forms.Button();
            this.TraceLogChkBx = new System.Windows.Forms.CheckBox();
            this.TimeSyncTab = new System.Windows.Forms.TabPage();
            this.TimeSyncBttn = new System.Windows.Forms.Button();
            this.NTPSrvrTxtBx = new System.Windows.Forms.TextBox();
            this.HTTPTab = new System.Windows.Forms.TabPage();
            this.PrxyCfgPanel = new System.Windows.Forms.Panel();
            this.PrxyPasswdTxtBx = new System.Windows.Forms.TextBox();
            this.PrxyUsrNameTxtBx = new System.Windows.Forms.TextBox();
            this.PrxyBypassesTxtBx = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.PrxyPrtNumTxtBx = new System.Windows.Forms.TextBox();
            this.PrxySrvrTxtBx = new System.Windows.Forms.TextBox();
            this.ManProxyBttn = new System.Windows.Forms.RadioButton();
            this.NoProxyBttn = new System.Windows.Forms.RadioButton();
            this.VerTab = new System.Windows.Forms.TabPage();
            this.CntryLbl = new System.Windows.Forms.Label();
            this.FrqBndLbl = new System.Windows.Forms.Label();
            this.AppVerLbl = new System.Windows.Forms.Label();
            this.MACVerLbl = new System.Windows.Forms.Label();
            this.DrvVerLbl = new System.Windows.Forms.Label();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.RstButton = new System.Windows.Forms.Button();
            this.BeepTstTmr = new System.Windows.Forms.Timer();
            columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.SttgsTabCtrl.SuspendLayout();
            this.RdrIDTab.SuspendLayout();
            this.UserAuthTab.SuspendLayout();
            this.SoundTab.SuspendLayout();
            this.DiagTab.SuspendLayout();
            this.MacRegPnl.SuspendLayout();
            this.TimeSyncTab.SuspendLayout();
            this.HTTPTab.SuspendLayout();
            this.PrxyCfgPanel.SuspendLayout();
            this.VerTab.SuspendLayout();
            this.SuspendLayout();
            // 
            // columnHeader1
            // 
            resources.ApplyResources(columnHeader1, "columnHeader1");
            // 
            // label18
            // 
            resources.ApplyResources(this.label18, "label18");
            this.label18.Name = "label18";
            // 
            // label20
            // 
            resources.ApplyResources(this.label20, "label20");
            this.label20.Name = "label20";
            // 
            // label21
            // 
            resources.ApplyResources(this.label21, "label21");
            this.label21.Name = "label21";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label16
            // 
            resources.ApplyResources(this.label16, "label16");
            this.label16.Name = "label16";
            // 
            // label19
            // 
            resources.ApplyResources(this.label19, "label19");
            this.label19.Name = "label19";
            // 
            // label22
            // 
            resources.ApplyResources(this.label22, "label22");
            this.label22.Name = "label22";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // label10
            // 
            resources.ApplyResources(this.label10, "label10");
            this.label10.Name = "label10";
            // 
            // label11
            // 
            resources.ApplyResources(this.label11, "label11");
            this.label11.ForeColor = System.Drawing.Color.Gray;
            this.label11.Name = "label11";
            // 
            // label12
            // 
            resources.ApplyResources(this.label12, "label12");
            this.label12.Name = "label12";
            // 
            // label13
            // 
            resources.ApplyResources(this.label13, "label13");
            this.label13.Name = "label13";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label14
            // 
            resources.ApplyResources(this.label14, "label14");
            this.label14.Name = "label14";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label28
            // 
            resources.ApplyResources(this.label28, "label28");
            this.label28.Name = "label28";
            // 
            // label29
            // 
            resources.ApplyResources(this.label29, "label29");
            this.label29.Name = "label29";
            // 
            // SttgsTabCtrl
            // 
            this.SttgsTabCtrl.Controls.Add(this.RdrIDTab);
            this.SttgsTabCtrl.Controls.Add(this.UserAuthTab);
            this.SttgsTabCtrl.Controls.Add(this.SoundTab);
            this.SttgsTabCtrl.Controls.Add(this.DiagTab);
            this.SttgsTabCtrl.Controls.Add(this.TimeSyncTab);
            this.SttgsTabCtrl.Controls.Add(this.HTTPTab);
            this.SttgsTabCtrl.Controls.Add(this.VerTab);
            resources.ApplyResources(this.SttgsTabCtrl, "SttgsTabCtrl");
            this.SttgsTabCtrl.Name = "SttgsTabCtrl";
            this.SttgsTabCtrl.SelectedIndex = 0;
            this.SttgsTabCtrl.SelectedIndexChanged += new System.EventHandler(this.OnSelectedPageChanged);
            // 
            // RdrIDTab
            // 
            this.RdrIDTab.BackColor = System.Drawing.Color.White;
            this.RdrIDTab.Controls.Add(this.RdrNameTxtBx);
            this.RdrIDTab.Controls.Add(this.label14);
            resources.ApplyResources(this.RdrIDTab, "RdrIDTab");
            this.RdrIDTab.Name = "RdrIDTab";
            // 
            // RdrNameTxtBx
            // 
            resources.ApplyResources(this.RdrNameTxtBx, "RdrNameTxtBx");
            this.RdrNameTxtBx.Name = "RdrNameTxtBx";
            this.RdrNameTxtBx.TextChanged += new System.EventHandler(this.OnRdrNameTxtBxChanged);
            // 
            // UserAuthTab
            // 
            this.UserAuthTab.BackColor = System.Drawing.Color.White;
            this.UserAuthTab.Controls.Add(this.PwdTxtBx);
            this.UserAuthTab.Controls.Add(this.UsrNameTxtBx);
            this.UserAuthTab.Controls.Add(this.label29);
            this.UserAuthTab.Controls.Add(this.label28);
            this.UserAuthTab.Controls.Add(this.SignOnChkBx);
            resources.ApplyResources(this.UserAuthTab, "UserAuthTab");
            this.UserAuthTab.Name = "UserAuthTab";
            // 
            // PwdTxtBx
            // 
            resources.ApplyResources(this.PwdTxtBx, "PwdTxtBx");
            this.PwdTxtBx.Name = "PwdTxtBx";
            this.PwdTxtBx.TextChanged += new System.EventHandler(this.OnPwdTxtBxChanged);
            this.PwdTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPwdTxtBxKeyPress);
            // 
            // UsrNameTxtBx
            // 
            resources.ApplyResources(this.UsrNameTxtBx, "UsrNameTxtBx");
            this.UsrNameTxtBx.Name = "UsrNameTxtBx";
            this.UsrNameTxtBx.TextChanged += new System.EventHandler(this.OnUsrNameTxtBxChanged);
            this.UsrNameTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnUsrNameTxtBxKeyPress);
            // 
            // SignOnChkBx
            // 
            resources.ApplyResources(this.SignOnChkBx, "SignOnChkBx");
            this.SignOnChkBx.Name = "SignOnChkBx";
            this.SignOnChkBx.CheckStateChanged += new System.EventHandler(this.OnSignOnChkBxChanged);
            // 
            // SoundTab
            // 
            this.SoundTab.BackColor = System.Drawing.Color.White;
            this.SoundTab.Controls.Add(this.label5);
            this.SoundTab.Controls.Add(this.EndMldyCmbBx);
            this.SoundTab.Controls.Add(this.label4);
            this.SoundTab.Controls.Add(this.StartMldyCmbBx);
            this.SoundTab.Controls.Add(this.SituationLstV);
            this.SoundTab.Controls.Add(this.label3);
            this.SoundTab.Controls.Add(this.label2);
            this.SoundTab.Controls.Add(this.label1);
            this.SoundTab.Controls.Add(this.SndVolBr);
            resources.ApplyResources(this.SoundTab, "SoundTab");
            this.SoundTab.Name = "SoundTab";
            // 
            // EndMldyCmbBx
            // 
            this.EndMldyCmbBx.Items.Add(resources.GetString("EndMldyCmbBx.Items"));
            this.EndMldyCmbBx.Items.Add(resources.GetString("EndMldyCmbBx.Items1"));
            this.EndMldyCmbBx.Items.Add(resources.GetString("EndMldyCmbBx.Items2"));
            this.EndMldyCmbBx.Items.Add(resources.GetString("EndMldyCmbBx.Items3"));
            this.EndMldyCmbBx.Items.Add(resources.GetString("EndMldyCmbBx.Items4"));
            resources.ApplyResources(this.EndMldyCmbBx, "EndMldyCmbBx");
            this.EndMldyCmbBx.Name = "EndMldyCmbBx";
            this.EndMldyCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnEndMldyChanged);
            // 
            // StartMldyCmbBx
            // 
            this.StartMldyCmbBx.Items.Add(resources.GetString("StartMldyCmbBx.Items"));
            this.StartMldyCmbBx.Items.Add(resources.GetString("StartMldyCmbBx.Items1"));
            this.StartMldyCmbBx.Items.Add(resources.GetString("StartMldyCmbBx.Items2"));
            this.StartMldyCmbBx.Items.Add(resources.GetString("StartMldyCmbBx.Items3"));
            this.StartMldyCmbBx.Items.Add(resources.GetString("StartMldyCmbBx.Items4"));
            resources.ApplyResources(this.StartMldyCmbBx, "StartMldyCmbBx");
            this.StartMldyCmbBx.Name = "StartMldyCmbBx";
            this.StartMldyCmbBx.SelectedIndexChanged += new System.EventHandler(this.OnStartMldyChanged);
            // 
            // SituationLstV
            // 
            this.SituationLstV.CheckBoxes = true;
            this.SituationLstV.Columns.Add(columnHeader1);
            this.SituationLstV.FullRowSelect = true;
            this.SituationLstV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.SituationLstV, "SituationLstV");
            this.SituationLstV.Name = "SituationLstV";
            this.SituationLstV.View = System.Windows.Forms.View.Details;
            this.SituationLstV.ItemActivate += new System.EventHandler(this.OnSituationActivate);
            this.SituationLstV.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.OnSituationChanged);
            // 
            // SndVolBr
            // 
            this.SndVolBr.LargeChange = 1;
            resources.ApplyResources(this.SndVolBr, "SndVolBr");
            this.SndVolBr.Maximum = 3;
            this.SndVolBr.Minimum = 1;
            this.SndVolBr.Name = "SndVolBr";
            this.SndVolBr.TickStyle = System.Windows.Forms.TickStyle.TopLeft;
            this.SndVolBr.Value = 1;
            this.SndVolBr.ValueChanged += new System.EventHandler(this.OnVolChanged);
            // 
            // DiagTab
            // 
            this.DiagTab.BackColor = System.Drawing.Color.White;
            this.DiagTab.Controls.Add(this.MacRegPnl);
            this.DiagTab.Controls.Add(this.HlthChkBttn);
            this.DiagTab.Controls.Add(this.RstHiTempBttn);
            this.DiagTab.Controls.Add(this.PATempRecHiLbl);
            this.DiagTab.Controls.Add(this.label17);
            this.DiagTab.Controls.Add(this.XcvrTempRecHiLbl);
            this.DiagTab.Controls.Add(this.label15);
            this.DiagTab.Controls.Add(this.BeepTstCntLbl);
            this.DiagTab.Controls.Add(this.BeepTstBttn);
            this.DiagTab.Controls.Add(this.TraceLogChkBx);
            resources.ApplyResources(this.DiagTab, "DiagTab");
            this.DiagTab.Name = "DiagTab";
            // 
            // MacRegPnl
            // 
            this.MacRegPnl.BackColor = System.Drawing.Color.Wheat;
            this.MacRegPnl.Controls.Add(this.MacRegRdBttn);
            this.MacRegPnl.Controls.Add(this.MacRegAddrTxtBx);
            this.MacRegPnl.Controls.Add(this.MacRegValLbl);
            resources.ApplyResources(this.MacRegPnl, "MacRegPnl");
            this.MacRegPnl.Name = "MacRegPnl";
            // 
            // MacRegRdBttn
            // 
            resources.ApplyResources(this.MacRegRdBttn, "MacRegRdBttn");
            this.MacRegRdBttn.Name = "MacRegRdBttn";
            this.MacRegRdBttn.Click += new System.EventHandler(this.OnMacRegRdBttnClicked);
            // 
            // MacRegAddrTxtBx
            // 
            resources.ApplyResources(this.MacRegAddrTxtBx, "MacRegAddrTxtBx");
            this.MacRegAddrTxtBx.Name = "MacRegAddrTxtBx";
            this.MacRegAddrTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnMacRegAddrTxtBxKeyPressed);
            // 
            // MacRegValLbl
            // 
            resources.ApplyResources(this.MacRegValLbl, "MacRegValLbl");
            this.MacRegValLbl.Name = "MacRegValLbl";
            // 
            // HlthChkBttn
            // 
            resources.ApplyResources(this.HlthChkBttn, "HlthChkBttn");
            this.HlthChkBttn.Name = "HlthChkBttn";
            this.HlthChkBttn.Click += new System.EventHandler(this.OnHlthChkBttnClicked);
            // 
            // RstHiTempBttn
            // 
            resources.ApplyResources(this.RstHiTempBttn, "RstHiTempBttn");
            this.RstHiTempBttn.Name = "RstHiTempBttn";
            this.RstHiTempBttn.Click += new System.EventHandler(this.OnRstTempBttnClicked);
            // 
            // PATempRecHiLbl
            // 
            resources.ApplyResources(this.PATempRecHiLbl, "PATempRecHiLbl");
            this.PATempRecHiLbl.Name = "PATempRecHiLbl";
            // 
            // label17
            // 
            resources.ApplyResources(this.label17, "label17");
            this.label17.Name = "label17";
            // 
            // XcvrTempRecHiLbl
            // 
            resources.ApplyResources(this.XcvrTempRecHiLbl, "XcvrTempRecHiLbl");
            this.XcvrTempRecHiLbl.Name = "XcvrTempRecHiLbl";
            // 
            // label15
            // 
            resources.ApplyResources(this.label15, "label15");
            this.label15.Name = "label15";
            // 
            // BeepTstCntLbl
            // 
            resources.ApplyResources(this.BeepTstCntLbl, "BeepTstCntLbl");
            this.BeepTstCntLbl.Name = "BeepTstCntLbl";
            // 
            // BeepTstBttn
            // 
            resources.ApplyResources(this.BeepTstBttn, "BeepTstBttn");
            this.BeepTstBttn.Name = "BeepTstBttn";
            this.BeepTstBttn.Click += new System.EventHandler(this.OnBeepTstClicked);
            // 
            // TraceLogChkBx
            // 
            resources.ApplyResources(this.TraceLogChkBx, "TraceLogChkBx");
            this.TraceLogChkBx.Name = "TraceLogChkBx";
            this.TraceLogChkBx.CheckStateChanged += new System.EventHandler(this.OnTraceLogChanged);
            // 
            // TimeSyncTab
            // 
            this.TimeSyncTab.BackColor = System.Drawing.Color.White;
            this.TimeSyncTab.Controls.Add(this.TimeSyncBttn);
            this.TimeSyncTab.Controls.Add(this.label6);
            this.TimeSyncTab.Controls.Add(this.NTPSrvrTxtBx);
            resources.ApplyResources(this.TimeSyncTab, "TimeSyncTab");
            this.TimeSyncTab.Name = "TimeSyncTab";
            // 
            // TimeSyncBttn
            // 
            resources.ApplyResources(this.TimeSyncBttn, "TimeSyncBttn");
            this.TimeSyncBttn.Name = "TimeSyncBttn";
            this.TimeSyncBttn.Click += new System.EventHandler(this.OnTimeSyncBttnClicked);
            // 
            // NTPSrvrTxtBx
            // 
            resources.ApplyResources(this.NTPSrvrTxtBx, "NTPSrvrTxtBx");
            this.NTPSrvrTxtBx.Name = "NTPSrvrTxtBx";
            this.NTPSrvrTxtBx.TextChanged += new System.EventHandler(this.OnNtpSrvrTxtBxChanged);
            // 
            // HTTPTab
            // 
            this.HTTPTab.BackColor = System.Drawing.Color.White;
            this.HTTPTab.Controls.Add(this.PrxyCfgPanel);
            this.HTTPTab.Controls.Add(this.ManProxyBttn);
            this.HTTPTab.Controls.Add(this.NoProxyBttn);
            resources.ApplyResources(this.HTTPTab, "HTTPTab");
            this.HTTPTab.Name = "HTTPTab";
            // 
            // PrxyCfgPanel
            // 
            this.PrxyCfgPanel.BackColor = System.Drawing.Color.White;
            this.PrxyCfgPanel.Controls.Add(this.PrxyPasswdTxtBx);
            this.PrxyCfgPanel.Controls.Add(this.label12);
            this.PrxyCfgPanel.Controls.Add(this.label13);
            this.PrxyCfgPanel.Controls.Add(this.PrxyUsrNameTxtBx);
            this.PrxyCfgPanel.Controls.Add(this.label11);
            this.PrxyCfgPanel.Controls.Add(this.PrxyBypassesTxtBx);
            this.PrxyCfgPanel.Controls.Add(this.label10);
            this.PrxyCfgPanel.Controls.Add(this.label9);
            this.PrxyCfgPanel.Controls.Add(this.PrxyPrtNumTxtBx);
            this.PrxyCfgPanel.Controls.Add(this.PrxySrvrTxtBx);
            this.PrxyCfgPanel.Controls.Add(this.label8);
            resources.ApplyResources(this.PrxyCfgPanel, "PrxyCfgPanel");
            this.PrxyCfgPanel.Name = "PrxyCfgPanel";
            // 
            // PrxyPasswdTxtBx
            // 
            resources.ApplyResources(this.PrxyPasswdTxtBx, "PrxyPasswdTxtBx");
            this.PrxyPasswdTxtBx.Name = "PrxyPasswdTxtBx";
            this.PrxyPasswdTxtBx.TextChanged += new System.EventHandler(this.OnPrxyPasswdTxtBxChanged);
            this.PrxyPasswdTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPrxyPasswdKeyPressed);
            // 
            // PrxyUsrNameTxtBx
            // 
            resources.ApplyResources(this.PrxyUsrNameTxtBx, "PrxyUsrNameTxtBx");
            this.PrxyUsrNameTxtBx.Name = "PrxyUsrNameTxtBx";
            this.PrxyUsrNameTxtBx.TextChanged += new System.EventHandler(this.OnPrxyUsrNameTxtBxChanged);
            this.PrxyUsrNameTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPrxyUsrNameKeyPressed);
            // 
            // PrxyBypassesTxtBx
            // 
            resources.ApplyResources(this.PrxyBypassesTxtBx, "PrxyBypassesTxtBx");
            this.PrxyBypassesTxtBx.Name = "PrxyBypassesTxtBx";
            this.PrxyBypassesTxtBx.TabStop = false;
            this.PrxyBypassesTxtBx.TextChanged += new System.EventHandler(this.OnPrxyBypassesTxtBxChanged);
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // PrxyPrtNumTxtBx
            // 
            resources.ApplyResources(this.PrxyPrtNumTxtBx, "PrxyPrtNumTxtBx");
            this.PrxyPrtNumTxtBx.Name = "PrxyPrtNumTxtBx";
            this.PrxyPrtNumTxtBx.TextChanged += new System.EventHandler(this.OnPrxyPrtTxtBxChanged);
            this.PrxyPrtNumTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPrxyPrtKeyPressed);
            // 
            // PrxySrvrTxtBx
            // 
            resources.ApplyResources(this.PrxySrvrTxtBx, "PrxySrvrTxtBx");
            this.PrxySrvrTxtBx.Name = "PrxySrvrTxtBx";
            this.PrxySrvrTxtBx.TabStop = false;
            this.PrxySrvrTxtBx.TextChanged += new System.EventHandler(this.OnPrxySrvrAddrTxtBxChanged);
            this.PrxySrvrTxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.OnPrxySrvrAddrKeyPressed);
            // 
            // ManProxyBttn
            // 
            resources.ApplyResources(this.ManProxyBttn, "ManProxyBttn");
            this.ManProxyBttn.Name = "ManProxyBttn";
            this.ManProxyBttn.TabStop = false;
            this.ManProxyBttn.CheckedChanged += new System.EventHandler(this.OnManProxyBttnChecked);
            // 
            // NoProxyBttn
            // 
            this.NoProxyBttn.Checked = true;
            resources.ApplyResources(this.NoProxyBttn, "NoProxyBttn");
            this.NoProxyBttn.Name = "NoProxyBttn";
            this.NoProxyBttn.CheckedChanged += new System.EventHandler(this.OnNoProxyBttnChecked);
            // 
            // VerTab
            // 
            this.VerTab.BackColor = System.Drawing.Color.White;
            this.VerTab.Controls.Add(this.CntryLbl);
            this.VerTab.Controls.Add(this.FrqBndLbl);
            this.VerTab.Controls.Add(this.label22);
            this.VerTab.Controls.Add(this.label19);
            this.VerTab.Controls.Add(this.label16);
            this.VerTab.Controls.Add(this.label7);
            this.VerTab.Controls.Add(this.AppVerLbl);
            this.VerTab.Controls.Add(this.label21);
            this.VerTab.Controls.Add(this.MACVerLbl);
            this.VerTab.Controls.Add(this.label18);
            this.VerTab.Controls.Add(this.DrvVerLbl);
            this.VerTab.Controls.Add(this.label20);
            resources.ApplyResources(this.VerTab, "VerTab");
            this.VerTab.Name = "VerTab";
            // 
            // CntryLbl
            // 
            resources.ApplyResources(this.CntryLbl, "CntryLbl");
            this.CntryLbl.Name = "CntryLbl";
            // 
            // FrqBndLbl
            // 
            resources.ApplyResources(this.FrqBndLbl, "FrqBndLbl");
            this.FrqBndLbl.Name = "FrqBndLbl";
            // 
            // AppVerLbl
            // 
            resources.ApplyResources(this.AppVerLbl, "AppVerLbl");
            this.AppVerLbl.Name = "AppVerLbl";
            // 
            // MACVerLbl
            // 
            resources.ApplyResources(this.MACVerLbl, "MACVerLbl");
            this.MACVerLbl.Name = "MACVerLbl";
            // 
            // DrvVerLbl
            // 
            resources.ApplyResources(this.DrvVerLbl, "DrvVerLbl");
            this.DrvVerLbl.Name = "DrvVerLbl";
            // 
            // ApplyButton
            // 
            resources.ApplyResources(this.ApplyButton, "ApplyButton");
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Click += new System.EventHandler(this.OnApplyButtonClicked);
            // 
            // RstButton
            // 
            resources.ApplyResources(this.RstButton, "RstButton");
            this.RstButton.Name = "RstButton";
            this.RstButton.Click += new System.EventHandler(this.OnRstButtonClicked);
            // 
            // BeepTstTmr
            // 
            this.BeepTstTmr.Interval = 200;
            this.BeepTstTmr.Tick += new System.EventHandler(this.OnBeepTstTmrTick);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.SttgsTabCtrl);
            this.Controls.Add(this.RstButton);
            this.Controls.Add(this.ApplyButton);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsForm";
            this.Load += new System.EventHandler(this.OnFormLoad);
            this.Closed += new System.EventHandler(this.OnFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnFormClosing);
            this.SttgsTabCtrl.ResumeLayout(false);
            this.RdrIDTab.ResumeLayout(false);
            this.UserAuthTab.ResumeLayout(false);
            this.SoundTab.ResumeLayout(false);
            this.DiagTab.ResumeLayout(false);
            this.MacRegPnl.ResumeLayout(false);
            this.TimeSyncTab.ResumeLayout(false);
            this.HTTPTab.ResumeLayout(false);
            this.PrxyCfgPanel.ResumeLayout(false);
            this.VerTab.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl SttgsTabCtrl;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button RstButton;
        private System.Windows.Forms.TabPage UserAuthTab;
        private System.Windows.Forms.CheckBox SignOnChkBx;
        private System.Windows.Forms.TextBox PwdTxtBx;
        private System.Windows.Forms.TextBox UsrNameTxtBx;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TabPage SoundTab;
        private System.Windows.Forms.TrackBar SndVolBr;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView SituationLstV;
        private System.Windows.Forms.ComboBox StartMldyCmbBx;
        private System.Windows.Forms.ComboBox EndMldyCmbBx;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Timer BeepTstTmr;
        private System.Windows.Forms.TabPage RdrIDTab;
        private System.Windows.Forms.TextBox RdrNameTxtBx;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TabPage TimeSyncTab;
        private System.Windows.Forms.Button TimeSyncBttn;
        private System.Windows.Forms.TextBox NTPSrvrTxtBx;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TabPage HTTPTab;
        private System.Windows.Forms.RadioButton ManProxyBttn;
        private System.Windows.Forms.RadioButton NoProxyBttn;
        private System.Windows.Forms.TextBox PrxySrvrTxtBx;
        private System.Windows.Forms.Panel PrxyCfgPanel;
        private System.Windows.Forms.TextBox PrxyPrtNumTxtBx;
        private System.Windows.Forms.TextBox PrxyBypassesTxtBx;
        private System.Windows.Forms.TextBox PrxyPasswdTxtBx;
        private System.Windows.Forms.TextBox PrxyUsrNameTxtBx;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage DiagTab;
        private System.Windows.Forms.Button RstHiTempBttn;
        private System.Windows.Forms.Label PATempRecHiLbl;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label XcvrTempRecHiLbl;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label BeepTstCntLbl;
        private System.Windows.Forms.Button BeepTstBttn;
        private System.Windows.Forms.CheckBox TraceLogChkBx;
        private System.Windows.Forms.TabPage VerTab;
        private System.Windows.Forms.Label AppVerLbl;
        private System.Windows.Forms.Label MACVerLbl;
        private System.Windows.Forms.Label DrvVerLbl;
        private System.Windows.Forms.Label FrqBndLbl;
        private System.Windows.Forms.Label CntryLbl;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button HlthChkBttn;
        private System.Windows.Forms.Panel MacRegPnl;
        private System.Windows.Forms.TextBox MacRegAddrTxtBx;
        private System.Windows.Forms.Label MacRegValLbl;
        private System.Windows.Forms.Button MacRegRdBttn;
        private System.Windows.Forms.Label label9;
    }
}