namespace OnRamp
{
    partial class TagConfirmForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.ColumnHeader RowNumHdr;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagConfirmForm));
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.TagCntLabel = new System.Windows.Forms.Label();
            this.EPCListV = new System.Windows.Forms.ListView();
            this.EPCColHdr = new System.Windows.Forms.ColumnHeader();
            this.ScanButton = new System.Windows.Forms.Button();
            this.PwdReqChkBx = new System.Windows.Forms.CheckBox();
            this.btnDispatch = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.lsvTotal = new System.Windows.Forms.ListView();
            this.colLotNO = new System.Windows.Forms.ColumnHeader();
            this.colTotal = new System.Windows.Forms.ColumnHeader();
            this.btnScreen = new System.Windows.Forms.Button();
            this.btnAll = new System.Windows.Forms.Button();
            this.pnlDispatch = new System.Windows.Forms.Panel();
            this.lblPage = new System.Windows.Forms.Label();
            this.cmbPage = new System.Windows.Forms.ComboBox();
            this.lstAsset = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.AssetName = new System.Windows.Forms.ColumnHeader();
            this.colDSLotNO = new System.Windows.Forms.ColumnHeader();
            this.colDSTagID = new System.Windows.Forms.ColumnHeader();
            this.btnDispatchAll = new System.Windows.Forms.Button();
            this.btnScanBarcode = new System.Windows.Forms.Button();
            RowNumHdr = new System.Windows.Forms.ColumnHeader();
            this.pnlDispatch.SuspendLayout();
            this.SuspendLayout();
            // 
            // RowNumHdr
            // 
            resources.ApplyResources(RowNumHdr, "RowNumHdr");
            // 
            // TagCntLabel
            // 
            resources.ApplyResources(this.TagCntLabel, "TagCntLabel");
            this.TagCntLabel.ForeColor = System.Drawing.Color.Indigo;
            this.TagCntLabel.Name = "TagCntLabel";
            // 
            // EPCListV
            // 
            this.EPCListV.Columns.Add(RowNumHdr);
            this.EPCListV.Columns.Add(this.EPCColHdr);
            this.EPCListV.FullRowSelect = true;
            this.EPCListV.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            resources.ApplyResources(this.EPCListV, "EPCListV");
            this.EPCListV.Name = "EPCListV";
            this.EPCListV.View = System.Windows.Forms.View.Details;
            this.EPCListV.SelectedIndexChanged += new System.EventHandler(this.OnEPCListVSelChanged);
            // 
            // EPCColHdr
            // 
            resources.ApplyResources(this.EPCColHdr, "EPCColHdr");
            // 
            // ScanButton
            // 
            resources.ApplyResources(this.ScanButton, "ScanButton");
            this.ScanButton.Name = "ScanButton";
            this.ScanButton.Click += new System.EventHandler(this.OnScanButtonClicked);
            // 
            // PwdReqChkBx
            // 
            resources.ApplyResources(this.PwdReqChkBx, "PwdReqChkBx");
            this.PwdReqChkBx.Name = "PwdReqChkBx";
            this.PwdReqChkBx.CheckStateChanged += new System.EventHandler(this.PwdReqChkBx_CheckStateChanged);
            // 
            // btnDispatch
            // 
            resources.ApplyResources(this.btnDispatch, "btnDispatch");
            this.btnDispatch.Name = "btnDispatch";
            this.btnDispatch.Click += new System.EventHandler(this.btnDispatch_Click);
            // 
            // btnClear
            // 
            resources.ApplyResources(this.btnClear, "btnClear");
            this.btnClear.Name = "btnClear";
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // lsvTotal
            // 
            this.lsvTotal.Columns.Add(this.colLotNO);
            this.lsvTotal.Columns.Add(this.colTotal);
            resources.ApplyResources(this.lsvTotal, "lsvTotal");
            this.lsvTotal.Name = "lsvTotal";
            this.lsvTotal.View = System.Windows.Forms.View.Details;
            // 
            // colLotNO
            // 
            resources.ApplyResources(this.colLotNO, "colLotNO");
            // 
            // colTotal
            // 
            resources.ApplyResources(this.colTotal, "colTotal");
            // 
            // btnScreen
            // 
            resources.ApplyResources(this.btnScreen, "btnScreen");
            this.btnScreen.Name = "btnScreen";
            this.btnScreen.Tag = "Dispatch";
            this.btnScreen.Click += new System.EventHandler(this.btnScreen_Click);
            // 
            // btnAll
            // 
            resources.ApplyResources(this.btnAll, "btnAll");
            this.btnAll.Name = "btnAll";
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // pnlDispatch
            // 
            this.pnlDispatch.Controls.Add(this.lblPage);
            this.pnlDispatch.Controls.Add(this.cmbPage);
            this.pnlDispatch.Controls.Add(this.lstAsset);
            resources.ApplyResources(this.pnlDispatch, "pnlDispatch");
            this.pnlDispatch.Name = "pnlDispatch";
            // 
            // lblPage
            // 
            resources.ApplyResources(this.lblPage, "lblPage");
            this.lblPage.Name = "lblPage";
            // 
            // cmbPage
            // 
            resources.ApplyResources(this.cmbPage, "cmbPage");
            this.cmbPage.Name = "cmbPage";
            // 
            // lstAsset
            // 
            this.lstAsset.Columns.Add(this.columnHeader1);
            this.lstAsset.Columns.Add(this.AssetName);
            this.lstAsset.Columns.Add(this.colDSLotNO);
            this.lstAsset.Columns.Add(this.colDSTagID);
            resources.ApplyResources(this.lstAsset, "lstAsset");
            this.lstAsset.Name = "lstAsset";
            this.lstAsset.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            resources.ApplyResources(this.columnHeader1, "columnHeader1");
            // 
            // AssetName
            // 
            resources.ApplyResources(this.AssetName, "AssetName");
            // 
            // colDSLotNO
            // 
            resources.ApplyResources(this.colDSLotNO, "colDSLotNO");
            // 
            // colDSTagID
            // 
            resources.ApplyResources(this.colDSTagID, "colDSTagID");
            // 
            // btnDispatchAll
            // 
            resources.ApplyResources(this.btnDispatchAll, "btnDispatchAll");
            this.btnDispatchAll.Name = "btnDispatchAll";
            this.btnDispatchAll.Click += new System.EventHandler(this.btnDispatchAll_Click);
            // 
            // btnScanBarcode
            // 
            resources.ApplyResources(this.btnScanBarcode, "btnScanBarcode");
            this.btnScanBarcode.Name = "btnScanBarcode";
            this.btnScanBarcode.Click += new System.EventHandler(this.btnScanBarcode_Click);
            // 
            // TagConfirmForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            resources.ApplyResources(this, "$this");
            this.Controls.Add(this.btnScanBarcode);
            this.Controls.Add(this.btnDispatchAll);
            this.Controls.Add(this.pnlDispatch);
            this.Controls.Add(this.btnScreen);
            this.Controls.Add(this.ScanButton);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnDispatch);
            this.Controls.Add(this.PwdReqChkBx);
            this.Controls.Add(this.TagCntLabel);
            this.Controls.Add(this.EPCListV);
            this.Controls.Add(this.lsvTotal);
            this.Controls.Add(this.btnAll);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "TagConfirmForm";
            this.Load += new System.EventHandler(this.OnTagRdFormLoad);
            this.Closed += new System.EventHandler(this.OnTagRdFormClosed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnTagRdFormClosing);
            this.pnlDispatch.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label TagCntLabel;
        private System.Windows.Forms.ListView EPCListV;
        private System.Windows.Forms.ColumnHeader EPCColHdr;
        private System.Windows.Forms.Button ScanButton;
        private System.Windows.Forms.CheckBox PwdReqChkBx;
        private System.Windows.Forms.Button btnDispatch;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.ListView lsvTotal;
        private System.Windows.Forms.ColumnHeader colLotNO;
        private System.Windows.Forms.ColumnHeader colTotal;
        private System.Windows.Forms.Button btnScreen;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Panel pnlDispatch;
        private System.Windows.Forms.ListView lstAsset;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader AssetName;
        private System.Windows.Forms.ColumnHeader colDSLotNO;
        private System.Windows.Forms.ColumnHeader colDSTagID;
        private System.Windows.Forms.Label lblPage;
        private System.Windows.Forms.ComboBox cmbPage;
        private System.Windows.Forms.Button btnDispatchAll;
        private System.Windows.Forms.Button btnScanBarcode;
    }
}