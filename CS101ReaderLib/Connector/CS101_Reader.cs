﻿using System;
using System.Collections.Generic;

using System.Text;
using HHDeviceInterface.RFIDSp;
using ReaderTypes;
using OnRamp;
using ClsLibBKLogs;



namespace CS101ReaderLib
{
    public class RFID_Reader : HHDeviceInterface.RFIDSp.Reader
    {

        #region Property
       // public static ushort macerr;
        public  RFID_VERSION DriverVer
        {
            get
            {
                return Rfid.st_RfidSpReq_Startup.LibraryVersion;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public  RFID_VERSION MacVer
        {
            get
            {
                return Rfid.st_RfidSpReq_MacGetVersion.version;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public override HRESULT_RFID LastErrCode
        {
            get
            {
                return RFIDRdr.GetInstance().LastErrCode;
            }
        }
        public override ushort LastMacErrCode
        {
            get
            {
                return RFIDRdr.GetInstance().LastMacErrCode;
            }
        }

        #endregion



        #region  Events
        public override void RegisterDscvrTagEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs = handler;          
            RFIDRdr.GetInstance().DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(CS101_Reader_DscvrTagEvent);
        }
        void CS101_Reader_DscvrTagEvent(object sender, DscvrTagEventArgs e)
        {
            try
            {
                if (GetTagEventArgs != null)
                    GetTagEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterDscvrTagEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs  -= handler;
            RFIDRdr.GetInstance().DscvrTagEvent -= CS101_Reader_DscvrTagEvent;
        }

        public override void RegisterInventoryOpStEvent(EventHandler<InvtryOpEventArgs> handler)
        {
            this.InventoryOpEventArgs = handler;
            RFIDRdr.GetInstance().InvtryOpStEvent += new EventHandler<InvtryOpEventArgs>(CS101_Reader_InvtryOpStEvent);
        }
        void CS101_Reader_InvtryOpStEvent(object sender, InvtryOpEventArgs e)
        {
            try
            {
                if (InventoryOpEventArgs != null)
                    InventoryOpEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterInventoryOpStEvent(EventHandler<InvtryOpEventArgs> handler)
        {
            this.InventoryOpEventArgs -= handler;
            RFIDRdr.GetInstance().InvtryOpStEvent -= CS101_Reader_InvtryOpStEvent;
        }

        public override void RegisterLckOpStEvent(EventHandler<LckOpEventArgs> handler)
        {
            this.LockOpEventArgs = handler;
            RFIDRdr.GetInstance().LckOpStEvent += new EventHandler<LckOpEventArgs>(CS101_Reader_LckOpStEvent);
        }
        void CS101_Reader_LckOpStEvent(object sender, LckOpEventArgs e)
        {
            try
            {
                if (LockOpEventArgs  != null)
                    LockOpEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterLckOpStEvent(EventHandler<LckOpEventArgs> handler)
        {
            this.LockOpEventArgs -= handler;
            RFIDRdr.GetInstance().LckOpStEvent -= CS101_Reader_LckOpStEvent;
        }

        public override void RegisterMemBnkRdEvent(EventHandler<MemBnkRdEventArgs> handler)
        {
            this.MemberBankReadEvenArgs = handler;
            RFIDRdr.GetInstance().MemBnkRdEvent += new EventHandler<MemBnkRdEventArgs>(CS101_Reader_MemBnkRdEvent);
        }
        void CS101_Reader_MemBnkRdEvent(object sender, MemBnkRdEventArgs e)
        {
            try
            {
                if (MemberBankReadEvenArgs != null)
                    MemberBankReadEvenArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterMemBnkRdEvent(EventHandler<MemBnkRdEventArgs> handler)
        {
            this.MemberBankReadEvenArgs -= handler;
            RFIDRdr.GetInstance().MemBnkRdEvent -= CS101_Reader_MemBnkRdEvent;
        }

        public override void RegisterMemBnkWrEvent(EventHandler<MemBnkWrEventArgs> handler)
        {
            this.MemberBankWriteEventArgs = handler;
            RFIDRdr.GetInstance().MemBnkWrEvent += new EventHandler<MemBnkWrEventArgs>(CS101_Reader_MemBnkWrEvent);
        }
        void CS101_Reader_MemBnkWrEvent(object sender, MemBnkWrEventArgs e)
        {
            try
            {
                if (MemberBankWriteEventArgs  != null)
                    MemberBankWriteEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterMemBnkWrEvent(EventHandler<MemBnkWrEventArgs> handler)
        {
            this.MemberBankWriteEventArgs -= handler;
            RFIDRdr.GetInstance().MemBnkWrEvent -= CS101_Reader_MemBnkWrEvent;
        }

        public override void RegisterRdOpStEvent(EventHandler<RdOpEventArgs> handler)
        {
            this.ReadOpEventArgs = handler;
            RFIDRdr.GetInstance().RdOpStEvent += new EventHandler<RdOpEventArgs>(CS101_Reader_RdOpStEvent);
        }
        void CS101_Reader_RdOpStEvent(object sender, RdOpEventArgs e)
        {
            try
            {
                if (ReadOpEventArgs != null)
                    ReadOpEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterRdOpStEvent(EventHandler<RdOpEventArgs> handler)
        {
            this.ReadOpEventArgs -= handler;
            RFIDRdr.GetInstance().RdOpStEvent -= CS101_Reader_RdOpStEvent;
        }

        public override void RegisterTagPermSetEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs = handler;
            RFIDRdr.GetInstance().TagPermSetEvent += new EventHandler<DscvrTagEventArgs>(CS101_Reader_TagPermSetEvent);
        }
        void CS101_Reader_TagPermSetEvent(object sender, DscvrTagEventArgs e)
        {
            try
            {
                if (GetTagEventArgs != null)
                    GetTagEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterTagPermSetEvent(EventHandler<DscvrTagEventArgs> handler)
        {
            this.GetTagEventArgs -= handler;
            RFIDRdr.GetInstance().TagPermSetEvent -= CS101_Reader_TagPermSetEvent;
        }

        public override void RegisterTagRateEvent(EventHandler<TagRateEventArgs> handler)
        {
            this.TagRateEventArgs = handler;
            RFIDRdr.GetInstance().TagRateEvent += new EventHandler<TagRateEventArgs>(CS101_Reader_TagRateEvent);
        }
        void CS101_Reader_TagRateEvent(object sender, TagRateEventArgs e)
        {
            try
            {
                if (TagRateEventArgs != null)
                    TagRateEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterTagRateEvent(EventHandler<TagRateEventArgs> handler)
        {
            this.TagRateEventArgs -= handler;
            RFIDRdr.GetInstance().TagRateEvent -= CS101_Reader_TagRateEvent;
        }

        public override void RegisterTagRssiEvent(EventHandler<TagRssiEventArgs> handler)
        {
            this.TagRessiEventArgs = handler;
            RFIDRdr.GetInstance().TagRssiEvent += new EventHandler<TagRssiEventArgs>(CS101_Reader_TagRssiEvent);
        }
        void CS101_Reader_TagRssiEvent(object sender, TagRssiEventArgs e)
        {
            try
            {
                if (TagRessiEventArgs  != null)
                    TagRessiEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterTagRssiEvent(EventHandler<TagRssiEventArgs> handler)
        {
            this.TagRessiEventArgs -= handler;
            RFIDRdr.GetInstance().TagRssiEvent -= CS101_Reader_TagRssiEvent;
        }

        public override void RegisterTempUpdateEvt(EventHandler<TempUpdateEventArgs> handler)
        {
            this.TempUpdateEventArgs = handler;
            RFIDRdr.GetInstance().TempUpdateEvt += new EventHandler<TempUpdateEventArgs>(CS101_Reader_TempUpdateEvt);
        }
        void CS101_Reader_TempUpdateEvt(object sender, TempUpdateEventArgs e)
        {
            try
            {
                if (TempUpdateEventArgs  != null)
                    TempUpdateEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterTempUpdateEvt(EventHandler<TempUpdateEventArgs> handler)
        {
            this.TempUpdateEventArgs -= handler;
            RFIDRdr.GetInstance().TempUpdateEvt -= CS101_Reader_TempUpdateEvt;
        }

        public override void RegisterWrOpStEvent(EventHandler<WrOpEventArgs> handler)
        {
            this.WriteOpEventArgs = handler;
            RFIDRdr.GetInstance().WrOpStEvent += new EventHandler<WrOpEventArgs>(CS101_Reader_WrOpStEvent);
        }
        void CS101_Reader_WrOpStEvent(object sender, WrOpEventArgs e)
        {
            try
            {
                if (WriteOpEventArgs  != null)
                    WriteOpEventArgs(sender, e);
            }
            catch
            { }
        }
        public override void UnregisterWrOpStEvent(EventHandler<WrOpEventArgs> handler)
        {
            this.WriteOpEventArgs -= handler;
            RFIDRdr.GetInstance().WrOpStEvent -= CS101_Reader_WrOpStEvent;
        }

       

       
     
      
      
      
       
        public override void RegisterRadioStatusNotificationEvent(RadioStatusNotify handler)
        {
            this.RadioStatusNotification = handler;
        }
        public override void UnregisterRadioStatusNotificationEvent(RadioStatusNotify handler)
        {
            this.RadioStatusNotification -= handler;
        }
       
     
        #endregion

        #region Tag Inventory Request

        /// <summary>
        /// Method for start the tag range
        /// </summary>
        /// <param name="session"></param>
        /// <param name="qSize"></param>
        /// <param name="qAlgo"></param>
        /// <returns></returns>
        public override bool TagRangeStart()
        {
            bool result = false;
            try
            {
                UserPref Pref = UserPref.GetInstance();
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagRangeStart(Pref.InvtrySession, Pref.QSize, Pref.QAlgo);
            }
            catch (Exception ex)
            {
                Logger.LogError("TagRangeStart : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="qSize"></param>
        /// <param name="epcBankMask"></param>
        /// <param name="maskOffset"></param>
        /// <returns></returns>
        public override bool TagInventoryStart(EPCEvent evt)
        {
            return false;
        }
        public override bool TagInventoryStart(int option)
        {
            bool result = false;
            try
            {
                UserPref Pref = UserPref.GetInstance();
                RFIDRdr rdr = RFIDRdr.GetInstance();
                Byte[] Mask; uint MaskOffset;

                switch (option)
                {
                    case 4:
                        {

                            Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                            result = rdr.TagInvtryStart(Pref.InvtrySession, Pref.QSize, Pref.QAlgo, Mask, MaskOffset);
                        }
                        break;
                    case 5:
                        {
                            Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                            result = rdr.TagInvtryStart(Pref.InvtrySession, Pref.QSize, Pref.QAlgo, Mask, MaskOffset);
                        }
                        break;
                }


            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryStart : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="search1Only"></param>
        /// <param name="epcBnkMask"></param>
        /// <param name="maskOffset"></param>
        /// <returns></returns>
        public override bool TagInvtryRdRateStart(RFID_18K6C_INVENTORY_SESSION session, bool search1Only, byte[] epcBnkMask, uint maskOffset)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.TagRateEvent += TagRateEvent;
                //rdr.InvtryOpStEvent += InventoryOpStEvent;
                result = rdr.TagInvtryRdRateStart(session, search1Only, epcBnkMask, maskOffset);
                //if (!result)
                //{
                //    rdr.TagRateEvent -= TagRateEvent;
                //    rdr.InvtryOpStEvent -= InventoryOpStEvent;
                //}
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInvtryRdRateStart : " + ex.Message);
            }
            return result;
        }

        public override bool TagInventoryRdRateStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagInvtryRdRateStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryRdRateStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="session"></param>
        /// <param name="qSize"></param>
        /// <param name="epcBankMask"></param>
        /// <param name="maskOffset"></param>
        /// <returns></returns>
        public override bool TagInventoryRssiStart(RFID_18K6C_INVENTORY_SESSION session, bool search1Only, byte[] epcBnkMask, uint maskOffset, bool filterRes)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.TagRssiEvent += TagRssiEvent;
                //rdr.InvtryOpStEvent += InventoryOpStEvent;
                result = rdr.TagInvtryRssiStart(session, search1Only, epcBnkMask, maskOffset, filterRes);
                //if (!result)
                //{
                //    rdr.TagRssiEvent -= TagRssiEvent;
                //    rdr.InvtryOpStEvent -= InventoryOpStEvent;
                //}
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryRssiStart : " + ex.Message);
            }
            return result;
        }

        public override bool TagInventoryFirstStart()
        {
            bool result = false;
            try
            {
                UserPref Pref = UserPref.GetInstance();
                Byte[] BnkMask; uint WdOffset;
                Pref.GetEPCBnkSelMask(out BnkMask, out WdOffset);
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagInvtryFirstStart(Pref.InvtrySession, 1, BnkMask, WdOffset);
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryFirstStart : " + ex.Message);
            }
            return result;
        }
        public override bool TagInventoryOnceStart(bool perfSelCmd)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagInvtryOnceStart(perfSelCmd);
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryOnceStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagInvtryRssiStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.TagRssiEvent -= TagRssiEvent;
                //rdr.InvtryOpStEvent -= InventoryOpStEvent;
                result = rdr.TagInvtryRssiStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInvtryRssiStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryAbort()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagInvtryAbort();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryAbort : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// Method for stop Inventory tag
        /// </summary>
        /// <returns></returns>
        public override bool TagInventoryStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.InvtryOpStEvent -= InventoryOpStEvent;
                //rdr.DscvrTagEvent -= DscvrTagEvent;
                result = rdr.TagInvtryStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInventoryStop : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagReadStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.TagRateEvent -= TagRateEvent;
                //rdr.InvtryOpStEvent -= InventoryOpStEvent;
                result = rdr.TagReadStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagReadStop : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="perm"></param>
        /// <param name="PC"></param>
        /// <param name="EPC"></param>
        /// <param name="accPasswd"></param>
        /// <returns></returns>
        public override bool TagSetPermStart(ref RFID_18K6C_TAG_PERM perm, ushort PC, ref UINT96_T EPC, uint accPasswd)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.LckOpStEvent += LckOpStEvent;
                //rdr.TagPermSetEvent += TagPermSetEvent;
                result = rdr.TagSetPermStart(ref perm, PC, ref EPC, accPasswd);
                //if (!result)
                //{
                //    rdr.LckOpStEvent -= LckOpStEvent;
                //    rdr.TagPermSetEvent -= TagPermSetEvent;
                //}
            }
            catch (Exception ex)
            {
                Logger.LogError("GetMacError : " + ex.Message);
            }
            return result;
        }
        public override bool TagSetPermStart()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                // result = rdr.TagSetPermStart(ref perm, PC, ref EPC, accPasswd);
            }
            catch (Exception ex)
            {
                Logger.LogError("TagSetPermStart : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagSetPermStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                //rdr.LckOpStEvent -= LckOpStEvent;
                //rdr.TagPermSetEvent -= TagPermSetEvent;
                result = rdr.TagSetPermStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagSetPermStop : " + ex.Message);
            }
            return result;
        }

        //public uint AccessPassword { get; set; }
        //public uint NumberTags { get; set; }
        //public byte[] LastWrittenEPC { get; set; }
        //public UINT96_T TagtEPC;
        //public UInt32 CurAccPasswd { get; set; }
        //public UInt32 ProfNum;
        //public UInt32 bandNum;
        //public UInt32 PwrLevRegVal;
        //public UInt32 RevPwrLevRegVal;
        //public UInt32 RevPwrThrshRegVal;
        //public UInt32 Size;
        //public int power;
        //public UInt32 PortNum;
        //public ushort amb, xcvr, pamp;
        //public int frequency;
        //public SoundVol Svol;
        //public RingTone RingingID;
        //public UInt32 lnkProfNum { get; set; }

       // public event ReqTagWrBnkData TagWriteBankData;
        //public event ReqTagRdBnkInfo TagReadBankData;
        //public event CustomFreqGetNotify CustomerFreqGetNotification;
        //public event AntPortCfgGetOneNotify AntPotCfgGetonNotification;
        //public event AntPortCfgSetOneNotify AntPotCfgSetonNotification;
        //public event RadioStatusNotify RadioStatusNotification;
        //public event LnkProfInfoGetNotify LnkProfInfoGetNotification;
        //public event LnkProfNumSetNotify LnkProfNumoSetNotification;
        //// public event CustomFreqSetNotify CustomFreqSetNotification;
        //public event HealthCheckStatusNotify HealthCheckStatusNotification;
        //public event ThrshTempGetNotify ThrshTempGetNotification;

        public override void RegisterTagWriteBankDataEvent(ReqTagWrBnkData handler)
        {
            this.TagWriteBankData = handler;
            //RFIDRdr.GetInstance()
        }

        public override void UnregisterTagWriteBankDataEvent(ReqTagWrBnkData handler)
        {
            this.TagWriteBankData -= handler;
        }

        private  bool TagWrBnkDataReqCb(out MemoryBanks4Op bnk, out ushort wdOffSet,
           out String dataStr)
        {
            bool result = false;
            bnk = MemoryBanks4Op.None;
            wdOffSet = 0;
            dataStr = null;
            
            if (TagWriteBankData != null)
            {
                try
                {
                    result = TagWriteBankData(out bnk, out wdOffSet, out dataStr);
                }
                catch (Exception ex)
                {
                    Logger.LogError("TagWrBnkDataReqCb : " + ex.Message);
                }
            }
            return result;
        }

        public override bool TagWriteStart(int Option)
        {
            bool result = false;
            UserPref Pref = UserPref.GetInstance();
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                switch (Option)
                {
                    case 2:
                        result = rdr.TagWriteStart(TagWrBnkDataReqCb, AccessPassword);
                        break;
                    case 3:
                        result = rdr.TagWriteStart(TagWrBnkDataReqCb, (ushort)Pref.QSize, AccessPassword);
                        break;
                    case 4:
                        //UserPref Pref = UserPref.GetInstance();
                        result = rdr.TagWriteStart(Pref.QSize, ref TagtEPC, TagWrBnkDataReqCb, CurAccPasswd);
                        break;
                    case 5:
                        result = rdr.TagWriteStart(null, 0, TagWrBnkDataReqCb, (ushort)UserPref.Population2QSize((int)NumberTags * 2), AccessPassword);
                        break;
                    case 6:
                        result = rdr.TagWriteStart(LastWrittenEPC, 0, TagWrBnkDataReqCb, (ushort)UserPref.Population2QSize((int)NumberTags * 2), AccessPassword);
                        break;
                }

            }
            catch (Exception ex)
            {
                Logger.LogError("TagWriteStart : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagInvtryClr()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagInvtryClr();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagInvtryClr : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool TagWriteStop()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagWriteStop();
            }
            catch (Exception ex)
            {
                Logger.LogError("TagWriteStop : " + ex.Message);
            }
            return result;
        }

        public override void RegisterTagReadBankDataEvent(ReqTagRdBnkInfo handler)
        {
            this.TagReadBankData = handler;
        }
        public override void UnregisterTagReadBankDataEvent(ReqTagRdBnkInfo handler)
        {
            this.TagReadBankData -= handler;
        }

        private bool TgtRdBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        {
            bnk2Rd = MemoryBanks4Op.None;
            wdOffset = 0;
            wdCnt = 0;

            bool result = false;

            if (TagReadBankData != null)
            {
                try
                {
                    result = TagReadBankData(out bnk2Rd, out   wdOffset, out   wdCnt);
                }
                catch (Exception ex)
                {
                    Logger.LogError("TgtRdBnkReqCb : " + ex.Message);
                }
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="qSize"></param>
        /// <param name="epc"></param>
        /// <param name="cb"></param>
        /// <param name="accPwd"></param>
        /// <returns></returns>
        public override bool TagReadBanksStart()
        {
            bool result = false;
            try
            {
                UserPref Pref = UserPref.GetInstance();
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.TagReadBanksStart(Pref.QSize, ref TagtEPC, TgtRdBnkReqCb, AccessPassword);
            }
            catch (Exception ex)
            {
                Logger.LogError("TagReadBanksStart : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="enable"></param>
        /// <returns></returns>
        public override bool SetRepeatedTagObsrvMode(bool enable)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.SetRepeatedTagObsrvMode(enable);
            }
            catch (Exception ex)
            {
                Logger.LogError("SetRepeatedTagObsrvMode : " + ex.Message);
            }
            return result;
        }

        public override void RegistereventRespDataModeEvent(RespDatModeSetNotify handler)
        {
            this.eventRespDataMode = handler;
        }
        public override void UnregistereventRespDataModeEvent(RespDatModeSetNotify handler)
        {
            this.eventRespDataMode -= handler;
        }

        private void RespDataModeCb(bool succ,string errmsg)
        {
            eventRespDataMode(succ, errmsg);
        }

        public override bool SetResponseDataMode(int Option)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                switch (Option)
                {
                    case 1:
                        result = rdr.SetResponseDataMode(RFID_RESPONSE_MODE.RFID_RESPONSE_MODE_COMPACT);
                        break;
                    case 2:
                        if (eventRespDataMode != null)
                            result = rdr.SetResponseDataMode(RFID_RESPONSE_MODE.RFID_RESPONSE_MODE_NORMAL, RespDataModeCb);
                        else
                            result = rdr.SetResponseDataMode(RFID_RESPONSE_MODE.RFID_RESPONSE_MODE_COMPACT);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("SetResponseDataMode : " + ex.Message);
            }
            return result;
        }
        #endregion


        #region Mac Error Routines
        /// <summary>
        /// 
        /// </summary>
        /// <param name="macerr"></param>
        /// <returns></returns>
        public override bool GetMacError()
        {
            bool result = false;
            macerr = 0;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.GetMacError(out macerr);
            }
            catch (Exception ex)
            {
                Logger.LogError("GetMacError : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="macErrCode"></param>
        /// <returns></returns>
        public override bool MacErrorIsOverheat()
        {
            bool result = false;
            try
            {
                result = RFIDRdr.MacErrorIsOverheat(macerr);
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsOverheat : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="macErrCode"></param>
        /// <returns></returns>
        public override bool MacErrorIsFatal()
        {
            bool result = false;
            try
            {
                result = RFIDRdr.MacErrorIsFatal(macerr);
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsFatal : " + ex.Message);
            }
            return result;
        }
        public override bool MacErrorIsNegligible()
        {
            bool result = false;
            try
            {
                result = RFIDRdr.MacErrorIsNegligible(macerr);
            }
            catch (Exception ex)
            {
                Logger.LogError("MacErrorIsNegligible : " + ex.Message);
            }
            return result;
        }
        public override HRESULT_RFID f_CFlow_RfidDev_RadioGetConfigurationParameter()
        {

            return RFIDRdr.f_CFlow_RfidDev_RadioGetConfigurationParameter();

        }
        public override bool ClearMacError()
        {
            bool result = false;
            try
            {
                RFIDRdr Rdr = RFIDRdr.GetInstance();
                result = Rdr.ClearMacError();
            }
            catch (Exception ex)
            {
                Logger.LogError("ClearMacError : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Antenna Configuration

        private void AntPotCfgGetOneNotificationCb(bool succ, uint portNum, ref RFID_ANTENNA_PORT_CONFIG confg, string msg)
        {
            if(AntPotCfgGetOneNotification!=null)
            AntPotCfgGetOneNotification(succ, portNum, ref   confg, msg);
        }
        public override void RegisterAntPotCfgGetOneNotificationEvent(AntPortCfgGetOneNotify handler)
        {
            this.AntPotCfgGetOneNotification = handler;
        }
        public override void UnregisterAntPotCfgGetOneNotificationEvent(AntPortCfgGetOneNotify handler)
        {
            this.AntPotCfgGetOneNotification -= handler;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="portNum"></param>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool AntPortCfgGetOne()
        {
            bool result = false;
            try
            {

                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.AntPortCfgGetOne(0, AntPotCfgGetOneNotificationCb);
                 
            }
            catch (Exception ex)
            {
                Logger.LogError("AntPortCfgGetOne : " + ex.Message);
            }
            return result;
        }

        private void AntPotCfgSetOneNotificationCb(bool succ, uint PortNum, string errMsg)
        {
            if (AntPotCfgSetOneNotification != null)
            AntPotCfgSetOneNotification(succ, PortNum, errMsg);
        }
        public override void RegisterAntPotCfgSetOneNotificationEvent(AntPortCfgSetOneNotify handler)
        {
            this.AntPotCfgSetOneNotification = handler;
        }
        public override void UnregisterAntPotCfgSetOneNotificationEvent(AntPortCfgSetOneNotify handler)
        {
            this.AntPotCfgSetOneNotification -= handler;
        }            
        /// <summary>
        /// 
        /// </summary>
        /// <param name="portNum"></param>
        /// <param name="pwr"></param>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool AntPortCfgSetPwr()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.AntPortCfgSetPwr(PortNum, power, AntPotCfgSetOneNotificationCb); 
            }
            catch (Exception ex)
            {
                Logger.LogError("AntPortCfgSetPwr : " + ex.Message);
            }
            return result;
        }

        public override bool AntPortCfgSetOne(uint portNum, ref RFID_ANTENNA_PORT_CONFIG cfg, AntPortCfgSetOneNotify notify)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.AntPortCfgSetOne(portNum, ref cfg, notify);
            }
            catch (Exception ex)
            {
                Logger.LogError("AntPortCfgSetOne : " + ex.Message);
            }
            return result;
        }
        #endregion

        #region Sound/vol
        /// <summary>
        /// 
        /// </summary>
        /// <param name="vol"></param>
        public override void BuzzerBeep(int option)
        {
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                switch (option)
                {
                    case 1:
                        rdr.BuzzerBeep(Svol);
                        break;
                    case 2:
                        rdr.BuzzerBeep(frequency, Svol);
                        break;
                }
            }
            catch (Exception ex)
            {
                Logger.LogError("BuzzerBeep : " + ex.Message);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="toneID"></param>
        /// <param name="vol"></param>
        public override void MelodyRing()
        {
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                rdr.MelodyRing(RingingID, Svol);
            }
            catch (Exception ex)
            {
                Logger.LogError("MelodyRing : " + ex.Message);
            }
        }
        #endregion

        #region Custom Freq Band
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bandNum"></param>
        /// <returns></returns>
        public override bool CustomFreqBandNumGet()
        {
            // UInt32 bandNum;
            bool result = false;
            // bandNum = 0;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqBandNumGet(out bandNum);
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqBandNumGet : " + ex.Message);
            }
            return result;
        }
        public override bool CustomFreqBandNumSet()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqBandNumSet(bandNum);
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqBandNumSet : " + ex.Message);
            }
            return result;
        }
        private  void CustomerFreqGetNotificationCb(bool succ, CustomFreqGrp freqGrp, int chn, bool enLBT)
        {
            CustomFreqGetNotification(succ, freqGrp, chn, enLBT);
        }
        public override void RegisterCustomFreqGetNotificationEvent(CustomFreqGetNotify handler)
        {
            this.CustomFreqGetNotification = handler;
        }
        public override void UnregisterCustomFreqGetNotificationEvent(CustomFreqGetNotify handler)
        {
            this.CustomFreqGetNotification -= handler;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool CustomFreqGet()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqGet(CustomerFreqGetNotificationCb);
                
                
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqGet : " + ex.Message);
            }
            return result;
        }
        #endregion

        private void ThrshTempGetNotificationCb(ushort amb, ushort xcvr, ushort pwrAmp, ushort paDelta)
        {
            if (ThrshTempGetNotification != null)
            this.ThrshTempGetNotification(amb, xcvr, pwrAmp, paDelta);
        }
        public override void RegisterThrshTempGetNotificationEvent(ThrshTempGetNotify handler)
        {
            this.ThrshTempGetNotification = handler;
        }
        public override void UnregisterThrshTempGetNotificationEvent(ThrshTempGetNotify handler)
        {
            this.ThrshTempGetNotification -= handler;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool GetTempThreshold()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.GetTempThreshold(ThrshTempGetNotificationCb);
                
            }
            catch (Exception ex)
            {
                Logger.LogError("GetTempThreshold : " + ex.Message);
            }
            return result;
        }

        public override bool SetTempThreshold(ushort xcvrLmt)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.SetTempThreshold(xcvrLmt);
            }
            catch (Exception ex)
            {
                Logger.LogError("SetTempThreshold : " + ex.Message);
            }
            return result;
        }
        public override bool SetTempThreshold(ushort xcvrLmt, ushort ambLmt, ushort paLmt, ushort deltaLmt)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.SetTempThreshold(xcvrLmt, ambLmt, paLmt, deltaLmt);
            }
            catch (Exception ex)
            {
                Logger.LogError("SetTempThreshold : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="amb"></param>
        /// <param name="xcvr"></param>
        /// <param name="pamp"></param>
        /// <returns></returns>
        public override bool GetCurrTemp()
        {
            //ushort amb, xcvr, pamp;
            bool result = false;
            //amb = 0;
            //xcvr = 0;
            //pamp = 0;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.GetCurrTemp(out amb, out xcvr, out pamp);
            }
            catch (Exception ex)
            {
                Logger.LogError("GetCurrTemp : " + ex.Message);
            }
            return result;
        }


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool CancelRunningOp()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CancelRunningOp();
            }
            catch (Exception ex)
            {
                Logger.LogError("CancelRunningOp : " + ex.Message);
            }
            return result;
        }
        private void HealthCheckStatusNotificationCb(HealthChkOpStatus status, string msg)
        {
            if (HealthCheckStatusNotification != null)
            this.HealthCheckStatusNotification(status, msg);
        }
        public override void RegisterHealthCheckStatusNotificationEvent(HealthCheckStatusNotify handler)
        {
            this.HealthCheckStatusNotification = handler;
        }
        public override void UnregisterHealthCheckStatusNotificationEvent(HealthCheckStatusNotify handler)
        {
            this.HealthCheckStatusNotification -= handler;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool PerformHealthCheck()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.PerformHealthCheck(HealthCheckStatusNotificationCb);
            }
            catch (Exception ex)
            {
                Logger.LogError("PerformHealthCheck : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public override bool StopHealthCheck()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.StopHealthCheck();
            }
            catch (Exception ex)
            {
                Logger.LogError("StopHealthCheck : " + ex.Message);
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="freqSet"></param>
        /// <param name="chn"></param>
        /// <param name="enLBT"></param>
        /// <param name="notify"></param>
        /// <returns></returns>
        public override bool CustomFreqSet(CustomFreqGrp freqSet, int chn, bool enLBT, CustomFreqSetNotify notify)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqSet(freqSet, chn, enLBT, notify);
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqSet : " + ex.Message);
            }
            return result;
        }
        public override bool CustomFreqSet(CustomFreqGrp freqSet, bool enLBT, CustomFreqSetNotify notify)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqSet(freqSet, enLBT, notify);
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqSet : " + ex.Message);
            }
            return result;
        }
        public override bool CustomFreqSet(CustomFreqGrp freqSet, CustomFreqSetNotify notify)
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CustomFreqSet(freqSet, notify);
            }
            catch (Exception ex)
            {
                Logger.LogError("CustomFreqSet : " + ex.Message);
            }
            return result;
        }

        public override bool CarrierWaveOn()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CarrierWaveOn();
            }
            catch (Exception ex)
            {
                Logger.LogError("CarrierWaveOn : " + ex.Message);
            }
            return result;
        }
        public override bool CarrierWaveOff()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.CarrierWaveOff();
            }
            catch (Exception ex)
            {
                Logger.LogError("CarrierWaveOff : " + ex.Message);
            }
            return result;
        }

        public override bool GetPwrLvlRegVals()
        {
            bool result = false;
            PwrLevRegVal = 0;
            RevPwrLevRegVal = 0;
            RevPwrThrshRegVal = 0;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.GetPwrLvlRegVals(out PwrLevRegVal, out RevPwrLevRegVal, out RevPwrThrshRegVal);
            }
            catch (Exception ex)
            {
                Logger.LogError("GetPwrLvlRegVals : " + ex.Message);
            }
            return result;
        }

        public override void Dispose()
        {
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                rdr.Dispose();
            }
            catch (Exception ex)
            {
                Logger.LogError("Dispose : " + ex.Message);
            }
        }

        #region Set/Get LinkProfNum

        public void LnkProfNumoSetNotificationCb(bool succ, string errMsg)
        {
            LnkProfNumoSetNotification(succ, errMsg);
        }
        public override void RegisterLnkProfNumoSetNotificationEvent(LnkProfNumSetNotify handler)
        {
            this.LnkProfNumoSetNotification = handler;
        }
        public override void UnregisterLnkProfNumoSetNotificationEvent(LnkProfNumSetNotify handler)
        {
            this.LnkProfNumoSetNotification -= handler;
        }
        public override bool LinkProfNumSet()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.LinkProfNumSet(ProfNum, LnkProfNumoSetNotificationCb);
                
            }
            catch (Exception ex)
            {
                Logger.LogError("LinkProfNumSet : " + ex.Message);
            }
            return result;
        }
        
        void LnkProfInfoGetNotificationCb(bool succ,ref RFID_RADIO_LINK_PROFILE InkPrf,string errMsg)
        {
            LnkProfInfoGetNotification(succ, ref InkPrf, errMsg);
        }
        public override void RegisterLnkProfInfoGetNotificationEvent(LnkProfInfoGetNotify handler)
        {
            this.LnkProfInfoGetNotification = handler;
        }
        public override void UnregisterLnkProfInfoGetNotificationEvent(LnkProfInfoGetNotify handler)
        {
            this.LnkProfInfoGetNotification -= handler;
        }

        public override bool LinkProfInfoGet()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.LinkProfInfoGet(ProfNum, LnkProfInfoGetNotificationCb);
                
            }
            catch (Exception ex)
            {
                Logger.LogError("LinkProfInfoGet : " + ex.Message);
            }
            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="profNum"></param>
        /// <returns></returns>
        public override bool LinkProfNumGet()
        {
            bool result = false;
            ProfNum = 0;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.LinkProfNumGet(out ProfNum);
            }
            catch (Exception ex)
            {
                Logger.LogError("LinkProfNumGet : " + ex.Message);
            }
            return result;
        }
        #endregion


        #region RF

        public override bool RFGenerateRandomData()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RFGenerateRandomData(Size);
            }
            catch (Exception ex)
            {
                Logger.LogError("RFGenerateRandomData : " + ex.Message);
            }
            return result;
        }

        public override bool RFGenerateRandomDataSetup()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RFGenerateRandomDataSetup();
            }
            catch (Exception ex)
            {
                Logger.LogError("RFGenerateRandomDataSetup : " + ex.Message);
            }
            return result;
        }
        #endregion


        #region Radio
        public override bool RadioReady()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RadioReady();
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioReady : " + ex.Message);
            }
            return result;
        }

        private void RadioOpenResultNotifyCb(RadioOpRes res, RadioStatus status, String msg)
        {
            if(RadioStatusNotification!=null)
                RadioStatusNotification(res, status, msg);
        }

        public override bool RadioOpen()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RadioOpen(RadioOpenResultNotifyCb);
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioOpen : " + ex.Message);
            }
            return result;
        }
        public override bool RadioReOpen()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RadioReOpen(RadioStatusNotification);
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioReOpen : " + ex.Message);
            }
            return result;
        }
        public override bool RadioClose()
        {
            bool result = false;
            try
            {
                RFIDRdr rdr = RFIDRdr.GetInstance();
                result = rdr.RadioClose(RadioStatusNotification);
            }
            catch (Exception ex)
            {
                Logger.LogError("RadioClose : " + ex.Message);
            }
            return result;
        }
        # endregion
    }
}
