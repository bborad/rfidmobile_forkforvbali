﻿using System;
using System.Collections.Generic;

using System.Text;
using SP = ClslibPosSp.PosSp;
using ClslibPosSp;

namespace CS101ReaderLib
{
    public class PosSp
    {
        public const int BUZZER_ALWAYS_ON = -1; 

        private const int RadioPwrPin = 3;
        private const int ScnrLEDPin = 1;

        private static bool disposed = false;

        static PosSp()
        {
            // Initialize Native libraries
            PosSp.f_PosSp_Initialize();

            // Turn on Radio-related GPIO
            if (!f_PosSp_GpioIni())
                throw new ApplicationException("PosSp_GpioIni() failed, Abort!");
            // Setup GPIO pins that will be used within this application
            f_PosSp_GpioSetIo(ScnrLEDPin);
            f_PosSp_GpioSetIo(RadioPwrPin);
        }


        public static void Dispose()
        {
            if (!disposed)
            {
                // Unload native library
                PosSp.f_PosSp_GpioUnini();
                PosSp.f_PosSp_Uninitialize();
                disposed = true;
            }
        }

        /// 

        public static int f_PosSp_Initialize()
        {
            return SP.f_PosSp_Initialize();
        }


        public static  int f_PosSp_Uninitialize()
        {
            return SP.f_PosSp_Uninitialize();
        }
        /// void f_PosSp_GetDeviceName(WCHAR* DeviceName);

        public static void f_PosSp_GetDeviceName(string DeviceName)
        {
            SP.f_PosSp_GetDeviceName(DeviceName);
        }//ref?
        /// LED
        /// GSLB101API_API BOOL LedSetOn(COLORREF Color);
       // [DllImport(B101DLL_Path, EntryPoint = "LedSetOn", CharSet = CharSet.Auto)]
        public static bool f_PosSp_LedSetOn(uint Color)
        {
           return SP.f_PosSp_LedSetOn(Color);
        }
        /// GSLB101API_API BOOL LedBlink(COLORREF colorRGB,WORD Period, WORD OnTime);
        
        public static bool f_PosSp_LedBlink(uint colorRGB, short Period, short OnTime)
        {
            return SP.f_PosSp_LedBlink( colorRGB,  Period,  OnTime);
        }
        /// GSLB101API_API void LedSetOff();

        public static  bool f_PosSp_LedSetOff()
        {
            return SP.f_PosSp_LedSetOff();
        }
        /// Buzzer Sound
        /// GSLB101API_API void ToneOn(WORD freq,WORD Duration,BUZZER_SOUND SoundLevel);

        public static void f_PosSp_ToneOn(short freq, short Duration, uint SoundLevel)
        {
            SP.f_PosSp_ToneOn( freq,  Duration,  SoundLevel);
        }
        /// GSLB101API_API void ToneOff();

        public static void f_PosSp_ToneOff()
        {
            SP.f_PosSp_ToneOff();
        }
        /// GSLB101API_API void MelodyPlay(int ToneID,WORD Duration,BUZZER_SOUND SoundLevel);

        public static  void f_PosSp_MelodyPlay(int ToneID, short Duration, uint SoundLevel)
        {
            SP.f_PosSp_MelodyPlay(ToneID, Duration, SoundLevel);
        }
        /// GSLB101API_API void MelodyStop();

        public static void f_PosSp_MelodyStop()
        {
            SP.f_PosSp_MelodyStop();
        }

        /// Vibrator GPIO
        /// void f_PosSp_VibrationOn(short OnPeriod,short OffPeriod, short Duration);
        /// Hw Not connected
        public static void f_PosSp_VibrationOn(short OnPeriod, short OffPeriod, short Duration)
        {
            SP.f_PosSp_VibrationOn( OnPeriod,  OffPeriod,  Duration);
        }
        /// void f_PosSp_VibrationStop();
        /// Hw Not connected
        public static void f_PosSp_VibrationStop()
        {
            SP.f_PosSp_VibrationStop();
        }
        /// WiFi
        /// int f_PosSp_GetWiFiState(); /// WIFISTATE

        public static int f_PosSp_GetWiFiState()
        {
            return SP.f_PosSp_GetWiFiState();
        }
        /// void f_PosSp_ForceScan();

        public static void f_PosSp_ForceScan()
        {
            SP.f_PosSp_ForceScan();
        }
        //////////////////////////////////////////////////////////////////////////
        /// Open the filehandle for the device

        public static bool f_PosSp_GpioIni()
        {
            return SP.f_PosSp_GpioIni();
        }
        /// Close the filehandle 
        public static bool f_PosSp_GpioUnini()
        {
            return SP.f_PosSp_GpioUnini();
        }
        /// Set the IO of Gpio 0--3    
        public static bool f_PosSp_GpioSetIo(int iGpio)
        {
            return SP.f_PosSp_GpioSetIo(iGpio);
        }
        /// Write a state to Gpio 0--3    
        //[DllImport(W_PosSpDLL_Path, EntryPoint = "f_PosSp_GpioWrite", CharSet = CharSet.Auto)]
        //[return: MarshalAs(UnmanagedType.Bool)]
        public static bool f_PosSp_GpioWrite(int iGpio, char iState)
        {
            return SP.f_PosSp_GpioWrite(iGpio, iState);
        }

        public static bool f_PosSp_GpioRead(int iGpio, ref char piState)
        {
            return SP.f_PosSp_GpioRead(iGpio,ref piState);
        }
        //////////////////////////////////////////////////////////////////////////

        public static bool RadioPwrOn()
        {
            return PosSp.f_PosSp_GpioWrite(RadioPwrPin, (char)0); // set GPIO3 to Logic-L
        }

        public static bool RadioPwrOff()
        {

            // Turn off Radio-related GPIO
            return PosSp.f_PosSp_GpioWrite(RadioPwrPin, (char)1); // set GPIO3 to Logic-H
        }

        public static bool ScnrLaserOn()
        {
            if (PosSp.f_PosSp_GpioWrite(ScnrLEDPin, (char)0)) // set GPIO1 to Active-L
            {
                //Datalog.LogStr(Thread.CurrentThread.ManagedThreadId + " ++++ Laser On +++++");
                //Thread.Sleep(10); // prevent Off-On sequence too close apart, render useless
                // Use busy loop instead of Sleep to avoid thread-switch (Serial Port)
                for (int i = 0; i < 2000000; i++) ;
                return true;
            }
            return false;
        }

        public static bool ScnrLaserOff()
        {
            if (PosSp.f_PosSp_GpioWrite(ScnrLEDPin, (char)1)) // set GPIO1 to Active-L
            {
                //Datalog.LogStr(Thread.CurrentThread.ManagedThreadId + " ----- Laser Off -----");
                //Thread.Sleep(10); // prevent Off-On sequence too close apart, render useless
                // Use busy loop instead of Sleep to avoid thread-switch (Serial Port)
                for (int i = 0; i < 2000000; i++) ;
                return true;
            }
            return false;
        }

    }
}
