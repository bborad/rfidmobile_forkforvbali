using System;
using System.Data;
using System.Data.SqlServerCe;

namespace DataSetFactory {

    partial class InvtryDataSet
    {
        /// <summary>
        /// This corresponds to schema defined in DataTable InvtryTbl
        /// </summary>
        public SqlCeCommand CreateTableCmd
        {
            get
            {
                SqlCeCommand Cmd = new SqlCeCommand(
                    @"CREATE TABLE InvtryTbl ( TagID  national character(28) PRIMARY KEY, Rssi float NOT NULL, Date datetime NOT NULL)");
                return Cmd;
            }
        }

        public SqlCeCommand SelectAllFldsCmd
        {
            get
            {
                SqlCeCommand Cmd = new SqlCeCommand(
                    @"SELECT * from InvtryTbl");
                return Cmd;
            }
        }

        private string lastErrMsg = string.Empty;

        public string LastErrMsg
        {
            get
            {
                return lastErrMsg;
            }
        }


        // default 3
        private double RssiSetSignificantFigures(float rssiVal)
        {
            double RssiDVal = 0.0d;
            try
            {
                String StrVal = rssiVal.ToString("F1"); // 1 decimal place
                RssiDVal = Double.Parse(StrVal);
            }
            catch (FormatException fe)
            {
                lastErrMsg = ("Invalid RSSI value.\n" + fe.Message);
            }
            catch (ArgumentException ae)
            {
                lastErrMsg = ("Invalid RSSI value.\n" + ae.Message);
            }

            return RssiDVal;
        }


        public bool AssignToDBRow(DataRow row, String TagID, float rssi, DateTime dateTime)
        {
            bool Succ = false;

            try
            {
                row[this.InvtryTbl.TagIDColumn.Ordinal] = TagID;
                Succ = AssignToDBRow(row, rssi, dateTime);
            }
             catch (ArgumentException)
            {
                lastErrMsg = "Number of columns fewer than expected";
            }
            catch (InvalidCastException)
            {
                lastErrMsg = "Column data type not the same expected";
            }
            catch (Exception ex)
            {
                lastErrMsg =  "Unexpected Exception (" + ex.GetBaseException().GetType().Name + ") caught:" + "\n"
                    + ex.GetBaseException().Message;
            }

            return Succ;
        }

        public bool AssignToDBRow(DataRow row, float rssi, DateTime dateTime)
        {
            bool Succ = false;

            try
            {
                Double d = RssiSetSignificantFigures(rssi);
                row[this.InvtryTbl.RssiColumn.Ordinal] = d;
                row[this.InvtryTbl.DateColumn.Ordinal] = dateTime;
                Succ = true;
            }
            catch (ArgumentException)
            {
                lastErrMsg = "Number of columns fewer than expected";
            }
            catch (InvalidCastException)
            {
                lastErrMsg = "Column data type not the same expected";
            }
            catch (Exception ex)
            {
                lastErrMsg =  "Unexpected Error (" + ex.GetBaseException().GetType().Name + ") caught:" + "\n"
                    + ex.GetBaseException().Message;
            }

            return Succ;
        }

        public bool SelectRowsByTagID(String TagID, out DataRow[] rows)
        {
            bool Succ = false;
            rows = null;
            try
            {
                rows = this.InvtryTbl.Select(
                    this.InvtryTbl.Columns[this.InvtryTbl.TagIDColumn.Ordinal].ColumnName + "="
                                     + "'" + TagID + "'");
                Succ = true;
            }
            catch (Exception ex)
            {
                lastErrMsg = "Unexpected Error (" + ex.GetBaseException().GetType().Name + ") caught:" + "\n"
                    + ex.GetBaseException().Message;
            }

            return Succ;
        }
    }
}
