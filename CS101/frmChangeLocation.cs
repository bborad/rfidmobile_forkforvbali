﻿using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmChangeLocation : Form
    {
        public frmChangeLocation()
        {
            InitializeComponent();
        }

        public ListViewItem lvItem;

        public string itemName,tagId,locName,locId;

        public bool locChanged;

        private void btnChangeLoc_Click(object sender, EventArgs e)
        {
            try
            {  
                Int32 checkLocationID;

                if (Convert.ToInt32(cboLoc.SelectedValue) == 0)
                {
                    MessageBox.Show("Please select the new location.");
                    return;
                }
                else if (Convert.ToInt32(cboLoc.SelectedValue) == Convert.ToInt32(locId))
                {
                    MessageBox.Show("Please select the different location.");
                    return;
                }
                else
                {
                    checkLocationID = Convert.ToInt32(cboLoc.SelectedValue);
                }
                               
                string strCsvTag = "'" + tagId + "'";
                int result = Assets.UpdateAssetLocation(strCsvTag, checkLocationID, 0);
                if (result > 0)
                {
                    MessageBox.Show("Location changed successfully.");
                    locChanged = true;
                }
            }
            catch (Exception ex)
            {
                Program.ShowError(ex.Message.ToString());
                Logger.LogError(ex.Message); 
            }
            this.Close();
        }

        private void frmChangeLocation_Load(object sender, EventArgs e)
        {
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();

            DataRow dr = dtList.NewRow();
            dr["ID_Location"] = 0;
            dr["Name"] = "Select Location";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "ID_Location";
            cboLoc.DisplayMember = "Name";
            cboLoc.DataSource = dtList;

            cboLoc.SelectedValue = 0;

            locChanged = false;

            txtItemName.Text = itemName;
           
            txtTagId.Text = tagId;
            txtLocName.Text = locName;

            if (tagId == "")
            {
                btnChangeLoc.Enabled = false;
            }
            else
            {
                btnChangeLoc.Enabled = true;
            }
             
        }  
        
    }
}