/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Inventory functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;

using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using System.Collections;
using ClsLibBKLogs;
using ClsReaderLib;using ClsReaderLib.Devices;
namespace OnRamp
{
    public partial class TagRdForm : Form
    {
        private String ItemTags = "-1";
        private String InTagList = "";
        private Assets SelectedAsset;
        List<Assets> lstItems;
        ArrayList InTagArray;
        Int32 SelectedLocationID = 0;

        private StringBuilder csvTagList;

        #region "sound and meolody variables"
        const int NumThresholdLvls = 8;
        //#if USE_WBRSSI
        //        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
        //            {
        //                0.0F, // within read range
        //                100.0F, // low
        //                100.0F, // 
        //                100.0F,
        //                110.0F, // medium
        //                110.0F,
        //                110.0F, // 
        //                120.0F, // high
        //            };
        //#else
        static float[] RssiLvlThresholds = new float[NumThresholdLvls]
            {
                0.0F, // within read range
                50.0F, // low
                60.0F, // 
                65.0F,
                70.0F, // medium
                75.0F,
                80.0F, // 
                90.0F, // high
            };
        //#endif

        static uint[] PulsePeriods = new uint[NumThresholdLvls]
            {
                1000,
                1000,
                1000,
                600,
                400,
                200,
                100,
                100,
            };

        static uint[] LedRadius = new uint[NumThresholdLvls]
            {
                20,
                20,
                20,
                30,
                40,
                50,
                60,
                60,
            };

        static uint[] BuzzerPitch = new uint[NumThresholdLvls]
            {
                200,
                200,
                200,
                400,
                800,
                1600,
                3200,
                3200,
            };
        static RingTone[] RingMelody = new RingTone[NumThresholdLvls]
            {
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T1,
                RingTone.T3,
                RingTone.T4,
                RingTone.T5,
                RingTone.T5,
            };
        #endregion

        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch

            //RFIDRdr rdr = RFIDRdr.GetInstance();
        
        }
        #endregion

        private TagAccListVHelper ListVHelper;
        private TagAccErrSummary errSummary;

        public TagRdForm()
        {
            InitializeComponent();

            InitLngLstButtonState();

            InitScanButtonState();

            InitRead1ButtonState();

            InitChkBxState();

            ListVHelper = new TagAccListVHelper(this.EPCListV);

            errSummary = new TagAccErrSummary();

            lstItems = new List<Assets>();

            EPCListV.BringToFront();

        }


        #region AddBarCOdeTag
        public void AddListViewItems(ListView.ListViewItemCollection lAr)
        {
            try
            {
                //RFID_18K6C_MEMORY_BANK en = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                //ushort crc = 0;
                //ushort pc = 0;
                //UINT96_T p = new UINT96_T();
                //string s = li.Text.PadRight(24).ToString().Replace("Z", "A");
                //p.ParseString(li.Text.PadLeft(24, '0').ToString().Replace("Z", "A"));
                //string data = "";
                //UInt16 iRSSI = 20;
                //MemBnkRdEventArgs mem = new MemBnkRdEventArgs(ref crc, ref pc, ref p, en, data, iRSSI);
                //MemBnkRdEvtHandler(this, mem);


                //lstAsset.Items.Clear();
                ListViewItem lstItem;
                ListViewItem.ListViewSubItem ls;
                Int16 iCount = Convert.ToInt16(lstAsset.Items.Count);

                Int32 iTag = 0;
                String TgNo;
                Assets Ast;

                foreach (ListViewItem li in lAr)
                {

                    // for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                    // {
                    iCount++;
                    iTag++;
                    lstItem = new ListViewItem(iCount.ToString());


                    //  TgNo = EPCListV.Items[iTag].SubItems[1].Text;
                    TgNo = li.SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');

                    Ast = new Assets(TgNo);
                    //Doc = new Documents(TgNo);

                    if (Ast.ServerKey != 0)
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.Name.ToString());
                        lstItem.SubItems.Add(ls);

                        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString(Ast.getLocationName());
                        lstItem.SubItems.Add(ls1);

                        ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                        ls2.Text = Convert.ToString(Ast.LotNo.Trim());
                        lstItem.SubItems.Add(ls2);
                    }
                    else
                    {
                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls);

                        ListViewItem.ListViewSubItem ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls1);

                        ListViewItem.ListViewSubItem ls2 = new ListViewItem.ListViewSubItem();
                        ls2.Text = Convert.ToString("NA");
                        lstItem.SubItems.Add(ls2);
                    }

                    ListViewItem.ListViewSubItem ls3 = new ListViewItem.ListViewSubItem();
                    ls3.Text = Convert.ToString(TgNo);
                    lstItem.SubItems.Add(ls3);

                    lstAsset.Items.Add(lstItem);
                    ////}

                }
                lstAsset.Refresh();

                lstAsset.Visible = true;
                EPCListV.Visible = false;
                ScanButton.Enabled = true;
                //btnScnBarcd.Enabled = true;
                btnSavePage.Enabled = true;
                btnClear.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                ScanButton.Enabled = true;
                //btnScnBarcd.Enabled = true;
            }
        }
        #endregion

        #region AddBarCOdeTag

        public ListView BCList;

        public bool barCodeScan = false;

        public void AddListViewItems()
        {
            try
            {
                if (BCList == null)
                {
                    return;
                }

                TagCntLabel.Text = BCList.Items.Count.ToString();

                // Int32 iTag = 0;
                String TgNo;
                //Assets Ast;

                if (BCList.Items.Count > 0)
                {
                    barCodeScan = true;
                    Program.RefreshStatusLabel(this, "Data Processing..");

                    lstAsset.Visible = true;

                    lstAsset.BringToFront();

                    cboLoc.Size = new Size(220, 23);

                    lblPage.Visible = true;
                    cmbPage.Visible = true;

                    btnClear.Visible = true;
                    btnSaveAll.Visible = true;

                    if (Login.OnLineMode)
                    {
                        btnSavePage.Visible = false;
                    }
                    else
                    {
                        btnSavePage.Visible = true;
                    }

                    ScanButton.Visible = false;
                    btnScnBarcd.Visible = false;

                    //recPerPage = 10;
                    totalPages = SetupPaging(BCList.Items.Count);

                    Application.DoEvents();

                    SetupPaging(BCList.Items.Count);

                    curPage = 1;

                    setScanTags();

                    FillDetailView();

                }
                else
                {
                    this.Text = "Inventory : Total Read - " + TagCntLabel.Text;
                }

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                ScanButton.Enabled = true;
                btnScnBarcd.Enabled = true;
            }
        }

        #endregion

        #region CheckBox Routines
        private void InitChkBxState()
        {
            // Set default 'Checked' status according to Availability
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Zero) != 0)
                B0ChkBx.Checked = true;
            else // not available, useless to try
                B0ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.One) != 0)
                B1ChkBx.Checked = true;
            else // not available, useless to try
                B1ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Two) != 0)
                B2ChkBx.Checked = true;
            else // not available, useless to try
                B2ChkBx.Enabled = false;
            if ((EPCTag.AvailBanks & MemoryBanks4Op.Three) != 0)
                B3ChkBx.Checked = true;
            else // not available, useless to try
                B3ChkBx.Enabled = false;
        }

        private void OnB0ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B0ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B0_1TxtBx.Enabled = true;
                    B0_2TxtBx.Enabled = true;
                    PwdReqChkBx.Visible = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B0_1TxtBx.Enabled = false;
                    B0_2TxtBx.Enabled = false;
                    PwdReqChkBx.Visible = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");

            }
        }

        private void OnB2ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B2ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B2_1TxtBx.Enabled = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B2_1TxtBx.Enabled = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");

            }
        }

        private void OnB3ChkBxStateChanged(object sender, EventArgs e)
        {
            switch (B3ChkBx.CheckState)
            {
                case CheckState.Checked:
                    // enable corresponding TextBox (reset content?)
                    B3TxtBx.Enabled = true;
                    break;
                case CheckState.Unchecked:
                    // disable corresponding TextBox
                    B3TxtBx.Enabled = false;
                    break;
                case CheckState.Indeterminate:
                    throw new ApplicationException("Not supposed to be here");
            }
        }

        private MemoryBanks4Op SelectedBanksToEnum()
        {
            MemoryBanks4Op bnks = MemoryBanks4Op.None;

            if (B0ChkBx.Checked)
                bnks |= MemoryBanks4Op.Zero;
            if (B1ChkBx.Checked)
                bnks |= MemoryBanks4Op.One;
            if (B2ChkBx.Checked)
                bnks |= MemoryBanks4Op.Two;
            if (B3ChkBx.Checked)
                bnks |= MemoryBanks4Op.Three;

            return bnks;
        }
        #endregion

        #region LngLstButton Routines
        private void OnLngLstButtonClicked(object sender, EventArgs e)
        {
            bool IsLong;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            // State stored in Control's 'Tag' field
            if ((IsLong = ListIsLong()) == true)
            {
                // Restore EPCListV Size
                EPCListV.Size = (System.Drawing.Size)resources.GetObject("EPCListV.Size");
                // Show Panel
                //panel1.Show();
                //DataDispPanel.Show();
            }
            else
            {
                // Hide Panel
                //DataDispPanel.Hide();
                panel1.Hide();
                // Change EPCListV Size
                // EPCListV.Height = panel1.Location.Y + panel1.Size.Height - EPCListV.Location.Y;
                //EPCListV.Height = DataDispPanel.Location.Y + DataDispPanel.Size.Height - EPCListV.Location.Y;
            }
            // update state
            SetLngLstButtonState(!IsLong);
        }

        private void InitLngLstButtonState()
        {
            bool ListIsLong = false;
            this.LngLstButton.Tag = ListIsLong; // boxing : creates an object of type bool in heap
        }

        private bool ListIsLong()
        {
            Boolean IsLong = false;

            if (this.LngLstButton.Tag is Boolean)
            {
                IsLong = (Boolean)this.LngLstButton.Tag;
            }
            else
            {
                throw new ApplicationException("LngLstButton Tag is not Boolean");
            }

            return IsLong;
        }

        private void SetLngLstButtonState(bool Long)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));
            Boolean IsLong = Long;

            // Update Label
            if (Long)
            {
                // Display 'Short'
                this.LngLstButton.Text = "Short";
            }
            else
            {
                // Display original 
                this.LngLstButton.Text = resources.GetString("LngLstButton.Text");
            }
            this.LngLstButton.Tag = IsLong;
        }
        #endregion

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        #region Action State variables
        private int fldRdCnt = 0; // TBD: find a better method to store/manage this.
        #endregion

        #region Scan Button Routines
        private void InitScanButtonState()
        {
            OpState State = OpState.Stopped;
            this.ScanButton.Tag = State; // boxing
        }

        private OpState ScanState()
        {
            OpState state = OpState.Stopped;
            if (ScanButton.Tag is OpState)
            {
                state = (OpState)ScanButton.Tag;
            }
            else
            {
                throw new ApplicationException("ScanButton Tag is not OpState");
            }
            return state;
        }

        private void setScanTags()
        {
            int fromRec, toRec;

            fromRec = ((curPage - 1) * recPerPage) + 1;
            toRec = curPage * recPerPage;

            csvTagList = new StringBuilder("'-1'");
            Int32 iTag;

            if (InTagArray == null)
                InTagArray = new ArrayList(recPerPage);
            else
                InTagArray.Clear();

            if (barCodeScan == false)
            {
                if (EPCListV.Items.Count <= toRec)
                    toRec = EPCListV.Items.Count;

                for (iTag = fromRec - 1; iTag < toRec; iTag++)
                {
                    csvTagList.Append("," + "'" + EPCListV.Items[iTag].SubItems[1].Text + "'");
                    InTagArray.Add(EPCListV.Items[iTag].SubItems[1].Text);
                }
            }
            else
            {
                if (BCList.Items.Count <= toRec)
                    toRec = BCList.Items.Count;

                string tagid;

                for (iTag = fromRec - 1; iTag < toRec; iTag++)
                {
                    tagid = BCList.Items[iTag].SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                    csvTagList.Append("," + "'" + tagid + "'");
                    InTagArray.Add(tagid);
                }
            }

            lstItems.Clear();
        }

        private void FillDetailData()
        {
            if (lstItems.Count <= 0 && InTagList.Length > 0)
            {
                lstItems = Assets.GetAssetsByTagID("-1" + InTagList);
            }
            //Program.NoOfThreads = (short)(Program.NoOfThreads - 1);
        }

        //public void AddTagInfo(String sTagList, String RSSIValuelist)
        //{
        //    Double RSSIValue;
        //    String[] RssiAr = RSSIValuelist.Split(new char[] { ',' });
        //    Int32 cnt = 0;
        //    foreach (String sTagID in sTagList.Split(new char[] { ',' }))
        //    {
        //        if (sTagID != "-1")
        //        {
        //            try
        //            {
        //                RSSIValue = Convert.ToDouble(RssiAr[cnt].ToString());
        //                TagInfo T = new TagInfo(sTagID);
        //                if (T.isAssetTag())
        //                {
        //                    ItemTags = ItemTags + "," + sTagID;
        //                }
        //                else if (T.isLocationTag())
        //                {
        //                    locationTags = locationTags + "," + sTagID;
        //                    if (LocationRSSI > RSSIValue)
        //                    {
        //                        LocationRSSI = RSSIValue;
        //                        if (SelectedLocationID == 0)
        //                            SelectedLocationID = T.DataKey;
        //                    }
        //                }
        //                else if (T.isEmployeeTag())
        //                {
        //                    EmployeeTags = EmployeeTags + "," + sTagID;
        //                }
        //                else
        //                {
        //                    UnknownTags = UnknownTags + "," + sTagID;
        //                }
        //            }
        //            catch (Exception ep)
        //            {
        //                throw ep;
        //            }
        //            finally
        //            {
        //                cnt++;
        //                GotTagInfo = GotTagInfo + 1;
        //            }
        //        }
        //    }
        //    //Program.NoOfThreads = (Int16)(Program.NoOfThreads - 1);
        //}

        private void SetScanState(OpState newState)
        {
            OpState CurState = ScanState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable scan button
                    ScanButton.Enabled = false;
                    // btnScnBarcd.Enabled = false;
                    // disable read1 button
                    Read1Button.Enabled = false;

                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    ScanButton.Text = "Stop";
                    ScanButton.Enabled = true;
                    // btnScnBarcd.Enabled = true;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    ScanButton.Enabled = false;
                    // btnScnBarcd.Enabled = false;
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ScanButton.Text = resources.GetString("ScanButton.Text");
                    // btnScnBarcd.Enabled = true;
                    ScanButton.Enabled = true;
                    Read1Button.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            ScanButton.Tag = newState;
        }

        private void FillDetailView()
        {
            lstAsset.Items.Clear();
            if (lstItems.Count <= 0 && csvTagList.Length > 0)
            {
                lstItems = Assets.GetAssets_ByTagID(csvTagList.ToString(), recPerPage);
            }

            if (SelectedLocationID != 0)
            {
                cboLoc.SelectedValue = SelectedLocationID;
            }
            Int32 iCount = 0;

            iCount = (curPage - 1) * recPerPage;

            ListViewItem lstItem;
            if (lstAsset.Items.Count <= 0)
            {
                //lstAsset.CheckBoxes = true;

                ListViewItem.ListViewSubItem ls;
                ListViewItem.ListViewSubItem ls1;
                ListViewItem.ListViewSubItem ls2;
                ListViewItem.ListViewSubItem ls3;
                ItemTags = "-1";

                foreach (Assets Ast in lstItems)
                {
                    iCount++;
                    lstItem = new ListViewItem(iCount.ToString());
                    //lstItem = new ListViewItem();
                    if (Ast.ServerKey != 0)
                    {
                        // ItemTags = ItemTags + "," + Ast.TagID.Trim();

                        InTagArray.Remove(Ast.TagID);

                        ls = new ListViewItem.ListViewSubItem();
                        ls.Text = Convert.ToString(Ast.Name.ToString());
                        lstItem.SubItems.Add(ls);

                        ls1 = new ListViewItem.ListViewSubItem();
                        ls1.Text = Convert.ToString(Ast.getLocationName());
                        lstItem.SubItems.Add(ls1);

                        ls2 = new ListViewItem.ListViewSubItem();
                        ls2.Text = Convert.ToString(Ast.LotNo.ToString());
                        lstItem.SubItems.Add(ls2);
                    }

                    ls3 = new ListViewItem.ListViewSubItem();
                    ls3.Text = Convert.ToString(Ast.TagID.Trim());
                    lstItem.SubItems.Add(ls3);

                    lstAsset.Items.Add(lstItem);
                }


                //Tags Those are not assigned to database.
                // String[] TagAr = Convert.ToString("-1" + InTagList).Split(new char[] { ',' });

                foreach (String uTag in InTagArray)
                {
                    //if (uTag.Trim().IndexOf("-1") < 0)
                    // {
                    // if (ItemTags.IndexOf(uTag.Trim()) < 0)
                    // {
                    iCount++;
                    lstItem = new ListViewItem(iCount.ToString());
                    //lstItem = new ListViewItem();

                    ls = new ListViewItem.ListViewSubItem();
                    ls.Text = Convert.ToString("NA");
                    lstItem.SubItems.Add(ls);

                    ls1 = new ListViewItem.ListViewSubItem();
                    ls1.Text = Convert.ToString("");
                    lstItem.SubItems.Add(ls1);

                    ls2 = new ListViewItem.ListViewSubItem();
                    ls2.Text = Convert.ToString("");
                    lstItem.SubItems.Add(ls2);

                    ls3 = new ListViewItem.ListViewSubItem();
                    ls3.Text = Convert.ToString(uTag.Trim());
                    lstItem.SubItems.Add(ls3);

                    lstAsset.Items.Add(lstItem);
                    // }
                    // }
                }
                lstAsset.Refresh();
            }

            //ScanButton.Enabled = true;
            lstAsset.Visible = true;
            EPCListV.Visible = false;
            lstAsset.BringToFront();
            btnSavePage.Enabled = true;
            btnClear.Enabled = true;

            this.Text = "Inventory: Total Read - " + TagCntLabel.Text;

        }

        private void OnScanButtonClicked(object sender, EventArgs e)
        {
            ScanButton.Enabled = false;
            try
            {
                // Scan or Stop
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                switch (ScanState())
                {
                    case OpState.Stopped:
                        if (EPCListV.Items.Count <= 0)
                        {
                            ItemTags = "-1";
                            InTagList = "";
                        }

                        lstAsset.Items.Clear();
                        TagCntLabel.Visible = true;
                        btnSavePage.Visible = false;
                        lstAsset.Visible = false;
                        EPCListV.Visible = true;
                        btnSave.Enabled = false;

                        //Rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                        Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                        SetScanState(OpState.Starting);
                        Program.RefreshStatusLabel(this, "Starting...");
                        //Count Status Message
                        if (!Rdr.SetRepeatedTagObsrvMode(true))
                            // Continue despite error
                            Program.ShowError("Disable Repeated Tag Observation mode failed");
                        //UserPref Pref = UserPref.GetInstance();
                        //Byte[] Mask; uint MaskOffset;
                        //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                        bool Succ = Rdr.TagInventoryStart(5);
                        if (!Succ)
                        {
                            SetScanState(OpState.Stopped);
                            Program.RefreshStatusLabel(this, "Error Stopped");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            Program.ShowError("Error: TagInventory Failed to Start\n");
                            Application.DoEvents();
                            btnSavePage.Enabled = true;
                            btnClear.Enabled = true;
                        }

                       // btnSavePage.Enabled = false;
                       // btnClear.Enabled = false;
                        btnScnBarcd.Enabled = false;
                        break;
                    case OpState.Started:
                        // Stop Tag Read
                        SetScanState(OpState.Stopping);
                        Program.RefreshStatusLabel(this, "Stopping...");
                        if (!Rdr.TagInventoryStop())
                        {
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            SetScanState(OpState.Stopped); // give up
                            Program.RefreshStatusLabel(this, "Error Stopped...");
                            Program.ShowError("Error: TagRead Failed to Stop\n");
                            btnSavePage.Enabled = false;
                            btnClear.Enabled = false;

                        }
                        btnScnBarcd.Enabled = true;
                        break;
                    case OpState.Starting:
                        // Stop Tag Read
                        SetScanState(OpState.Stopping);
                        Program.RefreshStatusLabel(this, "Stopping...");
                        if (!Rdr.TagInventoryStop())
                        {
                            // Restore
                            SetScanState(OpState.Starting);
                            Program.RefreshStatusLabel(this, "Starting...");
                            Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
                        }
                        btnScnBarcd.Enabled = true;
                        break;
                    default:
                        // ignore
                        break;
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
        }
        #endregion

        #region Read1 Button Routines
        private void InitRead1ButtonState()
        {
            OpState State = OpState.Stopped;
            this.Read1Button.Tag = State; // boxing
        }

        private OpState Read1State()
        {
            OpState state = OpState.Stopped;
            if (Read1Button.Tag is OpState)
            {
                state = (OpState)Read1Button.Tag;
            }
            else
            {
                throw new ApplicationException("Read1Button Tag is not OpState");
            }

            return state;
        }

        private void SetRead1State(OpState newState)
        {
            OpState CurState = Read1State();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagRdForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable read1 button
                    Read1Button.Enabled = false;
                    // disable scan button
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    Read1Button.Text = "Stop";
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    Read1Button.Enabled = false;
                    ScanButton.Enabled = false;
                    //btnScnBarcd.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    Read1Button.Text = resources.GetString("Read1Button.Text");
                    Read1Button.Enabled = true;
                    ScanButton.Enabled = true;
                    // btnScnBarcd.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            Read1Button.Tag = newState;
        }

        private bool GetCurrRowUnreadBnk(out MemoryBanks4Op bnk2Rd, out UINT96_T epc)
        {
            bool Succ = false;
            epc = new UINT96_T();
            bnk2Rd = MemoryBanks4Op.None;
            MemoryBanks4Op UnreadBnks = MemoryBanks4Op.None;
            if ((EPCListV.Items.Count > 0) && EPCListV.SelectedIndices.Count > 0)
            {
                Succ = true;
                ListViewItem SelectedRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
                ListVHelper.GetEPC(SelectedRow, out epc);
                UnreadBnks = RowGetEmptyBanks(SelectedRow)
                    & EPCTag.AvailBanks & SelectedBanksToEnum();
                bnk2Rd = EPCTag.CurBankToAcc(UnreadBnks);
            }
            else
                Succ = false;

            return Succ;
        }

        private void OnRead1ButtonClicked(object sender, EventArgs e)
        {
            // Read1 or Stop
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (Read1State())
            {
                case OpState.Stopped:
                    // Target EPC to Read (currently selected) 
                    // TBD: strongest RSSI (sorted list?)
                    UINT96_T TgtEPC = new UINT96_T();
                    MemoryBanks4Op NxtBnk2Rd = MemoryBanks4Op.None;
                    if (GetCurrRowUnreadBnk(out NxtBnk2Rd, out TgtEPC))
                    {
                        if (NxtBnk2Rd == MemoryBanks4Op.None)
                        {
                            MessageBox.Show("No Empty Banks to Read");
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select a tag from the list first");
                        return;
                    }
                    // Prompt for password if required
                    UInt32 CurAccPasswd = 0;
                    if (PwdReqChkBx.Visible == true && PwdReqChkBx.Checked == true)
                    {
                        if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
                        {
                            // user cancel
                            return;
                        }
                    }
                    //Rdr.RdOpStEvent += new EventHandler<RdOpEventArgs>(TgtRdOpStEventHandler);
                    //Rdr.MemBnkRdEvent += MemBnkRdEvtHandler;
                    Rdr.RegisterRdOpStEvent(TgtRdOpStEventHandler);
                    Rdr.RegisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    SetRead1State(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                    fldRdCnt = 0;
                    BnkRdOffset = 0;
                    errSummary.Clear();
                    UserPref Pref = UserPref.GetInstance();
                    Rdr.TagtEPC = TgtEPC;
                    //Rdr.TagReadBankData += TgtBnkReqCb;
                    Rdr.RegisterTagReadBankDataEvent(TgtBnkReqCb);
                    Rdr.CurAccPasswd = CurAccPasswd;
                    if (!Rdr.TagReadBanksStart())
                    {
                        SetRead1State(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        MessageBox.Show("Error: TagRead Failed to Start\n");
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    }
                    break;
                case OpState.Started:
                    // Stop Tag Read
                    SetRead1State(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    btnSave.Enabled = true;
                    if (!Rdr.TagReadStop())
                    {
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                        MessageBox.Show("Error: TagRead Failed to Stop\n");
                        SetRead1State(OpState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped...");
                        btnSave.Enabled = false;
                    }
                    break;
                default:
                    // ignore
                    break;
            }
        }


        #endregion

        #region EPCListV Routines

        //
        // Helper function for RowGetEmptyBanks
        //
        private void CheckAndMarkEmptyBank(String[] memBnks, int bIdx,
            MemoryBanks4Op bnkToChk, ref MemoryBanks4Op markBnks)
        {
            if (memBnks[bIdx] == null || memBnks[bIdx].Length == 0)
                markBnks |= bnkToChk;
        }

        private MemoryBanks4Op RowGetEmptyBanks(ListViewItem item)
        {
            MemoryBanks4Op EmptyBanks = MemoryBanks4Op.None;
            String[] MemBanks = ListVHelper.GetBnkData(item);
            CheckAndMarkEmptyBank(MemBanks, 0, MemoryBanks4Op.Zero,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 1, MemoryBanks4Op.One,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 2, MemoryBanks4Op.Two,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 3, MemoryBanks4Op.Three,
                ref EmptyBanks);
            return EmptyBanks;
        }

        private void OnEPCListVSelChanged(object sender, EventArgs e)
        {
            if (AnyOperationRunning() || EPCListV.SelectedIndices.Count <= 0)
            {
                DataDispPanel.Enabled = false;
                Read1Button.Enabled = false;
                return;
            }

            if (EPCListV.SelectedIndices.Count > 0)
            {
                if (Convert.ToInt32(cboLoc.SelectedValue) != 0)
                    LoadTxtBxes(EPCListV.Items[EPCListV.SelectedIndices[0]]);
                else
                    MessageBox.Show("Please select refrence location first.");
            }

            Read1Button.Enabled = true;
            DataDispPanel.Enabled = true;
        }

        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text = EPCListV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
            // Application.DoEvents(); 
        }
        #endregion

        #region TextBoxes Routines
        private void LoadTxtBxes(ListViewItem row)
        {
            String[] MemBnks = ListVHelper.GetBnkData(row);
            // Display Tag Data at the TextFields
            TxtBxesDpyData(MemBnks);
        }

        private void TxtBxesDpyData(String[] MemBnks)
        {
            // Bank 0 (Reserved) // Kill and Access Passwords
            B0_1TxtBx.Text = (MemBnks[0] != null) ? MemBnks[0].Substring(0, 8) : "";
            B0_2TxtBx.Text = (MemBnks[0] != null) ? MemBnks[0].Substring(8, 8) : "";

            // Bank 1 (EPC + PC)
            // CRC(2x2) + PC(2x2) + EPC(...)
            B1_1TxtBx.Text = (MemBnks[1] != null) ? MemBnks[1].Substring(4, 4) : ""; // PC
            B1_2TxtBx.Text = (MemBnks[1] != null) ? MemBnks[1].Substring(8) : ""; //EPC

            // Bank2 (TID)
            B2_1TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(0, 2) : ""; // Allocation Class Identifier
            B2_2TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(2, 6) : ""; // Class-specific attributes
            B2_3TxtBx.Text = (MemBnks[2] != null) ? MemBnks[2].Substring(8) : ""; // Tag/Vendor-specific

            // Bank3 (User)
            B3TxtBx.Text = (MemBnks[3] != null) ? MemBnks[3] : "";

            //Retrive Tag Info from database.
            setTagInfo(B1_2TxtBx.Text);

        }

        private void setTagInfo(String TagID)
        {
            try
            {
                //Bkey
                SelectedAsset = new Assets(TagID);
                txtAsset.Text = SelectedAsset.Name;
                txtTag.Text = TagID;
                txtLoc.Text = SelectedAsset.getLocationName();
                btnSave.Enabled = true;
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
        }
        #endregion

        #region ScanRead Event Handlers
        private void ScanRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetScanState(OpState.Started);
                    // Make the List Long (purpose of scanning)
                    if (!ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    errSummary.Rec(e.TagAccErr);
                    break;
                case RdOpStatus.errorStopped:
                case RdOpStatus.stopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= ScanRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(ScanRdOpStEventHandler);
                    SetScanState(OpState.Stopped);
                    if (fldRdCnt > 0)
                    {
                        if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
                        {
                            EPCListV.Items[0].Selected = true; // select and focus on the first one
                            EPCListV.Focus();
                        }
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " new Tags");
                    }
                    else
                        Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
                    if (ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        private void MemBnkRdEvtHandler(object sender, MemBnkRdEventArgs e)
        {
            bool FldAdded;
            // Debug: Add/Update Tag to EPCListV 
            // Save/Update Assciated Data with Row
            ListViewItem Item = ListVHelper.AddEPC(e.CRC, e.PC, e.EPC);
            RefreshTagCntLabel();
            // associate bank data with row
            if (e.BankNum == MemoryBanks4Op.One)
                FldAdded = false; // ignore this bank
            else
            {
                if (BnkRdBuf == null)
                    BnkRdBuf = new StringBuilder();
                if (BnkRdOffset == 0 && BnkRdBuf.Length > 0)
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                BnkRdBuf = BnkRdBuf.Append(e.Data);
                BnkRdOffset += e.Data.Length / 4; // 4 hex chars per Word
                // if full-bank is read, store to list and reset the buffer
                if ((BnkRdBuf.Length / 4) == EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(e.BankNum)))
                {
                    FldAdded = ListVHelper.StoreData(Item, e.BankNum, BnkRdBuf.ToString());
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                    BnkRdOffset = 0;
                }
                else
                {
                    FldAdded = false;
                }
            }
            if (FldAdded)
            {
                fldRdCnt++;
                Program.RefreshStatusLabel(this, "Fields Added : " + fldRdCnt);
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }

            // Update Text Fields if applicable
            if (EPCListV.SelectedIndices.Count > 0)
            {
                if (Item.Index == EPCListV.SelectedIndices[0])
                {
                    LoadTxtBxes(Item);
                }
            }
        }

        private bool Bnk2ReqCb(out MemoryBanks4Op bnks2Rd)
        {
            bnks2Rd = MemoryBanks4Op.Two;
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (ScanState() == OpState.Stopping || ScanState() == OpState.Stopped)
                return false;
            // Should we continue if invalid address error / too many errors occurs?
            return true; // until user stops
        }
        #endregion

        #region Tag Inventory Event Handler

        private TagRangeList tagList = new TagRangeList();

        int count = 0;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {

            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                count++;
                //if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                //{
                //    // New Tag (Note: missing CRC data)
                //    ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);
                //    RefreshTagCntLabel();
                //    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                //    if (count >= 10)
                //    {                        
                //        Application.DoEvents();
                //        EPCListV.Refresh(); 
                //        count = 0;
                //    }
                //}

                if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                {
                    AddEPCToListV(e.PC, e.EPC);
                    RefreshTagCntLabel();
                    if (count >= 10)
                    {
                        count = 0;
                        EPCListV.Refresh();
                        Application.DoEvents();
                    }
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                }

            }
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState State = ScanState();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    SetScanState(OpState.Started);
                    // Make the List Long (purpose of scanning)
                    if (!ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        // Display error or Restart Radio (if necessary)
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        //ushort MacErrCode;
                        if (Rdr.GetMacError() && Reader.macerr != 0)
                        {
                            // if fatal mac error, display message
                            if (Rdr.MacErrorIsFatal() || (ScanState() != OpState.Stopping))
                                Program.ShowError(e.msg);
                        }
                        else
                            Program.ShowError("Unknown HW error. Abort");
                    }
                    // restore StartButton

                    SetScanState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Finished");
                    if (ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (State)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetScanState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }

            // Re-enable Field Input Textboxes (disabled during inventory operation)
            if (ScanState() == OpState.Stopped)
            {
                ScanButton.Enabled = false;
                Cursor.Current = Cursors.WaitCursor;
                try
                {
                    if (EPCListV.Items.Count > 0)
                    {
                        EPCListV.Visible = false;
                        Program.RefreshStatusLabel(this, "Data Processing..");
                        TagCntLabel.Visible = false;
                        lstAsset.Visible = true;

                        lstAsset.BringToFront();

                        cboLoc.Size = new Size(220, 23);

                        lblPage.Visible = true;
                        cmbPage.Visible = true;

                        btnClear.Visible = true;
                        btnSaveAll.Visible = true;

                        if (Login.OnLineMode)
                        {
                            btnSavePage.Visible = false;
                        }
                        else
                        {
                            btnSavePage.Visible = true;
                        }

                        ScanButton.Visible = false;
                        btnScnBarcd.Visible = false;

                        Application.DoEvents();

                        //while (Program.NoOfThreads >= 10)
                        //{
                        //    System.Threading.Thread.Sleep(5);
                        //}

                        //if (NewTags != "-1")
                        //{
                        //    //Program.NoOfThreads = (short)(Program.NoOfThreads + 1);
                        //    AddTagInfo(NewTags, NewRSSI);
                        //    NewTags = "-1";
                        //    NewRSSI = "0";
                        //}

                        //Program.NoOfThreads = (Int16)(Program.NoOfThreads + 1);
                        //System.Threading.Thread objDetailData = new System.Threading.Thread(new System.Threading.ThreadStart(FillDetailData));
                        //objDetailData.Start();

                        SetupPaging(EPCListV.Items.Count);
                        curPage = 1;
                        setScanTags();

                        FillDetailView();

                        if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
                        {
                           // ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                           // LoadTxtBxes(row);
                            // Enable (Avail) Text Fields
                           // DataDispPanel.Enabled = true;
                        }
                    }
                    else
                    {
                        this.Text = "Inventory : Total Read - " + TagCntLabel.Text;
                    }
                }
                catch (Exception epx)
                {
                    throw epx;
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                    ScanButton.Enabled = true;
                    btnScnBarcd.Enabled = true;
                }
            }
        }

        private void AddEPCToListV(UInt16 PC, UINT96_T EPC)
        {
            // Assuming that the Tag of this EPC is not already in list
            int CurNumRows = EPCListV.Items != null ? EPCListV.Items.Count : 0;
            ListViewItem item = new ListViewItem(new String[] {
                 (CurNumRows+1).ToString(), EPC.ToString() });
            // Insert new item to the top of the list for easy viewing
            EPCListV.Items.Insert(0, item);
            item.Tag = PC;
            // EPCListV.Refresh();
            // RefreshTagCntLabel();
        }

        #endregion

        #region Paging Code

        int curPage, totalPages;
        public int recPerPage;

        DataTable dtPages;
        private int SetupPaging(int totalRec)
        {
            //int totalRec = EPCListV.Items.Count;
            int totalPages = 0;

            dtPages = new DataTable();
            DataColumn dc = new DataColumn("Page");
            dtPages.Columns.Add(dc);

            DataColumn dcc = new DataColumn("Index");
            dtPages.Columns.Add(dcc);

            if (totalRec > 0)
            {
                totalPages = totalRec / recPerPage;
                if (totalRec % recPerPage != 0)
                    totalPages = totalPages + 1;
            }
            
            DataRow dr;

            //dr = dtPages.NewRow();
            //dr["Page"] = "Select";
            //dr["Index"] = 0;
            //dtPages.Rows.Add(dr);

            for (int i = 1; i <= totalPages; i++)
            {
                dr = dtPages.NewRow();
                dr["Page"] = i.ToString();
                dr["Index"] = i;
                dtPages.Rows.Add(dr);
            }

            dtPages.AcceptChanges();

             cmbPage.DataSource = dtPages;
             cmbPage.DisplayMember = "Page";
             cmbPage.ValueMember = "Index";

            if (totalPages > 0)
            cmbPage.SelectedValue = 1;

             cmbPage.Visible = true;
             lblPage.Visible = true;
             cmbPage.SelectedValueChanged += new EventHandler(cmbPage_SelectedValueChanged);

            return totalPages;
        }

        private void cmbPage_SelectedValueChanged(object sender, EventArgs e)
        {
            if (curPage == Convert.ToInt32(cmbPage.SelectedValue))
                return;
            curPage = Convert.ToInt32(cmbPage.SelectedValue);

            Cursor.Current = Cursors.WaitCursor;

            setScanTags();
            FillDetailView();

            Cursor.Current = Cursors.Default;
        }

        #endregion

        #region TgtRead Event Handlers
        private void TgtRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetRead1State(OpState.Started);
                    UINT96_T EPC;
                    MemoryBanks4Op CurEmptyBnk;
                    GetCurrRowUnreadBnk(out CurEmptyBnk, out EPC);
                    Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(CurEmptyBnk) + "." + BnkRdOffset);
                    break;
                case RdOpStatus.completed:
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    // Add error to summary
                    errSummary.Rec(e.TagAccErr);
                    // display error immediately upon Unrecoverable Errors
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.InvalidAddr:
                            MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                "Tag Bank Read Failed");
                            break;
                    }
                    break;
                case RdOpStatus.stopped:
                case RdOpStatus.errorStopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                    SetRead1State(OpState.Stopped);
                    if (fldRdCnt > 0)
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " Banks");
                    else
                        Program.RefreshStatusLabel(this, "Stopped:  " + fldRdCnt + " Banks");
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        StringBuilder BnkRdBuf = null;
        int BnkRdOffset = 0;

        private ushort OptNumWdsToRd(MemoryBanks4Op bnk2Rd, int wdOffset)
        {
            ushort MaxNumWds = EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk2Rd));
            return (ushort)Math.Min(4, MaxNumWds - wdOffset);
        }

        private bool TgtBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        {
            bnk2Rd = MemoryBanks4Op.None;
            wdOffset = 0;
            wdCnt = 0;

            // if unrecoverable error from last read (such as password), stop
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (Read1State() == OpState.Stopping || Read1State() == OpState.Stopped)
                return false;
            UINT96_T TgtEPC;
            if (GetCurrRowUnreadBnk(out bnk2Rd, out TgtEPC) == false)
                return false;
            bool MoreBnks2Rd = (bnk2Rd != MemoryBanks4Op.None);
            if (MoreBnks2Rd)
            {
                wdOffset = (ushort)BnkRdOffset;
                wdCnt = OptNumWdsToRd(bnk2Rd, wdOffset);
                Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(bnk2Rd)
                    + "." + wdOffset);
            }
            return MoreBnks2Rd;
        }
        #endregion

        private bool AnyOperationRunning()
        {
            return ((ScanState() != OpState.Stopped)
                         || (Read1State() != OpState.Stopped));
        }

        private void OnTagRdFormClosing(object sender, CancelEventArgs e)
        {
            e.Cancel = false;

            if (AnyOperationRunning())
            {
                MessageBox.Show("Please stop the scan operation first");
                e.Cancel = true;
            }

            // TBD: Ask for confirmation here
        }

        private void OnTagRdFormClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnTagRdFormLoad(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor; 

            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);

            recPerPage = UserPref.GetInstance().PageSize;

            //Set Location Combo
            DataTable dtList = new DataTable();
            dtList = Locations.getLocationList();

            DataRow dr = dtList.NewRow();
            dr["ID_Location"] = 0;
            dr["Name"] = "Select Location";
            dtList.Rows.Add(dr);
            dtList.AcceptChanges();

            cboLoc.ValueMember = "ID_Location";
            cboLoc.DisplayMember = "Name";
            cboLoc.DataSource = dtList;

            cboLoc.SelectedValue = 0;

            cboLoc.Size = new Size(310, 23);

            cmbPage.BringToFront();
            lblPage.BringToFront();
            lblPage.Visible = false;
            cmbPage.Visible = false;

            btnClear.Visible = false;
            btnSaveAll.Visible = false;
            btnSavePage.Visible = false;

            barCodeScan = false;

            if (Login.OnLineMode)
            {
                btnSaveAll.Text = "Show All";
            }
            else
            {
                btnSaveAll.Text = "Save All";
            }

            Cursor.Current = Cursors.Default; 

            // Scan Rd
            //this.OnScanButtonClicked(this, null);
        }

        #region F11/F4/F5 Hotkey
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                    // will continue until user stops
                    if (down)
                    {
                        // fake 'Start' key press if not already running
                        if ((!F11Depressed) && (!AnyOperationRunning()))
                        {
                            OnScanButtonClicked(this, null);
                        }
                        F11Depressed = true;
                    }
                    else // up
                    {
                        // Stop running Scan-Rd Op
                        if (ScanState() == OpState.Starting
                            || ScanState() == OpState.Started)
                        {
                            OnScanButtonClicked(this, null);
                        }
                        F11Depressed = false;
                    }
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }
        #endregion

        //private void OnClrLstBttnClicked(object sender, EventArgs e)
        //{
        //    btnSave.Enabled = false;

        //    EPCListV.BeginUpdate();

        //    if (EPCListV.Items != null)
        //        EPCListV.Items.Clear();

        //    EPCListV.EndUpdate();

        //    tagList.Clear();

        //    RefreshTagCntLabel();

        //    // lstAsset.Items.Clear();
        //    // No need to do any thing in TagAccListVHelper
        //}

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void cboLoc_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtLoc_TextChanged(object sender, EventArgs e)
        {

        }

        #region "now this button is not working it is Hidden for all time."
        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (Login.OnLineMode)
                {

                }
                else
                {
                    if (SelectedAsset != null)
                    {
                        SelectedAsset.updateInventory(Convert.ToInt32(cboLoc.SelectedValue));
                        txtLoc.Text = SelectedAsset.getLocationName();
                    }
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
        }
        #endregion

        #region "This is save Page button."
        private void btnSavePage_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                int itemFound = 0;
                int locationId = 0;

                if (Convert.ToInt32(cboLoc.SelectedValue) <= 0)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Please select the location first.");
                    return;
                }
                else
                {
                    locationId = Convert.ToInt32(cboLoc.SelectedValue);
                }
                Int32 iTag;

                String TgNo;
                // Int32 iDescripencies = 0;
                // Assets Ast;
                //DataTable dtOnLine = new DataTable("dtOnLine");
                //if (Login.OnLineMode)
                //{
                //    dtOnLine.Columns.Add("TagID", typeof(string));
                //    dtOnLine.Columns.Add("ID_Location", typeof(Int32));
                //    dtOnLine.Columns.Add("Date_Modified", typeof(DateTime));
                //    dtOnLine.Columns.Add("ModifiedBy", typeof(Int32));
                //    dtOnLine.Columns.Add("RowStatus", typeof(Int32));
                //    dtOnLine.Columns.Add("ServerKey", typeof(Int32));
                //    dtOnLine.AcceptChanges();
                //}

                StringBuilder csvTagids = new StringBuilder("'-1'");

                for (iTag = 0; iTag <= (lstAsset.Items.Count - 1); iTag++)
                {
                    // TgNo = lstAsset.Items[iTag].SubItems[4].Text;                    
                    //Ast = new Assets(TgNo);
                    // if (cboLoc.Text.Trim() != Ast.getLocationName().Trim())
                    // {
                    //     iDescripencies++;
                    // }
                    // if (Ast.AssetNo != null)
                    //     Ast.updateInventory(Convert.ToInt32(cboLoc.SelectedValue));
                    if (lstAsset.Items[iTag].SubItems[1].Text != "NA")
                    {
                        itemFound = 1;
                        csvTagids.Append("," + "'" + lstAsset.Items[iTag].SubItems[4].Text + "'");
                    }

                }

                try
                {
                    if (itemFound == 1)
                    {
                        int result = Assets.update_Inventory(csvTagids.ToString(), locationId);
                        if (result > 0)
                        {
                            MessageBox.Show(" Inventory Saved for Synchronization.");
                        }
                        else
                        {
                            MessageBox.Show("Invalid Tags.");
                        }
                        Cursor.Current = Cursors.Default;
                    }
                    else
                    {
                        MessageBox.Show("Invalid Tags.");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Error : " + ex.Message);
                    Logger.LogError(ex.Message);
                }             

            }            
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }

            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        #endregion

        private void PwdReqChkBx_CheckStateChanged(object sender, EventArgs e)
        {

        }

        // disable form when operation is running
        private void OnBCScanFrmClosed(object sender, EventArgs e)
        {
            try
            {
                this.Enabled = true;
                AddListViewItems();
            }
            catch (Exception ex)
            {
            }
        }

        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            try
            {
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
                this.Enabled = true;
            }
            catch (Exception ex)
            {
            }
        }

        //redirect to scan barcode form.
        private void btnScnBarcd_Click(object sender, EventArgs e)
        {
            if (Convert.ToInt32(cboLoc.SelectedValue) != 0)
            {
                InTagList = null;
                EPCListV.Items.Clear();
                this.Text = "Inventory";
                lstAsset.Items.Clear();
                tagList.Clear();
                lstItems.Clear();

                barCodeScan = true;
                // Open new BarCode Scanning Window
                frmBCScanForm fBCSFm = new frmBCScanForm();
                fBCSFm.Owner = this;
                fBCSFm.OpenMode = frmBCScanForm.FormType.Inventoryform;
                fBCSFm.Show();
                // disable form until this new form closed
                this.Enabled = false;
                fBCSFm.Closed += new EventHandler(this.OnBCScanFrmClosed);
            }
            else
                MessageBox.Show("Please select refrence location first.");

        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            tagList.Clear();
            EPCListV.Items.Clear();
            lstAsset.Items.Clear();
            EPCListV.Visible = true;
            lstAsset.Visible = false;
            //btnSavePage.Enabled = false;
            RefreshTagCntLabel();

            lstItems.Clear(); 

            barCodeScan = false;

            cboLoc.Size = new Size(310, 23);

            lblPage.Visible = false;
            cmbPage.Visible = false;

            btnClear.Visible = false;
            btnSaveAll.Visible = false;
            btnSavePage.Visible = false;

            ScanButton.Visible = true;
            btnScnBarcd.Visible = true;

            this.Text = "Inventory";

        }

        private void btnSaveAll_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                int locationId = 0;

                if (Convert.ToInt32(cboLoc.SelectedValue) <= 0)
                {
                    Cursor.Current = Cursors.Default;
                    MessageBox.Show("Please select the location first.");
                    return;
                }
                else
                {
                    locationId = Convert.ToInt32(cboLoc.SelectedValue);
                }
                Int32 iTag,result = 0;
                String TgNo;
              //  Int32 iDescripencies = 0;
                Assets Ast;
                DataTable dtOnLine = new DataTable("dtOnLine");

                StringBuilder csvTagids = new StringBuilder("'-1'");

                if (!Login.OnLineMode)
                {
                    if (barCodeScan == false)
                    {
                        for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                        {
                            csvTagids.Append("," + "'" + EPCListV.Items[iTag].SubItems[1].Text + "'");
                        }
                    }
                    else
                    {
                        for (iTag = 0; iTag <= (BCList.Items.Count - 1); iTag++)
                        {
                            csvTagids.Append("," + "'" + BCList.Items[iTag].SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A') + "'");
                        }
                    }

                    try
                    {
                        if (csvTagids.Length > 4)
                        {
                            result = Assets.update_Inventory(csvTagids.ToString(), locationId);
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show("Error : " + ex.Message);
                        Logger.LogError(ex.Message);
                    }
                  
                }
                else
                {
                    dtOnLine.Columns.Add("TagID", typeof(string));
                    dtOnLine.Columns.Add("ID_Location", typeof(Int32));
                    dtOnLine.Columns.Add("Date_Modified", typeof(DateTime));
                    dtOnLine.Columns.Add("ModifiedBy", typeof(Int32));
                    dtOnLine.Columns.Add("RowStatus", typeof(Int32));
                    dtOnLine.Columns.Add("ServerKey", typeof(Int32));
                    dtOnLine.AcceptChanges();

                    if (barCodeScan == false)
                    {
                        for (iTag = 0; iTag <= (EPCListV.Items.Count - 1); iTag++)
                        {
                            TgNo = EPCListV.Items[iTag].SubItems[1].Text;
                            DataRow dr;
                            dr = dtOnLine.NewRow();
                            dr["ID_Location"] = locationId;
                            dr["TagID"] = TgNo;
                            dtOnLine.Rows.Add(dr);
                        }
                    }
                    else
                    {
                        for (iTag = 0; iTag <= (BCList.Items.Count - 1); iTag++)
                        {
                            TgNo = BCList.Items[iTag].SubItems[0].Text.PadLeft(24, '0').Replace('Z', 'A');
                            DataRow dr;
                            dr = dtOnLine.NewRow();
                            dr["ID_Location"] = locationId;
                            dr["TagID"] = TgNo;
                            dtOnLine.Rows.Add(dr);

                        }
                    }

                }

                if (!Login.OnLineMode)
                {
                    if (result > 0)
                    {
                        Cursor.Current = Cursors.Default;
                        MessageBox.Show(" Inventory Saved for Synchronization.");
                    }
                    else
                    {
                        MessageBox.Show("Invalid Tags.");
                    }
                }
                else
                {
                    DataTable dtResult = new DataTable("dtResult");
                    dtResult = Assets.OnLineInventory(ref dtOnLine);

                    ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);

                    // Open new Form
                    frmInventory InvFm = new frmInventory();
                    InvFm.checkLocationID = Convert.ToInt64(cboLoc.SelectedValue);
                    InvFm.recPerPage = recPerPage;
                    InvFm.searchedData = dtResult;
                    InvFm.Show();
                    // disable form until this new form closed
                    this.Enabled = false;
                    InvFm.Closed += new EventHandler(this.OnOperFrmClosed);
                }

            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }
        
    }

    public class TagAccErrSummary
    {
        private int[] tagTAErrs;

        public TagAccErrSummary()
        {
            tagTAErrs = new int[(int)AccErrorTypes.totalNumDefined];
        }

        public void Clear()
        {
            Array.Clear(tagTAErrs, 0, tagTAErrs.Length);
        }

        public void DspyErrSummary()
        {
            StringBuilder Sb = new StringBuilder();
            for (int i = 0; i < tagTAErrs.Length; i++)
            {
                if (tagTAErrs[i] > 0)
                {
                    Sb.Append(((AccErrorTypes)i).ToString("F") + ": " + tagTAErrs[i].ToString() + "\n");
                }
            }
            if (Sb.Length > 0)
                MessageBox.Show(Sb.ToString(), "Tag Access Error Summary");
            else
                Program.ShowWarning("No Tag Access Error recorded");
        }

        public void Rec(AccErrorTypes err)
        {
            (tagTAErrs[(int)err])++;
        }

        public bool UnrecoverableErrOccurred
        {
            get
            {
                return (tagTAErrs[(int)AccErrorTypes.InvalidAddr] > 0)
                        || (tagTAErrs[(int)AccErrorTypes.Unauthorized] > 0)
                        || (tagTAErrs[(int)AccErrorTypes.AccessPasswordError] > 0);
            }
        }

        public bool IsUnrecoverable(AccErrorTypes err)
        {
            return (err == AccErrorTypes.InvalidAddr)
                || (err == AccErrorTypes.Unauthorized)
                || (err == AccErrorTypes.AccessPasswordError);
        }

        public bool IsWriteVerifyErrors(AccErrorTypes err)
        {
            return (err == AccErrorTypes.WriteResponseCrcError)
                || (err == AccErrorTypes.WriteVerifyCrcError)
                || (err == AccErrorTypes.WriteRetryCountExceeded);
        }

        public bool IsAnyWriteError(AccErrorTypes err)
        {
            return (err == AccErrorTypes.WriteResponseCrcError)
              || (err == AccErrorTypes.WriteVerifyCrcError)
              || (err == AccErrorTypes.WriteRetryCountExceeded)
            || (err == AccErrorTypes.WriteError)
            || (err == AccErrorTypes.WriteCmdError);
        }

        public bool WriteVerifyWarningsOccurred
        {
            get
            {
                return (tagTAErrs[(int)AccErrorTypes.WriteResponseCrcError] > 0)
                    || (tagTAErrs[(int)AccErrorTypes.WriteVerifyCrcError] > 0)
                    || (tagTAErrs[(int)AccErrorTypes.WriteRetryCountExceeded] > 0);
            }
        }


    }
}