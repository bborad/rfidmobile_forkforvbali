/**************************************************************************************
 * Author : Deepanshu Jouhari
 * Created Date : 21 Sep 2008
 * Last Modified by : 
 * Last Modified : 18 Aug 2009
 * Module Name : Ramp Mobile
 * Decription : For Synchronize functionality
 **************************************************************************************/

using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;

namespace OnRamp
{
    public partial class frmSynchronize : Form
    {
        public delegate void stateHandler(Int64 status);
        public frmSynchronize()
        {
            InitializeComponent();
        }

        

        private void btnStart_Click(object sender, EventArgs e)
        {
            btnStart.Enabled = false;
            try
            {
                Sync ss = new Sync();
                ss.SyncTables();
                SyncProgress.Value = SyncProgress.Maximum;
                btnStart.Enabled = true;
                if (ss.strError.Length != 0)
                {
                    MessageBox.Show("Synchronization completed.Following errors are occured. \r" + ss.strError);
                }
                else
                {
                    MessageBox.Show("Synchronization completed sucessfully.");
                    this.Close();
                }
            }
            catch (ApplicationException ap)
            {
                Program.ShowError(ap.Message.ToString());
                Logger.LogError(ap.Message); 
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message); 
            }
            catch (System.Data.SqlServerCe.SqlCeException epc)
            {
                MessageBox.Show("Data File is not able to access.\r\n"+ epc.Message);
                Logger.LogError(epc.Message); 
            }
            catch (System.Net.WebException wex)
            {
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message); 
            }
            catch (Exception ep)
            {
                Logger.LogError(ep.Message); 
                MessageBox.Show(ep.Message.ToString());
            }
            finally
            {
                //
            }
        }
        private void frmSynchronize_Load(object sender, EventArgs e)
        {
            //ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState); 
            ProgressStatus.progressState += new ProgressStatus.progressStateHandler(ProgressStatus_progressState);  
        }

        void ProgressStatus_progressState(long status)
        {
            if (this.InvokeRequired)
            {
                //ProgressStatus_progressState(status);
                this.Invoke(new stateHandler(this.ProgressStatus_progressState), new Object[] { status });
                return;
            }
            this.SyncProgress.Value = (int)status;
            return;
        }

        private void frmSynchronize_Closing(object sender, CancelEventArgs e)
        {
            ProgressStatus.Status = 0; 
            ProgressStatus.progressState -= new ProgressStatus.progressStateHandler(ProgressStatus_progressState); 
        }
    }
}