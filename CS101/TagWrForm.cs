using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Text;using CS101UILib;
using System.Windows.Forms;
using ClsRampdb;
using ClsLibBKLogs;
using ClsReaderLib;using ClsReaderLib.Devices;
using ClsReaderLib.Devices;
using ClsReaderLib.Devices.Barcode;

namespace OnRamp
{
    public partial class TagWrForm : Form
    {
        #region Tag Operation States (for Sound/LED indicators)
        static public event EventHandler<TagOperEvtArgs> TagOperEvt = new EventHandler<TagOperEvtArgs>(DummyTagOperEvtHandler);
        static private void DummyTagOperEvtHandler(object sender, TagOperEvtArgs e)
        {
            // a dummy such that there would be need to check whether the
            // event instance is null before each dispatch
        }
        #endregion


        public void AddListViewItems(ListView.ListViewItemCollection lAr)
        {
            foreach (ListViewItem li in lAr)
            {

                RFID_18K6C_MEMORY_BANK en = RFID_18K6C_MEMORY_BANK.RFID_18K6C_MEMORY_BANK_EPC;
                ushort crc = 0;
                ushort pc = 0;
                UINT96_T p = new UINT96_T();
                string s = li.Text.PadRight(24).ToString().Replace("Z", "A");
                p.ParseString(li.Text.PadLeft(24, '0').ToString().Replace("Z", "A"));
                string data = "";
                UInt16 iRSSI = 20;
                MemBnkRdEventArgs mem = new MemBnkRdEventArgs(ref crc, ref pc, ref p, en, data, iRSSI);
                MemBnkRdEvtHandler(this, mem);


                ////ListViewItem Row;
                //UINT96_T EPC = new UINT96_T();
                //EPC.ParseString(li.Text.PadLeft(24, '0').ToString().Replace("Z", "A"));
                //UInt16 cnt = 0;
                //float rssi = 20;
                //TagListItem ti = new TagListItem(30, EPC, rssi);

                //tagList.Add(ti);
                //Row = AddEPCToListV(EPC, cnt, rssi);
                //tagList.SetDisplayListRow(Row, li);
                //ti.Updated = TagListItem.UpdatedFlg.NoChange; // reset state

            }
            EPCListV.Refresh();
        }

        private TagAccListVHelper ListVHelper;
        private TagAccErrSummary errSummary;

        enum TagType
        {
            NotSet,  //The tag not read any time to determine it's type 
            Asset,
            Employee,
            Location,
            NotAssigned //This Tags information not found in database
        };

        public enum FormType
        {
            WriteTag,
            AddAsset,
            AddLocation,
            AddEmployee
        };

        private Object TagObject;

        private TagType SelectedTagType = TagType.NotSet;

        public TagWrForm()
        {
            InitializeComponent();

            InitLngLstButtonState();

            InitScanRdButtonState();

            InitRead1ButtonState();

            InitWrite1ButtonState();

            ListVHelper = new TagAccListVHelper(EPCListV);

            errSummary = new TagAccErrSummary();

            tagWrBnkInput.SetBnkModifiedCb(BnkIsChanged);
        }

        #region enum decls
        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped
        }
        #endregion

        #region Action State variables
        private int fldRdCnt = 0; // TBD: find a better method to store/manage this.
        private int fldWrCnt = 0; // TBD: find a better method to store/manage this.
        #endregion

        #region LngLstButton Routines
        private void OnLngLstButtonClicked(object sender, EventArgs e)
        {
            bool IsLong;
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));
            // State stored in Control's 'Tag' field
            if ((IsLong = ListIsLong()) == true)
            {
                // Restore EPCListV Size
                EPCListV.Size = (System.Drawing.Size)resources.GetObject("EPCListV.Size");
                // Show Panel
                DataDispPanel.Show();
            }
            else
            {
                // Hide Panel
                DataDispPanel.Hide();
                // Change EPCListV Size
                EPCListV.Height = DataDispPanel.Location.Y + DataDispPanel.Size.Height - EPCListV.Location.Y;
               
            }
            // update state
            SetLngLstButtonState(!IsLong);
        }

        private void InitLngLstButtonState()
        {
            bool ListIsLong = false;
            this.LngLstButton.Tag = ListIsLong; // boxing : creates an object of type bool in heap
        }

        private bool ListIsLong()
        {
            Boolean IsLong = false;

            if (this.LngLstButton.Tag is Boolean)
            {
                IsLong = (Boolean)this.LngLstButton.Tag;
            }
            else
            {
                throw new ApplicationException("LngLstButton Tag is not Boolean");
            }

            return IsLong;
        }

        private void SetLngLstButtonState(bool Long)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));
            Boolean IsLong = Long;

            // Update Label
            if (Long)
            {
                // Display 'Short'
                this.LngLstButton.Text = "Short";
            }
            else
            {
                // Display original 
                this.LngLstButton.Text = resources.GetString("LngLstButton.Text");
            }
            this.LngLstButton.Tag = IsLong;
        }
        #endregion

        #region EPCListV Routines

        private String ListVItemBnkData(ListViewItem item, int bIdx)
        {
            String[] MemBanks = ListVHelper.GetBnkData(item);

            return MemBanks[bIdx];
        }

        //
        // Helper function for RowGetEmptyBanks
        //
        private void CheckAndMarkEmptyBank(String[] memBnks, int bIdx,
            MemoryBanks4Op bnkToChk, ref MemoryBanks4Op markBnks)
        {
            if (memBnks[bIdx] == null || memBnks[bIdx].Length == 0)
                markBnks |= bnkToChk;
        }

        private MemoryBanks4Op RowGetEmptyBanks(ListViewItem item)
        {
            String[] MemBanks = ListVHelper.GetBnkData(item);
            MemoryBanks4Op EmptyBanks = MemoryBanks4Op.None;

            CheckAndMarkEmptyBank(MemBanks, 0, MemoryBanks4Op.Zero,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 1, MemoryBanks4Op.One,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 2, MemoryBanks4Op.Two,
                ref EmptyBanks);
            CheckAndMarkEmptyBank(MemBanks, 3, MemoryBanks4Op.Three,
                ref EmptyBanks);

            return EmptyBanks;
        }


        private void OnEPCListVSelChanged(object sender, EventArgs e)
        {
            //Put Here the code to get value from database.


            if (AnyOperationRunning() || EPCListV.SelectedIndices.Count <= 0)
            {
                tagWrBnkInput.DisableBnks(EPCTag.AvailBanks);
                tagWrBnkInput.SetBnksRdOnly(EPCTag.AvailBanks);
                Read1Button.Enabled = false;
                Write1Button.Enabled = false;
                btnAddAsset.Enabled = false;
                btnAddEmployee.Enabled = false;
                btnAddLocation.Enabled = false;
                return;
            }
            if (tagWrBnkInput.TxtBxesHasModifiedData())
            {
                MessageBox.Show("User changes will be lost", "Warning");
            }
            ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
            LoadTxtBxes(row);

            // Enable (Avail) Text Fields
            tagWrBnkInput.EnableBnks(EPCTag.AvailBanks);
            tagWrBnkInput.SetBnksWriteable(EPCTag.AvailBanks);

            // Enable Read-Selected button
            Read1Button.Enabled = true;
            Write1Button.Enabled = true;
            btnAddAsset.Enabled = true;
            btnAddEmployee.Enabled = true;
            btnAddLocation.Enabled = true;
        }

        private void RefreshTagCntLabel()
        {
            TagCntLabel.Text = EPCListV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLabel.Refresh();
        }

        private void OnClrLstBttnClicked(object sender, EventArgs e)
        {
            EPCListV.BeginUpdate();

            if (EPCListV.Items != null)
                EPCListV.Items.Clear();

            EPCListV.EndUpdate();

            tagList.Clear();

           
            RefreshTagCntLabel();
            // No need to do any thing in TagAccListVHelper
        }
        #endregion

        #region ScanRd Button Routines
        private void InitScanRdButtonState()
        {
            OpState State = OpState.Stopped;
            this.ScanRdButton.Tag = State; // boxing
        }

        private OpState ScanRdState()
        {
            OpState state = OpState.Stopped;
            if (ScanRdButton.Tag is OpState)
            {
                state = (OpState)ScanRdButton.Tag;
            }
            else
            {
                throw new ApplicationException("ScanRdButton Tag is not OpState");
            }
            return state;
        }

        private void SetScanRdState(OpState newState)
        {
            OpState CurState = ScanRdState();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable scan button
                    ScanRdButton.Enabled = false;
                    // disable read1 button
                    Read1Button.Enabled = false;
                    // disable write1 button
                    Write1Button.Enabled = false;
                    btnAddAsset.Enabled = false;
                    btnAddEmployee.Enabled = false;
                    btnAddLocation.Enabled = false;
                    // disable write-any button
                    ScanWrButton.Enabled = false;
                    ClrLstBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    ScanRdButton.Text = "Stop";
                    ScanRdButton.Enabled = true;
                    ClrLstBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    ScanRdButton.Enabled = false;
                    ClrLstBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    ScanRdButton.Text = resources.GetString("ScanRdButton.Text");
                    ScanRdButton.Enabled = true;
                    Read1Button.Enabled = true;
                    Write1Button.Enabled = true;
                    btnAddAsset.Enabled = true;
                    btnAddEmployee.Enabled = true;
                    btnAddLocation.Enabled = true;
                    ScanWrButton.Enabled = true;
                    ClrLstBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }
            ScanRdButton.Tag = newState;
        }

        private void OnScanRdButtonClicked(object sender, EventArgs e)
        {
            // Scan or Stop
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (ScanRdState())
            {
                case OpState.Stopped:
                    //Rdr.InvtryOpStEvent += InvtryOpEvtHandler;
                    //Rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent += DscvrTagEvtHandler;
                    Rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                    //Rdr.handler += DscvrTagEvtHandler;
                    SetScanRdState(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                    if (!Rdr.SetRepeatedTagObsrvMode(true))
                        // Continue despite error
                        MessageBox.Show("Disable Repeated Tag Observation mode failed", "Error");
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] Mask; uint MaskOffset;
                    //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                    if (!Rdr.TagInventoryStart(5))
                    {
                        // rewind previous setup
                        SetScanRdState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        MessageBox.Show("Tag Inventory Start Failed", "Error");
                    }
                    btnScnBarcode.Enabled = false;
                    break;
                case OpState.Started:
                    // Stop Tag Read
                    SetScanRdState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!Rdr.TagInventoryStop())
                    {
                        //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                        SetScanRdState(OpState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        MessageBox.Show("Tag Inventory Stop Failed", "Error");
                    }
                    btnScnBarcode.Enabled = true;
                    break;
                case OpState.Starting:
                    SetScanRdState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!Rdr.TagInventoryStop())
                    {
                        // Restore
                        Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                        SetScanRdState(OpState.Starting);
                        Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
                    }
                    btnScnBarcode.Enabled = true;
                    break;
                    
                default:
                    // ignore
                    break;
            }
        }
        #endregion

        #region ScanRead Event Handlers

        private bool RdBnk2ReqCb(out MemoryBanks4Op bnks2Rd)
        {
            if (this.InvokeRequired)
                Program.ShowWarning("RdBnk2ReqCb: InvokeRequired");
            bnks2Rd = MemoryBanks4Op.Two;
            // Should we continue if invalid address error / too many errors occurs?
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (ScanRdState() == OpState.Stopping || ScanRdState() == OpState.Stopped)
                return false;
            return true; // until user stops
        }

        private void ScanRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetScanRdState(OpState.Started);
                    // Make the List Long (purpose of scanning)
                    if (!ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    Program.RefreshStatusLabel(this, "Scanning...");
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    errSummary.Rec(e.TagAccErr);
                    break;
                case RdOpStatus.stopped:
                case RdOpStatus.errorStopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= ScanRdOpStEventHandler;
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    Rdr.UnregisterRdOpStEvent(ScanRdOpStEventHandler);
                    

                    SetScanRdState(OpState.Stopped);
                    if (fldRdCnt > 0)
                    {
                        if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
                        {
                            EPCListV.Items[0].Selected = true; // select and focus on the first one
                            EPCListV.Focus();
                        }
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " new Tags");
                    }
                    else
                        Program.RefreshStatusLabel(this, "Stopped: 0 new Tags");
                    if (ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        private void MemBnkRdEvtHandler(object sender, MemBnkRdEventArgs e)
        {
            bool FldAdded;
            // Debug: Add/Update Tag to EPCListV 
            // Save/Update Assciated Data with Row
            ListViewItem Item = ListVHelper.AddEPC(e.CRC, e.PC, e.EPC);
            RefreshTagCntLabel();
            // associate bank data with row
            if (e.BankNum == MemoryBanks4Op.One)
                FldAdded = false; // ignore this bank
            else
            {
                if (BnkRdBuf == null)
                    BnkRdBuf = new StringBuilder();
                if (BnkRdOffset == 0 && BnkRdBuf.Length > 0)
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                BnkRdBuf = BnkRdBuf.Append(e.Data);
                BnkRdOffset += e.Data.Length / 4; // 4 hex chars per Word
                // if full-bank is read, store to list and reset the buffer
                if ((BnkRdBuf.Length / 4) == EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(e.BankNum)))
                {
                    FldAdded = ListVHelper.StoreData(Item, e.BankNum, BnkRdBuf.ToString());
                    BnkRdBuf = BnkRdBuf.Remove(0, BnkRdBuf.Length);
                    BnkRdOffset = 0;
                }
                else
                {
                    FldAdded = false;
                }
            }
            if (FldAdded)
            {
                fldRdCnt++;
                Program.RefreshStatusLabel(this, "Fields Added : " + fldRdCnt);
                TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
            }
            /** Remove 'Fld Cnt' column from List 
             * Update "#Fld Rd" column in EPCListV
            if (FldAdded)
                ListVIncrFldCntCol(Item);
             */

            // Update Text Fields if applicable
            if (EPCListV.SelectedIndices.Count > 0)
            {
                if (Item.Index == EPCListV.SelectedIndices[0])
                {
                    LoadTxtBxes(Item);
                }
            }

        }

        #endregion

        #region Tag Inventory Event Handler

        private TagRangeList tagList = new TagRangeList();

        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                if (tagList.Add(new TagListItem(e.PC, e.EPC, e.RSSI)))
                {
                    // New Tag (Note: missing CRC data)
                    ListViewItem Item = ListVHelper.AddEPC(0, e.PC, e.EPC);
                    RefreshTagCntLabel();

                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                }
            }
        }

        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState State = ScanRdState();
            Reader Rdr = ReaderFactory.GetReader();
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Scanning...");
                    SetScanRdState(OpState.Started);
                    if (!ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                    //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        // Display error or Restart Radio (if necessary)
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            Program.ShowError("Tag Inventory Stopped with Error: " + e.msg);
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        //ushort MacErrCode;
                        if (Rdr.GetMacError() && Reader.macerr != 0)
                        {
                            // if fatal mac error, display message
                            if (Rdr.MacErrorIsFatal() && ((ScanRdState() != OpState.Stopping) && (ScanRdState() != OpState.Stopped)))
                                Program.ShowError(e.msg);
                        }
                        else
                            Program.ShowError("Unknown HW error. Abort");
                    }
                    // restore StartButton
                    SetScanRdState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Finished");
                    if (ListIsLong())
                        OnLngLstButtonClicked(this, null);
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (State)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //Rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //Rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            Rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            Rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    SetScanRdState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
            }
            // Re-enable Field Input Textboxes (disabled during inventory operation)
            if (ScanRdState() == OpState.Stopped)
            {
                if (EPCListV.SelectedIndices != null && EPCListV.SelectedIndices.Count > 0)
                {
                    ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                    LoadTxtBxes(row);

                    // Enable (Avail) Text Fields
                    tagWrBnkInput.EnableBnks(EPCTag.AvailBanks);
                    tagWrBnkInput.SetBnksWriteable(EPCTag.AvailBanks);
                }
            }
        }
        #endregion

        #region Write1 Button Struct and Routines
        private struct Write1Oper
        {
            public UINT96_T EPC;
            public MemoryBanks4Op RemBnksToWr; // Remaining Banks to Write
            public OpState State;

            public Write1Oper(String EPCStr, MemoryBanks4Op bnks, OpState state)
            {
                this.EPC = new UINT96_T();
                this.EPC.ParseString(EPCStr);
                this.RemBnksToWr = bnks;
                this.State = state;
            }

            public Write1Oper(ref UINT96_T EPC, MemoryBanks4Op bnks, OpState state)
            {
                this.EPC = EPC;
                this.RemBnksToWr = bnks;
                this.State = state;
            }
        }

        private void InitWrite1ButtonState()
        {
            Write1Oper State = new Write1Oper(null,
                MemoryBanks4Op.None, OpState.Stopped);
            this.Write1Button.Tag = State;
        }

        // Get Write1Oper.State
        private OpState Write1OpState()
        {
            OpState OpSt = OpState.Stopped;
            if (Write1Button.Tag is Write1Oper)
            {
                Write1Oper Wr1Op = (Write1Oper)Write1Button.Tag;
                OpSt = Wr1Op.State;
            }
            else
            {
                throw new ApplicationException("Write1Button Tag is not Write1Oper struct type");
            }
            return OpSt;
        }

        // Set Write1Oper.State
        private void SetWrite1OpState(OpState newState)
        {
            if (Write1Button.Tag is Write1Oper)
            {
                Write1Oper Wr1Op = (Write1Oper)Write1Button.Tag;
                System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));
                switch (newState)
                {
                    case OpState.Starting:
                        // disable read1 button
                        Write1Button.Enabled = false;
                        btnAddAsset.Enabled = false;
                        btnAddEmployee.Enabled = false;
                        btnAddLocation.Enabled = false;
                        // disable scan button
                        ScanRdButton.Enabled = false;
                        // disable read1 button
                        Read1Button.Enabled = false;
                        // disable write-any button
                        ScanWrButton.Enabled = false;
                        // Disable ListV to avoid selection change
                        EPCListV.Enabled = false;
                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                        break;
                    case OpState.Started:
                        Write1Button.Text = "Stop";
                        Write1Button.Enabled = true;
                        btnAddAsset.Enabled = true;
                        btnAddEmployee.Enabled = true;
                        btnAddLocation.Enabled = true;
                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                        break;
                    case OpState.Stopping:
                        Write1Button.Enabled = false;
                        btnAddAsset.Enabled = false;
                        btnAddEmployee.Enabled = false;
                        btnAddLocation.Enabled = false;
                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                        break;
                    case OpState.Stopped:
                        Write1Button.Text = resources.GetString("Write1Button.Text");
                        Write1Button.Enabled = true;
                        btnAddAsset.Enabled = true;
                        btnAddEmployee.Enabled = true;
                        btnAddLocation.Enabled = true;
                        ScanRdButton.Enabled = true;
                        Read1Button.Enabled = true;
                        ScanWrButton.Enabled = true;
                        // Re-enable ListV
                        EPCListV.Enabled = true;
                        TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                        break;
                }
                Wr1Op.State = newState;
                Write1Button.Tag = Wr1Op;
            }
        }

        private FormType _OpenMode;

        public FormType OpenMode
        {
            get
            {
                return _OpenMode;
            }
            set
            {
                _OpenMode = value;
            }
        }

#if false
        // MemBnk currently in operation
        private MemoryBanks4Op CurWrite1MemBnk()
        {
            MemoryBanks4Op Bnk = MemoryBanks4Op.None;

            if (Write1Button.Tag is Write1Oper)
            {
                Write1Oper Wr1Op = (Write1Oper)Write1Button.Tag;
                Bnk = EPCTag.CurBankToAcc(Wr1Op.RemBnksToWr);
            }
            return Bnk;
        }

        // Get the next Write1Oper.RemBnksToWr
        // Update the current Write1Oper.RemBnksToWr
        private MemoryBanks4Op DecrWrite1MemBnk()
        {
            MemoryBanks4Op Bnk = MemoryBanks4Op.None;

            if (Write1Button.Tag is Write1Oper)
            {
                Write1Oper Wr1Op = (Write1Oper)Write1Button.Tag;

                Bnk = EPCTag.NextBankToAcc(ref Wr1Op.RemBnksToWr);

                Write1Button.Tag = Wr1Op;
            }

            return Bnk;
        }

        // Set the Write1Oper.RemBnksToWr
        private void SetWrite1MemBnks(MemoryBanks4Op bnks)
        {
            if (Write1Button.Tag is Write1Oper)
            {
                Write1Oper Wr1Op = (Write1Oper)Write1Button.Tag;
                Wr1Op.RemBnksToWr = bnks;
                Write1Button.Tag = Wr1Op;
            }  
        }
#endif
        private void OnWrite1ButtonClicked(object sender, EventArgs e)
        {
            try
            {
                // Write1 or Stop
                //RFIDRdr Rdr = RFIDRdr.GetInstance();
                Reader Rdr = ReaderFactory.GetReader();
                switch (Write1OpState())
                {
                    case OpState.Stopped:
                        if (EPCListV.SelectedIndices.Count > 0)
                        {
                            if (!tagWrBnkInput.TxtBxesHasModifiedData())
                            {
                                MessageBox.Show("Tag Data has not been changed.\r\n"
                                                                        + "No Tag-Write to be performed",
                                                                        "Warning");
                                return;
                            }
                            else
                            {
                                //Change the TagID if itis changed by user.
                                if (tagWrBnkInput.TagNoModified())
                                {
                                    bool showConfirmMsg = false;
                                    //Edit the database
                                    if (SelectedTagType == TagType.NotSet)
                                    {
                                        ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                                        LoadExternalData(row);
                                        showConfirmMsg = true;
                                    }
                                    if (SelectedTagType != TagType.NotAssigned)
                                    {
                                        switch (SelectedTagType)
                                        {
                                            case TagType.Employee:
                                                if (showConfirmMsg)
                                                {
                                                    if (MessageBox.Show("Employee Tag Writing..... OK?", "Info..", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                                                    {
                                                        ((Employee)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                    }
                                                }
                                                else
                                                {
                                                    ((Employee)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                }
                                                break;
                                            case TagType.Asset:
                                                if (showConfirmMsg)
                                                {
                                                    if (MessageBox.Show("Asset Tag Writing..... OK?", "Info..", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                                                    {
                                                        ((Assets)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                    }
                                                }
                                                else
                                                {
                                                    ((Assets)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                }
                                                break;
                                            case TagType.Location:
                                                if (showConfirmMsg)
                                                {
                                                    if (MessageBox.Show("Location Tag Writing..... OK?", "Info..", MessageBoxButtons.OKCancel, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.OK)
                                                    {
                                                        ((Locations)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                    }
                                                }
                                                else
                                                {
                                                    ((Locations)TagObject).AssignNewTag(tagWrBnkInput.TagNo);
                                                }
                                                break;
                                        }
                                    }

                                }
                                // Prompt for password if required
                                UInt32 CurAccPasswd = 0;
                                if (PwdReqChkBx.Checked == true)
                                {
                                    if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
                                        return; // user cancel;
                                }
                                //MemoryBanks4Op Bnk2Wr = GetCurrModifiedBnk2Wr();                             
                               // Rdr.WrOpStEvent += new EventHandler<WrOpEventArgs>(TgtWrOpStEventHandler);
                                //Rdr.MemBnkWrEvent += MemBnkWrEvtHandler;
                                Rdr.RegisterWrOpStEvent(TgtWrOpStEventHandler);
                                Rdr.RegisterMemBnkWrEvent(MemBnkWrEvtHandler);
                                SetWrite1OpState(OpState.Starting);
                                Program.RefreshStatusLabel(this, "Starting..."); // Clear Count Status Message
                                fldWrCnt = 0;
                                errSummary.Clear();
                                ListViewItem SelItem = EPCListV.Items[EPCListV.SelectedIndices[0]];
                                UINT96_T TgtEPC;
                                ListVHelper.GetEPC(SelItem, out TgtEPC);
                                // SetWrite1MemBnks(ModifiedBnks);
                                UserPref Pref = UserPref.GetInstance();
                                Rdr.CurAccPasswd = CurAccPasswd;
                                Rdr.TagtEPC= TgtEPC;
                                Rdr.WriteTagEPC = tagWrBnkInput.TagNo;
                                //Rdr.TagWriteBankData += TagWrBnkDataReqCb;
                                Rdr.RegisterTagWriteBankDataEvent(TagWrBnkDataReqCb);
                                //if (!Rdr.TagWriteStart(Pref.QSize, ref TgtEPC, this.TagWrBnkDataReqCb, CurAccPasswd))
                                if (!Rdr.TagWriteStart(4))
                                {
                                    SetWrite1OpState(OpState.Stopped);
                                    Program.RefreshStatusLabel(this, "Error Stopped");
                                    MessageBox.Show("Error: TagWrite Failed to Start\n");
                                    //Rdr.WrOpStEvent -= TgtWrOpStEventHandler;
                                    //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;
                                    Rdr.UnregisterTagWriteBankDataEvent(TagWrBnkDataReqCb);
                                    Rdr.UnregisterWrOpStEvent(TgtWrOpStEventHandler);
                                    Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                                }
                            }
                        }
                        else
                        {
                            MessageBox.Show("Please select a Tag from List", "Error");
                        }
                        break;
                    case OpState.Started:
                        // Stop Tag Write (graceful stop for now)
                        SetWrite1OpState(OpState.Stopping); // give up
                        Program.RefreshStatusLabel(this, "Stopping...");
                        if (!Rdr.TagWriteStop())
                        {
                            //Rdr.WrOpStEvent -= TgtWrOpStEventHandler;
                            //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;
                            Rdr.UnregisterWrOpStEvent(TgtWrOpStEventHandler);
                            Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                            MessageBox.Show("Error: TagWrite Failed to Stop\n");
                            SetWrite1OpState(OpState.Stopped); // give up
                            Program.RefreshStatusLabel(this, "Error Stopped...");
                        }
                        Rdr.UnregisterTagWriteBankDataEvent(TagWrBnkDataReqCb);
                        break;
                } // switch
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
               // Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
               // Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                //Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                //Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
        }

        
        #endregion

        #region ScanWr Button Routines
        private void TagWrAnyOperStatusUpdated(TagOperEvtType state)
        {
            TagOperEvt(this, new TagOperEvtArgs(state));
        }

        private void OnScanWrButtonClicked(object sender, EventArgs e)
        {
            // Allow TagWrAnyFm to handle hotkeys
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            Program.RefreshStatusLabel(this, null); // clear current status
            TagWrAnyForm TagWrAnyFm = new TagWrAnyForm(TagWrAnyOperStatusUpdated);
            TagWrAnyFm.ShowDialog(); // return after form closed
            TagWrAnyFm.Dispose();

            // Restore Hotkey Handler (avoid duplicate event)
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);

#if false
            // Scan or Stop
            RFIDRdr Rdr = RFIDRdr.GetInstance();
            switch (ScanWrState())
            {
                case OpState.Stopped:
                    // Start Tag Write on Checked Banks
                    if (Rdr.TagWriteStart(null, MemoryBanks4Op.Zero))
                    {
                        Rdr.WrOpStEvent += new EventHandler<WrOpEventArgs>(ScanWrOpStEventHandler);
                        SetScanWrState(OpState.Starting);
                    }
                    else
                    {
                        MessageBox.Show("Error: TagWrite Failed to Start\n");
                    }
                    break;
                case OpState.Started:
                    // Stop Tag Write
                    if (!Rdr.TagWriteStop())
                    {
                        Rdr.WrOpStEvent -= ScanWrOpStEventHandler;
                        MessageBox.Show("Error: TagWrite Failed to Stop\n");
                        SetScanWrState(OpState.Stopped); // give up
                    }
                    else
                    {
                        SetScanWrState(OpState.Stopping);
                    }
                    break;
                default:
                    // ignore
                    break;
            }
#endif
        }
        #endregion

        #region Read1 Button Routines
        private void InitRead1ButtonState()
        {
            OpState State = OpState.Stopped;
            this.Read1Button.Tag = State; // boxing
        }

        private OpState Read1State()
        {
            OpState state = OpState.Stopped;
            if (Read1Button.Tag is OpState)
            {
                state = (OpState)Read1Button.Tag;
            }
            else
            {
                throw new ApplicationException("Read1Button Tag is not OpState");
            }

            return state;
        }

        private void SetRead1State(OpState newState)
        {
            OpState CurState = Read1State();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrForm));

            switch (newState)
            {
                case OpState.Starting:
                    // disable read1 button
                    Read1Button.Enabled = false;
                    // disable scan button
                    ScanRdButton.Enabled = false;
                    // disable write1 button
                    Write1Button.Enabled = false;
                    btnAddAsset.Enabled = false;
                    btnAddEmployee.Enabled = false;
                    btnAddLocation.Enabled = false;
                    // disable write-any button
                    ScanWrButton.Enabled = false;
                    ClrLstBttn.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Started));
                    break;
                case OpState.Started:
                    Read1Button.Text = "Stop";
                    Read1Button.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopping:
                    Read1Button.Enabled = false;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));
                    break;
                case OpState.Stopped:
                    Read1Button.Text = resources.GetString("Read1Button.Text");
                    Read1Button.Enabled = true;
                    ScanRdButton.Enabled = true;
                    Write1Button.Enabled = true;
                    btnAddAsset.Enabled = true;
                    btnAddEmployee.Enabled = true;
                    btnAddLocation.Enabled = true;
                    ScanWrButton.Enabled = true;
                    ClrLstBttn.Enabled = true;
                    TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Stopped));
                    break;
            }

            Read1Button.Tag = newState;
        }

        private void OnRead1ButtonClicked(object sender, EventArgs e)
        {
            // Read1 or Stop
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (Read1State())
            {
                case OpState.Stopped:
                    UINT96_T TgtEPC = new UINT96_T();
                    MemoryBanks4Op NxtBnk2Rd = MemoryBanks4Op.None;
                    if (GetCurrRowUnreadBnk(out NxtBnk2Rd, out TgtEPC))
                    {
                        if (NxtBnk2Rd == MemoryBanks4Op.None)
                        {
                            MessageBox.Show("No Empty Banks to Read");
                            tagWrBnkInput.setDataFromDatabase(TgtEPC.ToString());
                            return;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Select a tag from the list first");
                        return;
                    }
                    // Prompt for password if required
                    UInt32 CurAccPasswd = 0;
                    if (PwdReqChkBx.Checked == true)
                    {
                        if (!PasswdInputDlg.GetPassword(out CurAccPasswd))
                            return; // user cancel;
                    }
                    //Rdr.RdOpStEvent += new EventHandler<RdOpEventArgs>(TgtRdOpStEventHandler);
                    //Rdr.MemBnkRdEvent += MemBnkRdEvtHandler;
                    Rdr.RegisterRdOpStEvent(TgtRdOpStEventHandler);
                    Rdr.RegisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    SetRead1State(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting...");
                    fldRdCnt = 0;
                    BnkRdOffset = 0;
                    errSummary.Clear();
                    UserPref Pref = UserPref.GetInstance();
                    Rdr.TagtEPC = TgtEPC;
                    //Rdr.TagReadBankData += TgtRdBnkReqCb;
                    Rdr.RegisterTagReadBankDataEvent(TgtRdBnkReqCb);
                    Rdr.CurAccPasswd = CurAccPasswd;
                    if (!Rdr.TagReadBanksStart())
                    {
                        SetRead1State(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        MessageBox.Show("Error: TagRead Failed to Start\n");
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                    }
                    break;
                case OpState.Started:
                    // Stop Tag Read
                    SetRead1State(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!Rdr.TagReadStop())
                    {
                        //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                        //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                        Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                        Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);
                        MessageBox.Show("Error: TagRead Failed to Stop\n");
                        SetRead1State(OpState.Stopped); // give up
                        Program.RefreshStatusLabel(this, "Error Stopped...");
                    }
                    break;
                default:
                    // ignore
                    break;
            }
        }

        private bool GetCurrRowUnreadBnk(out MemoryBanks4Op bnk2Rd, out UINT96_T epc)
        {
            bool Succ = false;
            epc = new UINT96_T();
            bnk2Rd = MemoryBanks4Op.None;
            MemoryBanks4Op UnreadBnks = MemoryBanks4Op.None;
            if ((EPCListV.Items.Count > 0) && EPCListV.SelectedIndices.Count > 0)
            {
                Succ = true;
                ListViewItem SelectedRow = EPCListV.Items[EPCListV.SelectedIndices[0]];
                ListVHelper.GetEPC(SelectedRow, out epc);
                UnreadBnks = RowGetEmptyBanks(SelectedRow)
                    & EPCTag.AvailBanks;
                bnk2Rd = EPCTag.CurBankToAcc(UnreadBnks);
            }
            else
                Succ = false;

            return Succ;
        }
        #endregion

        #region TgtRead Event Handlers
        private void TgtRdOpStEventHandler(object sender, RdOpEventArgs e)
        {
           // RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case RdOpStatus.started:
                    SetRead1State(OpState.Started);
                    UINT96_T EPC;
                    MemoryBanks4Op CurEmptyBnk;
                    GetCurrRowUnreadBnk(out CurEmptyBnk, out EPC);
                    Program.RefreshStatusLabel(this, "Reading Bank " + +EPCTag.MemoryBanks4OpToIdx(CurEmptyBnk) + "." + BnkRdOffset);
                    break;
                case RdOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case RdOpStatus.tagAccError:
                    // Add error to summary
                    errSummary.Rec(e.TagAccErr);
                    // display error immediately upon Unrecoverable Errors
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Read Failed");
                            break;
                        case AccErrorTypes.InvalidAddr:
                            MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                "Tag Bank Read Failed");
                            break;
                    }
                    break;
                case RdOpStatus.stopped:
                case RdOpStatus.errorStopped:
                    //Rdr.MemBnkRdEvent -= MemBnkRdEvtHandler;
                    //Rdr.RdOpStEvent -= TgtRdOpStEventHandler;
                    Rdr.UnregisterRdOpStEvent(TgtRdOpStEventHandler);
                    Rdr.UnregisterMemBnkRdEvent(MemBnkRdEvtHandler);

                    SetRead1State(OpState.Stopped);
                    if (fldRdCnt > 0)
                        Program.RefreshStatusLabel(this, "Success:  " + fldRdCnt + " Banks");
                    else
                        Program.RefreshStatusLabel(this, "Stopped:  " + fldRdCnt + " Banks");
                    if (e.Status == RdOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
            }
        }

        StringBuilder BnkRdBuf = null;
        int BnkRdOffset = 0;

        private ushort OptNumWdsToRd(MemoryBanks4Op bnk2Rd, int wdOffset)
        {
            ushort MaxNumWds = EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk2Rd));
            return (ushort)Math.Min(4, MaxNumWds - wdOffset);
        }

        private bool TgtRdBnkReqCb(out MemoryBanks4Op bnk2Rd, out ushort wdOffset, out ushort wdCnt)
        {
            bnk2Rd = MemoryBanks4Op.None;
            wdOffset = 0;
            wdCnt = 0;
            // if unrecoverable error from last read (such as password), stop
            if (errSummary.UnrecoverableErrOccurred)
                return false;
            if (Read1State() == OpState.Stopping || Read1State() == OpState.Stopped)
                return false;
            UINT96_T TgtEPC;
            if (GetCurrRowUnreadBnk(out bnk2Rd, out TgtEPC) == false)
                return false;
            bool MoreBnks2Rd = (bnk2Rd != MemoryBanks4Op.None);
            if (MoreBnks2Rd)
            {
                wdOffset = (ushort)BnkRdOffset;
                wdCnt = OptNumWdsToRd(bnk2Rd, wdOffset);
                Program.RefreshStatusLabel(this, "Reading Bank " + EPCTag.MemoryBanks4OpToIdx(bnk2Rd)
                    + "." + wdOffset);
            }
            return MoreBnks2Rd;
        }
        #endregion

        #region Write1 EventHandlers
        private void TgtWrOpStEventHandler(object sender, WrOpEventArgs e)
        {
            //RFIDRdr Rdr = RFIDRdr.GetInstance();
            Reader Rdr = ReaderFactory.GetReader();
            switch (e.Status)
            {
                case WrOpStatus.started:
                    SetWrite1OpState(OpState.Started);
                    // Not to over-write the more precise message in ReqTagWrBnkData
                    //Program.RefreshStatusLabel(this, "Writing Bank " + GetCurrModifiedBnk2Wr() + "...");
                    break;
                case WrOpStatus.completed:
                    break;
                case WrOpStatus.error:
                    if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                        MessageBox.Show("Error: " + e.ErrMsg);
                    break;
                case WrOpStatus.tagAccError:
                    // Only collect error summary (before Tag write completed)
                    errSummary.Rec(e.TagAccErr);
                    // display error immediately upon Unrecoverable Errors
                    switch (e.TagAccErr)
                    {
                        case AccErrorTypes.Unauthorized:
                            MessageBox.Show("Password Required", "Tag Bank Write Failed");
                            break;
                        case AccErrorTypes.AccessPasswordError:
                            MessageBox.Show("Incorrect Bank Access Password", "Tag Bank Write Failed");
                            break;
                        case AccErrorTypes.InvalidAddr:
                            MessageBox.Show("Invalid Addr: Requested bank does not exist or Bank Size exceeds available memory",
                                "Tag Bank Write Failed");
                            break;
                    }
                    break;
                case WrOpStatus.stopped:
                case WrOpStatus.errorStopped:
                    //Rdr.WrOpStEvent -= TgtWrOpStEventHandler;
                    //Rdr.MemBnkWrEvent -= MemBnkWrEvtHandler;

                    Rdr.UnregisterWrOpStEvent(TgtWrOpStEventHandler);
                    Rdr.UnregisterMemBnkWrEvent(MemBnkWrEvtHandler);
                    SetWrite1OpState(OpState.Stopped);
                    if (fldWrCnt == 0) // ignore error that comes before successful write
                    {
                        errSummary.DspyErrSummary();
                        Program.RefreshStatusLabel(this, "Stopped: 0 Banks");
                    }
                    else
                        Program.RefreshStatusLabel(this, "Success:  " + fldWrCnt + " Banks");
                    if (e.Status == WrOpStatus.errorStopped)
                    {
                        if (Rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            if (e.ErrMsg != null && e.ErrMsg.Length > 0)
                                MessageBox.Show("Error: " + e.ErrMsg);
                    }
                    break;
                case WrOpStatus.configured:
                    Program.RefreshStatusLabel(this, "Singulation options setup finished");
                    break;
                default:
                    MessageBox.Show("Uncatched WrOpStatus code");
                    break;
            }
        }

        // Assuming that the 'write' is good
        private void MemBnkWrEvtHandler(object sender, MemBnkWrEventArgs e)
        {
            // Update corresponding row (merge if original length is > than write length)
            UINT96_T EPC = e.EPC; // readonly e.EPC couldn't be passed as 'ref'
            ListViewItem Item = ListVHelper.GetRow(ref EPC);
            if (Item == null)
                throw new ApplicationException("BUG: Unable to find EPC from ListV");
            // Note: it looks like TagAccess response does not provide the TagData
            // Assumption:
            //     Whole TextField is written.
            //     Data Written is full-memory-bank-size
            // Special care taken for writing Bank-1 (EPC)
            String InputDataStr = null;
            ushort WdOffset = 0;
            tagWrBnkInput.GetBnkStr(e.BankNum, out WdOffset, out InputDataStr);
            // only update the 'written' part of the input-str to ListVHelper
            ushort FieldOffset = (ushort)(e.wdOffset - WdOffset);
            String WrittenDataStr = InputDataStr.Substring(FieldOffset * 4, e.wdCnt * 4);
            ListVHelper.StoreData(Item, e.BankNum, e.wdOffset, WrittenDataStr);
            if (e.BankNum == MemoryBanks4Op.One)
            {
                // Bnk1 input str merged with Written portion
                String Bnk1Str = ListVHelper.GetBnkData(Item)[1]; // CRC+PC+EPC
                String NewEPCStr = Bnk1Str.Substring(8); // Extract the EPC part
                UINT96_T NewEPC = new UINT96_T();
                if (NewEPC.ParseString(NewEPCStr))
                    ListVHelper.SetEPC(Item, ref NewEPC);
                else
                    MessageBox.Show(NewEPCStr, "Unable to parse EPC String");
            }
            fldWrCnt++;
            Program.RefreshStatusLabel(this, "Banks Written : " + fldWrCnt);
            TagOperEvt(this, new TagOperEvtArgs(TagOperEvtType.Updated));

            // Update Text (Sub)Fields
            if ((EPCListV.SelectedIndices.Count > 0)
                && (Item.Index == EPCListV.SelectedIndices[0]))
            {
                int ChrOffset = 0;
                switch (e.BankNum)
                {
                    case MemoryBanks4Op.Zero:
                        if (e.wdOffset < EPCTag.KillPwdFldSz)
                        {
                            int KillPortionSz = Math.Min(EPCTag.KillPwdFldSz - e.wdOffset, e.wdCnt);
                            String KillPwdPortion = WrittenDataStr.Substring(0, KillPortionSz * 4);
                            ChrOffset = e.wdOffset * 4;
                            tagWrBnkInput.SetTxtBx(e.BankNum, 0, ChrOffset, KillPwdPortion);
                            int SpillOverSz = (e.wdOffset + e.wdCnt) - EPCTag.KillPwdFldSz;
                            if (SpillOverSz > 0)
                            {
                                ChrOffset = 0;
                                tagWrBnkInput.SetTxtBx(e.BankNum, 1, ChrOffset,
                                    WrittenDataStr.Substring(KillPwdPortion.Length));
                            }
                        }
                        else // Starts at/after 'Access Password' offset
                        {
                            ChrOffset = (e.wdOffset - EPCTag.KillPwdFldSz) * 4;
                            tagWrBnkInput.SetTxtBx(e.BankNum, 1, ChrOffset,
                                WrittenDataStr);
                        }
                        break;
                    case MemoryBanks4Op.One:
                        int PCOffset = EPCTag.CRC16FldSz;
                        int EPCOffset = (EPCTag.CRC16FldSz + EPCTag.PCFldSz);
                        if ((e.wdOffset >= PCOffset) && (e.wdOffset < EPCOffset))
                        {
                            ChrOffset = (e.wdOffset - PCOffset) * 4;
                            int PCPortionSz = Math.Min(EPCOffset - e.wdOffset, e.wdCnt);
                            String PCPortion = WrittenDataStr.Substring(0, PCPortionSz * 4);
                            tagWrBnkInput.SetTxtBx(e.BankNum, 0, ChrOffset, PCPortion);
                            int SpillOverSz = (e.wdOffset + e.wdCnt) - EPCOffset;
                            if (SpillOverSz > 0)
                            {
                                ChrOffset = 0;
                                tagWrBnkInput.SetTxtBx(e.BankNum, 1, ChrOffset,
                                    WrittenDataStr.Substring(PCPortion.Length));
                            }
                        }
                        else // Starts at/after EPCOffset
                        {
                            ChrOffset = (e.wdOffset - EPCOffset) * 4;
                            tagWrBnkInput.SetTxtBx(e.BankNum, 1, ChrOffset,
                                WrittenDataStr);
                        }
                        break;
                    case MemoryBanks4Op.Three:
                        ChrOffset = e.wdOffset * 4;
                        tagWrBnkInput.SetTxtBx(e.BankNum, 0, ChrOffset,
                            WrittenDataStr);
                        break;
                }
            }
        }
        #endregion

        #region TagWrite delegate callback

        // TBD: wildcard char. support
        // Each element represent one word(2-bytes/4-char); true=equal, false=different
        // if str1 length != str2 length, ignore the extra char
        // case also ignored
        private bool[] CreateWdDiffMask(String str1, String str2)
        {
            int MaskLen = Math.Min(str1.Length, str2.Length) / 4;
            bool[] DiffMask = new bool[MaskLen];
            for (int i = 0; i < MaskLen; i++)
            {
                DiffMask[i] = true; // initialize to true
                for (int j = i * 4, k = j + 4; j < k; j++)
                {
                    if (Char.ToLower(str1[j]) != Char.ToLower(str2[j]))
                    {
                        DiffMask[i] = false; // mark different
                        break;
                    }
                }
            }
            return DiffMask;
        }

        private bool TagWrBnkDataReqCb(out MemoryBanks4Op bnk, out ushort wdOffSet,
            out String dataStr)
        {
            bnk = MemoryBanks4Op.None;
            wdOffSet = 0;
            dataStr = String.Empty;
            if (errSummary.UnrecoverableErrOccurred) // give up on unrecoverable error
                return false;
            if (Write1OpState() == OpState.Stopping || Write1OpState() == OpState.Stopped)
                return false;
            // Currently Selected item
            bnk = GetCurrModifiedBnk2Wr();
            if (bnk == MemoryBanks4Op.None)
                return false;
            // get user input bank data
            ushort InputFldBnkOffset;
            String InputFldStr;          
            tagWrBnkInput.GetBnkStr(bnk, out InputFldBnkOffset, out InputFldStr);
            // compare to what's loaded (assuming that selection has not changed)
            ListViewItem SelItem = EPCListV.Items[EPCListV.SelectedIndices[0]];
            String CurBnkData = ListVHelper.GetBnkData(SelItem, (int)EPCTag.MemoryBanks4OpToIdx(bnk));
            int BeginMaskIdx = 0, EndMaskIdx = 0;
            if (String.IsNullOrEmpty(CurBnkData))
            {
                // word index
                BeginMaskIdx = 0;
                EndMaskIdx = (InputFldStr.Length / 4) - 1;
            }
            else if ((CurBnkData.Length / 4) <
                EPCTag.MaxBnkMemSizeInWds(EPCTag.MemoryBanks4OpTo18K6CBank(bnk)))
            {
                // Similar to above case, but only partially written so far
                // Assuming that the input field is always filled to the maximum
                BeginMaskIdx = CurBnkData.Length / 4;
                EndMaskIdx = (InputFldStr.Length / 4);
            }
            else
            {
                // Based on the difference, set wdOffset and dataStr
                bool[] DiffMask = CreateWdDiffMask(InputFldStr, CurBnkData.Substring(InputFldBnkOffset * 4));
                // write the next (max) eight words from the first different word

                for (BeginMaskIdx = 0; BeginMaskIdx < DiffMask.Length && DiffMask[BeginMaskIdx] == true; BeginMaskIdx++) ;
                if (BeginMaskIdx >= DiffMask.Length)
                {
                    /* there is no difference some how */
                    MessageBox.Show("Programming error: no difference found");
                    return false;
                }
                for (EndMaskIdx = DiffMask.Length - 1; EndMaskIdx >= 0 && DiffMask[EndMaskIdx] == true; EndMaskIdx--) ;
            }
            wdOffSet = (ushort)(InputFldBnkOffset + BeginMaskIdx);
            int wdCnt = EndMaskIdx - BeginMaskIdx + 1;
            // Bank-1 : write all at once (no way to change match-mask in this callback)
            dataStr = InputFldStr.Substring(BeginMaskIdx * 4,
                (bnk == MemoryBanks4Op.One ? wdCnt : Math.Min(1, wdCnt)) * 4);
            Program.RefreshStatusLabel(this, "Writing Bank " + EPCTag.MemoryBanks4OpToIdx(bnk) + "." + BeginMaskIdx);
            return true;
        }
        #endregion

        #region TextBox support routines

        private bool BnkIsChanged(MemoryBanks4Op bnk, int part, String curInputStr)
        {
            if (EPCListV.Items == null || EPCListV.Items.Count == 0)
                return false;
            if (EPCListV.SelectedIndices == null || EPCListV.SelectedIndices.Count == 0)
                return false;
            ListViewItem Row = EPCListV.Items[EPCListV.SelectedIndices[0]];
            String OrigWholeData = ListVItemBnkData(Row, (int)EPCTag.MemoryBanks4OpToIdx(bnk));
            String OrigData = null;
            int SStrOffset, SStrLen;
            switch (bnk)
            {
                case MemoryBanks4Op.Zero:
                    switch (part)
                    {
                        case 0:
                            SStrOffset = 0;
                            SStrLen = EPCTag.KillPwdFldSz * 4;
                            OrigData = (OrigWholeData != null && OrigWholeData.Length >= (SStrOffset + SStrLen)) ?
                                OrigWholeData.Substring(SStrOffset, SStrLen) : String.Empty;
                            break;
                        case 1:
                            SStrOffset = EPCTag.KillPwdFldSz * 4;
                            SStrLen = EPCTag.AccPwdFldSz * 4;
                            OrigData = (OrigWholeData != null && OrigWholeData.Length >= (SStrOffset + SStrLen)) ?
                                OrigWholeData.Substring(SStrOffset, SStrLen) : String.Empty;
                            break;
                    }
                    break;
                case MemoryBanks4Op.One:
                    int PCOffset = EPCTag.CRC16FldSz;
                    int EPCOffset = EPCTag.CRC16FldSz + EPCTag.PCFldSz;
                    switch (part)
                    {
                        case 0:
                            SStrOffset = PCOffset * 4;
                            SStrLen = EPCTag.PCFldSz * 4;
                            OrigData = (OrigWholeData != null && OrigWholeData.Length >= (SStrOffset + SStrLen)) ?
                                OrigWholeData.Substring(SStrOffset, SStrLen) : String.Empty;
                            break;
                        case 1:
                            SStrOffset = EPCOffset * 4;
                            SStrLen = EPCTag.EPCFldSz * 4;
                            OrigData = (OrigWholeData != null && OrigWholeData.Length >= (SStrOffset + SStrLen)) ?
                                OrigWholeData.Substring(SStrOffset, SStrLen) : String.Empty;
                            break;
                    }
                    break;
                case MemoryBanks4Op.Three:
                    OrigData = OrigWholeData != null ? OrigWholeData : String.Empty;
                    break;
            }

            return (OrigData != null) ? (String.Compare(curInputStr, OrigData, true) != 0)
                : false;
        }

        private MemoryBanks4Op GetCurrModifiedBnk2Wr()
        {
            MemoryBanks4Op ModifiedBnks = tagWrBnkInput.ModifiedBnksToEnum();

            return EPCTag.CurBankToAcc(ModifiedBnks);
        }
        private void LoadExternalData(ListViewItem item)
        {
            try
            {
                UINT96_T tagNo = new UINT96_T();
                ListVHelper.GetEPC(item, out tagNo);

                TagInfo Ti = new TagInfo(tagNo.ToString());
                String displayText;

                if (Ti.isEmployeeTag())
                {
                    B3Label.Text = "Employee";
                    TagObject = new Employee(Ti.DataKey);
                    displayText = ((Employee)TagObject).EmpNo;
                    SelectedTagType = TagType.Employee;
                   // btnAddEmployee.Text = "Edit Employee"; 
                }
                else if (Ti.isLocationTag())
                {
                    B3Label.Text = "Location";
                    TagObject = new Locations(Ti.DataKey);
                    displayText = ((Locations)TagObject).Name;
                    SelectedTagType = TagType.Location;
                    btnAddLocation.Text = "Edit Location";
                }
                else 
                {
                    TagObject = new Assets(tagNo.ToString());
                    if (((Assets)TagObject).Name == null)
                    {
                        B3Label.Text = "-";
                        displayText = "Not Assigned";
                        SelectedTagType = TagType.NotAssigned;                       
                    }
                    else
                    {
                        SelectedTagType = TagType.Asset;
                        B3Label.Text = "Asset";
                        displayText = ((Assets)TagObject).Name;
                        btnAddAsset.Text = "Edit Item";
                    }
                }

                if (SelectedTagType == TagType.NotAssigned)
                {
                    if (OpenMode == FormType.AddAsset)
                        btnAddAsset.Text = "Add Item";
                    else if (OpenMode == FormType.AddLocation)
                        btnAddLocation.Text = "Add Location";
                    else if (OpenMode == FormType.AddEmployee)
                        btnAddEmployee.Text = "Add Employee"; 
                }

                tagWrBnkInput.setDataFromDatabase(displayText);
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }
        }

        private void LoadTxtBxes(ListViewItem item)
        {
            // Display Tag Data at the TextFields
            LoadTxtBx(item, MemoryBanks4Op.Zero);
            LoadTxtBx(item, MemoryBanks4Op.One);
            LoadTxtBx(item, MemoryBanks4Op.Three);

            LoadExternalData(item);

        }

        // Load current Bank Data into TextBox for display
        // Reset TextBox states to unmodified.
        private void LoadTxtBx(ListViewItem item, MemoryBanks4Op bnk)
        {
            String[] MemBnks = ListVHelper.GetBnkData(item);
            String CurBnkData = MemBnks[EPCTag.MemoryBanks4OpToIdx(bnk)];
            switch (EPCTag.MemoryBanks4OpToIdx(bnk))
            {
                case 0:
                    tagWrBnkInput.LoadTxtBx(bnk, 0, (CurBnkData != null && CurBnkData.Length >= 8) ?
                        CurBnkData.Substring(0, 8) : String.Empty);
                    tagWrBnkInput.LoadTxtBx(bnk, 1, (CurBnkData != null && CurBnkData.Length >= 16) ?
                        CurBnkData.Substring(8, 8) : String.Empty);
                    break;
                case 1:
                    tagWrBnkInput.LoadTxtBx(bnk, 0, (CurBnkData != null && CurBnkData.Length >= 8) ?
                        CurBnkData.Substring(4, 4) : String.Empty); // PC
                    tagWrBnkInput.LoadTxtBx(bnk, 1, (CurBnkData != null && CurBnkData.Length >= 8) ?
                        CurBnkData.Substring(8) : String.Empty); //EPC
                    break;
                case 3:

                    tagWrBnkInput.LoadTxtBx(bnk, 0, (CurBnkData != null) ? CurBnkData : String.Empty);
                    //tagWrBnkInput.LoadTxtBx(bnk, 0,DateTime.Now.ToString());
                    break;
                default:
                    MessageBox.Show("Programming Error");
                    break;
            }
        }

        #endregion

        private bool AnyOperationRunning()
        {
            return ((ScanRdState() != OpState.Stopped)
                || (Read1State() != OpState.Stopped)
                || (Write1OpState() != OpState.Stopped));
        }

        private void OnFrmLoad(object sender, EventArgs e)
        {
            try
            {
                // Set up to receive HotKey event
                ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
                
                // Start Scan-Rd Tag
                // Fake Scan-Rd button clicked
                //OnScanRdButtonClicked(this, null);

                //Select Form Mode
                Write1Button.Visible = false;
                btnAddAsset.Visible = false;
                ///Added for Scan Barcode
                btnScnBarcode.Visible = false;
                ///

                switch (this.OpenMode)
                {
                    case FormType.WriteTag:
                        Write1Button.Visible = true;

                        break;
                    case FormType.AddAsset:
                        btnAddAsset.Visible = true;
                        btnScnBarcode.Visible = true;
                        this.Text = "Add Item";
                        break;
                    case FormType.AddEmployee:
                        btnAddEmployee.Visible = true;
                        this.Text = "Add Employee";
                        btnScnBarcode.Visible = true;
                        break;
                    case FormType.AddLocation:
                        btnAddLocation.Visible = true;
                        this.Text = "Add Location";
                        btnScnBarcode.Visible = true;
                        break;
                    default:
                        Write1Button.Visible = true;
                        break;
                }
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                Cursor.Current = Cursors.Default;
                if (ex.Actor.ToString().ToLower().Trim() == "getlogin")
                    Program.ShowError("Request from innvalid IP address.");
                else
                    Program.ShowError("Network Protocol Failure.");
                Logger.LogError(ex.Message);
            }
            catch (System.Data.SqlServerCe.SqlCeException sqlex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Data File is not able to access.");
                Logger.LogError(sqlex.Message);
            }
            catch (System.Net.WebException wex)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show("Web exception occured.");
                Logger.LogError(wex.Message);
            }
            catch (Exception ep)
            {
                Cursor.Current = Cursors.Default;
                MessageBox.Show(ep.Message.ToString());
                Logger.LogError(ep.Message);
            }

        }

        private void OnFmClosing(object sender, CancelEventArgs e)
        {
            // Deny close if operation is running
            if (AnyOperationRunning())
            {
                MessageBox.Show("Please stop any Tag Operation first", "Denied");
                e.Cancel = true;
            }

        }

        private void OnFrmClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }

        #region F11/F4/F5 HotKey Handler
        bool F11Depressed = false;
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11: // no need to support auto-fire because ScanRd is
                    // will continue until user stops
                    if (down)
                    {
                        // fake 'Start' key press if not already running
                        if ((!F11Depressed) && (!AnyOperationRunning()))
                        {
                            OnScanRdButtonClicked(this, null);
                        }
                        F11Depressed = true;
                    }
                    else // up
                    {
                        if (ScanRdState() == OpState.Starting
                            || ScanRdState() == OpState.Started)
                        {
                            OnScanRdButtonClicked(this, null);
                        }
                        F11Depressed = false;
                    }
                    // ignore the up
                    break;
                case eVKey.VK_F4:
                case eVKey.VK_F5:
                    if (!AnyOperationRunning())
                    {
                        AntPwrHtKyPopup.ShowPopup(HotKeyEvtHandler);
                    }
                    break;
                case eVKey.VK_F1:
                    if (down && !AnyOperationRunning())
                    {
                        Program.ShowRdrSummaryDisplayWindow(HotKeyEvtHandler);
                    }
                    break;
            }
        }
        #endregion

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

        private void TagCntLabel_ParentChanged(object sender, EventArgs e)
        {

        }

        private void btnAddAsset_Click(object sender, EventArgs e)
        {
            if (tagWrBnkInput.TagNoModified())
            {
                if (MessageBox.Show("Tag No. Modified, Still want to Add Item.", "Info..", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    return;
                }
            }

            if (EPCListV.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Please select the Tag first.");
                return;
            }

            //Edit the database
            if (SelectedTagType == TagType.NotSet)
            {
                ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                LoadExternalData(row);
            }

            if (SelectedTagType == TagType.Asset)
            {
                if (TagObject != null)
                {
                    Assets objAsset = (Assets)TagObject;
                    frmAddAsset EditAsset = new frmAddAsset();
                    EditAsset.newAsset = false;
                    EditAsset.TagNo = tagWrBnkInput.TagNo;                
                    EditAsset.AssetName = objAsset.Name;
                    EditAsset.RefNo = objAsset.ReferenceNo;
                    EditAsset.LocationID = objAsset.ID_Location.ToString();
                    EditAsset.Description = objAsset.Description.ToString();
                    EditAsset.ServerKey = objAsset.ServerKey;
                    EditAsset.Show();
                    this.Enabled = false;
                    EditAsset.Closed += new EventHandler(this.OnOperFrmClosed);
                }
                else
                {
                    MessageBox.Show("Invalid Item selected.");
                }
            }
            else if (SelectedTagType == TagType.NotAssigned)
            {
                // Open Add Item Form
                frmAddAsset AddAsset = new frmAddAsset();
                AddAsset.newAsset = true;
                AddAsset.TagNo = tagWrBnkInput.TagNo;
                AddAsset.Show();
                this.Enabled = false;
                AddAsset.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Tag already assigned.");
            }


            //if (SelectedTagType != TagType.NotAssigned)
            //{
            //    MessageBox.Show("Tag already assigned.");
            //}



            //frmAd
            //TagRangingForm TagRangingFm = new TagRangingForm();
            //TagRangingFm.Show();
            //// disable form until this new form closed
            //this.Enabled = false;
            //TagRangingFm.Closed += new EventHandler(this.OnOperFrmClosed);

        }

        // disable form when operation is running
        private void OnOperFrmClosed(object sender, EventArgs e)
        {
            this.Enabled = true;
        }

        private void btnAddLocation_Click(object sender, EventArgs e)
        {
            if (tagWrBnkInput.TagNoModified())
            {
                if (MessageBox.Show("Tag No. Modified, Still want to Add Location.", "Info..", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    return;
                }
            }

            if (EPCListV.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Please select the Tag first.");
                return;
            }

            //Edit the database
            if (SelectedTagType == TagType.NotSet)
            {
                ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                LoadExternalData(row);
            }
            if (SelectedTagType == TagType.Location)
            {
                if (TagObject != null)
                {
                    Locations objLoc = (Locations)TagObject;
                    frmAddLocation EditLocation = new frmAddLocation();
                    EditLocation.TagNo = tagWrBnkInput.TagNo;
                    EditLocation.newLocation = false;
                    EditLocation.LocationName = objLoc.Name;
                    EditLocation.LocationNo = objLoc.LocNo;
                    EditLocation.Serverkey = objLoc.ServerKey;
                    EditLocation.Show();
                    this.Enabled = false;
                    EditLocation.Closed += new EventHandler(this.OnOperFrmClosed);
                }
                else
                {
                    MessageBox.Show("Invalid Location selected.");
                }
            }
            else if (SelectedTagType == TagType.NotAssigned)
            {
                // Open Add Item Form
                frmAddLocation AddLocation = new frmAddLocation();
                AddLocation.TagNo = tagWrBnkInput.TagNo;
                AddLocation.newLocation = true;
                AddLocation.Show();
                this.Enabled = false;
                AddLocation.Closed += new EventHandler(this.OnOperFrmClosed);
            }
            else
            {
                MessageBox.Show("Tag already assigned.");
            }

            //if (SelectedTagType != TagType.NotAssigned)
            //{
            //    // OverwriteTag(SelectedTagType, tagWrBnkInput.TagNo);
            //    MessageBox.Show("Tag already assigned.");
            //}

        }

        //private void OverwriteTag(TagType SelectedTagType, string sOldTagNo)
        //{
        //    switch (SelectedTagType)
        //    {
        //        case TagType.Asset:
        //            frmAddAsset fAddAsset = new frmAddAsset();
        //            fAddAsset.OpenMode = frmAddAsset.FormType.OverWiteTag;

        //        case TagType.Employee:
        //            frmAddEmployee fAddEmployee = new frmAddEmployee();
        //        case TagType.Location:
        //            frmAddLocation fAddLocation = new frmAddLocation();


        //    };
        //}

        private void btnAddEmployee_Click(object sender, EventArgs e)
        {
            if (tagWrBnkInput.TagNoModified())
            {
                if (MessageBox.Show("Tag No. Modified, Still want to Add Employee.", "Info..", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button1) == DialogResult.No)
                {
                    return;
                }
            }

            if (EPCListV.SelectedIndices.Count == 0)
            {
                MessageBox.Show("Please select the Tag first.");
                return;
            }

            //Edit the database
            if (SelectedTagType == TagType.NotSet)
            {
                ListViewItem row = EPCListV.Items[EPCListV.SelectedIndices[0]];
                LoadExternalData(row);
            }
            if (SelectedTagType != TagType.NotAssigned)
            {
                MessageBox.Show("Tag already assigned.");
            }
            else
            {
                // Open Add Item Form
                frmAddEmployee AddEmployee = new frmAddEmployee();
                AddEmployee.TagNo = tagWrBnkInput.TagNo;
                AddEmployee.Show();
                this.Enabled = false;
                AddEmployee.Closed += new EventHandler(this.OnOperFrmClosed);
            }
        }

        private void btnScnBarcode_Click(object sender, EventArgs e)
        {
            // Open new BarCode Scanning Window
            frmBCScanForm fBCSFm = new frmBCScanForm();
            fBCSFm.Owner = this;
            fBCSFm.OpenMode = frmBCScanForm.FormType.Addform;
            fBCSFm.Show();
            // disable form until this new form closed
            this.Enabled = false;
            fBCSFm.Closed += new EventHandler(this.OnOperFrmClosed);
        }
    }
}