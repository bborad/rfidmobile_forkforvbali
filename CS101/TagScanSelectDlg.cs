using System;using HHDeviceInterface.RFIDSp;  using ReaderTypes; using ClslibHotkey;
using System.ComponentModel;
using System.Windows.Forms;
using ClsReaderLib;using ClsReaderLib.Devices;

namespace OnRamp
{
    public partial class TagScanSelectDlg : Form
    {
        private TagChildFormOperStateNotify OpStateChangeNotify;

        public struct TagID
        {
            public UInt16 PC;
            public UINT96_T EPC;
        }

        public TagID ChosenTag
        {
            get
            {
                TagID Tag = new TagID();
                if (TagLstV.SelectedIndices != null && TagLstV.SelectedIndices.Count > 0)
                {
                    ListViewItem Item = TagLstV.Items[TagLstV.SelectedIndices[0]];
                    if (Item.Tag is TagID)
                    {
                        Tag = (TagID)Item.Tag;
                    }
                }
                return Tag;
            }
        }

        public TagScanSelectDlg()
        {
            InitializeComponent();

            ScanBttnInitState();
        }

        public TagScanSelectDlg(TagChildFormOperStateNotify opStNotify)
        {
            InitializeComponent();

            ScanBttnInitState();

            OpStateChangeNotify = opStNotify;
        }

        #region Scan Button

        enum OpState
        {
            Starting,
            Started,
            Stopping,
            Stopped,
        }

        private void ScanBttnInitState()
        {
            OpState state = OpState.Stopped;
            ScanBttn.Tag = state;
        }

        private OpState ScanBttnState()
        {
            OpState state = OpState.Stopped;
            if (ScanBttn.Tag is OpState)
            {
                state = (OpState)ScanBttn.Tag;
            }
            else
                throw new ApplicationException("ScanBttn Tag is not of type OpState");

            return state;
        }

        private void ScanBttnSetState(OpState newState)
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagScanSelectDlg));

            ScanBttn.Tag = newState;
            switch (newState)
            {
                case OpState.Starting:
                    ScanBttn.Enabled = false;
                    OKBttn.Enabled = false;
                    CancelBttn.Enabled = false;
                    if (OpStateChangeNotify != null)
                        OpStateChangeNotify(TagOperEvtType.Started);
                    break;
                case OpState.Started:
                    ScanBttn.Text = "Stop";
                    ScanBttn.Enabled = true;
                    OpStateChangeNotify(TagOperEvtType.Updated);
                    break;
                case OpState.Stopping:
                    ScanBttn.Enabled = false;
                    OpStateChangeNotify(TagOperEvtType.Updated);
                    break;
                case OpState.Stopped:
                    ScanBttn.Text = resources.GetString("ScanBttn.Text");
                    ScanBttn.Enabled = true;
                    OKBttn.Enabled = true;
                    CancelBttn.Enabled = true;
                    OpStateChangeNotify(TagOperEvtType.Stopped);
                    break;
            }
        }


        private void OnScanBttnClicked(object sender, EventArgs e)
        {
            OpState ScanState = ScanBttnState();
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            //Reader rdr = ReaderFactory.GetReader();
            Reader rdr = ReaderFactory.GetReader();
            Program.RefreshStatusLabel(this, null);
            switch (ScanState)
            {
                case OpState.Stopped:
                    // Clear UI
                    ClearTagListV();
                    // Clear List in Mw
                    if (!rdr.TagInvtryClr())
                        MessageBox.Show("Clear Tag Inventory Failed.", "Error");
                    // Tag Inventory (continuous)
                    //rdr.InventoryOpStEvent += InvtryOpEvtHandler;
                    //rdr.DscvrTagEvent += new EventHandler<DscvrTagEventArgs>(DscvrTagEvtHandler);
                    rdr.RegisterInventoryOpStEvent(InvtryOpEvtHandler);
                    rdr.RegisterDscvrTagEvent(DscvrTagEvtHandler);
                    // dispatch start request //TestStart();
                    // disable button until actually started
                    ScanBttnSetState(OpState.Starting);
                    Program.RefreshStatusLabel(this, "Starting...");
#if false // need to add duplicate filtering at the same time
                    if (!rdr.SetRepeatedTagObsrvMode(false))
                        // Continue despite error
                        MessageBox.Show("Disable Repeated Tag Observation mode failed", "Error");
#endif
                    //UserPref Pref = UserPref.GetInstance();
                    //Byte[] Mask; uint MaskOffset;
                    //Pref.GetEPCBnkSelMask(out Mask, out MaskOffset);
                    if (!rdr.SetRepeatedTagObsrvMode(true))
                        // Continue despite error
                        MessageBox.Show("Enable Repeated Tag Observation mode failed", "Error");
                    if (!rdr.TagInventoryStart(5))
                    {
                        MessageBox.Show("Tag Inventory Start Failed", "Error");
                        // rewind previous setup
                        ScanBttnSetState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case OpState.Started:
                    // dispatch stop request//TestStop();
                    // disable button until actually stopped
                    ScanBttnSetState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!rdr.TagInventoryStop())
                    {
                        MessageBox.Show("Tag Inventory Stop Failed", "Error");
                        ScanBttnSetState(OpState.Stopped);
                        Program.RefreshStatusLabel(this, "Error Stopped");
                        //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                        //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                        rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                        rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    }
                    break;
                case OpState.Starting:
                    ScanBttnSetState(OpState.Stopping);
                    Program.RefreshStatusLabel(this, "Stopping...");
                    if (!rdr.TagInventoryStop())
                    {
                        // Restore 
                        ScanBttnSetState(OpState.Starting);
                        Program.RefreshStatusLabel(this, "Starting...");
                        Program.ShowWarning("Tag Inventory Stop failed during Starting phase, please try again");
                    }
                    break;
            }
        }
#endregion

        #region Buttons

        private void OnOKBttnClicked(object sender, EventArgs e)
        {
            if (TagLstV.SelectedIndices == null || TagLstV.SelectedIndices.Count <= 0)
            {
                this.DialogResult = DialogResult.No; // override
            }            
        }
        
        #endregion

        #region TagCntLbl Routines
        private void RefreshTagCntLabel()
        {
            TagCntLbl.Text = TagLstV.Items.Count.ToString();
            // force 'immediate' redraw (tag event too much in a short period of time)
            TagCntLbl.Refresh();
        }
        #endregion

        #region TagLstV Routines
        private bool TagAlreadyInListV (UInt16 PC, UINT96_T EPC)
        {
            bool Found = false;
            foreach (ListViewItem item in TagLstV.Items)
            {
                if (item.Tag is TagID)
                {
                    TagID Tag = (TagID)item.Tag;
                    if ((Tag.PC == PC)
                        && (Tag.EPC.m_LSB == EPC.m_LSB)
                        && (Tag.EPC.m_CSB == EPC.m_CSB)
                        && (Tag.EPC.m_MSB == EPC.m_MSB))
                    {
                        Found = true;
                        break;
                    }

                }
                else
                    Program.ShowError("ListViewItem Tag is not of type TagID");
            }
            return Found;
        }

        private void AddTagToListV(UInt16 PC, UINT96_T EPC)
        {
            // Assuming that the Tag of this EPC is not already in list
            int CurNumRows = TagLstV.Items != null ? TagLstV.Items.Count : 0;
            if (TagAlreadyInListV(PC, EPC))
                return;

            ListViewItem item = new ListViewItem(new String[] {
                 (CurNumRows+1).ToString(), PC.ToString("X4"), EPC.ToString() });
            TagLstV.Items.Add(item);
            TagID Tag = new TagID();
            Tag.PC = PC;
            Tag.EPC = EPC;
            item.Tag = Tag;
            RefreshTagCntLabel();
        }

        private void AddTagToListV(UInt16[] PCArr, UINT96_T[] EPCArr)
        {
            TagLstV.SuspendLayout();
            int CurNumRows = TagLstV.Items != null ? TagLstV.Items.Count : 0;
            TagID Tag = new TagID();
            for (int i = 0; i < EPCArr.Length; i++, CurNumRows++)
            {
                ListViewItem item = new ListViewItem(new String[] {
                     (CurNumRows+1).ToString(), PCArr[i].ToString("X4"),
                    EPCArr[i].ToString() });
                TagLstV.Items.Add(item);
                Tag.PC = PCArr[i];
                Tag.EPC = EPCArr[i];
                item.Tag = Tag;
            }
            TagLstV.ResumeLayout();
            RefreshTagCntLabel();
        }

        private void AddTagToListV(TagRangeList tagList)
        {
            TagLstV.SuspendLayout();
            foreach (TagListItem Tag in tagList)
                AddTagToListV(Tag.PC, Tag.EPC);
            TagLstV.ResumeLayout();
        }

        private void ClearTagListV()
        {
            TagLstV.Items.Clear();
            RefreshTagCntLabel();
        }
        #endregion

        #region RFIDRdr Event Handlers
        private void InvtryOpEvtHandler(object sender, InvtryOpEventArgs e)
        {
            OpState state = ScanBttnState();
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            //Reader rdr = ReaderFactory.GetReader();
            Reader rdr = ReaderFactory.GetReader();
            switch (e.status)
            {
                case InvtryOpStatus.started:
                    Program.RefreshStatusLabel(this, "Running...");
                    ScanBttnSetState(OpState.Started);
                    break;
                case InvtryOpStatus.stopped:
                case InvtryOpStatus.errorStopped:
                case InvtryOpStatus.macErrorStopped:
                    //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                    //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                    rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                    rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                    Program.RefreshStatusLabel(this, "Finished");
                    if (e.status == InvtryOpStatus.errorStopped)
                    {
                        if (rdr.LastErrCode == HRESULT_RFID.E_RFID_ERROR_RADIO_NOT_PRESENT)
                            RestartRfidDevice();
                        else
                            MessageBox.Show(e.msg, "Tag Inventory Stopped with Error");
                    }
                    else if (e.status == InvtryOpStatus.macErrorStopped)
                    {
                        //ushort MacErrCode;

                        if (rdr.GetMacError() && Reader.macerr != 0)
                        {
                            if (rdr.MacErrorIsFatal())
                            {
                                Program.ShowError(e.msg);
                            }
                            else if (ScanBttnState() != OpState.Stopping)
                            {
                                Program.ShowError(e.msg);
                            }
                        }
                        else
                            Program.ShowError("Unknown hardware (mac) error");
                    }
                    // restore StartButton
                    ScanBttnSetState(OpState.Stopped);
                    break;
                case InvtryOpStatus.error:
                    // restore StartButton
                    switch (state)
                    {
                        case OpState.Starting:
                            MessageBox.Show("Check connection and try again.", "Tag Inventory Start Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Started:
                            // TBD
                            MessageBox.Show(e.msg, "Tag Inventry Error");
                            break;
                        case OpState.Stopping:
                            MessageBox.Show(e.msg, "Tag Inventory Stop Error");
                            //rdr.InventoryOpStEvent -= InvtryOpEvtHandler;
                            //rdr.DscvrTagEvent -= DscvrTagEvtHandler;
                            rdr.UnregisterInventoryOpStEvent(InvtryOpEvtHandler);
                            rdr.UnregisterDscvrTagEvent(DscvrTagEvtHandler);
                            break;
                        case OpState.Stopped:
                            throw new ApplicationException("Unexpected error return during Stopped State");
                    }
                    ScanBttnSetState(OpState.Stopped);
                    Program.RefreshStatusLabel(this, "Error Stopped");
                    break;
                case InvtryOpStatus.intervalTimeRpt:
                    //Program.RefreshStatusLabel(this, "Running for " + e.ElapsedTime + " ms");
                    break;
                case InvtryOpStatus.cycBegin:
                    //invtryCycRan++;
                    //Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Begins");
                    break;
                case InvtryOpStatus.cycEnd:
                    //Program.RefreshStatusLabel(this, "InvCyc " + invtryCycRan + " Ends");
                    break;
                default:
                    break;
            }
        }

        //bool firstTag = true;
        private void DscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            // Add new TagID to list (or update occurrence)
            if (e.Cnt != 0)
            {
                AddTagToListV(e.PC, e.EPC);
                if (OpStateChangeNotify != null)
                    OpStateChangeNotify(TagOperEvtType.Updated);
            }
            else if (e.EPCArr != null)
                AddTagToListV(e.PCArr, e.EPCArr);
#if false
            // Testing only
            if (firstTag)
            {
                RFIDRdr.GetInstance().TagInvtryStop();
                firstTag = false;
            }
#endif
        }

        private void OnLoadDscvrTagEvtHandler(object sender, DscvrTagEventArgs e)
        {
            //RFIDRdr rdr = RFIDRdr.GetInstance();
            //Reader rdr = ReaderFactory.GetReader();
            Reader rdr = ReaderFactory.GetReader();
            //rdr.DscvrTagEvent -= OnLoadDscvrTagEvtHandler; // one-shot
            rdr.UnregisterDscvrTagEvent(OnLoadDscvrTagEvtHandler);
            if (e.EPCArr != null)
                AddTagToListV(e.PCArr, e.EPCArr);

            Program.RefreshStatusLabel(this, "Ready");
        }
        #endregion

        #region Form Routines
        private void OnDlgLoad(object sender, EventArgs e)
        {
            if (TagInvtryForm.TagList != null)
                AddTagToListV(TagInvtryForm.TagList);

            // Set up to receive HotKey event
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
        }

        private void OnDlgClosed(object sender, EventArgs e)
        {
            // Remove HotKey handler
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
        }
        

        private void OnDlgClosing(object sender, CancelEventArgs e)
        {
            if (ScanBttnState() != OpState.Stopped)
            {
                MessageBox.Show("Please Stop The Scan Operation First", "Request Denied");
                e.Cancel = true;
            }

        }
        #endregion

        #region Hotkey
        private void HotKeyEvtHandler(eVKey keyCode, bool down)
        {
            switch (keyCode)
            {
                case eVKey.VK_F11:
                if (down)
                {
                    // fake 'Start' key press if not already running
                    if (ScanBttnState() == OpState.Stopped)
                    {
                        OnScanBttnClicked(this, null);
                    }
                }
                else // up
                {
                    if (ScanBttnState() == OpState.Starting
                        || ScanBttnState() == OpState.Started)
                    {
                        // Stop!
                        OnScanBttnClicked(this, null);
                    }
                }
                break;
            case eVKey.VK_F4:
            case eVKey.VK_F5:
                if (down)
                {
                    if (ScanBttnState() == OpState.Stopped)
                    {
                        AntPwrHtKyPopup Popup = new AntPwrHtKyPopup(HotKeyEvtHandler);
                        Popup.ShowDialog();
                    }
                }
                break;       
            }
        }
        #endregion

        #region Handle Restart RFID Device (after E_RFID_ERROR_RADIO_NOT_PRESENT error)
        private void RestartRfidDevice()
        {
            ClsHotkey.SubHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = false; // Disable Form to prevent user touching GUI
            Program.PromptUserRestartRadio(RadioRestartCompleted);
        }

        private void RadioRestartCompleted(bool succ)
        {
            ClsHotkey.AddHotKeyDelegate(HotKeyEvtHandler);
            this.Enabled = true;
        }
        #endregion

  
    }
}