namespace OnRamp
{
    partial class RdrInfoSummaryForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;
        private System.Windows.Forms.MainMenu mainMenu1;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(RdrInfoSummaryForm));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.mainMenu1 = new System.Windows.Forms.MainMenu();
            this.RdrNameLbl = new System.Windows.Forms.Label();
            this.FrqBndLbl = new System.Windows.Forms.Label();
            this.CountryLbl = new System.Windows.Forms.Label();
            this.PwrLbl = new System.Windows.Forms.Label();
            this.LnkPrfLbl = new System.Windows.Forms.Label();
            this.IPAddrLbl = new System.Windows.Forms.Label();
            this.GWLbl = new System.Windows.Forms.Label();
            this.DNSLbl = new System.Windows.Forms.Label();
            this.DhcpLnkLbl = new System.Windows.Forms.LinkLabel();
            this.SuspendLayout();
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // label5
            // 
            resources.ApplyResources(this.label5, "label5");
            this.label5.Name = "label5";
            // 
            // label6
            // 
            resources.ApplyResources(this.label6, "label6");
            this.label6.Name = "label6";
            // 
            // label7
            // 
            resources.ApplyResources(this.label7, "label7");
            this.label7.Name = "label7";
            // 
            // label9
            // 
            resources.ApplyResources(this.label9, "label9");
            this.label9.Name = "label9";
            // 
            // label8
            // 
            resources.ApplyResources(this.label8, "label8");
            this.label8.Name = "label8";
            // 
            // RdrNameLbl
            // 
            resources.ApplyResources(this.RdrNameLbl, "RdrNameLbl");
            this.RdrNameLbl.Name = "RdrNameLbl";
            // 
            // FrqBndLbl
            // 
            resources.ApplyResources(this.FrqBndLbl, "FrqBndLbl");
            this.FrqBndLbl.Name = "FrqBndLbl";
            // 
            // CountryLbl
            // 
            resources.ApplyResources(this.CountryLbl, "CountryLbl");
            this.CountryLbl.Name = "CountryLbl";
            // 
            // PwrLbl
            // 
            resources.ApplyResources(this.PwrLbl, "PwrLbl");
            this.PwrLbl.Name = "PwrLbl";
            // 
            // LnkPrfLbl
            // 
            resources.ApplyResources(this.LnkPrfLbl, "LnkPrfLbl");
            this.LnkPrfLbl.Name = "LnkPrfLbl";
            // 
            // IPAddrLbl
            // 
            resources.ApplyResources(this.IPAddrLbl, "IPAddrLbl");
            this.IPAddrLbl.Name = "IPAddrLbl";
            // 
            // GWLbl
            // 
            resources.ApplyResources(this.GWLbl, "GWLbl");
            this.GWLbl.Name = "GWLbl";
            // 
            // DNSLbl
            // 
            resources.ApplyResources(this.DNSLbl, "DNSLbl");
            this.DNSLbl.Name = "DNSLbl";
            // 
            // DhcpLnkLbl
            // 
            resources.ApplyResources(this.DhcpLnkLbl, "DhcpLnkLbl");
            this.DhcpLnkLbl.Name = "DhcpLnkLbl";
            this.DhcpLnkLbl.Click += new System.EventHandler(this.OnDhcpLnkClicked);
            // 
            // RdrInfoSummaryForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            resources.ApplyResources(this, "$this");
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.DhcpLnkLbl);
            this.Controls.Add(this.DNSLbl);
            this.Controls.Add(this.GWLbl);
            this.Controls.Add(this.IPAddrLbl);
            this.Controls.Add(this.LnkPrfLbl);
            this.Controls.Add(this.PwrLbl);
            this.Controls.Add(this.CountryLbl);
            this.Controls.Add(this.FrqBndLbl);
            this.Controls.Add(this.RdrNameLbl);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "RdrInfoSummaryForm";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.OnClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.OnKeyDown);
            this.Load += new System.EventHandler(this.OnLoad);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label RdrNameLbl;
        private System.Windows.Forms.Label FrqBndLbl;
        private System.Windows.Forms.Label CountryLbl;
        private System.Windows.Forms.Label PwrLbl;
        private System.Windows.Forms.Label LnkPrfLbl;
        private System.Windows.Forms.Label IPAddrLbl;
        private System.Windows.Forms.Label GWLbl;
        private System.Windows.Forms.Label DNSLbl;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.LinkLabel DhcpLnkLbl;
    }
}