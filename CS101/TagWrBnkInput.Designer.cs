namespace OnRamp
{
    partial class TagWrBnkInput
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TagWrBnkInput));
            this.B0_2TxtBx = new System.Windows.Forms.TextBox();
            this.B1_2TxtBx = new System.Windows.Forms.TextBox();
            this.B3TxtBx = new System.Windows.Forms.TextBox();
            this.B1_1TxtBx = new System.Windows.Forms.TextBox();
            this.B0_1TxtBx = new System.Windows.Forms.TextBox();
            this.txtName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // B0_2TxtBx
            // 
            resources.ApplyResources(this.B0_2TxtBx, "B0_2TxtBx");
            this.B0_2TxtBx.Name = "B0_2TxtBx";
            this.B0_2TxtBx.TextChanged += new System.EventHandler(this.OnB0_2TxtBxChanged);
            this.B0_2TxtBx.GotFocus += new System.EventHandler(this.OnMBTxtBxGotFocus);
            this.B0_2TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.B0_2TxtBx.LostFocus += new System.EventHandler(this.OnMBTxtBxFocusLost);
            // 
            // B1_2TxtBx
            // 
            resources.ApplyResources(this.B1_2TxtBx, "B1_2TxtBx");
            this.B1_2TxtBx.Name = "B1_2TxtBx";
            this.B1_2TxtBx.TextChanged += new System.EventHandler(this.OnB1_2TxtBxChanged);
            this.B1_2TxtBx.GotFocus += new System.EventHandler(this.OnMBTxtBxGotFocus);
            this.B1_2TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.B1_2TxtBx.LostFocus += new System.EventHandler(this.OnMBTxtBxFocusLost);
            // 
            // B3TxtBx
            // 
            resources.ApplyResources(this.B3TxtBx, "B3TxtBx");
            this.B3TxtBx.Name = "B3TxtBx";
            this.B3TxtBx.TextChanged += new System.EventHandler(this.OnB3TxtBxChanged);
            this.B3TxtBx.GotFocus += new System.EventHandler(this.OnMBTxtBxGotFocus);
            this.B3TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.B3TxtBx.LostFocus += new System.EventHandler(this.OnMBTxtBxFocusLost);
            // 
            // B1_1TxtBx
            // 
            resources.ApplyResources(this.B1_1TxtBx, "B1_1TxtBx");
            this.B1_1TxtBx.Name = "B1_1TxtBx";
            this.B1_1TxtBx.TextChanged += new System.EventHandler(this.OnB1_1TxtBxChanged);
            this.B1_1TxtBx.GotFocus += new System.EventHandler(this.OnMBTxtBxGotFocus);
            this.B1_1TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.B1_1TxtBx.LostFocus += new System.EventHandler(this.OnMBTxtBxFocusLost);
            // 
            // B0_1TxtBx
            // 
            resources.ApplyResources(this.B0_1TxtBx, "B0_1TxtBx");
            this.B0_1TxtBx.Name = "B0_1TxtBx";
            this.B0_1TxtBx.TextChanged += new System.EventHandler(this.OnB0_1TxtBxChanged);
            this.B0_1TxtBx.GotFocus += new System.EventHandler(this.OnMBTxtBxGotFocus);
            this.B0_1TxtBx.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.HexKeyPressChk);
            this.B0_1TxtBx.LostFocus += new System.EventHandler(this.OnMBTxtBxFocusLost);
            // 
            // txtName
            // 
            resources.ApplyResources(this.txtName, "txtName");
            this.txtName.Name = "txtName";
            // 
            // TagWrBnkInput
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(96F, 96F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.B1_2TxtBx);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.B1_1TxtBx);
            this.Controls.Add(this.B0_1TxtBx);
            this.Controls.Add(this.B3TxtBx);
            this.Controls.Add(this.B0_2TxtBx);
            this.Name = "TagWrBnkInput";
            resources.ApplyResources(this, "$this");
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox B0_2TxtBx;
        private System.Windows.Forms.TextBox B1_2TxtBx;
        private System.Windows.Forms.TextBox B3TxtBx;
        private System.Windows.Forms.TextBox B1_1TxtBx;
        private System.Windows.Forms.TextBox B0_1TxtBx;
        private System.Windows.Forms.TextBox txtName;
    }
}
